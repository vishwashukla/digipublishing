﻿using System;
using System.Data;

public partial class Home : System.Web.UI.Page
{
    public string strHTML = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string singlePageQuery = "select SinglePageCode from brand_settings";
        DBHelper dp1 = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt = dp1.FillDataTable(singlePageQuery);
        if (dt.Rows.Count > 0)
        {
            strHTML =Convert.ToString(dt.Rows[0]["SinglePageCode"]);
        }
    }
    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}