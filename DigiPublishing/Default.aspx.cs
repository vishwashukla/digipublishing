﻿using System;
using System.Data;
using System.Web.UI;

public partial class _Default : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        /* Code to make this page non-accessible by Super Admin*/
        if (Request.Url.Host.ToString() == ConfigLoader.GetSAMainURL())
            Response.Redirect(ConfigLoader.GetErrorPageUrl());
        /*******************************************************************************/

        //Server.Transfer("~/Features/Index.aspx");

        if (!Page.IsPostBack)
        {
            setDefaultHomePage();
        }
    }

    protected void setDefaultHomePage()
    {
        try
        {
            string defaultPage = "";
            string defaultPageType = "";
            string Searchkeyword = "";
            DBCommand strQuery = new DBCommand();
            strQuery.CommandText = "select defaultfeature,defaultfeatureType,defaultfeatureMulti,SearchKeyword,defaultSearch from brand_settings";
            strQuery.CommandType = CommandType.Text;
            DataTable dt = dp.FillDataTable(strQuery);
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToString(dt.Rows[0]["defaultfeature"]) != "")
                {
                    defaultPage = Convert.ToString(dt.Rows[0]["defaultfeature"]);
                }
                if (Convert.ToString(dt.Rows[0]["defaultfeatureType"]) != "")
                {
                    defaultPageType = Convert.ToString(dt.Rows[0]["defaultfeatureType"]);
                }
                Searchkeyword = Convert.ToString(dt.Rows[0]["SearchKeyword"]);
            }
            if (defaultPageType == "0")
            {
                if (!string.IsNullOrWhiteSpace(defaultPage))
                {
                    features currFeatures = (features)Convert.ToInt32(defaultPage);
                    switch (currFeatures)
                    {
                        case features.Calendar:
                            Server.Transfer("~/Calendar/Index.aspx");
                            break;
                        case features.Blog:
                            Server.Transfer("~/Features/Index.aspx");
                            break;
                        case features.RSS:
                            Server.Transfer("~/Feeds/Index.aspx");
                            break;
                        case features.Bookmark:
                            Server.Transfer("~/Links/Index.aspx");
                            break;
                        case features.BlogPostSummary:
                            Server.Transfer("~/BlogPostSummary.aspx");
                            break;
                        case features.Search:
                            string defaultSearch = "1";
                            defaultSearch = Convert.ToString(dt.Rows[0]["defaultSearch"]);
                            if (defaultSearch == "2")
                            {
                                Response.Redirect("~/search.aspx?q=" + Searchkeyword);
                            }
                            else
                            {
                                Session["q"] = Searchkeyword;
                                Server.Transfer("~/search.aspx");
                            }
                            break;
                        default:
                            Server.Transfer("~/Features/Index.aspx");
                            break;
                    }
                }
                else
                    Server.Transfer("~/ondemandhome.aspx");
            }
            else if (defaultPageType == "1")
            {
                Server.Transfer("~/ondemandhome.aspx");
            }
            else if (defaultPageType == "2")
            {
                Server.Transfer("~/Home.aspx");
            }
            else
            {
                Server.Transfer("~/ondemandhome.aspx");
            }
        }
        catch(Exception ex)
        {
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}