﻿<%@ Page  Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Features_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
      <script type="text/javascript">        
          BannerIDs = BannerIDs+'<%= BannerIDs%>';
    </script>
    <div id="content-full">
        <div id="blogpost">
            <div>
                <!---<strong>
                    <asp:Label ID="lblBlogtitle" runat="server"></asp:Label>
                </strong>&nbsp;&nbsp; --->
                <asp:DataList ID="dlstBlog" runat="server" Width="100%">
                    <ItemTemplate>
                        <table style="width:100%;">
                            <tr>
                                <td>
                                   <%--<asp:Label ID="lblBlogPosts" runat="server" CssClass="lblBlogPostsTitle" Text='<%# utils.formatStringForURLSelect(Convert.ToString(Eval("post_title")))%>'></asp:Label>&nbsp;&nbsp; --%>
                                   <h1 id="lblBlogPosts" runat="server" class="lblBlogPostsTitle"><%# utils.formatStringForURLSelect(Convert.ToString(Eval("post_title")))%></h1>&nbsp;&nbsp;
                                    <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("post_id")%>'></asp:Label>
                                    <br />
                                    <div id="divPost" runat="server" style="display: inline;">
                                        <div class="st_blogpost_container">
                                            <%# utils.formatStringForURLSelect(Convert.ToString(Eval("post_description")))%>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr align="left" runat="server">
                                <td colspan="2" runat="server">
                                    <%# utils.formatStringForURLSelect(Convert.ToString(Eval("adString")))%>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
             
                <asp:Panel ID="pnlPagingMain" runat="server" Visible="false">
                    <div style="width:100%;text-align:center;">
                        <table style="display:inline;">
                            <tr>
                                <td style="text-align:center">
                                    <asp:LinkButton ID="lbtnPrevious" Font-Bold="true" runat="server" CausesValidation="false" OnClick="lbtnPrevious_Click">...</asp:LinkButton>
                                    &nbsp;&nbsp;
                                </td>
                                <td style="text-align:center;vertical-align:middle;">
                                    <asp:DataList ID="dlPaging" runat="server" RepeatDirection="Horizontal" OnItemCommand="dlPaging_ItemCommand"
                                        OnItemDataBound="dlPaging_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnPaging" Font-Bold="true" runat="server" PostBackUrl="~/Features/Index.aspx" CommandArgument='<%# Eval("PageIndex") %>' CommandName="Paging" Text='<%# Eval("PageText") %>'>
                                            </asp:LinkButton>&nbsp;
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                                <td style="text-align:left">
                                    &nbsp;&nbsp;<asp:LinkButton ID="lbtnNext" Font-Bold="true" runat="server" CausesValidation="false" OnClick="lbtnNext_Click">...</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align:center;vertical-align:middle;height:30px">
                                    <asp:Label ID="lblPageInfo" Font-Bold="true" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
