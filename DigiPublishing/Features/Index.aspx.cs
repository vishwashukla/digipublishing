﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Features_Index : System.Web.UI.Page
{
   
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    public string strMidWidget = string.Empty;
    public string BannerIDs = string.Empty;
    public string strDivMidArticle = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Features";

        if (!utils.CheckFeature((int)features.Blog))
            Response.Redirect("~/OndemandHome.aspx");

        if (!IsPostBack)
        {
            strMidWidget = GetWidgets();
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["post_title"])) && !Convert.ToString(Request.RawUrl).ToLower().Contains("features/index.aspx"))
            {
                string post = Convert.ToString(Request.QueryString["post_title"]).Trim().Replace('-', ' ');
                DBCommand cmdPostSelect = new DBCommand();
                cmdPostSelect.CommandText = "SP_Brand_Select_BRAND_BLOG_POSTS";
                cmdPostSelect.CommandType = CommandType.StoredProcedure;
                DataTable dtTitleID = dp.FillDataTable(cmdPostSelect);
                foreach (DataRow dr in dtTitleID.Rows)
                {
                    string allPostTitles = Convert.ToString(dr["post_title"]).Trim().Replace("&quot;", "\"");
                    allPostTitles = utils.formatStringForURLSelect(allPostTitles);
                    allPostTitles = allPostTitles.Trim().Replace("&rsquo;", "'");
                    allPostTitles = utils.GeneratePostTitleForURL(allPostTitles);
                    allPostTitles = allPostTitles.Trim().Replace("-", " ");
                    allPostTitles = allPostTitles.Trim().Replace("  ", " ");
                    if (Convert.ToString(allPostTitles) == Convert.ToString(post))
                    {
                        ShowBlog(Convert.ToString(dr["post_id"]));
                    }
                }
            }
            else
            {
                GetLatestSortBlogPost();

            }
        }
       
    }

    private void ShowBlog(string postid)
    {
        dlstBlog.Visible = false;
        if (postid != "")
        {
            this.dlstBlog.DataSource = getBlog(postid);
            this.dlstBlog.DataBind();
            dlstBlog.Visible = true;
        }
    }

    private DataTable getBlog(string id)
    {
        DataTable dt = new DataTable();
        DBCommand cmdTopBRANDBLOG = new DBCommand();
        cmdTopBRANDBLOG.CommandText = "SP_Brand_Select_Top1_BRAND_BLOG_POSTS";
        cmdTopBRANDBLOG.CommandType = CommandType.StoredProcedure;
        cmdTopBRANDBLOG.Parameters.Add("@PostId", Convert.ToString(id));
        dt = dp.FillDataTable(cmdTopBRANDBLOG);
        dt.Columns.Add("adString");

        if (dt.Rows.Count > 0)
        {
            if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["post_id"])))
            {
                string postDesc = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["post_description"]));
                postDesc = postDesc.Replace("<ul>", "<ul style='list-style-type:square'>");
                dt.Rows[0]["post_description"] = postDesc;
                string strPostTitle = utils.formatStringForURLSelect(Convert.ToString(dt.Rows[0]["post_title"]));

                if (strPostTitle.Length > 50)
                    strPostTitle = strPostTitle.Substring(0, 50) + "...";
                dt.Rows[0]["post_title"] = strPostTitle;
                dt.Rows[0]["adString"] = strMidWidget.Replace("[(id)]", Convert.ToString(dt.Rows[0]["post_id"]));
            }
            dt.Rows[0]["fullname"] = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["fullname"]));
            string strBlogTitle = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["blog_title"]));
            if (strBlogTitle.Length > 50)
                strBlogTitle = strBlogTitle.Substring(0, 50) + "...";
           // lblBlogtitle.Text = strBlogTitle;
        }
        return dt;
    }

    private void RandomRows(DataTable dt)
    {
        DataTable dtFeatures = new DataTable();
        foreach (DataColumn dc in dt.Columns)
        {
            if (dc.ColumnName.ToLower() != "random")
            {
                dtFeatures.Columns.Add(dc.ColumnName);
            }
        }
        dtFeatures.Columns.Add("random", typeof(int));

        if (dt.Rows.Count > 0)
        {
            Random rnd = new Random();
            foreach (DataRow dr in dt.Rows)
            {
                DataRow drFeatures = dtFeatures.NewRow();
                foreach(DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToLower() != "random")
                    {
                        drFeatures[dc.ColumnName] = dr[dc.ColumnName];
                    }
                }
                drFeatures["random"] = rnd.Next(100);
                dtFeatures.Rows.Add(drFeatures);
            }
        }
        string random = dp.ExecuteScalar("select ISNULL(RandomiseStatus,0 ) from brand_settings").ToString();
        if (random == "True")
        {
            DataView dv = dtFeatures.DefaultView;
            dv.Sort = "random asc";
            dtFeatures = dv.ToTable();
            Session["featuresdtnew"] = dtFeatures;
        }
        else
        {
            Session["featuresdtnew"] = dtFeatures;
        }
    }

    private void GetLatestSortBlogPost()
    {
        DataTable dt = new DataTable();
        //cache code 
        string KeySortBlogPosts = "SortBlogPosts" + ReadConfig.getDomainUrl().ToLower().Trim();

        if (Cache[KeySortBlogPosts] != null)
        {
            dt = (DataTable)Cache[KeySortBlogPosts];
        }
        else
        {
            DBCommand cmdsortblog = new DBCommand();
            cmdsortblog.CommandText = "SP_Brand_Select_Blog_N_Posts";
            cmdsortblog.CommandType = CommandType.StoredProcedure;
            dt = dp.FillDataTable(cmdsortblog);
            //stringAd
            if (dt.Rows.Count > 0)
            {
                dt.Columns.Add("adString");
                foreach (DataRow dr in dt.Rows)
                {
                    dr["adString"] = strMidWidget.Replace("[(id)]", Convert.ToString(dr["post_id"]));
                }
            }
            Cache[KeySortBlogPosts] = dt;
        }

        RandomRows(dt);

        if (dt.Rows.Count > 0)
        {
           //lblBlogtitle.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["blog_title"]));
         
            BindItemsList();
        }
    }

    protected void BindItemsList()
    {
        if (Session["featuresdtnew"] != null)
        {
            PagedDataSource _PageDataSource = new PagedDataSource();
            DataView dv = ((DataTable)Session["featuresdtnew"]).DefaultView;
            if (dv != null)
            {
                DataTable dt = dv.ToTable();
                if (dt.Rows.Count > 10)
                    pnlPagingMain.Visible = true;
                else
                    pnlPagingMain.Visible = false;
                _PageDataSource.DataSource = dv;
                _PageDataSource.AllowPaging = true;
                _PageDataSource.PageSize = 10;
                _PageDataSource.CurrentPageIndex = CurrentPage;
                ViewState["TotalPages"] = _PageDataSource.PageCount;
                this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + _PageDataSource.PageCount;
                this.dlstBlog.DataSource = _PageDataSource;
                this.dlstBlog.DataBind();
                this.doPaging();
            }
        }
    }

    #region Private Properties
    private int CurrentPage
    {
        get
        {
            object objPage = ViewState["_CurrentPage"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPage"] = value; }
    }
    private int firstIndex
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndex"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndex"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndex"] = value; }
    }
    private int lastIndex
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndex"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndex"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndex"] = value; }
    }
    #endregion

    private void doPaging()
    {
        lbtnPrevious.Visible = true;
        lbtnNext.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        firstIndex = CurrentPage - (CurrentPage % 10);
        lastIndex = CurrentPage + (10 - (CurrentPage % 10));
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            firstIndex = lastIndex - 10;
            lbtnNext.Visible = false;
        }

        if (firstIndex <= 0)
        {
            firstIndex = 0;
            lbtnPrevious.Visible = false;
        }

        for (int i = firstIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
    }

    protected void lbtnPrevious_Click(object sender, EventArgs e)
    {
        lbtnPrevious.Visible = true;
        lbtnNext.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPage = CurrentPage - 10;
        firstIndex = CurrentPage - (CurrentPage % 10);
        lastIndex = CurrentPage + (10 - (CurrentPage % 10));

        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            firstIndex = lastIndex - 10;
            lbtnNext.Visible = false;
        }

        if (firstIndex <= 0)
        {
            firstIndex = 0;
            lbtnPrevious.Visible = false;
        }
        CurrentPage = firstIndex;
        for (int i = firstIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + Convert.ToString(ViewState["TotalPages"]);
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
        this.BindItemsList();
    }

    protected void lbtnNext_Click(object sender, EventArgs e)
    {
        lbtnPrevious.Visible = true;
        lbtnNext.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPage = CurrentPage + 10;
        firstIndex = CurrentPage - (CurrentPage % 10);
        lastIndex = CurrentPage + (10 - (CurrentPage % 10));

        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            firstIndex = lastIndex - 10;
            lbtnNext.Visible = false;
        }

        if (firstIndex <= 0)
        {
            firstIndex = 0;
            lbtnPrevious.Visible = false;
        }
        CurrentPage = firstIndex;
        for (int i = firstIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + Convert.ToString(ViewState["TotalPages"]);
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
        this.BindItemsList();
    }

    protected void dlPaging_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            CurrentPage = Convert.ToInt16(Convert.ToString(e.CommandArgument));
            this.BindItemsList();
        }
    }

    protected void dlPaging_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");
        if (Convert.ToString(lnkbtnPage.CommandArgument) == Convert.ToString(CurrentPage))
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("font-size", "Medium");
            lnkbtnPage.Style.Add("color", "red");
            lnkbtnPage.Font.Bold = true;
        }

    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
    //=====================================issue mid artical 3707==============================================================================================================
    private string GetWidgets()
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string widgetOS = utils.GetOperatingSystem();
        string stradValue = string.Empty;
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dtAdBox = dp.FillDataTable("select * from brand_Applist_Random where Random_Position='random_midarticle'");
        if (dtAdBox.Rows.Count > 0)
        {
            /*  MidArticle  */
            if (Convert.ToInt32(dtAdBox.Rows[0]["Value"]) == 1)
            {
                stradValue = funAdRandom("midarticle", widgetconid, widgetBrowser, widgetOS);
                if (!string.IsNullOrEmpty(stradValue))
                     stradValue = strDivMidArticle + stradValue + "</div>";
            }
            else if (Convert.ToInt32(dtAdBox.Rows[0]["Value"]) == 0)
            {
                stradValue = funAd("midarticle", widgetconid, widgetBrowser, widgetOS);
                if (!string.IsNullOrEmpty(stradValue))
                {
                    stradValue = strDivMidArticle + stradValue + "</div>";
                }
            }
            if (stradValue == "")
            {
                string imagealt = "";
                string filepath = Server.MapPath(ReadConfig.LogoPath);
                FileInfo fi = new FileInfo(filepath);
                if (fi.Exists)
                {
                    string filename = fi.Name;
                    string ext = fi.Extension;
                    imagealt = filename.Replace(ext, "").Replace("-", " ").Replace("_", " ");
                    stradValue = "<img src='" + ReadConfig.LogoPath.Replace("~", "..") + "' id='imgLogo' id='imgLogo' alt='" + imagealt + "' />";
                }
            }
        }
        return stradValue; ;
    }

    private string funAd(string strPosition, string widgetconid, string widgetBrowser, string widgetOS)
    {
        StringBuilder strAdBox = new StringBuilder();
        DataTable dtAdBoxIndivi = new DataTable();
        DataTable dtAdBox = new DataTable();

        string strSQLRule = string.Empty;
        int App_Id = 0;
        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_BrandAppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        strQueryAdBox.AddParameter("@Position", strPosition);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);

        if (dt_AppID.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_AppID.Rows)
            {
                App_Id = Convert.ToInt32(dr["id"]);
                string KeyWidgetAllPosition = "WidgetAllPosition" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + App_Id + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + strPosition.Trim() + widgetOS.Trim();
                if (Cache[KeyWidgetAllPosition] != null)
                {
                    dtAdBoxIndivi = (DataTable)Cache[KeyWidgetAllPosition];
                }
                else
                {
                    string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
                    strSQLRule = SqlQueryMain;
                    DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
                    dtAdBoxIndivi = dp1.FillDataTable(strSQLRule);
                    Cache.Insert(KeyWidgetAllPosition, dtAdBoxIndivi, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
                }

                if (dtAdBox.Rows.Count > 0)
                {
                    foreach (DataRow drIndivi in dtAdBoxIndivi.Rows)
                    {
                        dtAdBox.ImportRow(drIndivi);
                    }
                }
                else
                    dtAdBox = dtAdBoxIndivi.Copy();
            }
        }
        if (dtAdBox.Rows.Count > 0)
        {
            foreach (DataRow dr in dtAdBox.Rows)
            {
                if (utils.ReferalCheck(Convert.ToString(dr["Referal"]).Trim(), Convert.ToString(dr["Not_Referal"]).Trim()))
                {
                    strAdBox.Append(GetfunAd(dr, strPosition));
                }
            }
        }
        return Convert.ToString(strAdBox);
    }

    private string funAdRandom(string strPosition, string widgetconid, string widgetBrowser, string widgetOS)
    {
        StringBuilder strAdBox = new StringBuilder();
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        int App_Id = 0;

        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_Top1_Brand_AppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Position", strPosition);
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);

        if (dt_AppID.Rows.Count > 0)
        {
            App_Id = Convert.ToInt32(dt_AppID.Rows[0]["id"]);
        }
        string KeyWidgetRandomAllPosition = "WidgetRandomAllPosition" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + App_Id + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + strPosition.Trim() + widgetOS.Trim();
        if (Cache[KeyWidgetRandomAllPosition] != null)
        {
            dtAdBox = (DataTable)Cache[KeyWidgetRandomAllPosition];
        }
        else
        {
            string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
            strSQLRule = SqlQueryMain;
            DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            dtAdBox = dp1.FillDataTable(strSQLRule);
            Cache.Insert(KeyWidgetRandomAllPosition, dtAdBox, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        if (dtAdBox.Rows.Count > 0)
        {
            if (utils.ReferalCheck(Convert.ToString(dtAdBox.Rows[0]["Referal"]).Trim(), Convert.ToString(dtAdBox.Rows[0]["Not_Referal"]).Trim()))
            {
                strAdBox.Append(GetfunAd(dtAdBox.Rows[0], strPosition));
            }
        }
        return Convert.ToString(strAdBox);
    }

    private string GetfunAd(DataRow dr, string strPosition)
    {
        string stradimage = "";
        int IPStatus = 0;
        int IPValue = 0;
        int IPDays = 0;
        int Blocker = 0;
        string ApplicationName = Convert.ToString(dr["ApplicationName"]);
        int BlockerValue = 0;
        int BanID = Convert.ToInt32(dr["id"]);
        int height = 0;
        int width = 0;

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Status"])))
        {
            IPStatus = Convert.ToInt32(dr["SameIP_Status"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Days"])))
        {
            IPDays = Convert.ToInt32(dr["SameIP_Days"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Value"])))
        {
            IPValue = Convert.ToInt32(dr["SameIP_Value"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["Blocker"])))
        {
            Blocker = Convert.ToInt32(dr["Blocker"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["BlockerValue"])))
        {
            BlockerValue = Convert.ToInt32(dr["BlockerValue"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["height"])))
        {
            height = Convert.ToInt32(dr["height"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["width"])))
        {
            width = Convert.ToInt32(dr["width"].ToString().Trim());
        }

        if (strPosition == "midarticle")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "imageMidArticleBlock", "ClickMidArticle", "MidArticleBlockerCount", "SameIPCountMidArticle", "MidArticle");
        }
  

        StringBuilder strAdBox = new StringBuilder();
        if (height == 0 && width == 0)
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" >" + stradimage 
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]))) + "</div>");
        }
        else
        { 
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"height:auto;width:" + width + "px;\">" + stradimage
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]))) + "</div>");
        }
        return strAdBox.ToString();
    }

    private string Get_Image_RegisterScriptBlock_And_Attach_Click_Event(int BanID, int IPStatus, int IPValue, int IPDays, int Blocker, int BlockerValue, string ApplicationName
        , string imageid, string clickfunname, string JSVarBlockerCount, string JSVarSameIPCount, string ScriptblockKeyName)
    {

        int SameIPCount = utils.SameIPClicks(BanID, "banner", IPDays);

        BannerIDs += BanID + ",";
        string strdis = "none";
        string type = "0";
        if (IPStatus == 1 && IPValue <= SameIPCount && BanID != 0)
        {
            strdis = "block";
            type = "2";
        }
        if (strdis == "none" && Blocker != 1)
            type = "1";

        strDivMidArticle = "<div id=\"DivMidArticle\" style=\"position: relative; float: left;\"class=\"ads midArticle_ads_Box\" onclick=\"javascript:" + clickfunname + "('" + BanID + "'," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",'" + imageid + "','" + ApplicationName + "')\">";
        
        string stradimage = "<img  alt=\"1x2\" id =\"" + imageid + "\" style=\"display:" + strdis + ";z-index:9999;float:right;position:absolute;left:0;top:0;width:100%; height:100%;\" "
            + "src=\"../images_common/1x2.gif\" />";

        StringBuilder strScript = new StringBuilder();
        strScript.Append(" <script type=\"text/javascript\" language=\"javascript\">");
        strScript.Append(" var " + JSVarBlockerCount + " = 0;var " + JSVarSameIPCount + " = " + SameIPCount + ";");
        strScript.Append("</script>");
        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), ScriptblockKeyName, strScript.ToString());
        return stradimage;
    }
    protected void dlstBlog_ItemDataBound(object sender, DataListItemEventArgs e)
    {

    }
}