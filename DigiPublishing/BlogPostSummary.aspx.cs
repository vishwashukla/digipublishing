﻿using System;
using System.Data;
using System.Text.RegularExpressions;

public partial class BlogPostSummary : System.Web.UI.Page
{
    public string strHTML = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string strQuery = "select BlogsNUmberInSummary from brand_settings";
        DBHelper dp1 = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt = dp1.FillDataTable(strQuery);
        int numberofposts = 0;
        if (dt.Rows.Count > 0)
        {
            string Key = "TOTALNUMBEROFBLOGPOST_" + ReadConfig.getDomainUrl();
            DataTable dtposts = new DataTable();//RK 18-07-2019 CACHE
            if (Cache[Key] != null)
            {
                dtposts = (DataTable)Cache[Key];
            }
            else
            {
                numberofposts = Convert.ToInt32(dt.Rows[0]["BlogsNUmberInSummary"]);
                string selectPosts = "SELECT TOP " + numberofposts + " post_id,post_title,post_description "
                                    + "FROM BRAND_BLOG_NEW LEFT OUTER JOIN BRAND_BLOG_POSTS ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id "
                                    + "LEFT JOIN  brand_user ON BRAND_BLOG_POSTS.c_by = brand_user.uid "
                                    + "WHERE blog_id=(Select  TOP 1 blog_id from BRAND_BLOG_NEW "
                                    + "left join  brand_user on BRAND_BLOG_NEW.c_by = brand_user.uid "
                                    + "Where BRAND_BLOG_NEW.blog_status <>2) "
                                    + "and post_status<>'0' AND blog_subchannel_id=1 and BRAND_BLOG_POSTS.Active_Inactive=1 "
                                    + "ORDER BY SortOrder DESC";
                dtposts = dp1.FillDataTable(selectPosts);
                Cache[Key] = dtposts;
            }
         //END
            if (dtposts.Rows.Count > 0)
            {
                int count = 0;
                strHTML += "<table style=\"display: inline-table;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"700\">";
                strHTML += "<tbody>";
                foreach (DataRow dr in dtposts.Rows)
                {
                    string postTitle = dr["post_title"].ToString();
                    string strTitle = utils.formatStringForURLSelect(postTitle);
                    string posturl = "http://" + ReadConfig.getDomainUrl() + "/features/" + utils.GeneratePostTitleForURL(strTitle) + ".aspx";
                    string postdesc = utils.formatStringForDBSelect(dr["post_description"].ToString());
                    string imageSrc = "";
                    string imagealt = "";
                    if (postdesc.IndexOf("<img ") != -1)
                    {
                        string a = postdesc.Replace("<img ", "^").Split('^')[1];
                        string b = a.Replace("src=", "^").Split('^')[1];
                        if (b.StartsWith("\""))
                        {
                            imageSrc = b.Split('"')[1].TrimStart('"');
                        }
                        else
                        {
                            imageSrc = b.Split('\'')[1].TrimStart('\'');
                        }
                        int lastindexslash = imageSrc.LastIndexOf('/');
                        string newsrc = imageSrc.Substring(lastindexslash+1, imageSrc.Length-1- lastindexslash);
                        int lastindexdot = newsrc.LastIndexOf('.');
                        imagealt = newsrc.Substring(0, lastindexdot).Replace("-"," ").Replace("_", " ");
                    }
                    string plaindesc = Regex.Replace(postdesc, "<(.|\n)*?>", "").Replace("\r", "").Replace("\n", "").Trim();
                    if(plaindesc.Length > 300)
                    {
                        plaindesc = plaindesc.Substring(0, 297) + "...";
                    }
                    int index = dtposts.Rows.IndexOf(dr);
                    count++;
                    if (index % 3 == 0)
                    {
                        strHTML += "<tr>";
                    }
                    strHTML += "<td style=\"padding:5px 5px 15px 5px;vertical-align: top;\">";
                    strHTML += "<a href=\"" + posturl + "\">";
                    strHTML += "<div style=\"width:212px;height:109px;\">";
                    strHTML += "<img name=\"homepage1_s1\" src=\"" + imageSrc + "\" class=\"blogimg\" alt=\"" + imagealt + "\" border=\"0\" height=\"109\" width=\"211\">";
                    strHTML += "</div>";
                    strHTML += "<p class=\"style2\" style=\"margin:0px;margin-top:10px;\">";
                    strHTML += "<span style=\"color: rgb(128, 128, 128);\">";
                    strHTML += "<strong>";
                    strHTML += "<span style=\"font-size: larger;\">" + postTitle + "</span>";
                    strHTML += "</strong>";
                    strHTML += "</span>";
                    strHTML += "</p>";
                    strHTML += "<p class=\"style2\" style=\"margin:0px\">&nbsp;</p>";
                    strHTML += "<p class=\"style2\" style=\"margin:0px\">";
                    strHTML += plaindesc;
                    strHTML += "</p>";
                    strHTML += "</a>";
                    strHTML += "</td>";
                    if (index == dtposts.Rows.Count -1)
                    {
                        if(count == 3)
                        {
                            strHTML += "</tr>";
                        }
                        else if (count == 2) {
                            strHTML += "<td></td></tr>";
                        }
                        else if (count == 1)
                        {
                            strHTML += "<td></td><td></td></tr>";
                        }
                    }
                    else
                    {
                        if (count == 3)
                        {
                            strHTML += "</tr>";
                            count = 0;
                        }
                    }
                }
                strHTML += "</tbody>";
                strHTML += "</table>";
            }
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}