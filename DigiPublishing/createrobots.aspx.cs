using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Net;
using System.Text;

public partial class createrobots : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /* Code to make this page non-accessible by Super Admin*/
        if (Convert.ToString(Request.Url.Host) == Convert.ToString(ConfigurationManager.AppSettings["saMainURL"]))         //Commented to run application on local
            Response.Redirect(ConfigurationManager.AppSettings["ErrorPageUrl"].ToString());                                                                        //Commented to run application on local
        /*******************************************************************************/
        // For Non-existing or old agnencies
        //if (!utils.IsAgencyExist(ReadConfig.getDomainUrl()))                                                                   //Commented to run application on local
        //    Response.Redirect(Convert.ToString(ConfigurationManager.AppSettings["DIGIPTVURL"]));     
        if (!IsPostBack)
           LoadRobotsTxt();
    }
    
    protected void LoadRobotsTxt()
    {
        string strUrl = Request.PhysicalApplicationPath+ "config\\" + ReadConfig.getDomainUrl() + "\\robots.txt";
        if (File.Exists(strUrl))
        {
            FileStream textFile = new FileStream(strUrl, FileMode.Open, FileAccess.ReadWrite);
            StreamReader srFile = new StreamReader(textFile);
            try
            {
                string line = null;
                int i = 0;
                StringBuilder strcontents = new StringBuilder();
                while ((line = srFile.ReadLine()) != null)
                {
                    line = line.Replace("&", "&amp;");
                    if (i == 0)
                    {
                        strcontents.Append(line);
                    }
                    else
                    {
                        strcontents.Append("<br/>" + line);
                    }
                    i++;
                }
                strcontents.Append("\n" + "\n");
                textFile.Close();
                Response.Write(strcontents.ToString());
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }
}

