﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="OndemandHome.aspx.cs" Inherits="OndemandHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="content-full"> 
        <asp:Label ID="lblMessage" CssClass="error" runat="server"></asp:Label>
        <table>
            <tr>
                <td>
                    <div id="divAdvBlog" runat="server" style="display: none">
                        <h3 class="h-blog">Features</h3>
                        <strong>
                            <asp:Label ID="lblBlogtitle" runat="server"></asp:Label>
                        </strong>&nbsp;&nbsp;
                        <asp:DataList ID="dlstBlog" runat="server" Width="100%">
                            <ItemTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblBlogPosts" runat="server" Font-Bold="true" Font-Size="Medium" Text='<%# utils.formatStringForURLSelect(Convert.ToString(Eval("post_title")))%>'></asp:Label>&nbsp;&nbsp;
                                   
                                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("post_id")%>'></asp:Label>
                                            <br />
                                            <div id="divPost" runat="server" style="display: inline;">
                                                <div class="st_blogpost_container">
                                                    <%# utils.formatStringForURLSelect(Convert.ToString(Eval("post_description")))%>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr align="left" runat="server">
                                        <td colspan="2" runat="server">
                                            <%# utils.formatStringForURLSelect(Convert.ToString(Eval("adString")))%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
             
                        <asp:Panel ID="pnlPagingMainBlog" runat="server" Visible="false">
                            <div style="width:100%;text-align:center;">
                                <table style="display:inline;">
                                    <tr>
                                        <td style="text-align:center">
                                            <asp:LinkButton ID="lbtnPreviousBlog" Font-Bold="true" Font-Size="Medium" runat="server" CausesValidation="false" OnClick="lbtnPreviousBlog_Click">...</asp:LinkButton>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:center;vertical-align:middle;">
                                            <asp:DataList ID="dlPagingBlog" runat="server" RepeatDirection="Horizontal" OnItemCommand="dlPagingBlog_ItemCommand"
                                                OnItemDataBound="dlPagingBlog_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnPaging" Font-Bold="true" Font-Size="Medium" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="Paging" Text='<%# Eval("PageText") %>'>
                                                    </asp:LinkButton>&nbsp;
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                        <td style="text-align:left">
                                            &nbsp;&nbsp;<asp:LinkButton ID="lbtnNextBlog" Font-Bold="true" Font-Size="Medium" runat="server" CausesValidation="false" OnClick="lbtnNextBlog_Click">...</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align:center;vertical-align:middle;height:30px">
                                            <asp:Label ID="lblPageInfoBlog" Font-Bold="true" Font-Size="Medium" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divAdvRss" runat="server" style="display: none">
                        <h3 class="h-rss">Feeds</h3>
                        <div id="content-full">
                            <img id="img_loading" src="http://<%=ReadConfig.getDomainUrl() %>/Images_Common/loading.gif" style="display:none" alt="loading" />
                            <h4>
                                <asp:Label ID="lblRss" runat="server"></asp:Label></h4>
                                <asp:Label ID="lblmsgfeed" CssClass="error" runat="server"></asp:Label>
                                <div id="dvRSSFeed"></div>
                            <br />
                            <asp:Panel ID="Panel2" runat="server">
                                <div id="dlPaging"></div>
                                <div style="height: 30px; text-align: center; margin: 10px 0 0 0;">
                                    <asp:Label ID="lblPageInfo" Font-Bold="true" Font-Size="11pt" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divAdvBookmark" runat="server" style="display: none">
                        <h3 class="h-bookmarks">Links</h3>
                        <asp:Label ID="lblNoSavedbookmark" runat="server" Text="No link saved." ForeColor="red"></asp:Label>
                        <asp:DataGrid ID="dgrdBookmark" ShowHeader="false" AllowSorting="true" GridLines="None" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundColumn DataField="Id" Visible="False"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-Width="400px">
                                    <ItemTemplate>
                                        <a target="_blank" href="<%#utils.formatStringForDBSelect(Convert.ToString(Eval("BK_URL")))%>"<%#utils.formatStringForDBSelect(Convert.ToString(Eval("rel")))%>><b><%# utils.formatStringForDBSelect(Convert.ToString(Eval("BK_Title"))) %></b></a>                    
                                        <br>
                                        <%#utils.formatStringForDBSelect(Convert.ToString(Eval("BK_Desc")))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:Panel ID="pnlPagingMainLinks" runat="server">
                            <div style="text-align:center;width:100%;">
                                <table style="display:inline;">
                                    <tr>
                                        <td style="text-align:right;">
                                            <asp:LinkButton ID="lbtnPreviousLinks" Font-Bold="true" Font-Size="Medium" runat="server" CausesValidation="false" OnClick="lbtnPreviousLinks_Click">...</asp:LinkButton>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:right;vertical-align:middle;">
                                            <asp:DataList ID="dlPagingLinks" runat="server" RepeatDirection="Horizontal" OnItemCommand="dlPagingLinks_ItemCommand" OnItemDataBound="dlPagingLinks_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnPaging" Font-Bold="true" Font-Size="Medium" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="Paging" Text='<%# Eval("PageText") %>'></asp:LinkButton>&nbsp;
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                        <td style="text-align:left;">
                                            &nbsp;&nbsp;
                                            <asp:LinkButton ID="lbtnNextLinks" Font-Bold="true" Font-Size="Medium" runat="server" CausesValidation="false" OnClick="lbtnNextLinks_Click">...</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:30px;text-align:left;vertical-align:middle;">
                                            <asp:Label ID="lblPageInfoLinks" Font-Bold="true" Font-Size="Medium" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <br />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        var vars = [];
        $(document).ready(function () {
            if (window.location.href.toLowerCase().indexOf('feeditems') > -1) {
                vars = window.location.href.slice(window.location.href.toLowerCase().indexOf('feeditems') + ('feeditems').length + 1).split('/');
            }
            else if (window.location.href.toLowerCase().indexOf('updates') > -1) {
                vars = window.location.href.slice(window.location.href.toLowerCase().indexOf('updates') + ('updates').length + 1).split('/');
            }
            PopulateRSS('0');
        })

        function PopulateRSS(parRequestedPage) {
            var parRSSname = '';
            if (vars.length > 0) {
                parRSSname = vars[0];
                if (parRSSname.indexOf("#.") > -1)
                    parRSSname = parRSSname.slice(0, parRSSname.indexOf("#."));
            }

            var parRSSitem = '';
            if (vars.length > 1) {
                parRSSitem = vars[1];
                if (parRSSitem.indexOf("#.") > -1)
                    parRSSitem = parRSSitem.slice(0, parRSSitem.indexOf("#."));
            }


            parRSSname = escape(parRSSname).replace(/%25/g, '%');
            parRSSitem = escape(parRSSitem).replace(/%25/g, '%');

            setVarBlank();
            $('#img_loading').show();
            $.ajax({
                type: "Post",
                url: WebServiceDomain + "Services/RSSFeed.asmx/GetRSS",
                data: "{'rssname':'" + parRSSname + "','rssitem':'" + parRSSitem + "','RequestedPage':'" + parRequestedPage + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    try {
                        var obj = jQuery.parseJSON(data.d);
                        if (obj.Result.Message == "success") {
                            var dvRSSFeedContent = "";

                            if (typeof (obj.Result.DocumentElement.RSSitem.length) != "undefined") {
                                for (var item in obj.Result.DocumentElement.RSSitem) {
                                    dvRSSFeedContent += "<p><strong><a id=\"lnkURL\" target=\"_blank\" href='" + obj.Result.DocumentElement.RSSitem[item].link + "'>" + obj.Result.DocumentElement.RSSitem[item].title + "</a></strong></p>"
                                        + "<p>" + obj.Result.DocumentElement.RSSitem[item].description + "</p>";                            
                                }
                            }
                            else
                                dvRSSFeedContent += "<p><strong><a id=\"lnkURL\" target=\"_blank\" href='" + obj.Result.DocumentElement.RSSitem.link + "'>" + obj.Result.DocumentElement.RSSitem.title + "</a></strong></p>"
                                        + "<p>" + obj.Result.DocumentElement.RSSitem.description + "</p>";

                            $('#dvRSSFeed').html(dvRSSFeedContent);
                        }


                        if (typeof (obj.Result.RssTitle) != "undefined")
                            $('#<%= lblRss.ClientID %>').text(obj.Result.RssTitle);
                        if (typeof (obj.Result.Message) != "undefined" && obj.Result.Message != "success")
                            $('#<%= lblmsgfeed.ClientID %>').text(obj.Result.Message);
                        if (typeof (obj.Result.Paging) != "undefined")
                            $('#dlPaging').html(obj.Result.Paging);
                        if (typeof (obj.Result.CurrentPageCount) != "undefined")
                            $('#<%= lblPageInfo.ClientID %>').text(obj.Result.CurrentPageCount);

                    } catch (err) {
                        $('#<%= lblmsgfeed.ClientID %>').text(err.message);
                    }
                },
                error: function (xhr, thrownError) {
                    $('#<%= lblmsgfeed.ClientID %>').text("Server processing error!");
                },
                complete: function () {
                    $('#img_loading').hide();
                }
            });
        }

        function setVarBlank() {
                $('#<%= lblRss.ClientID %>').text('');
            $('#<%= lblmsgfeed.ClientID %>').text('');
            $('#dvRSSFeed').html('');
            $('#dlPaging').html('');
            $('#<%= lblPageInfo.ClientID %>').text('');
        }

        function PagingClick(indexpage) {
            PopulateRSS(indexpage);
        }
    </script>
</asp:Content>

