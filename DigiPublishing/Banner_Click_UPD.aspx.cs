using System;
using System.Data;

public partial class Banner_Click_UPD : System.Web.UI.Page
{
    public string Referer_URL = string.Empty;
    public string Campaign_Source = string.Empty;
    public string REMOTE_ADDR = string.Empty;
    public string DomainForGA = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["REMOTE_ADDR"])))
        {
            REMOTE_ADDR = Convert.ToString(Request.QueryString["REMOTE_ADDR"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["DomainForGA"])))
        {
            DomainForGA = Convert.ToString(Request.QueryString["DomainForGA"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["Referer_URL"])))
        {
            Referer_URL = Convert.ToString(Request.QueryString["Referer_URL"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["Campaign_Source"])))
        {
            Campaign_Source = Convert.ToString(Request.QueryString["Campaign_Source"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["banID"])))
        {
            if (Convert.ToInt32(Request.QueryString["banID"]) != 0)
            {
                Updatewidgetclick(Convert.ToInt32(Request.QueryString["banID"]));
            }
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["banIDBlocked"])))
        {
            if (Convert.ToInt32(Request.QueryString["banIDBlocked"]) != 0)
            {
                UpdatewidgetclickBlocked(Convert.ToInt32(Request.QueryString["banIDBlocked"]));
            }
        }
    }
    public void Updatewidgetclick(int banid)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", REMOTE_ADDR);
            cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
            cmd.Parameters.Add("@DIGIP_Domain", DomainForGA);
            cmd.Parameters.Add("@Referer_URL", Referer_URL);
            cmd.Parameters.Add("@Type", "banner");
            cmd.Parameters.Add("@block_status", Convert.ToString(0));
            cmd.Parameters.Add("@Campaign_Source", Campaign_Source);
            dpreport.ExecuteNonQuery(cmd);

            if(ConfigLoader.GetIsUseNewClickDB() == 1)
            {
                DBHelper dpreport2 = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
                DBCommand cmd2 = new DBCommand();
                cmd2.CommandText = "SP_Brand_Insert_ClickInfo";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.Add("@IPAdress", REMOTE_ADDR);
                cmd2.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd2.Parameters.Add("@DIGIP_Domain", DomainForGA);
                cmd2.Parameters.Add("@Type", "banner");
                dpreport2.ExecuteNonQuery(cmd2);
            }
        }
    }
    public void UpdatewidgetclickBlocked(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1 && widID != 564 && widID != 565)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", REMOTE_ADDR);
            cmd.Parameters.Add("@WidgetID", Convert.ToString(widID));
            cmd.Parameters.Add("@DIGIP_Domain", DomainForGA);
            cmd.Parameters.Add("@Referer_URL", Referer_URL);
            cmd.Parameters.Add("@Type", Convert.ToString("banner"));
            cmd.Parameters.Add("@block_status", Convert.ToString(1));
            cmd.Parameters.Add("@Campaign_Source", Campaign_Source);
            dpreport.ExecuteNonQuery(cmd);
        }
    }
}
