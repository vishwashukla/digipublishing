using System;
using System.Data;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;

public partial class RSSFeed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        get();
    }

    protected void get()
    {
        DataTable dt = new DataTable();
        XmlTextWriter TextWriter = new XmlTextWriter(Response.OutputStream, Encoding.ASCII);
        Response.ContentType = "text/xml";

        DBHelper dp1 = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DBCommand strqry = new DBCommand();
        strqry.CommandText = "SP_Brand_Select_Features_For_RSS_Feed";
        strqry.CommandType = CommandType.StoredProcedure;
        dt = dp1.FillDataTable(strqry);

        string strQuery = "SELECT description FROM Brand_AppSettings WHERE IsDelete='False' AND name='description' AND Type=1";
        DataTable dtData = dp1.FillDataTable(strQuery);
        string metadata = "";
        if (dtData.Rows.Count > 0)
        {
            metadata = dtData.Rows[0]["description"].ToString();
        }

        TextWriter.Formatting = Formatting.Indented;
        TextWriter.WriteStartDocument();
        TextWriter.WriteStartElement("rss");
        TextWriter.WriteAttributeString("version", "2.0");
        TextWriter.WriteAttributeString("xmlns:media", "http://search.yahoo.com/mrss/");
        string DomainURL = ReadConfig.getDomainUrl();
        string logo = ReadConfig.GetLogo().Replace('~', ' ').Trim();

        TextWriter.WriteStartElement("channel");
        if (dt.Rows.Count > 0)
            TextWriter.WriteElementString("title", dt.Rows[0]["blog_title"].ToString());
        TextWriter.WriteElementString("link", "http://" + DomainURL);
        TextWriter.WriteElementString("description", metadata);

        TextWriter.WriteStartElement("image");
        if (dt.Rows.Count > 0)
            TextWriter.WriteElementString("title", dt.Rows[0]["blog_title"].ToString());
        TextWriter.WriteElementString("link", "http://" + DomainURL);
        TextWriter.WriteElementString("url", "http://" + DomainURL + "/Config/" + DomainURL + "/" + logo);
        TextWriter.WriteElementString("height", "100");
        TextWriter.WriteElementString("width", "144");
        TextWriter.WriteEndElement();

        TextWriter.WriteStartElement("atom10:link");
        TextWriter.WriteAttributeString("xmlns:atom10", "http://www.w3.org/2005/Atom");
        TextWriter.WriteAttributeString("rel", "self");
        TextWriter.WriteAttributeString("type", "application/rss+xml");
        TextWriter.WriteAttributeString("href", "http://" + ReadConfig.getDomainUrl() + "/RSSFeed.aspx");
        TextWriter.WriteEndElement();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string str_postsdesc = "";
                if (!string.IsNullOrWhiteSpace(dt.Rows[i]["post_description"].ToString()))
                {
                    str_postsdesc = Regex.Replace(dt.Rows[i]["post_description"].ToString(), @"<(.|\n)*?>", string.Empty).Replace("&nbsp;", "");
                    if (str_postsdesc.Length > 200)
                    {
                        str_postsdesc = str_postsdesc.Substring(0, 197) + "...";
                    }
                }

                string strposttitle = utils.GeneratePostTitleForURL(utils.formatStringForURLSelect(Convert.ToString(dt.Rows[i]["post_title"]).Trim().Replace(' ', '-')));

                TextWriter.WriteStartElement("item");
                TextWriter.WriteElementString("title", utils.formatStringForURLSelect(dt.Rows[i]["post_title"].ToString()));
                TextWriter.WriteStartElement("guid");
                TextWriter.WriteAttributeString("isPermaLink", "true");
                TextWriter.WriteString("http://" + DomainURL + "/features/" + strposttitle + ".aspx");
                TextWriter.WriteEndElement();
                TextWriter.WriteStartElement("description");
                TextWriter.WriteCData(str_postsdesc);
                TextWriter.WriteEndElement();
                TextWriter.WriteElementString("link", "http://" + DomainURL + "/features/" + strposttitle + ".aspx");
                TextWriter.WriteEndElement();
            }
        }
        TextWriter.WriteEndElement();
        TextWriter.WriteEndElement();
        TextWriter.WriteEndDocument();
        TextWriter.Flush();
        TextWriter.Close();
    }
}
