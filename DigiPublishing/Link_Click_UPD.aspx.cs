using System;
using System.Data;

public partial class Link_Click_UPD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["linkID"])))
        {
            if (Convert.ToInt32(Request.QueryString["linkID"]) != 0 && Convert.ToInt32(Request.QueryString["domainID"]) != 0)
            {
                UpdateLinkclick(Convert.ToInt32(Request.QueryString["linkID"]), Convert.ToInt32(Request.QueryString["domainID"]), Convert.ToInt32(Request.QueryString["blockstatus"]));
            }
        }
    }
    public void UpdateLinkclick(int linkid, int did, int blockstatus)
    { 
        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Insert_LinkClickInfo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@IPAdress", utils.GetUserIP());
        cmd.Parameters.Add("@LinkID", Convert.ToString(linkid));
        cmd.Parameters.Add("@DIGIP_Domain", did);
        cmd.Parameters.Add("@block_status", blockstatus);
        dpreport.ExecuteNonQuery(cmd);
    }
}
