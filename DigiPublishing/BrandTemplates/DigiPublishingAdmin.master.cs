﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;

public partial class BrandTemplates_DigiPublishingAdmin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {   
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Administrative Task";
        if (Session["userid"] == null || Convert.ToString(Session["userid"]) == "0")
        {
            Response.Redirect("~/ondemandhome.aspx");
        }
        if (!IsPostBack)
        {
            favicon.Href = "http://" + ReadConfig.getDomainUrl() + "/App_Themes/" + ReadConfig.GetTheme() + "/images/favicon.ico";
            showTasks();
        }
    }
    
    //Tasks allowed by Super Admin
    public void showTasks()
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string strQuery = "select features from brand_settings";
        DataTable dtAdminFeatures = dp.FillDataTable(strQuery);

        string[] arrAdminFeatures = { };
        if (dtAdminFeatures != null && dtAdminFeatures.Rows.Count > 0)
            arrAdminFeatures = Convert.ToString(dtAdminFeatures.Rows[0][0]).Split(',');
        //string[] arrAdminFeatures = ReadConfig.GetFeatures().Split(',');

        //Blog
        if (arrAdminFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Blog)))))
            this.trAdminBlog.Visible = true;
        //RSS
        if (arrAdminFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.RSS)))))
            this.trAdminRss.Visible = true;
        //BookMark
        if (arrAdminFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Bookmark)))))
            this.trAdminBookMarks.Visible = true;
        //calendar
        if (arrAdminFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Bookmark)))))
            this.trAdminCalendars.Visible = true;
    }
}
