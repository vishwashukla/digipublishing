﻿using System;
using System.Data;

public partial class BrandTemplates_DigiPublishingSuperAdmin : System.Web.UI.MasterPage
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string strAppPath;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrWhiteSpace(Convert.ToString(Session["userid"])) || Convert.ToString(Session["userid"]).Trim() == "0")
        {
            Response.Redirect("~/" + ConfigLoader.GetSALoginPage());
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Session["username"])) && Convert.ToString(Session["username"]).Trim() == "pandeypriya2003")
        {
            Response.Redirect("~/" + ConfigLoader.GetSALoginPage());
        }
        SetAccessbility();
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Remove("userid");
        Response.Redirect("~/" + ConfigLoader.GetSALoginPage());
    }

    private void SetAccessbility()
    {
        DBCommand strQuery = new DBCommand();
        strQuery.CommandText = "SP_Brand_Select_AP_ID";
        strQuery.CommandType = CommandType.StoredProcedure;
        strQuery.AddParameter("@UID", Session["userid"]);
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                switch (dr["AP_ID"].ToString())
                {
                    case "1":
                        hlnkCreateAgency.Visible = true;
                        break;
                    case "2":
                        hlnkViewStastics.Visible = true;
                        break;
                    case "3":
                        hlnkSitesExportToHTML.Visible = true;
                        break;
                    case "4":
                        hlnkViewSummaryStastics.Visible = true;
                        break;
                    case "5":
                        hlnkBulkManageAgencies.Visible = true;
                        break;
                    case "6":
                        hlnkManageSuperAdminUsers.Visible = true;
                        break;
                    case "7":
                        hlnkManageAllSiteUsers.Visible = true;
                        break;
                    case "8":
                        hlnkManageWidgets.Visible = true;
                        break;
                    case "9":
                        hlnkVideos.Visible = true;
                        break;
                    case "10":
                        hlnkManageVideoAdNetworks.Visible = true;
                        break;
                    case "11":
                        hlnkManageTemplates.Visible = true;
                        break;
                    case "12":
                        hlnkManageRegions.Visible = true;
                        break;
                    case "13":
                        hlnkManageCountries.Visible = true;
                        break;
                    case "14":
                        hlnkSiteGenre.Visible = true;
                        break;
                    case "15":
                        hlnkManageLanguages.Visible = true;
                        break;
                    case "16":
                        hlnkMaintenance.Visible = true;
                        break;
                    case "17":
                        hlnkClickReport.Visible = true;
                        break;
                    case "18":
                        hlnkImpressionReport.Visible = true;
                        break;
                    case "19":
                        hlnkAdnetworkReport.Visible = true;
                        break;
                    case "20":
                        hlnkCacheRemove.Visible = true;
                        break;
                    case "21":
                        hlnkManagAdsTxt.Visible = true;
                        break;
                    case "22":
                        hlnkOutboundLinkReport.Visible = true;
                        break;
                    case "23":
                        hlnkManagPrivacy.Visible = true;
                        break;
                }
            }
        }
    }
}
