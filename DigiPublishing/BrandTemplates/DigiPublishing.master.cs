﻿using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Linq;
using ss2;
using System.Web.UI.WebControls;

public partial class BrandTemplates_DigiPublishing : System.Web.UI.MasterPage
{
    
    public string strAppPath;
    public string strDomainConfigPath;
    public string BannerIDs = string.Empty;
    string strDomainName;
    public string domainhost;
    public string ssResponsePID;
    public int AddTrackingCode;
    public string SiteId;
    protected void Page_Load(object sender, EventArgs e)
    {
        strDomainName = ReadConfig.getDomainUrl();

        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        if (!IsPostBack)
        {
            this.Session["CaptchaImageText"] = utils.GenerateRandomCode();
            msgLoginRegister.Text = "";
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.UrlReferrer)))
            {
                if (Request.UrlReferrer.ToString().ToLower().Contains("www.google.co") && Request.Url.ToString().ToLower().Contains("?id"))
                    Server.Transfer("~/Errorpage.aspx");
            }

            InitialSetting(dpSA, strDomainName);
            GetMetaTag(this.Page, dp);
            ShowComponents(dp);
            bindRssGrid(strDomainName, dp);
            getBookmarkSummary(strDomainName, dp);
            getMostPopular(strDomainName, dp);

            //Create Report DB statistics tables if not created via scheduler
            if (Convert.ToString(Cache["chkRecordADData"]) == "1")
            {
                string strCheckReportTable = "CheckReportTable_" + DateTime.Now.ToString("yyyyMMdd");
                if (string.IsNullOrWhiteSpace(Convert.ToString(Cache[strCheckReportTable])))
                {
                    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                    DBCommand cmd = new DBCommand();
                    cmd.CommandText = "SP_CREATE_NEW_TABLES_FOR_Today";
                    cmd.CommandType = CommandType.StoredProcedure;
                    dpreport.ExecuteNonQuery(cmd);
                    Cache.Insert(strCheckReportTable, "1", null, DateTime.Now.AddHours(25), System.Web.Caching.Cache.NoSlidingExpiration);
                }
            }
        }
        strAppPath = Convert.ToString(Request.ApplicationPath);
        if (strAppPath != "/")
        {
            strAppPath = Convert.ToString(Request.ApplicationPath) + "/";
        }

        domainhost = ReadConfig.getDomainUrl();
        if (Request.Url.Host.ToLower() == "localhost")
        {
            domainhost = Request.Url.Host;
            domainhost += ":" + Request.Url.Port;
        }
        strDomainConfigPath = "http://" + domainhost + "/Config/" + strDomainName + "/";

        #region shieldquare

        //string ss_domains = dpSA.ExecuteScalar("EXEC BRAND_Is_Apply_ShieldSquare @WidgetName='" + ConfigLoader.GetSS_WidgetName().ToLower().Trim() + "'").ToString();
        DataTable dtss_domains = dp.FillDataTable("EXEC BRAND_Is_Apply_ShieldSquare_New @WidgetName='" + ConfigLoader.GetSS_WidgetName().ToLower().Trim() + "'");
        //if (ss_domains.Contains("," + domainhost.ToLower().Trim() + ","))
        if (dtss_domains.Rows.Count > 0)
        {
            string shieldsquare_username = ""; // Enter the UserID of the user 
            int shieldsquare_calltype = 1;

            ShieldSquare s = new ShieldSquare();
            ShieldSquareResponce ShieldSquareResponce = s.ShieldSquare_ValidateRequest(shieldsquare_username, shieldsquare_calltype);

            if (ShieldSquareResponce.responsecode == -1)
            {
                //"Exception has ocurred. Check the ssResponse.reason for the cause of the exception "; 
                shieldsquare_response.Value = "Please reach out to ShieldSquare support team for assistance" + ShieldSquareResponce.reason;
            }
            else
            {
                shieldsquare_response.Value = "Allow User Request";
            }
            //copy the value of the tid to a hidden field shieldsquare_response_tid in the asp page.
            //This field can be used to pass the value to the IFRAME

            ssResponsePID = ShieldSquareResponce.pid;
        }
        #endregion

        GetNonePostionWidgets();
        GetWidgets();  
    }

    private void InitialSetting(DBHelper dpSA, string DomnainName)
    {
        DataTable dtserverMaintainance = new DataTable();
        if (Cache["dtserverMaintainance"] != null)
        {
            dtserverMaintainance = (DataTable)Cache["dtserverMaintainance"];
        }
        else
        {
            string strsqlMaintainance = "select isnull(Server_Maintainance,0) as Server_Maintainance,isnull(Start_Stop,0) as Start_Stop,VideoImpression,OutboundLink,VideoAdStart,VideoAdFinish,BannerClick,VideoAdClick"
                + " from brand_Record_Data";
            dtserverMaintainance = dpSA.FillDataTable(strsqlMaintainance);
            Cache["dtserverMaintainance"] = dtserverMaintainance;
        }
        if (dtserverMaintainance.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtserverMaintainance.Rows[0]["Server_Maintainance"]) == 1)
            {
                Response.Write("Site is currently under maintainance.");
                Response.End();
            }
        }
        string strsqlHtmloraspx = "select id,isnull(HTML_OR_ASPX,0) as HTML_OR_ASPX,isnull(IsAddTrackingCode,0) as IsAddTrackingCode from brand_domainname where domainname='" + DomnainName + "'";
        DataTable dtHtmloraspx = dpSA.FillDataTable(strsqlHtmloraspx);
        if (dtHtmloraspx.Rows.Count > 0)
        {
            if (Cache["SiteId_"+ DomnainName] == null)
            {
                Cache["SiteId_" + DomnainName] = Convert.ToString(dtHtmloraspx.Rows[0]["id"]);
            }
            SiteId = Convert.ToString(dtHtmloraspx.Rows[0]["id"]);
            if (Convert.ToInt32(dtHtmloraspx.Rows[0]["HTML_OR_ASPX"]) == 1)
            {
                Response.Redirect("~/html/default.html");
            }
            AddTrackingCode = Convert.ToInt32(dtHtmloraspx.Rows[0]["IsAddTrackingCode"]);
        }

        if (Cache["chkRecordADData"] == null)
        {
            Cache["chkRecordADData"] = Convert.ToByte(dtserverMaintainance.Rows[0]["Start_Stop"]);
        }
        if (Cache["VideoImpression"] == null)
        {
            Cache["VideoImpression"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoImpression"]);
        }
        if (Cache["OutboundLink"] == null)
        {
            Cache["OutboundLink"] = Convert.ToByte(dtserverMaintainance.Rows[0]["OutboundLink"]);
        }
        if (Cache["VideoAdStart"] == null)
        {
            Cache["VideoAdStart"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoAdStart"]);
        }
        if (Cache["VideoAdFinish"] == null)
        {
            Cache["VideoAdFinish"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoAdFinish"]);
        }
        if (Cache["BannerClick"] == null)
        {
            Cache["BannerClick"] = Convert.ToByte(dtserverMaintainance.Rows[0]["BannerClick"]);
        }
        if (Cache["VideoAdClick"] == null)
        {
            Cache["VideoAdClick"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoAdClick"]);
        }
        favicon.Href = "http://" + DomnainName + "/App_Themes/" + ReadConfig.GetTheme() + "/images/favicon.ico";

        if (Convert.ToString(Session["Referer_URL"]) == "")
        {
            if (Convert.ToString(Request.UrlReferrer) != "")
            {
                if (Convert.ToString(Request.UrlReferrer.Host) != "mantisaerg94tb.digipublishing.net")
                {
                    Session["Referer_URL"] = Convert.ToString(Request.UrlReferrer.AbsoluteUri);
                }
            }
        }

        if (Convert.ToString(Session["Campaign_Source"]) == "")
        {
            if (Request.QueryString["utm_campaign"] != null)
            {
                Session["Campaign_Source"] = Convert.ToString(Request.QueryString["utm_campaign"]);
            }
            else if (Request.QueryString["utm_source"] != null)
            {
                Session["Campaign_Source"] = Convert.ToString(Request.QueryString["utm_source"]);
            }
        }

        StringBuilder SBtrackinstr = new StringBuilder();
        SBtrackinstr.AppendLine("<script>");
        SBtrackinstr.AppendLine("var DomainForGA = '" + Convert.ToString(System.Web.HttpContext.Current.Request.Url.Host) + "';");
        var WebServiceDomain = "var WebServiceDomain = 'http://" + Convert.ToString(System.Web.HttpContext.Current.Request.Url.Host) + "/';";
        if(Convert.ToString(System.Web.HttpContext.Current.Request.Url.Host) == "localhost")
        {
            WebServiceDomain = "var WebServiceDomain = 'http://localhost:2740/';";
        }
        SBtrackinstr.AppendLine(WebServiceDomain);
        SBtrackinstr.AppendLine("var strAllowAdnetworkData = '" + Convert.ToString(HttpContext.Current.Cache["chkRecordADData"]) + "';");
        SBtrackinstr.AppendLine("var REMOTE_ADDR = '" + Convert.ToString(Request.ServerVariables["REMOTE_ADDR"]) + "';");
        SBtrackinstr.AppendLine("var Referer_URL = '" + Convert.ToString(Session["Referer_URL"]) + "';");
        if (AddTrackingCode == 1)
        {
            SBtrackinstr.AppendLine("var Campaign_Source = '" + Convert.ToString(Session["Campaign_Source"]) + "';");
            SBtrackinstr.AppendLine("var UserAgent = '" + Convert.ToString(Request.UserAgent) + "';");
            SBtrackinstr.AppendLine("var ISSendGAVideoImpression = '" + Convert.ToString(HttpContext.Current.Cache["VideoImpression"])+"';");
            SBtrackinstr.AppendLine("var ISSendGAOutboundLink = '" + Convert.ToString(HttpContext.Current.Cache["OutboundLink"])+"';");
            SBtrackinstr.AppendLine("var ISSendGAVideoAdStart = '" + Convert.ToString(HttpContext.Current.Cache["VideoAdStart"])+"';");
            SBtrackinstr.AppendLine("var ISSendGAVideoAdFinish = '" + Convert.ToString(HttpContext.Current.Cache["VideoAdFinish"])+"';");
            SBtrackinstr.AppendLine("var ISSendGABannerClick = '" + Convert.ToString(HttpContext.Current.Cache["BannerClick"])+"';");
            SBtrackinstr.AppendLine("var ISSendGAVideoAdClick = '" + Convert.ToString(HttpContext.Current.Cache["VideoAdClick"])+"';");
            SBtrackinstr.AppendLine("var GAID = '"+ConfigLoader.GetGA_ID() + "';");
            SBtrackinstr.AppendLine("var UserHostAddress = '" + Request.UserHostAddress + "';");
            SBtrackinstr.AppendLine("var Time = '" + System.DateTime.Now.ToString("M /d/yyyy h:m:s.ffffff tt") + "';");

            outbounglinktrackdiv.Visible = true;
            StringBuilder SBoutbounglinktrackdivstr = new StringBuilder();
            SBoutbounglinktrackdivstr.AppendLine("<script>");
            SBoutbounglinktrackdivstr.AppendLine("$(document).ready(function () {");
            SBoutbounglinktrackdivstr.AppendLine("function _gaLt(event) {");
            SBoutbounglinktrackdivstr.AppendLine("var el = event.srcElement || event.target;");
            SBoutbounglinktrackdivstr.AppendLine("while (el && (typeof el.tagName == 'undefined' || el.tagName.toLowerCase() != 'a' || !el.href))");
            SBoutbounglinktrackdivstr.AppendLine("el = el.parentNode;");
            SBoutbounglinktrackdivstr.AppendLine("if (el && el.href) {");
            SBoutbounglinktrackdivstr.AppendLine("if (el.href.indexOf(location.host) == -1) {");
            SBoutbounglinktrackdivstr.AppendLine("try {");
            SBoutbounglinktrackdivstr.AppendLine("if (ISSendGAOutboundLink == \"1\") {");
            SBoutbounglinktrackdivstr.AppendLine("if (typeof _gaq != \"undefined\") {");
            SBoutbounglinktrackdivstr.AppendLine("_gaq.push(['_trackEvent', 'OutboundLink', el.href]);");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("else if (typeof ga != \"undefined\") {");
            SBoutbounglinktrackdivstr.AppendLine("ga('send', 'event', 'OutboundLink', el.href);");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("} catch (err) { }");
            SBoutbounglinktrackdivstr.AppendLine("if (!el.target || el.target.match(/^_(self|parent|top)$/i)) {");
            SBoutbounglinktrackdivstr.AppendLine("setTimeout(function () {");
            SBoutbounglinktrackdivstr.AppendLine("document.location.href = el.href;");
            SBoutbounglinktrackdivstr.AppendLine("}.bind(el), 500);");
            SBoutbounglinktrackdivstr.AppendLine("event.preventDefault ? event.preventDefault() : event.returnValue = !1;");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("}");
            SBoutbounglinktrackdivstr.AppendLine("var w = window;");
            SBoutbounglinktrackdivstr.AppendLine("w.addEventListener ? w.addEventListener(\"load\", function () { document.body.addEventListener(\"click\", _gaLt, !1) }, !1) : w.attachEvent && w.attachEvent(\"onload\", function () { document.body.attachEvent(\"onclick\", _gaLt) });");
            SBoutbounglinktrackdivstr.AppendLine("});");
            SBoutbounglinktrackdivstr.AppendLine("</script>");
            outbounglinktrackdiv.InnerHtml = SBoutbounglinktrackdivstr.ToString();
        }
        SBtrackinstr.AppendLine("</script>");
        divTrackingCode.InnerHtml = SBtrackinstr.ToString();
    }

    private void GetMetaTag(Page page, DBHelper dp)
    {
        DBCommand strQuery = new DBCommand();
        strQuery.CommandText = "SELECT * FROM Brand_AppSettings WHERE IsDelete='False' AND Type=1";
        strQuery.CommandType = CommandType.Text;
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                HtmlMeta metaTag = new HtmlMeta();
                if (Convert.ToString(dr["name"]).ToLower() == "title")
                {
                    page.Title = Convert.ToString(dr["description"]);
                    if (Convert.ToBoolean(dt.Rows[0]["IsMetatag"]))
                    {
                        metaTag.Name = "robots";
                        metaTag.Content = "noindex,nofollow";
                        page.Header.Controls.Add(metaTag);   //page  is the object of Page class
                    }
                    else
                    {
                        metaTag.Name = "robots";
                        metaTag.Content = "index, follow all";
                        page.Header.Controls.Add(metaTag);

                        metaTag.Name = "revisit-after";
                        metaTag.Content = "7 days";
                        page.Header.Controls.Add(metaTag);
                    }
                }
                else
                {
                    if (Convert.ToString(dr["MetaType"]) == "True")
                    {
                        metaTag.Attributes.Add("property", Convert.ToString(dr["name"]));
                    }
                    else
                    {
                        metaTag.Name = utils.formatStringForDBSelect(Convert.ToString(dr["name"]));
                    }
                    metaTag.Content = utils.formatStringForDBSelect(Convert.ToString(dr["description"]));
                    Page.Header.Controls.Add(metaTag);
                }
            }
        }
    }

    private void ShowComponents(DBHelper dp)
    {
        bool ISShowSummaryComponents = true;
        if(Convert.ToString(Request.RawUrl).ToLower().Contains("contactus.aspx"))
        {
            ISShowSummaryComponents = false;
        }
        DBCommand strQuery = new DBCommand();
        strQuery.CommandText = "SP_Brand_Select_Features";
        strQuery.CommandType = CommandType.StoredProcedure;
        DataTable dtAdminFeatures = dp.FillDataTable(strQuery);
        string[] arrAdminFeatures = { };
        if (dtAdminFeatures != null && dtAdminFeatures.Rows.Count > 0)
        {
            arrAdminFeatures = Convert.ToString(dtAdminFeatures.Rows[0][0]).Split(',');
        }

        string[] strArrFeatures = ReadConfig.GetFeatures().Split(',');

        //Blog
        string Blog = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Blog)));
        if (strArrFeatures.Contains(Blog) && Array.IndexOf(arrAdminFeatures, Blog) != -1)
        {
            this.divImageBlog.Visible = true;
            this.divImageBlogFooter.Visible = true;
            this.divImageBlogFooter1.Visible = true;
            this.divImageBlog1.Visible = false;
        }
        else
        {
            this.divImageBlog.Visible = false;
            this.divImageBlogFooter.Visible = false;
            this.divImageBlogFooter1.Visible = false;
            this.divImageBlog1.Visible = true;
        }
        //RSS
        string RSS = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.RSS)));
        if (strArrFeatures.Contains(RSS) && Array.IndexOf(arrAdminFeatures, RSS) != -1)
        {
            this.divImageRssreader.Visible = true;
            this.divImageRssreaderFooter.Visible = true;
            this.divImageRssreaderFooter1.Visible = true;
            this.divImageRssreader1.Visible = false;
        }
        else
        {
            this.divImageRssreader.Visible = false;
            this.divImageRssreaderFooter.Visible = false;
            this.divImageRssreaderFooter1.Visible = false;
            this.divImageRssreader1.Visible = true;
        }
        //BookMark
        string Bookmark = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Bookmark)));
        if (strArrFeatures.Contains(Bookmark) && Array.IndexOf(arrAdminFeatures, Bookmark) != -1)
        {
            this.divImageBookmark.Visible = true;
            this.divImageBookmarkFooter.Visible = true;
            this.divImageBookmarkFooter1.Visible = true;
            this.divImageBookmark1.Visible = false;
        }
        else
        {
            this.divImageBookmark.Visible = false;
            this.divImageBookmarkFooter.Visible = false;
            this.divImageBookmarkFooter1.Visible = false;
            this.divImageBookmark1.Visible = true;
        }
        //Calendar
        string Calendar = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Calendar)));
        if (strArrFeatures.Contains(Calendar) && Array.IndexOf(arrAdminFeatures, Calendar) != -1)
        {
            this.divImageCalendar.Visible = true;
            this.divImageCalendarfooter.Visible = true;
            this.divImageCalendarfooter1.Visible = true;
            this.divImageCalendar1.Visible = false;
        }
        else
        {
            this.divImageCalendar.Visible = false;
            this.divImageCalendarfooter.Visible = false;
            this.divImageCalendarfooter1.Visible = false;
            this.divImageCalendar1.Visible = true;
        }
        //Search
        string Search = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Search)));
        if (strArrFeatures.Contains(Search) && Array.IndexOf(arrAdminFeatures, Search) != -1 && ISShowSummaryComponents)
        {
            this.divsurf_Search.Visible = true;
        }
        //RSSSummary
        string RSSSummary = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.RSSSummary)));
        if (strArrFeatures.Contains(RSSSummary) && Array.IndexOf(arrAdminFeatures, RSSSummary) != -1 && ISShowSummaryComponents)
        {
            this.divsurf_RSS.Visible = true;
            rssSummaryHeading.InnerHtml = "More News Stories<br><span class='SummaryByClass'>by " + ReadConfig.getDomainUrl() + "</span>";
        }
        //BookMarkSummary
        string BookMarkSummary = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.BookMarkSummary)));
        if (strArrFeatures.Contains(BookMarkSummary) && Array.IndexOf(arrAdminFeatures, BookMarkSummary) != -1 && ISShowSummaryComponents)
        {
            this.divsurf_BookMarks.Visible = true;
            BookmarkSummary_Heading.InnerHtml = "Suggested Places for You<br><span class='SummaryByClass'>by " + ReadConfig.getDomainUrl() + "</span>";
        }
        //Share
        string Share = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Share)));
        if (strArrFeatures.Contains(Share) && Array.IndexOf(arrAdminFeatures, Share) != -1)
        {
            this.div_share.Visible = true;
        }
        //MostPopular
        string MostPopular = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.MostPopular)));
        if (strArrFeatures.Contains(MostPopular) && Array.IndexOf(arrAdminFeatures, MostPopular) != -1)
        {
            this.divsurf_MostPopular.Visible = true;
            MostPopular_Heading.InnerHtml = "Most Popular";
        }
        //RegisterLogin
        string RegisterLogin = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.registerlogin)));
        if (strArrFeatures.Contains(RegisterLogin) && Array.IndexOf(arrAdminFeatures, RegisterLogin) != -1)
        {
            this.div_registerlogin.Visible = true;
            registerloginHeading.InnerHtml = "Login";
            if (Request.Cookies["userEmailLogin"] != null && Request.Cookies["userPasswordlLogin"] != null)
            {
                chkRememberMe.Checked = true;
                txtEmailLogin.Text = Request.Cookies["userEmailLogin"].Value.ToString().Trim();
                txtPasswordLogin.Attributes["value"] = Request.Cookies["userPasswordlLogin"].Value.ToString().Trim();
            }
            if ((Session["userid"] != null && Convert.ToString(Session["userid"]) != "0"))
            {
                divlogin.Style.Add("display", "none");
                divUserDetail.Style.Add("display", "block");
                registerloginHeading.InnerHtml = "User";
                DataTable dtuser = utils.GetUserDetail(Session["userid"].ToString());
                userName.InnerHtml = "Name: " + dtuser.Rows[0]["fullname"].ToString();
            }
        }
        //Subscribe
        string Subscribe = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Subscribe)));
        if (strArrFeatures.Contains(Subscribe) && Array.IndexOf(arrAdminFeatures, Subscribe) != -1)
        {
            this.divsurf_Subscribe.Visible = true;
            Subscribe_Heading.InnerHtml = "Subscribe to " + ReadConfig.getDomainUrl() + " via Email";
            divSubscribeDesc.InnerHtml = "Enter your email address to subscribe to " + ReadConfig.getDomainUrl() + " and receive notifications of new posts by email.";
        }

        if ((Convert.ToString(Session["role"]) == "super admin" || Convert.ToString(Session["role"]) == "admin") && (Session["userid"] != null && Convert.ToString(Session["userid"]) != "0"))
        {
            this.divAdminTask.Visible = true;
            this.divAdminTask1.Visible = false;
        }
        else
        {
            this.divAdminTask.Visible = false;
            this.divAdminTask1.Visible = true;
        }
    }

    private void bindRssGrid(string DomnainName, DBHelper dp)
    {
        string keyRssSUmmary = "rsssummary-" + DomnainName.ToLower().Trim();
        DataTable dt = new DataTable();
        if (Cache[keyRssSUmmary] != null)
        {
            dt = (DataTable)Cache[keyRssSUmmary];
        }
        else
        {
            DBCommand strSql = new DBCommand();
            strSql.CommandText = "SP_Brand_Select_Top10_Rss";
            strSql.CommandType = CommandType.StoredProcedure;
            dt = dp.FillDataTable(strSql);
            Cache.Insert(keyRssSUmmary, dt, null, DateTime.Now.AddHours(5), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        this.dgrdSurfRSS.DataSource = dt;
        this.dgrdSurfRSS.DataBind();
    }

    private void getBookmarkSummary(string DomnainName, DBHelper dp)
    {
        try
        {
            string keyBookmarkSUmmary = "bookmarksummary-" + DomnainName;
            DataTable dtBookmark = new DataTable();
            if (Cache[keyBookmarkSUmmary] != null)
            {
                dtBookmark = (DataTable)Cache[keyBookmarkSUmmary];
            }
            else
            {
                DBCommand cmdBookmark = new DBCommand();
                cmdBookmark.CommandType = CommandType.StoredProcedure;
                cmdBookmark.CommandText = "SP_Brand_Select_Top30_BookMark";
                dtBookmark = dp.FillDataTable(cmdBookmark);
                if(dtBookmark.Rows.Count > 0)
                {
                    if(!dtBookmark.Columns.Contains("rel"))
                        dtBookmark.Columns.Add("rel");
                    foreach (DataRow dr in dtBookmark.Rows)
                    {
                        int nofollow = Convert.ToInt32(dr["nofollow"].ToString());
                        int noindex = Convert.ToInt32(dr["noindex"].ToString());
                        int noopener = Convert.ToInt32(dr["noopener"].ToString());
                        int noreferrer = Convert.ToInt32(dr["noreferrer"].ToString());//RK
                        string strrel = " rel=\"";
                        if (nofollow != 0)
                        {
                            strrel += "nofollow";
                        }
                        if (noindex != 0)
                        {
                            strrel += strrel == " rel=\"" ? "noindex" : " noindex";
                        }
                        if (noopener != 0)
                        {
                            strrel += strrel == " rel=\"" ? "noopener" : " noopener";
                        }
                        //RK
                        if (noreferrer != 0)
                        {
                            strrel += strrel == " rel=\"" ? "noreferrer" : " noreferrer";
                        }
                        strrel += "\"";
                        dr["rel"] = strrel;
                    }
                }

                //Cache.Insert(keyBookmarkSUmmary, dtBookmark, null, DateTime.Now.AddHours(0.25), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            drgdList.DataSource = dtBookmark;
            drgdList.DataBind();
        }
        catch(Exception ex)
        {
        }
    }

    protected void drgdList_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            String linkid = drv["Id"].ToString();

            String SiteId2 = Convert.ToString(Cache["SiteId_" + ReadConfig.getDomainUrl()]);
            int clickstatus = Convert.ToInt32(drv["Blocker"].ToString());//rk 18-07-2019
            int sameipstatus = Convert.ToInt32(drv["SameIP_Status"].ToString());
            int sameipvalue = Convert.ToInt32(drv["SameIP_Value"].ToString());
            int sameipclickcountlink = 0;//rk 18-07-2019
            if (clickstatus != 0 || sameipstatus != 0)
            { 
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Select_SameIP_LinkClick";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", utils.GetUserIP());
                cmd.Parameters.Add("@LinkID", Convert.ToString(linkid));
                cmd.Parameters.Add("@DomainID", SiteId2);
                cmd.Parameters.Add("@BlockStatus", 0);
                sameipclickcountlink = Convert.ToInt32(dpreport.ExecuteScalar(cmd));
            }
            int rowIndex = e.Item.ItemIndex;
            Label SummarySameIPClicksLink = (Label)e.Item.FindControl("SummarySameIPClicksLink");

            SummarySameIPClicksLink.Text = sameipclickcountlink.ToString();
            SummarySameIPClicksLink.Style.Add(HtmlTextWriterStyle.Display, "none");

            Image SummaryLinkBlockImage = (Image)e.Item.FindControl("SummaryLinkBlockImage");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Display, "none");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.ZIndex, "9999");
            SummaryLinkBlockImage.Style.Add("float", "right");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Position, "absolute");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Left, "0");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Top, "0");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Width, "100%");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Height, "100%");
            SummaryLinkBlockImage.Attributes.Add("onclick", "RecordClickLink(" + linkid + ", " + SiteId2 + ", 1)");
            if(sameipvalue <= 0)
            {
                sameipstatus = 0;
            }
            if (sameipstatus == 1 && sameipvalue <= sameipclickcountlink)
            {
                SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Display, "block");
                e.Item.Attributes["style"] = "display:none";
            }
        }
    }

    private void getMostPopular(string DomnainName, DBHelper dp)
    {
        try
        {
            string keyMostPopular = "mostpopular-" + DomnainName;
            DataTable dtMostPopular = new DataTable();
            if (Cache[keyMostPopular] != null)
            {
                dtMostPopular = (DataTable)Cache[keyMostPopular];
            }
            else
            {
                DBCommand cmdMostPopular = new DBCommand();
                cmdMostPopular.CommandType = CommandType.Text;
                cmdMostPopular.CommandText = "SELECT TOP 5 post_title,post_title as URL,post_id FROM BRAND_BLOG_POSTS WHERE Active_Inactive=1 ORDER BY c_date DESC";
                dtMostPopular = dp.FillDataTable(cmdMostPopular);
                if (dtMostPopular.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtMostPopular.Rows)
                    {
                        if (Convert.ToString(dr["post_title"]) != "")
                        {
                            string strTitle = utils.formatStringForURLSelect(dr["post_title"].ToString());
                            dr["URL"] = "http://" + ReadConfig.getDomainUrl() + "/features/" + utils.GeneratePostTitleForURL(strTitle) + ".aspx";
                        }
                    }
                }
                Cache.Insert(keyMostPopular, dtMostPopular, null, DateTime.Now.AddHours(5), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            dgrdMostPopular.DataSource = dtMostPopular;
            dgrdMostPopular.DataBind();
        }
        catch
        {
        }
    }

    protected void lnkHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/features/index.aspx");
    }

    private void GetNonePostionWidgets()
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string widgetOS = utils.GetOperatingSystem();
        string stradValue = string.Empty;
        stradValue = funAd("none", widgetconid, widgetBrowser, widgetOS);
        if (stradValue != "")
        {
            widget_Grid_none.InnerHtml = stradValue;
        }
    }

    private void GetWidgets()
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string widgetOS = utils.GetOperatingSystem();
        string stradValue = string.Empty;
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dtAdBox = dp.FillDataTable("select * from brand_Applist_Random");
        if (dtAdBox.Rows.Count > 0)
        {
            foreach (DataRow dr in dtAdBox.Rows)
            {
                if (Convert.ToString(dr["Random_Position"]) == "random_topbanner")
                {
                    /*Video widget*/
                    funTopAd(widgetconid, widgetBrowser, Convert.ToBoolean(Convert.ToInt32(dr["Value"])), widgetOS);
                }
                /*  right */
                else if (Convert.ToString(dr["Random_Position"]) == "random_right")
                {
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        stradValue = funAdRandom("righttop", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        stradValue = funAd("righttop", widgetconid, widgetBrowser, widgetOS);
                    }

                    if (stradValue != "")
                    {
                        right_ad_box.Visible = true;
                        right_ad_box.InnerHtml = stradValue;
                    }
                    else
                    {
                        right_ad_box.Visible = false;
                    }

                } /* left */
                else if (Convert.ToString(dr["Random_Position"]) == "random_left")
                {
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        stradValue = funAdRandom("leftbottom", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        stradValue = funAd("leftbottom", widgetconid, widgetBrowser, widgetOS);
                    }

                    if (stradValue != "")
                    {
                        left_ad_box.Visible = true;
                        left_ad_box.InnerHtml = stradValue;
                    }
                    else
                    {
                        left_ad_box.Visible = false;
                    }
                }
                /*  bottom */
                else if (Convert.ToString(dr["Random_Position"]) == "random_bottom")
                {
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        stradValue = funAdRandom("bottomright", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        stradValue = funAd("bottomright", widgetconid, widgetBrowser, widgetOS);
                    }

                    if (stradValue != "")
                    {
                        bottom_ad_box.Visible = true;
                        bottom_ad_box.InnerHtml = stradValue;
                    }
                    else
                    {
                        bottom_ad_box.Visible = false;
                    }
                }
                /*  pop under */
                else if (Convert.ToString(dr["Random_Position"]) == "random_pop_under")
                {
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        stradValue = funAdRandom("pop_under", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        stradValue = funAd("pop_under", widgetconid, widgetBrowser, widgetOS);
                    }

                    if (stradValue != "")
                    {
                        pop_under_ad_box.Visible = true;
                        pop_under_ad_box.InnerHtml = stradValue;
                    }
                    else
                    {
                        pop_under_ad_box.Visible = false;
                    }
                }
                /*  top  */
                else if (Convert.ToString(dr["Random_Position"]) == "random_topheader")
                {
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        stradValue = funAdRandom("topheader", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        stradValue = funAd("topheader", widgetconid, widgetBrowser, widgetOS);
                    }

                    if (stradValue != "")
                    {
                        top_header_ad_box.Visible = true;
                        top_header_ad_box.InnerHtml = stradValue;
                    }
                    else
                    {
                        top_header_ad_box.Visible = false;
                    }
                }
                /*  subline  */
                else if (Convert.ToString(dr["Random_Position"]) == "random_subline")
                {
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        stradValue = funAdRandom("subline", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        stradValue = funAd("subline", widgetconid, widgetBrowser, widgetOS);
                    }

                    if (stradValue != "")
                    {
                        subline_ad_box.Visible = true;
                        subline_ad_box.InnerHtml = stradValue;
                    }
                    else
                    {
                        subline_ad_box.Visible = false;
                    }
                }
            }
        }
    }

    private void funTopAd(string widgetconid, string widgetBrowser, bool IsRandom, string widgetOS)
    {       
        StringBuilder strTopAdBox = new StringBuilder();
        DataTable dtTopAdBox = new DataTable();
        string strSQLRule = string.Empty;
        string codebox = string.Empty;
        string KeyWidgetTop = "WidgetTop" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + IsRandom + widgetOS.Trim();
        if (Cache[KeyWidgetTop] != null)
        {
            dtTopAdBox = (DataTable)Cache[KeyWidgetTop];
        }
        else
        {
            string strQueryTopAdBox = "SELECT id,Referal,Not_Referal,ApplicationName,CodeBox FROM Brand_AppList "
                + "WHERE status=1 and (Position ='topbanner' OR Position = 'Top_not_user') and (Country  like '%" + widgetconid + "%' or Country='' "
                + "or Country is null) and (Browser like '%" + widgetBrowser + "%' OR  Browser like '%0%') and (OS like '%" + widgetOS + "%' OR OS='' OR OS IS NULL)";
            if (IsRandom)
                strQueryTopAdBox += "  and IsRandom=1";

            DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
            dtTopAdBox = dp.FillDataTable(strQueryTopAdBox);
            Cache[KeyWidgetTop] = dtTopAdBox;
        }
        if (dtTopAdBox.Rows.Count > 0)
        {
            Random rnd = new Random();
            int i = 0;
            if (IsRandom && dtTopAdBox.Rows.Count > 1)
            {
                i = rnd.Next(dtTopAdBox.Rows.Count);
            }
            if (utils.ReferalCheck(Convert.ToString(dtTopAdBox.Rows[i]["Referal"]), Convert.ToString(dtTopAdBox.Rows[i]["Not_Referal"])))
            {
                top_ad_box.Visible = false;
                if (dtTopAdBox.Rows[i]["ApplicationName"].ToString().ToLower() == "video_html5_playlist")
                {
                    top_ad_box5.Visible = true;
                }
                else
                {
                    Random random = new Random();
                    int cacheBuster = random.Next(10000000, 100000000);
                    BannerIDs += dtTopAdBox.Rows[i]["id"].ToString() + ",";
                    codebox = utils.formatStringForDBSelect(Convert.ToString(dtTopAdBox.Rows[i]["CodeBox"]));
                    string pageUrl = Request.Url.Scheme + "://" + Request.Url.Host + Request.RawUrl;
                    //RK 14-05-2019
                    string newcodebox = codebox.Replace("[Host_URL]", strDomainName).Replace("[CACHEBUSTER]", Convert.ToString(cacheBuster)).Replace("[page_url]", pageUrl).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent()).Replace("[Host_Url]", strDomainName);                   
                    strTopAdBox.AppendLine("<div id=\"" + Convert.ToString(dtTopAdBox.Rows[i]["ApplicationName"]) + "\" class=\"" + Convert.ToString(dtTopAdBox.Rows[i]["ApplicationName"]) + "\" >");
                    strTopAdBox.AppendLine(HttpUtility.HtmlDecode(newcodebox));
                    strTopAdBox.AppendLine("</div>");
                    top_ad_box.InnerHtml = strTopAdBox.ToString();
                    top_ad_box5.Visible = false;
                    top_ad_box.Visible = true;
                }
                //utils.updateWidgetCount(Convert.ToInt16(dtTopAdBox.Rows[i]["id"]));
            }
        }
    }

    private string funAd(string strPosition, string widgetconid, string widgetBrowser, string widgetOS)
    {
        StringBuilder strAdBox = new StringBuilder();
        DataTable dtAdBoxIndivi = new DataTable();
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        string codebox = string.Empty;
        int App_Id = 0;
        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_BrandAppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        strQueryAdBox.AddParameter("@Position", strPosition);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);
        if (strPosition == "none")
        {
            if (dt_AppID.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_AppID.Rows)
                {
                    Random random = new Random();
                    int cacheBuster = random.Next(10000000, 100000000);
                    codebox = utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]));
                    if (AddTrackingCode == 1)
                    {
                        codebox = codebox.Replace("GAID", ConfigLoader.GetGA_ID());
                        codebox = codebox.Replace("UserHostAddress", Request.UserHostAddress);
                        codebox = codebox.Replace("Time", System.DateTime.Now.ToString("M/d/yyyy h:m:s.ffffff tt"));
                    }
                    string pageUrl = Request.Url.Scheme + "://" + Request.Url.Host + Request.RawUrl;
                    string newcodebox = codebox.Replace("[Host_URL]", strDomainName).Replace("[CACHEBUSTER]", Convert.ToString(cacheBuster)).Replace("[page_url]", pageUrl).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent()).Replace("[Host_Url]", strDomainName);                   
                    strAdBox.AppendLine("<div id=\"" + Convert.ToString(dr["ApplicationName"]) + "\" class=\"" + Convert.ToString(dr["ApplicationName"]) + "\" >");
                    strAdBox.AppendLine(HttpUtility.HtmlDecode(newcodebox));
                    strAdBox.AppendLine("</div>");
                    //utils.updateWidgetCount(Convert.ToInt16(dr["id"]));
                }
            }
        }
        else
        {
            if (dt_AppID.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_AppID.Rows)
                {
                    App_Id = Convert.ToInt32(dr["id"]);
                    string KeyWidgetAllPosition = "WidgetAllPosition" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + App_Id + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + strPosition.Trim() + widgetOS.Trim();
                    if (Cache[KeyWidgetAllPosition] != null)
                    {
                        dtAdBoxIndivi = (DataTable)Cache[KeyWidgetAllPosition];
                    }
                    else
                    {
                        string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
                        strSQLRule = SqlQueryMain;
                        DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
                        dtAdBoxIndivi = dp1.FillDataTable(strSQLRule);
                        Cache.Insert(KeyWidgetAllPosition, dtAdBoxIndivi, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
                    }

                    if (dtAdBox.Rows.Count > 0)
                    {
                        foreach (DataRow drIndivi in dtAdBoxIndivi.Rows)
                        {
                            dtAdBox.ImportRow(drIndivi);
                        }
                    }
                    else
                        dtAdBox = dtAdBoxIndivi.Copy();
                }
            }
            if (dtAdBox.Rows.Count > 0)
            {
                foreach (DataRow dr in dtAdBox.Rows)
                {
                    if (utils.ReferalCheck(Convert.ToString(dr["Referal"]), Convert.ToString(dr["Not_Referal"])))
                    {
                        strAdBox.Append(GetfunAd(dr, strPosition, dtAdBox.Rows.IndexOf(dr)));
                    }
                }
            }
        }
        return Convert.ToString(strAdBox);
    }

    private string funAdRandom(string strPosition, string widgetconid, string widgetBrowser, string widgetOS)
    {
        StringBuilder strAdBox = new StringBuilder();
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        int App_Id = 0;

        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_Top1_Brand_AppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Position", strPosition);
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);

        if (dt_AppID.Rows.Count > 0)
        {
            App_Id = Convert.ToInt32(dt_AppID.Rows[0]["id"]);
        }
        string KeyWidgetRandomAllPosition = "WidgetRandomAllPosition" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + App_Id + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + strPosition.Trim() + widgetOS.Trim();
        if (Cache[KeyWidgetRandomAllPosition] != null)
        {
            dtAdBox = (DataTable)Cache[KeyWidgetRandomAllPosition];
        }
        else
        {
            string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
            strSQLRule = SqlQueryMain;
            DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            dtAdBox = dp1.FillDataTable(strSQLRule);
            Cache.Insert(KeyWidgetRandomAllPosition, dtAdBox, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        if (dtAdBox.Rows.Count > 0)
        {
            if (utils.ReferalCheck(Convert.ToString(dtAdBox.Rows[0]["Referal"]), Convert.ToString(dtAdBox.Rows[0]["Not_Referal"])))
            {
                strAdBox.Append(GetfunAd(dtAdBox.Rows[0], strPosition,0));
            }
        }
        return Convert.ToString(strAdBox);
    }

    private string GetfunAd(DataRow dr, string strPosition,int rowindex)
    {
        string stradimage = "";
        int IPStatus = 0;
        int IPValue = 0;
        int IPDay = 0;
        int Blocker = 0;
        string ApplicationName = Convert.ToString(dr["ApplicationName"]);
        int BlockerValue = 0;
        int BanID = Convert.ToInt32(dr["id"]);
        int height = 0;
        int width = 0;
        string codebox = string.Empty;

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Status"])))
        {
            IPStatus = Convert.ToInt32(dr["SameIP_Status"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Value"])))
        {
            IPValue = Convert.ToInt32(dr["SameIP_Value"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Days"])))
        {
            IPDay = Convert.ToInt32(dr["SameIP_Days"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["Blocker"])))
        {
            Blocker = Convert.ToInt32(dr["Blocker"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["BlockerValue"])))
        {
            BlockerValue = Convert.ToInt32(dr["BlockerValue"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["height"])))
        {
            height = Convert.ToInt32(dr["height"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["width"])))
        {
            width = Convert.ToInt32(dr["width"].ToString().Trim());
        }

        string strclickfunction = "";
        if (strPosition == "pop_under")
        {
            //BannerIDs += BanID + ",";
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "imagepopunderblock", "WidgetClick", "PopunderBlockerCount", "SameIPCountPopunder", "popunderscript", rowindex, ref strclickfunction);
        }

        if (strPosition == "subline")
        {
            //BannerIDs += BanID + ",";
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "imagesublineblock", "WidgetClick", "SublineBlockerCount", "SameIPCountSubline", "sublinescript", rowindex, ref strclickfunction);
        }
        if (strPosition == "topheader")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "imageblock", "WidgetClick", "TopBlockerCount", "SameIPCountTop", "topHeaderscript", rowindex, ref strclickfunction);
        }

        if (strPosition == "leftbottom")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "imageleftblock", "WidgetClick", "leftbottomBlockerCount", "SameIPCountleftbottom", "leftBottomscript", rowindex, ref strclickfunction);
        }

        if (strPosition == "righttop")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "Imglogotextright", "WidgetClick", "righttopBlockerCount", "SameIPCountrighttop", "rightTopscript", rowindex, ref strclickfunction);
        }

        if (strPosition == "bottomright")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "imageBottomblock", "WidgetClick", "bottomrightBlockerCount", "SameIPCountbottomright", "rightBottomscript", rowindex, ref strclickfunction);
        }

        Random random = new Random();
        int cacheBuster = random.Next(10000000, 100000000);
        codebox = utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]));
        string pageUrl = Request.Url.Scheme + "://" + Request.Url.Host + Request.RawUrl;
        string newcodebox = codebox.Replace("[Host_URL]", strDomainName).Replace("[CACHEBUSTER]", Convert.ToString(cacheBuster)).Replace("[page_url]", pageUrl).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent()).Replace("[Host_Url]", strDomainName);        
        StringBuilder strAdBox = new StringBuilder();
        if (height == 0 && width == 0)
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"position:relative;width:fit-content;height:fit-content;width:intrinsic;width:-moz-max-content;width:-webkit-max-content;height:intrinsic;height:-moz-max-content;height:-webkit-max-content;\" " + strclickfunction + ">" + stradimage
                + HttpUtility.HtmlDecode(newcodebox) + "</div>");
        }
        else
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"height:fit-content;height:intrinsic;height:-moz-max-content;height:-webkit-max-content;width:" + width + "px;position:relative;\" " + strclickfunction + ">" + stradimage
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(newcodebox)) + "</div>");
        }
        //utils.updateWidgetCount(BanID);
        return strAdBox.ToString();
    }

    private string Get_Image_RegisterScriptBlock_And_Attach_Click_Event(int BanID, int IPStatus, int IPValue, int IPDays, int Blocker, int BlockerValue, string ApplicationName
        , string imageid, string clickfunname, string JSVarBlockerCount, string JSVarSameIPCount, string ScriptblockKeyName, int rowindex, ref string clickfunction)
    {
        
        int SameIPCount = utils.SameIPClicks(BanID, "banner", IPDays);
        BannerIDs += BanID + ",";
        //utils.Setimperssion(BanID, "banner");
        string strdis = "none";
        string type = "0";
        if (IPStatus == 1 && IPValue <= SameIPCount && BanID != 0)
        {
            strdis = "block";
            type = "2";
        }
        if (strdis == "none" && Blocker != 1)
            type = "1";
        clickfunction = "onclick=\"javascript:" + clickfunname + "('" + BanID + "'," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",'" + imageid + "_" + rowindex
            + "','" + ApplicationName + "','" + rowindex + "','" + JSVarSameIPCount + "','" + JSVarBlockerCount + "')\"";
        string stradimage = "<img  alt='1x2' id ='" + imageid + "_" + rowindex + "' style='display:" + strdis + ";z-index:9999;float:right;position:absolute;left:0;top:0;width:100%; height:100%;' "
            + "src='../images_common/1x2.gif' />";
        StringBuilder strScript = new StringBuilder();
        strScript.Append(" <script type=\"text/javascript\" language=\"javascript\">");
        strScript.Append(" var " + JSVarBlockerCount + "_" + rowindex + " = 0;var " + JSVarSameIPCount + "_" + rowindex + " = " + SameIPCount + ";");
        strScript.Append("</script>");
        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), ScriptblockKeyName + "_" + rowindex, strScript.ToString());
        return stradimage;
    }

    protected void btnSuscribe_Click(object sender, EventArgs e)
    {
        //MailerBrand.sendSubscribeEmail(txtsubscribeemail.Text);
        utils.SubscribeUser(txtsubscribeemail.Text);
        lblsubsmsg.Text = "Subscription is successful.";
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DBCommand strQuery = new DBCommand();
        strQuery.CommandText = "select defaultSearch from [brand_settings]";
        strQuery.CommandType = CommandType.Text;
        DataTable dt = dp.FillDataTable(strQuery);
        string defaultSearch = "1";
        if (dt != null && dt.Rows.Count > 0)
        {
            if (Convert.ToString(dt.Rows[0]["defaultSearch"]) != "")
            {
                defaultSearch = Convert.ToString(dt.Rows[0]["defaultSearch"]);
            }
        }
        if (defaultSearch == "2")
        {
            Response.Redirect("~/Search.aspx?q="+ txtSearch.Text.ToString().Trim());
        }
        else
        {
            Session["q"] = txtSearch.Text.ToString().Trim();
            Response.Redirect("~/Search.aspx");
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        msgLoginRegister.Text = "";
        if (Session["CaptchaImageText"] != null)
        {
            if (txtCaptcha.Text == Session["CaptchaImageText"].ToString())
            {
                DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
                string userEmail = txtEmailLogin.Text.Trim();
                string encodedpassword = utils.EncryptPassword(txtPasswordLogin.Text.Trim());
                string strQueryuserExist = "Select * from brand_user where email=@Email and password=@Password and roles='user'";
                DBCommand cmd = new DBCommand();
                cmd.CommandText = strQueryuserExist;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@Email", userEmail);
                cmd.Parameters.Add("@Password", encodedpassword);
                DataTable dt = dp.FillDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    if (chkRememberMe.Checked)
                    {
                        Response.Cookies["userEmailLogin"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["userEmailLogin"].Value = txtEmailLogin.Text.ToString().Trim();
                        Response.Cookies["userPasswordlLogin"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["userPasswordlLogin"].Value = txtPasswordLogin.Text.ToString().Trim();
                    }
                    else
                    {
                        Response.Cookies["userEmailLogin"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["userPasswordlLogin"].Expires = DateTime.Now.AddDays(-1);
                    }
                    Session["userid"] = dt.Rows[0]["uid"].ToString();
                    Session["role"] = "user";
                    divlogin.Style.Add("display", "none");
                    divUserDetail.Style.Add("display", "block");
                    registerloginHeading.InnerHtml = "User";
                    userName.InnerHtml = "Name: " + dt.Rows[0]["fullname"].ToString();
                    Response.Redirect("~/User/manageaccount.aspx");
                }
                else
                {
                    msgLoginRegister.Text = "Incorrect login credentials.";
                    divUserDetail.Style.Add("display", "none");
                    divlogin.Style.Add("display", "block");
                    registerloginHeading.InnerHtml = "Login";
                }
            }
            else
            {
                this.Session["CaptchaImageText"] = utils.GenerateRandomCode();
                imgcaptcha.Src = "~/Captcha.aspx";
                msgLoginRegister.Text = "please enter valid verification code.";
                divUserDetail.Style.Add("display", "none");
                divlogin.Style.Add("display", "block");
                registerloginHeading.InnerHtml = "Login";
            }
        }
        else
        {
            this.Session["CaptchaImageText"] = utils.GenerateRandomCode();
            imgcaptcha.Src = "~/Captcha.aspx";
        }
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        msgLoginRegister.Text = "";
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string userEmail = txtEmailRegister.Text.Trim();
        string strQueryEmailExist = "Select * from brand_user where email=@Email and roles<>'subscribe'";
        DBCommand cmd = new DBCommand();
        cmd.CommandText = strQueryEmailExist;
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.Add("@Email", userEmail);
        DataTable dt = dp.FillDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            msgLoginRegister.Text = "Email already registered.";
            divregister.Style.Add("display", "block");
            divlogin.Style.Add("display", "none");
            registerloginHeading.InnerHtml = "register";
        }
        else
        {
            string encodedpassword = utils.EncryptPassword(txtPasswordRegister.Text.Trim());
            utils.RegisterUser(userEmail, encodedpassword);
            string strQueryuserExist = "Select * from brand_user where email=@Email and password=@Password and roles='user'";
            DBCommand cmd1 = new DBCommand();
            cmd1.CommandText = strQueryuserExist;
            cmd1.CommandType = CommandType.Text;
            cmd1.Parameters.Add("@Email", userEmail);
            cmd1.Parameters.Add("@Password", encodedpassword);
            DataTable dt1 = dp.FillDataTable(cmd1);
            if (dt1.Rows.Count > 0)
            {
                Session["userid"] = dt1.Rows[0]["uid"].ToString();
                Session["role"] = "user";
                divregister.Style.Add("display", "none");
                divUserDetail.Style.Add("display", "block");
                registerloginHeading.InnerHtml = "User";
                userName.InnerHtml = "Name: " + dt1.Rows[0]["fullname"].ToString();
                Response.Redirect("~/User/manageaccount.aspx");
            }
            //msgLoginRegister.Text = "Registration successful. Please login to continue.";
            //divregister.Style.Add("display", "none");
            //divlogin.Style.Add("display", "block");
            //registerloginHeading.InnerHtml = "Login";
        }
    }

    protected void btnForgot_Click(object sender, EventArgs e)
    {
        msgLoginRegister.Text = "";
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string userEmail = txtEmailForgot.Text.Trim();
        string strQueryEmailExist = "Select * from brand_user where email=@Email and roles='user'";
        DBCommand cmd = new DBCommand();
        cmd.CommandText = strQueryEmailExist;
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.Add("@Email", userEmail);
        DataTable dt = dp.FillDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            string userid = dt.Rows[0]["uid"].ToString();
            string newpassword = utils.GetRandomPassword(10);
            string encodedpassword = utils.EncryptPassword(newpassword);
            if (utils.UpdatePassword(userid, encodedpassword))
            {
                MailerBrand.sendResetPasswordEmail(dt.Rows[0]["email"].ToString(), newpassword);
                if (Request.Cookies["userEmailLogin"].Value.ToString().Trim() == dt.Rows[0]["email"].ToString())
                {
                    Response.Cookies["userPasswordlLogin"].Value = newpassword;
                    txtPasswordLogin.Attributes["value"] = newpassword;
                }
                msgLoginRegister.Text = "Password sent.Please check your email for new password.";
                divforgotPassword.Style.Add("display", "none");
                divlogin.Style.Add("display", "block");
                registerloginHeading.InnerHtml = "Login";
            }
            else
            {
                msgLoginRegister.Text = "something went wrong. Please try again.";
            }
        }
        else
        {
            msgLoginRegister.Text = "Email not registered.";
        }
    }

    protected void lnkManageaccount_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/User/manageaccount.aspx");
    }

    protected void lnkSignout_Click(object sender, EventArgs e)
    {
        Session.Remove("userid");
        Session.Remove("role");
        if (Request.Cookies["userEmailLogin"] != null && Request.Cookies["userPasswordlLogin"] != null)
        {
            chkRememberMe.Checked = true;
            txtEmailLogin.Text = Request.Cookies["userEmailLogin"].Value.ToString().Trim();
            txtPasswordLogin.Attributes["value"] = Request.Cookies["userPasswordlLogin"].Value.ToString().Trim();
        }
        divUserDetail.Style.Add("display", "none");
        divlogin.Style.Add("display", "block");
        registerloginHeading.InnerHtml = "Login";
        Response.Redirect("~/Default.aspx");
    }
}
