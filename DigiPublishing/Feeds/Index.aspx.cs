﻿using System;
using System.Web.UI;

public partial class Feeds_Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - RSS ";
        lblmsgfeed.Text = "";

        if (!utils.CheckFeature((int)features.RSS))
            Response.Redirect("~/OndemandHome.aspx");
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}