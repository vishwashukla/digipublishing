﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Feeds_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div id="content-full">
    <img id="img_loading" src="http://<%=ReadConfig.getDomainUrl() %>/Images_Common/loading.gif" style="display:none" alt="loading" />
        <h4>
            <asp:Label ID="lblRss" runat="server"></asp:Label></h4>
            <asp:Label ID="lblmsgfeed" CssClass="error" runat="server"></asp:Label>
            <div id="dvRSSFeed"></div>
        <br />
        <asp:Panel ID="pnlPagingMain" runat="server">
            <div id="dlPaging"></div>
            <div style="height: 30px; text-align: center; margin: 10px 0 0 0;">
                <asp:Label ID="lblPageInfo" Font-Bold="true" runat="server"></asp:Label>
            </div>
        </asp:Panel>
    </div>
    <script type="text/javascript">
        var vars = [];
        $(document).ready(function () {
            if (window.location.href.toLowerCase().indexOf('feeditems') > -1) {
                vars = window.location.href.slice(window.location.href.toLowerCase().indexOf('feeditems') + ('feeditems').length + 1).split('/');
            }
            else if (window.location.href.toLowerCase().indexOf('updates') > -1) {
                vars = window.location.href.slice(window.location.href.toLowerCase().indexOf('updates') + ('updates').length + 1).split('/');
            }
            PopulateRSS('0');
        })

        function PopulateRSS(parRequestedPage) {
            var parRSSname = '';
            if (vars.length > 0) {
                parRSSname = vars[0];
                if (parRSSname.indexOf("#.") > -1)
                    parRSSname = parRSSname.slice(0, parRSSname.indexOf("#."));
            }

            var parRSSitem = '';
            if (vars.length > 1) {
                parRSSitem = vars[1];
                if (parRSSitem.indexOf("#.") > -1)
                    parRSSitem = parRSSitem.slice(0, parRSSitem.indexOf("#."));
            }


            parRSSname = escape(parRSSname).replace(/%25/g, '%');
            parRSSitem = escape(parRSSitem).replace(/%25/g, '%');

            setVarBlank();
            $('#img_loading').show();
            $.ajax({
                type: "Post",
                url: WebServiceDomain + "Services/RSSFeed.asmx/GetRSS",
                data: "{'rssname':'" + parRSSname + "','rssitem':'" + parRSSitem + "','RequestedPage':'" + parRequestedPage + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    try {
                        var obj = jQuery.parseJSON(data.d);
                        if (obj.Result.Message == "success") {
                            var dvRSSFeedContent = "";

                            if (typeof (obj.Result.DocumentElement.RSSitem.length) != "undefined") {
                                for (var item in obj.Result.DocumentElement.RSSitem) {
                                    dvRSSFeedContent += "<p><strong><a id=\"lnkURL\" target=\"_blank\" href='" + obj.Result.DocumentElement.RSSitem[item].link + "'>" + obj.Result.DocumentElement.RSSitem[item].title + "</a></strong></p>"
                                        + "<p>" + obj.Result.DocumentElement.RSSitem[item].description + "</p>";                            
                                }
                            }
                            else
                                dvRSSFeedContent += "<p><strong><a id=\"lnkURL\" target=\"_blank\" href='" + obj.Result.DocumentElement.RSSitem.link + "'>" + obj.Result.DocumentElement.RSSitem.title + "</a></strong></p>"
                                        + "<p>" + obj.Result.DocumentElement.RSSitem.description + "</p>";

                            $('#dvRSSFeed').html(dvRSSFeedContent);
                        }


                        if (typeof (obj.Result.RssTitle) != "undefined")
                            $('#<%= lblRss.ClientID %>').text(obj.Result.RssTitle);
                        if (typeof (obj.Result.Message) != "undefined" && obj.Result.Message != "success")
                            $('#<%= lblmsgfeed.ClientID %>').text(obj.Result.Message);
                        if (typeof (obj.Result.Paging) != "undefined")
                            $('#dlPaging').html(obj.Result.Paging);
                        if (typeof (obj.Result.CurrentPageCount) != "undefined")
                            $('#<%= lblPageInfo.ClientID %>').text(obj.Result.CurrentPageCount);

                    } catch (err) {
                        $('#<%= lblmsgfeed.ClientID %>').text(err.message);
                    }
                },
                error: function (xhr, thrownError) {
                    $('#<%= lblmsgfeed.ClientID %>').text("Server processing error!");
                },
                complete: function () {
                    $('#img_loading').hide();
                }
            });
        }

        function setVarBlank() {
                $('#<%= lblRss.ClientID %>').text('');
            $('#<%= lblmsgfeed.ClientID %>').text('');
            $('#dvRSSFeed').html('');
            $('#dlPaging').html('');
            $('#<%= lblPageInfo.ClientID %>').text('');
        }

        function PagingClick(indexpage) {
            PopulateRSS(indexpage);
        }
</script>
</asp:Content>

