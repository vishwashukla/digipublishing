﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;

public partial class DigipSuperAdmin_ManageSuperAdminUsers : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public static string strUserId = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Users";
        lblMsg.Text = "";
        if (Session["userid"] == null)
            Response.Redirect("~/" + ConfigLoader.GetSALoginPage());
        if (!IsPostBack)
        {
            GridBind();
            PopulatePermissions();
        }
    }

    // <summary>
    /// Method to bind data grid with all users list
    /// </summary>
    private void GridBind()
    {
        dgrdUsers.DataSource = getUserList();
        dgrdUsers.DataBind();
        mView.SetActiveView(vGrid);
    }

    /// <summary>
    /// Return data table with all users list
    /// </summary>
    /// <returns></returns>
    private DataTable getUserList()
    {
        DBCommand cmd = new DBCommand();
        string strQuery = "SELECT * FROM brand_user WHERE [status]=1 AND uid <> 0 AND roles='super admin' order by uname";
        DataTable dt = new DataTable();
        try
        {
            dt = dp.FillDataTable(strQuery);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["fullname"] = utils.formatStringForDBSelect(Convert.ToString(dr["fullname"]));
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
        return dt;
    }


    private void PopulatePermissions()
    {
        ChkbxlstAccessPermiossions.Items.Clear();
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Create New Site", "1"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("View Sites", "2"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Sites Export To HTML", "3"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("View Site Summary", "4"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Bulk Manage Sites", "5"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Superadmins", "6"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("All Sites Users", "7"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Widgets", "8"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Videos", "9"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Ad Networks", "10"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Templates", "11"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Regions", "12"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Countries", "13"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Genres", "14"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Languages", "15"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Maintenance", "16"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Click Report", "17"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Impression Report", "18"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("AdNetwork Report", "19"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Remove Cache", "20"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Ads.txt", "21"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("OutboundLink Report", "22"));
        ChkbxlstAccessPermiossions.Items.Add(new ListItem("Privacy & Term", "23"));
    }

    protected void lnkbtnAdd_OnClick(object sender, EventArgs e)
    {
        SetBlank();
        rfvPWD.Enabled = true;
        rfvCPWD.Enabled = true;
        mView.SetActiveView(vAddEdit);
    }

    /// <summary>
    /// Method event to edit a user's information
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dgrdUsers_EditCommand(object source, DataGridCommandEventArgs e)
    {
        strUserId = e.Item.Cells[0].Text;
        SetBlank();
        rfvPWD.Enabled = false;
        rfvCPWD.Enabled = false;
        btnCreate.Text = "Update"; 
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "BRAND_SEL_USER_SP";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@uid", Convert.ToInt32(strUserId));
        try
        {
            DataTable dt = dp.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                txtName.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["fullname"]));
                txtUserName.Text = Convert.ToString(Convert.ToString(dt.Rows[0]["uname"]));
                pwd.Value = Convert.ToString(dt.Rows[0]["password"]);
                txtEmail.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["email"]));

                string strUserPermissions = Convert.ToString(dt.Rows[0]["SuperAdminPermissions"]);
                if (!string.IsNullOrWhiteSpace(strUserPermissions))
                    utils.SetSelectedCheckBoxItems(ChkbxlstAccessPermiossions, strUserPermissions);

                mView.SetActiveView(vAddEdit);
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    /// <summary>
    /// Method to delete a user from database
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dgrdUsers_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        string strQuery = "delete from brand_user  WHERE uid=" + e.Item.Cells[0].Text;
        dp.ExecuteNonQuery(strQuery);
        lblMsg.Text = "user deleted successfully.";
        string prmsn_del="DELETE brand_SuperAdminPermissions WHERE UID= " + e.Item.Cells[0].Text + "";
        dp.ExecuteNonQuery(prmsn_del);
        GridBind();
    }

    protected void dgrdUsers_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Convert.ToString(Session["userid"]) == e.Item.Cells[0].Text)
            {
                LinkButton btnDel = (LinkButton)e.Item.FindControl("btnDel");
                if (btnDel != null)
                    btnDel.Visible = false;
            }
        }
    }

    /// <summary>
    /// Method to create or update a user
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCreate_Click(object sender, EventArgs e)
    {
      if (!ValidateFields())
           return;
        try
        {
            /// Password conversion 
            if (txtCPwd.Text.Trim() != "")
            {
                int saltSize = 2;
                Byte[] saltBytes = new byte[saltSize];
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5;
                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
                md5 = new MD5CryptoServiceProvider();
                originalBytes = ASCIIEncoding.Default.GetBytes(txtCPwd.Text.Trim() + this.sault.Value.Trim());
                encodedBytes = md5.ComputeHash(originalBytes);
                //Convert encoded bytes back to a 'readable' string
                pwd.Value = BitConverter.ToString(encodedBytes).Replace("-", "");
            }
       
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "[SP_BRAND_SUPERADMIN_UPDATE_USER]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@pwd", pwd.Value);
            cmd.Parameters.Add("@email", txtEmail.Text.Trim());
            cmd.Parameters.Add("@fullname", utils.formatStringForDBInsert(txtName.Text.Trim()));
            cmd.Parameters.Add("@uname", utils.formatStringForDBInsert(txtUserName.Text.Trim()));
            cmd.Parameters.Add("@roles", "super admin");
         
            string strPermissions = utils.GetSelectedCheckBoxItems(ChkbxlstAccessPermiossions);
            if (!string.IsNullOrWhiteSpace(strPermissions))
                cmd.Parameters.Add("@Permissions", strPermissions + ",");
            if (!string.IsNullOrWhiteSpace(strUserId))
            {
                cmd.Parameters.Add("@uid", strUserId);
                dp.ExecuteNonQuery(cmd);
            }
            else
            {
                strUserId = Convert.ToString(dp.FillDataTable(cmd).Rows[0][0]);
            }
           
            lblMsg.Text = "Action performed successfully.";
           
            SetBlank();
            GridBind();
            mView.SetActiveView(vGrid);
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }
    
    private bool ValidateFields()
    {    

        string strQuery = "DECLARE @MSG varchar(100)=''"
                    + " SET @MSG=(SELECT CASE WHEN COUNT(*)>0 THEN 'Email id already exists.' ELSE '' END FROM dbo.brand_user where status=1 AND email='" + txtEmail.Text.Trim() + "'";
        if (!string.IsNullOrWhiteSpace(strUserId))
            strQuery += "  and uid<>" + strUserId;
        strQuery += ")";
        strQuery += " IF @MSG =''"
                    + " BEGIN"
                    + " SET @MSG=(SELECT CASE WHEN COUNT(*)>0 THEN 'Name already exists,please enter another name.' ELSE '' END FROM dbo.brand_user where status=1 AND fullname='" + utils.formatStringForDBInsert(txtName.Text.Trim()) + "'";

        if (!string.IsNullOrWhiteSpace(strUserId))
            strQuery += "  and uid<>" + strUserId;
        strQuery += ")";
        strQuery += " END";
        strQuery += " IF @MSG =''"
                   + " BEGIN"
                   + " SET @MSG=(SELECT CASE WHEN COUNT(*)>0 THEN ' UserName already exists,please enter another Username.' ELSE '' END FROM dbo.brand_user where status=1 AND uname='" + utils.formatStringForDBInsert(txtUserName.Text.Trim()) + "'";

        if (!string.IsNullOrWhiteSpace(strUserId))
            strQuery += "  and uid<>" + strUserId;
        strQuery += ")";
        strQuery += " END"
                    + " SELECT @MSG as MSG";
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0][0])))
        {
            lblMsg.Text = Convert.ToString(dt.Rows[0][0]);
            return false;
        }

             
        return true;
    }
    /// <summary> 
    /// Method to cancel a creating or updating a user 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetBlank();
        mView.SetActiveView(vGrid);
        dvCheckEmail.Style.Value = "display : none;";
    }

    private void SetBlank()
    {
        txtName.Text = "";
        txtEmail.Text = "";
        txtUserName.Text = "";
        ChkbxlstAccessPermiossions.SelectedValue = null;
    }
}