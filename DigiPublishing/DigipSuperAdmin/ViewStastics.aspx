﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ViewStastics.aspx.cs" Inherits="DigipSuperAdmin_ViewStastics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="../JS/Password.js"></script>
    <div>
        <asp:HiddenField ID="pwd" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnUID" runat="server"></asp:HiddenField>
        <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
        <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
            <asp:View ID="Vgrid" runat="server">
                <div style="text-align: right;">
                    <asp:LinkButton ID="lnkExportInfo" runat="server" Text="export site info" OnClick="lnkExportInfo_Click" />
                </div>   
                <asp:DataGrid ID="dgrdDomain" runat="server" AutoGenerateColumns="False" Width="100%"
                    BorderWidth="1px" AllowSorting="True" OnSortCommand="dgrdDomain_SortCommand" OnItemCommand="dgrdDomain_OnItemCommand">
                    <Columns>
                        <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="domainname" HeaderText="Domain" SortExpression="domainname">
                            <ItemStyle Width="10%" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="genre" HeaderText="genre" SortExpression="genre">
                            <ItemStyle Width="20%" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="widgets" HeaderText="Widgets">
                            <ItemStyle Width="20%" />
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Action">
                            <ItemStyle Width="20%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit" CssClass="toplink"></asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="return confirm('Are You Sure To Delete this Site?');" CommandName="Delete" Text="Delete" CssClass="toplink"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Login" ID="lnkLogin" CommandName="Login" CommandArgument='<%# Eval("domainname") %>'
                                    OnCommand="lnkLogin_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div style="display: none;">
                    <asp:DataGrid ID="dgr" runat="server" AutoGenerateColumns="False" Width="100%">
                        <ItemStyle VerticalAlign="Top" />
                        <Columns>
                            <asp:BoundColumn DataField="domainname" HeaderText="Site name" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundColumn DataField="Email" HeaderText="Email" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundColumn DataField="titles" HeaderText="Title" />
                            <asp:BoundColumn DataField="keywords" HeaderText="Meta Keywords" />
                            <asp:BoundColumn DataField="description" HeaderText="Meta Description" />
                            <asp:BoundColumn DataField="site_genre" HeaderText="Site Genre" />
                            <%--<asp:BoundColumn DataField="site_plan" HeaderText="Current site plan" ItemStyle-HorizontalAlign="Center" />--%>
                            <asp:BoundColumn DataField="hd_space" HeaderText="HD used" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundColumn DataField="site_status" HeaderText="Status"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Widget" HeaderText="Widgets"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Tot_User" HeaderText="Total Users"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Tot_BP" HeaderText="Total Blog Post"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Tot_Rss" HeaderText="Total Rss Feeds"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </asp:View>
            <asp:View ID="vDetails" runat="server">
            </asp:View>
            <asp:View ID="ViewEdit" runat="server">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <h4>
                                Manage Agency</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Agency Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubDomain" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Themes
                        </td>
                        <td>
                            <asp:DropDownList ID="drpTheme" runat="server" Width="162px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpTheme" ForeColor="Red"
                                Display="Dynamic" SetFocusOnError="true" ErrorMessage="Select theme" ValidationGroup="ESVCG"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td>
                            Features
                        </td>
                        <td>
                            <br />
                            <table>
                                <tr valign="top">
                                    <td style="width: 200px">
                                        <span style="font-weight: bold">Select Features:</span>
                                        <br />
                                        <asp:CheckBoxList ID="chkFeatures" CssClass="maintext" runat="server">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Widgets
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        Select random widget
                                        <asp:ListBox ID="ddl_random_widgets" runat="server" SelectionMode="Multiple">
                                            <asp:ListItem Value="Select">----Select----</asp:ListItem>
                                            <asp:ListItem Value="random_topheader">Top banner</asp:ListItem>
                                            <asp:ListItem Value="random_topbanner">Top content</asp:ListItem>
                                            <asp:ListItem Value="random_left">Left</asp:ListItem>
                                            <asp:ListItem Value="random_right">Right</asp:ListItem>
                                            <asp:ListItem Value="random_bottom">Bottom</asp:ListItem>
                                            <asp:ListItem Value="random_pop_under">Pop-under</asp:ListItem>
                                            <asp:ListItem Value="random_subline">Subline</asp:ListItem>
                                            <asp:ListItem Value="random_midarticle">Mid Article</asp:ListItem>
                                        </asp:ListBox>
                                        <asp:HiddenField ID="hdnRandomWidgets" runat="server" />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:DataGrid ID="dgrdWidgets" runat="server" AutoGenerateColumns="false" GridLines="none"
                                            ItemStyle-BackColor="white">
                                            <Columns>
                                                <asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkWidgets" Text='<%#Eval("ApplicationName") %>' runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlwidgets" runat="server">
                                                            <asp:ListItem Selected="True" Value="topheader">Top banner</asp:ListItem>
                                                            <asp:ListItem Value="topbanner">Top content</asp:ListItem>
                                                            <asp:ListItem Value="Top_not_user">Top content not for user</asp:ListItem>
                                                            <asp:ListItem Value="leftbottom">Left</asp:ListItem>
                                                            <asp:ListItem Value="righttop">Right</asp:ListItem>
                                                            <asp:ListItem Value="bottomright">Bottom</asp:ListItem>
                                                            <asp:ListItem Value="pop_under">Pop-under</asp:ListItem>
                                                            <asp:ListItem Value="subline">Subline</asp:ListItem>
                                                            <asp:ListItem Value="midarticle">Mid Article</asp:ListItem>
                                                            <asp:ListItem Value="none">None</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            Name</td>
                        <td>
                            <asp:TextBox ID="txtFullName" runat="server" CssClass="maintext"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            E-mail*
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" ForeColor="Red"
                                ErrorMessage="Email could not be left blank" Display="dynamic" SetFocusOnError="true" ValidationGroup="ESVCG">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ForeColor="Red"
                                Display="dynamic" SetFocusOnError="true" ErrorMessage="Email format is not correct" ValidationGroup="ESVCG"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 17%;">
                            Password*
                            <input id="sault" type="hidden" name="sault" runat="server" />
                        </td>
                        <td><script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                            <asp:TextBox ID="txtPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                                runat="server" CssClass="maintext" Width="140" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;
                        </td>
                        <td align="left">
                            <div style="width: 100%; color: red" id="dvPwd">
                            </div>
                            <asp:RequiredFieldValidator ID="rfvPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtPwd" ForeColor="Red"
                                Display="Dynamic" ErrorMessage="please enter password" ValidationGroup="ESVCG"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" SetFocusOnError="true" ForeColor="Red"
                                ControlToValidate="txtPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                                 ValidationGroup="ESVCG" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                     <tr>
                        <td>
                        </td>
                        <td style="text-align:left;">
                            <span id="Words">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td style="width:150px;height:10px;background-color:#dddddd;">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Confirm&nbsp;password*</td>
                        <td>
                            <asp:TextBox ID="txtCPwd" runat="server" CssClass="maintext" Width="140" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;
                        </td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" ForeColor="Red"
                                Display="Dynamic" ErrorMessage="please enter confirm password" ValidationGroup="ESVCG"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" ForeColor="Red" Display="Dynamic" 
                                ErrorMessage="password & confirm password must match" ValidationGroup="ESVCG" ControlToCompare="txtPwd"></asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td>
                            Logo
                        </td>
                        <td>
                            <asp:HiddenField ID="hdnLogo" runat="server" />
                            <asp:FileUpload ID="UploadLogo" runat="server"></asp:FileUpload>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Icon (20x20)
                        </td>
                        <td>
                            <asp:FileUpload ID="Uploadicon20x20" runat="server"></asp:FileUpload>
                        </td>
                    </tr>
                    <tr>
                        <td class="txtDisplay">
                            Site Genre</td>
                        <td>
                            <asp:DropDownList ID="ddlSiteGenre" CssClass="maintext" runat="server" Width="152px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" InitialValue="-1" ValidationGroup="ESVCG" Display="Dynamic" SetFocusOnError="true" 
                                 ForeColor="Red" ControlToValidate="ddlSiteGenre" ErrorMessage="Select site genre"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="txtDisplay">
                            Source</td>
                        <td>
                            <asp:DropDownList ID="ddlSource" CssClass="maintext" runat="server" Width="79px">
                                <asp:ListItem Value="0">Closed</asp:ListItem>
                                <asp:ListItem Value="1">Open</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="txtDisplay">
                            Status</td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" CssClass="maintext" runat="server" Width="79px">
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">In-Active</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="txtDisplay">
                            Add Tracking Code</td>
                        <td>
                            <asp:CheckBox ID="chkTrackingCode" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="txtDisplay">
                            Honeypot email</td>
                        <td>
                            <asp:CheckBox ID="chkHoneypotemail" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td class="txtDisplay" style="height: 21px">
                            Latitude</td>
                        <td style="height: 21px">
                            <asp:TextBox ID="txtLatitude" runat="server" MaxLength="20"></asp:TextBox>
                           <asp:RegularExpressionValidator ID="revLatitude" runat="server" ControlToValidate="txtLatitude" ForeColor="Red" SetFocusOnError="true" Display="dynamic" 
                               ErrorMessage="please enter numeric value only" ValidationExpression="\-?[0-9]*\.?[0-9]*" ValidationGroup="ESVCG"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="txtDisplay" style="height: 21px">
                            Longitude</td>
                        <td style="height: 21px">
                            <asp:TextBox ID="txtLongitude" runat="server" MaxLength="20"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revLongitude" runat="server" ControlToValidate="txtLongitude" ForeColor="Red" SetFocusOnError="true" Display="dynamic" 
                                ErrorMessage="please enter numeric value only" ValidationExpression="\-?[0-9]*\.?[0-9]*" ValidationGroup="ESVCG"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="txtDisplay" style="height: 21px">
                            DBSize</td>
                        <td style="height: 21px">
                            <asp:Label ID="lblSize" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="txtDisplay" style="height: 21px">
                            FileSize</td>
                        <td style="height: 21px">
                            <asp:Label ID="lbFileSize" runat="server"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblmessage" ForeColor="red" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h4>
                                <asp:LinkButton ID="lbDomainName" runat="server" OnClick="lbDomainName_Click" Visible="false"></asp:LinkButton></h4>
                        </td>
                        <td style="text-align:right;">
                            <asp:LinkButton ID="lbBack" runat="server" OnClick="lbBack_Click" Visible="false">Back</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="ESVCG" CssClass="btnBgClass" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btnBgClass" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr /></td>
                    </tr>
                    <tr>
                        <td class="txtDisplay" style="height: 21px">
                            Convert to Full Domain
                        </td>
                        <td>
                            <asp:TextBox ID="txtFulldomainName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvFulldomainName" runat="server" ControlToValidate="txtFulldomainName" Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ErrorMessage="Full Domain is requited" ValidationGroup="vgFulldomainName"></asp:RequiredFieldValidator>
                            <asp:Button ID="btnConvert" runat="server" OnClientClick="return confirm('Are You Sure you want to convert this to full doamin?');" Text="Convert" OnClick="btnConvert_Click" ValidationGroup="vgFulldomainName" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
