﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;
using System.Xml;

public partial class DigipSuperAdmin_Maintenance : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Page.Title = ReadConfig.getDomainUrl() + " - Maintenance ";
        if (!IsPostBack)
        {
            string strsql = "select isnull(Server_Maintainance,0) as Server_Maintainance,VideoImpression,OutboundLink,VideoAdStart,VideoAdFinish,BannerClick,VideoAdClick from brand_Record_Data";
            DataTable dt = dp.FillDataTable(strsql);
            if (dt.Rows.Count > 0)
            {
                chkServerUpdate.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["Server_Maintainance"]));
                chkVideoImpression.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["VideoImpression"]));
                chkOutboundLink.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["OutboundLink"]));
                chkVideoAdStart.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["VideoAdStart"]));
                chkVideoAdFinish.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["VideoAdFinish"]));
                chkBannerClick.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["BannerClick"]));
                chkVideoAdClick.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["VideoAdClick"]));
            }
            PopulateDomainList(lstDomain);
            PopulateDomainList(lstDomainBookmarks);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string strsql = "";
            if (chkServerUpdate.Checked)
                strsql = "update brand_Record_Data set Server_Maintainance=1";
            else
                strsql = "update brand_Record_Data set Server_Maintainance=0";
            dp.ExecuteNonQuery(strsql);

            //For All sites
            try
            {
                string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=dtserverMaintainance";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //For demo sites(if those are is separate application pool)
                string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=dtserverMaintainance";
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch (Exception ex) {
                throw new Exception("Error", ex);
            }

            lblMsg.Text = "saved successfully.";
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }


    private void PopulateDomainList(ListBox lst)
    {
        string strdomain = "Select domainname as name, id as value from brand_domainname where status<>2";
        DataTable dtdomain = dp.FillDataTable(strdomain);
        lst.DataSource = dtdomain;
        lst.DataTextField = "name";
        lst.DataValueField = "value";
        lst.DataBind();
        lst.Items.Insert(0, new ListItem("--Select--", "-1"));
        lst.SelectedValue = "-1";
    }

    protected void btnSubmit_Feeds_Click(object sender, EventArgs e)
    {
        PopulateRssFeeds();
    }

    private void PopulateRssFeeds()
    {
        try
        {
            DataTable dtFeeds = new DataTable();
            dtFeeds.Columns.Add("feed_id", typeof(string));
            dtFeeds.Columns.Add("title", typeof(string));
            dtFeeds.Columns.Add("link", typeof(string));
            dtFeeds.Columns.Add("DomainName", typeof(string));
            dtFeeds.Columns.Add("ResponseText", typeof(string));
            dtFeeds.Columns.Add("ResponseTime", typeof(string));
            string[] arrDomain = utils.GetSelectedListBoxItems(lstDomain).Split(',');
            foreach (string str in arrDomain)
            {
                if (str != "-1")
                {
                    string sitename = lstDomain.Items.FindByValue(str).Text;
                    DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(sitename).Trim());
                    string strRss = "Select feed_id, title, link from brand_rss WHERE feed_status<>'0' ORDER BY SortOrder DESC";
                    DataTable dtrss = dpsite.FillDataTable(strRss);
                    if (dtrss.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtrss.Rows)
                        {
                            DataRow drNew = dtFeeds.NewRow();
                            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
                            try
                            {
                                drNew["DomainName"] = sitename;
                                drNew["feed_id"] = Convert.ToString(dr["feed_id"]);
                                drNew["title"] = Convert.ToString(dr["title"]);
                                drNew["link"] = Convert.ToString(dr["link"]);

                                string title = utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dr["Title"]))).TrimEnd('.').Replace('-', ' ');

                                DBCommand strQuery = new DBCommand();
                                strQuery.CommandType = CommandType.StoredProcedure;
                                strQuery.CommandText = "SP_Brand_Select_Top1_Feed_Brand_Rss";
                                strQuery.AddParameter("@title", utils.formatStringForDBInsert(title));
                                DataTable dt = new DataTable();
                                dt = dpsite.FillDataTable(strQuery);
                                if (dt.Rows.Count > 0)
                                {
                                    string url = dr["link"].ToString();
                                    timer.Start();

                                    try
                                    {
                                        XmlDocument xd = new XmlDocument();
                                        using (var webpage = new WebClient())
                                        {
                                            try
                                            {
                                                webpage.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
                                                byte[] data = webpage.DownloadData(url);
                                                xd.Load(new System.IO.MemoryStream(data));
                                            }
                                            catch (Exception)
                                            {
                                                //Create a WebRequest and WebResponse for get the Details.
                                                ServicePointManager.Expect100Continue = true;
                                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                                HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(url);
                                                HttpWReq.Timeout = 20000; // Time out is set for 40 secs to bypass the default time out which is 100,000 milliseconds (100 seconds)
                                                HttpWebResponse HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();

                                                //Load the Stream to XmlDocument
                                                xd.Load(HttpWResp.GetResponseStream());

                                                HttpWResp.Close();
                                                HttpWResp = null;
                                                HttpWReq = null;
                                            }

                                            //Select the rss/channel/item
                                            XmlNodeList xnl = xd.SelectNodes("rss/channel/item");

                                            //Select the feed/entry/
                                            if (xnl.Count == 0)
                                            {
                                                XmlNamespaceManager mngr = new XmlNamespaceManager(xd.NameTable);
                                                mngr.AddNamespace("p", "http://www.w3.org/2005/Atom");
                                                xnl = xd.DocumentElement.SelectNodes("//p:entry", mngr);
                                            }

                                            if (xnl.Count == 0)
                                                drNew["ResponseText"] = "<span style='color:Red;'>No item node in xml.</span>";
                                            else
                                                drNew["ResponseText"] = "Okay";
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        drNew["ResponseText"] = "<span style='color:Red;'>"+ex.Message+"</span>";
                                    }

                                    timer.Stop();
                                    drNew["ResponseTime"] = timer.Elapsed.ToString();
                                }
                                else
                                {
                                    drNew["ResponseText"] = "<span style='color:Red;'>Title encoding is not working properly.</span>";
                                }
                            }
                            catch (Exception er)
                            {
                                drNew["ResponseText"] = "<span style='color:Red;'>" + er.Message + "</span>";
                                timer.Stop();
                                drNew["ResponseTime"] = timer.Elapsed.ToString();
                            }

                            dtFeeds.Rows.Add(drNew);
                        }
                    }
                }
            }
            dgrdRss.DataSource = dtFeeds;
            dgrdRss.DataBind();
            if (dtFeeds.Rows.Count > 0)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    private void ClearCacheRssFeeds()
    {
        try
        {
            string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=rsssummary";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=rsssummary";
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch { }
    }

    protected void dgrdRss_OnItemCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "Update")
            {
                string sitename = e.Item.Cells[2].Text.ToString();
                DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(sitename).Trim());

                TextBox txtRssTitle = (TextBox)e.Item.FindControl("txtRssTitle");
                TextBox txtRssLink = (TextBox)e.Item.FindControl("txtRssLink");
                string strSql = "UPDATE Brand_Rss SET link='" + txtRssLink.Text + "',Title='" + utils.formatStringForDBInsert(txtRssTitle.Text.Trim()) + "' WHERE feed_id=" + e.Item.Cells[1].Text;
                dpsite.ExecuteNonQuery(strSql);
                ClearCacheRssFeeds();
                lblMsg.Text = "RSS Feed URL updated successfully.";
            }
        }
    }

    protected void DeleteRssFeeds(object sender, EventArgs e)
    {
        foreach (DataGridItem dgrdItem in dgrdRss.Items)
        {
            if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
            {
                string sitename = dgrdItem.Cells[2].Text;
                DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(sitename).Trim());
                CheckBox chkDelete =(CheckBox) dgrdItem.FindControl("chkDelete");
                if (chkDelete != null && chkDelete.Checked)
                {
                    string strSql = "DELETE FROM Brand_Rss WHERE feed_id=" + dgrdItem.Cells[1].Text;
                    dpsite.ExecuteNonQuery(strSql);
                }
            }
        }
        ClearCacheRssFeeds();
        lblMsg.Text = "Selected RSS Feed(s) deleted successfully.";
        PopulateRssFeeds();
    }

    protected void btnGAEvents_Click(object sender, EventArgs e)
    {
        try
        {
            int VideoImpression = 0;
            int OutboundLink = 0;
            int VideoAdStart = 0;
            int VideoAdFinish = 0;
            int BannerClick = 0;
            int VideoAdClick = 0;
            if (chkVideoImpression.Checked)
            {
                VideoImpression = 1;
            }
            if (chkOutboundLink.Checked)
            {
                OutboundLink = 1;
            }
            if (chkVideoAdStart.Checked)
            {
                VideoAdStart = 1;
            }
            if (chkVideoAdFinish.Checked)
            {
                VideoAdFinish = 1;
            }
            if (chkBannerClick.Checked)
            {
                BannerClick = 1;
            }
            if (chkVideoAdClick.Checked)
            {
                VideoAdClick = 1;
            }
            string strsql = "update brand_Record_Data set VideoImpression=" + VideoImpression + ",OutboundLink=" + OutboundLink
                + ",VideoAdStart=" + VideoAdStart + ",VideoAdFinish=" + VideoAdFinish + ",BannerClick=" + BannerClick
                + ",VideoAdClick=" + VideoAdClick;
            dp.ExecuteNonQuery(strsql);

            //For All sites
            try
            {
                string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=dtserverMaintainance";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //For demo sites(if those are is separate application pool)
                string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=dtserverMaintainance";
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch (Exception ex) { }

            lblMsg.Text = "saved successfully.";
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    protected void btnSubmit_Bookmarks_Click(object sender, EventArgs e)
    {
        PopulateBookmarks();
    }

    private void PopulateBookmarks()
    {
        try
        {
            DataTable dtLinks = new DataTable();
            dtLinks.Columns.Add("link_id", typeof(string));
            dtLinks.Columns.Add("desc", typeof(string));
            dtLinks.Columns.Add("link", typeof(string));
            dtLinks.Columns.Add("DomainName", typeof(string));
            dtLinks.Columns.Add("ResponseText", typeof(string));
            dtLinks.Columns.Add("ResponseTime", typeof(string));
            string[] arrDomain = utils.GetSelectedListBoxItems(lstDomainBookmarks).Split(',');
            foreach (string str in arrDomain)
            {
                if (str != "-1")
                {
                    string sitename = lstDomainBookmarks.Items.FindByValue(str).Text;
                    DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(sitename).Trim());
                    string strLinks = "Select Id, BK_Title, BK_URL,BK_Desc from brand_bookmark WHERE Status='True' AND BK_Status<>'0' ORDER BY BK_SortOrder DESC";
                    DataTable dtlink = dpsite.FillDataTable(strLinks);
                    if (dtlink.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtlink.Rows)
                        {
                            DataRow drNew = dtLinks.NewRow();
                            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
                            try
                            {
                                drNew["DomainName"] = sitename;
                                drNew["link_id"] = Convert.ToString(dr["Id"]);
                                drNew["desc"] = Convert.ToString(dr["BK_Desc"]);
                                drNew["link"] = Convert.ToString(dr["BK_URL"]);

                                string url = dr["BK_URL"].ToString();
                                timer.Start();

                                try
                                {
                                    using (var webpage = new WebClient())
                                    {
                                        try
                                        {
                                            webpage.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
                                            byte[] data = webpage.DownloadData(url);
                                        }
                                        catch (Exception)
                                        {
                                            //Create a WebRequest and WebResponse for get the Details.
                                            HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(url);
                                            HttpWReq.Timeout = 20000; // Time out is set for 40 secs to bypass the default time out which is 100,000 milliseconds (100 seconds)
                                            HttpWebResponse HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();

                                            //Load the Stream to XmlDocument
                                            HttpWResp.GetResponseStream();

                                            HttpWResp.Close();
                                            HttpWResp = null;
                                            HttpWReq = null;
                                        }
                                        drNew["ResponseText"] = "Okay";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    drNew["ResponseText"] = "<span style='color:Red;'>" + ex.Message + "</span>";
                                }

                                timer.Stop();
                                drNew["ResponseTime"] = timer.Elapsed.ToString();
                            }
                            catch (Exception er)
                            {
                                drNew["ResponseText"] = "<span style='color:Red;'>" + er.Message + "</span>";
                                timer.Stop();
                                drNew["ResponseTime"] = timer.Elapsed.ToString();
                            }

                            dtLinks.Rows.Add(drNew);
                        }
                    }
                }
            }
            dgrdBookmarks.DataSource = dtLinks;
            dgrdBookmarks.DataBind();
            if (dtLinks.Rows.Count > 0)
                btnDeleteBookmark.Visible = true;
            else
                btnDeleteBookmark.Visible = false;
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    protected void dgrdBookmarks_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "Update")
            {
                string sitename = e.Item.Cells[2].Text.ToString();
                DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(sitename).Trim());

                TextBox txtBookmarkDesc = (TextBox)e.Item.FindControl("txtBookmarkDesc");
                TextBox txtBookmarkLink = (TextBox)e.Item.FindControl("txtBookmarkLink");
                string strSql = "UPDATE brand_bookmark SET BK_URL='" + txtBookmarkLink.Text + "',BK_Desc='" + utils.formatStringForDBInsert(txtBookmarkDesc.Text.Trim()) + "' WHERE Id=" + e.Item.Cells[1].Text;
                dpsite.ExecuteNonQuery(strSql);
                lblMsg.Text = "Link URL updated successfully.";
            }
        }
    }

    protected void btnDeleteBookmark_Click(object sender, EventArgs e)
    {
        foreach (DataGridItem dgrdItem in dgrdBookmarks.Items)
        {
            if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
            {
                string sitename = dgrdItem.Cells[2].Text;
                DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(sitename).Trim());
                CheckBox chkDelete = (CheckBox)dgrdItem.FindControl("chkDelete");
                if (chkDelete != null && chkDelete.Checked)
                {
                    string strSql = "DELETE FROM brand_bookmark WHERE Id=" + dgrdItem.Cells[1].Text;
                    dpsite.ExecuteNonQuery(strSql);
                }
            }
        }
        lblMsg.Text = "Selected Bookmark Link(s) deleted successfully.";
        //Clear Links Cache
        string Key = "bookmarksummary-" + ReadConfig.getDomainUrl();
        if (Cache[Key] != null)
            Cache.Remove(Key);
        PopulateBookmarks();
    }
}