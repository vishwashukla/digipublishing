﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ClickReport.aspx.cs" Inherits="DigipSuperAdmin_ClickReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="Datepicker.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.js"></script>
    <script type="text/javascript">
        <%--$(function () {
            $('#<%=txtDate.ClientID%>').datepicker({
                minDate: parseInt('<%=MaxDays %>'),
                maxDate: 0,
            }).datepicker('setDate', new Date());
        });--%>

        function confirmSubmit() {
            if (confirm("Are you want to continue?")) {
                document.getElementById('<%=btnRecorddata.ClientID %>').click();
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <div style="vertical-align: top;">
        <h4>Click Report</h4>

        <div style="display: none;">
            <asp:Button ID="btnRecorddata" runat="server" OnClick="btnRecorddata_Click" />
        </div>
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                </td>
                <td style="text-align: right;">
                    <asp:CheckBox ID="chkUserAgent" runat="server" Text="Turn ON/OFF UserAgent Detection" onclick="return confirmSubmit()" />
                    &nbsp;&nbsp;
                    <%--<b>Date: </b><asp:TextBox ID="txtDate" runat="server"></asp:TextBox><b> (mm/dd/yyyy)</b>--%>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    <asp:Button ID="btnPurge" runat="server" Text="Purge Results" OnClick="btnPurge_Click" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="right">
                    <table>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Current server date time:
                            </td>
                            <td style="text-align: right; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblCurrentServerDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Last purge server date time:
                            </td>
                            <td style="text-align: left; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblLastPurgeDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Filter Result</h4>
                </td>
                <td>
                    <asp:DropDownList ID="drpType" runat="server"
                        AutoPostBack="true" OnSelectedIndexChanged="drpType_SelectedIndexChanged">
                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                        <asp:ListItem Value="ad" Text="Ad Networks"></asp:ListItem>
                        <asp:ListItem Value="banner" Text="Banners"></asp:ListItem>
                        <asp:ListItem Value="Campaign" Text="Campaign"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="drpList" runat="server" Visible="false"
                        AutoPostBack="true" OnSelectedIndexChanged="drpList_SelectedIndexChanged1">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DataGrid ID="dgrdClickReport" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderWidth="3px" Font-Size="Smaller" BorderColor="#D8D8D8" BorderStyle="Solid"
                        Width="100%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                        OnSortCommand="dgrdClickReport_SortCommand" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" OnItemDataBound="dgrdClickReport_ItemDataBound">
                        <Columns>
                            <asp:BoundColumn HeaderText="Block_Status" DataField="Block_Status" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Widget & Video Ad Networks" DataField="widget_name"
                                SortExpression="widget_name"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="User IP" DataField="ipaddress" SortExpression="ipaddress"></asp:BoundColumn>

                            <asp:BoundColumn HeaderText="Same IP Clicks In 24 Hours" DataField="widgetclick"
                                SortExpression="widgetclick"></asp:BoundColumn>

                            <asp:BoundColumn HeaderText="Same IP Clicks In 30 days" DataField="widgetclick30days"
                                SortExpression="widgetclick30days"></asp:BoundColumn>

                            <asp:BoundColumn HeaderText="DateTime Of Click" DataField="ClickDateTime" SortExpression="ClickDateTime"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Domain" DataField="digip_domain"
                                SortExpression="digip_domain"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Referer URL" DataField="Referer_URL" SortExpression="Referer_URL" ItemStyle-Width=""></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Campaign" DataField="Campaign" SortExpression="Campaign"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="UserAgent" DataField="UserAgent" SortExpression="UserAgent"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

