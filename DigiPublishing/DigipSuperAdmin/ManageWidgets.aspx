﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" 
    CodeFile="ManageWidgets.aspx.cs" Inherits="DigipSuperAdmin_ManageWidgets" ValidateRequest="false" %>
<%--<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../js/dispalydynamictextbox.js"></script>
    <script type="text/javascript" src="../js/WidgetsAndAdNetwork.js"></script>
    <script type="text/javascript" src="../js/WidgetsAndAdNetwork.js"></script>
    <script src="../JS/jquery.3.1.1.min.js"></script>
    <script src="../JS/bootstrap.min.3.3.7.js"></script>
    <script>
        function ClickTestLink(id, dId) {
            $("#<%=HdnfieldDatatoggle.ClientID %>").val(dId);
            $("#<%=hdnWidgetId.ClientID %>").val(id);
            $("#<%=drpTestSites.ClientID %>").val("--Select--");

        }
       $(document).ready(function () {
            $("#btnTest").click(function () {
                var domain = $("#<%=drpTestSites.ClientID %>").val();
                if (domain == "--Select--") {
                    alert("Please choose domain for testing.");
                    return false;
                }
                $("#<%=hdntestDomain.ClientID %>").val(domain);
                document.getElementById("<%=btnTestServer.ClientID %>").click();
                return true;

            });
       });
       

    </script>
    <style>
        .modal-backdrop {
          position: fixed;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          z-index: 1040;
          background-color: #000000;
        }
        .modal-backdrop.fade {
          opacity: 0;
        }
        .modal-backdrop,
        .modal-backdrop.fade.in {
          opacity: 0.8;
          filter: alpha(opacity=80);
        }
        .modal {
          position: fixed;
          top: 10%;
          left: 50%;
          z-index: 1050;
          width: 560px;
          margin-left: -280px;
          background-color: #ffffff;
          border: 1px solid #999;
          border: 1px solid rgba(0, 0, 0, 0.3);
          *border: 1px solid #999;
          /* IE6-7 */

          -webkit-border-radius: 6px;
          -moz-border-radius: 6px;
          border-radius: 6px;
          -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          -webkit-background-clip: padding-box;
          -moz-background-clip: padding-box;
          background-clip: padding-box;
          outline: none;
        }
        .modal.fade {
          -webkit-transition: opacity .3s linear, top .3s ease-out;
          -moz-transition: opacity .3s linear, top .3s ease-out;
          -o-transition: opacity .3s linear, top .3s ease-out;
          transition: opacity .3s linear, top .3s ease-out;
          top: -25%;
        }
        .modal.fade.in {
          top: 10%;
        }
        .modal-header {
          padding: 9px 15px;
          border-bottom: 1px solid #eee;
        }
        .modal-header .close {
          margin-top: 2px;
        }
        .modal-header h3 {
          margin: 0;
          line-height: 30px;
        }
        .modal-body {
          position: relative;
          overflow-y: auto;
          max-height: 400px;
          padding: 15px;
        }
        .modal-form {
          margin-bottom: 0;
        }
        .modal-footer {
          padding: 14px 15px 15px;
          margin-bottom: 0;
          text-align: right;
          background-color: #f5f5f5;
          border-top: 1px solid #ddd;
          -webkit-border-radius: 0 0 6px 6px;
          -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
          -webkit-box-shadow: inset 0 1px 0 #ffffff;
          -moz-box-shadow: inset 0 1px 0 #ffffff;
          box-shadow: inset 0 1px 0 #ffffff;
          *zoom: 1;
        }
        .modal-footer:before,
        .modal-footer:after {
          display: table;
          content: "";
          line-height: 0;
        }
        .modal-footer:after {
          clear: both;
        }
        .modal-footer .btn + .btn {
          margin-left: 5px;
          margin-bottom: 0;
        }
        .modal-footer .btn-group .btn + .btn {
          margin-left: -1px;
        }
        .modal-footer .btn-block + .btn-block {
          margin-left: 0;
        }

        @media (max-width: 767px) {
          .modal {
            position: fixed;
            top: 20px;
            left: 20px;
            right: 20px;
            width: auto;
            margin: 0;
          }
          .modal.fade {
            top: -100px;
          }
          .modal.fade.in {
            top: 20px;
          }
        }
        .labeldate{
            text-align:left;
            padding-left:10px;
        }
    </style>

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog">
             <asp:HiddenField runat="server" ID="HdnfieldDatatoggle" />  
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>Choose Domain</h4>
            </div>
            <div class="modal-body">
                <asp:HiddenField ID="hdnWidgetId" runat="server" />
                <asp:HiddenField ID="hdntestDomain" runat="server" />
                <div>
                    <asp:DropDownList ID="drpTestSites" runat="server" Width="225px"></asp:DropDownList>
                    <br />
                    <br />
                    <br /> 
                    <input type="button" value="Test" id="btnTest" data-dismiss="modal" />
                    <div style="display:none;">
                        <asp:Button ID="btnTestServer" runat="server" Text="Test" OnClick="btnTestServer_Click" />
                    </div>
                </div>
            </div>
          </div>
      
        </div>
      </div>

    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="mvManageComp" runat="server">
        <asp:View ID="viewComponent" runat="server">
            <div>
                <table style="width:100%;">
                    <tr>
                        <td>
                            <h4>
                                Manage Widgets
                            </h4>
                        </td>
                        <td style="text-align:right;">
                            <asp:LinkButton ID="lnkAddComponent" runat="server" OnClick="lnkAddComponent_Click">Add new widget </asp:LinkButton>|
                            <asp:LinkButton ID="lnkExportInfo" runat="server" Text="export widget info" OnClick="lnkExportInfo_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right;padding-bottom:10px;">
                            <span>Last saved date : </span>
                            <asp:Label ID="lbldate1" runat="server" CssClass="labeldate" Width="120px"></asp:Label>&nbsp;&nbsp;
                            <asp:Button ID="btnsave1" runat="server" Text="Save all settings" OnClick="btnsave_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnrestore1" runat="server" Text="Restore last saved settings" OnClick="btnrestore_Click" OnClientClick="javascript: return confirm('Are you sure you want to restore last saved settings?');" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate1" runat="server" Text="Update" OnClick="btnUpdate_OnClick" ValidationGroup="vgwidgetlist" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dgrdComponent" runat="server" GridLines="none" AutoGenerateColumns="False" Width="795px" BorderWidth="1" AllowSorting="true" 
                                OnItemDataBound="dgrdComponent_ItemDataBound" OnSortCommand="dgrdComponent_SortCommand" OnItemCommand="dgrdComponent_OnItemCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Application Name" SortExpression="ApplicationName">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkApplicationame" runat="server" CommandName="Edit" Text='<%#Eval("ApplicationName") %>'
                                                CssClass="toplink" CausesValidation="false"></asp:LinkButton>
                                            <asp:HiddenField ID="hdnRowColor" Value='<%#Eval("RowColor") %>' runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="25%" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Click">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkStatusClick" runat="server" Checked='<%#Convert.ToBoolean(Eval("Blocker")) %>'></asp:CheckBox>&nbsp;&nbsp;
                                            <asp:TextBox ID="txtValueClick" runat="server" Width="30px" Text='<%#Eval("BlockerValue") %>'></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtValueClick" runat="server" ErrorMessage="Numbers Only" 
                                                ValidationExpression="\d+" ValidationGroup="vgwidgetlist" ForeColor="Red"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Click IP">
                                        <ItemTemplate>
                                            <div style="display:inline-block;vertical-align:top;">
                                                <asp:CheckBox ID="chkStatusClickIP" runat="server" Checked='<%#Convert.ToBoolean(Eval("SameIP_Status")) %>'></asp:CheckBox>&nbsp;&nbsp;
                                            </div>
                                            <div style="display:inline-block">
                                                <asp:TextBox ID="txtValueClickIP" runat="server" Width="30px" Text='<%#Eval("SameIP_Value") %>'></asp:TextBox><br />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtValueClickIP" runat="server" ErrorMessage="Numbers Only" 
                                                    ValidationExpression="\d+" ValidationGroup="vgwidgetlist" ForeColor="Red"></asp:RegularExpressionValidator>
                                            </div>
                                            <div style="display:inline-block">
                                                <asp:TextBox ID="txtValueClickIPDays" runat="server" Width="30px" Text='<%#Eval("SameIP_Days") %>'></asp:TextBox> days<br />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtValueClickIPDays" runat="server" ErrorMessage="Numbers Only" 
                                                    ValidationExpression="\d+" ValidationGroup="vgwidgetlist" ForeColor="Red"></asp:RegularExpressionValidator>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Position">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddl_Position" runat="server" Width="145px">
                                                <asp:ListItem Value="">----Default Position----</asp:ListItem>
                                                <asp:ListItem Value="topheader">Top banner</asp:ListItem>
                                                <asp:ListItem Value="topbanner">Top content</asp:ListItem>
                                                <asp:ListItem Value="leftbottom">Left</asp:ListItem>
                                                <asp:ListItem Value="righttop">Right</asp:ListItem>
                                                <asp:ListItem Value="bottomright">Bottom</asp:ListItem>
                                                <asp:ListItem Value="pop_under">Pop-under</asp:ListItem>
                                                <asp:ListItem Value="subline">Subline</asp:ListItem>
                                                <asp:ListItem Value="midarticle">Mid Article</asp:ListItem>
                                                <asp:ListItem Value="none">None</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRemove" runat="server" Text="Remove" OnClientClick="javascript:return confirm('Do you want to remove this widget from all the sites?')" 
                                                CommandName="Remove" />&nbsp;|&nbsp;
                                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Delete" CssClass="toplink"></asp:LinkButton>&nbsp;|&nbsp;
                                            <a data-toggle="modal" href="#" data-target="#myModal" id="btnTest" onclick='ClickTestLink("<%#Eval("ID") %>","btnTest","")'>Test</a>&nbsp;|&nbsp;
                                            <a data-toggle="modal" href="#" data-target="#myModal" id="btnASPX" onclick='ClickTestLink("<%#Eval("ID") %>", "btnASPX","<%#Eval("aspxUrl") %>")'>Permalink</a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="20%" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Sites" HeaderText="Sites" Visible="false"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right;padding-top:10px;">
                            <span>Last saved date : </span>
                            <asp:Label ID="lbldate2" runat="server" CssClass="labeldate" Width="120px"></asp:Label>&nbsp;&nbsp;
                            <asp:Button ID="btnsave2" runat="server" Text="Save all settings" OnClick="btnsave_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnrestore2" runat="server" Text="Restore last saved settings" OnClick="btnrestore_Click" OnClientClick="javascript: return confirm('Are you sure you want to restore last saved settings?');" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate2" runat="server" Text="Update" OnClick="btnUpdate_OnClick" ValidationGroup="vgwidgetlist" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:View>
        <asp:View ID="viewAddComp" runat="server">
            <img alt="blank" src="../images_common/blank.jpg" onload="javascript:showaddElement('<%=strID%>','<%=objvalue%>');" />
            <table style="width:100%;">
                <tr>
                    <td colspan="2">
                        <h4>
                            <asp:Label ID="headerText" Text="Add Widget" runat="server"></asp:Label>
                            <asp:HiddenField ID="hdnCompID" runat="server" Value="-1" />
                            <asp:HiddenField ID="hdnValues" runat="server" />
                            <asp:HiddenField ID="addMore" Value="0" runat="server" />
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 30%">
                                    Application Name
                                </td>
                                <td style="height: 24px; width: 20%">
                                    <asp:TextBox ID="txtApplicationName" CssClass="maintext" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUserName" ControlToValidate="txtApplicationName" ForeColor="Red" Display="Dynamic"
                                        runat="server" ErrorMessage="required" Text="required" ValidationGroup="vgwidgetaddedit"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 20%">
                                </td>
                                <td style="width: 20%">
                                </td>
                                <td style="width: 20%">
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 30%">
                                    CodeBox
                                </td>
                                <td colspan="3">
                                    <%--<div id="fckeditor">
                                        <FCKeditorV2:FCKeditor ID="txtCodebox" OnPreRender="FCKDesc_PreRender" runat="server" OnLoad="txtCodebox_Load">
                                        </FCKeditorV2:FCKeditor>
                                    </div>--%>
                                    <textarea runat="server" id="txtCodebox" style="width:650px;height:250px;"></textarea>
                                </td>
                            </tr>
                            <tr>
								<td></td>
								<td>
									http://DomainName = [Host_Url] <br />
                                    [8 Digit random number] = [CACHEBUSTER]<br />
                                    IP = [ip]<br />
                                    useragent = [ua]<br />
                                    http://DomainName/page_url = [page_url]
								</td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Block After
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <table>
                                        <tr>
                                            <td rowspan="2">
                                                <asp:CheckBox ID="CheckBlock1" runat="server" AutoPostBack="false" /> 
                                                Status:
                                            </td>
                                            <td rowspan="2">
                                                <div id="div2" runat="server" style="display: block;">
                                                    Value:
                                                    <asp:TextBox ID="TextCheckBlock1" runat="server" Width="30" Text="1"></asp:TextBox>
                                                    Click
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Block Click by same IP after
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <table>
                                        <tr>
                                            <td rowspan="2">
                                                Status:
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkSameIPBlocker" runat="server" />
                                            </td>
                                            <td rowspan="2">
                                            </td>
                                            <td rowspan="2">
                                                <div id="divtxtSameIPBlocker" runat="server" style="display: block;">
                                                    Value:
                                                    <asp:TextBox ID="txtSameIPBlocker" runat="server" Text="1" Width="30"></asp:TextBox>times
                                                    within <asp:TextBox ID="txtSameIPDays" runat="server" Text="1" Width="30"></asp:TextBox> Day(s)
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Width x Height (example: 300x250)
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <asp:TextBox ID="txtWidthxHeight" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revWidthxHeight" runat="server" ControlToValidate="txtWidthxHeight" 
                                        ErrorMessage="Please enter width and height into correct format. (example: 300x250)" ValidationGroup="vgwidgetaddedit" 
                                        ToolTip="(example: 300x250)"  ForeColor="Red" Display="Dynamic"  ValidationExpression="^([0-9]{1,4})[x]([0-9]{1,4})$">
                                    </asp:RegularExpressionValidator> 
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 24px; width: 30%">
                                </td>
                                <td style="width: 100%">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 25%;text-align:left;">
                                                Video Url
                                            </td>
                                            <td style="width: 25%;text-align:left;">
                                                ClickThrough Url
                                            </td>
                                            <td style="width: 25%;text-align:left;">
                                                Impression Url
                                            </td>
                                            <td style="width: 28%">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px;">
                                    Vast
                                </td>
                                <td style="width: 100%">
                                    <div id="showexDiv" runat="server">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 25%">
                                                    <asp:TextBox ID="txtVideo" Text="http://example.flv" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:TextBox ID="txtClickThrough" Text="http://www.example.com" CssClass="maintext"
                                                        runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:TextBox ID="txtImpressions" Text="http://www.example.jpg" CssClass="maintext"
                                                        runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 28%">
                                                    <a href="javascript:;" onclick="addElement();">AddMore</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="showmyDiv" runat="server">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 28%">
                                                    <a href="javascript:;" onclick="addElement();">AddMore</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="4">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <div id="myDiv">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Targeting options
                                </td>
                                <td style="height: 24px">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdbBrowserAll" runat="server" GroupName="rdbBrowser" Text="Target All Browsers" />
                                                <asp:RadioButton ID="rdbBrowserSel" runat="server" GroupName="rdbBrowser" Text="Target Selected Browsers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lstBrowsers" runat="server" SelectionMode="Multiple" Height="100"
                                                    Width="150"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Referal Targeting Options
                                </td>
                                <td style="height: 24px">
                                    <span style="padding-right: 18px;">Show Only If Referal From</span>
                                    <asp:TextBox ID="txtReferalfrom" runat="server"></asp:TextBox><br />
                                    <span style="padding-right: 5px;">Do Not Show If Referal From</span>
                                    <asp:TextBox ID="txtNotReferalfrom" runat="server"></asp:TextBox>
                                    <div style="color: Gray;display:inline;">(Insert comma seperated values)</div>
                                    <br /><br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Operating System (OS)
                                </td>
                                <td>
                                    <asp:ListBox ID="lstOS" runat="server" SelectionMode="Multiple" Width="140px" Height="100px">
                                        <asp:ListItem Value="-1" Text="----Select OS-----"></asp:ListItem>
                                        <asp:ListItem Value="windows" Text="Windows"></asp:ListItem>
                                        <asp:ListItem Value="macintosh" Text="Macintosh"></asp:ListItem>
                                        <asp:ListItem Value="linux" Text="Linux"></asp:ListItem>
                                        <asp:ListItem Value="ios" Text="iOS"></asp:ListItem>
                                        <asp:ListItem Value="android" Text="Android"></asp:ListItem>
                                        <asp:ListItem Value="other" Text="Other"></asp:ListItem>
                                    </asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Region/Country
                                </td>
                                <td>
                                    <div id="divtree" runat="server" style="border: 1px double; width: 200px; height: 150px;
                                        overflow-y: scroll;">
                                        <asp:TreeView ID="TreeView1" runat="server" ExpandDepth="0" ShowLines="True" LineImagesFolder="~/Images_Common/TreeLineImages"
                                            ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event);">
                                        </asp:TreeView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Default Location
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <asp:DropDownList ID="ddl_DefaultLocation" runat="server" Width="145px">
                                        <asp:ListItem Value="">----Default Position----</asp:ListItem>
                                        <asp:ListItem Value="topheader">Top banner</asp:ListItem>
                                        <asp:ListItem Value="topbanner">Top content</asp:ListItem>
                                        <asp:ListItem Value="leftbottom">Left</asp:ListItem>
                                        <asp:ListItem Value="righttop">Right</asp:ListItem>
                                        <asp:ListItem Value="bottomright">Bottom</asp:ListItem>
                                        <asp:ListItem Value="pop_under">Pop-under</asp:ListItem>
                                        <asp:ListItem Value="subline">Subline</asp:ListItem>
                                        <asp:ListItem Value="midarticle">Mid Article</asp:ListItem>
                                        <asp:ListItem Value="none">None</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddl_DefaultLocation" ForeColor="Red" Display="Dynamic"
                                        runat="server" ErrorMessage="Select Default Position" Text="Select Default Position" ValidationGroup="vgwidgetaddedit"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Genre
                                </td>
                                <td>
                                    <asp:ListBox ID="lstGenre" runat="server" SelectionMode="Multiple" Width="225px"
                                        Height="125px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px">
                                    Sites
                                </td>
                                <td>
                                    <asp:ListBox ID="lstSites" runat="server" SelectionMode="Multiple" Height="200" Width="220">
                                    </asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px">
                                </td>
                                <td style="height: 24px" >
                                    <div id="divbtn" runat="server" style="display: none;">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="vgwidgetaddedit" OnClientClick="javascript:GetchildControlValues();" />
                                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CssClass="btnBgClass" CausesValidation="false"></asp:Button>
                                    </div>
                                </td>                               
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

