﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;

public partial class DigipSuperAdmin_Approve : System.Web.UI.Page
{
    DataTable dt;
    public string sText = "";
    public string str3 = "";
    public int pageno = 1;
    public string strOrder = "Desc";
    public StringBuilder strHtml = new StringBuilder();
    string strsql_sort = "";
    string strSql = "";
    bool blnisEdit = false;
    bool blnEditMode = false;
    string strsellanguage = "";
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, System.EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Videos";
        string strSearch = "";
        string strEditUrl = "";
        string img = "";
        string stus_dis = "";
        int intPagesize = 20;
        int nopages = 0;
        int recCount = 0;
        string strSql1 = "";
        int intRegionId = 0;
        int intCountryId = 0;
        string strRegionName = null;
        string strCountryName = null;
        int intTop1 = intPagesize;
        int intTop2 = 0;
        int intChannel_id = 0;
        string strPlayOption = "";
        string strOptStramUrl = " SELECT ";
        string strOptPlayList = " ";
        string strOptPlayListFile = " ";
        string ucstreamurl1 = "";
        string ucchannelurl1 = "";
        string strtxtchanneltags = "";
        string channelLanguage = "";

        if (!string.IsNullOrWhiteSpace(Request.QueryString["id"]))
        {
            intChannel_id = Convert.ToInt32(Request.QueryString["id"]);
        }
        if (IsPostBack)
        {
            if (Request.QueryString["action"] == "update")
            {
                string update_id = Request.QueryString["id"].Trim();
                string ucname1 = Request.Form["cname"].Trim();
                if ((!string.IsNullOrWhiteSpace(ucname1)))
                {
                    ucname1 = ucname1.Replace("'", "''").Trim();
                }
                if ((!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["cstreamurl"]))))
                {
                    ucstreamurl1 = Request.Form["cstreamurl"].ToString().Replace("'", "''");
                    ucstreamurl1 = ucstreamurl1.Replace("\r\n", "").Trim();
                }
                if ((!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["cchannelurl"]))))
                {
                    ucchannelurl1 = Request.Form["cchannelurl"].ToString().Replace("'", "''");
                }
                if ((!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["txtchanneltags"]))))
                {
                    strtxtchanneltags = Request.Form["txtchanneltags"].ToString().Replace("'", "''");
                }
                string ucformat = Request.Form["format1"].Trim();
                if (ucformat.ToLower().IndexOf("rtmp") > -1)
                {
                    ucformat = "RTMP";
                }
                string ucregion = Request.Form["region"].Trim();
                string uccountry = Request.Form["cbocountry"].Trim();
                if ((!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["cboLanguage"]))))
                {
                    channelLanguage = Request.Form["cboLanguage"].ToString().Replace("'", "''");
                }

                string SelLanguageID = "";
                if (!string.IsNullOrWhiteSpace(channelLanguage))
                {
                    SelLanguageID = channelLanguage;
                }
                string strDesc = Request["txtDesc"];
                string strPlayOption1 = Convert.ToString(Request["rPlayOption"]);
                if (string.IsNullOrWhiteSpace(strPlayOption1))
                {
                    strPlayOption1 = "0";
                }
                string ustr = null;
                if (!string.IsNullOrWhiteSpace(strDesc))
                {
                    if (strDesc.Length > 1000)
                    {
                        strDesc = strDesc.Substring(0, 1000);
                    }
                }
                strDesc = strDesc.Replace("'", "''");
                int strchstatus = 0;
                if (Request["Echk" + intChannel_id] == "on")
                {
                    strchstatus = 1;
                }
                ustr = "EXEC [dbo].[SP_Update_Channel] @LanguageID='" + SelLanguageID + "',@Status=" + strchstatus + ",@CName='" + ucname1 + "',@CDesc='" + strDesc + "',@StreamURL='" + ucstreamurl1 + "',@ChannelURL='" + ucchannelurl1 + "',@Format='" + ucformat + "',@Region='" + ucregion + "',@Country='" + uccountry + "',@PlayOption='" + strPlayOption1 + "',@ChannelID='" + update_id + "'";
                dp.ExecuteNonQuery(ustr);
                utils.ClearSitesSavedChannelCache(Convert.ToInt32(update_id));
            }
            if (Request.QueryString["action"] == "approve")
            {
                if (Request["chk" + intChannel_id] == "on")
                {
                    dp.ExecuteNonQuery("EXEC [dbo].[SP_Update_ChannelStatus] @Status=1,@ChannelID=" + intChannel_id);
                    utils.ClearSitesSavedChannelCache(intChannel_id);
                }
                else
                {
                    dp.ExecuteNonQuery("EXEC [dbo].[SP_Update_ChannelStatus] @Status=0,@ChannelID=" + intChannel_id);
                    utils.ClearSitesSavedChannelCache(intChannel_id);
                }
            }
        }
        intTop2 = 0;
        if (!string.IsNullOrWhiteSpace(Request.QueryString["pno"]))
        {
            pageno = Convert.ToInt32(Request.QueryString["pno"]);
            if (pageno > 0)
            {
                intTop2 = (pageno - 1) * intPagesize;
            }
        }
        if (Request.QueryString["action"] == "edit")
        {
            blnEditMode = true;
        }
        if (Request.QueryString["action"] == "del")
        {
            dp.ExecuteNonQuery("EXEC [dbo].[SP_Delete_Channel_ONID] @ChannelId=" + intChannel_id);
            utils.ClearSitesSavedChannelCache(intChannel_id);
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["txtChannelName"])))
        {
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["allowNames"])) && Convert.ToString(Session["allowNames"]) == "allow")
                strSearch = " And channel.channel_Name='" + Convert.ToString(Session["txtChannelName"]).Replace("'", "''") + "'";
            else
                strSearch = " And channel.channel_Name like '%" + Convert.ToString(Session["txtChannelName"]).Replace("'", "''") + "%'";
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["txtChannelID"])))
        {
            strSearch += " And channel.channel_ID in (" + Convert.ToString(Session["txtChannelID"]) + ")";
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["selStatus"])))
        {
            strSearch += " And channel.Status=" + Convert.ToString(Session["selStatus"]) + "";
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["selFormat"])))
        {
            strSearch += " And channel.Format in  (" + Convert.ToString(Session["selFormat"]) + ")";
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["SSites"])))
        {
            DataTable dtselctch = dp.FillDataTable("SELECT channel_id = ISNULL(Stuff((SELECT ',' +convert(varchar,channel_id) AS [text()] FROM"
                                                + " (SELECT channel_id FROM select_channel"
                                                + " where SiteID in(" + Convert.ToString(Session["SSites"]) + ")) x For XML PATH ('')),1,1,''),'')");
            if (dtselctch.Rows.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(dtselctch.Rows[0][0])))
                    strSearch += " And channel.channel_ID in (" + Convert.ToString(dtselctch.Rows[0][0]) + ")";
            }
        }
        strSearch = strSearch.Replace("'", "''");

        string strnewAppend = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["SRegion"])))
        {
            strnewAppend += " AND  channel.region_id=" + Convert.ToString(Session["SRegion"]);
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["SCountry"])))
            {
                strnewAppend += " AND  channel.country_id=" + Convert.ToString(Session["SCountry"]);
            }
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["Language"])))
        {
            string strLanguageID = Convert.ToString(Session["Language"]);
            string[] lang = strLanguageID.Split(',');

            strnewAppend += " AND (channel.LanguageID like '%" + lang[0] + "%' ";
            if (lang.Length > 0)
            {
                for (int i = 1; i <= lang.Length - 1; i++)
                {
                    strnewAppend += "or channel.LanguageID like '%" + lang[i] + "%'";
                }
            }
            strnewAppend += ")";
        }
        strnewAppend = strnewAppend.Replace("'", "''");

        strSql1 = "EXEC [dbo].[SP_Select_ChannelCount_DisplayAll] @StrNewAppend='" + strnewAppend + "',@StrSearch='" + strSearch + "'";
        DataTable dtCount = dp.FillDataTable(strSql1);
        recCount = Convert.ToInt32(dtCount.Rows[0][0]);
        if (recCount < intPagesize)
        {
            nopages = 1;
        }
        else
        {
            nopages = Convert.ToInt32(recCount / intPagesize);
            if (nopages * intPagesize < recCount)
            {
                nopages = nopages + 1;
            }
        }
        str3 = GetPageingString(pageno, nopages);
        if (Request.QueryString["order"] == "Desc")
        {
            strOrder = "Asc";
        }
        else
        {
            strOrder = "Desc";
        }
        strsql_sort = GetSortOrder();
        strsql_sort = strsql_sort.Replace("'", "''");

        string strnewAppend1 = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["SRegion"])))
        {
            strnewAppend1 += "  channel.region_id=" + Convert.ToString(Session["SRegion"]) + "  And  ";
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["SCountry"])))
            {
                strnewAppend1 += " channel.country_id=" + Convert.ToString(Session["SRegion"]) + "  AND  ";
            }
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["Language"])))
        {
            string strLanguageID1 = Convert.ToString(Session["Language"]);
            string[] lang1 = strLanguageID1.Split(',');
            if (lang1.Length > 1)
            {
                strnewAppend1 += "((replace(channel.LanguageID,' ','') like " + lang1[0] + " or replace(channel.LanguageID,' ','') like '%," + lang1[0] + ",%' or replace(channel.LanguageID,' ','') like '%," + lang1[0] + "' or replace(channel.LanguageID,' ','') like '" + lang1[0] + ",%') ";
                for (int i = 1; i <= lang1.Length - 1; i++)
                {
                    strnewAppend1 += "or (replace(channel.LanguageID,' ','') like " + lang1[i] + " or replace(channel.LanguageID,' ','') like '%," + lang1[i] + ",%' or replace(channel.LanguageID,' ','') like '%," + lang1[i] + "' or replace(channel.LanguageID,' ','') like '" + lang1[i] + ",%') ";
                }
                strnewAppend1 += ") and";
            }
            else
            {
                strnewAppend1 += "((replace(channel.LanguageID,' ','') like " + lang1[0] + " or replace(channel.LanguageID,' ','') like '%," + lang1[0] + ",%' or replace(channel.LanguageID,' ','') like '%," + lang1[0] + "' or replace(channel.LanguageID,' ','') like '" + lang1[0] + ",%'))";
                strnewAppend1 += "and";
            }
        }
        strnewAppend1 = strnewAppend1.Replace("'", "''");

        //For channel testing
        string channelTesting = "EXEC [dbo].[SP_Select_ChannelIDs_Testing] @StrNewAppend='" + strnewAppend1 + "',@StrSearch='" + strSearch + "',@StrSort='" + strsql_sort + "'";
        Session["query"] = channelTesting;
        //

        strSql = "EXEC [dbo].[SP_Select_Channel_DisplayAll] @StrNewAppend='" + strnewAppend1 + "',@StrSearch='" + strSearch + "',@StrSort='" + strsql_sort + "',@TOP=" + intTop1 + ",@NotTop=" + intTop2;
        dt = dp.FillDataTable(strSql);        
        foreach (DataRow drRow in dt.Rows)
        {
            blnisEdit = false;
            if (blnEditMode == true)
            {
                if (Convert.ToString(intChannel_id) == Convert.ToString(drRow["Channelid"]))
                {
                    blnisEdit = true;
                }
            }
            stus_dis = "";
            strRegionName = "";
            strCountryName = "";
            if (drRow["status"].ToString() == "1")
                stus_dis = " checked";
            intRegionId = Convert.ToInt32(drRow["region_id"].ToString());
            if ((!string.IsNullOrWhiteSpace(Convert.ToString(drRow["Language_ID"]))))
            {
                DataTable dtLanguageName = dp.FillDataTable("EXEC [SP_Language_Name] @Language_ID ='" + Convert.ToString(drRow["Language_ID"]) + "'");
                if (dtLanguageName.Rows.Count > 0)
                {
                    strsellanguage = Convert.ToString(dtLanguageName.Rows[0][0]);
                }
            }
            if (utils.IsNumeric(Convert.ToString(drRow["country_id"])))
            {
                intCountryId = Convert.ToInt32(drRow["country_id"]);
                strCountryName = GetCountryName(intCountryId);
            }
            if (intRegionId > 0)
            {
                strRegionName = GetRegionName(intRegionId);
            }
            if (Convert.ToString(drRow["ShowPL"]) == "0")
            {
                strPlayOption = "Stream URL";
                strOptStramUrl = " CHECKED ";
                strOptPlayList = " ";
                strOptPlayListFile = " ";
            }
            else if (Convert.ToString(drRow["ShowPL"]) == "1")
            {
                strPlayOption = "Playlist";
                strOptStramUrl = " ";
                strOptPlayList = " CHECKED ";
                strOptPlayListFile = " ";
            }
            else if (Convert.ToString(drRow["ShowPL"]) == "2")
            {
                strPlayOption = "Playlist File";
                strOptStramUrl = " ";
                strOptPlayList = " ";
                strOptPlayListFile = " CHECKED ";
            }
            int lastindex = drRow["channel_logo_path"].ToString() == "" ? 0 : drRow["channel_logo_path"].ToString().LastIndexOf('.');
            string stralt = drRow["channel_logo_path"].ToString().Substring(0,lastindex).Replace("-", " ").Replace("_", " ");
            img = "<img width=\"60\" height=\"60\" border=\"0\" src=\"../images_common/" + drRow["channel_logo_path"] + "\" alt=\"" + stralt + "\" >";
            strEditUrl = "approve.aspx?id=" + drRow["Channelid"] + "&action=edit&pno=" + pageno + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"];
            string strchannelformat = Convert.ToString(drRow["format"]);

            int chID = 0; chID = Convert.ToInt32(drRow["Channelid"]);
            if (blnisEdit == false)
            {
                strHtml.AppendLine("<tr style=\"background-color:#FFFFFF;\">");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><div align=\"center\"><a href='" + strEditUrl + "'><b>Edit</b></a></div><br/>");
                strHtml.Append("<a href='PlayList.aspx?chid=" + chID + "&pno=" + pageno + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + "'>PlayList</a><BR>");
                strHtml.Append("</td>");
                if (Request.QueryString["action"] == "edit")
                {
                    strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + strRegionName + "</br>" + strCountryName + "</td>");
                }
                else
                {
                    strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + strRegionName + "</td>");
                    strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + strCountryName + "</td>");
                }
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;word-wrap: break-word;width:120px;\">" + strsellanguage + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;word-wrap: break-word;width:60px;\"><strong>" + drRow["channel_name"] + "</strong><br>" + drRow["channel_URL"] + "</b><a name='" + drRow["channel_name"] + "'></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + chID + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + Server.HtmlEncode(drRow["channel_desc"].ToString()) + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + strchannelformat + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a class=\"rgttext\" href=\"javascript:confirmingSubmit(" + chID + ")\">" + "<br>" + img + "</a></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a class=\"rgttext\" href=\"javascript:confirmingSubmitaddUserChannel(" + chID + ")\">add to site</a></td>");
                string strstreamcode = "";
                strstreamcode = Server.HtmlEncode(drRow["stream_url"].ToString());
                if (!string.IsNullOrWhiteSpace(drRow["RTMPFile"].ToString()))
                {
                    strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;word-wrap: break-word;width:100px;\"><div style=\"width:150px;word-wrap: break-word;\">" + strstreamcode.Trim() + "<br><b>" + strPlayOption + "</div></td>");
                }
                else
                {
                    strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;word-wrap: break-word;width:100px;\"><div style=\"width:150px;word-wrap: break-word;\">" + strstreamcode.Trim() + "<br><b>" + strPlayOption + "</div></td>");
                }
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + drRow["entry_date"] + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><div align=\"center\"><input type=\"checkbox\" class='altinputnb' name=\"chk" + drRow["Channelid"] + "\"  ID=\"chk" + drRow["Channelid"] + "\" onclick=\"confirmSubmit(" + drRow["Channelid"] + ",'" + pageno + "','" + Request.QueryString["sort"] + "','" + Request.QueryString["order"] + "','approve')\"" + stus_dis + "></div></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a class=\"rgttext\" href=\"javascript:ShowChannelVideo(" + drRow["Channelid"] + ",'" + drRow["Channel_Name"].ToString().Replace("'", "&rsquo;") + "')\">" + "<br>" + "Channel testing" + "</a></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><div align=\"center\"><a onclick=\"javascript:return confirm('Are you sure, you want to delete?')\" href='approve.aspx?id=" + drRow["Channelid"] + "&action=del&pno=" + pageno + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + "'><b>Delete</b></a></div></td>");
                strHtml.AppendLine("</tr>");
            }
            else
            {
                strHtml.AppendLine("<tr id=\"str2row\" style=\"background-color:#FFFFFF;\">");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a href=\"javascript:void(0);\" onclick=\"confirmSubmit(" + drRow["Channelid"] + ",'" + pageno + "','" + Request.QueryString["sort"] + "','" + Request.QueryString["order"] + "','update');\"><b>Update</b></a>");
                strHtml.AppendLine("</b></td>");
                strHtml.Append("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + getRegionCombo(intRegionId) + "<br>");
                strHtml.Append("<div id='divcountry'><select name='cbocountry' id='cbocountry'><option value='' >- Select Country - </option><br>" + getCountry(intRegionId, intCountryId) + "</select>" + "</div></td>");
                //here code for select language
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + getLanguageCombo(drRow["language_ID"].ToString()) + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><input type='text' name='cname' id='cname' value=\"" + drRow["channel_Name"].ToString() + "\" >" + "<br><textArea name='cchannelurl' rows='3'>" + drRow["channel_URL"].ToString() + "</textArea><a name='" + drRow["channel_name"] + "'>");
                strHtml.AppendLine("</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + drRow["Channelid"] + "</td>");

                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><TextArea name=\"txtDesc\" id=\"txtDesc\" rows='5' maxlength='1000'>" + drRow["channel_desc"].ToString() + "</TextArea></td>");

                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + GetFormatBlock(strchannelformat) + "</td>");

                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a class=\"rgttext\" href=\"javascript:confirmingSubmit(" + drRow["Channelid"] + ")\">" + "<br>" + img + "</a></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a class=\"rgttext\" href=\"javascript:confirmingSubmitaddUserChannel(" + chID + ")\">add to site</a></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><textarea name='cstreamurl' rows='5'>" + drRow["stream_url"].ToString() + "</textarea><br><br>Stream URL:&nbsp;<input type='Radio' " + strOptStramUrl + "  name='rPlayOption' value='0' class='altinputnb'><br>PlayList:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='Radio' value='1' name='rPlayOption'  " + strOptPlayList + "   class='altinputnb'><br>PlayList File: &nbsp;&nbsp;<input type='Radio' value='2' name='rPlayOption'  " + strOptPlayListFile + "   class='altinputnb'></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\">" + drRow["entry_date"] + "</td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><div align=\"center\"><input type=\"checkbox\" class='altinputnb' name=\"Echk" + drRow["Channelid"] + "\"  ID=\"Echk" + drRow["Channelid"] + "\"" + stus_dis + "></div></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"><a class=\"rgttext\" href=\"javascript:ShowChannelVideo(" + drRow["Channelid"] + ",'" + drRow["Channel_Name"].ToString() + "')\">" + "<br>" + "Channel testing" + "</a></td>");
                strHtml.AppendLine("<td style=\"vertical-align:top;background-color:#FFFFFF;\"></td>");
                strHtml.AppendLine("</tr>");
            }
        }
    }
    public string GetFormatBlock(string strFormat)
    {
        strFormat = strFormat.Trim();
        string strCformat = null;
        strCformat = "<select name='format1'>";
        if (strFormat.ToLower().IndexOf("rm") > -1 || strFormat.ToLower().IndexOf("real") > -1)
        {
            strCformat += "<option selected>Real Video</option>";
        }
        else
        {
            strCformat += "<option>Real Video</option>";
        }
        if (strFormat.ToLower().IndexOf("windows") > -1 || strFormat.ToLower().IndexOf("wmv") > -1)
        {
            strCformat += "<option selected >Windows Media file</option>";
        }
        else
        {
            strCformat += "<option >Windows Media file</option>";
        }
        if (strFormat.ToLower().IndexOf("q") > -1 || strFormat.ToLower().IndexOf("mov") > -1)
        {
            strCformat += "<option selected  >Quicktime</option>";
        }
        else
        {
            strCformat += "<option >Quicktime</option>";
        }
        if (strFormat.ToLower().IndexOf("flash") > -1 || strFormat.ToLower().IndexOf("flv") > -1)
        {
            strCformat += "<option selected >Flash Video</option>";
        }
        else
        {
            strCformat += "<option >Flash Video</option>";
        }
        if (strFormat.ToLower().IndexOf("rtmp") > -1 || strFormat.ToLower().IndexOf("rtmp") > -1)
        {
            strCformat += "<option selected >Flash RTMP</option>";
        }
        else
        {
            strCformat += "<option >Flash RTMP</option>";
        }
        if (strFormat.ToLower().IndexOf("code") > -1 || strFormat.ToLower().IndexOf("code") > -1)
        {
            strCformat += "<option selected >Code</option>";
        }
        else
        {
            strCformat += "<option >Code</option>";
        }
        if (strFormat.ToLower().IndexOf("silverlight") > -1 || strFormat.ToLower().IndexOf("silverlight") > -1)
        {
            strCformat += "<option selected >Silverlight</option>";
        }
        else
        {
            strCformat += "<option >silverlight</option>";
        }
        strCformat += "<option >Other</option></select>";
        return strCformat;
    }

    public string getCountry(int Rid, int Cid)
    {
        string strSelect = "";
        string sqlsr = "select country_name,country_id from country where  region_id='" + Rid + "' Order by country_name";
        DataTable dtcountry = dp.FillDataTable(sqlsr);
        foreach (DataRow dr in dtcountry.Rows)
        {
            if (Cid.ToString().Trim() == dr["Country_id"].ToString().Trim())
            {
                strSelect += "<option value='" + dr["Country_id"].ToString() + " ' selected>" + dr["Country_name"].ToString() + "</option>";
            }
            else
            {
                strSelect += "<option value='" + dr["Country_id"].ToString() + " '>" + dr["country_name"].ToString() + "</option>";
            }
        }
        return strSelect;
    }
    public string getRegionCombo(int Rid)
    {
        string strSelect = "<select  onChange=\"javascript:fillCountryCbo(this.value)\" name='region'><option value='' >- Select - </option>";
        DataTable dtregion = null;
        dtregion = dp.FillDataTable("select region_id,region_name from bbc_region where region_id > 0 ORDER BY region_name");
        foreach (DataRow dr in dtregion.Rows)
        {
            if (Convert.ToString(Rid) == dr["region_id"].ToString())
            {
                strSelect += "<option value='" + dr["region_id"].ToString() + " ' selected>" + dr["region_name"].ToString() + "</option>";
            }
            else
            {
                strSelect += "<option value='" + dr["region_id"].ToString() + " '>" + dr["region_name"].ToString() + "</option>";
            }
        }
        strSelect = strSelect + "</Select>";
        return strSelect;
    }
    public string GetPageingString(int pageno, int nopages)
    {
        int pno = 0;
        string str3 = "";
        if (Convert.ToInt32(pageno) > 1)
        {
            str3 = str3 + "<a href=approve.aspx?pno=1&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + ">First</a>&nbsp;";
            str3 = str3 + "<a href=approve.aspx?pno=" + (pageno - 1) + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + ">&lt;&lt;Previous</a>&nbsp;";
        }
        else
        {
            str3 = str3 + "<a href='#'>First</a>&nbsp;";
            str3 = str3 + "<a href='#'>&lt;&lt;Previous</a>&nbsp;";
        }
        str3 = str3 + " <a>Page</a> ";
        for (pno = 1; pno <= nopages; pno++)
        {
            if (pno > 1)
                str3 = str3 + ", ";
            if (Convert.ToInt32(pno) == Convert.ToInt32(pageno))
            {
                str3 = str3 + "<strong><a>" + pno + "</strong></a>";
            }
            else
            {
                str3 = str3 + "<a href=approve.aspx?pno=" + pno + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + "><u>" + pno + "</u></a>";
            }
        }
        str3 = str3 + "&nbsp;";
        if (Convert.ToInt32(nopages) > 1)
        {
            if (Convert.ToInt32(pageno) < Convert.ToInt32(nopages))
            {
                str3 = str3 + "<a href=approve.aspx?pno=" + (pageno + 1) + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + ">Next&gt;&gt;</a>&nbsp;";
            }
            else
            {
                str3 = str3 + "<a href=#>Next&gt;&gt;</a>&nbsp;";
            }
            str3 = str3 + "<a href=approve.aspx?pno=" + nopages + "&sort=" + Request.QueryString["sort"] + "&order=" + Request.QueryString["order"] + ">Last</a>&nbsp;";
        }
        else
        {
            str3 = str3 + "<a href=#>Next&gt;&gt;</a>&nbsp;";
            str3 = str3 + "<a href=#>Last</a>&nbsp;";
        }
        str3 = str3 + "</strong>";
        return str3;
    }
    public string GetSortOrder()
    {
        string strsql_sort = "";
        if (Request.QueryString["sort"] == "channelname")
        {
            strsql_sort += " Order by channel_name " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Channel_URL")
        {
            strsql_sort += " Order by channel_URL " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Format")
        {
            strsql_sort += " Order by format " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Channel_Logo")
        {
            strsql_sort += " Order by channel_logo_path " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Stream_Url")
        {
            strsql_sort += " Order by stream_url " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Email")
        {
            strsql_sort += " Order by email " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Date")
        {
            strsql_sort += " Order by entry_date " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "IP_Address")
        {
            strsql_sort += " Order by system_ip " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Region")
        {
            strsql_sort += " Order by region_id " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Approve")
        {
            strsql_sort += " Order by status " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "country_id")
        {
            strsql_sort += " Order by country_id " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "Language")
        {
            strsql_sort += " Order by LanguageID " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "SLanguage")
        {
            strsql_sort += " Order by languages_text " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else if (Request.QueryString["sort"] == "flag")
        {
            strsql_sort += " Order by channel.flag " + Request.QueryString["order"] + ",channel.channel_id";
        }
        else
        {
            strsql_sort += " Order by channel.channel_id ";
        }
        return strsql_sort;
    }
    public string getLanguageCombo(string lidarr)
    {
        string strSelect = "<select multiple='multiple' name='cboLanguage' id='cbocboLanguage'><option value='0' >- Select - </option>";
        DataTable dtlangcomboselect = null;
        DataTable dtlangcombonotselect = null;
        if (string.IsNullOrWhiteSpace(lidarr))
        {
            lidarr = "0";
        }
        dtlangcomboselect = dp.FillDataTable("EXEC [dbo].[SP_Select_Language_Data] @LanguageID='" + lidarr + "',@Flag=1");
        dtlangcombonotselect = dp.FillDataTable("EXEC [dbo].[SP_Select_Language_Data] @LanguageID='" + lidarr + "',@Flag=0");
        foreach (DataRow dr in dtlangcomboselect.Rows)
        {
            strSelect += "<option value='" + dr["LanguageID"].ToString() + " ' selected>" + dr["LanguageName"].ToString() + "</option>";
        }
        foreach (DataRow dr in dtlangcombonotselect.Rows)
        {
            strSelect += "<option value='" + dr["LanguageID"].ToString() + " '>" + dr["LanguageName"].ToString() + "</option>";
        }
        strSelect = strSelect + "</Select>";
        return strSelect;
    }
    public string GetRegionName(int chid)
    {
        if (chid > 0)
        {
            string strCName = "0";
            DataTable dt = dp.FillDataTable("Select * from bbc_region where region_id=" + chid + "");
            if (dt.Rows.Count > 0)
                strCName = Convert.ToString(dt.Rows[0]["region_name"]);
            return strCName;
        }
        else
        {
            return "";
        }
    }
    public string GetCountryName(int chid)
    {
        if (chid > 0)
        {
            string strCName = "0";
            DataTable dt = dp.FillDataTable("select country_name from Country where Country_ID=" + chid + "");
            if (dt.Rows.Count > 0)
                strCName = Convert.ToString(dt.Rows[0]["country_Name"]);
            return strCName;
        }
        else
        {
            return "";
        }
    }
}