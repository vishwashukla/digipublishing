﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Drawing;
using System.Net;
using System.Linq;

public partial class DigipSuperAdmin_ManageVideoAdNetworks : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Ad Networks";
        lblMsg.Text = "";
        GetLastDate();
        if (!IsPostBack)
        {
            BindTestSites();
            GetAdNetworksDataBind();
        }
    }

    private void GetAdNetworksDataBind()
    {
        dgrdAdNetworks.DataSource = GetData();
        dgrdAdNetworks.DataBind();
        MvManadeAdNetworks.SetActiveView(ViewManageAdNetworks);
    }

    private DataTable GetData()
    {
        DataTable dt = new DataTable();
        string strSql = " SELECT Brand_AdNetwork.AdNetwork_ID,AdNetwork,Tag,Vast,priority,"
                      + " CASE WHEN ISNULL(Active_Inactive,0)=0 THEN 'false' ELSE 'true' END AS Active_Inactive"
                      + " FROM Brand_AdNetwork"
                      + " INNER JOIN Brand_AdNetworkRuleSettings ON Brand_AdNetwork.AdNetwork_ID=Brand_AdNetworkRuleSettings.AdNetwork_ID"
                      + " ORDER BY AdNetwork ASC";
        dt = dp.FillDataTable(strSql);
        return dt;
    }

    protected void lnkAddNetwork_Click(object sender, EventArgs e)
    {
        ResetControl();
        headerText.Text = "Add Network";
        Operation = "Add";
        if(lstBrowsers.Items.Count>0)
            lstBrowsers.Items.Cast<ListItem>().Where(n => n.Value !="-1").Select(n => n).ToList().ForEach(n => n.Selected = true);

        rdbBrowserAll.Checked = true;
        MvManadeAdNetworks.SetActiveView(ViewAddNetworks);
    }

    protected void dgrdAdNetworks_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkStatus = (CheckBox)e.Item.FindControl("chkStatus");

            if (chkStatus != null)
                chkStatus.Attributes.Add("onclick", "javascript:return confirmSubmit('" + e.Item.Cells[0].Text + "','" + hndAdnetworkid.ClientID + "','" + btnSetStatus.ClientID+ "')");
            
            if (chkStatus.Checked)
            {
                e.Item.BackColor = Color.GreenYellow;
            }
        }
    }

    protected void dgrdAdNetworks_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        string strAdNetwork_Id = e.Item.Cells[0].Text;
        try
        {
            if (e.CommandName == "Edit")
            {
                ResetControl();
                EditAdNetwork(strAdNetwork_Id);
                txtAdNetworkName.Enabled = true;
                this.headerText.Text = "Edit Network";
                Operation = "Edit";
                btnSubmit.Text = "Update";
                MvManadeAdNetworks.SetActiveView(ViewAddNetworks);
            }

            if (e.CommandName == "Delete")
            {
                if (utils.ExecuteNonQueryTrans("DELETE Brand_AdNetwork WHERE AdNetwork_ID='" + strAdNetwork_Id + "'", lblMsg))
                {
                    if (utils.ExecuteNonQueryTrans("DELETE FROM Brand_AdNetworkRuleSettings WHERE AdNetwork_ID='" + strAdNetwork_Id + "'", lblMsg))
                    {
                        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                        string strSqlReportDBInsert = "EXEC [dbo].[SP_Brand_DELETE_AdNetwork] @ADID='" + strAdNetwork_Id + "'";
                        dpreport.ExecuteNonQuery(strSqlReportDBInsert);

                        try
                        {
                            string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
                            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                        }
                        catch{}

                        this.lblMsg.Text = "Ad Network deleted successfully.";
                        GetAdNetworksDataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void dgrdAdNetworks_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = this.GetData();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    protected void btnSetStatus_Click(object sender, System.EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(hndAdnetworkid.Value.Trim()))
        {
            string strQueryCHK = "UPDATE Brand_AdNetworkRuleSettings SET Active_Inactive = case when Active_Inactive=1 then 0 else 1 END where AdNetwork_ID=" + hndAdnetworkid.Value.Trim();
            utils.ExecuteNonQuery(strQueryCHK, lblMsg);
            try
            {
                string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch{}
            GetAdNetworksDataBind();
        }
    }

    private void EditAdNetwork(string ID)
    {
        lblMsg.Text = "";
        string strBlocker = "";
        string strIPStatus = "";
        string strIPValue = "";
        string strIPDays = "";
        string strBlockerValue = "";

        DataTable dt = new DataTable();
        string strQuery = "";
        strQuery = "SELECT Brand_AdNetwork.*,Brand_AdNetworkRuleSettings.* FROM Brand_AdNetwork"
                    + " LEFT JOIN Brand_AdNetworkRuleSettings ON Brand_AdNetwork.AdNetwork_ID=Brand_AdNetworkRuleSettings.AdNetwork_ID"
                    + " WHERE Brand_AdNetwork.AdNetwork_ID=" + ID + "";

        dt = utils.RunQuery(strQuery, lblMsg);
        hndAdnetworkid.Value = Convert.ToString(dt.Rows[0]["AdNetwork_ID"]);
        if (dt.Rows.Count > 0)
        {
            txtAdNetworkName.Text = Convert.ToString(dt.Rows[0]["AdNetwork"]);
            txtAdTag.Text = Convert.ToString(dt.Rows[0]["Tag"]);
            if (Convert.ToString(dt.Rows[0]["Vast"]) == "0")
            {
                chkboxVast.Checked = false;
            }
            else
            {
                chkboxVast.Checked = true;
            }
            if (Convert.ToString(dt.Rows[0]["Ad_click_play"]) == "0" || Convert.ToString(dt.Rows[0]["Ad_click_play"]) == "")
            {
                chkadclick.Checked = false;
            }
            else
            {
                chkadclick.Checked = true;
            }

            if (Convert.ToString(dt.Rows[0]["Not_On_HomePage"]) == "0" || Convert.ToString(dt.Rows[0]["Not_On_HomePage"]) == "")
            {
                chkNotOnHomePage.Checked = false;
            }
            else
            {
                chkNotOnHomePage.Checked = true;
            }

            if (Convert.ToString(dt.Rows[0]["priority"]) != "")
            {
                drpPriority1.SelectedValue = Convert.ToString(dt.Rows[0]["priority"]);
            }
            else
            {
                drpPriority1.SelectedValue = "0";
            }

            //if (Convert.ToString(dt.Rows[0]["Plugin"]) != "")
            //{
            //    ddlPlugin.SelectedValue = Convert.ToString(dt.Rows[0]["Plugin"]);
            //}
            //else
            //{
            //    //ddlPlugin.SelectedValue = "0";
            //    ddlPlugin.SelectedValue = "OVA";
            //}

            if (Convert.ToString(dt.Rows[0]["Browser"]) != "")
            {
                string strBrowsers = Convert.ToString(dt.Rows[0]["Browser"]);
                if (strBrowsers.TrimEnd(',') != "")
                {
                    string[] strBrowserIDsarr = strBrowsers.Split(',');
                    strBrowserIDsarr.ToList().ForEach(r => lstBrowsers.Items.FindByValue(r).Selected = true);
                }
            }

            if (Convert.ToString(dt.Rows[0]["OS"]) != "")
            {
                string strOS = Convert.ToString(dt.Rows[0]["OS"]);
                if (strOS.TrimEnd(',') != "")
                {
                    string[] strOSarr = strOS.Split(',');
                    strOSarr.ToList().ForEach(x => lstOS.Items.FindByValue(x).Selected = true);
                }
            }

            string retval = string.Empty;
            retval = lstBrowsers.Items.Cast<ListItem>().Where(item => item.Selected == false && item.Value != "-1").Aggregate(retval, (current, item) => current + (item.Value + ','));
            if (retval != "")
                rdbBrowserSel.Checked = true;
            else
                rdbBrowserAll.Checked = true;

            txtReferalfrom.Text = Convert.ToString(dt.Rows[0]["Referal"]);
            txtNotReferalfrom.Text = Convert.ToString(dt.Rows[0]["Not_Referal"]);

            if (TreeView1.Nodes.Count > 0)
            {
                string strregion = Convert.ToString(dt.Rows[0]["Region"]);
                if(strregion.Trim()!="")
                    TreeView1.Nodes.Cast<TreeNode>().Where(n => strregion.Contains(n.Value)).Select(n => n).ToList().ForEach(n => n.Checked = true);

                foreach (TreeNode tnSite in TreeView1.Nodes)
                {
                    string strcountry = Convert.ToString(dt.Rows[0]["Country"]);
                    if (strcountry.Trim() != "")
                        tnSite.ChildNodes.Cast<TreeNode>().Where(n => strcountry.Contains(n.Value)).Select(n => n).ToList().ForEach(n => n.Checked = true);
                }
            }
            if (Convert.ToString(dt.Rows[0]["Blocker"]) == "")
            {
                strBlocker = "0";
            }
            else
            {
                strBlocker = Convert.ToString(dt.Rows[0]["Blocker"]);
            }
            CheckBlock1.Checked = Convert.ToBoolean(Convert.ToInt32(strBlocker));

            if (Convert.ToString(dt.Rows[0]["SameIP_Status"]) == "")
            {
                strIPStatus = "0";
            }
            else
            {
                strIPStatus = Convert.ToString(dt.Rows[0]["SameIP_Status"]);
            }

            if (Convert.ToString(dt.Rows[0]["SameIP_Value"]) == "")
            {
                strIPValue = "1";
            }
            else
            {
                strIPValue = Convert.ToString(dt.Rows[0]["SameIP_Value"]);
            }

            if (Convert.ToString(dt.Rows[0]["SameIP_Days"]) == "")
            {
                strIPDays = "1";
            }
            else
            {
                strIPDays = Convert.ToString(dt.Rows[0]["SameIP_Days"]);
            }

            if (Convert.ToString(dt.Rows[0]["BlockerValue"]) == "")
            {
                strBlockerValue = "1";
            }
            else
            {
                strBlockerValue = Convert.ToString(dt.Rows[0]["BlockerValue"]);
            }

            if (Convert.ToString(dt.Rows[0]["Targeted_DomainURLs"]) != "")
            {
                string strSites = Convert.ToString(dt.Rows[0]["Targeted_DomainURLs"]);
                if (strSites.TrimEnd(',') != "")
                    utils.SetSelectedListBoxItems(lstSites, strSites);
            }
        }
        if (strIPStatus == "0")
        {
            chkboxSameIPBlockerOn.Checked = false;           
        }
        else
        {
            chkboxSameIPBlockerOn.Checked = true;          
        }
        txtSameIPBlocker.Text = strIPValue;
        txtSameIPDays.Text = strIPDays;
        TextCheckBlock1.Text = strBlockerValue;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string strAdnetwork_id = string.Empty;
        string strAdNetworkName = string.Empty;
        string strAdTag = string.Empty;
        string strPriority = string.Empty;
        string strplugin = string.Empty;
        string stradclick = string.Empty;
        string strNotOnHomePage = string.Empty;

        strAdnetwork_id = Convert.ToString(hndAdnetworkid.Value);
        strAdNetworkName = Convert.ToString(txtAdNetworkName.Text);
        strAdTag = Convert.ToString(txtAdTag.Text);
        strPriority = Convert.ToString(drpPriority1.SelectedValue);
        strplugin = "OVA";//Convert.ToString(ddlPlugin.SelectedValue);
        string strChkVast = "";
        if (chkboxVast.Checked == true)
        {
            strChkVast = "1";
        }
        else
        {
            strChkVast = "0";
        }
        if (chkadclick.Checked == true)
        {
            stradclick = "1";
        }
        else
        {
            stradclick = "0";
        }
        if (chkNotOnHomePage.Checked == true)
        {
            strNotOnHomePage = "1";
        }
        else
        {
            strNotOnHomePage = "0";
        }
        StringBuilder strBrowser = new StringBuilder();
        strBrowser.Append(utils.GetSelectedListBoxItems(lstBrowsers));

        StringBuilder strOS = new StringBuilder();
        strOS.Append(utils.GetSelectedListBoxItems(lstOS));

        string strReferal = string.Empty;
        string strNotReferal = string.Empty;
        strReferal = txtReferalfrom.Text;
        strNotReferal = txtNotReferalfrom.Text;

        string strregion = string.Empty;
        string strcountry = string.Empty;

        foreach (TreeNode tnSite in TreeView1.Nodes)
        {
            string str = utils.GetSelectedChildTreeNodes(tnSite);
            if (str != "")
            {
                strregion += tnSite.Value + ",";
                strcountry += str + ",";
            }
        }
        strregion = strregion.TrimEnd(',');
        strcountry = strcountry.TrimEnd(',');

        string strCheckBlock1 = string.Empty;
        strCheckBlock1 = Convert.ToString(Convert.ToUInt32(CheckBlock1.Checked));
        string strIPStatus;
        int strIPValue;
        int strIPDays;

        if (chkboxSameIPBlockerOn.Checked == true)
        {
            strIPStatus = "1";
        }
        else
        {
            strIPStatus = "0";
        }
        strIPValue = txtSameIPBlocker.Text==""? 0 : Convert.ToInt32(txtSameIPBlocker.Text);
        strIPDays = txtSameIPDays.Text == "" ? 1 : Convert.ToInt32(txtSameIPDays.Text);

        int strBlockerValue = 0;
        if (CheckBlock1.Checked == true)
        {
            strBlockerValue = Convert.ToInt32(TextCheckBlock1.Text);
        }
        else
        {
            strBlockerValue = 0;
        }

        StringBuilder strTargetedDomains = new StringBuilder();
        strTargetedDomains.Append(utils.GetSelectedListBoxItems(lstSites));

        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);

        if (Operation == "Add")
        {
            string strSqlAdd = "Insert into Brand_AdNetwork (AdNetwork) VALUES('" + strAdNetworkName + "');"
                    + "Insert into Brand_AdNetworkRuleSettings (Tag,Vast,priority,Browser,Referal"
                    + ",Not_Referal,Country,Region,Plugin,Blocker,SameIP_Status,SameIP_Value,BlockerValue,Ad_click_play,AdNetwork_ID,Targeted_DomainURLs,Not_On_HomePage,OS,SameIP_Days)"
                    + " values('" + strAdTag + "','" + strChkVast + "','"
                    + strPriority + "','"
                    + Convert.ToString(strBrowser) + "','" + strReferal + "','" + strNotReferal + "','"
                    + Convert.ToString(strcountry) + "','" + Convert.ToString(strregion) + "','" + strplugin + "','"
                    + strCheckBlock1 + "','" + strIPStatus + "','" + strIPValue + "','" + strBlockerValue + "','" + stradclick + "',SCOPE_IDENTITY(),'" + Convert.ToString(strTargetedDomains) + "','" + strNotOnHomePage + "','"
                    + strOS + "','" + strIPDays + "')";
            utils.ExecuteNonQueryTrans(strSqlAdd, lblMsg);

            string strSQlAdNetworkSelect = "Select AdNetwork_ID,AdNetwork from Brand_AdNetwork where AdNetwork='" + strAdNetworkName + "'";
            DataTable dtAdNetworkSelect = dp.FillDataTable(strSQlAdNetworkSelect);
            if (dtAdNetworkSelect.Rows.Count > 0)
            {
                string strSqlReportDBInsert = "EXEC [dbo].[SP_Brand_Insert_AdNetwork] @ADID='" + Convert.ToString(dtAdNetworkSelect.Rows[0]["AdNetwork_ID"]) + "',@ADNAME='" + Convert.ToString(dtAdNetworkSelect.Rows[0]["AdNetwork"]) + "'";
                dpreport.ExecuteNonQuery(strSqlReportDBInsert);
            }

            try
            {
                string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch{ }

            this.lblMsg.Text = "Ad Network Added successfully.";
            GetAdNetworksDataBind();
        }
        else if (Operation == "Edit")
        {
            string strSqlEdit = "update Brand_AdNetwork set AdNetwork='" + strAdNetworkName + "' WHERE AdNetwork_ID=" + strAdnetwork_id;
            if (utils.ExecuteNonQueryTrans(strSqlEdit, lblMsg))
            {
                strSqlEdit = "update Brand_AdNetworkRuleSettings set Tag='" + strAdTag + "',"
                + "Vast='" + strChkVast + "',"
                + "priority='" + strPriority + "'"
                + " ,Browser='" + Convert.ToString(strBrowser) + "',Referal='" + strReferal + "',Not_Referal='" + strNotReferal
                + "',Country='" + Convert.ToString(strcountry) + "',Region='" + Convert.ToString(strregion) + "',Plugin='" + strplugin
                + "',Blocker='" + strCheckBlock1 + "',SameIP_Status='" + strIPStatus + "',SameIP_Value='" + strIPValue + "'"
                + ",BlockerValue='" + strBlockerValue + "',Ad_click_play='" + stradclick + "',Targeted_DomainURLs='" + Convert.ToString(strTargetedDomains) + "'"
                + ",Not_On_HomePage='" + strNotOnHomePage + "'" + ",OS='" + strOS + "',SameIP_Days='" + strIPDays + "' where AdNetwork_ID=" + strAdnetwork_id;
                dp.ExecuteNonQuery(strSqlEdit);

                try
                {
                    string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
                    HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                    HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                }
                catch{ }

                this.lblMsg.Text = "Ad Network Updated successfully.";
                GetAdNetworksDataBind();
            }
            string strSqlReportDBInsert = "EXEC [dbo].[SP_Brand_UPDATE_AdNetwork] @ADID='" + strAdnetwork_id + "',@ADNAME='" + strAdNetworkName + "'";
            dpreport.ExecuteNonQuery(strSqlReportDBInsert);
        }
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            foreach (DataGridItem dgi in dgrdAdNetworks.Items)
            {
                CheckBox chkStatus = (CheckBox)dgi.FindControl("chkStatus");
                TextBox txtPrioritygrid = (TextBox)dgi.FindControl("txtPrioritygrid");
                string strInactiveActive = "";
                if (chkStatus.Checked == true)
                {
                    strInactiveActive = "1";
                }
                else
                {
                    strInactiveActive = "0";
                }
                string strpriority = "0";
                if (txtPrioritygrid.Text != "")
                {
                    strpriority = txtPrioritygrid.Text;
                }

                string strSqlUpdate = "update Brand_AdNetworkRuleSettings set "
                               + "Active_Inactive='" + strInactiveActive + "',"
                               + " priority=" + strpriority + ""
                               + " where AdNetwork_ID=" + Convert.ToString(dgi.Cells[0].Text);
                dp.ExecuteNonQuery(strSqlUpdate);
            }
            lblMsg.Text = "Updated successfully";

            try
            {
                string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch{ }

            GetAdNetworksDataBind();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    private void BindDevicesBrowsers()
    {

        DataTable dtBrowsers = new DataTable();
        String SqlBrowsers = "SELECT ID,Device_Browser,Value FROM brand_Devices_Browsers Where Type='2'";
        dtBrowsers = dp.FillDataTable(SqlBrowsers);
        lstBrowsers.DataSource = dtBrowsers;
        lstBrowsers.DataTextField = "Device_Browser";
        lstBrowsers.DataValueField = "Value";
        lstBrowsers.DataBind();
        lstBrowsers.Items.Insert(0, new ListItem("----Select Browser----", "-1"));
        lstBrowsers.Items.Add(new ListItem("(Other)", "0"));
    }

    private void BindTreeData(TreeView tree)
    {
        string query = "select region_name as name,region_id as id from bbc_region ORDER by name";
        DataTable dt = dp.FillDataTable(query);
        PopulateNodes(dt, tree.Nodes, false);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes, bool flag)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode("region", "0");
            tn.Text = Convert.ToString(dr["name"]);
            tn.Value = Convert.ToString(dr["id"]);
            tn.PopulateOnDemand = flag;
            nodes.Add(tn);
            string query = "SELECT CASE WHEN country_id>0 AND country_id<10 THEN '00'+Convert(varchar(3),country_id)"
                        + " WHEN country_id>=10 AND country_id<100 THEN '0'+Convert(varchar(3),country_id)"
                        + " ELSE Convert(varchar(3),country_id) END AS ID,country_name as name FROM Country"
                        + " where region_id = " + Convert.ToInt32(Convert.ToString(dr["id"]))
                        + " ORDER by name";
            DataTable dtcountry = dp.FillDataTable(query);
            PopulateChildNodes(dtcountry, tn.ChildNodes, false);
            tn.Select();
        }
    }

    private void PopulateChildNodes(DataTable dt, TreeNodeCollection nodes, bool flag)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode("country", "0");
            tn.Text = Convert.ToString(dr["name"]);
            tn.Value = Convert.ToString(dr["id"]);
            tn.PopulateOnDemand = flag;
            nodes.Add(tn);
        }
    }

    protected void CheckBlock1_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBlock1.Checked == true)
        {
            TextCheckBlock1.Enabled = true;
            TextCheckBlock1.Focus();
            TextCheckBlock1.Text = "1";
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            string strdroptable = " Drop Table Brand_Restore_AdNetwork;"
                                + " Drop Table Brand_Restore_AdNetworkRuleSettings;";
            utils.ExecuteNonQueryTrans(strdroptable, lblMsg);

            string strqry = "SELECT * INTO Brand_Restore_AdNetwork FROM Brand_AdNetwork;"
                            + " ALTER TABLE Brand_Restore_AdNetwork ADD PRIMARY KEY(AdNetwork_ID);"
                            + " SELECT * INTO Brand_Restore_AdNetworkRuleSettings FROM Brand_AdNetworkRuleSettings;"
                            + " ALTER TABLE Brand_Restore_AdNetworkRuleSettings ADD PRIMARY KEY(ANS_ID);";
            utils.ExecuteNonQueryTrans(strqry, lblMsg);

            string strAdNetworkID = string.Empty;
            foreach (DataGridItem dgi in dgrdAdNetworks.Items)
            {
                strAdNetworkID += Convert.ToString(dgi.Cells[0].Text) + ",";
            }
            strAdNetworkID = strAdNetworkID.TrimEnd(',');
            string strSqlUpdate = "update Brand_AdNetworkRuleSettings set Saved_date='" + DateTime.Now + "' where AdNetwork_ID IN ("+strAdNetworkID+")";
            dp.ExecuteNonQuery(strSqlUpdate);
            string strSqlUpdate2 = "update Brand_Restore_AdNetworkRuleSettings set Saved_date='" + DateTime.Now + "'";
            dp.ExecuteNonQuery(strSqlUpdate2);
            lblMsg.Text = "Saved successfully";
            GetAdNetworksDataBind();
            GetLastDate();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void btnrestore_Click(object sender, EventArgs e)
    {
        string strQuery = "Drop Table Brand_AdNetwork;"
                        + " Drop Table Brand_AdNetworkRuleSettings;";
        if (utils.ExecuteNonQueryTrans(strQuery, lblMsg))
        {
            string strQuery1 = "SELECT * INTO Brand_AdNetwork FROM Brand_Restore_AdNetwork;"
                            + " ALTER TABLE Brand_AdNetwork ADD PRIMARY KEY(AdNetwork_ID);"
                            + " SELECT * INTO Brand_AdNetworkRuleSettings FROM Brand_Restore_AdNetworkRuleSettings;"
                            + " ALTER TABLE Brand_AdNetworkRuleSettings ADD PRIMARY KEY(ANS_ID);";
            if (utils.ExecuteNonQueryTrans(strQuery1, lblMsg))
            {
                DataTable dt = new DataTable();
                dt = GetData();
                try
                {
                    string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
                    HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                    HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                }
                catch{ }

                dgrdAdNetworks.DataSource = dt;
                dgrdAdNetworks.DataBind();                
            }
        }
    }

    protected void GetLastDate()
    {
        DataTable dt = new DataTable();
        DateTime date = new DateTime();
        string strSql = " SELECT Saved_Date FROM Brand_Restore_AdNetworkRuleSettings";
        dt = dp.FillDataTable(strSql);
        if (dt.Rows.Count > 0 && Convert.ToString(dt.Rows[0]["Saved_Date"]) != "")
        {
            date = Convert.ToDateTime(dt.Rows[0]["Saved_Date"]);
        }
        lbldate1.Text = date.ToString("dd MMMM yyyy");
        lbldate2.Text = date.ToString("dd MMMM yyyy");
    }

    public string Operation
    {
        get
        {
            object o = ViewState["Operation"];
            if (o == null)
            {
                return "Add";
            }
            return (string)o;
        }
        set
        {
            ViewState["Operation"] = value;
        }
    }

    private void ResetControl()
    {
        hndAdnetworkid.Value = "";
        txtAdNetworkName.Text = "";
        txtAdTag.Text = "";
        chkboxVast.Checked = true;
        drpPriority1.SelectedValue = "0";
        //ddlPlugin.SelectedValue = "0";
        //ddlPlugin.SelectedValue = "OVA";
        rdbBrowserAll.Checked = false;
        rdbBrowserSel.Checked = false;
        lstBrowsers.SelectedValue = null;
        lstOS.ClearSelection();
        txtReferalfrom.Text = "";
        txtNotReferalfrom.Text = "";
        TreeView1.Nodes.Clear();
        CheckBlock1.Checked = false;
        chkboxSameIPBlockerOn.Checked = false;       
        txtSameIPBlocker.Text = "";
        txtSameIPDays.Text = "";
        TextCheckBlock1.Text = "";
        lblMsg.Text = "";
        btnSubmit.Text = "Submit";
        txtAdNetworkName.Enabled = true;
        lstGenre.ClearSelection();
        lstSites.ClearSelection();
        chkadclick.Checked = false;
        chkNotOnHomePage.Checked = false;
        BindDevicesBrowsers();
        BindTreeData(TreeView1);
        BindGenre();
        BindSites();
        lstGenre.Attributes.Add("onclick", "GetSites('"+lstGenre.ClientID+"','"+lstSites.ClientID+"')");
        this.rdbBrowserAll.Attributes.Add("onclick", "Browserall('" + rdbBrowserAll.ClientID + "','" + lstBrowsers.ClientID + "')");
        this.rdbBrowserSel.Attributes.Add("onclick", "BrowserSel('" + rdbBrowserSel.ClientID + "','" + lstBrowsers.ClientID + "')");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControl();
        MvManadeAdNetworks.SetActiveView(ViewManageAdNetworks);
    }
    protected void BindGenre()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_GenreAll";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        lstGenre.DataSource = dt;
        lstGenre.DataTextField = "genre_name";
        lstGenre.DataValueField = "ID";
        lstGenre.DataBind();
        lstGenre.Items.Insert(0, new ListItem("----------------Select Genre----------------", "-1"));
    }
    protected void BindSites()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_SitesAll";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        lstSites.DataSource = dt;
        lstSites.DataTextField = "domainname";
        lstSites.DataValueField = "site_id";
        lstSites.DataBind();
        lstSites.Items.Insert(0, new ListItem("-----------------Select Sites-----------------", "-1"));
    }

    protected void BindTestSites()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_SitesAll";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        drpTestSites.DataSource = dt;
        drpTestSites.DataTextField = "domainname";
        drpTestSites.DataValueField = "domainname";
        drpTestSites.DataBind();
        drpTestSites.Items.Insert(0, new ListItem("--Select--", "--Select--"));
    }

    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }
}