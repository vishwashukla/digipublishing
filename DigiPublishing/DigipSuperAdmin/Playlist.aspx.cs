﻿using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Text;
using System.Web.UI;

public partial class DigipSuperAdmin_PlayList : System.Web.UI.Page
{
    int intChannel_id = 0;
    string strPreLink;
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, System.EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Channel PlayList";
        string strPno = Request["pno"];
        string strSort = Request["sort"];
        string strOrder = Request["order"];
        strPreLink = "&pno=" + strPno + "&sort=" + strSort + "&order=" + strOrder;
        if (!string.IsNullOrEmpty(Request["chid"]))
        {
            intChannel_id = Convert.ToInt32(Request["chid"]);
        }
        if (Convert.ToString(ConfigurationManager.AppSettings["KeyCode"]) == "live")
        {
            string url1 = Convert.ToString(ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=digipchannelplaylist&ChannelID=" + intChannel_id);
            HttpWebRequest request1 = (HttpWebRequest)System.Net.WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            string url2 = Convert.ToString(ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=digipchannelplaylist&ChannelID=" + intChannel_id);
            HttpWebRequest request2 = (HttpWebRequest)System.Net.WebRequest.Create(url2);
            HttpWebResponse response2 = (HttpWebResponse)request2.GetResponse();
        }
        chkRandomplaylist.Attributes.Add("onclick", "javascript:return RandomPlaylistClick('" + intChannel_id + "')");
        HyperLink1.NavigateUrl = "playlist.aspx?add=1&chid=" + intChannel_id + strPreLink;
        string strChannel_Name = GetChannelInfo(Convert.ToString(intChannel_id));
        hlChannel.NavigateUrl = "approve.aspx?id=" + intChannel_id + "&action=edit" + strPreLink + "#" + strChannel_Name;
        hlChannel.Text = strChannel_Name;
        

        if (!IsPostBack)
        {
            if (Request.QueryString["add"] == "1")
            {
                rowadd.Visible = true;
            }
            else
            {
                rowadd.Visible = false;
            }
            if (Convert.ToString(Request.QueryString["edit"]) == "1" && !string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"])))
            {
                rowedit.Visible = true;
                GetEntry(Convert.ToInt32(Request.QueryString["id"]));
            }
            else
            {
                rowedit.Visible = false;
            }
            if (Convert.ToString(Request.QueryString["del"]) == "1" && !string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"])))
            {
                rowdel.Visible = true;
                GetEntry(Convert.ToInt32(Request.QueryString["id"]));
                lbdel.Text = "Are you sure want to delete \"" + txtTitle1.Text + "\" Entry ?";
            }
            else
            {
                rowdel.Visible = false;
            }

            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["action"]))
                && !string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["sorder"]))
                && !string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"])))
            {
                if (Convert.ToString(Request.QueryString["action"]) == "up")
                {
                    int intOrder = Convert.ToInt32(Request.QueryString["sorder"]);
                    int intCPid = Convert.ToInt32(Request.QueryString["id"]);
                    if (intOrder > 1)
                    {
                        dp.ExecuteNonQuery("EXEC [dbo].[SP_Update_ChannelPlaylistSortOrder_OnSortOrder] @SortOrderNew=" + intOrder + ",@SortOrderOld=" + (intOrder - 1) + ",@ChanneID=" + intChannel_id);
                        dp.ExecuteNonQuery("EXEC [dbo].[SP_Update_ChannelPlaylistSortOrder_OnPlayList] @SortOrder=" + (intOrder - 1) + ",@PlayListID=" + intCPid + ",@ChanneID=" + intChannel_id);
                    }
                    Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
                }

                if (Convert.ToString(Request.QueryString["action"]) == "down")
                {
                    int intOrder = Convert.ToInt32(Request.QueryString["sorder"]);
                    int intCPid = Convert.ToInt32(Request.QueryString["id"]);
                    if (intOrder < GetMaxOrder())
                    {
                        dp.ExecuteNonQuery("EXEC [dbo].[SP_Update_ChannelPlaylistSortOrder_OnSortOrder] @SortOrderNew=" + intOrder + ",@SortOrderOld=" + (intOrder + 1) + ",@ChanneID=" + intChannel_id);
                        dp.ExecuteNonQuery("EXEC [dbo].[SP_Update_ChannelPlaylistSortOrder_OnPlayList] @SortOrder=" + (intOrder + 1) + ",@PlayListID=" + intCPid + ",@ChanneID=" + intChannel_id);
                    }
                    Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
                }
            }
            lbBody.Text = GetPlayList();
        }
    }

    private int GetMaxOrder()
    {
        int intO = 0;
        string strQuery = "EXEC [dbo].[SP_Select_MaxSortOrder] @ChanneID=" + intChannel_id;
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["MO"])))
        {
            intO = Convert.ToInt32(dt.Rows[0]["MO"]);
        }
        return intO;
    }

    public string GetPlayList()
    {
        StringBuilder strSB = new StringBuilder();
        int i = 0;
        string strQuery = "";
        string strQueryPlaylistAlternate = "EXEC [dbo].[SP_Select_CNBCMRSS_STREAMURL] @ChanneID=" + intChannel_id;
        DataTable dtPlaylistAlternate = dp.FillDataTable(strQueryPlaylistAlternate);
        if (dtPlaylistAlternate.Rows.Count > 0)
        {
            chkplaylistAlternate.Checked = Convert.ToBoolean(Convert.ToInt32(dtPlaylistAlternate.Rows[0]["Playlist_Alternate"]));
            chkCNBC_rss.Checked = Convert.ToBoolean(Convert.ToInt32(dtPlaylistAlternate.Rows[0]["CNBC_MRSS"]));
            txtplaylistAlternate.Text = Convert.ToString(dtPlaylistAlternate.Rows[0]["stream_url"]);
        }
        DataTable dtPlaylistRandom = dp.FillDataTable("EXEC [dbo].[SP_Select_PlaylistRandom] @ChanneID=" + intChannel_id);
        bool boolPlaylistRandom = false;
        if (dtPlaylistRandom.Rows.Count > 0)
        {
            boolPlaylistRandom = Convert.ToBoolean(dtPlaylistRandom.Rows[0]["PlaylistRandom"]);
        }
        chkRandomplaylist.Checked = boolPlaylistRandom;
        strQuery = "EXEC [dbo].[SP_Select_ChannelPlayList_OnCID] @ChanneID=" + intChannel_id;
        DataTable dt = dp.FillDataTable(strQuery);
        strSB.AppendLine("<table width='100%' border='0' bgcolor=\"#CCCCCC\">");
        strSB.AppendLine("<tr><td colspan=2 bgcolor=\"#FFFFFF\">Order</td><td bgcolor=\"#FFFFFF\"><strong>Entry</strong></td><td colspan=2 bgcolor=\"#FFFFFF\">Action</td></tr>");
        foreach (DataRow dr in dt.Rows)
        {
            i = i + 1;
            strSB.AppendLine("<tr><td align=center bgcolor=\"#FFFFFF\" width=10><a href='playlist.aspx?action=up&sorder=" + dr["sortorder"].ToString() + "&chid=" + intChannel_id + "&id=" + dr["Channel_PlayList_id"].ToString() + strPreLink + "'><img alt='upImg' border='0' src='../images_common/upImg.gif'></a></td><td bgcolor=\"#FFFFFF\" align=center width=10><a href='playlist.aspx?action=down&sorder=" + dr["sortorder"].ToString() + "&chid=" + intChannel_id + "&id=" + dr["Channel_PlayList_id"].ToString() + strPreLink + "'><img border='0' src='../images_common/DownImg.gif'></a></td>");
            strSB.AppendLine("<td bgcolor=\"#FFFFFF\">" + dr["Title"].ToString() + "</td>");
            strSB.AppendLine("<td bgcolor=\"#FFFFFF\"><a href='playlist.aspx?edit=1&id=" + dr["Channel_PlayList_id"] + "&chid=" + intChannel_id + strPreLink + "'><b>Edit</b></a></td>");
            strSB.AppendLine("<td  bgcolor=\"#FFFFFF\" align=center width='20'><a href='playlist.aspx?del=1&id=" + dr["Channel_PlayList_id"] + "&chid=" + intChannel_id + strPreLink + "'><b>Delete</b></a></td>");
            strSB.AppendLine("</tr>");
        }
        strSB.AppendLine("</table>");
        DataTable dtdomain = dp.FillDataTable("select distinct bd.domainname from select_channel As SC Inner Join brand_domainname as bd on bd.id=SC.SiteID where sc.channel_id=" + intChannel_id);
        string domain = "demo.digipublishing.net";
        if(dtdomain.Rows.Count > 0)
        {
            domain = dtdomain.Rows[0]["domainname"].ToString();
        }
        var playlistUrl = "http://" + domain + "/DigipSuperAdmin/GetPlaylist.aspx?id=" + intChannel_id;
        if (Request.Url.Host.ToLower() == "localhost")
        {
            playlistUrl = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/DigipSuperAdmin/GetPlaylist.aspx?id=" + intChannel_id;
        }
        lnkPlaylist.InnerHtml = playlistUrl;
        lnkPlaylist.HRef = playlistUrl;
        return strSB.ToString();
    }

    public string GetEntry(int gid)
    {
        string strGtext = "";
        DataTable dt = dp.FillDataTable("EXEC [dbo].[SP_Select_ChannelPlayList_OnPID] @PlayListID=" + gid);
        if (dt.Rows.Count > 0)
        {
            txtTitle1.Text = Convert.ToString(dt.Rows[0]["title"]);
            txtRefHref1.Text = Convert.ToString(dt.Rows[0]["REF_HREF"]);
            txtCopyright1.Text = Convert.ToString(dt.Rows[0]["copyright"]);
            txtDescription1.Text = Convert.ToString(dt.Rows[0]["Playlist_Desc"]);
        }
        return strGtext;
    }

    protected void btnAdd_Click(object sender, System.EventArgs e)
    {
        if (txtRefHref.Text.Trim() != "")
        {
            string strSql = "EXEC [dbo].[SP_Insert_Channel_PlayList] @ChannelID=" + intChannel_id + ",@Title='" + txtTitle.Text.Trim() + "',@REFHREF='" + txtRefHref.Text.Trim() + "',@CopyRight='" + txtCopyright.Text.Trim() + "',@SortOrder=" + (GetMaxOrder() + 1) + ",@Playlist_Desc='" + txtDescription.Text.Trim() + "'";
            dp.ExecuteNonQuery(strSql);
            Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
        }
    }

    protected void btnCancel1_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
    }

    protected void btnEdit_Click(object sender, System.EventArgs e)
    {
        if (txtRefHref1.Text.Trim() != "")
        {
            string strSql = "EXEC [dbo].[SP_Update_Channel_PlayList] @Title='" + txtTitle1.Text.Trim() + "',@REFHREF='" + txtRefHref1.Text.Trim() + "',@CopyRight='" + txtCopyright1.Text.Trim() + "',@PlayListID=" + Convert.ToInt32(Request.QueryString["id"]) + ",@Playlist_Desc='" + txtDescription1.Text.Trim() + "'";
            dp.ExecuteNonQuery(strSql);
            Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
        }
    }
    protected void btnCancel2_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
    }
    protected void btnDel_Click(object sender, System.EventArgs e)
    {
        dp.ExecuteNonQuery("EXEC [dbo].[SP_Delete_Channel_PlayList] @PlayListID=" + Convert.ToInt32(Request.QueryString["id"]));
        Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
    }
    protected void btnCancel3_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
    }
    private string GetChannelInfo(string chid)
    {
        string strChannel_Name = "";
        DataTable dt = dp.FillDataTable("EXEC [dbo].[SP_Select_Channel_forPlayList] @ChannelID=" + chid);
        if(dt.Rows.Count>0)
        {
            strChannel_Name =Convert.ToString(dt.Rows[0]["channel_name"]);
        }
        return strChannel_Name;
    }
    protected void Button2_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("approve.aspx?chid=" + intChannel_id + strPreLink);
    }
    protected void btnchkRandom_Click(object sender, System.EventArgs e)
    {
        if (!string.IsNullOrEmpty(hdnChannelID.Value.Trim()))
        {
            string strQueryCHK = "EXEC [dbo].[SP_Update_PlaylistRandom] @ChannelID=" + Convert.ToInt32(hdnChannelID.Value.Trim());
            dp.ExecuteNonQuery(strQueryCHK);
            Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
        }
    }
    protected void btnPlaylistAlternate_Click(object sender, System.EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtplaylistAlternate.Text))
        {
            int intPlaylistAlternate = Convert.ToInt32(chkplaylistAlternate.Checked);
            int intCNBCMRSS = Convert.ToInt32(chkCNBC_rss.Checked);
            string strPlaylistAlternate = Convert.ToString(txtplaylistAlternate.Text);
            string strSQLPlaylistAlternate = "EXEC [dbo].[SP_Update_Playlist_Alternate] @ChannelID=" + intChannel_id + ",@PlaylistAlternate=" + intPlaylistAlternate + ",@CNBCMRSS=" + intCNBCMRSS + ",@Streamurl='" + strPlaylistAlternate + "'";
            dp.ExecuteNonQuery(strSQLPlaylistAlternate);
            Response.Redirect("playlist.aspx?chid=" + intChannel_id + strPreLink);
        }
        else
        {
            lblMsg.Text = "Please Enter Alternative Playlist URL in TextBox";
            txtplaylistAlternate.Focus();
            return;
        }
    }
}