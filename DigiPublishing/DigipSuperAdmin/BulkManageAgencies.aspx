﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="BulkManageAgencies.aspx.cs" Inherits="DigipSuperAdmin_BulkManageAgencies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../JS/Password.js"></script>
    <table>
        <tr>
            <td colspan="3">
                <h4>Bulk Manage Sites</h4>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label></td>
        </tr>
        <tr>
            <td></td>
            <td style="width: 85px;">
                <b>please check which you want to update</b></td>
            <td></td>
        </tr>
        <tr>
            <td>Site Genre
            </td>
            <td>&nbsp;
            </td>
            <td>
                <asp:DropDownList ID="ddlSiteGenre" CssClass="maintext" runat="server" Width="152px"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlSiteGenre_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="height: 72px">Multi-select Sites to change</td>
            <td></td>
            <td style="height: 72px">
                <asp:ListBox ID="lstAgency" runat="server" SelectionMode="Multiple" Height="150px"
                    Width="300px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="txtDisplay" style="height: 24px">Address</td>
            <td>
                <asp:CheckBox ID="chkAddress" runat="server" /></td>
            <td style="height: 24px">
                <asp:TextBox ID="txtAddress" CssClass="maintext" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Themes
            </td>
            <td>
                <asp:CheckBox ID="chkTheme" runat="server" /></td>
            <td>
                <asp:DropDownList ID="drpTheme" runat="server" Width="162px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Features
            </td>
            <td colspan="2">
                <br />
                <table>
                    <tr valign="top">
                        <td>
                            <asp:CheckBox ID="chkFeature" runat="server" /></td>
                        <td style="width: 200px">
                            <span style="font-weight: bold">Select Features:</span>
                            <br />
                            <asp:CheckBoxList ID="chkFeatures" CssClass="maintext" runat="server">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <asp:CheckBox ID="chkWidget" runat="server" /></td>
                        <td colspan="2">
                            <span style="font-weight: bold">Select Widgets:</span>
                            <br />
                            <div>
                                <asp:CheckBox ID="chkWidgetlocation" runat="server" />
                                Only change widgets in this location:
                               
                                <asp:ListBox runat="server" ID="ddlwidgetsLocation" SelectionMode="Multiple">
                                    <asp:ListItem Selected="True" Value="topheader">Top banner</asp:ListItem>
                                    <asp:ListItem Value="topbanner">Top content</asp:ListItem>
                                    <asp:ListItem Value="leftbottom">Left</asp:ListItem>
                                    <asp:ListItem Value="righttop">Right</asp:ListItem>
                                    <asp:ListItem Value="bottomright">Bottom</asp:ListItem>
                                    <asp:ListItem Value="pop_under">Pop-under</asp:ListItem>
                                    <asp:ListItem Value="subline">Subline</asp:ListItem>
                                    <asp:ListItem Value="midarticle">Mid Article</asp:ListItem>
                                    <asp:ListItem Value="none">None</asp:ListItem>
                                </asp:ListBox>
                            </div>
                            <br />
                            <div>
                                <asp:CheckBox ID="chkWidgetRandomLoc" runat="server" />
                                Select random widget
                           
                                <asp:ListBox ID="ddl_random_widgets" runat="server" SelectionMode="Multiple">
                                    <asp:ListItem Value="Select">----Select----</asp:ListItem>
                                    <asp:ListItem Value="random_topheader">Top banner</asp:ListItem>
                                    <asp:ListItem Value="random_topbanner">Top content</asp:ListItem>
                                    <asp:ListItem Value="random_left">Left</asp:ListItem>
                                    <asp:ListItem Value="random_right">Right</asp:ListItem>
                                    <asp:ListItem Value="random_bottom">Bottom</asp:ListItem>
                                    <asp:ListItem Value="random_pop_under">Pop-under</asp:ListItem>
                                    <asp:ListItem Value="random_subline">Subline</asp:ListItem>
                                    <asp:ListItem Value="random_midarticle">Mid Article</asp:ListItem>
                                </asp:ListBox>
                                <asp:HiddenField ID="hdnRandomWidgets" runat="server" />
                            </div>
                            <br />
                            <asp:DataGrid ID="dgrdWidgets" runat="server" AutoGenerateColumns="false" GridLines="none"
                                ItemStyle-BackColor="white" OnItemDataBound="dgrdWidgets_ItemDataBound">
                                <Columns>
                                    <asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkWidgets" Text='<%#Eval("ApplicationName") %>' runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlwidgets" runat="server">
                                                <asp:ListItem Selected="True" Value="topheader">Top banner</asp:ListItem>
                                                <asp:ListItem Value="topbanner">Top content</asp:ListItem>
                                                <asp:ListItem Value="leftbottom">Left</asp:ListItem>
                                                <asp:ListItem Value="righttop">Right</asp:ListItem>
                                                <asp:ListItem Value="bottomright">Bottom </asp:ListItem>
                                                <asp:ListItem Value="pop_under">Pop-under</asp:ListItem>
                                                <asp:ListItem Value="subline">Subline</asp:ListItem>
                                                <asp:ListItem Value="midarticle">Mid Article</asp:ListItem>
                                                <asp:ListItem Value="none">None</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td>Name</td>
            <td>
                <asp:CheckBox ID="chkName" runat="server" /></td>
            <td>
                <asp:TextBox ID="txtFullName" runat="server" CssClass="maintext"></asp:TextBox></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>
                <asp:CheckBox ID="chkEmail" runat="server" /></td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                    ErrorMessage="Email formate is not correct" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td style="width: 17%;">Password*<input id="sault" type="hidden" name="sault" runat="server" />
            </td>
            <td>
                <asp:CheckBox ID="chkPwd" runat="server" /></td>
            <td>
                <script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                <asp:TextBox ID="txtPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                    runat="server" CssClass="maintext" Width="140" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;</td>
            <td>
                <div style="width: 100%; color: red" id="dvPwd">
                </div>
                <asp:RequiredFieldValidator ID="rfvPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtPwd"
                    Display="Dynamic" ErrorMessage="please enter password" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" SetFocusOnError="true"
                    ControlToValidate="txtPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                    ValidationGroup="MngUsrVCG" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <div id="Words" style="display:inline;">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width:150px;background-color:#dddddd;height:10px;"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>Confirm&nbsp;password*</td>
            <td></td>
            <td>
                <asp:TextBox ID="txtCPwd" runat="server" CssClass="maintext" Width="140" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd"
                    Display="Dynamic" ErrorMessage="please enter confirm password" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd"
                    Display="Dynamic" ErrorMessage="password & confirm password must match" ValidationGroup="MngUsrVCG"
                    ControlToCompare="txtPwd"></asp:CompareValidator></td>
        </tr>
        <tr>
            <td>Logo
            </td>
            <td>
                <asp:CheckBox ID="chkLogo" runat="server" /></td>
            <td>
                <asp:FileUpload ID="UploadLogo" runat="server"></asp:FileUpload>
            </td>
        </tr>
        <tr>
            <td>Site Genre to be applied
            </td>
            <td>
                <asp:CheckBox ID="chksitegenreapplied" runat="server" />&nbsp;
            </td>
            <td>
                <asp:DropDownList ID="ddlAppliedSiteGenre" CssClass="maintext" runat="server" Width="152px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="txtDisplay">Source</td>
            <td>
                <asp:CheckBox ID="chkSource" runat="server" /></td>
            <td>
                <asp:DropDownList ID="ddlSource" CssClass="maintext" runat="server" Width="79px">
                    <asp:ListItem Value="0">Closed</asp:ListItem>
                    <asp:ListItem Value="1">Open</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="txtDisplay">Status</td>
            <td>
                <asp:CheckBox ID="chkStatus" runat="server" /></td>
            <td>
                <asp:DropDownList ID="ddlStatus" CssClass="maintext" runat="server" Width="79px">
                    <asp:ListItem Value="1">Active</asp:ListItem>
                    <asp:ListItem Value="0">In-Active</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="txtDisplay">
                Add Tracking Code
            </td>
            <td>
                <asp:CheckBox ID="chkIsTracking" runat="server" />
            </td>
            <td>
                <asp:CheckBox ID="chkTrackingCode" runat="server" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                    CssClass="btnBgClass" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                    CausesValidation="false" CssClass="btnBgClass" />
            </td>
        </tr>
    </table>
</asp:Content>

