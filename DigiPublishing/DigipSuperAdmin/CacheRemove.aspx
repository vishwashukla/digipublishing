﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="CacheRemove.aspx.cs" Inherits="DigipSuperAdmin_CacheRemove" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <table style="width: 100%; border-collapse:collapse;" border="1">
            <tr>
                <th align="center">Cache Name</th>
                <th align="center">Action</th>
            </tr>
            <tr>
                <td align="justify">
                    <div>
                        <b>Widget Channels List - </b> 
                        Channels list is cached for widget of each site. 
                        Cache will get updated when admin add / edit / delete a channel.
                    </div>
                </td>
                <td align="center">
                    <asp:Button ID="btnWidgetChannelsList" runat="server" Text="Remove" OnClick="btnWidgetChannelsList_Click" />
                </td>
            </tr>
            <tr>
                <td align="justify">
                    <div>
                        <b>Ad Networks - </b>
                        Ad networks data is cached. Cache will be get updatedafter 
                        superadmin add / edit / delete either an ad network or an ad network rules or 
                        the priorities are changed on the main ad network list.
                    </div>
                </td>
                <td align="center">
                    <asp:Button ID="btnAd_Networks" runat="server" Text="Remove" OnClick="btnAd_Networks_Click" />
                </td>
            </tr>
            <tr>
                <td align="justify">
                    <div>
                        <b>Widgets Data - </b>
                        Video widget, Top, Left, Right and Bottom banner 
                        widgets data is cached. Cache will be get updated when superadmin 
                        add / edit / delete / update widgets using the bulk manage page or edit a site from 
                        view sites page.
                    </div>
                </td>
                <td align="center">
                    <asp:Button ID="btnWidgets" runat="server" Text="Remove" OnClick="btnWidgets_Click" />
                </td>
            </tr>
            <tr>
                <td align="justify">
                    <div>
                        <b>Features - </b>
                        Featurs data is cached. Cache will be get update when 
                        a Feature get updated from frontend or backend.
                    </div>
                </td>
                <td align="center">
                    <asp:Button ID="btnArticles" runat="server" Text="Remove" OnClick="btnArticles_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

