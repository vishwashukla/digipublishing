﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ClickReport : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
    public static string MaxDays = "";
    public static int limitcharacter = 128;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Click Report";
        lblMsg.Text = "";
        if (!IsPostBack)
        {
            string sqlSel = "Select ISNULL(MaxDays_To_StoreData,0) AS MaxDays_To_StoreData,ISNULL(UserAgent,0) AS UserAgent FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            if (dt.Rows.Count > 0)
            {
                chkUserAgent.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["UserAgent"]));
                //dgrdClickReport.Columns[7].Visible = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["UserAgent"]));
                MaxDays = "-" + Convert.ToString(Convert.ToInt32(dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim()) - 1);
            }
            GetCurretnAndLastPurgeDate();
            bindgrid();
        }
    }

    private void GetCurretnAndLastPurgeDate()
    {
        lblCurrentServerDate.Text = DateTime.Now.ToUniversalTime().ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        string strquery = "SELECT * FROM LastPurge_DateTime";
        DataTable DT = dpreport.FillDataTable(strquery);
        if(DT.Rows.Count> 0)
        {
            lblLastPurgeDate.Text =Convert.ToDateTime( DT.Rows[0]["Click_Report"]).ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        }
    }
    private void bindgrid()
    {
        DataTable dt = new DataTable();
        using (SqlConnection connection = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            connection.Open();
            string strSQL = "EXEC BRAND_Select_Widget_Click";           
            SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL, connection);
            dataAdapter.Fill(dt);
        }
        foreach (DataRow dtRow in dt.Rows)
        {
            var limit = dtRow["Referer_URL"].ToString();
            if(!String.IsNullOrEmpty(limit) && limit.Length>127)
                dtRow["Referer_URL"] = limit.Substring(0, limitcharacter);
        }
        Session["dgrdClickReport"] = dt;
        dgrdClickReport.DataSource = dt;
        dgrdClickReport.DataBind();
    }

    private void bindgridtype(string type)
    {
        DataTable dt = new DataTable();
        using (SqlConnection connection = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            connection.Open();
            string strSQL = "EXEC BRAND_Select_Widget_Click_Type @Type='" + type + "'";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL, connection);
            dataAdapter.Fill(dt);
        }
        foreach (DataRow dtRow in dt.Rows)
        {
            var limit = dtRow["Referer_URL"].ToString();
            if (!String.IsNullOrEmpty(limit))
             dtRow["Referer_URL"] = limit.Substring(0, limitcharacter);
        }
        Session["dgrdClickReport"] = dt;
        dgrdClickReport.DataSource = dt;
        dgrdClickReport.DataBind();
    }

    private void bindgridFilterdlisttypeandad(string type, string adid)
    {
        DataTable dt = new DataTable();
        using (SqlConnection connection = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            connection.Open();
            string strSQL = "EXEC BRAND_Select_Widget_Click_Type_AD @Type='" + type + "',@ADID='" + adid + "'";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL, connection);
            dataAdapter.Fill(dt);
        }
        foreach (DataRow dtRow in dt.Rows)
        {
            var limit = dtRow["Referer_URL"].ToString();
            if (!String.IsNullOrEmpty(limit))
                dtRow["Referer_URL"] = limit.Substring(0, limitcharacter);           
        }
        Session["dgrdClickReport"] = dt;
        dgrdClickReport.DataSource = dt;
        dgrdClickReport.DataBind();
    }
    private void bindgridFilterdlisttypeCampaign(string type, string adid,string campaign)
    {
        DataTable dt = new DataTable();
        using (SqlConnection connection = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            connection.Open();
            string strSQL = "EXEC BRAND_Select_Widget_Click_Type_AD @Type='" + type + "',@CampaignType='"+ campaign + "'";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL, connection);
            dataAdapter.Fill(dt);
        }
        foreach (DataRow dtRow in dt.Rows)
        {
            var limit = dtRow["Referer_URL"].ToString();
            if (!String.IsNullOrEmpty(limit))
                dtRow["Referer_URL"] = limit.Substring(0, limitcharacter);
        }
        Session["dgrdClickReport"] = dt;
        dgrdClickReport.DataSource = dt;
        dgrdClickReport.DataBind();
    }



    protected void btnExport_Click(object sender, EventArgs e)
    {
        //string SelectedDate = txtDate.Text;
        DateTime DateSelected = DateTime.Now;//Convert.ToDateTime(SelectedDate);
        TimeSpan span = DateSelected - DateTime.Now.Date;
        int datedifference = span.Days;
        DataTable dt = new DataTable();
        using (SqlConnection connection = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "Brand_Widget_Click_Export";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DateDifference", datedifference);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }
        DataGrid dgrdExport = new DataGrid();
        dgrdExport.DataSource = dt;
        dgrdExport.DataBind();
        string fileName = DateSelected.Year + "-" + DateSelected.Month + "-" + DateSelected.Day + "-WidgetClickReport.xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        Response.Charset = "";
        Response.ContentType = "application/excel";
        StringWriter StringWriter = new System.IO.StringWriter();
        HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);
        dgrdExport.RenderControl(HtmlTextWriter);
        Response.Write(StringWriter.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnPurge_Click(object sender, EventArgs e)
    {
        string sqlstr = "DECLARE @New_tableName varchar(50); "
                        + " SET @New_tableName='Ad_Network_Clicks'+'_'+(convert(varchar,year(GETDATE()))+'_'+convert(varchar,month(GETDATE()))+'_'+convert(varchar,day(GETDATE()))); "
                        + " EXEC('TRUNCATE TABLE '+@New_tableName)";
        string strQuery = "UPDATE LastPurge_DateTime SET Click_Report=GETUTCDATE()";
        try
        {
            dpreport.ExecuteReader(sqlstr);
            dpreport.ExecuteNonQuery(strQuery);          
            lblMsg.Text = "Old records has been deleted.";
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
        GetCurretnAndLastPurgeDate();
        bindgrid();
    }

    protected void dgrdClickReport_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        DataView dv = new DataView((DataTable)Session["dgrdClickReport"]);
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdClickReport.DataSource = dv;
        dgrdClickReport.DataBind();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }

    protected void drpType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpType.SelectedValue == "0")
        {
            drpList.Items.Clear();
            drpList.Visible = false;
            bindgrid();
        }
        else if (drpType.SelectedValue == "ad")
        {
            string sql = "Select * from Brand_AdNetwork order by AdNetwork";
            DataTable dt = dpreport.FillDataTable(sql);
            drpList.Items.Clear();
            drpList.DataSource = dt;
            drpList.DataTextField = "AdNetwork";
            drpList.DataValueField = "AdNetwork_ID";
            drpList.DataBind();
            drpList.Visible = true;
            bindgridtype(drpType.SelectedValue.ToString());
        }
        else if (drpType.SelectedValue == "banner")
        {
            string sql = "Select * from Brand_AppLists order by ApplicationName";
            DataTable dt = dpreport.FillDataTable(sql);
            drpList.Items.Clear();
            drpList.DataSource = dt;
            drpList.DataTextField = "ApplicationName";
            drpList.DataValueField = "id";
            drpList.DataBind();
            drpList.Visible = true;
            bindgridtype(drpType.SelectedValue.ToString());
        }
        else if(drpType.SelectedValue == "Campaign")
        {
            string sql = "BRAND_Select_Widget_Click_Type_CampaignList";
            DataTable dt = dpreport.FillDataTable(sql);
            drpList.Items.Clear();
            drpList.DataSource = dt;
            drpList.DataTextField = "Campaign_Source";
            drpList.DataValueField = "Campaign_Source";
            drpList.Items.Insert(0, new ListItem("ALL", "1"));
            drpList.DataBind();
            drpList.Visible = true;
            bindgridtype(drpType.SelectedValue.ToString());
        }
    }

    //protected void drpList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (drpType.SelectedValue == "Campaign")
    //    {
    //        bindgridFilterdlisttypeCampaign(drpType.SelectedValue.ToString(), drpList.SelectedValue.ToString(),drpList.SelectedItem.Text);
    //    }
    //    else
    //    {
    //        bindgridFilterdlisttypeandad(drpType.SelectedValue.ToString(), drpList.SelectedValue.ToString());
    //    }
    //}

    protected void btnRecorddata_Click(object sender, EventArgs e)
    {
        string strsqlupdate = " UPDATE brand_Record_Data SET UserAgent = case when UserAgent=1 then 0 else 1 END;"
                            + " Select ISNULL(UserAgent,0) AS UserAgent FROM brand_Record_Data;";
        DataTable dt = dp.FillDataTable(strsqlupdate);
        if (dt.Rows.Count > 0)
        {
            chkUserAgent.Checked = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["UserAgent"]));
           // dgrdClickReport.Columns[7].Visible = Convert.ToBoolean(Convert.ToInt32(dt.Rows[0]["UserAgent"]));
        }
        bindgrid();
        lblMsg.Text = "Successfully updated.";
    }

    protected void drpList_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (drpType.SelectedValue.ToUpper() == "CAMPAIGN")
        {
            bindgridFilterdlisttypeCampaign(drpType.SelectedValue.ToString(), drpList.SelectedValue.ToString(), drpList.SelectedItem.Text);
        }
        else
        {
            bindgridFilterdlisttypeandad(drpType.SelectedValue.ToString(), drpList.SelectedValue.ToString());
        }
    }

   private void ChangeGridColor()
    {

       
    }



    protected void dgrdClickReport_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) ||
        (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            int  count = Convert.ToInt16(e.Item.Cells[0].Text.ToString());
            if(count> 0)
            {
                e.Item.BackColor = Color.Red;
            }
        }
    }
}