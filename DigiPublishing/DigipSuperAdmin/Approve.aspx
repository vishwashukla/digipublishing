﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" AutoEventWireup="true" CodeFile="Approve.aspx.cs" Inherits="DigipSuperAdmin_Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="../JS/Approve.js"></script>
    <%=str3%>
    <div runat="server">
        <table border="0" style="background-color:#CCCCCC;width:100%;">
            <tr>
                <td style="background-color:#EAEAEA;">
                    <strong>Action</strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=Region&order=<%=strOrder%>">Region</a></strong>
                </td>
                <%if(Request["action"] != "edit"){%>
                <td style="background-color:#EAEAEA;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=country_id&order=<%=strOrder%>">Country</a></strong>
                </td>
                <%}%>
                <td style="background-color:#EAEAEA;">
                    <strong>Language</strong>
                </td>
                <td style="background-color:#EAEAEA;width:60px;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=channelname&order=<%=strOrder%>">Channel Name <br/>& Url</a></strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong>ID</strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong>Description</strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=Format&order=<%=strOrder%>">Format</a></strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=Channel_Logo&order=<%=strOrder%>">Channel Logo</a></strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong>add to site</strong>
                </td>
                <td style="background-color:#EAEAEA;width:60px;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=Stream_Url&order=<%=strOrder%>">Stream_Url</a></strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=Date&order=<%=strOrder%>">Date</a></strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong><a href="approve.aspx?pno=<%=pageno%>&sort=Approve&order=<%=strOrder%>">Approve</a></strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong>Testing</strong>
                </td>
                <td style="background-color:#EAEAEA;">
                    <strong>Delete</strong>
                </td>
            </tr>
            <%=strHtml.ToString()%>
        </table>
    </div>
    <%=str3%>
</asp:Content>

