﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddToUserChannel.aspx.cs" Inherits="DigipSuperAdmin_AddToUserChannel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="margin: 0px; background-color: #ffffff;">
    <form id="frmAddToUser" runat="server">

        <table border="0" style="background-color: #EAEAEA;width:100%;">
            <tr>
                <td style="background-color: #FFFFFF;">
                    <table border="0" style="width:100%;">
                        <tr>
                            <td>
                                <div style="align-content:center;">
                                    <table border="0" style="width:100%;">
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                                                <asp:Label ID="lblmsg" runat="Server" ForeColor="red"></asp:Label>
                                                <br />
                                                <asp:ListBox ID="lstSites" runat="server" SelectionMode="Multiple" Height="100"></asp:ListBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                                                <asp:RequiredFieldValidator ID="RFVSIte" ControlToValidate="lstSites" InitialValue="" runat="server" ForeColor="Red"  ErrorMessage="Please select sites to add channel"></asp:RequiredFieldValidator>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                            </td>
                                        </tr>
                                        <tr> 
                                            <td colspan="2" style="text-align:center;">
                                                <br />
                                                <br />
                                                <asp:Button ID="btnRemove" runat="server" Text="Remove From All Sites" OnClick="btnRemove_Click" />
                                            </td> 
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
