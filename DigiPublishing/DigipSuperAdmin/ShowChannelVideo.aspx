﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowChannelVideo.aspx.cs" Inherits="DigipSuperAdmin_ShowChannelVideo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Channel Testing</title>
    <style type="text/css"> 
        .clearfloat {
	        clear:both;
            height:0;
            font-size: 1px;
            line-height: 0px;
        }

        #contentElement,.videoContainer {
            float:left;
        }

        #playlist_container {
            float:left;
            width: 110px;
            height: 250px;
            overflow: auto;
            padding-left: 1px;
            padding-top: 1px;
            padding-bottom: 1px;
            background-color: #000000;
            text-align:left;
        }

        #MovieClip1
        {
            min-width:420px!important;
        }

        .vids {
            border-bottom: 1px solid #555;
            float: left;
            margin: 0;
            padding: 5px;
            width: 90px;
            color: #ffffff;
        }

        .vids:hover {
            background: #ffffff;
            color: #000000;
        }

        .title {
            font-weight: bold;
            text-decoration:underline;
        }

        .src {
            font-weight: normal;
            font-size:13px;
        }

        .jspContainer {
            overflow: hidden;
            position: relative;
        }

        .jspPane {
            position: absolute;
            width: 90px;
        }

        .jspVerticalBar {
            background: none repeat scroll 0 0 #222222;
            height: 250px;
            position: absolute;
            right: 0;
            top: 0;
            width: 16px;
        }

        .jspCap {
            display: none;
        }

        .jspTrack {
            background-image: url("Images_Common/scroll-m.png");
            height: 100% !important;
            position: relative;
        }

        .jspDrag {
            background: none repeat scroll 0 0 #FFFFFF;
            cursor: pointer;
            left: 6px;
            max-height: 85px;
            position: relative;
            width: 5px;
        }

        #imageblockVideoWidget{
            display : none; 
            z-index: 9999; 
            float: right; 
            position: absolute; 
            left: 0; 
            top: 0; 
            width: 100%; 
            height: 100%; 
            cursor: pointer;
        }

        #widget_Grid_none p{
            display:none!important;
        }

        video::-internal-media-controls-download-button {
            display:none;
        }

        video::-internal-media-controls-pause-button {
            background-color:red;
        }

        video::-webkit-media-controls-enclosure {
            overflow:hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px); /* Adjust as needed */
            background-color: #000000;
            color: #ffffff;
        }

        video::-webkit-media-controls-current-time-display,video::-webkit-media-controls-time-remaining-display
        {
            color: #ffffff;
        }

        .notice_div{
            position:absolute;
            top:0;
            left:0;
            background-color:white;
            opacity:0.6;
        }
    </style>  
    <script type="text/javascript" src="/JS/jquery.3.1.1.min.js"></script>
    <script type="text/javascript" src="/Widgets/JS/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="/Widgets/JS/mwheelIntent.js"></script>
    <script type="text/javascript" src="/Widgets/JS/jquery.mousewheel.js"></script>  
</head>
<body id="bgcol">
    <form id="frm1" runat="server">
        <div id="myclip" runat="server">
        </div>
        <div class="clearfloat">
        </div>
        <div style="text-align:center;">
            <div id="divbuttons" runat="server" style="padding: 5px; background-color: #FFFFCC;
                border: 1px solid #CC9900">
                <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" />
                <asp:Button ID="btnDecline" runat="server" Text="Decline" OnClick="btnDecline_Click" />
            </div>
        </div>
        <script type="text/javascript">
            var playlistitemindex = 0;
            var playListLength = 0;
            var xmlDocPlaylist = null;
            var strlightcolor = "#ff0000";

            function CallService() {
                var streamURLtest = Channel_streamUrl;
                var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
                xhttp.onreadystatechange = function () {
                    if (xhttp.readyState == 4 && xhttp.status == 200) {
                        xmlDocPlaylist = xhttp.responseXML;
                        playListLength = parseInt(xmlDocPlaylist.getElementsByTagName("item").length);
                    }
                }
                console.log(streamURLtest);
                xhttp.open("GET", streamURLtest, false);
                xhttp.send();

                playlistitemindex = 0;
                AddPlayList();
                videoContent = document.getElementById('contentElement');
                videoContent.play();
                UpdateColors();
            }

            function AddPlayList() {
                var track = xmlDocPlaylist.getElementsByTagName("item");
                var startindex = playlistitemindex;
                var strvideo = "<div class='videoContainer'>";
                for (var i = 0; i < track.length; i++) {
                    if (i >= startindex) {
                        playlistitemindex++;
                        var tracktitle = track[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
                        var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
                        if (tracklocation.indexOf(".mp4") > 0) {
                            var strinnerhtml = "<video style='background:#000' id='contentElement' width='300px' ";
                            strinnerhtml += "height='250px' controls autoplay onended='LoadNextPlayListItem()'><source id='mp4src' src='" + tracklocation + "' type='video/mp4'></source></video>";
                            strvideo += strinnerhtml + "</div>";
                            break;
                        }
                    }
                }
                strvideo += "<div id=\"playlist_container\">"
                for (var i = 0; i < track.length; i++) {
                    var tracktitle = track[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
                    var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
                    var trackdesc = "";
                    if (track[i].getElementsByTagName("description")[0].childNodes.length > 0) {
                        trackdesc = track[i].getElementsByTagName("description")[0].childNodes[0].nodeValue;
                    }
                    if (tracklocation.indexOf(".mp4") > 0) {
                        strvideo += "<div onmouseover=\"this.style.cursor='pointer'\" onclick=\"LoadSelectedPlayListItem(" + i + ");\""
                            + " class=\"vids\" id=\"vids_" + i + "\"><div class=\"ch_info\"><span class=\"title\">" + tracktitle + "</span><br />"
                            + "<span class=\"src\">" + trackdesc + "</span></div></div>";
                    }
                }

                strvideo += "</div>"
                document.getElementById('wrapper').innerHTML = strvideo;
                scrollelement = $('#playlist_container').jScrollPane();
                scrollapi = scrollelement.data('jsp');
                $('#vids_' + (playlistitemindex - 1)).addClass("currentvideo");
                scrollapi.scrollToElement('#vids_' + (playlistitemindex - 1), true, false);
            }

            function playItem() {
                videoContent = document.getElementById('contentElement');
                var srcelement = document.getElementById('mp4src');
                videoContent.pause();
                var track = xmlDocPlaylist.getElementsByTagName("item");
                var startindex = playlistitemindex;
                for (var i = 0; i < track.length; i++) {
                    if (i >= startindex) {
                        playlistitemindex++;
                        var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
                        srcelement.src = tracklocation;
                        break;
                    }
                }
                videoContent.load();
                videoContent.play();
                $('#vids_' + currentindex).removeClass("currentvideo");
                $('#vids_' + currentindex).css("background-color", "#000000").css("color", "#ffffff");
                $('#vids_' + (playlistitemindex - 1)).addClass("currentvideo");
                scrollapi.scrollToElement('#vids_' + (playlistitemindex - 1), true, false);
                UpdateColors();
            }

            function LoadNextPlayListItem() {
                LoadSelectedPlayListItem(playlistitemindex);
            }

            function LoadSelectedPlayListItem(index) {
                currentindex = playlistitemindex - 1;
                playlistitemindex = index;
                if (playListLength == playlistitemindex) {
                    playlistitemindex = 0;
                }
                playItem();
            }

            function UpdateColors() {
                $(".currentvideo").css("background-color", "#000000").css("color", strlightcolor);
                $(".currentvideo").mouseenter(function () {
                    $(this).css("background-color", strlightcolor).css("color", "#000000");
                }).mouseleave(function () {
                    $(this).css("background-color", "#000000").css("color", strlightcolor);
                });

                if (!$(".vids").hasClass("currentvideo"))
                    $(".vids").css("background-color", "#000000").css("color", "#ffffff");
                $(".vids").mouseenter(function () {
                    $(this).css("background-color", strlightcolor).css("color", "#000000");
                }).mouseleave(function () {
                    if (!$(this).hasClass("currentvideo"))
                        $(this).css("background-color", "#000000").css("color", "#ffffff");
                });

                $(".jspDrag").mouseenter(function () {
                    $(this).css("background-color", strlightcolor);
                }).mouseleave(function () {
                    $(this).css("background-color", "#ffffff");
                });
            }
            $(document).ready(function () {
                CallService();
            });
            
        </script>
    </form>
</body>
</html>