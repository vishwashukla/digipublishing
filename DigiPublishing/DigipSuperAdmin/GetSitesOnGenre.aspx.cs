﻿using System;
using System.Data;

public partial class DigipSuperAdmin_GetSitesOnGenre : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Request.QueryString["GenreIDS"]) != "")
        {
            string SiteIDS = GetSiteIDS(Convert.ToString(Request.QueryString["GenreIDS"]));
            Response.Write(SiteIDS);
        }
    }

    protected string GetSiteIDS(string genreIDS)
    {
        string siteids = "";
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_SiteIds_Ongenre";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@GenreIDs", genreIDS);
        DataTable dt = dp.FillDataTable(cmd);

        if (dt.Rows.Count > 0)
        {
            siteids =Convert.ToString(dt.Rows[0][0]);
        }
        return siteids;
    }
}