﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageCountries.aspx.cs" Inherits="DigipSuperAdmin_ManageCountries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
<h4>Manage Countries</h4>
    <asp:MultiView ID="mView" runat="server">
        <asp:View ID="vGrid" runat="server">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add New" OnClick="lnkbtnAdd_Click" Visible="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataGrid ID="dgCountries" runat="server" OnEditCommand="dgCountries_EditCommand" GridLines="None" 
                                AutoGenerateColumns="False"  OnDeleteCommand="dgCountries_DeleteCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="country_id" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="country_name" HeaderText="Country"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="region_name" HeaderText="Region"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="region_id" Visible="false"></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Edit" HeaderText="Edit" Text="Edit"></asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDel" runat="server" Text="Delete" CommandName="Delete" CssClass="toplink" OnClientClick="return confirm('Are you sure you wish to delete?')"
                                            CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate> 
                                    </asp:TemplateColumn>                                      
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>        
                </table>
            </asp:View>
            <asp:View ID="vAddEdit" runat="server">
                <table>  
                    <tr>
                        <td>
                            Country Name*</td>
                        <td>
                            <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox> 
                              <div style="color: red" id="Div1">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                    ControlToValidate="txtCountry" Display="Dynamic" ErrorMessage="please enter country"
                                    ValidationGroup="MngUsrVCG">
                                </asp:RequiredFieldValidator>
                            </div>
                        </td>
                    </tr>                                        
                    <tr>
                        <td>
                            Select Region*
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRegion" runat="server"></asp:DropDownList>
                            <div  style="color: red">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" SetFocusOnError="true"
                                ControlToValidate="ddlRegion" ErrorMessage="please select region" ValidationGroup="MngUsrVCG"
                                 InitialValue="-1">
                            </asp:RequiredFieldValidator>
                            </div>
                       </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnCreate" runat="server" Text="Save" ValidationGroup="MngUsrVCG" CausesValidation="true" OnClick="btnCreate_Click"></asp:Button></td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel"></asp:Button></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnCountryID" runat="server"></asp:HiddenField>
            </asp:View>
    </asp:MultiView>
</asp:Content>
