﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="SiteGenre.aspx.cs" Inherits="DigipSuperAdmin_SiteGenre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <h4>Manage Site Genres</h4>
    <asp:MultiView ID="mView" runat="server">
        <asp:View ID="vGrid" runat="server">
            <table style="width:100%">
                <tr>
                    <td style="text-align:right;">
                        <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add New" OnClick="lnkbtnAdd_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgGenre" runat="server" OnEditCommand="dgGenre_EditCommand" GridLines="None"
                            AutoGenerateColumns="False" OnDeleteCommand="dgGenre_DeleteCommand">
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="genre_name" HeaderText="Site Genre"></asp:BoundColumn>
                                <asp:BoundColumn DataField="m_date" HeaderText="Edited Date"></asp:BoundColumn>
                                <asp:ButtonColumn CommandName="Edit" HeaderText="Edit" Text="Edit"></asp:ButtonColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDel" runat="server" Text="Delete" CommandName="Delete" CssClass="toplink" OnClientClick="return confirm('Are you sure you wish to delete?')"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vAddEdit" runat="server">
            <table>
                <tr>
                    <td>
                        Site Genre Name*
                    </td>
                    <td>
                        <asp:TextBox ID="txtSiteGenre" runat="server"></asp:TextBox>
                        <div style="color: red" id="Div1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtSiteGenre" Display="Dynamic" ErrorMessage="please enter name" ValidationGroup="MngUsrVCG">
                            </asp:RequiredFieldValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCreate" runat="server" Text="Create" ValidationGroup="MngUsrVCG" CausesValidation="true" OnClick="btnCreate_Click"></asp:Button>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="false"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnGenreID" runat="server"></asp:HiddenField>
        </asp:View>
    </asp:MultiView>
</asp:Content>

