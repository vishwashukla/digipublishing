﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Generic;

public partial class DigipSuperAdmin_SitesExportToHTML : System.Web.UI.Page
{
    DBHelper dpSAdmin = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public DataTable dtChannel;
    string domainAll = "";
    string appcode = "";
    private string strDivMidArticle = string.Empty;
    public string BannerIDs = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + "Export To HTML";
        MSG.Text = "";
        if (!IsPostBack)
        {
            rdbAllASPX.Attributes.Add("onclick", "javascript:return pointTo(0)");
            rdbAllHTML.Attributes.Add("onclick", "javascript:return pointTo(1)");
        }
        BindGrid();
    }

    private void BindGrid()
    {
        string strSql = "SELECT domainname,id,DateOfExportToHTML,isnull(HTML_OR_ASPX,0) As HTML_OR_ASPX FROM brand_domainname where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "'";
        DataTable dt = dpSAdmin.FillDataTable(strSql);
        foreach (DataRow dr in dt.Rows)
        {
            if (domainAll == "")
            {
                domainAll = Convert.ToString(dr["id"]);
            }
            else
            {
                domainAll += "^" + Convert.ToString(dr["id"]);
            }
        }
        dgrdDomain.DataSource = dt;
        dgrdDomain.DataBind();
    }

    protected void dgrdDomain_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        DataRowView drv = (DataRowView)e.Item.DataItem;
        RadioButton rdbASPX = (RadioButton)e.Item.FindControl("rdbASPX");
        RadioButton rdbHTML = (RadioButton)e.Item.FindControl("rdbHTML");
        Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
        Label lblComponents = (Label)e.Item.FindControl("lblComponents");
        if (drv != null)
        {
            int intHTML = Convert.ToInt32(Convert.ToString(drv["HTML_OR_ASPX"]));
            int domainid = Convert.ToInt32(Convert.ToString(drv["id"]));
            if (intHTML == 0)
            {
                if (rdbASPX != null)
                {
                    rdbASPX.Checked = true;
                }
            }
            else if (intHTML == 1)
            {
                if (rdbHTML != null)
                {
                    rdbHTML.Checked = true;
                }
            }
            if (rdbASPX != null)
            {
                rdbASPX.Attributes.Add("onclick", "javascript:return updateHtmlorAspx('" + domainid + "','0')");
            }
            if (rdbHTML != null)
            {
                rdbHTML.Attributes.Add("onclick", "javascript:return updateHtmlorAspx('" + domainid + "','1')");
            }
            if (lblDateTime != null)
            {
                lblDateTime.Text = Convert.ToString(drv["DateOfExportToHTML"]);
            }
            if (Convert.ToString(drv["DateOfExportToHTML"]) != "")
            {
                if (lblComponents != null)
                {
                    string DomainName = Convert.ToString(drv["domainname"]);
                    StringBuilder SBComp = new StringBuilder();
                    SBComp.Append("<div style='width:350px;word-wrap:break-word;' >");
                    SBComp.Append("<b>Components: </b><br />Blog,Bookmark,BookMarkSummary,RSS,RSSSummary<br />");
                    XmlDocument xmldoc = new XmlDocument();
                    try
                    {
                        string Path = "../html/" + DomainName + "/Widgets.xml";
                        SBComp.Append("<b>Widgets: </b><br />");
                        ArrayList arrWidget = new ArrayList();
                        xmldoc.Load(Server.MapPath(Path));
                        XmlNodeList xnList = xmldoc.SelectNodes("/Widgets/TopWidgets/TopWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/RightWidgets/RightWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/LeftWidgets/LeftWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/BottomWidgets/BottomWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/NoneWidgets/NoneWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/PopWidgets/PopWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/SublineWidgets/SublineWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        xnList = xmldoc.SelectNodes("/Widgets/VideoWidgets/VideoWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }

                        //issue3707==================================================================
                        xnList = xmldoc.SelectNodes("/Widgets/MidWidgets/MidWidget");
                        if (xnList.Count > 0)
                        {
                            foreach (XmlNode xn in xnList)
                            {
                                string AppName = xn["ApplicationName"].InnerText;
                                arrWidget.Add(AppName);
                            }
                        }
                        arrWidget.Sort();
                        for (int i = 0; i < arrWidget.Count; i++)
                        {
                            if (i == 0)
                            {
                                SBComp.Append(arrWidget[i]);
                            }
                            else
                            {
                                SBComp.Append("," + arrWidget[i]);
                            }
                        }
                        SBComp.Append("<br />");
                    }
                    catch
                    {
                    }
                    try
                    {
                        SBComp.Append("<b>Ad Networks: </b><br />");
                        xmldoc = null;
                        xmldoc = new XmlDocument();
                        string Pathad = "../html/" + DomainName + "/Channels.xml"; ;
                        xmldoc.Load(Server.MapPath(Pathad));
                        string AllAdsString = xmldoc.DocumentElement.ChildNodes[0].InnerText;
                        string AllAdsString1 = AllAdsString.Substring(0, AllAdsString.IndexOf("<ADXML>"));
                        string AllAdsString2 = AllAdsString.Substring(AllAdsString.IndexOf("</ADXML>"), AllAdsString.Length - (AllAdsString.IndexOf("</ADXML>"))).Replace("</ADXML>", "");
                        AllAdsString = AllAdsString1 + "BLANK_TAG" + AllAdsString2;
                        AllAdsString = AllAdsString.Replace("$#$", "^");
                        string[] arrary = AllAdsString.Split('^');
                        ArrayList arr = new ArrayList();
                        for (int i = 1; i < arrary.Length; i = i + 15)
                        {
                            arr.Add(arrary[i]);
                        }
                        arr.Sort();
                        for (int i = 0; i < arr.Count; i++)
                        {
                            if (i == 0)
                            {
                                SBComp.Append(arr[i]);
                            }
                            else
                            {
                                SBComp.Append("," + arr[i]);
                            }
                        }
                    }
                    catch
                    {
                    }
                    SBComp.Append("</div>");
                    lblComponents.Text = Convert.ToString(SBComp);
                }
            }
        }
    }

    protected void dgrdDomain_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Export")
        {
            string domain = Convert.ToString(e.CommandArgument);
            try
            {
                DBHelper dpsite = ReadConfig.getDomainDBHelper(domain);
                ExportWidgetsToXml(domain, dpsite);
                ExportArticleDataToXml(domain, dpsite);
                ExportLatestLinksAndUpdatesToXML(domain, dpsite);
                ExportLinksToXml(domain, dpsite);
                ExportAdsAndChannels(domain);
                ExportRSSFeedToXML(domain, dpsite);
                ExportSingleHomePage(domain, dpsite);
                ExportCalendarToXml(domain, dpsite);
                ExportUpdatesToXml(domain, dpsite);
                string sql = "Update brand_domainname set DateOfExportToHTML=getdate() where id='" + e.Item.Cells[0].Text + "'";
                dpSAdmin.ExecuteNonQuery(sql);
                MSG.Text = "Site successfully exported as HTML.";
                BindGrid();
            }
            catch (Exception Ex)
            {
                MSG.Text = "Site export as HTML Failed." + Ex.Message;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int domainid = Convert.ToInt32(Convert.ToString(hdnDomainID.Value));
        int value = Convert.ToInt32(Convert.ToString(hdnValue.Value));
        string sql = "update brand_domainname set HTML_OR_ASPX='" + value + "' where id='" + domainid + "'";
        dpSAdmin.ExecuteNonQuery(sql);
        MSG.Text = "Site option updated Successfully.";
        BindGrid();
    }

    protected void btnAllAspx_Click(object sender, EventArgs e)
    {
        string[] domainid = domainAll.Split('^');
        for (int i = 0; i < domainid.Length; i++)
        {
            string sql = "update brand_domainname set HTML_OR_ASPX='0' where id='" + domainid[i] + "'";
            dpSAdmin.ExecuteNonQuery(sql);
        }
        MSG.Text = "All Sites option updated Successfully to ASPX";
        BindGrid();
    }

    protected void btnAllHTML_Click(object sender, EventArgs e)
    {
        string[] domainid = domainAll.Split('^');
        for (int i = 0; i < domainid.Length; i++)
        {
            string sql = "update brand_domainname set HTML_OR_ASPX='1' where id='" + domainid[i] + "'";
            dpSAdmin.ExecuteNonQuery(sql);
        }
        MSG.Text = "All Sites option updated Successfully to HTML";
        BindGrid();
    }

    #region Articles
    private void ExportArticleDataToXml(string DomainName, DBHelper dpsite)
    {
        StringBuilder SB = new StringBuilder();
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        string sql = "SELECT post_id AS PostID,post_title AS Title,post_tags AS Tags,"
                        + "post_description AS Description FROM BRAND_BLOG_POSTS"
                        + " WHERE Active_Inactive=1 ORDER BY SortOrder DESC";
        DataTable dt = dpsite.FillDataTable(sql);
        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Articles>");
        int blogsperpage = 10;
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string posttitle = Convert.ToString(dr["Title"]);
                posttitle = utils.formatStringForURLSelect(posttitle);
                posttitle = utils.GeneratePostTitleForURL(posttitle);

                string strPostTitle = utils.formatStringForURLSelect(Convert.ToString(dr["Title"]));
                if (strPostTitle.Length > 50)
                {
                    strPostTitle = strPostTitle.Substring(0, 49) + "...";
                }
                string description = Convert.ToString(dr["Description"]).Replace("&quot;", "\"").Replace("&rsquo;", "'");
                description = description.Replace("\"", "'");
                if (appcode == "")
                {
                    string imagealt = "";
                    string filepath = Server.MapPath(ReadConfig.LogoPath);
                    FileInfo fi = new FileInfo(filepath);
                    if (fi.Exists)
                    {
                        string filename = fi.Name;
                        string ext = fi.Extension;
                        imagealt = filename.Replace(ext, "").Replace("-", " ").Replace("_", " ");
                        appcode = "<img src='http://" + DomainName + "/config/" + DomainName + "/" + ReadConfig.GetLogo(DomainName) + "' id='imgLogo' alt='" + imagealt + "' />";
                    }
                }
                SB.AppendLine("<Article>");
                SB.AppendLine("<PostID>" + Convert.ToString(dr["PostID"]) + "</PostID>");
                SB.AppendLine("<Title><![CDATA[" + utils.formatStringForURLSelect(Convert.ToString(dr["Title"])) + "]]></Title>");
                SB.AppendLine("<TitleForUrl><![CDATA[" + posttitle + "]]></TitleForUrl>");
                SB.AppendLine("<TitleForLink><![CDATA[" + strPostTitle + "]]></TitleForLink>");
                SB.AppendLine("<Tags><![CDATA[" + Convert.ToString(dr["Tags"]) + "]]></Tags>");
                SB.AppendLine("<Description><![CDATA[" + description + "]]></Description>");
                SB.AppendLine("<PostPerPage><![CDATA[" + blogsperpage + "]]></PostPerPage>");
                SB.AppendLine("<Logo><![CDATA[" + appcode + "]]></Logo>");
                SB.AppendLine("</Article>");
            }
        }
        SB.AppendLine("</Articles>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Articles.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Articles.xml"));
        }
    }
    #endregion

    #region Links_Updates_MostPopular
    private void ExportLatestLinksAndUpdatesToXML(string DomainName, DBHelper dpsite)
    {
        StringBuilder SB = new StringBuilder();
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Root>");
        SB.AppendLine("<Logo><![CDATA[http://" + DomainName + "/config/" + DomainName + "/" + ReadConfig.GetLogo(DomainName) + "]]></Logo>");
        SB.AppendLine("<Theme><![CDATA[http://" + DomainName + "/App_Themes/" + ReadConfig.GetTheme(DomainName) + "/base.css" + "]]></Theme>");
        //RK
        string sqlLink = "select Id,BK_Title,BK_URL,isnull(nofollow,0) as nofollow,isnull(noindex,0) as noindex,isnull(noopener,0) as noopener,isnull(noreferrer,0) as noreferrer,Blocker,BlockerValue,SameIP_Status,SameIP_Value from brand_bookmark WHERE Status='True' AND BK_Status<>'0' order by bk_sortorder desc";
        DataTable dtLink = dpsite.FillDataTable(sqlLink);

        string strSqlLinkSettings = "select isnull(RandomiseLinksInSummary,0) as RandomiseLinksInSummary,case when isnull(NumberOfLinksInSummary,0)>0 then NumberOfLinksInSummary else 30 end as NumberOfLinksInSummary FROM brand_settings";
        DataTable dtLinkSettings = dpsite.FillDataTable(strSqlLinkSettings);

        if (dtLinkSettings.Rows.Count > 0)
        {
            SB.AppendLine("<LinksSetting>");
            SB.AppendLine("<Random>" + dtLinkSettings.Rows[0]["RandomiseLinksInSummary"].ToString() + "</Random>");
            SB.AppendLine("<Top>" + dtLinkSettings.Rows[0]["NumberOfLinksInSummary"].ToString() + "</Top>");
            SB.AppendLine("</LinksSetting>");
        }

        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        string strsqlHtmloraspx = "select id from brand_domainname where domainname='" + DomainName + "'";
        DataTable dtHtmloraspx = dpSA.FillDataTable(strsqlHtmloraspx);
        if (dtLink.Rows.Count > 0)
        {
            foreach (DataRow dr in dtLink.Rows)
            {
                string LinkTitle = Convert.ToString(dr["BK_Title"]);
                if (LinkTitle.Length > 20)
                {
                    LinkTitle = LinkTitle.Substring(0, 19) + "...";
                }
                int nofollow = Convert.ToInt32(dr["nofollow"].ToString());
                int noindex = Convert.ToInt32(dr["noindex"].ToString());
                int noopener = Convert.ToInt32(dr["noopener"].ToString());
                int noreferrer = Convert.ToInt32(dr["noreferrer"].ToString());
                string strrel = " rel=\"";
                if (nofollow != 0)
                {
                    strrel += "nofollow";
                }
                if (noindex != 0)
                {
                    strrel += strrel == " rel=\"" ? "noindex" : " noindex";
                }
                if (noopener != 0)
                {
                    strrel += strrel == " rel=\"" ? "noopener" : " noopener";
                }
                if(noreferrer != 0)
                {
                    strrel += strrel == " rel=\"" ? "noreferrer" : " noreferrer";
                }
                strrel += "\"";

                int index = dtLink.Rows.IndexOf(dr);
                string bookmarkLink = string.Empty;
                bookmarkLink = "<img id=\"" + index + "_SummaryLinkBlockImage\" onclick=\"RecordClickLink(" + Convert.ToString(dr["Id"]) + ", " + dtHtmloraspx.Rows[0][0].ToString() + ", 1)\" src=\"/Images_Common/1x2.gif\" alt=\"1x2\" style=\"float:right;display:none;z-index:9999;position:absolute;left:0;top:0;width:100%;height:100%;\">";
                bookmarkLink += "<a id=\"SummaryLink_" + index + "\" target='_blank' href='" + Convert.ToString(dr["BK_URL"]) + "'" + strrel + " onclick=\"javascript: GetLinkData('SummaryLinkData_', 'SummaryCurrentClicksLink_', '_SummarySameIPClicksLink', '_SummaryLinkBlockImage', 'SummaryLink_', '" + Convert.ToString(dr["Id"]) + "', " + index + ")\"><u>" + LinkTitle + "</u></a>";
                bookmarkLink += "<span id=\"SummaryLinkData_" + index + "\" style=\"display:none;\">" + Convert.ToString(dr["Blocker"]) + "," + Convert.ToString(dr["BlockerValue"]) + "," + Convert.ToString(dr["SameIP_Status"]) + "," + Convert.ToString(dr["SameIP_Value"]) + "," + dtHtmloraspx.Rows[0][0].ToString() + "</span>";
                bookmarkLink += "<span id=\"SummaryCurrentClicksLink_" + index + "\" style=\"display:none;\"></span>";
                bookmarkLink += "<span id=\"" + index + "_SummarySameIPClicksLink\" style=\"display:none;\"></span>";
                SB.AppendLine("<Links>");
                SB.AppendLine("<ALinkID><![CDATA[" + Convert.ToString(dr["Id"]) + "]]></ALinkID>");
                SB.AppendLine("<noindex><![CDATA[" + noindex + "]]></noindex>");
                SB.AppendLine("<ALink><![CDATA[" + bookmarkLink + "]]></ALink>");
                SB.AppendLine("</Links>");
            }
        }
        string sqlUpdate = "select feed_id,Brand_Rss.title "
                            + "from Brand_Rss left join dbo.brand_user on Brand_Rss.c_by = "
                            + "brand_user.uid where feed_status<>'0'";
        DataTable dtUpdate = dpsite.FillDataTable(sqlUpdate);
        if (dtUpdate.Rows.Count > 0)
        {
            foreach (DataRow dr in dtUpdate.Rows)
            {
                string UpdateTitle = utils.formatStringForDBSelect(Convert.ToString(dr["title"]));
                if (UpdateTitle.Length > 30)
                    UpdateTitle = UpdateTitle.Substring(0, 29) + "...";
                SB.AppendLine("<Updates>");
                SB.AppendLine("<UpdateID><![CDATA[" + Convert.ToString(dr["feed_id"]) + "]]></UpdateID>");
                SB.AppendLine("<UpdateTitle><![CDATA[" + UpdateTitle + "]]></UpdateTitle>");
                SB.AppendLine("</Updates>");
            }
        }


        string sqlMostPopular = "SELECT TOP 5 post_title,post_id FROM BRAND_BLOG_POSTS WHERE Active_Inactive=1 ORDER BY c_date DESC";
        DataTable dtMostPopular = dpsite.FillDataTable(sqlMostPopular);
        if (dtMostPopular.Rows.Count > 0)
        {
            foreach (DataRow dr in dtMostPopular.Rows)
            {
                if (Convert.ToString(dr["post_title"]) != "")
                {
                    string strTitle = utils.formatStringForURLSelect(dr["post_title"].ToString());
                    string strURL = "http://" + DomainName + "/html/index.html?featureid=" + Convert.ToString(dr["post_id"]);
                    SB.AppendLine("<Features>");
                    SB.AppendLine("<FeatureLink><![CDATA[" + "<a href='" + strURL + "'><u>" + strTitle + "</u></a>" + "]]></FeatureLink>");
                    SB.AppendLine("</Features>");
                }
            }
        }

        SB.AppendLine("</Root>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "LatestLinksUpdates.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "LatestLinksUpdates.xml"));
        }
    }

    private void ExportLinksToXml(string DomainName, DBHelper dpsite)
    {
        StringBuilder SB = new StringBuilder();
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        string sql = "select Id,BK_URL,BK_Title,BK_Desc,BK_Tags,isnull(nofollow,0) as nofollow,isnull(noindex,0) as noindex,isnull(noopener,0) as noopener,isnull(noreferrer,0) as noreferrer,Blocker,BlockerValue,SameIP_Status,SameIP_Value from brand_bookmark "
                  + "WHERE Status='True' AND BK_Status<>'0' "
                  + "order by BK_SortOrder desc";
        DataTable dt = dpsite.FillDataTable(sql);

        string strSqlLinkSettings = "select isnull(RandomiseLinksInLink,0) as RandomiseLinksInLink,case when isnull(NumberOfLinksinlink,0)>0 then NumberOfLinksinlink else (SELECT COUNT(Id) FROM  brand_bookmark WHERE Status='True' AND BK_Status<>'0') end as NumberOfLinksinlink FROM brand_settings";
        DataTable dtLinkSettings = dpsite.FillDataTable(strSqlLinkSettings);

        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        string strsqlHtmloraspx = "select id from brand_domainname where domainname='" + DomainName + "'";
        DataTable dtHtmloraspx = dpSA.FillDataTable(strsqlHtmloraspx);

        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Links>");
        if(dtLinkSettings.Rows.Count > 0)
        {
            SB.AppendLine("<LinksSetting>");
            SB.AppendLine("<Random>" + dtLinkSettings.Rows[0]["RandomiseLinksInLink"].ToString() + "</Random>");
            SB.AppendLine("<Top>" + dtLinkSettings.Rows[0]["NumberOfLinksinlink"].ToString() + "</Top>");
            SB.AppendLine("</LinksSetting>");
        }
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                dr["BK_URL"] = utils.formatStringForDBSelect(Convert.ToString(dr["BK_URL"]));

                dr["BK_Title"] = utils.formatStringForDBSelect(Convert.ToString(dr["BK_Title"]));
                string siteMapTitle = Convert.ToString(dr["BK_Title"]);
                if (Convert.ToString(dr["BK_Title"]).Length > 20)
                    dr["BK_Title"] = Convert.ToString(dr["BK_Title"]).Substring(0, 19) + "...";

                dr["BK_Desc"] = utils.formatStringForDBSelect(Convert.ToString(dr["BK_Desc"]));
                if (Convert.ToString(dr["BK_Desc"]).Length > 180)
                    dr["BK_Desc"] = Convert.ToString(dr["BK_Desc"]).Substring(0, 179) + "...";

                dr["BK_Tags"] = utils.formatStringForDBSelect(Convert.ToString(dr["BK_Tags"]));
                if (Convert.ToString(dr["BK_Tags"]).Length > 20)
                    dr["BK_Tags"] = Convert.ToString(dr["BK_Tags"]).Substring(0, 19) + "...";

                string bookmarkLink = string.Empty;
                int nofollow = Convert.ToInt32(dr["nofollow"].ToString());
                int noindex = Convert.ToInt32(dr["noindex"].ToString());
                int noopener = Convert.ToInt32(dr["noopener"].ToString());
                int noreferrer = Convert.ToInt32(dr["noreferrer"].ToString());
                string strrel = " rel=\"";
                if (nofollow != 0)
                {
                    strrel += "nofollow";
                }
                if (noindex != 0)
                {
                    strrel += strrel == " rel=\"" ? "noindex" : " noindex";
                }
                if (noopener != 0)
                {
                    strrel += strrel == " rel=\"" ? "noopener" : " noopener";
                }
                if (noreferrer != 0)
                {
                    strrel += strrel == " rel=\"" ? "noreferrer" : " noreferrer";
                }
                strrel += "\"";

                int index = dt.Rows.IndexOf(dr);
                bookmarkLink = "<img id=\"" + index + "_MainLinkBlockImage\" onclick=\"RecordClickLink(" + Convert.ToString(dr["Id"]) + ", " + dtHtmloraspx.Rows[0][0].ToString() + ", 1)\" src=\"/Images_Common/1x2.gif\" alt=\"1x2\" style=\"float:right;display:none;z-index:9999;position:absolute;left:0;top:0;width:100%;height:100%;\">";
                bookmarkLink += "<a id=\"MainLink_" + index + "\" onclick=\"javascript: GetLinkData('MainLinkData_', 'MainCurrentClicksLink_', '_MainSameIPClicksLink', '_MainLinkBlockImage', 'MainLink_', '" + Convert.ToString(dr["Id"]) + "', " + index + ")\" target='_blank' href='" + Convert.ToString(dr["BK_URL"]) + "'" + strrel + "><b>" + Convert.ToString(dr["BK_Title"]) + "</b>";
                bookmarkLink += "</a><br/>" + dr["BK_Desc"] ;
                bookmarkLink += "<span id=\"MainLinkData_" + index + "\" style=\"display:none;\">" + Convert.ToString(dr["Blocker"]) + "," + Convert.ToString(dr["BlockerValue"]) + "," + Convert.ToString(dr["SameIP_Status"]) + "," + Convert.ToString(dr["SameIP_Value"]) + "," + dtHtmloraspx.Rows[0][0].ToString() + "</span>";
                bookmarkLink += "<span id=\"MainCurrentClicksLink_" + index + "\" style=\"display:none;\"></span>";
                bookmarkLink += "<span id=\"" + index + "_MainSameIPClicksLink\" style=\"display:none;\"></span>";

                string sitemaplink = "<a target='_blank' href='" + Convert.ToString(dr["BK_URL"]) + "'" + strrel + ">" + siteMapTitle + "</a>";
                SB.AppendLine("<Link>");
                SB.AppendLine("<ALinkID><![CDATA[" + Convert.ToString(dr["Id"]) + "]]></ALinkID>");
                SB.AppendLine("<noindex><![CDATA[" + noindex + "]]></noindex>");
                SB.AppendLine("<Alink><![CDATA[" + bookmarkLink + "]]></Alink>");
                SB.AppendLine("<SiteMaplink><![CDATA[" + sitemaplink + "]]></SiteMaplink>");
                SB.AppendLine("</Link>");
            }
        }
        SB.AppendLine("</Links>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Links.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Links.xml"));
        }
    }

    private void ExportUpdatesToXml(string DomainName, DBHelper dpsite)
    {
        StringBuilder SB = new StringBuilder();
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        string sql = "select feed_id,Brand_Rss.title,description,link,Brand_Rss.rssTags "
                     + "from Brand_Rss left join dbo.brand_user on Brand_Rss.c_by = brand_user.uid "
                     + "where feed_status<>'0'";
        DataTable dt = dpsite.FillDataTable(sql);
        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Updates>");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                dr["title"] = utils.formatStringForDBSelect(Convert.ToString(dr["title"]));

                //RSS Feeds
                //DataTable dtRss = new DataTable();
                //try
                //{
                //    dtRss = utils.LoadRSS(Convert.ToString(dr["link"]), utils.formatStringForDBSelect(Convert.ToString(dr["title"])));
                //}
                //catch
                //{
                //}
                //////////////////////////////////////////////////////////////////////////////////////////////////
                //if (!dtRss.Columns.Contains("link"))
                //{
                //    dtRss.Columns.Add("link", typeof(string));
                //    dtRss.Columns.Add("pubdate", typeof(string));
                //    dtRss.Columns.Add("creator", typeof(string));
                //    dtRss.Columns.Add("description", typeof(string));
                //    dtRss.Columns.Add("title", typeof(string));
                //}
                //if (dtRss.Rows.Count <= 0)
                //{
                //    DataRow drdtRss = dtRss.NewRow();
                //    drdtRss["link"] = "";
                //    drdtRss["pubdate"] = "";
                //    drdtRss["creator"] = "";
                //    drdtRss["description"] = "Feed is not available";
                //    drdtRss["title"] = "";
                //    dtRss.Rows.Add(drdtRss);
                //}

                //RSS
                string UpdatetitleForLink = Convert.ToString(dr["title"]);
                if (UpdatetitleForLink.Length > 30)
                {
                    UpdatetitleForLink = UpdatetitleForLink.Substring(0, 29) + "...";
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////
                SB.AppendLine("<Update>");
                SB.AppendLine("<UpdateID>" + Convert.ToString(dr["feed_id"]) + "</UpdateID>");
                SB.AppendLine("<UpdatetitleForLink><![CDATA[" + UpdatetitleForLink + "]]></UpdatetitleForLink>");
                SB.AppendLine("<UpdateTitle><![CDATA[" + Convert.ToString(dr["title"]) + "]]></UpdateTitle>");
                SB.AppendLine("<Description><![CDATA[" + Convert.ToString(dr["description"]) + "]]></Description>");
                SB.AppendLine("<UpdateLink><![CDATA[" + Convert.ToString(dr["link"]) + "]]></UpdateLink>");
                SB.AppendLine("<UpdateTags><![CDATA[" + Convert.ToString(dr["rssTags"]) + "]]></UpdateTags>");
                //foreach (DataRow drRss in dtRss.Rows)
                //{
                //    SB.AppendLine("<Item>");
                //    SB.AppendLine("<link><![CDATA[" + Convert.ToString(drRss["link"]) + "]]></link>");
                //    SB.AppendLine("<pubdate><![CDATA[" + Convert.ToString(drRss["pubdate"]) + "]]></pubdate>");
                //    SB.AppendLine("<creator><![CDATA[" + Convert.ToString(drRss["creator"]) + "]]></creator>");
                //    SB.AppendLine("<description><![CDATA[" + Server.HtmlEncode(Convert.ToString(drRss["description"])) + "]]></description>");
                //    SB.AppendLine("<title><![CDATA[" + Convert.ToString(drRss["title"]) + "]]></title>");
                //    SB.AppendLine("</Item>");
                //}
                SB.AppendLine("</Update>");
            }
        }
        SB.AppendLine("</Updates>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Updates.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Updates.xml"));
        }
    }
    #endregion

    #region Channels
    private void ExportAdsAndChannels(string DomainName)
    {
        StringBuilder SB = new StringBuilder();
        StringBuilder SBPanel = new StringBuilder();
        int i = 0;
        DataTable dt = GetAPI(DomainName);
        string AllAdsString = getNetworksFromDB(DomainName);
        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Root>");
        SB.AppendLine("<AllAdsString><![CDATA[" + AllAdsString + "]]></AllAdsString>");
        SBPanel.Append("<div id=\"disply\" >");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                i++;
                string strFLVPATH = Convert.ToString(dr["ChannelStreamURL"]);
                string MyFileURL = "";
                string strChannelFormat = Convert.ToString(dr["ChannelFormat"]);
                if (strChannelFormat.ToUpper() == "CODE")
                {
                    string strcodestream = strFLVPATH.Replace("\"", "'").Trim();
                    strcodestream = strFLVPATH.Replace("/n", "").Trim();
                    strcodestream = "<div id='mediadivcode'>" + strcodestream.Trim() + "</div>";
                    strFLVPATH = strcodestream.Replace("\"", "'").Trim();
                }
                else if (strChannelFormat.ToUpper() == "RTMP")
                {
                    string strDdlValue = strFLVPATH;
                    int slpos = strDdlValue.Trim().LastIndexOf("/");
                    slpos = slpos + 1;
                    strFLVPATH = strDdlValue.Trim().Substring(0, slpos).Trim();
                    MyFileURL = strDdlValue.Trim().Substring(slpos).Trim();
                }

                SB.AppendLine("<Channel>");
                SB.AppendLine("<ChannelID><![CDATA[" + Convert.ToString(dr["ChannelID"]) + "]]></ChannelID>");
                SB.AppendLine("<ChannelName><![CDATA[" + Convert.ToString(dr["ChannelName"]) + "]]></ChannelName>");
                SB.AppendLine("<ChannelStreamURL><![CDATA[" + strFLVPATH + "]]></ChannelStreamURL>");
                SB.AppendLine("<ChannelLogo><![CDATA[" + Convert.ToString(dr["ChannelLogo"]) + "]]></ChannelLogo>");
                SB.AppendLine("<ChannelDescription><![CDATA[" + Server.HtmlDecode(Convert.ToString(dr["ChannelDescription"])) + "]]></ChannelDescription>");
                SB.AppendLine("<ChannelFormat><![CDATA[" + strChannelFormat + "]]></ChannelFormat>");
                SB.AppendLine("<ChannelPathWithID><![CDATA[" + Convert.ToString(dr["ChannelPathWithID"]) + "]]></ChannelPathWithID>");
                SB.AppendLine("<PlayList><![CDATA[" + Convert.ToString(dr["PlayList"]) + "]]></PlayList>");
                SB.AppendLine("<MyFileURL><![CDATA[" + MyFileURL + "]]></MyFileURL>");
                SB.AppendLine("</Channel>");

                string strChannelIDCurrent = Convert.ToString(dr["ChannelID"]);
                string strChannelIDNext = Convert.ToString(dr["ChannelID"]);
                string title = Convert.ToString(dr["ChannelName"]).Trim().Replace("'", "&rsquo;");
                string logopath = Convert.ToString(dr["ChannelLogo"]);
                string playlist = Convert.ToString(dr["PlayList"]);

                if (i < dt.Rows.Count)
                {
                    strChannelIDNext = Convert.ToString(dt.Rows[i]["ChannelID"]);
                }
                else
                {
                    strChannelIDNext = Convert.ToString(dt.Rows[0]["ChannelID"]);
                }
                string isShowPlayList = Convert.ToString(dr["PlayList"]);

                string ChannelPathWithID = Convert.ToString(dr["ChannelPathWithID"]).Trim().Replace("\"", "");
                SBPanel.Append("<div class=\"panel\" >");
                SBPanel.Append("<img alt=\"logoChannel\" id = \"logoChannel\" style =\"border:0px\" src='" + logopath + "' height=\"60\" overflow: hidden; float: left; margin: 10px;width=\"60\" onmouseover=\"Tip('<table bgcolor=white><tr valign=center><td height=60 style=background-color:#E3E3E3;><b>" + title + "</b></td></tr></table>');this.style.cursor='pointer'\" onmouseout=\"UnTip()\" onclick=\"ShowChannel(" + strChannelIDCurrent + ")\"  />");
                SBPanel.Append("</div>");
            }
        }
        SBPanel.Append("</div>");
        SB.AppendLine("<Panel><![CDATA[" + Convert.ToString(SBPanel) + "]]></Panel>");
        SB.AppendLine("</Root>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Channels.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Channels.xml"));
        }
    }

    private DataTable GetAPI(string DomainName)
    {
        DataTable dtSiteID = dpSAdmin.FillDataTable("Select id from brand_domainname where domainname='" + DomainName + "'");
        int siteID = 0;
        if (dtSiteID.Rows.Count > 0)
        {
            siteID = Convert.ToInt32(Convert.ToString(dtSiteID.Rows[0]["id"]));
        }
        return CallAPI(DomainName, siteID);
    }

    private DataTable CallAPI(string strDomainName, int SiteID)
    {
        string strIPaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
        dtChannel = new DataTable();
        dtChannel.Columns.Add("ChannelID", typeof(string));
        dtChannel.Columns.Add("ChannelName", typeof(string));
        dtChannel.Columns.Add("ChannelStreamURL", typeof(string));
        dtChannel.Columns.Add("ChannelLogo", typeof(string));
        dtChannel.Columns.Add("ChannelDescription", typeof(string));
        dtChannel.Columns.Add("ChannelFormat", typeof(string));
        dtChannel.Columns.Add("ChannelPathWithID", typeof(string));
        dtChannel.Columns.Add("PlayList", typeof(string));

        dtChannel = produceUserChannelsdiv(strDomainName, SiteID);

        foreach (DataRow dr in dtChannel.Rows)
        {
            if (Convert.ToString(dr["ChannelFormat"]) != null)
                dr["ChannelPathWithID"] = Convert.ToString(dr["ChannelStreamurl"]) + "," + Convert.ToInt32(dr["ChannelID"]) + "," + Convert.ToString(dr["ChannelFormat"]);
        }
        return dtChannel;
    }

    private DataTable produceUserChannelsdiv(string strDomainName, int SiteID)
    {
        DataTable dtChannels = new DataTable();
        string struserid = string.Empty;
        string genre = string.Empty;
        string lang = string.Empty;
        string strformat = string.Empty;

        DBCommand cmdchannel = new DBCommand();
        cmdchannel.CommandText = "Sp_Select_Channels_Ad_rotator";
        cmdchannel.CommandType = CommandType.StoredProcedure;
        cmdchannel.Parameters.Add("@SiteID", Convert.ToString(SiteID));
        dtChannels = dpSAdmin.FillDataTable(cmdchannel);

        if (dtChannels.Rows.Count > 0)
        {
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                DataRow dr = dtChannel.NewRow();
                string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();
                if ((Convert.ToString(strChannel_Format).IndexOf("flash") >= 0 || Convert.ToString(strChannel_Format).IndexOf("flv") >= 0) && (Convert.ToString(drChannels["PlayList"]) == "0"))
                {
                    strChannel_Format = "FLASH";
                    dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                    dr["ChannelStreamURL"] = Convert.ToString(drChannels["stream_url"]);
                    dr["ChannelLogo"] = "http://" + strDomainName + "/images_common/" + Convert.ToString(drChannels["channel_logo_path"]) + "";
                    dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                    dr["ChannelFormat"] = Convert.ToString(strChannel_Format);
                    dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                    dtChannel.Rows.Add(dr);
                }
            }
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                DataRow dr = dtChannel.NewRow();
                string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();
                if ((Convert.ToString(strChannel_Format).IndexOf("flash") >= 0 || Convert.ToString(strChannel_Format).IndexOf("flv") >= 0) && (Convert.ToString(drChannels["PlayList"]) == "1"))
                {
                    strChannel_Format = "FLASH";
                    dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                    dr["ChannelStreamURL"] = "http://" + strDomainName + "/Widgets/" + "GetPlaylist.aspx?Id=" + Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelLogo"] = "http://" + strDomainName + "/images_common/" + Convert.ToString(drChannels["channel_logo_path"]) + "";
                    dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                    dr["ChannelFormat"] = Convert.ToString(strChannel_Format);
                    dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                    dtChannel.Rows.Add(dr);
                }
            }
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                DataRow dr = dtChannel.NewRow();
                string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();
                if ((Convert.ToString(strChannel_Format).IndexOf("flash") >= 0 || Convert.ToString(strChannel_Format).IndexOf("flv") >= 0) && (Convert.ToString(drChannels["PlayList"]) == "2"))
                {
                    strChannel_Format = "FLASH";
                    dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                    dr["ChannelStreamURL"] = Convert.ToString(drChannels["stream_url"]);
                    dr["ChannelLogo"] = "http://" + strDomainName + "/images_common/" + Convert.ToString(drChannels["channel_logo_path"]) + "";
                    dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                    dr["ChannelFormat"] = Convert.ToString(strChannel_Format);
                    dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                    dtChannel.Rows.Add(dr);
                }
            }
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                DataRow dr = dtChannel.NewRow();
                string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();
                if (Convert.ToString(strChannel_Format).IndexOf("rtmp") >= 0)
                {
                    strChannel_Format = "RTMP";
                    dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                    dr["ChannelStreamURL"] = Convert.ToString(drChannels["stream_url"]);
                    dr["ChannelLogo"] = "http://" + strDomainName + "/images_common/" + Convert.ToString(drChannels["channel_logo_path"]) + "";
                    dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                    dr["ChannelFormat"] = Convert.ToString(strChannel_Format);
                    dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                    dtChannel.Rows.Add(dr);
                }
            }
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                DataRow dr = dtChannel.NewRow();
                string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();
                if (Convert.ToString(strChannel_Format).IndexOf("windows") >= 0 || Convert.ToString(strChannel_Format).IndexOf("wmv") >= 0)
                {
                    strChannel_Format = "WMV";
                    dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                    if (Convert.ToString(drChannels["PlayList"]) == "1")
                        dr["ChannelStreamURL"] = "http://" + strDomainName + "/" + "getplaylist.aspx?Id=" + Convert.ToString(drChannels["channel_id"]);
                    else
                        dr["ChannelStreamURL"] = Convert.ToString(drChannels["stream_url"]);

                    dr["ChannelLogo"] = "http://" + strDomainName + "/images_common/" + Convert.ToString(drChannels["channel_logo_path"]) + "";
                    dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                    dr["ChannelFormat"] = Convert.ToString(strChannel_Format);
                    dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                    dtChannel.Rows.Add(dr);
                }
            }
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                DataRow dr = dtChannel.NewRow();
                string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();
                if (Convert.ToString(strChannel_Format).IndexOf("flash") < 0 && Convert.ToString(strChannel_Format).IndexOf("flv") < 0)
                {
                    if (Convert.ToString(strChannel_Format).IndexOf("rtmp") < 0)
                    {
                        if (Convert.ToString(strChannel_Format).IndexOf("windows") < 0 && Convert.ToString(strChannel_Format).IndexOf("wmv") < 0)
                        {
                            if (Convert.ToString(strChannel_Format).IndexOf("rm") >= 0 || Convert.ToString(strChannel_Format).IndexOf("real") >= 0)
                            {
                                strChannel_Format = "RM";
                            }
                            if (Convert.ToString(strChannel_Format).IndexOf("q") > 0)
                            {
                                strChannel_Format = "Q";
                            }
                            dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                            dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                            if (Convert.ToString(drChannels["PlayList"]) == "1")
                                dr["ChannelStreamURL"] = "http://" + strDomainName + "/" + "getplaylist.aspx?Id=" + Convert.ToString(drChannels["channel_id"]);
                            else
                                dr["ChannelStreamURL"] = Convert.ToString(drChannels["stream_url"]);
                            dr["ChannelLogo"] = "http://" + strDomainName + "/images_common/" + Convert.ToString(drChannels["channel_logo_path"]) + "";
                            dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                            dr["ChannelFormat"] = Convert.ToString(strChannel_Format);
                            dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                            dtChannel.Rows.Add(dr);
                        }
                    }
                }
            }
        }
        return dtChannel;
    }
    #endregion

    #region AD Networks
    private string getNetworksFromDB(string DomainName)
    {
        string SiteId = GetSiteID(DomainName);
        DataTable TagData = new DataTable();
        DBCommand cmd = new DBCommand();
        string sqlAll = "SELECT a.AdNetwork_ID AS AdNetwork_ID,a.AdNetwork,b.Tag,b.priority,"
                        + "b.Referal,b.Not_Referal,b.Browser,b.Country,"
                        + "b.Plugin,b.OS,isnull(b.Blocker,0) as Blocker,isnull(b.SameIP_Status,0) as SameIP_Status,"
                        + "isnull(b.SameIP_Value,0) as SameIP_Value,isnull(b.SameIP_Days,0) as SameIP_Days"
                        + ",isnull(b.BlockerValue,0) as BlockerValue"
                        + ",isnull(b.Ad_click_play,0) as Ad_click_play,"
                        + "(SELECT Count(*) FROM Brand_AdNetworkRuleSettings as c "
                        + "INNER JOIN Brand_AdNetwork as d "
                        + "ON c.AdNetwork_ID = d.AdNetwork_ID "
                        + "WHERE c.Active_Inactive='1' and c.priority <>0 and c.priority=b.priority "
                        + "AND (c.Targeted_DomainURLs like '%" + SiteId + "%' or c.Targeted_DomainURLs='' or c.Targeted_DomainURLs is null or c.Targeted_DomainURLs='-1') "
                        + ") As CountPriority,b.Not_On_HomePage "
                        + "FROM Brand_AdNetworkRuleSettings as b "
                        + "INNER JOIN Brand_AdNetwork as a "
                        + "ON b.AdNetwork_ID = a.AdNetwork_ID "
                        + "WHERE b.Active_Inactive='1' and b.priority <>0 "
                        + "AND (b.Targeted_DomainURLs like '%" + SiteId + "%' or b.Targeted_DomainURLs='' or b.Targeted_DomainURLs is null or b.Targeted_DomainURLs='-1') "
                        + "ORDER BY b.priority ";
        cmd.CommandText = sqlAll;
        cmd.CommandType = CommandType.Text;
        TagData = dpSAdmin.FillDataTable(cmd);
        if (TagData.Rows.Count > 0)
        {
            foreach (DataRow dr in TagData.Rows)
            {
                int adnetwork_ID = Convert.ToInt32(dr["AdNetwork_ID"]);
                string adnetwork_name = Convert.ToString(dr["AdNetwork"]);
                DataTable dtRule = new DataTable(); string strSQLRule = "";
                string mainSqlQuery = "SELECT a.AdNetwork_ID AS AdNetwork_ID,a.AdNetwork,b.Tag,b.priority,"
                                    + "b.Referal,b.Not_Referal,"
                                    + "b.Browser,b.Country,b.Plugin,b.OS,"
                                    + "isnull(b.Blocker,0) as Blocker,isnull(b.SameIP_Status,0) as SameIP_Status,"
                                    + "isnull(b.SameIP_Value,0) as SameIP_Value,isnull(b.SameIP_Days,0) as SameIP_Days,isnull(b.BlockerValue,0) as BlockerValue,isnull(b.Ad_click_play,0) as Ad_click_play,b.Not_On_HomePage "
                                    + "FROM Brand_AdNetwork as a INNER JOIN "
                                    + "Brand_AdNetworkRuleSettings as b ON a.AdNetwork_ID = b.AdNetwork_ID "
                                    + "WHERE b.Active_Inactive='1' and b.priority <>0 "
                                    + "AND (b.Targeted_DomainURLs  like '%" + SiteId + "%' or b.Targeted_DomainURLs='' or b.Targeted_DomainURLs is null or b.Targeted_DomainURLs='-1') "
                                    + "AND a.AdNetwork_ID=" + adnetwork_ID + "";
                strSQLRule = mainSqlQuery;
                dtRule = dpSAdmin.FillDataTable(strSQLRule);
                if (dtRule.Rows.Count > 0)
                {
                    if (Convert.ToString(dtRule.Rows[0]["Blocker"]) != "")
                    {
                        dr["Blocker"] = dtRule.Rows[0]["Blocker"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["SameIP_Status"]) != "")
                    {
                        dr["SameIP_Status"] = dtRule.Rows[0]["SameIP_Status"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["SameIP_Value"]) != "")
                    {
                        dr["SameIP_Value"] = dtRule.Rows[0]["SameIP_Value"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["BlockerValue"]) != "")
                    {
                        dr["BlockerValue"] = dtRule.Rows[0]["BlockerValue"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Tag"]) != "")
                    {
                        dr["Tag"] = dtRule.Rows[0]["Tag"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["priority"]) != "")
                    {
                        dr["priority"] = dtRule.Rows[0]["priority"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Ad_click_play"]) != "")
                    {
                        dr["Ad_click_play"] = dtRule.Rows[0]["Ad_click_play"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Not_Referal"]) != "")
                    {
                        dr["Not_Referal"] = dtRule.Rows[0]["Not_Referal"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Referal"]) != "")
                    {
                        dr["Referal"] = dtRule.Rows[0]["Referal"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Plugin"]) != "")
                    {
                        dr["Plugin"] = dtRule.Rows[0]["Plugin"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Browser"]) != "")
                    {
                        dr["Browser"] = dtRule.Rows[0]["Browser"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["Country"]) != "")
                    {
                        dr["Country"] = dtRule.Rows[0]["Country"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["OS"]) != "")
                    {
                        dr["OS"] = dtRule.Rows[0]["OS"];
                    }
                    if (Convert.ToString(dtRule.Rows[0]["SameIP_Days"]) != "")
                    {
                        dr["SameIP_Days"] = dtRule.Rows[0]["SameIP_Days"];
                    }
                }
            }
        }
        return GetAllAdsString(TagData, SiteId);
    }

    private string GetSiteID(string DomainName)
    {
        string strSiteId = "";
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Select_SiteID_ONDomain";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Domain", Convert.ToString(DomainName));
        DataTable dt = dpSAdmin.FillDataTable(cmd);
        int siteid = 0;
        if (dt.Rows.Count > 0)
        {
            siteid = Convert.ToInt32(dt.Rows[0]["id"]);
        }
        if (siteid > 0 & siteid < 10)
        {
            strSiteId = "00" + Convert.ToString(siteid);
        }
        else if (siteid == 10)
        {
            strSiteId = "0" + Convert.ToString(siteid);
        }
        else if (siteid > 10 & siteid < 100)
        {
            strSiteId = "0" + Convert.ToString(siteid);
        }
        else if (siteid == 100)
        {
            strSiteId = Convert.ToString(siteid);
        }
        else if (siteid > 100)
        {
            strSiteId = Convert.ToString(siteid);
        }
        return strSiteId;
    }

    private string GetAllAdsString(DataTable dt, string SiteID)
    {
        StringBuilder strAds = new StringBuilder();
        StringBuilder strTags = new StringBuilder();
        strTags.Append("<ADXML>");
        if (dt.Rows.Count > 0)
        {
            int indexto = Convert.ToInt32(dt.Rows[0]["CountPriority"]);
            int k = 0;
            while (indexto <= dt.Rows.Count)
            {
                strTags.Append(GetPriorityString(k, indexto, dt, Convert.ToInt32(dt.Rows[k]["priority"])));
                k = indexto;
                if (k < dt.Rows.Count)
                {
                    indexto = indexto + Convert.ToInt32(dt.Rows[k]["CountPriority"]);
                }
                else
                {
                    break;
                }
            }
        }
        strTags.Append("</ADXML>");

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToString(strAds) != "")
                {
                    strTags.Length = 0;
                    strTags.Append("BLANK_TAG");
                }
                int adnetwork_ID = Convert.ToInt32(dr["AdNetwork_ID"]);
                string adnetwork_name = Convert.ToString(dr["AdNetwork"]);
                int adSameIPCount = 0;
                string adclickplay = "true";
                if (Convert.ToInt32(Convert.ToString(dr["Ad_click_play"])) == 0)
                {
                    adclickplay = "true";
                }
                else if (Convert.ToInt32(Convert.ToString(dr["Ad_click_play"])) == 1)
                {
                    adclickplay = "false";
                }
                strAds.Append(adnetwork_ID + "$#$" + adnetwork_name + "$#$" + strTags + "$#$" + Convert.ToString(dr["Plugin"]) + "$#$" + adclickplay + "$#$" + Convert.ToString(dr["Blocker"]) + "$#$" + Convert.ToString(dr["BlockerValue"]) + "$#$" + Convert.ToString(dr["SameIP_Status"]) + "$#$" + Convert.ToString(dr["SameIP_Value"]) + "$#$" + adSameIPCount + "$#$"
                    + Convert.ToString(dr["Not_Referal"]) + "$#$" + Convert.ToString(dr["Referal"]) + "$#$" + Convert.ToString(dr["Browser"]) + "$#$" + Convert.ToString(dr["Country"]) + "$#$" + Convert.ToString(dr["OS"]) + "$#$" + Convert.ToString(dr["SameIP_Days"]) + "$#$");
            }
        }
        return Convert.ToString(strAds);
    }

    private string GetPriorityString(int startindex, int endindex, DataTable dt, int priority)
    {
        StringBuilder SbAdTags = new StringBuilder();
        SbAdTags.Append("<priority" + priority + ">");
        for (int j = startindex; j < endindex; j++)
        {
            SbAdTags.Append(Convert.ToInt32(Convert.ToString(dt.Rows[j]["AdNetwork_ID"])) + "^^^" + Convert.ToString(dt.Rows[j]["Tag"]) + "^*^");
        }
        SbAdTags.Append("</priority" + priority + ">");
        return Convert.ToString(SbAdTags);
    }
    #endregion

    #region Widgets
    private void ExportWidgetsToXml(string DomainName, DBHelper dpsite)
    {
        StringBuilder SB = new StringBuilder();
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Widgets>");
        string sql = "";
        string SqlQueryMain = "";
        DataTable dt = new DataTable();
        //Start Random Code
        sql = "select * from brand_Applist_Random";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<Random>");
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToString(dr["Random_Position"]) == "random_topbanner")
                {
                    SB.AppendLine("<VideoRandom>" + Convert.ToString(dr["Value"]) + "</VideoRandom>");
                }
                if (Convert.ToString(dr["Random_Position"]) == "random_topheader")
                {
                    SB.AppendLine("<TopRandom>" + Convert.ToString(dr["Value"]) + "</TopRandom>");
                }
                if (Convert.ToString(dr["Random_Position"]) == "random_left")
                {
                    SB.AppendLine("<LeftRandom>" + Convert.ToString(dr["Value"]) + "</LeftRandom>");
                }
                if (Convert.ToString(dr["Random_Position"]) == "random_bottom")
                {
                    SB.AppendLine("<BottomRandom>" + Convert.ToString(dr["Value"]) + "</BottomRandom>");
                }
                if (Convert.ToString(dr["Random_Position"]) == "random_right")
                {
                    SB.AppendLine("<RightRandom>" + Convert.ToString(dr["Value"]) + "</RightRandom>");
                }
                if (Convert.ToString(dr["Random_Position"]) == "random_subline")
                {
                    SB.AppendLine("<SublineRandom>" + Convert.ToString(dr["Value"]) + "</SublineRandom>");
                }
                if (Convert.ToString(dr["Random_Position"]) == "random_pop_under")
                {
                    SB.AppendLine("<PopunderRandom>" + Convert.ToString(dr["Value"]) + "</PopunderRandom>");
                }
                //mid article issue 3707.............
                if (Convert.ToString(dr["Random_Position"]) == "random_midarticle")
                {
                    SB.AppendLine("<MidArticleRandom>" + Convert.ToString(dr["Value"]) + "</MidArticleRandom>");
                }
            }
            SB.AppendLine("</Random>");
        }

        //Start Video Widgets
        sql = "SELECT id,ApplicationName,CodeBox,Referal,Not_Referal,Browser,Country,OS"
            + " FROM Brand_AppList WHERE status=1 and Position ='topbanner'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<VideoWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                SB.AppendLine("<VideoWidget>");
                SB.AppendLine("<id><![CDATA[" + Convert.ToString(dr["id"]) + "]]></id>");
                SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dr["ApplicationName"]) + "]]></ApplicationName>");
                SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dr["Referal"]) + "]]></Referal>");
                SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dr["Not_Referal"]) + "]]></NotReferal>");
                SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dr["Browser"]) + "]]></Browser>");
                SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dr["Country"]) + "]]></Country>");
                SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dr["OS"]) + "]]></OS>");
                SB.AppendLine("</VideoWidget>");
            }
            SB.AppendLine("</VideoWidgets>");
        }
        //End Video Widgets

        //Start Top
        sql = "SELECT id FROM Brand_AppList WHERE status=1 and Position ='topheader'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<TopWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.width,b.height,b.OS,b.SameIP_Days FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                if (dtWidget.Rows.Count > 0)
                {
                    SB.AppendLine("<TopWidget>");
                    SB.AppendLine("<id><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["id"]) + "]]></id>");
                    SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["ApplicationName"]) + "]]></ApplicationName>");
                    SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dtWidget.Rows[0]["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                    SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                    SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                    SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                    SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                    SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                    SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                    SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Referal"]) + "]]></Referal>");
                    SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Not_Referal"]) + "]]></NotReferal>");
                    SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Browser"]) + "]]></Browser>");
                    SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Country"]) + "]]></Country>");
                    SB.AppendLine("<width><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["width"]) + "]]></width>");
                    SB.AppendLine("<height><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["height"]) + "]]></height>");
                    SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                    SB.AppendLine("</TopWidget>");
                }
            }
            SB.AppendLine("</TopWidgets>");
        }
        //End Top

        //Start Right
        sql = "SELECT id FROM Brand_AppList WHERE status=1 and Position ='righttop'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<RightWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.width,b.height,b.OS,b.SameIP_Days FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                if (dtWidget.Rows.Count > 0)
                {
                    SB.AppendLine("<RightWidget>");
                    SB.AppendLine("<id><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["id"]) + "]]></id>");
                    SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["ApplicationName"]) + "]]></ApplicationName>");
                    SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dtWidget.Rows[0]["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                    SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                    SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                    SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                    SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                    SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                    SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                    SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Referal"]) + "]]></Referal>");
                    SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Not_Referal"]) + "]]></NotReferal>");
                    SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Browser"]) + "]]></Browser>");
                    SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Country"]) + "]]></Country>");
                    SB.AppendLine("<width><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["width"]) + "]]></width>");
                    SB.AppendLine("<height><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["height"]) + "]]></height>");
                    SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                    SB.AppendLine("</RightWidget>");
                }
            }
            SB.AppendLine("</RightWidgets>");
        }
        //End Right

        //Start Left
        sql = "SELECT id FROM Brand_AppList WHERE status=1 and Position ='leftbottom'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<LeftWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.width,b.height,b.OS,b.SameIP_Days FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                if (dtWidget.Rows.Count > 0)
                {
                    SB.AppendLine("<LeftWidget>");
                    SB.AppendLine("<id><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["id"]) + "]]></id>");
                    SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["ApplicationName"]) + "]]></ApplicationName>");
                    SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dtWidget.Rows[0]["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                    SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                    SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                    SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                    SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                    SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                    SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                    SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Referal"]) + "]]></Referal>");
                    SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Not_Referal"]) + "]]></NotReferal>");
                    SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Browser"]) + "]]></Browser>");
                    SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Country"]) + "]]></Country>");
                    SB.AppendLine("<width><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["width"]) + "]]></width>");
                    SB.AppendLine("<height><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["height"]) + "]]></height>");
                    SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                    SB.AppendLine("</LeftWidget>");
                }
            }
            SB.AppendLine("</LeftWidgets>");
        }
        //End Left

        //Start Bottom
        sql = "SELECT id FROM Brand_AppList WHERE status=1 and Position ='bottomright'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<BottomWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.width,b.height,b.OS,b.SameIP_Days FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                if (dtWidget.Rows.Count > 0)
                {
                    SB.AppendLine("<BottomWidget>");
                    SB.AppendLine("<id><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["id"]) + "]]></id>");
                    SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["ApplicationName"]) + "]]></ApplicationName>");
                    SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dtWidget.Rows[0]["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                    SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                    SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                    SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                    SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                    SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                    SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                    SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Referal"]) + "]]></Referal>");
                    SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Not_Referal"]) + "]]></NotReferal>");
                    SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Browser"]) + "]]></Browser>");
                    SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Country"]) + "]]></Country>");
                    SB.AppendLine("<width><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["width"]) + "]]></width>");
                    SB.AppendLine("<height><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["height"]) + "]]></height>");
                    SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                    SB.AppendLine("</BottomWidget>");
                }
            }
            SB.AppendLine("</BottomWidgets>");
        }
        //End Bottom

        //Start None
        sql = "SELECT id,ApplicationName,CodeBox FROM Brand_AppList WHERE status=1 and Position ='none'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<NoneWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                SB.AppendLine("<NoneWidget>");
                SB.AppendLine("<id><![CDATA[" + Convert.ToString(dr["id"]) + "]]></id>");
                SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dr["ApplicationName"]) + "]]></ApplicationName>");
                SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                SB.AppendLine("</NoneWidget>");
            }
            SB.AppendLine("</NoneWidgets>");
        }
        //End None

        //Start Pop under
        sql = "SELECT id,ApplicationName,CodeBox,Not_Referal,Referal,Browser,Country,OS FROM Brand_AppList WHERE status=1 and Position ='pop_under'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<PopWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.width,b.height,b.SameIP_Days,b.OS FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                SB.AppendLine("<PopWidget>");
                SB.AppendLine("<id><![CDATA[" + Convert.ToString(dr["id"]) + "]]></id>");
                SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dr["ApplicationName"]) + "]]></ApplicationName>");
                SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dr["Referal"]) + "]]></Referal>");
                SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dr["Not_Referal"]) + "]]></NotReferal>");
                SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dr["Browser"]) + "]]></Browser>");
                SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dr["Country"]) + "]]></Country>");
                SB.AppendLine("<width><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["width"]) + "]]></width>");
                SB.AppendLine("<height><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["height"]) + "]]></height>");
                SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                SB.AppendLine("</PopWidget>");
            }
            SB.AppendLine("</PopWidgets>");
        }
        //End Pop under

        //Start Subline
        sql = "SELECT id,ApplicationName,CodeBox,Not_Referal,Referal,Browser,Country FROM Brand_AppList WHERE status=1 and Position ='subline'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            SB.AppendLine("<SublineWidgets>");
            foreach (DataRow dr in dt.Rows)
            {
                int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.width,b.height,b.OS,b.SameIP_Days FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                SB.AppendLine("<SublineWidget>");
                SB.AppendLine("<id><![CDATA[" + Convert.ToString(dr["id"]) + "]]></id>");
                SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dr["ApplicationName"]) + "]]></ApplicationName>");
                SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dr["Referal"]) + "]]></Referal>");
                SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dr["Not_Referal"]) + "]]></NotReferal>");
                SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dr["Browser"]) + "]]></Browser>");
                SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dr["Country"]) + "]]></Country>");
                SB.AppendLine("<width><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["width"]) + "]]></width>");
                SB.AppendLine("<height><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["height"]) + "]]></height>");
                SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                SB.AppendLine("</SublineWidget>");
            }
            SB.AppendLine("</SublineWidgets>");
        }
        //End Subline
        //issue 3707===========================
        //start mid article.
        sql = "SELECT id FROM Brand_AppList WHERE status=1 and Position ='midarticle'";
        dt = dpsite.FillDataTable(sql);
        if (dt.Rows.Count > 0)
        {
            List<int> ids = new List<int>();
            foreach (DataRow dr in dt.Rows)
            {
                ids.Add(Convert.ToInt32(dr["id"]));
            }
            int l = ids.Count;
            Random r = new Random();
            int num = r.Next(l);
            int App_Id = ids[num];
            SB.AppendLine("<MidWidgets>");
               // int App_Id = Convert.ToInt32(dr["id"]);
                SqlQueryMain = "SELECT a.id,a.ApplicationName,b.CodeBox,b.Blocker,b.Not_Referal,b.Referal,b.Browser,b.Country,"
                            + "b.SameIP_Status,b.SameIP_Value,b.BlockerValue,b.OS,b.SameIP_Days FROM Brand_AppLists AS a "
                            + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + "WHERE a.id='" + App_Id + "'";
                string strSQLRule = "";
                strSQLRule = SqlQueryMain;
                DataTable dtWidget = dpSAdmin.FillDataTable(strSQLRule);
                if (dtWidget.Rows.Count > 0)
                {
                    //appcode = HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dtWidget.Rows[0]["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", ""));
                    appcode = GetWidgets(App_Id);
                    SB.AppendLine("<MidWidget>");
                    SB.AppendLine("<id><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["id"]) + "]]></id>");
                    SB.AppendLine("<ApplicationName><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["ApplicationName"]) + "]]></ApplicationName>");
                    SB.AppendLine("<CodeBox><![CDATA[" + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dtWidget.Rows[0]["CodeBox"])).Replace("]]>", "").Replace("<![CDATA[", "")) + "]]></CodeBox>");
                    SB.AppendLine("<Blocker><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Blocker"]) + "]]></Blocker>");
                    SB.AppendLine("<SameIPStatus><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Status"]) + "]]></SameIPStatus>");
                    SB.AppendLine("<SameIPValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Value"]) + "]]></SameIPValue>");
                    SB.AppendLine("<SameIPDays><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["SameIP_Days"]) + "]]></SameIPDays>");
                    SB.AppendLine("<BlockerValue><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["BlockerValue"]) + "]]></BlockerValue>");
                    SB.AppendLine("<SameIPCount><![CDATA[" + "0" + "]]></SameIPCount>");
                    SB.AppendLine("<Referal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Referal"]) + "]]></Referal>");
                    SB.AppendLine("<NotReferal><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Not_Referal"]) + "]]></NotReferal>");
                    SB.AppendLine("<Browser><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Browser"]) + "]]></Browser>");
                    SB.AppendLine("<Country><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["Country"]) + "]]></Country>");
                    SB.AppendLine("<OS><![CDATA[" + Convert.ToString(dtWidget.Rows[0]["OS"]) + "]]></OS>");
                    SB.AppendLine("</MidWidget>");
                }
            
            SB.AppendLine("</MidWidgets>");
        }


        //End mid article.

        SB.AppendLine("</Widgets>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Widgets.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Widgets.xml"));
        }
    }
    #endregion

    # region getMidArticlewidget code
    private string GetWidgets(int apid)
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string OS = utils.GetOperatingSystem();
        string stradValue = string.Empty;
        stradValue = funAdRandom("midarticle", widgetconid, widgetBrowser, apid, OS);
        if (!string.IsNullOrEmpty(stradValue))
            stradValue = strDivMidArticle + stradValue + "</div>";

        if (stradValue == "")
        {
            string imagealt = "";
            string filepath = Server.MapPath(ReadConfig.LogoPath);
            FileInfo fi = new FileInfo(filepath);
            if (fi.Exists)
            {
                string filename = fi.Name;
                string ext = fi.Extension;
                imagealt = filename.Replace(ext, "").Replace("-", " ").Replace("_", " ");
                stradValue = "<img src='" + ReadConfig.LogoPath.Replace("~", "..") + "' id='imgLogo' id='imgLogo' alt='" + imagealt + "' />";
            }
        }
        //}
        return stradValue;
    }

    private string funAdRandom(string strPosition, string widgetconid, string widgetBrowser, int App_Id, String OS)
    {
        StringBuilder strAdBox = new StringBuilder();
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + OS + "'";
        strSQLRule = SqlQueryMain;
        DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        dtAdBox = dp1.FillDataTable(strSQLRule);
        if (dtAdBox.Rows.Count > 0)
        {
            if (utils.ReferalCheck(Convert.ToString(dtAdBox.Rows[0]["Referal"]), Convert.ToString(dtAdBox.Rows[0]["Not_Referal"])))
            {
                strAdBox.Append(GetfunAd(dtAdBox.Rows[0], strPosition));
            }
        }
        return Convert.ToString(strAdBox);
    }

    private string GetfunAd(DataRow dr, string strPosition)
    {
        string stradimage = "";
        int IPStatus = 0;
        int IPValue = 0;
        int IPDay = 0;
        int Blocker = 0;
        string ApplicationName = Convert.ToString(dr["ApplicationName"]);
        int BlockerValue = 0;
        int BanID = Convert.ToInt32(dr["id"]);
        int height = 0;
        int width = 0;

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Status"])))
        {
            IPStatus = Convert.ToInt32(dr["SameIP_Status"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Value"])))
        {
            IPValue = Convert.ToInt32(dr["SameIP_Value"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Days"])))
        {
            IPDay = Convert.ToInt32(dr["SameIP_Days"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["Blocker"])))
        {
            Blocker = Convert.ToInt32(dr["Blocker"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["BlockerValue"])))
        {
            BlockerValue = Convert.ToInt32(dr["BlockerValue"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["height"])))
        {
            height = Convert.ToInt32(dr["height"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["width"])))
        {
            width = Convert.ToInt32(dr["width"].ToString().Trim());
        }

        if (strPosition == "midarticle")
        {

            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDay, Blocker, BlockerValue, ApplicationName
                , "imageMidArticleBlock", "ClickMidArticle", "MidArticleBlockerCount", "SameIPCountMidArticle", "MidArticle");
        }


        StringBuilder strAdBox = new StringBuilder();
        if (height == 0 && width == 0)
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" >" + stradimage
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]))) + "</div>");
        }
        else
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"height:auto;width:" + width + "px;\">" + stradimage
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]))) + "</div>");
        }
        return strAdBox.ToString();
    }

    private string Get_Image_RegisterScriptBlock_And_Attach_Click_Event(int BanID, int IPStatus, int IPValue, int IPDays, int Blocker, int BlockerValue, string ApplicationName
        , string imageid, string clickfunname, string JSVarBlockerCount, string JSVarSameIPCount, string ScriptblockKeyName)
    {

        int SameIPCount = utils.SameIPClicks(BanID, "banner", IPDays);
        BannerIDs += BanID + ",";
        string strdis = "none";
        string type = "0";
        if (IPStatus == 1 && IPValue <= SameIPCount && BanID != 0)
        {
            strdis = "block";
            type = "2";
        }
        if (strdis == "none" && Blocker != 1)
            type = "1";

        strDivMidArticle = "<div id=\"DivMidArticle\" style=\"position: relative; float: left;\"class=\"ads midArticle_ads_Box\" onclick=\"javascript:" + clickfunname + "('" + BanID + "'," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",'" + imageid + "','" + ApplicationName + "')\">";

        string stradimage = "<img  alt='1x2' id ='" + imageid + "' style='display:" + strdis + ";z-index:9999;float:right;position:absolute;left:0;top:0;width:100%; height:100%;' "
            + "src='../images_common/1x2.gif' />";

        return stradimage;
    }
    #endregion

    #region RssFeed
    private void ExportRSSFeedToXML(string DomainName, DBHelper dpsite)
    {
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        string logo = "http://" + DomainName + "/config/" + DomainName + "/" + ReadConfig.GetLogo(DomainName);

        DataTable dt = new DataTable();
        StringBuilder SB = new StringBuilder();
        DBCommand strqry = new DBCommand();
        strqry.CommandText = "SELECT post_id,post_title,post_description,c_date as CreatedOn "
                              + "FROM BRAND_BLOG_POSTS where BRAND_BLOG_POSTS.Active_Inactive=1 "
                              + "ORDER BY CreatedOn DESC";
        strqry.CommandType = CommandType.Text;
        dt = dpsite.FillDataTable(strqry);

        string strQuery = "SELECT * FROM Brand_AppSettings WHERE IsDelete='False' AND name<>'title' AND Type=1";
        DataTable dtData = dpsite.FillDataTable(strQuery);
        string metadata = "";
        if (dtData.Rows.Count > 0)
        {
            metadata = Convert.ToString(dtData.Rows[0]["description"]);
        }
        else
        {
            metadata = "";
        }

        SB.AppendLine("<rss version=\"2.0\" xmlns:media=\"http://search.yahoo.com/mrss/\">");
        SB.AppendLine("<channel>");
        SB.AppendLine("<title>" + DomainName + "</title>");
        SB.AppendLine("<link>http://" + DomainName + "</link>");
        SB.AppendLine("<description><![CDATA[" + metadata + "]]></description>");
        SB.AppendLine("<image>");
        SB.AppendLine("<title>" + DomainName + "</title>");
        SB.AppendLine("<link>http://" + DomainName + "</link>");
        SB.AppendLine("<url>" + logo + "</url>");
        SB.AppendLine("<height>100</height>");
        SB.AppendLine("<width>144</width>");
        SB.AppendLine("</image>");
        SB.AppendLine("<atom10:link rel=\"self\" type=\"application/rss+xml\" href=\"http://" + DomainName + "/html/Rssfeed.html" + "\" xmlns:atom10=\"http://www.w3.org/2005/Atom\" />");

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string str_postsdesc = "";
                int page = i / 10;
                if (Convert.ToString(dt.Rows[i]["post_description"]) != "")
                {
                    str_postsdesc = Regex.Replace(Convert.ToString(dt.Rows[i]["post_description"]), @"<(.|\n)*?>", string.Empty);
                    str_postsdesc = str_postsdesc.Replace("&nbsp;", "");
                    if (str_postsdesc.Length > 200)
                    {
                        str_postsdesc = str_postsdesc.Substring(0, 197) + "...";
                    }
                }

                string strposttitle = Convert.ToString(dt.Rows[i]["post_title"]).Trim().Replace(' ', '-');
                strposttitle = utils.formatStringForURLSelect(strposttitle);
                strposttitle = utils.GeneratePostTitleForURL(strposttitle);
                string date1 = String.Format("{0:r}", dt.Rows[i]["CreatedOn"]);
                SB.AppendLine("<item>");
                SB.AppendLine("<title><![CDATA[" + utils.formatStringForURLSelect(Convert.ToString(dt.Rows[i]["post_title"])) + "]]></title>");
                SB.AppendLine("<guid isPermaLink=\"false\"><![CDATA[");
                SB.Append("http://" + DomainName + "/html/Index.html?page=" + page + "&featureid=" + Convert.ToString(dt.Rows[i]["post_id"]));
                SB.Append("]]></guid>");
                SB.AppendLine("<pubDate>" + date1 + "</pubDate>");
                SB.AppendLine("<description><![CDATA[" + str_postsdesc + "]]></description>");
                SB.AppendLine("<link><![CDATA[");
                SB.Append("http://" + DomainName + "/html/Index.html?page=" + page + "&featureid=" + Convert.ToString(dt.Rows[i]["post_id"]));
                SB.Append("]]></link>");
                SB.AppendLine("</item>");
            }
        }
        SB.AppendLine("</channel>");
        SB.AppendLine("</rss>");

        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "RSSFeed.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "RSSFeed.xml"));
        }
    }
    #endregion

    #region Single_Home_Page
    private void ExportSingleHomePage(string DomainName, DBHelper dpsite)
    {
        string defaultPageType = "";
        string defaultPage = "";
        string strHTML = "";
        int BlogsInSummary = 0;
        DBCommand strQuery = new DBCommand();
        strQuery.CommandText = "SP_Brand_Select_Default_Feature";
        strQuery.CommandType = CommandType.StoredProcedure;
        DataTable dt = dpsite.FillDataTable(strQuery);
        if (dt != null && dt.Rows.Count > 0)
        {
            if (Convert.ToString(dt.Rows[0]["defaultfeatureType"]) != "")
            {
                defaultPageType = Convert.ToString(dt.Rows[0]["defaultfeatureType"]);
            }
            if (Convert.ToString(dt.Rows[0]["defaultfeature"]) != "")
            {
                defaultPage = Convert.ToString(dt.Rows[0]["defaultfeature"]);
                if (defaultPage == ((int)Enum.Parse(typeof(features), features.BlogPostSummary.ToString())).ToString())
                {
                    string strQuery1 = "select BlogsNUmberInSummary from brand_settings";
                    DataTable dt1 = dpsite.FillDataTable(strQuery1);
                    if (dt1.Rows.Count > 0)
                    {
                        if (Convert.ToString(dt1.Rows[0]["BlogsNUmberInSummary"]) != "")
                        {
                            BlogsInSummary = Convert.ToInt32(dt1.Rows[0]["BlogsNUmberInSummary"].ToString());
                        }
                    }
                }
            }
        }

        string singlePageQuery = "select SinglePageCode from brand_settings";
        dt = dpsite.FillDataTable(singlePageQuery);
        if (dt.Rows.Count > 0)
        {
            strHTML = Convert.ToString(dt.Rows[0]["SinglePageCode"]);
        }

        StringBuilder SB = new StringBuilder();
        SB.AppendLine("<Home>");
        SB.AppendLine("<PageType><![CDATA[" + defaultPageType + "]]></PageType>");
        SB.AppendLine("<DefaultPage><![CDATA[" + defaultPage + "]]></DefaultPage>");
        SB.AppendLine("<BlogsInSummary><![CDATA[" + BlogsInSummary + "]]></BlogsInSummary>");
        SB.AppendLine("<Code><![CDATA[" + strHTML + "]]></Code>");
        SB.AppendLine("</Home>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Home.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Home.xml"));
        }
    }
    #endregion

    #region Calendar
    private void ExportCalendarToXml(string DomainName, DBHelper dpsite)
    {
        StringBuilder SB = new StringBuilder();
        dpsite = ReadConfig.getDomainDBHelper(DomainName);
        string sql = "select cal_id,Brand_Calendar.title,link,c_by from Brand_Calendar where Brand_Calendar.status<>'0'  ORDER BY brand_calendar.c_date desc";
        DataTable dt = dpsite.FillDataTable(sql);
        SB.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        SB.AppendLine("<Calendars>");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                dr["title"] = utils.formatStringForDBSelect(Convert.ToString(dr["title"]));
                DataTable dtCal = new DataTable();
                try
                {
                    dtCal = LoadCalendar(Convert.ToString(dr["link"]));
                }
                catch
                {
                }
                if (!dtCal.Columns.Contains("SUMMARY"))
                {
                    dtCal.Columns.Add("SUMMARY", typeof(string));
                    dtCal.Columns.Add("DESCRIPTION", typeof(string));
                    dtCal.Columns.Add("DTSTART", typeof(string));
                    dtCal.Columns.Add("DTEND", typeof(string));
                }
                if (dtCal.Rows.Count <= 0)
                {
                    DataRow drdtCal = dtCal.NewRow();
                    drdtCal["SUMMARY"] = "";
                    drdtCal["DESCRIPTION"] = "Calendar not available!";
                    drdtCal["DTSTART"] = "";
                    drdtCal["DTEND"] = "";
                    dtCal.Rows.Add(drdtCal);
                }
                string CalendartitleForLink = Convert.ToString(dr["title"]);
                if (CalendartitleForLink.Length > 30)
                {
                    CalendartitleForLink = CalendartitleForLink.Substring(0, 29) + "...";
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////
                SB.AppendLine("<Calendar>");
                SB.AppendLine("<CalendarID>" + Convert.ToString(dr["cal_id"]) + "</CalendarID>");
                SB.AppendLine("<CalendartitleForLink><![CDATA[" + CalendartitleForLink + "]]></CalendartitleForLink>");
                SB.AppendLine("<CalendarTitle><![CDATA[" + Convert.ToString(dr["title"]) + "]]></CalendarTitle>");
                SB.AppendLine("<CalendarLink><![CDATA[" + Convert.ToString(dr["link"]) + "]]></CalendarLink>");
                foreach (DataRow drCal in dtCal.Rows)
                {
                    SB.AppendLine("<Item>");
                    SB.AppendLine("<DTSTART><![CDATA[" + Convert.ToString(drCal["DTSTART"]) + "]]></DTSTART>");
                    SB.AppendLine("<DTEND><![CDATA[" + Convert.ToString(drCal["DTEND"]) + "]]></DTEND>");
                    SB.AppendLine("<description><![CDATA[" + Server.HtmlEncode(Convert.ToString(drCal["description"])) + "]]></description>");
                    SB.AppendLine("<title><![CDATA[" + Convert.ToString(drCal["SUMMARY"]) + "]]></title>");
                    SB.AppendLine("</Item>");
                }
                SB.AppendLine("</Calendar>");
            }
        }
        SB.AppendLine("</Calendars>");
        XmlDocument XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(Convert.ToString(SB));
        string Path = "../html/" + DomainName + "/";
        DirectoryInfo di = new DirectoryInfo(Server.MapPath(Path));
        if (di.Exists)
        {
            XMLDoc.Save(Server.MapPath(Path + "Calendars.xml"));
        }
        else
        {
            di.Create();
            XMLDoc.Save(Server.MapPath(Path + "Calendars.xml"));
        }
    }

    private DataTable LoadCalendar(string URL)
    {
        DataTable dtCalendar = new DataTable();
        try
        {
            dtCalendar.Columns.Add(new DataColumn("SUMMARY", typeof(string)));
            dtCalendar.Columns.Add(new DataColumn("DESCRIPTION", typeof(string)));
            dtCalendar.Columns.Add(new DataColumn("DTSTART", typeof(string)));
            dtCalendar.Columns.Add(new DataColumn("DTEND", typeof(string)));
            //Create a WebRequest and WebResponse for get the Details.
            if (URL.Contains("webcal://"))
            {
                URL = URL.Replace("webcal://", "http://");
            }
            XmlDocument xd = new XmlDocument();
            HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(URL);
            HttpWebResponse HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();

            vCardReader vCR = new vCardReader();

            StreamReader myReader = new StreamReader(HttpWResp.GetResponseStream());
            string strFileData = myReader.ReadToEnd();
            string[] strArrFieldvalues = Regex.Split(strFileData.Trim(), "END:VEVENT");
            foreach (string FieldValue in strArrFieldvalues)
            {
                DataRow drNew = dtCalendar.NewRow();
                vCR.ParseLines(FieldValue);
                if (FieldValue.Contains("SUMMARY"))
                {
                    drNew["SUMMARY"] = vCR.Summary;
                }
                if (FieldValue.Contains("DESCRIPTION"))
                {
                    drNew["DESCRIPTION"] = vCR.DESCRIPTION;
                }
                if (FieldValue.Contains("DTSTART"))
                {
                    ArrayList DtArrlst = new ArrayList();
                    string strDTSTART = vCR.DTSTART;
                    string strStart = "";
                    string strStartTime = "";

                    if (strDTSTART != "")
                    {
                        if (!strDTSTART.Contains("T"))
                        {
                            strDTSTART += "T000000";
                        }
                        DtArrlst = getDate(strDTSTART);
                        strStart = DtArrlst[0].ToString();
                        strStartTime = DtArrlst[1].ToString();
                    }
                    if (!string.IsNullOrWhiteSpace(strStart))
                        drNew["DTSTART"] = strStart;
                }
                if (FieldValue.Contains("DTEND"))
                {
                    ArrayList DtArrlst = new ArrayList();
                    string strDTEND = vCR.DTEND;
                    string strEnd = "";
                    string strEndTime = "";

                    if (strDTEND != "")
                    {
                        if (!strDTEND.Contains("T"))
                        {
                            strDTEND += "T000000";
                        }
                        DtArrlst = getDate(strDTEND);
                        strEnd = DtArrlst[0].ToString();
                        strEndTime = DtArrlst[1].ToString();
                    }

                    if (!string.IsNullOrWhiteSpace(strEnd))
                        drNew["DTEND"] = strEnd;
                }
                if ((FieldValue.Contains("SUMMARY") || FieldValue.Contains("DESCRIPTION") || FieldValue.Contains("DTSTART") || FieldValue.Contains("DTEND"))
                    && drNew["DTSTART"] != DBNull.Value && Convert.ToDateTime(drNew["DTSTART"]) >= DateTime.Now)
                {
                    dtCalendar.Rows.Add(drNew);
                }
            }
            HttpWResp.Close();
            HttpWResp = null;
            HttpWReq = null;
            myReader.Close();
            myReader = null;
        }
        catch (Exception ex) { throw ex; }
        return dtCalendar;
    }

    private ArrayList getDate(string strDate)
    {
        string[] strDTArr = strDate.Split('T');
        ArrayList retArr = new ArrayList();

        string strDt = "";
        string strTt = "";
        if (strDTArr[0].Length == 8)
        {
            strDt = strDTArr[0].Insert(4, "-");
            strDt = strDt.Insert(7, "-");
        }
        if (strDTArr[1] != "")
        {
            strTt = strDTArr[1].Substring(0, strDTArr[1].Length);
            if (strTt.Length == 6)
            {
                strTt = strTt.Insert(2, ":");
                strTt = strTt.Insert(5, ":");
            }
            else if (strTt.Length > 6)
            {
                strTt = strTt.Substring(0, 6);
                strTt = strTt.Insert(2, ":");
                strTt = strTt.Insert(5, ":");
            }
        }
        DateTime dtDate = DateTime.Parse(strDt + " " + strTt);
        retArr.Add(Convert.ToString(dtDate));
        retArr.Add(strTt);
        return retArr;
    }
    #endregion
}

