﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="SitesExportToHTML.aspx.cs" Inherits="DigipSuperAdmin_SitesExportToHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function updateHtmlorAspx(domainid, value1) {
            if (confirm("Are you want to update site option?")) {
                document.getElementById("<%=hdnDomainID.ClientID %>").value = domainid;
                 document.getElementById("<%=hdnValue.ClientID %>").value = value1;
                 document.getElementById("<%=btnSubmit.ClientID %>").click();
                 return true;
             }
             else {
                 return false;
             }
         }
         function pointTo(choice) {
             if (choice == 0) {
                 if (confirm("Are you sure to Point All site to ASPX?")) {
                     document.getElementById("<%=btnAllASPX.ClientID %>").click();
                    return true;
                }
                else {
                    return false;
                }
            }
            if (choice == 1) {
                if (confirm("Are you sure to Point All site to HTML?")) {
                    document.getElementById("<%=btnAllHtml.ClientID %>").click();
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>
    <asp:Label ID="MSG" runat="server" ForeColor="red"></asp:Label><br />
    <br />
    <div style="float: right; margin: 0 20px 10px 0;">
        <p style="margin: 0 0 5px 0; font-weight: bold;">Point All Sites to:</p>
        <asp:RadioButton ID="rdbAllASPX" runat="server" Text="Aspx" TextAlign="Left" GroupName="AllAspxHtml" />&nbsp
        <asp:RadioButton ID="rdbAllHTML" runat="server" Text="HTML" TextAlign="Left" GroupName="AllAspxHtml" />
    </div>
    <asp:DataGrid ID="dgrdDomain" runat="server" AutoGenerateColumns="False" Width="100%"
        BorderWidth="1px" OnItemCommand="dgrdDomain_ItemCommand" OnItemDataBound="dgrdDomain_ItemDataBound">
        <Columns>
            <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
            <asp:BoundColumn DataField="domainname" HeaderText="Domain">
                <ItemStyle Width="40%" />
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Action">
                <ItemStyle Width="20%" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnkExport" runat="server" OnClientClick="return confirm('Are You Sure To Export site as HTML?');"
                        CommandName="Export" Text="Export" CommandArgument='<%# Eval("domainname")%>' CssClass="toplink"></asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Last export date/time">
                <ItemStyle Width="20%" />
                <ItemTemplate>
                    <asp:Label ID="lblDateTime" runat="server"></asp:Label><br />
                    <asp:Label ID="lblComponents" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Aspx/HTML">
                <ItemStyle Width="20%" />
                <ItemTemplate>
                    <asp:RadioButton ID="rdbASPX" runat="server" Text="Aspx" TextAlign="Left" GroupName="AspxHtml" /><br />
                    <asp:RadioButton ID="rdbHTML" runat="server" Text="HTML" TextAlign="Left" GroupName="AspxHtml" />
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <div style="display: none">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" />
        <asp:Button ID="btnAllASPX" runat="server" OnClick="btnAllAspx_Click" />
        <asp:Button ID="btnAllHtml" runat="server" OnClick="btnAllHTML_Click" />
        <asp:HiddenField ID="hdnDomainID" runat="server" />
        <asp:HiddenField ID="hdnValue" runat="server" />
    </div>
</asp:Content>

