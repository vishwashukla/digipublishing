﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ViewSummaryStastics.aspx.cs" Inherits="DigipSuperAdmin_ViewSummaryStastics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div>
        Site Genre :        
        <asp:DropDownList ID="ddlSiteGenre" CssClass="maintext" runat="server" Width="152px" AutoPostBack="true"
            OnSelectedIndexChanged="ddlSiteGenre_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <asp:DataGrid ID="dgrdDomain" runat="server" AutoGenerateColumns="False"
        Width="795px" BorderWidth="1px" AllowSorting="True" OnSortCommand="dgrdDomain_SortCommand"
        OnItemCommand="dgrdDomain_OnItemCommand">
        <Columns>
            <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
            <asp:BoundColumn DataField="domainname" HeaderText="Domain"
                ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                SortExpression="domainname"></asp:BoundColumn>
            <asp:BoundColumn DataField="ActivePosts" HeaderText="Blog Posts Active"
                ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                SortExpression="ActivePosts"></asp:BoundColumn>
            <asp:BoundColumn DataField="InActivePosts" HeaderText="Blog Posts Inactive"
                ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                SortExpression="InActivePosts"></asp:BoundColumn>
            <asp:BoundColumn DataField="PlayListItems" HeaderText="Video Playlist Items"
                ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                SortExpression="PlayListItems"></asp:BoundColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Login" ID="lnkLogin" CommandName="Login" CommandArgument='<%# Eval("domainname") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>

