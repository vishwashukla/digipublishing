﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageRegions.aspx.cs" Inherits="DigipSuperAdmin_ManageRegions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <h4>Manage Regions</h4>
    <asp:MultiView ID="mView" runat="server">
        <asp:View ID="vGrid" runat="server">
                <table width="100%">
                    <tr>
                        <td align="right"> 
                            <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add New" OnClick="lnkbtnAdd_Click" Visible="true"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataGrid ID="dgRegions" runat="server" OnEditCommand="dgRegions_EditCommand" GridLines="None" 
                                AutoGenerateColumns="False"  OnDeleteCommand="dgRegions_DeleteCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="region_id" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="region_name" HeaderText="Region"></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Edit" HeaderText="Edit" Text="Edit"></asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDel" runat="server" Text="Delete" CommandName="Delete" CssClass="toplink" OnClientClick="return confirm('Are you sure you wish to delete?')"
                                            CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate> 
                                    </asp:TemplateColumn>                                      
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>        
                </table>
            </asp:View>
            <asp:View ID="vAddEdit" runat="server">
                <table>  
                    <tr>
                        <td>
                            Region Name*</td>
                        <td>
                            <asp:TextBox ID="txtRegion" runat="server"></asp:TextBox> 
                              <div style="color: red" id="Div1">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                    ControlToValidate="txtRegion" Display="Dynamic" ErrorMessage="please enter country"
                                    ValidationGroup="MngUsrVCG">
                                </asp:RequiredFieldValidator>
                            </div>
                        </td>
                    </tr>                                        
                    <tr>
                        <td>
                            <asp:Button ID="btnCreate" runat="server" Text="Save" ValidationGroup="MngUsrVCG" CausesValidation="true" OnClick="btnCreate_Click"></asp:Button></td>
                        <td>
                            <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel"></asp:Button></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnRegionID" runat="server" />
            </asp:View>
    </asp:MultiView>
   </asp:Content>