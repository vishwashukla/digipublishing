﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageTemplates.aspx.cs" Inherits="DigipSuperAdmin_ManageTemplates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="mvManageTemp" runat="server">
        <asp:View ID="viewTemp" runat="server">
            <div>
                <table width="100%">
                    <tr>
                        <td></td>
                        <td>
                            <asp:HiddenField ID="hdnID" runat="server" />
                            <asp:HiddenField ID="hdnthemes" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h4>Manage Templates</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add New" OnClick="lnkbtnAdd_OnClick" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dgrdTemplates" runat="server" GridLines="none" AutoGenerateColumns="False"
                                Width="795px" BorderWidth="1" AllowSorting="true" OnItemCommand="dgrdTemplates_ItemCommand"
                                OnItemDataBound="dgrdTemplates_OnItemDataBound" OnSortCommand="dgrdTemplates_SortCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Available to Sites" ItemStyle-Width="140px" Visible="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTheme" runat="server" Checked='<%#Eval("available_to_agencies") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn DataField="name" HeaderText="Template Name" SortExpression="name"></asp:BoundColumn>--%>
                                    <asp:TemplateColumn HeaderText="Template Name" SortExpression="name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkname" runat="server" CommandName="Edit" Text='<%#Eval("name") %>'
                                                CssClass="toplink" CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="value" HeaderText="CSS Name" Visible="false" SortExpression="value"></asp:BoundColumn>
                                    <%--<asp:BoundColumn DataField="genre" HeaderText="Site Genre" SortExpression="genre"></asp:BoundColumn>--%>
                                    <%--<asp:TemplateColumn HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="Edit" Text="Edit" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                    <%--<asp:ButtonColumn HeaderText="Delete" Text="Delete" CommandName="Delete"></asp:ButtonColumn>--%>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Delete" CssClass="toplink"></asp:LinkButton>
                                         </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="20%" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="available_to_agencies" Visible="False"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:Button ID="btnAvailable" Text="Set Available" runat="server" OnClick="btnAvailable_Click" Visible="false" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:View>
        <asp:View ID="vAdd" runat="server">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <h4>
                            <asp:Label ID="lblAdd" Text="Add Template" runat="server"></asp:Label>&nbsp;
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td class="txtDisplay" style="height: 24px; width: 199px;">Template Name</td>
                    <td style="height: 24px">
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                        <div style="color: red" id="Div2">
                            <asp:RequiredFieldValidator ID="RFV_TEXT" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtName" Display="Dynamic" ErrorMessage="please enter Template Name"
                                ValidationGroup="Mngtemplate">
                            </asp:RequiredFieldValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="txtDisplay" style="height: 24px; width: 199px;">Value</td>
                    <td style="height: 24px">
                        <asp:DropDownList ID="ddlValue" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlValue"
                            runat="server" InitialValue="-1" ErrorMessage="Please Select Value" ValidationGroup="Mngtemplate" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td  >
                        <asp:Button ID="btnAddSave" runat="server" Text="Save" OnClick="btnAddSave_Click" ValidationGroup="Mngtemplate" CausesValidation="true" />
                  
                        <asp:Button ID="btnAddCancel" runat="server" Text="Cancel" OnClick="btnAddCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="viewEditTemp" runat="server">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <h4>
                            <asp:Label ID="headerText" Text="Edit Component" runat="server"></asp:Label>&nbsp;
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td class="txtDisplay" style="height: 24px; width: 199px;">Template Name</td>
                    <td style="height: 24px">
                        <asp:TextBox ID="txtTemplateName" runat="server"></asp:TextBox>
                        <div style="color: red" id="Div1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtTemplateName" Display="Dynamic" ErrorMessage="please enter Template Name"
                                ValidationGroup="Mngtmp">
                            </asp:RequiredFieldValidator>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>CSS</td>
                    <td>
                        <asp:TextBox ID="txtCSS" runat="server" Height="197px" TextMode="MultiLine" Width="457px"></asp:TextBox></td>
                </tr>
                <tr><td></td>
                    <td >
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="Mngtmp" CausesValidation="true" />
                    
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

