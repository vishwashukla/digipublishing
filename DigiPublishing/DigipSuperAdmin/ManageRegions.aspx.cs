﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ManageRegions : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Regions";
        lblMsg.Text = "";
        if (!IsPostBack)
            gridBind();
    }

    public void gridBind()
    {
        dgRegions.DataSource = getRegionList();
        dgRegions.DataBind();
        mView.SetActiveView(vGrid);
    }

    public DataTable getRegionList()
    {
        DBCommand cmd = new DBCommand();
        string strQuery = "SELECT region_name, region_id FROM bbc_region ORDER BY region_name";
        DataTable dt = new DataTable();
        try
        {
            dt = dp.FillDataTable(strQuery);
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
        return dt;
    }

    protected void dgRegions_EditCommand(object source, DataGridCommandEventArgs e)
    {
        ResetControl();
        hdnRegionID.Value = e.Item.Cells[0].Text;
        txtRegion.Text = e.Item.Cells[1].Text;
        mView.SetActiveView(vAddEdit);
    }

    protected void dgRegions_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        string sQuery = "DELETE FROM bbc_region WHERE region_id=" + e.Item.Cells[0].Text;
        dp.ExecuteNonQuery(sQuery);
        lblMsg.Text = "Region deleted successfully.";
        gridBind();
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (regionExist())
        {
            lblMsg.Text = "Region already exists";
            mView.SetActiveView(vAddEdit);
            return;
        }

        if (!String.IsNullOrWhiteSpace(hdnRegionID.Value))
        {
            // For Edit
            string sQuery = "UPDATE bbc_region SET region_name='" + txtRegion.Text.Trim() + "', IsActive='" + true + "', m_by=" + Convert.ToString(Session["userid"]) + ", m_date='" + DateTime.Now + "' WHERE region_id = " + hdnRegionID.Value;
            dp.ExecuteNonQuery(sQuery);
            lblMsg.Text = "Region successfully updated";
            gridBind();
        }

        else
        {
            // For New Entry
            string sQuery = "INSERT INTO bbc_region (region_name, IsActive , c_date, c_by) VALUES ('" + txtRegion.Text.Trim() + "','" + true + "', '" + DateTime.Now + "','" + Convert.ToString(Session["userid"]) + "')";
            dp.ExecuteNonQuery(sQuery);
            lblMsg.Text = "Region successfully added";
            gridBind();
        }
    }
    public bool regionExist()
    {
        string sQuery = "SELECT count(*) FROM bbc_region WHERE region_name='" + utils.formatStringForDBInsert(txtRegion.Text.Trim()) + "'";
        DataTable dt = dp.FillDataTable(sQuery);
        if (Convert.ToString(dt.Rows[0][0]) != "0")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControl();
        mView.SetActiveView(vGrid);
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        ResetControl();
        mView.SetActiveView(vAddEdit);
    }

    public void ResetControl()
    {
        hdnRegionID.Value = "";
        txtRegion.Text = "";
    }
}