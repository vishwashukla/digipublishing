﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" AutoEventWireup="true" CodeFile="PlayList.aspx.cs" Inherits="DigipSuperAdmin_PlayList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <script type="text/javascript">
         function RandomPlaylistClick(id) {
             if (confirm("Are you sure you wish to continue?")) {
                 var hdnChannelID = document.getElementById('<% =hdnChannelID.ClientID %>');
                if (hdnChannelID != null)
                    hdnChannelID.value = id;
                var btnchkRandom = document.getElementById('<% =btnchkRandom.ClientID %>');
                 if (btnchkRandom != null)
                     btnchkRandom.click();
                 return true;
             }
             else {
                 return false;
             }
         }
    </script>
        <div style="text-align:center;">
            <asp:Label ID="lblMsg" runat="server" ForeColor="red" Text=""></asp:Label>
        </div>
        <div style="text-align:center;">
            <table id="Table1" style="width:80%;" border='0' runat="server">
                <tr>
                    <td style="text-align:center;" colspan="2">
                        <div style="height: 10%; padding-bottom: 5px;
                            padding-top: 10px;">
                            <asp:CheckBox ID="chkRandomplaylist" runat="server" Text="Random Playlist" TextAlign="Right" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="height: 5%; padding-bottom: 10px;
                            padding-top: 5px;">
                            <asp:CheckBox ID="chkplaylistAlternate" runat="server" Text="Alternative Playlist Source"
                                TextAlign="Left" />
                            <asp:TextBox ID="txtplaylistAlternate" runat="server" Text=""></asp:TextBox>
                            <asp:CheckBox ID="chkCNBC_rss" runat="server" Text="CNBC To MRSS" TextAlign="Left" />
                            <asp:Button ID="btnPlaylistAlternate" runat="server" Text="Submit" OnClick="btnPlaylistAlternate_Click" />
                        </div>
                    </td>
                </tr>
                <tr runat="server" id="Tr1">
                    <td style="width:60%;">
                        Channel:<asp:HyperLink ID="hlChannel" runat="server"></asp:HyperLink>
                    </td>
                    <td style="width:100%;text-align:right;">
                        <asp:HyperLink ID="HyperLink1" Text="Add New" runat="server"></asp:HyperLink>
                    </td>
                </tr>
                <tr runat="server" id="rowadd">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    Enter Title</td>
                                <td>
                                    <asp:TextBox ID="txtTitle" Width="300" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Enter Ref Href</td>
                                <td>
                                    <asp:TextBox ID="txtRefHref" Width="300" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Enter Copyright</td>
                                <td>
                                    <asp:TextBox ID="txtCopyright" Width="300" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Enter Description</td>
                                <td>
                                    <asp:TextBox ID="txtDescription" Width="300" TextMode="MultiLine" runat="server"></asp:TextBox></td>
                            </tr>
                        </table>
                        <asp:Button ID="btnAdd" runat="server" Text="&nbsp;&nbsp;Add&nbsp;&nbsp;" OnClick="btnAdd_Click" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancel1" runat="server" Text="cancel" OnClick="btnCancel1_Click" />
                    </td>
                </tr>
                <tr runat="server" id="rowedit">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    Enter Title</td>
                                <td>
                                    <asp:TextBox ID="txtTitle1" Width="300" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Enter Ref Href</td>
                                <td>
                                    <asp:TextBox ID="txtRefHref1" Width="300" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Enter Copyright</td>
                                <td>
                                    <asp:TextBox ID="txtCopyright1" Width="300" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Enter Description</td>
                                <td>
                                    <asp:TextBox ID="txtDescription1" Width="300" TextMode="MultiLine" runat="server"></asp:TextBox></td>
                            </tr>
                        </table>
                        <asp:Button ID="btnEdit" runat="server" Text="&nbsp;&nbsp;Update&nbsp;&nbsp;" OnClick="btnEdit_Click" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancel2" runat="server" Text="cancel" OnClick="btnCancel2_Click" />
                    </td>
                </tr>
                <tr runat="server" id="rowdel">
                    <td colspan="2">
                        <asp:Label ID="lbdel" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnDel" runat="server" Text="&nbsp;&nbsp;Delete&nbsp;&nbsp;" OnClick="btnDel_Click" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancel3" runat="server" Text="cancel" OnClick="btnCancel3_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lbBody" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="text-align:center;">
            <asp:Button ID="Button2" runat="server" Text="Back" OnClick="Button2_Click" />
            <div style="padding-top: 20px;">
                <a id="lnkPlaylist" target="_blank" href="#" runat="server"></a>
            </div>
        </div>
        <div style="display: none;">
            <asp:Button runat="server" ID="btnchkRandom" OnClick="btnchkRandom_Click" />
            <asp:HiddenField runat="server" ID="hdnChannelID" />
        </div>
</asp:Content>

