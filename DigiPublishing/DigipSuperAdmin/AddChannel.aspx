﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" AutoEventWireup="true" CodeFile="AddChannel.aspx.cs" Inherits="DigipSuperAdmin_AddChannel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../JS/Approve.js"></script>
    <script type="text/javascript" language="JavaScript">
        var IsValidFile = false;
        function Checkfiles(oSrc, args) {
            var fname = args.Value;
            if (fname.length > 0) {
            }
            if (fname.length == 0) {
                IsValidFile = true;
                args.IsValid = (fname.length == 0);
            }
            else {
                var fext = fname.substring(fname.length - 3).toLowerCase();
                args.IsValid = (fext == 'jpg' || fext == 'gif' || fext == 'jpe');
                IsValidFile = args.IsValid;
            }
        }
    </script>
    <div style="text-align:center;">
        <div>    
		<!--Test-->
            <table style="height: 100%;width:500px;" border="0">
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;" colspan="2">
                            <div style="text-align:center;align-content:center;">
                                <asp:Label ID="lblmsg" runat="server" ForeColor="red"></asp:Label>
                            </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #D3D3D3; width: 28%;" class="footerlink05">
                        <div style="text-align:center;align-content:center;">
                            <strong>Add Channel</strong>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #EBEBEB; width: 28%;vertical-align:top;">
                        <span style="color: Red">*</span> Required fields</td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <strong>Location</strong>:&nbsp;<span style="color: Red">*</span></td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <table style="width:100%" border="0">
                            <tr>
                                <td>
                                    <table style="width:100%" border="0">
                                        <tr>
                                            <td style="width: 19%">
                                                <asp:DropDownList ID="cboregion" Width="150" runat="server" onchange="fillCountryCbo(this.value)" /></td>
                                            <td style="text-align:left;">
                                                <div id="divcountry">
                                                </div>
                                                <input type="hidden" name="ForCountryID" id="ForCountryID" value="" runat="server" /></td>
                                            <td style="text-align:left;">
                                                <asp:RequiredFieldValidator ID="regionrequired" runat="server" InitialValue="-SelectRegion-"
                                                    ErrorMessage="Location   is  required " ControlToValidate="cboregion" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Channel Name:&nbsp;<span style="color: Red">*</span></td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:TextBox ID="channelname" MaxLength="100" Width="250" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Channel Description:</td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:TextBox MaxLength="500" ID="txtChannelDesc" runat="server" TextMode="MultiLine"
                            Rows="5" Columns="35"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Channel URL:&nbsp;<span style="color: Red">*</span></td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:TextBox MaxLength="200" Width="250" ID="channelurl" runat="Server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Stream URL:&nbsp;<span style="color: Red">*</span></td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:TextBox TextMode="MultiLine" Rows="5" Columns="60" Width="302px" ID="streamurl"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Format:&nbsp;<span style="color: Red">*</span></td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:DropDownList ID="ddlmsgformat" Width="150" runat="Server">
                            <asp:ListItem Value="-1" Selected="True">Select Video Format</asp:ListItem>
                            <asp:ListItem Value="wmv">Windows Media file</asp:ListItem>
                            <asp:ListItem Value="rm">Real Video</asp:ListItem>
                            <asp:ListItem Value="qt">Quicktime</asp:ListItem>
                            <asp:ListItem Value="Flash Video">Flash Video</asp:ListItem>
                            <asp:ListItem Value="RTMP">Flash RTMP</asp:ListItem>
                            <asp:ListItem Value="code">Code</asp:ListItem>
                            <asp:ListItem Value="silverlight">Silverlight</asp:ListItem>
                            <asp:ListItem Value="other">Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Language:</td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                       <asp:ListBox ID="lstLanguages" runat="server" SelectionMode="Multiple" Height="150"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Upload logo for Channel:</td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:FileUpload ID="fp1" runat="server" />
                        <br />
                        <asp:CustomValidator ID="cvFileUpload" runat="server" ErrorMessage="Only .jpg, .png or .gif files allowed"
                            ClientValidationFunction="Checkfiles" ControlToValidate="fp1" SetFocusOnError="True"
                            ValidateEmptyText="True" Style="vertical-align: top;" Font-Names="Arial" Font-Size="Small"></asp:CustomValidator>
                        <div>
                            <asp:Label ID="lbluploadmsg" runat="server"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Your Email:</td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:TextBox MaxLength="100" Width="150" ID="txtEmail" runat="server"></asp:TextBox>
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        Status</td>
                    <td style="background-color: #EBEBEB;vertical-align:top;">
                        <asp:RadioButton ID="rbapp" runat="server" Text="Approve" GroupName="adoption" />
                        <asp:RadioButton ID="rbnapp" runat="server" Text="Not Approved" GroupName="adoption" Checked="true" />
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="background-color: #D2D2D2;">
                        <div style="align-content:center;">
                            <asp:Button ID="btnAddmore" runat="server" Text="Submit" OnClick="btnAddmore_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

