﻿using System;
using System.Data;

public partial class DigipSuperAdmin_AjaxCountry_Admin : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, System.EventArgs e)
    {
        int intRegionId = 0;
        if (!string.IsNullOrWhiteSpace(Request["region"]))
        {
            intRegionId = Convert.ToInt32(Request["region"]);
        }
        string strBOdy = "<select name='cbocountry' id='cbocountry'><option value='' >- Select Country - </option><br>" + getCountry(intRegionId) + "</select>";
        Response.Write(strBOdy);
    }

    public string getCountry(int Rid)
    {
        string strSelect = "";
        string strQuery = "select country_name,country_id from country where  region_id='" + Rid + "' Order by country_name";
        DataTable dt = dp.FillDataTable(strQuery);
        foreach (DataRow dr in dt.Rows)
        {
            strSelect += "<option value='" + Convert.ToString(dr["country_id"]) + " '>" + Convert.ToString(dr["country_name"]) + "</option>";
        }
        return strSelect;
    }
}