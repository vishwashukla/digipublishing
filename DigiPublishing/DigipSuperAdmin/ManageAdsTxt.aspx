﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageAdsTxt.aspx.cs" Inherits="DigipSuperAdmin_ManageAdsTxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="mvManageAdstxt" runat="server">
        <asp:View ID="viewAdstxt" runat="server">
            <div>
                <table style="width:100%;">
                    <tr>
                        <td></td>
                        <td>
                            <asp:HiddenField ID="hdnID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h4>Manage Ads.txt</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dgrdAdsTxt" runat="server" GridLines="none" AutoGenerateColumns="False"
                                Width="795px" BorderWidth="1" AllowSorting="true" OnItemCommand="dgrdAdsTxt_ItemCommand"
                                 OnSortCommand="dgrdAdsTxt_SortCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="domainname" HeaderText="Domain Name" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Domain Name" SortExpression="domainname">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdomainname" runat="server" CommandName="Edit" Text='<%#Eval("domainname") %>'
                                                CssClass="toplink" CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="genre" HeaderText="Site Genre" SortExpression="genre"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:View>
        <asp:View ID="viewEditAdstxt" runat="server">
            <table style="width:100%;">
                <tr>
                    <td class="txtDisplay" style="height: 24px; width: 199px;">Domain Name</td>
                    <td style="height: 24px">
                        <asp:Label ID="lblDomainName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Ads.txt</td>
                    <td>
                        <asp:TextBox ID="txtAdsTxt" runat="server" Height="197px" TextMode="MultiLine" Width="457px"></asp:TextBox></td>
                </tr>
                <tr><td></td>
                    <td >
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="Mngadstxt" CausesValidation="true" />
                    
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

