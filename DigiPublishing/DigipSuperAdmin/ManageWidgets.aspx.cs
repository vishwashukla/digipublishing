﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Drawing;
using System.Xml;
using System.Text.RegularExpressions;
using System.Net;
using System.Linq;

public partial class DigipSuperAdmin_ManageWidgets : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public static string objvalue;
    public static string strID;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Widgets";
        lblMsg.Text = "";
        if (!IsPostBack)
        {
            GetLastDate();
            BindTestSites();
            GetComponentDataBind();         
            this.showexDiv.Attributes.Add("style", "display:inline");
            this.showmyDiv.Attributes.Add("style", "display:none");
        }
    }

    private void GetComponentDataBind()
    {
        dgrdComponent.DataSource = getDT();
        dgrdComponent.DataBind();
        mvManageComp.SetActiveView(viewComponent);
    }

    /*public void SetFCKPath()
    {
        string strPath = Request.ApplicationPath;
        if (strPath == "/")
        {
            txtCodebox.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
        }
        else
        {
            txtCodebox.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
        }
    }

    protected void FCKDesc_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedField(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, txtCodebox.GetType(), "FCKUpdater", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, txtCodebox.GetType(), "WebContentManagerEditorScript_" + this.txtCodebox.ClientID, "FCKUpdateLinkedField('" + this.txtCodebox.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(txtCodebox.GetType(), "FCKUpdater", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(txtCodebox.GetType(), "WebContentManagerEditorScript_" + this.txtCodebox.ClientID, "FCKUpdateLinkedField('" + this.txtCodebox.ClientID + "');");
        }
    }

    protected void txtCodebox_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.txtCodebox.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, txtCodebox.GetType(), "FCKSource", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, txtCodebox.GetType(), "WebContentManagerEditorSourceScript_" + this.txtCodebox.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(txtCodebox.GetType(), "FCKSource", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(txtCodebox.GetType(), "WebContentManagerEditorSourceScript_" + this.txtCodebox.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
    }*/

    private DataTable getDT()
    {
        DataTable dt = new DataTable();
        try
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Get_Widgets";
            cmd.CommandType = CommandType.StoredProcedure;
            dt = dp.FillDataTable(cmd);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        return dt;
    }

    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }

    protected void dgrdComponent_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = this.getDT();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    protected void dgrdComponent_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string applicationName = "";
            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton lnkApplicationame = (LinkButton)e.Item.FindControl("lnkApplicationame");
            DropDownList ddl_Position = (DropDownList)e.Item.FindControl("ddl_Position");
            DataRowView drv = (DataRowView)e.Item.DataItem;
            ddl_Position.SelectedValue = drv["Position"].ToString();
            if (lnkApplicationame != null)
                applicationName = lnkApplicationame.Text;

            lnkDelete.Attributes.Add("onclick", "javascript: return confirm('Are you sure to delete Widget [" + applicationName + "] ?');");

            HiddenField hdnRowColor = (HiddenField)e.Item.FindControl("hdnRowColor");
            if (hdnRowColor != null && hdnRowColor.Value!="")
                e.Item.BackColor =Color.FromName(hdnRowColor.Value);
        }
    }

    protected void dgrdComponent_OnItemCommand(object source, DataGridCommandEventArgs e)
    {
        string strCompID = e.Item.Cells[0].Text.Trim();
        if (e.CommandName == "Edit")
        {
            strID = e.Item.Cells[0].Text;
            resetControl();
            divbtn.Style.Add("display", "inline");
            editComponent(strCompID);
            DataTable dt = new DataTable();
            objvalue = "";
            string strSql = "SELECT  a.id, b.VastValues FROM   Brand_AppLists AS a"
                            + " INNER JOIN  Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                            + " WHERE  b.status = 1 AND  a.id = '" + strCompID + "'";
            dt = dp.FillDataTable(strSql);
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToString(dr["VastValues"]).Trim() != "")
                {
                    objvalue += utils.formatStringForDBSelect(Convert.ToString(dr["id"])) + ">>" + utils.formatStringForDBSelect(Convert.ToString(dr["VastValues"]));
                }
            }
            mvManageComp.SetActiveView(viewAddComp);
           
        }
        else if (e.CommandName == "Delete")
        {
            deleteComponent(strCompID);
        }
        else if (e.CommandName == "Remove")
        {
            removeWidgets(strCompID);
        }
        else if (e.CommandName == "Test")
        {
            TestUrlWidgets(strCompID);
        }
    }

    private void deleteComponent(string id)
    {
        if (utils.ExecuteNonQueryTrans("DELETE Brand_AppLists WHERE id='" + id + "'", lblMsg))
        {
            if (utils.ExecuteNonQueryTrans("DELETE FROM Brand_AppListRuleSettings WHERE AppList_ID='" + id + "'", lblMsg))
            {
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                string strSQlrepotrDbApplist = "EXEC [dbo].[SP_Brand_DELETE_AppLists] @APPID='" + id + "'";
                dpreport.ExecuteNonQuery(strSQlrepotrDbApplist);

                String msg = "Widget successfully deleted ";

                string strSql = "SELECT * FROM brand_domainname where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() 
                    + "' AND domainname<>'dev" + Convert.ToString(ConfigurationManager.AppSettings["saMainURL"]) + "'";
                DataTable dt = dp.FillDataTable(strSql);
                try
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            try
                            {
                                string strDomain = Convert.ToString(dr["domainname"]);
                                string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\" + strDomain + "\\";
                                DirectoryInfo dirInfo = new DirectoryInfo(strPath);
                                if (dirInfo.Exists)
                                {
                                    DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomain);
                                    string strDelete = "DELETE brand_applist where id=" + id + "";
                                    dp1.ExecuteNonQuery(strDelete);
                                }
                            }
                            catch
                            {
                            }
                        }
                        msg += "and successfully removed from all sites.";
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("timeout expired"))
                        msg += "But Timeout expired while removing from all sites.";
                    else
                        msg += "But Server processing error occured while removing from all sites.";
                }

                try
                {
                    string strSqlSelect = "";
                    strSqlSelect = " SELECT b.Position FROM Brand_AppLists AS a INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID WHERE b.status = 1 AND b.AppList_ID='" + id + "'";
                    DataTable dtSel = dp.FillDataTable(strSqlSelect);

                    string StrDefaultPosition = dtSel.Rows[0]["Position"].ToString();

                    string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscache&id=" + Convert.ToInt32(id) 
                        + "&Cachekey1=WidgetTop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&position=" + StrDefaultPosition;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscache&id=" 
                        + Convert.ToInt32(id) + "&Cachekey1=WidgetTop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&position=" + StrDefaultPosition;
                    HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                    HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                }
                catch
                {
                }

                this.lblMsg.Text = msg;
                GetComponentDataBind();
            }
        }
    }

    public void GetUrl(string domainName, string widgetid)
    {
        string strSqlSelect = string.Format("SELECT ISNULL(aspxUrl,'') as aspxUrl FROM Brand_AppListRuleSettings where AppList_ID=" + widgetid);
        string aspxId = Convert.ToString(dp.ExecuteScalar(strSqlSelect));
        if (aspxId.ToString() != "")
        {
            string strFileUrl = "http://" + domainName + "/widgets/aspx/" + aspxId + ".aspx";
            string fullURL = "window.open('" + strFileUrl + "', '_blank');";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        }
        else
        {
            string aspxUrl = getaplhaNumricString();
            aspxUrl = aspxUrl.Remove(aspxUrl.Length - 1);
            string strSqlUpdate = string.Format("update Brand_AppListRuleSettings set aspxUrl='" + aspxUrl + "' where AppList_ID = " + widgetid + "");
            dp.ExecuteNonQuery(Convert.ToString(strSqlUpdate));

            string strFileUrl = "http://" + domainName + "/widgets/aspx/" + aspxUrl + ".aspx";
            string fullURL = "window.open('" + strFileUrl + "', '_blank');";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        }
    }
    
    /// <summary>
    /// Remove the current Widgets from all the ondemand Sites.
    /// </summary>
    private void removeWidgets(string strWidgetID)
    {
        string strSql = "SELECT * FROM brand_domainname where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "' AND domainname<>'dev" + Convert.ToString(ConfigurationManager.AppSettings["saMainURL"]) + "'";
        DataTable dt = dp.FillDataTable(strSql);
        try
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    try
                    {
                        string strDomain = Convert.ToString(dr["domainname"]);
                        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\" + strDomain + "\\";
                        DirectoryInfo dirInfo = new DirectoryInfo(strPath);
                        if (dirInfo.Exists)
                        {
                            DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomain);
                            string strDelete = "DELETE brand_applist"
                            + " where id=" + strWidgetID + "";
                            dp1.ExecuteNonQuery(strDelete);
                        }
                    }
                    catch
                    {
                    }
                }
                this.lblMsg.Text = "Widget removed from all sites successfully.";
                GetComponentDataBind();
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.ToLower().Contains("timeout expired"))
                lblMsg.Text = "Timeout expired. You should try and run the task again.";
            else
                lblMsg.Text = "Server processing error.Please re-try.";
        }

        try
        {
            string strSqlSelect = "";
            strSqlSelect = " SELECT b.Position"
                            + " FROM Brand_AppLists AS a "
                            + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                            + " WHERE b.status = 1 AND b.AppList_ID='" + strWidgetID + "'";
            DataTable dtSel = dp.FillDataTable(strSqlSelect);

            string StrDefaultPosition = dtSel.Rows[0]["Position"].ToString();

            string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscache&id=" + Convert.ToInt32(strWidgetID) + "&Cachekey1=WidgetTop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&position=" + StrDefaultPosition;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscache&id=" + Convert.ToInt32(strWidgetID) + "&Cachekey1=WidgetTop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&position=" + StrDefaultPosition;
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch 
        { 
        }
    }

    private void TestUrlWidgets(string id)
    {
        string strSql = "SELECT a.ApplicationName,b.CodeBox FROM Brand_AppLists AS a "
                        + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                        + "WHERE b.status = 1 and CodeBox like'%video%' and CodeBox like'%.aspx%' AND b.AppList_ID =" + id;
        string strFileUrl = "";
        string AppCodeBox = "";
        string strcode = "";
        string[] strval;
        string[] strfinal;
        string strlink = "";//
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(strSql);
        if (dt.Rows.Count > 0)
        {
            AppCodeBox = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["CodeBox"]));
            if (AppCodeBox.Contains("a.src"))
            {
                strcode = AppCodeBox.Replace("/widgets/", "^");
                strval = strcode.Split('^');
                AppCodeBox = strval[0] + strval[1];
                strval[1] = strval[1].Replace("a.allowTransparency", "^");
                strfinal = strval[1].Split('^');
                strlink = strfinal[0];
                strlink = strlink.Replace("\";\r\n", "");
            }

            strFileUrl =  ConfigLoader.GetWidgetTestDomainUrl() + "widgets/" + strlink;
            string fullURL = "window.open('" + strFileUrl + "', '_blank');";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        }
        else
        {
            string strFileName = "";
            strFileName = CreateHTMLFile(id);
            if (strFileName != "")
            {
               // strFileUrl = ConfigLoader.GetWidgetTestDomainUrl() + "widgets/TestUrl/" + strFileName;             
                strFileUrl = ConfigLoader.GetWidgetTestDomainUrl() + "widgets/TestUrl/" + strFileName;             
                string fullURL = "window.open('" + strFileUrl + "', '_blank');";
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
            }
            else
            {
                lblMsg.Text = "File Not Found";
            }
        }
    }

    private string CreateHTMLFile(string id)
    {
        string strFileName = "";
        if (!Directory.Exists(Request.PhysicalApplicationPath + "widgets\\TestUrl\\"))
        {
            Directory.CreateDirectory(Request.PhysicalApplicationPath + "widgets\\TestUrl\\");
        }
        if (Directory.Exists(Request.PhysicalApplicationPath + "widgets\\TestUrl\\"))
        {
            string[] logList = Directory.GetFiles(Request.PhysicalApplicationPath + "widgets\\TestUrl\\", "*.html");
            foreach (string log in logList)
            {
                FileInfo logInfo = new FileInfo(log);
                if (logInfo.LastAccessTime < DateTime.Now.AddDays(-1))
                {
                    logInfo.Delete();
                }
            }
            string strSql = "SELECT a.ApplicationName,b.CodeBox FROM Brand_AppLists AS a "
                         + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                         + "WHERE b.status = 1 AND b.AppList_ID =" + id;
            string AppName = "";
            string CodeBox = "";
            DataTable dt = new DataTable();
            dt = dp.FillDataTable(strSql);
            if (dt.Rows.Count > 0)
            {
                AppName = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["ApplicationName"]));
                CodeBox = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["CodeBox"]));
            }
            strFileName = AppName + ".html";
            Stream streamfile = File.Create(Server.MapPath("~/widgets/TestUrl") + "/" + strFileName + "");
            StreamWriter sw = new StreamWriter(streamfile);
            sw.WriteLine("<html xmlns=\"http://www.w3.org/1999/xhtml\" >");
            sw.WriteLine("\t<head>");
            sw.WriteLine("\t<title>" + AppName + "</title>");
            sw.WriteLine("\t</head>");
            sw.WriteLine("\t<body>" + CodeBox + "</body>");
            sw.WriteLine("\t</html>");
            sw.Close();
            streamfile.Close();
        }
        return strFileName;
    }

    protected void lnkAddComponent_Click(object sender, EventArgs e)
    {
        resetControl();
        headerText.Text = "Add Widget"; 
        if(lstBrowsers.Items.Count>0)
            lstBrowsers.Items.Cast<ListItem>().Where(n => n.Value != "-1").Select(n => n).ToList().ForEach(n => n.Selected = true);
        rdbBrowserAll.Checked = true;
        divbtn.Style.Add("display", "inline");
        mvManageComp.SetActiveView(viewAddComp);
    }

    private void editComponent(string id)
    {
        resetControl();
        this.headerText.Text = "Edit Widget";
        string strBlocker = "";
        string strIPStatus = "";
        string strIPValue = "";
        string strIPDays = "";
        string strHeight = "";
        string strWidth = "";
        string strBlockerValue = "";
        DataTable dt = new DataTable();
        string strSql = "";
        strSql = " SELECT a.id, a.ApplicationName, b.Version, b.CodeBox,b.status,"
                        + " b.Image, b.cost,b.Position,"
                        + " b.VastValues, b.Region, b.Country,"
                        + " b.Browser, b.ScheduleDate, b.Referal, b.Not_Referal,"
                        + " b.Blocker, b.height, b.width, b.SameIP_Value, b.SameIP_Days, b.SameIP_Status, b.BlockerValue, b.AppList_ID , b.aspxUrl as aspxUrl, b.OS"
                        + " FROM Brand_AppLists AS a "
                        + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                        + " WHERE b.status = 1 AND b.AppList_ID='" + id + "'";
        dt = dp.FillDataTable(strSql);
        hdnCompID.Value = id;
        if (dt.Rows.Count > 0)
        {
            txtApplicationName.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["ApplicationName"]));          
            txtCodebox.Value = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["CodeBox"]));
            hdnValues.Value = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["VastValues"]));

            if (TreeView1.Nodes.Count > 0)
            {
                string strregion = Convert.ToString(dt.Rows[0]["Region"]);
                if(strregion.Trim()!="")
                    TreeView1.Nodes.Cast<TreeNode>().Where(n => strregion.Contains(n.Value)).Select(n => n).ToList().ForEach(n => n.Checked = true);

                foreach (TreeNode tnSite in TreeView1.Nodes)
                {
                    string strcountry = Convert.ToString(dt.Rows[0]["Country"]);
                    if(strcountry.Trim()!="")
                        tnSite.ChildNodes.Cast<TreeNode>().Where(n => strcountry.Contains(n.Value)).Select(n => n).ToList().ForEach(n => n.Checked = true);
                }
            }
            if (Convert.ToString(dt.Rows[0]["Browser"]) != "")
            {
                string strBrowsers = Convert.ToString(dt.Rows[0]["Browser"]);
                if (strBrowsers.TrimEnd(',') != "")
                    utils.SetSelectedListBoxItems(lstBrowsers, strBrowsers);
            }
            if (Convert.ToString(dt.Rows[0]["OS"]) != "")
            {
                string strOS = Convert.ToString(dt.Rows[0]["OS"]);
                if (strOS.TrimEnd(',') != "")
                    utils.SetSelectedListBoxItems(lstOS, strOS);
            }

            string retval = string.Empty;
            retval = lstBrowsers.Items.Cast<ListItem>().Where(item => item.Selected==false && item.Value!="-1").Aggregate(retval, (current, item) => current + (item.Value + ','));
            if (retval != "")
                rdbBrowserSel.Checked = true;
            else
                rdbBrowserAll.Checked = true;

            txtReferalfrom.Text = Convert.ToString(dt.Rows[0]["Referal"]);
            txtNotReferalfrom.Text = Convert.ToString(dt.Rows[0]["Not_Referal"]);
            ddl_DefaultLocation.SelectedValue = Convert.ToString(dt.Rows[0]["Position"]);

            if (Convert.ToString(dt.Rows[0]["Blocker"]) == "")
            {
                strBlocker = "0";
            }
            else
            {
                strBlocker = Convert.ToString(dt.Rows[0]["Blocker"]);
            }
            CheckBlock1.Checked = Convert.ToBoolean(Convert.ToInt32(strBlocker));
            if (Convert.ToString(dt.Rows[0]["height"]) == "")
            {
                strHeight = "0";
            }
            else
            {
                strHeight = Convert.ToString(dt.Rows[0]["height"]);
            }

            if (Convert.ToString(dt.Rows[0]["width"]) == "")
            {
                strWidth = "0";
            }
            else
            {
                strWidth = Convert.ToString(dt.Rows[0]["width"]);
            }
            txtWidthxHeight.Text = strWidth + "x" + strHeight;
            if (Convert.ToString(dt.Rows[0]["SameIP_Status"]) == "")
            {
                strIPStatus = "0";
            }
            else
            {
                strIPStatus = Convert.ToString(dt.Rows[0]["SameIP_Status"]);
            }

            if (Convert.ToString(dt.Rows[0]["SameIP_Value"]) == "")
            {
                strIPValue = "1";
            }
            else
            {
                strIPValue = Convert.ToString(dt.Rows[0]["SameIP_Value"]);
            }

            if (Convert.ToString(dt.Rows[0]["SameIP_Days"]) == "")
            {
                strIPDays = "1";
            }
            else
            {
                strIPDays = Convert.ToString(dt.Rows[0]["SameIP_Days"]);
            }
            
            if (Convert.ToString(dt.Rows[0]["BlockerValue"]) == "")
            {
                strBlockerValue = "1";
            }
            else
            {
                strBlockerValue = Convert.ToString(dt.Rows[0]["BlockerValue"]);
            }
        }
        if (strIPStatus == "0")
        {
            chkSameIPBlocker.Checked = false;
        }
        else
        {
            chkSameIPBlocker.Checked = true;
        }
        txtSameIPBlocker.Text = strIPValue;
        TextCheckBlock1.Text = strBlockerValue;
        txtSameIPDays.Text = strIPDays;
        if (hdnValues.Value != "")
        {
            this.showmyDiv.Attributes.Add("style", "display:inline");
            this.showexDiv.Attributes.Add("style", "display:none");
        }
        else
        {
            this.showexDiv.Attributes.Add("style", "display:inline");
            this.showmyDiv.Attributes.Add("style", "display:none");
        }

        DBCommand cmd = new DBCommand();
        cmd.CommandText = "BRAND_Name_Of_Agencies_For_Widget";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.AddParameter("@ID", id.Trim());
        DataTable dtsites = dp.FillDataTable(cmd);
        if (dtsites.Rows.Count > 0)
        {
            string strSites = Convert.ToString(dtsites.Rows[0]["Domain"]);
            if (strSites.TrimEnd(',') != "")
                utils.SetSelectedListBoxText(lstSites, strSites);
        }
    }

    private void resetControl()
    {
        hdnCompID.Value = "-1";
        txtApplicationName.Text = "";
        txtCodebox.Value = "";
        TreeView1.Nodes.Clear();

        rdbBrowserAll.Checked = false;
        rdbBrowserSel.Checked = false;
        lstBrowsers.ClearSelection();
        lstGenre.ClearSelection();
        lstSites.ClearSelection();
        lstOS.ClearSelection();

        txtReferalfrom.Text = "";
        txtNotReferalfrom.Text = "";
        ddl_DefaultLocation.SelectedValue = "";
        CheckBlock1.Checked = false;
        txtWidthxHeight.Text = "";
        chkSameIPBlocker.Checked = false;
        TextCheckBlock1.Text = "1";
        txtSameIPDays.Text = "";
        BindTreeData(TreeView1);
        BindDevicesBrowsers();
        //SetFCKPath();
        BindGenre();
        BindSites();
        lstGenre.Attributes.Add("onclick", "GetSites('" + lstGenre.ClientID + "','" + lstSites.ClientID + "')");
        this.rdbBrowserAll.Attributes.Add("onclick", "Browserall('" + rdbBrowserAll.ClientID + "','" + lstBrowsers.ClientID + "')");
        this.rdbBrowserSel.Attributes.Add("onclick", "BrowserSel('" + rdbBrowserSel.ClientID + "','" + lstBrowsers.ClientID + "')");
    }
    
    public string getaplhaNumricString()
    {   string plainText = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);        
        return System.Convert.ToBase64String(plainTextBytes);       
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string appName = string.Empty;
            string appCodebox = string.Empty;
            string appKey = string.Empty;
            string appSecretKey = string.Empty;
            string strregion = string.Empty;
            string strcountry = string.Empty;
            string image = string.Empty;
            double cost = 0.0;
            int IsActive = 1;
            string videoUrl = string.Empty;
            string clickThroughUrl = string.Empty;
            string ImpressionUrl = string.Empty;
            string ScheduleDate = string.Empty;
            string StrDefaultPosition = string.Empty;
            string strCheckBlock1 = string.Empty;
            int strtxtWidth = 0;
            int strtxtHeight = 0;
            if (Convert.ToString(txtWidthxHeight.Text).Trim() != "")
            {
                string[] widthheight = Convert.ToString(txtWidthxHeight.Text).Trim().Split('x');
                strtxtHeight = Convert.ToInt32(widthheight[1].Trim());
                strtxtWidth = Convert.ToInt32(widthheight[0].Trim());
            }
            strCheckBlock1 = Convert.ToString(Convert.ToUInt32(CheckBlock1.Checked));
            string strIPStatus;
            int strIPValue;
            int strIPDays;
            if (chkSameIPBlocker.Checked == true)
            {
                strIPStatus = "1";
            }
            else
            {
                strIPStatus = "0";
            }
            strIPValue = Convert.ToInt16(txtSameIPBlocker.Text);
            strIPDays = txtSameIPDays.Text==""?1:Convert.ToInt16(txtSameIPDays.Text);
            int strBlockerValue = 0;
            if (CheckBlock1.Checked == true)
            {
                strBlockerValue = Convert.ToInt32(TextCheckBlock1.Text);
            }
            else
            {
                strBlockerValue = 0;
            }
            StringBuilder strBrowser = new StringBuilder();
            strBrowser.Append(utils.GetSelectedListBoxItems(lstBrowsers));

            StringBuilder strOS = new StringBuilder();
            strOS.Append(utils.GetSelectedListBoxItems(lstOS));

            string strReferal = string.Empty;
            string strNotReferal = string.Empty;
            strReferal = txtReferalfrom.Text;
            strNotReferal = txtNotReferalfrom.Text;
            appName = utils.formatStringForDBInsert(txtApplicationName.Text);
            StrDefaultPosition = Convert.ToString(ddl_DefaultLocation.SelectedValue);
            appCodebox = utils.formatStringForDBInsertWidget(txtCodebox.Value);
            string compiledstring;
            string firststr, height1, width1, strstr2, laststr, repalce, midstr;
            compiledstring = appCodebox;
            string[] temp = null;
            if (appCodebox.Contains(" height") && appCodebox.Contains(" width"))
            {
                if (strtxtHeight == 0 && strtxtWidth == 0)
                {
                    appCodebox = compiledstring;
                }
                else
                {
                    if (appCodebox.IndexOf(" height") > appCodebox.IndexOf(" width"))
                    {
                        string[] split = null;
                        split = Regex.Split(appCodebox, " width");
                        firststr = split[0];
                        height1 = split[1];
                        split = Regex.Split(height1, " height");
                        width1 = split[0];
                        strstr2 = split[1];
                        split = Regex.Split(strstr2, " ");
                        if (split[0] == strstr2)
                        {
                            string tempstr = strstr2.Replace("=&quot;", "");
                            int firstindex = tempstr.IndexOf("&quot;");
                            string tempheight = tempstr.Substring(0, firstindex);
                            split[0] = "=&quot;" + tempheight + "&quot;";
                        }
                        temp = Regex.Split(height1, " ");
                        if (temp[0].Contains("=") && temp[1].Contains("height"))
                        {
                            midstr = " ";
                            repalce = split[0];
                            split = Regex.Split(strstr2, repalce);
                            laststr = split[1];
                        }
                        else
                        {
                            midstr = " " + temp[1] + " ";
                            repalce = split[0];
                            split = Regex.Split(strstr2, repalce);
                            laststr = split[1];
                        }
                        appCodebox = firststr + " height=\"" + strtxtHeight + "\"" + midstr + "width=\"" + strtxtWidth + "\"" + laststr;
                    }
                    else
                    {
                        string[] split = null;
                        split = Regex.Split(appCodebox, " height");
                        firststr = split[0];
                        height1 = split[1];
                        split = Regex.Split(height1, " width");
                        width1 = split[0];
                        strstr2 = split[1];

                        split = Regex.Split(strstr2, " ");
                        if(split[0] == strstr2)
                        {
                            string tempstr = strstr2.Replace("=&quot;", "");
                            int firstindex = tempstr.IndexOf("&quot;");
                            string tempwidth = tempstr.Substring(0, firstindex);
                            split[0] = "=&quot;" + tempwidth + "&quot;";
                        }
                        temp = Regex.Split(height1, " ");
                        if (temp[0].Contains("=") && temp[1].Contains("width"))
                        {
                            midstr = " ";
                            repalce = split[0];
                            split = Regex.Split(strstr2, repalce);
                            laststr = split[1];
                        }
                        else
                        {
                            midstr = " " + temp[1] + " ";
                            repalce = split[0];
                            split = Regex.Split(strstr2, repalce);
                            laststr = split[1];
                        }
                        appCodebox = firststr + " height=\"" + strtxtHeight + "\"" + midstr + "width=\"" + strtxtWidth + "\"" + laststr;
                    }
                }
            }
            else
            {
                appCodebox = compiledstring;
            }

            foreach (TreeNode tnSite in TreeView1.Nodes)
            {
                string str = utils.GetSelectedChildTreeNodes(tnSite);
                if (str != "")
                {
                    strregion += tnSite.Value + ",";
                    strcountry += str+",";
                }
            }
            strregion = strregion.TrimEnd(',');
            strcountry = strcountry.TrimEnd(',');

            string addedValue = hdnValues.Value.Replace("BUTTON,", ";");
            if (addedValue.LastIndexOf("BUTTON") > 0)
            {
                addedValue = addedValue.Substring(0, addedValue.LastIndexOf("BUTTON"));
            }

            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);

            if (hdnCompID.Value == "-1")
            {
                string strSqlInsert;

                strSqlInsert = "insert into Brand_AppLists (ApplicationName) Values('" + appName + "')";
                strSqlInsert += " Select Scope_Identity() as id";
                DataTable dtq = dp.FillDataTable(strSqlInsert);

                if (dtq.Rows.Count > 0)
                {
                    hdnCompID.Value = Convert.ToString(dtq.Rows[0]["id"]);
                    if (appCodebox.Contains(".aspx&quot;"))
                    {
                        string newstring = appCodebox.Replace(".aspx&quot;", "^");
                        string[] str = newstring.Split('^');
                        appCodebox = str[0] + ".aspx?id=" + hdnCompID.Value + "\"" + str[1];
                    }

                    string strSQlrepotrDbApplist = "EXEC [dbo].[SP_Brand_INSERT_AppLists] @APPID='" + hdnCompID.Value + "',@APPName='" + appName + "'";
                    dpreport.ExecuteNonQuery(strSQlrepotrDbApplist);
                    StringBuilder strSqlInsertSetting = new StringBuilder();
                    strSqlInsertSetting.Append("insert into Brand_AppListRuleSettings ([CodeBox]   ");
                    strSqlInsertSetting.Append(" ,[status],[Image],[cost] ");
                    strSqlInsertSetting.Append(" ,[VastValues],[Region],[Country],[Browser],[ScheduleDate] ");
                    strSqlInsertSetting.Append(" ,[Referal],[Not_Referal],[Position],[Blocker],[height],[width],[SameIP_Status],[SameIP_Value],[BlockerValue],AppList_ID,[aspxUrl],[OS],[SameIP_Days])");
                    strSqlInsertSetting.Append("values ('");
                    strSqlInsertSetting.Append(appCodebox + "',");
                    strSqlInsertSetting.Append(IsActive + ",'" + image + "'," + cost);
                    strSqlInsertSetting.Append(",'" + addedValue + "','" + Convert.ToString(strregion) + "','" + Convert.ToString(strcountry) + "'");
                    strSqlInsertSetting.Append(",'" + Convert.ToString(strBrowser));
                    strSqlInsertSetting.Append("','" + ScheduleDate);
                    strSqlInsertSetting.Append("','" + strReferal + "','" + strNotReferal);
                    strSqlInsertSetting.Append("','" + ddl_DefaultLocation.SelectedValue + "','" + strCheckBlock1 + "','" + strtxtHeight + "','" + strtxtWidth);
                    strSqlInsertSetting.Append("','" + strIPStatus + "','" + strIPValue + "','" + strBlockerValue + "'," + hdnCompID.Value + ",'" + getaplhaNumricString() + "','" + strOS + "','" + strIPDays + "')");
                    utils.ExecuteNonQueryTrans(Convert.ToString(strSqlInsertSetting), lblMsg);
                }
            }
            else
            {
                if (appCodebox.Contains(".aspx&quot;"))
                {
                    string newstring = appCodebox.Replace(".aspx&quot;", "^");
                    string[] str = newstring.Split('^');
                    appCodebox = str[0] + ".aspx?id=" + hdnCompID.Value + "\"" + str[1];
                }
                StringBuilder strSqlUpdate = new StringBuilder();
                strSqlUpdate.Append("update Brand_AppLists set ApplicationName='" + appName + "' where id=" + hdnCompID.Value + ";");
                strSqlUpdate.Append("update Brand_AppListRuleSettings set ");
                strSqlUpdate.Append(" [CodeBox] = '" + appCodebox + "'");
                strSqlUpdate.Append(" ,[status] =" + IsActive + ",[Image]='" + image + "',[cost]=" + cost);
                strSqlUpdate.Append(" ,[VastValues] = '" + addedValue + "',[Region] = '" + Convert.ToString(strregion) + "',[Country] = '" + Convert.ToString(strcountry) + "'");
                strSqlUpdate.Append(" ,[Browser] = '" + Convert.ToString(strBrowser) + "'");
                strSqlUpdate.Append(" ,[Referal] = '" + strReferal + "',[Not_Referal] = '" + strNotReferal + "'");
                strSqlUpdate.Append(" ,[Position]='" + ddl_DefaultLocation.SelectedValue + "',[Blocker] ='" + strCheckBlock1 + "'");
                strSqlUpdate.Append(" ,[height]='" + strtxtHeight + "',[width] ='" + strtxtWidth + "'");
                strSqlUpdate.Append(" ,[SameIP_Status]='" + strIPStatus + "',[SameIP_Value] ='" + strIPValue + "',[BlockerValue] ='" + strBlockerValue + "',[OS]='" + strOS + "',[SameIP_Days]='" + strIPDays + "'");
                strSqlUpdate.Append(" where AppList_ID = " + hdnCompID.Value + "");
                dp.ExecuteNonQuery(Convert.ToString(strSqlUpdate));
                string strSQlrepotrDbApplist = "EXEC [dbo].[SP_Brand_UPDATE_AppLists] @APPID='" + hdnCompID.Value + "',@APPName='" + appName + "'";
                dpreport.ExecuteNonQuery(strSQlrepotrDbApplist);
            }
            if (hdnCompID.Value != "-1")
            {

                try
                {
                    if (lstSites.Items.Count > 0)
                    {
                        foreach (ListItem li in lstSites.Items)
                        {
                            DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(li.Text));
                            if (li.Selected == false && li.Value != "-1")
                            {
                                string strDomain = Convert.ToString(li.Text);
                                string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\" + strDomain + "\\";
                                DirectoryInfo dirInfo = new DirectoryInfo(strPath);
                                if (dirInfo.Exists)
                                {
                                    string strSqlSelect = "delete from brand_AppList where id = " + hdnCompID.Value;
                                    dpsite.ExecuteNonQuery(strSqlSelect);
                                }
                            }
                            if (li.Selected == true && li.Value != "-1")
                            {
                                string strDomain = Convert.ToString(li.Text);
                                string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\" + strDomain + "\\";
                                DirectoryInfo dirInfo = new DirectoryInfo(strPath);
                                if (dirInfo.Exists)
                                {
                                    string strSqlSelect = "Select * from brand_AppList where id = " + hdnCompID.Value;
                                    DataTable dtsiteAppList = dpsite.FillDataTable(strSqlSelect);

                                    string randomposition = "";
                                    if (StrDefaultPosition == "topbanner")
                                    {
                                        randomposition = "random_topbanner";
                                    }
                                    else if (StrDefaultPosition == "righttop")
                                    {
                                        randomposition = "random_right";
                                    }
                                    else if (StrDefaultPosition == "leftbottom")
                                    {
                                        randomposition = "random_left";
                                    }
                                    else if (StrDefaultPosition == "bottomright")
                                    {
                                        randomposition = "random_bottom";
                                    }
                                    else if (StrDefaultPosition == "pop_under")
                                    {
                                        randomposition = "random_pop_under";
                                    }
                                    else if (StrDefaultPosition == "topheader")
                                    {
                                        randomposition = "random_topheader";
                                    }
                                    else if (StrDefaultPosition == "subline")
                                    {
                                        randomposition = "random_subline";
                                    }
                                    else if (StrDefaultPosition == "midarticle")
                                    {
                                        randomposition = "random_midarticle";
                                    }
                                    string Sqlrandom = "select Value from brand_Applist_Random where Random_Position='" + randomposition + "'";
                                    int israndom = 0;
                                    DataTable dtisrandom = dpsite.FillDataTable(Sqlrandom);
                                    if (dtisrandom.Rows.Count > 0)
                                    {
                                        israndom = Convert.ToInt32(dtisrandom.Rows[0]["Value"]);
                                    }

                                    if (dtsiteAppList.Rows.Count > 0)
                                    {
                                        if (StrDefaultPosition != "")
                                        {
                                            StringBuilder strUpdate = new StringBuilder();
                                            strUpdate.Append("update brand_AppList set [ApplicationName] = '" + appName + "', ");
                                            strUpdate.Append("   [CodeBox] = '" + appCodebox + "'  ");
                                            strUpdate.Append(" , ");
                                            strUpdate.Append(" [status] =" + IsActive + ",[Image]='" + image + "',[cost]=" + cost + ",[Position]='" + StrDefaultPosition + "',[IsRandom]='" + israndom + "'");
                                            //strUpdate.Append(" [status] =" + IsActive + ",[Image]='" + image + "',[cost]=" + cost + ",[IsRandom]='" + israndom + "'");
                                            strUpdate.Append(" ,[VastValues] = '" + addedValue + "',[Region] = '" + Convert.ToString(strregion) + "', [Country] = '" + Convert.ToString(strcountry) + "'");
                                            strUpdate.Append(" ,[Browser] = '" + Convert.ToString(strBrowser) + "'");
                                            strUpdate.Append(" ,[Referal] = '" + strReferal + "',[Not_Referal] = '" + strNotReferal + "'");
                                            strUpdate.Append(" ,[Blocker] = '" + strCheckBlock1 + "',[height] = '" + strtxtHeight + "',[width] = '" + strtxtWidth + "'");
                                            strUpdate.Append(" ,[SameIP_Status] = '" + strIPStatus + "',[SameIP_Value] = '" + strIPValue + "',[BlockerValue] = '" + strBlockerValue + "',[OS]='" + strOS + "',[SameIP_Days]='" + strIPDays + "'");
                                            strUpdate.Append(" where id = " + hdnCompID.Value);
                                            dpsite.ExecuteNonQuery(Convert.ToString(strUpdate));
                                        }
                                    }
                                    else
                                    {
                                        if (StrDefaultPosition != "")
                                        {
                                            string strSqlInsert1 = "insert into brand_AppList ([id],[ApplicationName],[CodeBox],[status],[Image]   "
                                                    + " ,[cost],[Position],[IsRandom],[VastValues],[Region],[Country] "
                                                    + " ,[Browser],[Referal],[Not_Referal],[Blocker],[height],[width],[SameIP_Status],[SameIP_Value],[BlockerValue],[OS],[SameIP_Days])"
                                                    + " values (" + hdnCompID.Value + ",'" + appName + "','" + appCodebox + "',"
                                                    + IsActive + ",'" + image + "'," + cost + ",'" + StrDefaultPosition + "','" + israndom + "','"
                                                    + addedValue + "','"
                                                    + Convert.ToString(strregion) + "','" + Convert.ToString(strcountry) + "','"
                                                    + Convert.ToString(strBrowser) + "','" + strReferal + "','" + strNotReferal + "','"
                                                    + strCheckBlock1 + "','" + strtxtHeight + "','"
                                                    + strtxtWidth + "','" + strIPStatus + "','" + strIPValue + "','" + strBlockerValue + "','" + strOS + "','" + strIPDays + "')";
                                            dpsite.ExecuteNonQuery(strSqlInsert1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    try
                    {
                        string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscache&id=" + Convert.ToInt32(hdnCompID.Value) + "&Cachekey1=WidgetTop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&position=" + StrDefaultPosition;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscache&id=" + Convert.ToInt32(hdnCompID.Value) + "&Cachekey1=WidgetTop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&position=" + StrDefaultPosition;
                        HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                        HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                    }
                    catch{ }
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("timeout expired"))
                        lblMsg.Text = "Timeout expired. You should try and run the task again.";
                    else
                        lblMsg.Text = "Server processing error.Please re-try.";
                }
            }
            string strFileName = appName + ".xml";
            DeleteVastXml(appName);
            string[] arrValues = addedValue.Split(';');
            if (addedValue.Length > 0)
            {
                for (int i = 0; i < arrValues.Length; i++)
                {
                    string textvalue = arrValues[i];
                    string[] arrtextValues = new string[5];
                    if (textvalue.Contains(","))
                        arrtextValues = textvalue.Split(',');
                    if (arrtextValues.Length > 0)
                    {
                        if (arrtextValues[0] != "")
                            videoUrl = arrtextValues[0];

                        if (arrtextValues[1] != "")
                            clickThroughUrl = arrtextValues[1];

                        if (arrtextValues[2] != "")
                            ImpressionUrl = arrtextValues[2];
                    }
                    if (i == 0)
                    {
                        strFileName = appName + ".xml";
                    }
                    else
                    {
                        strFileName = appName + Convert.ToString(i) + ".xml";
                    }
                    if ((!string.IsNullOrWhiteSpace(videoUrl)) || (!string.IsNullOrWhiteSpace(clickThroughUrl)) || (!string.IsNullOrWhiteSpace(ImpressionUrl)))
                    {
                        CreateVastXml(videoUrl, clickThroughUrl, ImpressionUrl, strFileName);
                    }
                }
            }
            resetControl();
            GetComponentDataBind();
        }
        catch (ArgumentException errArgument)
        {
            lblMsg.Text = "File type is not supported. Only image files of extension .jpg or .gif are allowed." + Convert.ToString(errArgument.Message);
            return;
        }
    }

    protected void DeleteVastXml(string Name)
    {
        if (Directory.Exists(Request.PhysicalApplicationPath + "widgets\\Vast\\"))
        {
            string[] logList = Directory.GetFiles(Request.PhysicalApplicationPath + "widgets\\Vast\\", "*.xml");
            foreach (string log in logList)
            {
                FileInfo logInfo = new FileInfo(log);
                string logInfoName = logInfo.Name.Substring(0, logInfo.Name.LastIndexOf('.'));
                if (logInfoName.Length >= Name.Length)
                {
                    if (Name.Equals(logInfoName.Substring(0, Name.Length)))
                    {
                        logInfo.Delete();
                    }
                }
            }
        }
    }

    public XmlDocument CreateVastXml(string videoUrl, string clickThroughUrl, string ImpressionUrl, string strFileName)
    {
        XmlDocument xdoc = new XmlDocument();
        if (Directory.Exists(Request.PhysicalApplicationPath + "widgets\\Vast"))
        {
            Stream streamfile = File.Create(Server.MapPath("~/widgets/Vast") + "/" + strFileName + "");
            StreamWriter sw = new StreamWriter(streamfile);
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sw.WriteLine("<VAST version=\"2.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"vast.xsd\">");
            sw.WriteLine("\t<Ad id=\"DART_DFA\">");
            sw.WriteLine("\t<InLine>");
            sw.WriteLine("\t<AdSystem version=\"2.0\">DART</AdSystem>");
            sw.WriteLine("\t<AdTitle>Own Served</AdTitle>");
            sw.WriteLine("\t<Description />");
            sw.WriteLine("\t<Impression id=\"DART\"><![CDATA[]]></Impression>");
            sw.WriteLine("\t<Creatives>");
            sw.WriteLine("\t<Creative AdID=\"BMB-CARL057-030_Video_Initiative_New\">");
            sw.WriteLine("\t<Linear>");
            sw.WriteLine("\t<TrackingEvents />");
            sw.WriteLine("\t<AdParameters />");
            sw.WriteLine("\t<VideoClicks>");
            sw.WriteLine("\t<ClickThrough><![CDATA[]]>");
            sw.WriteLine("\t</ClickThrough>");
            sw.WriteLine("\t<ClickTracking />");
            sw.WriteLine("\t</VideoClicks>");
            sw.WriteLine("\t<MediaFiles>");
            sw.WriteLine("\t<MediaFile delivery=\"streaming\" type=\"video/x-flv\" width=\"500\" height=\"331\"><![CDATA[]]>");
            sw.WriteLine("\t</MediaFile>");
            sw.WriteLine("\t</MediaFiles>");
            sw.WriteLine("\t</Linear>");
            sw.WriteLine("\t</Creative>");
            sw.WriteLine("\t</Creatives>");
            sw.WriteLine("\t</InLine>");
            sw.WriteLine("\t</Ad>");
            sw.WriteLine("\t</VAST>");
            sw.Close();
            streamfile.Close();
            string docName = Request.PhysicalApplicationPath + "widgets\\Vast\\" + strFileName;
            xdoc.Load(docName);
            XmlNodeList resultNodes = xdoc.GetElementsByTagName("InLine");
            foreach (XmlNode aNode in resultNodes)
            {
                XmlNode Impression = aNode.SelectSingleNode("Impression");
                if (!string.IsNullOrWhiteSpace(ImpressionUrl) && (Impression != null))
                {
                    Impression.InnerXml = "<![CDATA[" + ImpressionUrl.Replace(" ", "") + "]]>";
                }
            }
            resultNodes = xdoc.GetElementsByTagName("Linear");
            foreach (XmlNode aNode in resultNodes)
            {
                XmlNode VideoClicks = aNode.SelectSingleNode("VideoClicks");
                XmlNode ClickThrough = VideoClicks.SelectSingleNode("ClickThrough");
                if (!string.IsNullOrWhiteSpace(clickThroughUrl) && (ClickThrough != null))
                {
                    ClickThrough.InnerXml = "<![CDATA[" + clickThroughUrl.Replace(" ", "") + "]]>";
                }
            }
            resultNodes = xdoc.GetElementsByTagName("MediaFiles");
            foreach (XmlNode aNode in resultNodes)
            {
                XmlNode MediaFile = aNode.SelectSingleNode("MediaFile");
                if (!string.IsNullOrWhiteSpace(videoUrl) && (MediaFile != null))
                {
                    MediaFile.InnerXml = "<![CDATA[" + videoUrl.Replace(" ", "") + "]]>";
                }
            }
            xdoc.Save(docName);
        }
        return xdoc;
    }
         
    protected void lnkExportInfo_Click(object sender, EventArgs e)
    {
        StringBuilder strB = new StringBuilder();
        strB = strBldrGetList();
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=widget.xml");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xml";
        Response.Write(strB);
        Response.End();
    }

    private StringBuilder strBldrGetList()
    {
        StringBuilder str = new StringBuilder();
        DataTable dtFileGroup = getDT();
        if (dtFileGroup.Rows.Count > 0)
        {
            str.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            str.Append("<body>");
            str.Append("<title>widget</title>");
            str.Append("<fields>");
            foreach (DataRow drFileGroup in dtFileGroup.Rows)
            {
                string strAppname = Convert.ToString(drFileGroup["ApplicationName"]);
                string strCodeBox = Convert.ToString(drFileGroup["CodeBox"]);
                string strSites = Convert.ToString(drFileGroup["Sites"]);
                str.Append("<field>");
                str.AppendLine();
                str.Append("<Application>" + Convert.ToString(drFileGroup["ApplicationName"]) + "</Application>");
                str.AppendLine();
                str.Append("<CodeBox><![CDATA[" + utils.formatStringForDBSelect(Convert.ToString(drFileGroup["CodeBox"])) + "]]></CodeBox>");
                str.AppendLine();
                str.Append("<Sites>" + Convert.ToString(drFileGroup["Sites"]) + "</Sites>");
                str.AppendLine();
                str.Append("</field>");
                str.AppendLine();
            }
            str.Append("</fields>");
        }
        str.Append("</body>");
        return str;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/DigipSuperAdmin/ManageWidgets.aspx");
    }

    public void BindTreeData(TreeView tree)
    {
        string query = "select region_name as name,region_id as id from bbc_region order by name asc";
        DataTable dt = dp.FillDataTable(query);
        PopulateNodes(dt, tree.Nodes, false);
    }

    public void PopulateNodes(DataTable dt, TreeNodeCollection nodes, bool flag)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode("region", "0");
            tn.Text = Convert.ToString(dr["name"]);
            tn.Value = Convert.ToString(dr["id"]);
            tn.PopulateOnDemand = flag;
            nodes.Add(tn);
            DataTable dtCountry = new DataTable();
            string query = "select country_id as id,country_name as name from Country where region_id = " + Convert.ToInt32(Convert.ToString(dr["id"])) + "order by name asc";
            dtCountry = dp.FillDataTable(query);
            dtCountry.Columns.Add("country_id", Type.GetType("System.String"));
            if (dtCountry.Rows.Count > 0)
            {
                foreach (DataRow drcoun in dtCountry.Rows)
                {
                    if (Convert.ToInt32(drcoun["id"]) > 0 & Convert.ToInt32(drcoun["id"]) < 10)
                    {
                        drcoun["country_id"] = "00" + Convert.ToString(drcoun["id"]);
                    }
                    else if (Convert.ToInt32(drcoun["id"]) == 10)
                    {
                        drcoun["country_id"] = "0" + Convert.ToString(drcoun["id"]);
                    }
                    else if (Convert.ToInt32(drcoun["id"]) > 10 & Convert.ToInt32(drcoun["id"]) < 100)
                    {
                        drcoun["country_id"] = "0" + Convert.ToString(drcoun["id"]);
                    }
                    else if (Convert.ToInt32(drcoun["id"]) == 100)
                    {
                        drcoun["country_id"] = Convert.ToString(drcoun["id"]);
                    }
                    else if (Convert.ToInt32(drcoun["id"]) > 100)
                    {
                        drcoun["country_id"] = Convert.ToString(drcoun["id"]);
                    }
                }
            }
            PopulateChildnodes(dtCountry, tn.ChildNodes, false);
        }
    }

    protected void PopulateChildnodes(DataTable dt, TreeNodeCollection nodes, bool flag)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode("country", "0");
            tn.Text = Convert.ToString(dr["name"]);
            tn.Value = Convert.ToString(dr["country_id"]);
            tn.PopulateOnDemand = flag;
            nodes.Add(tn);
        }
    }

    protected void BindDevicesBrowsers()
    {
        DataTable dtBrowsers = new DataTable();
        String SqlBrowsers = "SELECT ID,Device_Browser,Value FROM brand_Devices_Browsers Where Type='2'";
        dtBrowsers = dp.FillDataTable(SqlBrowsers);

        lstBrowsers.DataSource = dtBrowsers;
        lstBrowsers.DataTextField = "Device_Browser";
        lstBrowsers.DataValueField = "Value";
        lstBrowsers.DataBind();
        lstBrowsers.Items.Insert(0, new ListItem("----Select Browser----", "-1"));
        lstBrowsers.Items.Add(new ListItem("(Other)", "0"));
    }

    protected void BindGenre()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_GenreAll";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        lstGenre.DataSource = dt;
        lstGenre.DataTextField = "genre_name";
        lstGenre.DataValueField = "ID";
        lstGenre.DataBind();
        lstGenre.Items.Insert(0, new ListItem("----------------Select Genre----------------", "-1"));
    }

    protected void BindSites()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_SitesAll";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        lstSites.DataSource = dt;
        lstSites.DataTextField = "domainname";
        lstSites.DataValueField = "site_id";
        lstSites.DataBind();
        lstSites.Items.Insert(0, new ListItem("-----------------Select Sites-----------------", "-1"));
    }

    protected void BindTestSites()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_SitesAll";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        drpTestSites.DataSource = dt;
        drpTestSites.DataTextField = "domainname";
        drpTestSites.DataValueField = "domainname";
        drpTestSites.DataBind();
        drpTestSites.Items.Insert(0, new ListItem("--Select--", "--Select--"));
    }

    

    protected void btnTestServer_Click(object sender, EventArgs e)
    {
        string datatoggle = this.HdnfieldDatatoggle.Value;
        string strWidgetID = Convert.ToString(this.hdnWidgetId.Value);
        string strdomain = Convert.ToString(this.hdntestDomain.Value);
        if (datatoggle == "btnASPX")
        {
            GetUrl(strdomain, strWidgetID);
        }
        else
        {
            TestUrlWidgets(strWidgetID, strdomain);
        }
    }

    private void TestUrlWidgets(string id, string domain)
    {
        string strSql = "SELECT a.ApplicationName,b.CodeBox FROM Brand_AppLists AS a "
                        + "INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID "
                        + "WHERE b.status = 1 and CodeBox like'%video%' and CodeBox like'%.aspx%' AND b.AppList_ID =" + id;
        string strFileUrl = "";
        string AppCodeBox = "";
        string strcode = "";
        string[] strval;
        string[] strfinal;
        string strlink = "";//
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(strSql);
        if (dt.Rows.Count > 0)
        {
            AppCodeBox = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["CodeBox"]));
            if (AppCodeBox.Contains("a.src"))
            {
                strcode = AppCodeBox.Replace("/widgets/", "^");
                strval = strcode.Split('^');
                AppCodeBox = strval[0] + strval[1];
                strval[1] = strval[1].Replace("a.allowTransparency", "^");
                strfinal = strval[1].Split('^');
                strlink = strfinal[0];
                strlink = strlink.Replace("\";\r\n", "");
            }

            strFileUrl = "http://" + domain + "/widgets/" + strlink;
            string fullURL = "window.open('" + strFileUrl + "', '_blank');";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        }
        else
        {
            string strFileName = "";
            strFileName = CreateHTMLFile(id);
            if (strFileName != "")
            {
                // strFileUrl = ConfigLoader.GetWidgetTestDomainUrl() + "widgets/TestUrl/" + strFileName;             
                strFileUrl = "http://" + domain + "/widgets/TestUrl/" + strFileName;
                string fullURL = "window.open('" + strFileUrl + "', '_blank');";
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
            }
            else
            {
                lblMsg.Text = "File Not Found";
            }
        }
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            foreach (DataGridItem dgi in dgrdComponent.Items)
            {
                CheckBox chkStatusClick = (CheckBox)dgi.FindControl("chkStatusClick");
                TextBox txtValueClick = (TextBox)dgi.FindControl("txtValueClick");

                CheckBox chkStatusClickIP = (CheckBox)dgi.FindControl("chkStatusClickIP");
                TextBox txtValueClickIP = (TextBox)dgi.FindControl("txtValueClickIP");
                TextBox txtValueClickIPDays = (TextBox)dgi.FindControl("txtValueClickIPDays");

                DropDownList ddl_Position = (DropDownList)dgi.FindControl("ddl_Position");

                string Id = Convert.ToString(dgi.Cells[0].Text);

                string sites = Convert.ToString(dgi.Cells[6].Text);
                try
                {
                    string StrDefaultPosition = ddl_Position.SelectedValue;

                    string strCheckBlock1 = Convert.ToString(Convert.ToUInt32(chkStatusClick.Checked));
                    int strBlockerValue = Convert.ToString(txtValueClick.Text) == "" ? 0 : Convert.ToInt16(txtValueClick.Text);

                    string strIPStatus = Convert.ToString(Convert.ToUInt32(chkStatusClickIP.Checked));
                    int strIPValue = Convert.ToString(txtValueClickIP.Text) == "" ? 0 : Convert.ToInt16(txtValueClickIP.Text);
                    int strIPDays = Convert.ToString(txtValueClickIPDays.Text) == "" ? 1 : Convert.ToInt16(txtValueClickIPDays.Text);


                    StringBuilder strSqlUpdate = new StringBuilder();
                    strSqlUpdate.Append("update Brand_AppListRuleSettings set ");
                    strSqlUpdate.Append("[Blocker] ='" + strCheckBlock1 + "'");
                    if (StrDefaultPosition != "")
                    {
                        strSqlUpdate.Append(",[Position]='" + StrDefaultPosition + "'");
                    }
                    strSqlUpdate.Append(",[SameIP_Status]='" + strIPStatus + "',[SameIP_Value] ='" + strIPValue + "',[BlockerValue] ='" + strBlockerValue + "'");
                    strSqlUpdate.Append(",[SameIP_Days]='" + strIPDays + "'");
                    strSqlUpdate.Append(" where AppList_ID = " + Id + "");
                    dp.ExecuteNonQuery(Convert.ToString(strSqlUpdate));

                    try
                    {
                        string[] domains = sites.Split(',');
                        if (domains.Length > 0)
                        {
                            foreach (string strDomain in domains)
                            {
                                DBHelper dpsite = ReadConfig.getDomainDBHelper(strDomain);
                                string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\" + strDomain + "\\";
                                DirectoryInfo dirInfo = new DirectoryInfo(strPath);
                                if (dirInfo.Exists)
                                {
                                    try
                                    {
                                        string strSqlSelect = "Select * from brand_AppList where id = " + Id;
                                        DataTable dtsiteAppList = dpsite.FillDataTable(strSqlSelect);


                                        if (dtsiteAppList.Rows.Count > 0)
                                        {
                                            StringBuilder strUpdate = new StringBuilder();
                                            strUpdate.Append("update brand_AppList set ");
                                            strUpdate.Append("[Blocker] = '" + strCheckBlock1 + "'");
                                            if (StrDefaultPosition != "")
                                            {
                                                strUpdate.Append(",[Position]='" + StrDefaultPosition + "'");
                                            }
                                            strUpdate.Append(",[SameIP_Status] = '" + strIPStatus + "',[SameIP_Value] = '" + strIPValue + "',[BlockerValue] = '" + strBlockerValue + "'");
                                            strSqlUpdate.Append(",[SameIP_Days]='" + strIPDays + "'");
                                            strUpdate.Append(" where id = " + Id);
                                            dpsite.ExecuteNonQuery(Convert.ToString(strUpdate));
                                        }
                                    }
                                    catch(Exception expersite)
                                    {
                                        lblMsg.Text = Convert.ToString(expersite.Message);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.ToLower().Contains("timeout expired"))
                            lblMsg.Text = "Timeout expired. You should try and run the task again.";
                        else
                            lblMsg.Text = "Server processing error.Please re-try.";
                    }
                }
                catch (ArgumentException errArgument)
                {
                    lblMsg.Text = Convert.ToString(errArgument.Message);
                    return;
                }
            }
            RemoveCache();
            lblMsg.Text = "Successfully Updated.";
            GetComponentDataBind();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
            return;
        }
    }

    private void RemoveCache()
    {
        StringBuilder SBDomain = new StringBuilder();
        string strSqlDomains = "SELECT DomainName FROM brand_domainname where DomainName not in ('demo.digipublishing.net','demoopen.digipublishing.net','democlosed.digipublishing.net')";
        DataTable dtdomains = dp.FillDataTable(strSqlDomains);
        if (dtdomains.Rows.Count > 0)
        {
            foreach (DataRow dr in dtdomains.Rows)
            {
                SBDomain.Append(Convert.ToString(dr["DomainName"]) + ",");
            }
        }
        StringBuilder SBIdPosition = new StringBuilder();
        string strSqlIdPosition = "SELECT a.id, b.Position FROM Brand_AppLists AS a INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                + "  where Position not in ('topbanner','pop_under','none') AND Position IS NOT NULL AND Position<>''";
        DataTable dtPositions = dp.FillDataTable(strSqlIdPosition);
        if (dtPositions.Rows.Count > 0)
        {
            foreach (DataRow dr in dtPositions.Rows)
            {
                SBIdPosition.Append(Convert.ToString(dr["id"]) + "," + Convert.ToString(dr["Position"]) + ",");
            }
        }
        if (Convert.ToString(SBIdPosition).TrimEnd(',') != "")
        {
            try
            {
                string url4 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitenottopsuperadmin&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=abc";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url4);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string url5 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitenottopsuperadmin&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=abc";
                HttpWebRequest request5 = (HttpWebRequest)WebRequest.Create(url5);
                HttpWebResponse response5 = (HttpWebResponse)request5.GetResponse();
            }
            catch { }
        }

        try
        {
            string url0 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitetopsuperadmin&Cachekey1=WidgetTop&domainurl=" + Convert.ToString(SBDomain).TrimEnd(',');
            HttpWebRequest request0 = (HttpWebRequest)WebRequest.Create(url0);
            HttpWebResponse response0 = (HttpWebResponse)request0.GetResponse();
            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitetopsuperadmin&Cachekey1=WidgetTop&domainurl=demo.digipublishing.net,demoopen.digipublishing.net,democlosed.digipublishing.net";
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch { }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            string strdroptable = " Drop Table Brand_Restore_AppLists;"
                                + " Drop Table Brand_Restore_AppListRuleSettings;";
            utils.ExecuteNonQueryTrans(strdroptable, lblMsg);

            string strqry = "SELECT * INTO Brand_Restore_AppLists FROM Brand_AppLists;"
                            + " ALTER TABLE Brand_Restore_AppLists ADD PRIMARY KEY(id);"
                            + " SELECT * INTO Brand_Restore_AppListRuleSettings FROM Brand_AppListRuleSettings;"
                            + " ALTER TABLE Brand_Restore_AppListRuleSettings ADD PRIMARY KEY(ANS_ID);";
            utils.ExecuteNonQueryTrans(strqry, lblMsg);

            string strcompID = string.Empty;
            foreach (DataGridItem dgi in dgrdComponent.Items)
            {
                strcompID += Convert.ToString(dgi.Cells[0].Text) + ",";
            }
            strcompID = strcompID.TrimEnd(',');
            string strSqlUpdate = "update Brand_AppListRuleSettings set Saved_date='" + DateTime.Now + "' where AppList_ID IN (" + strcompID + ")";
            dp.ExecuteNonQuery(strSqlUpdate);
            string strSqlUpdate2 = "update Brand_Restore_AppListRuleSettings set Saved_date='" + DateTime.Now + "'";
            dp.ExecuteNonQuery(strSqlUpdate2);
            lblMsg.Text = "Saved successfully";
            GetComponentDataBind();
            GetLastDate();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void btnrestore_Click(object sender, EventArgs e)
    {
        string strQuery = "Drop Table Brand_AppLists;"
                        + " Drop Table Brand_AppListRuleSettings;";
        if (utils.ExecuteNonQueryTrans(strQuery, lblMsg))
        {
            string strQuery1 = "SELECT * INTO Brand_AppLists FROM Brand_Restore_AppLists;"
                            + " ALTER TABLE Brand_AppLists ADD PRIMARY KEY(id);"
                            + " SELECT * INTO Brand_AppListRuleSettings FROM Brand_Restore_AppListRuleSettings;"
                            + " ALTER TABLE Brand_AppListRuleSettings ADD PRIMARY KEY(ANS_ID);";
            if (utils.ExecuteNonQueryTrans(strQuery1, lblMsg))
            {
                DataTable dt = new DataTable();
                GetComponentDataBind();
            }
        }
    }

    protected void GetLastDate()
    {
        DataTable dt = new DataTable();
        DateTime date = new DateTime();
        string strSql = " SELECT Saved_Date FROM Brand_Restore_AppListRuleSettings";
        dt = dp.FillDataTable(strSql);
        if (dt.Rows.Count > 0 && Convert.ToString(dt.Rows[0]["Saved_Date"]) != "")
        {
            date = Convert.ToDateTime(dt.Rows[0]["Saved_Date"]);
        }
        lbldate1.Text = date.ToString("dd MMMM yyyy");
        lbldate2.Text = date.ToString("dd MMM yyyy");
    }
}