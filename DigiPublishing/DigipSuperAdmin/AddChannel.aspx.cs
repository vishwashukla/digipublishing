﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_AddChannel : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, System.EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Add Channel";
        if (!IsPostBack)
        {
            FillRegion();
            FillLanguages();
        }

        lbluploadmsg.Text = "<font  face='Arial' size='2' color='#333333' font-weight='bold'>Image size is restricted to  60px width and 60px height</font>";
    }

    public void FillRegion()
    {
        DataTable dt = dp.FillDataTable("SELECT region_name,region_id FROM bbc_region");
        cboregion.DataSource = dt;
        cboregion.DataTextField = "region_name";
        cboregion.DataValueField = "region_id";
        cboregion.DataBind();
        cboregion.Items.Insert(0, new ListItem("-------Select Region-------", "0"));
    }

    public void FillLanguages()
    {
        DataTable dt = dp.FillDataTable("select LanguageID,LanguageName from Brand_Languages");
        lstLanguages.DataSource = dt;
        lstLanguages.DataTextField = "LanguageName";
        lstLanguages.DataValueField = "LanguageID";
        lstLanguages.DataBind();
    }
    private bool ChannelIsExist(string strChannel)
    {
        DataTable dt = null;
        bool blnIsExists = false;
        dt = dp.FillDataTable("select Channel_name From Channel where Channel_Name='" + strChannel + "'");
        if (dt.Rows.Count > 0)
        {
            blnIsExists = true;
        }
        return blnIsExists;
    }
    protected void btnBack_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("VideoChannelSearch.aspx");
    }
    protected void btnAddmore_Click(object sender, System.EventArgs e)
    {
        int str_status = 0;
        string languages = utils.GetSelectedListBoxItems(lstLanguages);
        
        if (rbapp.Checked == true)
            str_status = 1;

        int str_regionId = Convert.ToInt32(cboregion.SelectedItem.Value);
        int str_countryId = 0;
        if (!string.IsNullOrWhiteSpace(Request.Form["cbocountry"]))
            str_countryId = Convert.ToInt32(Request.Form["cbocountry"]);

        if (str_countryId <= 0)
            str_countryId = 0;

        string str_Email = "admin";
        string str_channelName = channelname.Text.Replace("'", "''").Trim();
        string str_channeldescription = txtChannelDesc.Text;
        string str_channelurl = channelurl.Text;
        string str_streamurl = streamurl.Text;
        string str_format = ddlmsgformat.SelectedItem.Value.ToString();
        if (ddlmsgformat.SelectedItem.Value == "-1")
        {
            str_format = "flash video";
        }

        if (!string.IsNullOrWhiteSpace(txtEmail.Text))
            str_Email = txtEmail.Text;

        if (str_channeldescription.Length > 500)
            str_channeldescription = str_channeldescription.Substring(0, 500);

        string str_fileName = "default_Channel_Logo.gif";
        if (fp1.HasFile)
        {
            try
            {
                int Id = getchannelId();
                str_fileName = fp1.FileName.Trim();
                str_fileName = str_fileName.Replace(" ", "_");
                str_fileName = str_fileName.Substring(str_fileName.LastIndexOf(".") + 1, (str_fileName.Length - str_fileName.LastIndexOf(".") - 1));
                str_fileName = "logo_" + Id + "." + str_fileName;
                fp1.SaveAs(Server.MapPath("../images_common\\") + str_fileName);
            }
            catch (Exception ex)
            {
                lbluploadmsg.Text = ex.Message.ToString();
            }
        }
        string str_uploadlogo = str_fileName;
        string SQLSTR1 = "EXEC SP_Insert_Channel_Details @region_id=" + str_regionId + ",@country_id=" + str_countryId + ",@stream_url='" + str_streamurl.Replace("'", "''") + "',@email='" + str_Email.Replace("'", "''") + "',@channel_name='" + str_channelName.Replace("'", "''") + "',@channel_desc=N'" + str_channeldescription.Replace("'", "''") + "',@channel_url='" + str_channelurl.Replace("'", "''") + "',@format='" + str_format + "',@channel_logo_path='" + str_fileName + "',@status=" + str_status + ",@entry_date='" + DateTime.Now + "',@system_ip='" + Request.ServerVariables["REMOTE_ADDR"] + "',@languages='" + languages + "'";
        try
        {
            int intchanid = dp.ExecuteNonQuery(SQLSTR1);
            if (intchanid > 0)
            {
                lblmsg.Text = "channel has been inserted";
                ResetControls();
            }
        }
        catch (Exception ex)
        {
            lblmsg.Text = "Reason is" + ex.Message;
        }
    }

    private void ResetControls()
    {
        cboregion.ClearSelection();
        channelname.Text = "";
        txtChannelDesc.Text = "";
        channelurl.Text = "";
        streamurl.Text = "";
        ddlmsgformat.SelectedIndex = 0;
        lstLanguages.ClearSelection();
        txtEmail.Text = "";
        rbnapp.Checked = true;
    }
    public int getchannelId()
    {
        int id = 0;
        string sqlstr = "select ISNULL(Max(channel_id),0) as cint from channel";
        DataTable dt= dp.FillDataTable(sqlstr);
        if (dt.Rows.Count > 0)
        {
            id = Convert.ToInt32(dt.Rows[0]["cint"])+1;
        }
        return id;
    }
}