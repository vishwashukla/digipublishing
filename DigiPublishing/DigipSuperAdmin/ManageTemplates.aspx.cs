﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ManageTemplates : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Templates";
        lblMsg.Text = "";
        if (!IsPostBack)
        {
            BindTemplate();
        }
    }

    private void BindTemplate()
    {
        dgrdTemplates.DataSource = GetTemplate();
        dgrdTemplates.DataBind();
        mvManageTemp.SetActiveView(viewTemp);
    }

    private DataTable GetTemplate()
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT *, ISNULL(available_to_agencies,0) AS available_to_agencies FROM brand_themes";
        dt = dp.FillDataTable(strSql);
        return dt;
    }
  
    protected void lnkbtnAdd_OnClick(object sender, EventArgs e)
    {       
        string strPath = Server.MapPath("~/App_Themes");
        if (Directory.Exists(strPath))
        {
            folderwatcher(strPath);
        }
        resetControl();
        mvManageTemp.SetActiveView(vAdd);
    }

    public void folderwatcher(string folderpath)
    {
        ddlValue.Items.Clear();
        System.IO.DirectoryInfo dr = new System.IO.DirectoryInfo(folderpath);
        System.IO.DirectoryInfo[] dif = dr.GetDirectories();
        //code to display all the directories
        ddlValue.Items.Insert(0, new ListItem("---Select Value---", "-1"));
        string strQuery = "SELECT  value from brand_themes";
        DataTable dtValue = dp.FillDataTable(strQuery);
        foreach (System.IO.DirectoryInfo di in dif)
        {
            if (di.Name != "admin" && di.Name != "site_admin" && di.Name != "simon_template_v8")
            {
                bool isfolderusedwithaTemplate = false;
                if (dtValue.Rows.Count > 0)
                {
                    foreach(DataRow drvalue in dtValue.Rows)
                    {
                        if(drvalue["value"].ToString().ToLower().Trim() == di.Name.ToLower().Trim())
                        {
                            isfolderusedwithaTemplate = true;
                        }
                    }
                }
                if (!isfolderusedwithaTemplate)
                {
                    ddlValue.Items.Add(new ListItem(Convert.ToString(di.Name), Convert.ToString(di.Name)));
                }
            }
        }
    }

    protected void btnAddCancel_Click(object sender, EventArgs e)
    {
        resetControl();
        mvManageTemp.SetActiveView(viewTemp);
    }

    protected void btnAddSave_Click(object sender, EventArgs e)
    {
        try
        {
          string strQuery = "INSERT INTO [brand_themes]([name],[value],[status])VALUES('"
                            + utils.formatStringForDBInsert(txtName.Text) + "','"
                            + ddlValue.SelectedItem.Text + "','1')";
            dp.ExecuteNonQuery(strQuery);
            lblMsg.Text = "Template added successfully.";
            BindTemplate();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void dgrdTemplates_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                resetControl();
                ShowItem(e.Item.Cells[0].Text);
                this.hdnID.Value = e.Item.Cells[0].Text;
            }
            if (e.CommandName == "Delete")
            {
              
                string strQuery = "DELETE brand_themes WHERE id='" + e.Item.Cells[0].Text + "'";
                dp.ExecuteNonQuery(strQuery);
                this.lblMsg.Text = "Template deleted successfully.";
                BindTemplate();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    private void ShowItem(string ID)
    {
        try
        {
            string strQuery = "SELECT * FROM brand_themes WHERE id=" + ID + "";
            DataTable dt = dp.FillDataTable(strQuery);

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                this.txtTemplateName.Text = Convert.ToString(dr["name"]);
                hdnthemes.Value = Convert.ToString(dt.Rows[0]["value"]);
                string myCssFileContents = System.IO.File.ReadAllText(Server.MapPath(@"~/App_Themes/" + hdnthemes.Value + "/base.css"));
                this.txtCSS.Text = myCssFileContents;
            }
            mvManageTemp.SetActiveView(viewEditTemp);
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void dgrdTemplates_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string themeName = "," + Convert.ToString(e.Item.Cells[3].Text).Trim() + ",";
            if (GetUsedThemes().ToLower().Contains(themeName.ToLower()))
            {
                e.Item.BackColor = Color.GreenYellow;
            }

            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            lnkDelete.Attributes.Add("onclick", "javascript: return confirm('Are you sure to delete this template?');");
        }
    }

    private string GetUsedThemes()
    {
        string strTheme = ",";
        string strSql = "SELECT domainname FROM brand_domainname";
        DataTable dt = dp.FillDataTable(strSql);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string Theme=ReadConfig.GetTheme(Convert.ToString(dr["domainname"]));
                if(!string.IsNullOrWhiteSpace(Theme))
                    strTheme += Theme + ",";
            }
        }
        return strTheme;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetControl();
        mvManageTemp.SetActiveView(viewTemp);
    }

    private void resetControl()
    {
        txtName.Text = "";
        lblMsg.Text = "";
        ddlValue.SelectedValue = "-1";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string strQuery = "UPDATE brand_themes SET name='" + this.txtTemplateName.Text + "' WHERE id=" + this.hdnID.Value + "";
            if (dp.ExecuteNonQuery(strQuery) > 0)
            {
                this.lblMsg.Text = "Template updated successfully";
                System.IO.File.WriteAllText(Server.MapPath(@"~/App_Themes/" + hdnthemes.Value + "/base.css"), this.txtCSS.Text);
            }
            BindTemplate();
        }
        catch (Exception ex)
        {
            lblMsg.Text =Convert.ToString(ex.Message);
        }
    }

    protected void btnAvailable_Click(object sender, EventArgs e)
    {
        string strChecked = string.Empty;
        string strUnChecked = string.Empty;
        try
        {
            foreach (DataGridItem dgi in dgrdTemplates.Items)
            {
                CheckBox chkTheme = (CheckBox)dgi.FindControl("chkTheme");
                if (chkTheme != null)
                {
                    if (chkTheme.Checked)
                        strChecked += dgi.Cells[0].Text + ",";
                    else
                        strUnChecked += dgi.Cells[0].Text + ",";
                }
            }
            strChecked = strChecked.TrimEnd(',');
            strUnChecked = strUnChecked.TrimEnd(',');

            if (!string.IsNullOrWhiteSpace(strChecked))
            {
                string strQuery = "UPDATE brand_themes SET available_to_agencies='True' WHERE id IN (" + strChecked + ")";
                dp.ExecuteNonQuery(strQuery);
            }
            if (!string.IsNullOrWhiteSpace(strUnChecked))
            {
                string strQuery = "UPDATE brand_themes SET available_to_agencies='False' WHERE id IN (" + strUnChecked + ")";
                dp.ExecuteNonQuery(strQuery);
            }
            lblMsg.Text = "Availability set successfully.";
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void dgrdTemplates_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        DataView dv = new DataView(GetTemplate());
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)source;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }
}