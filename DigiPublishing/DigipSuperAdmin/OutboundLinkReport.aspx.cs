﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class DigipSuperAdmin_OutboundLinkReport : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
    public static string MaxDays = "";
    public DataTable dtall = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Impression Report";
        lblMsg.Text = "";

        if (!IsPostBack)
        {
            string sqlSel = "Select ISNULL(MaxDays_To_StoreData,0) AS MaxDays_To_StoreData FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            if (dt.Rows.Count > 0)
                MaxDays = "-" + Convert.ToString(Convert.ToInt32(dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim()) - 1);
            GetCurretnAndLastPurgeDate();
            bindgrid();
            bindDomains();
        }
    }

    private void GetCurretnAndLastPurgeDate()
    { 
        lblCurrentServerDate.Text = DateTime.Now.ToUniversalTime().ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        string strquery = "SELECT * FROM LastPurge_DateTime";
        DataTable DT = dpreport.FillDataTable(strquery);
        if (DT.Rows.Count > 0)
        {
            lblLastPurgeDate.Text = Convert.ToDateTime(DT.Rows[0]["OutboundLink_Report"]).ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        }
    }

    private void bindgrid()
    {
        dtall = GetData();
        dgrdOutboundLinkReport.DataSource = dtall;
        dgrdOutboundLinkReport.DataBind();
    }

    private DataTable GetData()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "Brand_Outboundlink_Report";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DateDifference", "0");
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }
        return dt;
    }

    private void bindDomains()
    {
        DataTable dt = GetDataDomain();
        DataRow dr = dt.NewRow();
        dr["id"] = -1;
        dr["domainname"] = "All Domains";
        dt.Rows.InsertAt(dr, 0);
        drpDomain.DataSource = dt;
        drpDomain.DataTextField = "domainname";
        drpDomain.DataValueField = "id";
        drpDomain.DataBind();
    }

    private DataTable GetDataDomain()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigLoader.GetConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "select id,domainname from brand_domainname where status<>2";
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }
        return dt;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        DateTime DateSelected = DateTime.Now;//Convert.ToDateTime(txtDate.Text);
        TimeSpan span = DateSelected - DateTime.Now.Date;
        int datedifference = span.Days;
        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "Brand_Outboundlink_Report";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DateDifference", datedifference);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }

        dt.Columns.Remove("Link_ID");
        dt.Columns.Remove("Block_Status");
        dt.Columns.Remove("digip_domain");
        dt.Columns.Remove("BTitle");

        dt.Columns["BUrl"].ColumnName = "OutboundLink URL";
        dt.Columns["ipaddress"].ColumnName = "IP";
        dt.Columns["Click"].ColumnName = "Clicks	";
        dt.Columns["domainname"].ColumnName = "Domain";
        dt.Columns["ClickDateTime"].ColumnName = "Date/time";

        DataGrid dgrdExport = new DataGrid();
        dgrdExport.DataSource = dt;
        dgrdExport.DataBind();
        string fileName = DateSelected.Year + "-" + DateSelected.Month + "-" + DateSelected.Day + "-OutboundlinkReport.xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        Response.Charset = "";
        Response.ContentType = "application/excel";
        StringWriter StringWriter = new System.IO.StringWriter();
        HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);
        dgrdExport.RenderControl(HtmlTextWriter);
        Response.Write(StringWriter.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnPurge_Click(object sender, EventArgs e)
    {
        string sqlstr = "DECLARE @New_tableName varchar(50); "
                        + " SET @New_tableName='Links_Clicks'+'_'+(convert(varchar,year(GETDATE()))+'_'+convert(varchar,month(GETDATE()))+'_'+convert(varchar,day(GETDATE()))); "
                        + " EXEC('TRUNCATE TABLE '+@New_tableName)";
        string strQuery = "UPDATE LastPurge_DateTime SET OutboundLink_Report=GETUTCDATE()";
        try
        {
            dpreport.ExecuteReader(sqlstr);
            dpreport.ExecuteNonQuery(strQuery);
            lblMsg.Text = "old records has been deleted";
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
        GetCurretnAndLastPurgeDate();
        bindgrid();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }

    protected void dgrdOutboundLinkReport_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (txtdomain.Text.ToString() != "")
        {
            dtall = GetData();
            DataView dv = new DataView(dtall);
            int domainid = Convert.ToInt32(drpDomain.SelectedValue);
            if (domainid == -1)
                dv.RowFilter = "BUrl like '%" + txtdomain.Text.ToString() + "%'";
            else
                dv.RowFilter = "BUrl like '%" + txtdomain.Text.ToString() + "%' and digip_domain=" + domainid;
            dtall = dv.ToTable();
        }
        else
        {
            dtall = GetData();
            DataView dv = new DataView(dtall);
            int domainid = Convert.ToInt32(drpDomain.SelectedValue);
            if (domainid == -1)
            {
            }
            else
            {
                dv.RowFilter = "digip_domain=" + domainid;
                dtall = dv.ToTable();
            }
        }

        DataView dvsort = new DataView(dtall);
        SortField = e.SortExpression;
        dvsort.Sort = SortField;
        if (!Sortascending)
        {
            dvsort.Sort += " DESC";
        }
        dgrdOutboundLinkReport.DataSource = dvsort;
        dgrdOutboundLinkReport.DataBind();
    }

    protected void drpDomain_SelectedIndexChanged(object sender, EventArgs e)
    {
        int domainid = Convert.ToInt32(drpDomain.SelectedValue);
        txtdomain.Text = "";
        if (domainid == -1)
        {
            bindgrid();
        }
        else
        {
            dtall = GetData();
            DataView dv = new DataView(dtall);
            dv.RowFilter = "digip_domain="+ domainid; // query example = "id = 10"
            dtall = dv.ToTable();
            dgrdOutboundLinkReport.DataSource = dtall;
            dgrdOutboundLinkReport.DataBind();
        }
    }

    protected void btnfilter_Click(object sender, EventArgs e)
    {
        if (txtdomain.Text.ToString() != "")
        {
            dtall = GetData();
            DataView dv = new DataView(dtall);
            int domainid = Convert.ToInt32(drpDomain.SelectedValue);
            if(domainid == -1)
                dv.RowFilter = "BUrl like '%" + txtdomain.Text.ToString() + "%'";
            else
                dv.RowFilter = "BUrl like '%" + txtdomain.Text.ToString() + "%' and digip_domain="+domainid;
            dtall = dv.ToTable();
            dgrdOutboundLinkReport.DataSource = dtall;
            dgrdOutboundLinkReport.DataBind();
        }
        else
        {
            dtall = GetData();
            DataView dv = new DataView(dtall);
            int domainid = Convert.ToInt32(drpDomain.SelectedValue);
            if (domainid == -1)
            {
            }
            else
            {
                dv.RowFilter = "digip_domain=" + domainid;
                dtall = dv.ToTable();
            }
            dgrdOutboundLinkReport.DataSource = dtall;
            dgrdOutboundLinkReport.DataBind();
        }
    }
}