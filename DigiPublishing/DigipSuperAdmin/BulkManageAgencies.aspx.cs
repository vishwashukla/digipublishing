﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;
using System.Text;
using System.Net;
using System.Xml;
using System.Security.Cryptography;

public partial class DigipSuperAdmin_BulkManageAgencies : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Page.Title = ReadConfig.getDomainUrl() + " - Bulk Manage Agencies";
        if (!IsPostBack)
        {
            GetAgencies();
            bindFeatures();
            bindWidgets();
            bindTheme();
            bindSiteGenre(ddlSiteGenre);
            bindSiteGenre(ddlAppliedSiteGenre);
            resetControl();
        }
    }

    private void GetAgencies()
    {
        lstAgency.Items.Clear();
        string strSql = "SELECT id,domainname FROM brand_domainname where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "'";
        DataTable dt = dp.FillDataTable(strSql);
        if (dt != null)
        {
            lstAgency.DataSource = dt;
            lstAgency.DataTextField = "domainname";
            lstAgency.DataValueField = "id";
            lstAgency.DataBind();
        }
    }

    private void bindFeatures()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("col1", typeof(string));
        dt.Columns.Add("col2", typeof(Int32));

        foreach (string s in Enum.GetNames(typeof(features)))
        {
            int enumVal = (int)Enum.Parse(typeof(features), s);
            var name = s;
            if (s == features.registerlogin.ToString())
            {
                name = "Register / Login";
            }
            DataRow dr = dt.NewRow();
            dr[0] = name;
            dr[1] = enumVal;
            dt.Rows.Add(dr);
        }
        chkFeatures.DataSource = dt;
        chkFeatures.DataTextField = dt.Columns[0].ColumnName;
        chkFeatures.DataValueField = dt.Columns[1].ColumnName;
        chkFeatures.DataBind();
    }

    private void bindWidgets()
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT a.id,a.ApplicationName,b.Position FROM Brand_AppLists AS a"
                        + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                        + " WHERE b.status = 1"
                        + " ORDER BY a.ApplicationName asc";
        dt = dp.FillDataTable(strSql);
        dgrdWidgets.DataSource = dt;
        dgrdWidgets.DataBind();
    }

    private void resetControl()
    {
        lstAgency.SelectedValue = null;
        this.txtAddress.Text = "";
        drpTheme.SelectedValue = "-1";
        txtEmail.Text = "";
        ddlSource.SelectedValue = "1";
        chkFeatures.SelectedValue = null;
        foreach (DataGridItem dgi in dgrdWidgets.Items)
        {
            CheckBox chkWidgets = (CheckBox)dgi.FindControl("chkWidgets");
            DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
            if (chkWidgets != null)
                chkWidgets.Checked = false;
        }     
        txtFullName.Text = "";
        chkAddress.Checked = false;
        chkTheme.Checked = false;
        chkFeature.Checked = false;
        chkWidget.Checked = false;
        chkName.Checked = false;
        chkEmail.Checked = false;
        chkPwd.Checked = false;
        chkLogo.Checked = false;
        chkSource.Checked = false;
        chkStatus.Checked = false;
        chkWidgetlocation.Checked = false;
        chkWidgetRandomLoc.Checked = false;
        chkIsTracking.Checked = false;
        chkTrackingCode.Checked = false;
    }

    private void bindTheme()
    {
        string strSql = "select id, name, value from brand_themes where status = 1 ORDER BY name";
        DataTable dt = dp.FillDataTable(strSql);
        drpTheme.Items.Clear();
        drpTheme.ClearSelection();
        if (dt != null)
        {
            drpTheme.DataSource = dt;
            drpTheme.DataTextField = "name";
            drpTheme.DataValueField = "value";
            drpTheme.DataBind();
            drpTheme.Items.Insert(0, new ListItem("Select Theme", "-1"));
            drpTheme.SelectedValue = "-1";
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        

        foreach (ListItem li in lstAgency.Items)
        {
            if (li.Selected)
            {
                UpdateAllAgency(li.Text);
            }
        }
        bindWidgets();
        resetControl();
    }

    private void UpdateAllAgency(string strDomain)
    {
        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strPath + strDomain);

        if (dirInfo.Exists)
        {
            int countsite = 1;
            if (strDomain != ConfigLoader.GetSAMainURL())
            {
                try
                {
                    string fileName = strPath + strDomain + "\\Config.config";
                    DBHelper dp1 = new DBHelper(GetConnection(fileName), DBSType.MSSQL);
                    
                    int strSiteGenre = 0;
                    string strGenreQuery = "select * from brand_domainname where domainname = '" + strDomain + "'";
                    DataTable dtDomain = dp.FillDataTable(strGenreQuery);
                    if (dtDomain.Rows.Count > 0)
                    {
                        if (ddlAppliedSiteGenre.SelectedValue != "-1")
                        {
                            strSiteGenre = Convert.ToInt32(this.ddlAppliedSiteGenre.SelectedValue);
                        }
                        else
                            strSiteGenre = Convert.ToInt32(dtDomain.Rows[0]["domain_genre"]);
                    }

                    string updateTrackinCode = "";
                    if(chkIsTracking.Checked)
                    {
                        int AddTrackinCode = 0;
                        if (chkTrackingCode.Checked)
                        {
                            AddTrackinCode = 1;
                        }
                        updateTrackinCode = ",IsAddTrackingCode=" + AddTrackinCode;
                    }

                    string strSqlQuery = "update brand_domainname set modifydate = getdate(),Address = '" + utils.formatStringForDBInsert(this.txtAddress.Text) + "'"
                                + " ,domain_genre = " + strSiteGenre + updateTrackinCode + " where domainname = '" + strDomain + "'";
                    dp.ExecuteNonQuery(strSqlQuery);

                    //UPDATE Domain name and Genre name in report database
                    string strGenreName = "";
                    DataTable dtGenreName = dp.FillDataTable("SELECT genre_name FROM brand_sitegenre WHERE ID='" + strSiteGenre + "'");
                    if (dtGenreName.Rows.Count > 0)
                    {
                        strGenreName = Convert.ToString(dtGenreName.Rows[0]["genre_name"]);
                        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                        string sqlreportDBDomainGenre = "EXEC [dbo].[SP_Brand_Update_Domain_Genre] @Domain='" + strDomain + "',@Genre='" + strGenreName + "'";
                        dpreport.ExecuteNonQuery(sqlreportDBDomainGenre);
                    }

                    if (chkWidget.Checked)
                    {
                        string strRandom = "1";
                        int strRandomPosition = 0;

                        ArrayList arrLst = new ArrayList();
                        foreach (ListItem lstChangeLocation in ddlwidgetsLocation.Items)
                        {
                            if (lstChangeLocation.Selected)
                            {
                                arrLst.Add(lstChangeLocation.Value);
                                string strUpldatQuery = "update Brand_AppList SET MarktoDelete= 1,MarktoDeleteID=id WHERE Position='" + lstChangeLocation.Value + "'";
                                dp1.ExecuteNonQuery(strUpldatQuery);
                            }
                        }
                        StringBuilder SBIdPosition = new StringBuilder();
                        foreach (DataGridItem dgi in dgrdWidgets.Items)
                        {
                            CheckBox chkWidgets = (CheckBox)dgi.FindControl("chkWidgets");
                            string strCost1 = "";
                            if (chkWidgets != null && chkWidgets.Checked == true)
                            {
                                //Check for location to update without affecting others
                                if (this.chkWidgetlocation.Checked)
                                {
                                    //Without affecting other positions
                                    DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
                                    string strPosition = "none";

                                    if (ddlwidgets != null)
                                        strPosition = ddlwidgets.SelectedValue;

                                    string strQuery = "";
                                    if (arrLst != null && arrLst.Contains(strPosition))
                                    {
                                        string strGetDeleted = "SELECT * FROM Brand_AppList where MarktoDelete= 1 and ID='" + dgi.Cells[0].Text + "'";
                                        DataTable dtGetDeleted = dp1.FillDataTable(strGetDeleted);

                                        strQuery = "SELECT a.*,b.* FROM Brand_AppLists AS a"
                                                   + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                                   + " WHERE a.id='" + dgi.Cells[0].Text + "'";
                                        DataTable dtResult = dp.FillDataTable(strQuery);

                                        if (dtGetDeleted.Rows.Count > 0)
                                        {
                                            strCost1 = Convert.ToString(dtGetDeleted.Rows[0]["cost"]);
                                        }
                                        else
                                        {
                                            strCost1 = Convert.ToString(dtResult.Rows[0]["cost"]);
                                        }

                                        string strUpldatQuery1 = "delete from Brand_AppList WHERE MarktoDelete= 1 and id=" + dgi.Cells[0].Text + " ";
                                        dp1.ExecuteNonQuery(strUpldatQuery1);

                                        foreach (ListItem lstItem in ddl_random_widgets.Items)
                                        {
                                            if (lstItem.Selected != true)
                                                continue;
                                            if (lstItem.Value != "Select")
                                            {
                                                if (strPosition == "topbanner" && lstItem.Value == "random_topbanner")
                                                    strRandomPosition = 1;
                                                if (strPosition == "leftbottom" && lstItem.Value == "random_left")
                                                    strRandomPosition = 1;
                                                if (strPosition == "righttop" && lstItem.Value == "random_right")
                                                    strRandomPosition = 1;
                                                if (strPosition == "bottomright" && lstItem.Value == "random_bottom")
                                                    strRandomPosition = 1;
                                                if (strPosition == "pop_under" && lstItem.Value == "random_pop_under")
                                                    strRandomPosition = 1;
                                                if (strPosition == "topheader" && lstItem.Value == "random_topheader")
                                                    strRandomPosition = 1;
                                                if (strPosition == "subline" && lstItem.Value == "random_subline")
                                                    strRandomPosition = 1;
                                                // issue mid article 3707
                                                if (strPosition == "midarticle" && lstItem.Value == "random_midarticle")
                                                    strRandomPosition = 1;
                                                //end

                                                strRandom = "1";
                                                string strQueryRandomWidget1 = "update brand_Applist_Random set Value = '" + strRandomPosition + "' where Random_Position = '" + lstItem.Value + "'";
                                                dp1.ExecuteNonQuery(strQueryRandomWidget1);
                                            }
                                        }
                                        strQuery = "insert into brand_AppList ([id],[ApplicationName],[Version],[CodeBox]   "
                                                    + " ,[status],[Image],[cost],[IsRandom],[Position],[Region],[Country]"
                                                    + " ,[Browser],[Referal],[Not_Referal],[Blocker],[height],[width],[SameIP_Status],[SameIP_Value],[BlockerValue])"
                                                    + "values ('" + dtResult.Rows[0]["id"] + "','"
                                                    + dtResult.Rows[0]["ApplicationName"] + "','"
                                                    + dtResult.Rows[0]["Version"] + "','" + dtResult.Rows[0]["CodeBox"] + "',"
                                                    + dtResult.Rows[0]["status"] + ",'"
                                                    + dtResult.Rows[0]["Image"] + "'," + strCost1 + ","
                                                    + strRandom + ",'" + strPosition + "','" + dtResult.Rows[0]["Region"] + "','" + dtResult.Rows[0]["Country"] + "','"
                                                    + dtResult.Rows[0]["Browser"] + "','" + dtResult.Rows[0]["Referal"] + "','" + dtResult.Rows[0]["Not_Referal"] + "','"
                                                    + dtResult.Rows[0]["Blocker"] + "','" + dtResult.Rows[0]["height"] + "','" + dtResult.Rows[0]["width"] + "','" + dtResult.Rows[0]["SameIP_Status"] + "','" + dtResult.Rows[0]["SameIP_Value"] + "','" + dtResult.Rows[0]["BlockerValue"] + "')";
                                        dp1.ExecuteNonQuery(strQuery);
                                    }
                                }
                                else
                                {
                                    string strQuery = "";
                                    if (!GetConnection(fileName).Contains(ConfigLoader.GetSAMainURL()) && countsite == 1)
                                    {
                                        strQuery = "truncate table brand_AppList";
                                        dp1.ExecuteNonQuery(strQuery);

                                        countsite = 2;
                                    }

                                    DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
                                    string strPosition = "none";
                                    if (ddlwidgets != null)
                                        strPosition = ddlwidgets.SelectedValue;
                                    strQuery = "SELECT a.*,b.* FROM Brand_AppLists AS a"
                                               + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                               + " WHERE a.id='" + dgi.Cells[0].Text + "'";
                                    DataTable dt = dp.FillDataTable(strQuery);

                                    string strCost = "";
                                    if (Convert.ToString(dt.Rows[0]["cost"]) != "")
                                    {
                                        strCost = Convert.ToString(dt.Rows[0]["cost"]);
                                    }
                                    else
                                    {
                                        strCost = "0";
                                    }

                                    if (dt.Rows.Count > 0)
                                    {
                                        strQuery = "insert into brand_AppList ([id],[ApplicationName],[Version],[CodeBox]   "
                                        + " ,[status],[Image],[cost],[Position],[Region],[Country]"
                                        + " ,[Browser],[Referal],[Not_Referal],[Blocker],[height],[width],[SameIP_Status],[SameIP_Value],[BlockerValue])"
                                        + "values ('" + dt.Rows[0]["id"] + "','"
                                        + dt.Rows[0]["ApplicationName"] + "','"
                                        + dt.Rows[0]["Version"] + "','" + dt.Rows[0]["CodeBox"] + "',"
                                        + dt.Rows[0]["status"] + ",'"
                                        + dt.Rows[0]["Image"] + "'," + strCost + ",'"
                                        + strPosition + "','" + dt.Rows[0]["Region"] + "','" + dt.Rows[0]["Country"] + "','"
                                        + dt.Rows[0]["Browser"] + "','" + dt.Rows[0]["Referal"] + "','" + dt.Rows[0]["Not_Referal"] + "','"
                                        + dt.Rows[0]["Blocker"] + "','" + dt.Rows[0]["height"] + "','" + dt.Rows[0]["width"] + "','" + dt.Rows[0]["SameIP_Status"] + "','" + dt.Rows[0]["SameIP_Value"] + "','" + dt.Rows[0]["BlockerValue"] + "')";
                                        dp1.ExecuteNonQuery(strQuery);
                                    }
                                }
                            }
                            DropDownList ddlwidgetsCache = (DropDownList)dgi.FindControl("ddlwidgets");
                            string StrDefaultPosition = "none";
                            if (ddlwidgetsCache != null)
                                StrDefaultPosition = ddlwidgetsCache.SelectedValue;
                            if (StrDefaultPosition != "topbanner")
                            {
                                if (Convert.ToString(SBIdPosition) == "")
                                {
                                    SBIdPosition.Append(Convert.ToString(dgi.Cells[0].Text) + "," + StrDefaultPosition);
                                }
                                else
                                {
                                    SBIdPosition.Append("," + Convert.ToString(dgi.Cells[0].Text) + "," + StrDefaultPosition);
                                }
                            }
                        }
                        if (Convert.ToString(SBIdPosition) != "")
                        {
                            try
                            {
                                string url1 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitenottop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=" + strDomain;
                                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
                                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                                string url2 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitenottop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=" + strDomain;
                                HttpWebRequest request2 = (HttpWebRequest)WebRequest.Create(url2);
                                HttpWebResponse response2 = (HttpWebResponse)request1.GetResponse();
                            }
                            catch{ }
                        }

                        try
                        {
                            string url3 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitetop&Cachekey1=WidgetTop&domainurl=" + strDomain;
                            HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(url3);
                            HttpWebResponse response3 = (HttpWebResponse)request3.GetResponse();
                            string url4 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitetop&Cachekey1=WidgetTop&domainurl=" + strDomain;
                            HttpWebRequest request4 = (HttpWebRequest)WebRequest.Create(url4);
                            HttpWebResponse response4 = (HttpWebResponse)request4.GetResponse();
                        }
                        catch{ }

                        string strUpdateQuery = "delete from Brand_AppList WHERE MarktoDelete= 1 ";
                        dp1.ExecuteNonQuery(strUpdateQuery);

                        if (chkWidgetRandomLoc.Checked)
                        {
                            string strRandomR = "0";
                            int strRandomPositionR = 0;
                            foreach (ListItem lstItem in ddl_random_widgets.Items)
                            {
                                if (lstItem.Selected != true)
                                {
                                    strRandomPositionR = 0;
                                    string strSqlQuery1 = "update brand_Applist_Random set Value = '" + strRandomPositionR + "' where Random_Position = '" + lstItem.Value + "'";
                                    dp1.ExecuteNonQuery(strSqlQuery1);

                                }
                                else if (lstItem.Value != "Select")
                                {
                                    strRandomPositionR = 1;
                                    strRandomR = "1";
                                    string strSqlQuery1 = "update brand_Applist_Random set Value = '" + strRandomPositionR + "' where Random_Position = '" + lstItem.Value + "'";
                                    dp1.ExecuteNonQuery(strSqlQuery1);

                                    foreach (DataGridItem dgi in dgrdWidgets.Items)
                                    {
                                        string strQuery = "SELECT a.*,b.* FROM Brand_AppLists AS a"
                                                           + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                                           + " WHERE a.id='" + dgi.Cells[0].Text + "'";
                                        DataTable dt = dp.FillDataTable(strQuery);
                                        if (dt.Rows.Count > 0)
                                        {
                                            string strQuery1 = "update Brand_AppList set IsRandom= '" + strRandomR + "' where ID='" + dgi.Cells[0].Text + "'";
                                            dp1.ExecuteNonQuery(strQuery1);
                                        }


                                    }

                                }

                            }
                        }
                        // /Issue: 2215 
                    }
                   

                    if (chkName.Checked)
                    {
                        string strSql = "SELECT TOP 1 uid FROM Brand_User WHERE roles='admin'";
                        DataTable dt = dp1.FillDataTable(strSql);
                        if (dt.Rows.Count > 0)
                        {
                            strSql = "Update brand_user SET fullname='" + utils.formatStringForDBInsert(this.txtFullName.Text) + "' WHERE uid='" + Convert.ToString(dt.Rows[0][0]) + "'";
                            dp1.ExecuteNonQuery(strSql);
                        }
                    }

                    if (chkPwd.Checked)
                        UpdatePWD(fileName);

                    if (chkStatus.Checked)
                    {
                        string strSql = "update brand_domainname set modifydate = getdate(),status = " + ddlStatus.SelectedValue
                                    + " where domainname = '" + strDomain + "'";
                        dp.ExecuteNonQuery(strSql);
                    }
                    SetConfig(fileName, dp1, strDomain);
                    if (lblMsg.Text != "")

                        lblMsg.Text = "Settings updated successfully";
                    else
                        lblMsg.Text = "Settings updated successfully";

                }
                catch (Exception ex)
                {
                    lblMsg.Text = ex.Message;
                }
            }
            countsite = 1;
        }
    }

    private string GetConnection(string fileName)
    {
        string strConnection = "";
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(fileName);
        XmlNode xn = xmlDoc.SelectSingleNode("/appSettings/connection");
        if (xn != null)
            strConnection = xn.InnerText;
        return strConnection;
    }

    private void SetConfig(string fileName, DBHelper dp1, string strDomain)
    {
        StringBuilder strFeatures = new StringBuilder();
        string strLogoName = string.Empty;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(fileName);

        XmlNode xn = xmlDoc.SelectSingleNode("/appSettings/features");
        if (xn != null && chkFeature.Checked)
        {
            strFeatures.Append(utils.GetSelectedCheckBoxItems(chkFeatures));
            xn.InnerText = Convert.ToString(strFeatures);
        }

        xn = xmlDoc.SelectSingleNode("/appSettings/logo");
        if (xn != null && chkLogo.Checked)
        {
            HttpPostedFile strLogo = UploadLogo.PostedFile;
            strLogoName = strLogo.FileName;
            int index = strLogoName.LastIndexOf(@"\");
            strLogoName = strLogoName.Substring(index + 1);

            if (strLogoName != "")
            {
                xn.InnerText = strLogoName;
                UploadLogo.PostedFile.SaveAs(Server.MapPath("~/config/" + strDomain + "/" + strLogoName));
            }
        }
        
        xn = xmlDoc.SelectSingleNode("/appSettings/theme");
        if (xn != null && chkTheme.Checked)
        {
            xn.InnerText =drpTheme.SelectedValue;
        }

        xn = xmlDoc.SelectSingleNode("/appSettings/email");
        if (xn != null && chkEmail.Checked)
        {
            xn.InnerText = txtEmail.Text.Trim();
            string strSql = "SELECT TOP 1 uid FROM Brand_User WHERE roles='admin'";
            DataTable dt = dp1.FillDataTable(strSql);
            if (dt.Rows.Count > 0)
            {
                strSql = "Update brand_user SET email='" + utils.formatStringForDBInsert(txtEmail.Text.Trim()) + "' WHERE uid='" +Convert.ToString(dt.Rows[0][0]) + "'";
                dp1.ExecuteNonQuery(strSql);
            }
        }

        xn = xmlDoc.SelectSingleNode("/appSettings/Source");
        if (xn != null && chkSource.Checked)
        {
            xn.InnerText = ddlSource.SelectedValue;
        }
        
        xn = xmlDoc.SelectSingleNode("/appSettings/Status");
        if (xn != null && chkStatus.Checked)
        {
            xn.InnerText = ddlStatus.SelectedValue;
        }
        
        xn = xmlDoc.SelectSingleNode("/appSettings/latitude");
        if(xn!=null)
            xn.InnerText = "0.0";
        
        xn = xmlDoc.SelectSingleNode("/appSettings/longitude");
        if(xn!=null)
            xn.InnerText = "0.0";
                
        xmlDoc.Save(fileName);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetControl();
    }

    private void UpdatePWD(string fileName)
    {
        string strPwd = "";
        string strCalculatedPwd = "";
        string originalPassword = "";

        strPwd = txtCPwd.Text.Trim();
        originalPassword = strPwd + this.sault.Value.Trim();
        int saltSize = 2;
        Byte[] saltBytes = new byte[saltSize];
        Byte[] originalBytes;
        Byte[] encodedBytes;
        MD5 md5;
        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        md5 = new MD5CryptoServiceProvider();
        originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
        encodedBytes = md5.ComputeHash(originalBytes);
        //Convert encoded bytes back to a 'readable' string
        strCalculatedPwd = BitConverter.ToString(encodedBytes).Replace("-", "");

        DBHelper dp1 = new DBHelper(GetConnection(fileName), DBSType.MSSQL);
        string strSql = "SELECT TOP 1 uid FROM Brand_User WHERE roles='admin'";
        DataTable dt = dp1.FillDataTable(strSql);
        if (dt.Rows.Count > 0)
        {
            strSql = "Update brand_user SET password='" + strCalculatedPwd + "' WHERE uid='" + Convert.ToString(dt.Rows[0][0]) + "'";
            dp1.ExecuteNonQuery(strSql);
        }
    }

    private void bindSiteGenre(DropDownList ddlSiteGenre)
    {
        string strSql = "select ID as value, genre_name as name from brand_sitegenre order by name";
        DataTable dt = dp.FillDataTable(strSql);
        if (dt != null)
        {
            ddlSiteGenre.DataSource = dt;
            ddlSiteGenre.DataTextField = "name";
            ddlSiteGenre.DataValueField = "value";
            ddlSiteGenre.DataBind();
            ddlSiteGenre.Items.Insert(0, new ListItem("Select Genre", "-1"));
            ddlSiteGenre.SelectedValue = "-1";
        }
    }

    protected void ddlSiteGenre_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.ddlSiteGenre.SelectedValue != "-1")
        {
            selectAgency(this.ddlSiteGenre.SelectedValue);
        }
    }

    private void selectAgency(String strSiteGenreValue)
    {
        this.lstAgency.SelectedIndex = -1;
        string strQuery = "SELECT id FROM brand_domainname WHERE status <> 2 "
        + " AND domain_genre=" + strSiteGenreValue;
        DataTable dt = dp.FillDataTable(strQuery);

        foreach (DataRow dr in dt.Rows)
        {
            ListItem li=new ListItem();
            li = this.lstAgency.Items.FindByValue(Convert.ToString(dr["id"]));
            if (li != null)
                li.Selected = true;
        }
    }

    protected void dgrdWidgets_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        DataRowView Drv = (DataRowView)e.Item.DataItem;
        DropDownList ddlwidgets = (DropDownList)e.Item.FindControl("ddlwidgets");
        if (ddlwidgets != null)
        {
            ddlwidgets.SelectedValue = Convert.ToString(Drv["Position"]);
        }
    }
}