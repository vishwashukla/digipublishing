﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_AdNetworkReport : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
    public static string MaxDays = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - AdNetwork Report";
        lblMsg.Text = "";

        if (!IsPostBack)
        {
            string sqlSel = "Select ISNULL(MaxDays_To_StoreData,0) AS MaxDays_To_StoreData FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            if (dt.Rows.Count > 0)
            {
                txtMaxDays.Text = dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim();
                MaxDays = "-" + Convert.ToString(Convert.ToInt32(dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim()) - 1);
            }
            BindGrid();
            GetCurretnAndLastPurgeDate();
        }
    }

    private void GetCurretnAndLastPurgeDate()
    {
        lblCurrentServerDate.Text = DateTime.Now.ToUniversalTime().ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        string strquery = "SELECT * FROM LastPurge_DateTime";
        DataTable DT = dpreport.FillDataTable(strquery);
        if (DT.Rows.Count > 0)
        {
            lblLastPurgeDate.Text = Convert.ToDateTime(DT.Rows[0]["AdNetwork_Report"]).ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        }
    }

    protected void BindGrid()
    {
        try
        {
            DataTable dt = GetAdNetworkData();
            dgrdAdNetworkReport.DataSource = dt;
            dgrdAdNetworkReport.DataBind();
            DataTable dtcheck = dp.FillDataTable("select isnull(Start_Stop,0) as Start_Stop,isnull(EmailReport,0) as EmailReport from brand_Record_Data");
            if (dtcheck.Rows.Count > 0)
            {
                chkAllowAdnetworkData.Checked = Convert.ToBoolean(Convert.ToInt32(dtcheck.Rows[0]["Start_Stop"]));
                chkAllowEmailReports.Checked = Convert.ToBoolean(Convert.ToInt32(dtcheck.Rows[0]["EmailReport"]));
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    private DataTable GetAdNetworkData()
    {
        //DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
        //DataTable dtmain = new DataTable();
        //string strSQL = "EXEC [SP_Select_AdNetwork_Report]";
        //DataTable dt = dpreport.FillDataTable(strSQL);

        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "SP_Select_AdNetwork_Report";
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }

        return dt;
    }

    protected void btnSaveMaxDays_Click(object sender, EventArgs e)
    {
        string sqlUpd = " Update brand_Record_Data SET MaxDays_To_StoreData='" + txtMaxDays.Text.ToString().Trim() + "';"
                       + " Select ISNULL(MaxDays_To_StoreData,0) AS MaxDays_To_StoreData FROM brand_Record_Data";
        DataTable dt = dp.FillDataTable(sqlUpd);
        if (dt.Rows.Count > 0)
        {
            txtMaxDays.Text = dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim();
            MaxDays = "-" + Convert.ToString(Convert.ToInt32(dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim()) - 1);
        }
    }

    protected void dgrdAdNetworkReport_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
    {
        DataView dv = new DataView(GetAdNetworkData());
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdAdNetworkReport.DataSource = dv;
        dgrdAdNetworkReport.DataBind();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }

    protected void btnExport_Click(object sender, System.EventArgs e)
    {
        string style = "< style> .text { mso-number-format:\\@; } < /script> ";
        WriteXLS(style);
    }

    private void WriteXLS(string style)
    {
        //string SelectedDate = txtDate.Text;
        DateTime DateSelected = DateTime.Now;//Convert.ToDateTime(SelectedDate);
        TimeSpan span = DateSelected - DateTime.Now.Date;
        int datedifference = span.Days;
        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "Brand_Select_AdNetwork_Report_Export";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DateDifference", datedifference);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt); 
            
            
        }
        DataGrid dgrdExport = new DataGrid();
        dgrdExport.DataSource = dt;
        dgrdExport.DataBind();
        string fileName = DateSelected.Year + "-" + DateSelected.Month + "-" + DateSelected.Day + "-AdNetworkReport.xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        Response.Charset = "";
        Response.ContentType = "application/excel";
        StringWriter StringWriter = new System.IO.StringWriter();
        HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);
        dgrdExport.RenderControl(HtmlTextWriter);
        Response.Write(StringWriter.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnPurge_Click(object sender, System.EventArgs e)
    {
        string sqlstr = "EXEC SP_Truncate_LogData";
        dpreport.ExecuteNonQuery(sqlstr);
        lblMsg.Text = "Old records has been deleted.";
        GetCurretnAndLastPurgeDate();
        BindGrid();
    }

    protected void btnRecorddata_Click(object sender, EventArgs e)
    {
        string strsqlupdate = "UPDATE brand_Record_Data SET Start_Stop = case when Start_Stop=1 then 0 else 1 END";
        dp.ExecuteNonQuery(strsqlupdate);
        string sqlstr = "EXEC SP_Truncate_LogData";
        dpreport.ExecuteNonQuery(sqlstr);

        //For All sites
        try
        {
            string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=chkRecordData";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //For demo sites(if those are is separate application pool)
            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=chkRecordData";
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch{ }

        BindGrid();
        lblMsg.Text = "Successfully updated.";
    }
    protected void btnEmailReport_Click(object sender, EventArgs e)
    {
        string strsqlupdate = "UPDATE brand_Record_Data SET EmailReport = case when EmailReport=1 then 0 else 1 END";
        dp.ExecuteNonQuery(strsqlupdate);
        BindGrid();
        lblMsg.Text = "Successfully updated.";
    }
}