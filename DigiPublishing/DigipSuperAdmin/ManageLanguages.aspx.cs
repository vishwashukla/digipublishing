﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class DigipSuperAdmin_ManageLanguages : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Languages";
        lblMsg.Text = "";
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataTable dt = dp.FillDataTable("SELECT LanguageID,LanguageName from Brand_Languages order by LanguageName asc");
        dgrdLanguage.DataSource = dt;
        dgrdLanguage.DataBind();
        MvLanguages.SetActiveView(viewLanguages);
    }

    private void ReSetControls()
    {
        txtLanguage.Text = "";
        Operation = "";
    }

    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        ReSetControls();
        Operation = "Add";
        hAddEdit.InnerHtml = "Add Language";
        btnSave.Text = "ADD";
        MvLanguages.SetActiveView(viewAddEdit);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (LanguageExist())
        {
            lblMsg.Text = "Language already exists";
            MvLanguages.ActiveViewIndex = 1;
            return;
        }
   
        if (Operation == "Add")
        {
            dp.ExecuteNonQuery("Insert into Brand_Languages (LanguageName) values('" + txtLanguage.Text.ToString().Trim().Replace("'", "''") + "')");
            lblMsg.Text = "Language successfully added.";
            BindGrid();
        }
        else if (Operation == "Edit")
        {
            int ID = Convert.ToInt32(LanguageID.Value.ToString());
            dp.ExecuteNonQuery("Update Brand_Languages Set LanguageName='" + txtLanguage.Text.ToString().Trim().Replace("'", "''") + "' Where LanguageID=" + ID);
            lblMsg.Text = "Language successfully updated.";
            BindGrid();
        }   
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ReSetControls();
        BindGrid();
    }

    protected void dgrdLanguage_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        int ID = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            ReSetControls();
            Operation = "Edit";
            LanguageID.Value = Convert.ToString(e.CommandArgument);
            hAddEdit.InnerHtml = "Edit Language";
            btnSave.Text = "Update";
            GetLanguage(Convert.ToInt32(e.CommandArgument));
            MvLanguages.ActiveViewIndex = 1;
        }
        if (e.CommandName == "Delete")
        {
            dp.ExecuteNonQuery("Delete from Brand_Languages WHERE LanguageID=" + ID);
            lblMsg.Text = "Language successfully deleted.";
            BindGrid();
        }
    }

    private void GetLanguage(int ID)
    {
        DataTable dt = dp.FillDataTable("SELECT LanguageName from Brand_Languages WHERE LanguageID=" + ID);
        if (dt.Rows.Count > 0)
        {
            txtLanguage.Text = Convert.ToString(dt.Rows[0]["LanguageName"]);
        }
    }

    public string Operation
    {
        get
        {
            object o = ViewState["Operation"];
            if (o == null)
            {
                return "Add";
            }
            return (string)o;
        }
        set
        {
            ViewState["Operation"] = value;
        }
    }
    public bool LanguageExist()
    {
        string sQuery = "SELECT count(*) FROM Brand_Languages WHERE LanguageName='" + utils.formatStringForDBInsert(txtLanguage.Text.Trim()) + "'";

        DataTable dt = dp.FillDataTable(sQuery);
        if (Convert.ToString(dt.Rows[0][0]) != "0")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}