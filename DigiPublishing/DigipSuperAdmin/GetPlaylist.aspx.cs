﻿using System;
using System.Data;
using System.Text;

public partial class DigipSuperAdmin_GetPlaylist : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request["Id"])) && Convert.ToString(Request["Id"]) != "0")
        {
            StringBuilder strSB = new StringBuilder();
            string strChannelId = Request["Id"];
            strSB.Append(CreatePlayListFile(Convert.ToInt32(strChannelId)));
            Response.Clear();
            Response.ContentType = "application/xml";
            Response.Write(strSB.ToString());
        }
    }

    public string CreatePlayListFile(int chid)
    {
        string strreturnstring = "";
        DataTable dtPlaylist = new DataTable();
        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        dtPlaylist = dpSA.FillDataTable("EXEC [dbo].[SP_Brand_Select_PlayList_Data_JWPlayer_6] @ChannelID=" + chid);
        if (dtPlaylist.Rows.Count > 0)
        {
            StringBuilder strChannelContent = new StringBuilder();
            strChannelContent.Append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
            strChannelContent.Append("<rss version=\"2.0\"  xmlns:media=\"http://search.yahoo.com/mrss/\">");
            if (dtPlaylist.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPlaylist.Rows)
                {
                    strChannelContent.Append(dr[0].ToString());
                }
            }
            strChannelContent.Append("</rss>");
            Session["DTPlayList"] = strChannelContent.ToString();
            strreturnstring = strChannelContent.ToString();
        }
        return strreturnstring.ToString();
    }
}