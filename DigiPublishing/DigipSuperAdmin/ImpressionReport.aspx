﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ImpressionReport.aspx.cs" Inherits="DigipSuperAdmin_ImpressionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="Datepicker.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.js"></script>
    <script type="text/javascript">
        <%--$(function () {
            $('#<%=txtDate.ClientID%>').datepicker({
                minDate: parseInt('<%=MaxDays %>'),
                maxDate: 0,
            }).datepicker('setDate', new Date());
        });--%>
  </script>
<div style="vertical-align:top;">
    <h4>Impression Report</h4>
        <table width="100%">
        <tr>
            <td align="left">
                <asp:DropDownList ID="drpTYpe" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpTYpe_SelectedIndexChanged">
                    <asp:ListItem Text="All Impressions" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Impressions Per Site" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Impressions Per Genre" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
          </tr>
            <tr>
                <td align="left"> 
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                </td>
                <td align="right">
                    <%--<b>Date: </b><asp:TextBox ID="txtDate" runat="server"></asp:TextBox><b> (mm/dd/yyyy)</b>--%>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    <asp:Button ID="btnPurge" runat="server" Text="Purge Results" OnClick="btnPurge_Click" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="right">
                    <table>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Current server date time:
                            </td>
                            <td style="text-align: left; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblCurrentServerDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Last purge server date time:
                            </td>
                            <td style="text-align: left; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblLastPurgeDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                  <asp:DataGrid ID="dgrdImpressionReport" runat="server" AllowSorting="True"
                          AutoGenerateColumns="False" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" BorderWidth="3px"  
                          Font-Size="Smaller" BorderColor="#D8D8D8" BorderStyle="Solid" Width="100%"
                          ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnSortCommand="dgrdImpressionReport_SortCommand">
                            <Columns>
                                <asp:BoundColumn HeaderText="Widget &amp; Video Ad Networks" DataField="ApplicationName" SortExpression="ApplicationName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Impression" DataField="widgetimpression" SortExpression="widgetimpression"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Referer URL" DataField="Referer_URL" SortExpression="Referer_URL"></asp:BoundColumn>
                            </Columns>  
                  </asp:DataGrid>
                  <asp:DataGrid ID="dgrdImpressionReport1" runat="server" AllowSorting="True" Visible="false"
                          AutoGenerateColumns="False" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" BorderWidth="3px"  
                          Font-Size="Smaller" BorderColor="#D8D8D8" BorderStyle="Solid" Width="100%"
                          ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnSortCommand="dgrdImpressionReport1_SortCommand1">
                            <Columns>
                                <asp:BoundColumn HeaderText="Widget &amp; Video Ad Networks" DataField="ApplicationName" SortExpression="ApplicationName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Impression" DataField="widgetimpression" SortExpression="widgetimpression"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Digipublishing Domain" DataField="digip_domain" SortExpression="digip_domain" ></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Referer URL" DataField="Referer_URL" SortExpression="Referer_URL"></asp:BoundColumn>
                            </Columns>  
                  </asp:DataGrid>
                     <asp:DataGrid ID="dgrdImpressionReport2" runat="server" AllowSorting="True" Visible="false"
                          AutoGenerateColumns="False" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" BorderWidth="3px"  
                          Font-Size="Smaller" BorderColor="#D8D8D8" BorderStyle="Solid" Width="100%"
                          ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnSortCommand="dgrdImpressionReport2_SortCommand">
                            <Columns>
                                <asp:BoundColumn HeaderText="Genre" DataField="genre" SortExpression="Genre"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Widget &amp; Video Ad Networks" DataField="ApplicationName" SortExpression="ApplicationName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Impression" DataField="widgetimpression" SortExpression="widgetimpression" ></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Referer URL" DataField="Referer_URL" SortExpression="Referer_URL"></asp:BoundColumn>
                            </Columns>  
                  </asp:DataGrid>
                </td>
            </tr>  
        </table>
    </div>
</asp:Content>

