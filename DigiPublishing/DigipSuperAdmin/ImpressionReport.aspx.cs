﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class DigipSuperAdmin_ImpressionReport : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
    public static string MaxDays = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Impression Report";
        lblMsg.Text = "";

        if (!IsPostBack)
        {
            string sqlSel = "Select ISNULL(MaxDays_To_StoreData,0) AS MaxDays_To_StoreData FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            if (dt.Rows.Count > 0)
                MaxDays = "-" + Convert.ToString(Convert.ToInt32(dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim()) - 1);
            
            bindgrid();
            GetCurretnAndLastPurgeDate();
        }
    }

    private void GetCurretnAndLastPurgeDate()
    {
        lblCurrentServerDate.Text = DateTime.Now.ToUniversalTime().ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        string strquery = "SELECT * FROM LastPurge_DateTime";
        DataTable DT = dpreport.FillDataTable(strquery);
        if (DT.Rows.Count > 0)
        {
            lblLastPurgeDate.Text = Convert.ToDateTime(DT.Rows[0]["Impressions_Report"]).ToString("dd MMMM yyyy. 'UTC' HH:mm:ss tt");
        }
    }

    private void bindgrid()
    {
        DataTable dt = GetData();
        if (drpTYpe.SelectedValue == "-1")
        {
            dgrdImpressionReport1.Visible = false;
            dgrdImpressionReport2.Visible = false;
            dgrdImpressionReport.Visible = true;
            dgrdImpressionReport.DataSource = dt;
            dgrdImpressionReport.DataBind();
        }
        else if (drpTYpe.SelectedValue == "1")
        {
            dgrdImpressionReport.Visible = false;
            dgrdImpressionReport2.Visible = false;
            dgrdImpressionReport1.Visible = true;
            dgrdImpressionReport1.DataSource = dt;
            dgrdImpressionReport1.DataBind();
        }
        else if (drpTYpe.SelectedValue == "2")
        {
            dgrdImpressionReport.Visible = false;
            dgrdImpressionReport1.Visible = false;
            dgrdImpressionReport2.Visible = true;
            dgrdImpressionReport2.DataSource = dt;
            dgrdImpressionReport2.DataBind();
        }
    }

    private DataTable GetData()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "BRAND_Select_Widget_Impression";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Type", drpTYpe.SelectedValue);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }
        return dt;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        DateTime DateSelected = DateTime.Now;//Convert.ToDateTime(txtDate.Text);
        TimeSpan span = DateSelected - DateTime.Now.Date;
        int datedifference = span.Days;
        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(ConfigLoader.GetReportDBConnectionString()))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandTimeout = 1000;
            cmd.CommandText = "Brand_Widget_Impression_Export";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DateDifference", datedifference);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
        }

        DataGrid dgrdExport = new DataGrid();
        dgrdExport.DataSource = dt;
        dgrdExport.DataBind();
        string fileName = DateSelected.Year + "-" + DateSelected.Month + "-" + DateSelected.Day + "-ImpressionReport.xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        Response.Charset = "";
        Response.ContentType = "application/excel";
        StringWriter StringWriter = new System.IO.StringWriter();
        HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);
        dgrdExport.RenderControl(HtmlTextWriter);
        Response.Write(StringWriter.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnPurge_Click(object sender, EventArgs e)
    {
        string sqlstr = "DECLARE @New_tableName varchar(50); "
                        + " SET @New_tableName='AD_Network_Impressions'+'_'+(convert(varchar,year(GETDATE()))+'_'+convert(varchar,month(GETDATE()))+'_'+convert(varchar,day(GETDATE()))); "
                        + " EXEC('TRUNCATE TABLE '+@New_tableName)";
        string strQuery = "UPDATE LastPurge_DateTime SET Impressions_Report=GETUTCDATE()";
        try
        {
            dpreport.ExecuteReader(sqlstr);
            dpreport.ExecuteNonQuery(strQuery);
            lblMsg.Text = "old records has been deleted";
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
        GetCurretnAndLastPurgeDate();
        bindgrid();
    }

    protected void dgrdImpressionReport_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        DataTable dt = GetData();
        DataView dv = new DataView(dt);
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdImpressionReport.DataSource = dv;
        dgrdImpressionReport.DataBind();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }

    protected void dgrdImpressionReport1_SortCommand1(object source, DataGridSortCommandEventArgs e)
    {
        DataTable dt = GetData();
        DataView dv = new DataView(dt);
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdImpressionReport1.DataSource = dv;
        dgrdImpressionReport1.DataBind();
    }

    protected void dgrdImpressionReport2_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        DataTable dt = GetData();
        DataView dv = new DataView(dt);
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdImpressionReport2.DataSource = dv;
        dgrdImpressionReport2.DataBind();
    }

    protected void drpTYpe_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindgrid();
    }
}