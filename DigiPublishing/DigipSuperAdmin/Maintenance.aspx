﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="Maintenance.aspx.cs" Inherits="DigipSuperAdmin_Maintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .chkServerUpdate > input{
            vertical-align:middle;
        }
        .chkServerUpdate
        {
            min-width:200px;
        }
    </style>
    <div style="margin-top:20px;">
        <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
        <br /><br />
        <b>If checkbox is checked then sites will show "Site under maintenance" message.</b><br /><br />
        <div>
            <asp:CheckBox ID="chkServerUpdate" runat="server" Text="server update in progress" CssClass="chkServerUpdate"  />
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btnBgClass" />
        </div>
    </div>
    <br /><hr />
    <div style="margin-top:20px;">
        <b>Turn on/off events being sent to Google Analytics</b><br /><br />
        <div>
            <asp:CheckBox ID="chkVideoImpression" Width="120" runat="server" Text="VideoImpression" CssClass="chkServerUpdate"  />
            &nbsp;
            <asp:CheckBox ID="chkOutboundLink" runat="server" Text="OutboundLink" CssClass="chkServerUpdate"  />
            <br />
            <br />
            <asp:CheckBox ID="chkVideoAdStart" Width="120" runat="server" Text="VideoAdStart" CssClass="chkServerUpdate"  />
            &nbsp;
            <asp:CheckBox ID="chkVideoAdFinish" runat="server" Text="VideoAdFinish" CssClass="chkServerUpdate"  />
            <br />
            <br />
            <asp:CheckBox ID="chkBannerClick" Width="120" runat="server" Text="BannerClick" CssClass="chkServerUpdate"  />
            &nbsp;
            <asp:CheckBox ID="chkVideoAdClick" runat="server" Text="VideoAdClick" CssClass="chkServerUpdate"  />
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnGAEvents" runat="server" Text="Submit" OnClick="btnGAEvents_Click" CssClass="btnBgClass" />
        </div>
    </div>
    <br /><hr /><br />
    <h4>Check RSS Feeds</h4>
    <div>
        <asp:ListBox ID="lstDomain" SelectionMode="Multiple" Height="150" runat="server"></asp:ListBox>
        <asp:Button ID="btnSubmit_Feeds" runat="server" Text="Submit" OnClick="btnSubmit_Feeds_Click" CssClass="btnBgClass" />
    </div>
    <asp:DataGrid ID="dgrdRss" runat="server" AutoGenerateColumns="false" OnItemCommand="dgrdRss_OnItemCommand">
        <Columns>
            <asp:TemplateColumn HeaderText="Delete All">
                <HeaderTemplate>
                    <label for="chkAll">
                        Delete</label>
                    <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDelete', this.checked)" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkDelete" runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="feed_id" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="DomainName" HeaderText="Domain Name"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Title">
                <ItemTemplate>
                    <asp:TextBox ID="txtRssTitle" runat="server" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("Title"))) %>' Width="350px" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Link">
                <ItemTemplate>
                    <asp:TextBox ID="txtRssLink" runat="server" Text='<%# Eval("link") %>' Width="350px" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:ButtonColumn HeaderText="Update Feed URL" Text="Update" CommandName="Update"></asp:ButtonColumn>
            <asp:BoundColumn DataField="ResponseTime" HeaderText="Response Time"></asp:BoundColumn>
            <asp:BoundColumn DataField="ResponseText" HeaderText="Response"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:Button ID="btnDelete" Text="[Delete]" runat="server" Visible="false" OnClick="DeleteRssFeeds" OnClientClick="return confirm('Are you sure to delete selected Feed(s)?')"></asp:Button>


    <br /><hr /><br />
    <h4>Check Bookmark Links</h4>
    <div>
        <asp:ListBox ID="lstDomainBookmarks" SelectionMode="Multiple" Height="150" runat="server"></asp:ListBox>
        <asp:Button ID="btnSubmit_Bookmarks" runat="server" Text="Submit" OnClick="btnSubmit_Bookmarks_Click" CssClass="btnBgClass" />
    </div>
    <asp:DataGrid ID="dgrdBookmarks" runat="server" AutoGenerateColumns="false" OnItemCommand="dgrdBookmarks_ItemCommand">
        <Columns>
            <asp:TemplateColumn HeaderText="Delete All">
                <HeaderTemplate>
                    <label for="chkAll">Delete</label>
                    <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDelete', this.checked)" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkDelete" runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="link_id" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="DomainName" HeaderText="Domain Name"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Link">
                <ItemTemplate>
                    <asp:TextBox ID="txtBookmarkLink" runat="server" Text='<%# Eval("link") %>' Width="350px" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Description">
                <ItemTemplate>
                    <asp:TextBox ID="txtBookmarkDesc" runat="server" Text='<%# Eval("desc") %>' Width="350px" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:ButtonColumn HeaderText="Update Link URL" Text="Update" CommandName="Update"></asp:ButtonColumn>
            <asp:BoundColumn DataField="ResponseTime" HeaderText="Response Time"></asp:BoundColumn>
            <asp:BoundColumn DataField="ResponseText" HeaderText="Response"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:Button ID="btnDeleteBookmark" Text="[Delete]" runat="server" Visible="false" OnClick="btnDeleteBookmark_Click" OnClientClick="return confirm('Are you sure to delete selected Links(s)?')"></asp:Button>

<script type="text/javascript">

    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID);
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i];
            if (elm.type == 'checkbox') {
                if (re.test(elm.name))
                    elm.checked = checkVal;
            }
        }
    }
</script>

</asp:Content>