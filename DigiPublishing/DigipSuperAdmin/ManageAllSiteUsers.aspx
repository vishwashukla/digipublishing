﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageAllSiteUsers.aspx.cs" Inherits="DigipSuperAdmin_ManageAllSiteUsersTest" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v10.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v10.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../JS/Password.js"></script>
    <div>
        <h4>Manage All Users</h4>
    </div>
    <style>
        .dxgvFilterBar {
            display: none;
        }
    </style>
    <asp:Label ID="lblmessage" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="mvallusers" runat="server">
        <asp:View ID="vgrid" runat="server">
            <table style="width:100%;">
                <tr>
                    <td>
                        <div style="text-align:right;padding-right:90px;">
                            <asp:Button runat="server" ID="ToolbarExport" Text="Export To CSV" OnClick="ToolbarExport_Click" />
                        </div>
                        <br />
                        <dx:ASPxGridView ID="aspxAllSiteUsers" runat="server" AutoGenerateColumns="False" KeyFieldName="uid;DomainName" DataSourceID="SQLDS" 
                            SettingsPager-PageSize="100" ClientInstanceName="grid" Styles-AlternatingRow-BackColor="#ccebff" SettingsPager-Position="TopAndBottom" SettingsDetail-ExportMode="All">                  
                            <Columns>
                                <dx:GridViewDataColumn FieldName="uid" Caption="User ID"  Settings-AutoFilterCondition="Contains"/>
                                <dx:GridViewDataColumn FieldName="fullname" Caption="Name"  Settings-AutoFilterCondition="Contains" />
                                <dx:GridViewDataColumn FieldName="email" Caption="Email"  Settings-AutoFilterCondition="Contains" />      
                                <dx:GridViewDataColumn FieldName="roles" Caption="Role"  Settings-AutoFilterCondition="Contains"/>
                                <dx:GridViewDataColumn FieldName="DomainName" Caption="Domain Name"  Settings-AutoFilterCondition="Contains"/>                    
                                <dx:GridViewDataColumn FieldName="c_date" Caption="Date of Registration"  Settings-AutoFilterCondition="Contains" />
                                <dx:GridViewDataColumn FieldName="DEL" Caption="Date of DEL" Visible="false"  Settings-AutoFilterCondition="Contains" />
                                <dx:GridViewDataColumn Caption="Edit">
                                    <DataItemTemplate>
                                        <%#GetEditRowHtml(Container.VisibleIndex) %>
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="Delete">
                                    <DataItemTemplate>
                                        <%#GetDeleteRowHtml(Container.VisibleIndex) %>
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                            </Columns>
                             <Settings ShowFilterRow="true" ShowGroupPanel="false" ShowFooter="true" ShowHeaderFilterButton="true" ShowFilterBar="Visible" ShowTitlePanel="false" />
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="SQLDS" runat="server" ProviderName="System.Data.SqlClient">
                            <DeleteParameters>
                                <asp:Parameter Name="uid" Type="Int64" />
                                <asp:Parameter Name="DomainName" Type="String" />
                            </DeleteParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vedit" runat="server">
            <table class="table-full">
                <tr>
                    <td>Title</td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td>
                        <asp:DropDownList ID="ddlRoles" runat="server" CssClass="select" AutoPostBack="false">
                            <asp:ListItem Value="admin">admin</asp:ListItem>
                            <asp:ListItem Value="super admin">Super Admin</asp:ListItem>
                            <asp:ListItem Value="subscribe">subscribe</asp:ListItem>
                            <asp:ListItem Value="user">user</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Name*</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>                       
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true" ForeColor="Red"
                            ControlToValidate="txtname" ErrorMessage="Please enter name" ValidationGroup="MngUsrVCG">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Email*</td>
                    <td>
                        <asp:TextBox ID="txtUserEmail" runat="server"></asp:TextBox>&nbsp;        
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" SetFocusOnError="true" ForeColor="Red"
                            ControlToValidate="txtUserEmail" Display="Dynamic" ErrorMessage="invalid e-mail address! please re-enter"
                            ValidationGroup="MngUsrVCG" ValidationExpression="^[a-zA-Z]?[\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="fckeditor">
                            <FCKeditorV2:FCKeditor OnPreRender="welcome_text_PreRender" ID="welcome_text" runat="server" OnLoad="welcome_text_Load">
                            </FCKeditorV2:FCKeditor>
                        </div>
                        <div id="msg" runat="server" class="error"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset style="text-align: left; width: 510px">
                            <legend><span style="color: Black">Create password</span></legend>
                            <table class="table-full">
                                <tbody>

                                    <tr>
                                        <td>Password*
                                            <input id="sault" type="hidden" name="sault" runat="server" />
                                        </td>
                                        <td>
                                            <script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                                            <asp:TextBox ID="txtUserPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                                                runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;</td>
                                        <td>
                                            <div style="width: 100%; color: red" id="dvPwd">
                                            </div>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" SetFocusOnError="true" ForeColor="Red"
                                                ControlToValidate="txtUserPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                                                ValidationGroup="MngUsrVCG" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <span id="Words"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Confirm&nbsp;password*</td>
                                        <td>
                                            <asp:TextBox ID="txtCPwd" runat="server" TextMode="Password"></asp:TextBox>&nbsp;
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" Enabled="false" ForeColor="Red"
                                                Display="Dynamic" ErrorMessage="please enter confirm password" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" ForeColor="Red"
                                                Display="Dynamic" ErrorMessage="password & confirm password must match" ValidationGroup="MngUsrVCG" ControlToCompare="txtUserPwd"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 162px">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text="Update" CssClass="btnBgClass" ValidationGroup="MngUsrVCG"></asp:Button></td>
                                    <td>
                                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel"
                                            CssClass="btnBgClass"></asp:Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="pwd" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="HdnUID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="HdnDomain" runat="server"></asp:HiddenField>
</asp:Content>

