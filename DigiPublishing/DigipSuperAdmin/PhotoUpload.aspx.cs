﻿using System;
using System.Data;
using System.Web.UI;

public partial class DigipSuperAdmin_PhotoUpload : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, System.EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Channel Logo Upload";
        lblmsg.Text = "";
        int intChannelId = 0;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"])))
            intChannelId = Convert.ToInt32(Request.QueryString["id"]);
        string str_logoPath = Server.MapPath("..\\images_common\\");
        if (!IsPostBack)
        {
            string sqlstrchname = "EXEC [dbo].[SP_Select_ChannelName_OnId] @ChanneID=" + intChannelId;
            DataTable dtCHName = dp.FillDataTable(sqlstrchname);
            if (dtCHName.Rows.Count > 0)
            {
                lbName.Text = dtCHName.Rows[0]["Channel_Name"].ToString();
            }
        }
        if (IsPostBack)
        {
            if (fp1.HasFile)
            {
                try
                {
                    if (fp1.FileContent.Length < 10240)
                    {
                        string str_fileName = fp1.FileName;
                        str_fileName = str_fileName.Substring(str_fileName.LastIndexOf(".") + 1, (str_fileName.Length - str_fileName.LastIndexOf(".") - 1));
                        Trace.Warn("EXT" + str_fileName);
                        if (str_fileName.ToUpper() == "GIF" || str_fileName.ToUpper() == "PNG" || str_fileName.ToUpper() == "JPEG" || str_fileName.ToUpper() == "JPG")
                        {
                            str_fileName = "logo_" + intChannelId + "." + str_fileName;
                            fp1.SaveAs(str_logoPath + str_fileName);
                            string sqlstr = "EXEC [dbo].[SP_Update_Channel_LogoPath] @ChannelLogoPath='" + str_fileName + "',@ChanneID=" + intChannelId;
                            dp.ExecuteNonQuery(sqlstr);
                            Response.Write("update channel set  channel_logo_path='" + str_fileName + "' where channel_id=" + intChannelId);
                            Response.Write("<script language=\"JavaScript\">");
                            Response.Write("parent.opener.location.reload();");
                            Response.Write("self.close();</script>");
                            lblmsg.Text = "<font color=\"red\">Image is Uploaded</font>";
                        }
                        else
                        {
                            lblmsg.Text = "Invalid file format(Allow only jpeg, gif, png files )";
                        }
                    }
                    else
                    {
                        lblmsg.Text = "A file you are trying to upload exceeds the maximum size allowed of 10k.";
                    }
                }
                catch (Exception ex)
                {
                    lblmsg.Text = ex.Message.ToString();
                }
            }
        }
    }
}