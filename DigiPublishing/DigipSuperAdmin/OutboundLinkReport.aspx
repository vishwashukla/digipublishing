﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="OutboundLinkReport.aspx.cs" Inherits="DigipSuperAdmin_OutboundLinkReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="Datepicker.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.js"></script>
    <script type="text/javascript">
       <%-- $(function () {
            $('#<%=txtDate.ClientID%>').datepicker({
                minDate: parseInt('<%=MaxDays %>'),
                maxDate: 0,
            }).datepicker('setDate', new Date());
        });--%>
  </script>
<div style="vertical-align:top;">
    <h4>OutboundLink Report</h4>
        <table width="100%">
        <tr>
            <td align="left">
                <asp:DropDownList ID="drpDomain" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpDomain_SelectedIndexChanged">
                </asp:DropDownList>
                <br />
                <br />
                OutboundLink Filter <asp:TextBox ID="txtdomain" runat="server"></asp:TextBox> 
                <asp:Button ID="btnfilter" runat="server" Text="Filter" OnClick="btnfilter_Click" />
            </td>
          </tr>
            <tr>
                <td align="left"> 
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                </td>
                <td align="right">
                    <%--<b>Date: </b><asp:TextBox ID="txtDate" runat="server"></asp:TextBox><b> (mm/dd/yyyy)</b>--%>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    <asp:Button ID="btnPurge" runat="server" Text="Purge Results" OnClick="btnPurge_Click" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="right">
                    <table>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Current server date time:
                            </td>
                            <td style="text-align: left; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblCurrentServerDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Last purge server date time:
                            </td>
                            <td style="text-align: left; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblLastPurgeDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                  <asp:DataGrid ID="dgrdOutboundLinkReport" runat="server" AllowSorting="True"
                          AutoGenerateColumns="False" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" BorderWidth="3px"  
                          Font-Size="Smaller" BorderColor="#D8D8D8" BorderStyle="Solid" Width="100%"
                          ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnSortCommand="dgrdOutboundLinkReport_SortCommand">
                            <Columns>
                                <asp:BoundColumn HeaderText="OutboundLink URL" DataField="BUrl" SortExpression="BUrl"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="IP" DataField="ipaddress" SortExpression="ipaddress"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Clicks" DataField="Click" SortExpression="Click"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Domain" DataField="domainname" SortExpression="domainname"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Date/time" DataField="ClickDateTime" SortExpression="ClickDateTime"></asp:BoundColumn>
                            </Columns>  
                  </asp:DataGrid>
                </td>
            </tr>  
        </table>
    </div>
</asp:Content>

