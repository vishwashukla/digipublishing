﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="VideoChannelSearch.aspx.cs" Inherits="DigipSuperAdmin_VideoChannelSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../JS/WidgetsAndAdNetwork.js"></script>

    <div style="text-align: right;">
        <asp:LinkButton ID="lnkAddChannel" Text="Add Channel" runat="server" OnClick="lnkAddChannel_Click"></asp:LinkButton>
    </div>
    <div>
        <h2>Channel Search</h2>
    </div>
    <br />
    <table border="0">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <strong>Channel Id</strong>
            </td>
            <td>
                <asp:TextBox ID="txtChannelID" runat="server" TextMode="MultiLine" Width="563px" Height="87px" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <strong>Channel Name</strong>
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtChannelName" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <strong>exact</strong>
            </td>
            <td valign="top">
                <asp:CheckBox ID="chkallownames" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <strong>Channel Format</strong>
            </td>
            <td valign="top">
                <asp:ListBox ID="ddlmsgformat" runat="server" Height="120" SelectionMode="Multiple">
                    <asp:ListItem Text="Windows Media file" Value="Windows Media file" />
                    <asp:ListItem Text="Real Video" Value="Real Video" />
                    <asp:ListItem Text="Quicktime" Value="Quicktime" />
                    <asp:ListItem Text="Flash Video" Value="Flash Video" />
                    <asp:ListItem Text="Flash RTMP" Value="RTMP" />
                    <asp:ListItem Text="Silverlight" Value="Silverlight" />
                    <asp:ListItem Text="code" Value="code" />
                    <asp:ListItem Text="Other" Value="Other" />
                </asp:ListBox>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Regions</strong>
            </td>
            <td>
                <asp:DropDownList ID="ddlregions" runat="server" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlregions_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Countries</strong>
            </td>
            <td>
                <asp:DropDownList ID="ddlCountrys" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Languages</strong>
            </td>
            <td>
                <asp:ListBox ID="ddlLanguage" runat="server" Height="120" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Site Genres</strong>
            </td>
            <td>
                <asp:ListBox ID="ddlgenres" runat="server" Height="150" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Sites</strong>
            </td>
            <td>
                <asp:ListBox ID="ddlSites" runat="server" Height="150" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <strong>Channel Status</strong>
            </td>
            <td valign="top">
                <asp:DropDownList ID="selStatus" runat="server">
                    <asp:ListItem Value="-1">-- All --</asp:ListItem>
                    <asp:ListItem Value="1">Approved</asp:ListItem>
                    <asp:ListItem Value="0">Not Approved</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <div>
                    <asp:Button ID="btnSubmit" Text="Search" runat="server" OnClick="btnSubmit_Click" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

