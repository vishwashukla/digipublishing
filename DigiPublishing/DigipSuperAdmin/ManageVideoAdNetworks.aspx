﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageVideoAdNetworks.aspx.cs" Inherits="DigipSuperAdmin_ManageVideoAdNetworks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../js/WidgetsAndAdNetwork.js"></script>
    <script src="../JS/jquery.3.1.1.min.js"></script>
    <script src="../JS/bootstrap.min.3.3.7.js"></script>
    <script>
        function ClickTestLink(id)
        {
            $("#hdnAdnetworkId").val(id);
            $("#<%=drpTestSites.ClientID %>").val("--Select--");
        }
        $(document).ready(function () {
            $("#btnTest").click(function () {
                var id = $("#hdnAdnetworkId").val();
                var domain = $("#<%=drpTestSites.ClientID %>").val();
                if(domain == "--Select--")
                {
                    alert("Please choose domain for testing.");
                    return false;
                }
                var url = "http://" + domain + "/widgets/AdnetworkTest.aspx?adnetworkid=" + id;
                window.open(url, "_blank");
                return true;
            });
        });
    </script>
    <style>
        .TagLabel{
            word-wrap:break-word;
        }
    </style>
    <style>
        .modal-backdrop {
          position: fixed;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          z-index: 1040;
          background-color: #000000;
        }
        .modal-backdrop.fade {
          opacity: 0;
        }
        .modal-backdrop,
        .modal-backdrop.fade.in {
          opacity: 0.8;
          filter: alpha(opacity=80);
        }
        .modal {
          position: fixed;
          top: 10%;
          left: 50%;
          z-index: 1050;
          width: 560px;
          margin-left: -280px;
          background-color: #ffffff;
          border: 1px solid #999;
          border: 1px solid rgba(0, 0, 0, 0.3);
          *border: 1px solid #999;
          /* IE6-7 */

          -webkit-border-radius: 6px;
          -moz-border-radius: 6px;
          border-radius: 6px;
          -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          -webkit-background-clip: padding-box;
          -moz-background-clip: padding-box;
          background-clip: padding-box;
          outline: none;
        }
        .modal.fade {
          -webkit-transition: opacity .3s linear, top .3s ease-out;
          -moz-transition: opacity .3s linear, top .3s ease-out;
          -o-transition: opacity .3s linear, top .3s ease-out;
          transition: opacity .3s linear, top .3s ease-out;
          top: -25%;
        }
        .modal.fade.in {
          top: 10%;
        }
        .modal-header {
          padding: 9px 15px;
          border-bottom: 1px solid #eee;
        }
        .modal-header .close {
          margin-top: 2px;
        }
        .modal-header h3 {
          margin: 0;
          line-height: 30px;
        }
        .modal-body {
          position: relative;
          overflow-y: auto;
          max-height: 400px;
          padding: 15px;
        }
        .modal-form {
          margin-bottom: 0;
        }
        .modal-footer {
          padding: 14px 15px 15px;
          margin-bottom: 0;
          text-align: right;
          background-color: #f5f5f5;
          border-top: 1px solid #ddd;
          -webkit-border-radius: 0 0 6px 6px;
          -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
          -webkit-box-shadow: inset 0 1px 0 #ffffff;
          -moz-box-shadow: inset 0 1px 0 #ffffff;
          box-shadow: inset 0 1px 0 #ffffff;
          *zoom: 1;
        }
        .modal-footer:before,
        .modal-footer:after {
          display: table;
          content: "";
          line-height: 0;
        }
        .modal-footer:after {
          clear: both;
        }
        .modal-footer .btn + .btn {
          margin-left: 5px;
          margin-bottom: 0;
        }
        .modal-footer .btn-group .btn + .btn {
          margin-left: -1px;
        }
        .modal-footer .btn-block + .btn-block {
          margin-left: 0;
        }

        @media (max-width: 767px) {
          .modal {
            position: fixed;
            top: 20px;
            left: 20px;
            right: 20px;
            width: auto;
            margin: 0;
          }
          .modal.fade {
            top: -100px;
          }
          .modal.fade.in {
            top: 20px;
          }
        }
        .labeldate{
            text-align:left;
            padding-left:5px;
        }
    </style>

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog">
    
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>Choose Domain</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="hdnAdnetworkId" />
                <div>
                    <asp:DropDownList ID="drpTestSites" runat="server" Width="225px"></asp:DropDownList>
                    <br />
                    <br />
                    <br /> 
                    <input type="button" value="Test" id="btnTest" data-dismiss="modal" />
                </div>
            </div>
          </div>
      
        </div>
      </div>

    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="MvManadeAdNetworks" runat="server">
        <asp:View ID="ViewManageAdNetworks" runat="server">
            <div>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <h4>
                                Manage Ad Networks
                            </h4>
                        </td>
                        <td style="text-align:right;">
                            <asp:LinkButton ID="lnkAddNetwork" runat="server" OnClick="lnkAddNetwork_Click">Add New</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right" colspan="2">
                            <span>Last saved date : </span>
                            <asp:Label ID="lbldate1" runat="server" CssClass="labeldate" Width="120px"></asp:Label>&nbsp;&nbsp;
                            <asp:Button ID="btnsave1" runat="server" Text="Save all settings" ValidationGroup="EventVCG"
                                OnClick="btnsave_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnrestore1" runat="server" Text="Restore last saved settings" ValidationGroup="EventVCG"
                                OnClick="btnrestore_Click" OnClientClick="javascript: return confirm('Are you sure you want to restore last saved settings?');" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate1" runat="server" Text="Update" OnClick="btnUpdate_OnClick"
                                ValidationGroup="EventVCG" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 310px">
                            <asp:DataGrid ID="dgrdAdNetworks" runat="server" GridLines="none" AutoGenerateColumns="false"
                                Width="795px" BorderWidth="1" AllowSorting="true" OnItemDataBound="dgrdAdNetworks_ItemDataBound"
                                OnItemCommand="dgrdAdNetworks_ItemCommand" BorderColor="black" BorderStyle="solid" OnSortCommand="dgrdAdNetworks_SortCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="AdNetwork_ID" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Ad Network" SortExpression="AdNetwork">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAdNetworkname" runat="server" CommandName="Edit" Text='<%#Eval("AdNetwork") %>' CssClass="toplink"
                                                CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Tag">
                                        <ItemTemplate>
                                            <asp:Label ID="txtTag" Width="600px" CssClass="TagLabel" runat="server" ReadOnly="true" Text='<%#Eval("Tag") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="600px" />
                                    </asp:TemplateColumn>
                                   <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Status">
                                        <ItemTemplate>
                                       <asp:CheckBox ID="chkStatus" runat="server" Checked='<%#Convert.ToBoolean(Eval("Active_Inactive")) %>'>
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Priority" SortExpression="priority">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPrioritygrid" runat="server" Width="25px" Text='<%#Eval("priority") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rqvtxtPriority" ControlToValidate="txtPrioritygrid" ValidationGroup="EventVCG"
                                                Display="Dynamic" SetFocusOnError="false" ErrorMessage="Priority is required" runat="server"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Enabled="true" ID="rgvtxtPriority" ValidationGroup="EventVCG"
                                                runat="server" ControlToValidate="txtPrioritygrid" ErrorMessage="Only numeric values are allowed."
                                                Display="Dynamic" SetFocusOnError="true" ValidationExpression="([0-9])*"></asp:RegularExpressionValidator>
                                            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtPrioritygrid" MinimumValue="0" MaximumValue="10" 
                                                Type="Integer" ValidationGroup="EventVCG" Display="Dynamic" Text="The value must be from 0 to 10" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="Edit" CssClass="toplink"
                                                CausesValidation="false"></asp:LinkButton>&nbsp;|&nbsp;
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="return confirm('Are you sure to delete this Ad Network?');"
                                                CommandName="Delete" Text="Delete" CssClass="toplink"></asp:LinkButton>&nbsp;|&nbsp;
                                            <a data-toggle="modal" href="#" data-target="#myModal" onclick='ClickTestLink(<%#Eval("AdNetwork_ID") %>)'>Test</a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right" colspan="2">
                            <span>Last saved date : </span>
                            <asp:Label ID="lbldate2" runat="server" CssClass="labeldate" Width="120px"></asp:Label>&nbsp;&nbsp;
                            <asp:Button ID="btnsave2" runat="server" Text="Save all settings" ValidationGroup="EventVCG"
                                OnClick="btnsave_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnrestore2" runat="server" Text="Restore last saved settings" ValidationGroup="EventVCG"
                                OnClick="btnrestore_Click" OnClientClick="javascript: return confirm('Are you sure you want to restore last saved settings?');" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate2" runat="server" Text="Update" OnClick="btnUpdate_OnClick"
                                ValidationGroup="EventVCG" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 50px;">
            </div>
            <div id="HelpText" style="color: Gray;">
                If Active Ad Networks in the top "Priority" level (1) do not have any ads available,
                then system looks in next highest "Priority" level (2). Continues till an ad is
                found. If no ad is found in all "Priority" levels then, continue to play the video
                content. If "Priority" levels are the same for Active Ad Networks, then the Ad Networks
                are randomly rotated.
            </div>
        </asp:View>
        <asp:View ID="ViewAddNetworks" runat="server">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <h4>
                            <asp:Label ID="headerText" Text="Add Network" runat="server"></asp:Label>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Ad Network Name</td>
                                <td style="height: 24px" colspan="4">
                                    <asp:TextBox ID="txtAdNetworkName" runat="server" ValidationGroup="EventVCG"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFVtxtAdNetworkName" runat="server" ControlToValidate="txtAdNetworkName"
                                        ErrorMessage="Please fill ad network name." Display="dynamic" SetFocusOnError="true"
                                        ValidationGroup="EventVCG">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Ad Tag</td>
                                <td style="height: 24px" colspan="4">
                                    <asp:TextBox ID="txtAdTag" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%"></td>
                                <td style="height: 24px;color:Gray;" colspan="4">
                                    videoPageUrl or pageurl = [page_url] OR [PAGE_URL]<br />
                                    cachebuster = [timestamp] OR [TIMESTAMP] OR [random] OR [RANDOM]<br />
                                    Width = [w]<br />
                                    Height = [h]<br />
                                    IP = [ip]<br />
                                    useragent = [ua]<br />
                                    videoId = [video_id] OR [VIDEO_ID]<br />
                                    videoTitle = [video_title] OR [VIDEO_TITLE]<br />
                                    videoDesc = [video_desc] OR [VIDEO_DESC]<br />
                                    videoUrl = [video_file_url] OR [VIDEO_FILE_URL]
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Vast</td>
                                <td>
                                    <asp:CheckBox ID="chkboxVast" runat="server" />
                                </td>
                            </tr>
                             <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Priority</td>
                                <td>
                                    <asp:DropDownList ID="drpPriority1" runat="server">
                                        <asp:ListItem Value="0"></asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Only play if user initiated</td>
                                <td style="height: 24px" colspan="4">
                                    <asp:CheckBox ID="chkadclick" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Not On Homepage</td>
                                <td style="height: 24px" colspan="4">
                                    <asp:CheckBox ID="chkNotOnHomePage" runat="server" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="txtDisplay" style="height: 24px; width: 25%">
                                    Plugin
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <asp:DropDownList ID="ddlPlugin" runat="server" Width="130" AutoPostBack="false" Enabled="false">
                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        <asp:ListItem Value="OVA" Selected="True">OVA</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Targeting options</td>
                                <td style="height: 24px">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdbBrowserAll" runat="server" GroupName="rdbBrowser" Text="Target All Browsers" />
                                                <asp:RadioButton ID="rdbBrowserSel" runat="server" GroupName="rdbBrowser" Text="Target Selected Browsers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lstBrowsers" runat="server" SelectionMode="Multiple" Height="100"
                                                    Width="150"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Referal Targeting Options
                                </td>
                                <td style="height: 24px">
                                    <span style="padding-right: 18px;">Show Only If Referal From</span>
                                    <asp:TextBox ID="txtReferalfrom" runat="server"></asp:TextBox><br />
                                    <span style="padding-right: 5px;">Do Not Show If Referal From</span>
                                    <asp:TextBox ID="txtNotReferalfrom" runat="server"></asp:TextBox>
                                    <div style="color: Gray;display:inline">(Insert comma seperated values)</div>
                                    <br /><br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Operating System (OS)
                                </td>
                                <td>
                                    <asp:ListBox ID="lstOS" runat="server" SelectionMode="Multiple" Width="140px" Height="100px">
                                        <asp:ListItem Value="-1" Text="----Select OS-----"></asp:ListItem>
                                        <asp:ListItem Value="windows" Text="Windows"></asp:ListItem>
                                        <asp:ListItem Value="macintosh" Text="Macintosh"></asp:ListItem>
                                        <asp:ListItem Value="linux" Text="Linux"></asp:ListItem>
                                        <asp:ListItem Value="ios" Text="iOS"></asp:ListItem>
                                        <asp:ListItem Value="android" Text="Android"></asp:ListItem>
                                        <asp:ListItem Value="other" Text="Other"></asp:ListItem>
                                    </asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Genre
                                </td>
                                <td>
                                    <asp:ListBox ID="lstGenre" runat="server" SelectionMode="Multiple" Width="225px"
                                        Height="125px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sites
                                </td>
                                <td>
                                    <asp:ListBox ID="lstSites" runat="server" SelectionMode="Multiple" Width="225px"
                                        Height="125px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Region/Country
                                </td>
                                <td>
                                    <div id="divtree" runat="server" style="border: 1px double; width: 200px; height: 150px;
                                        overflow-y: scroll;">
                                        <asp:TreeView ID="TreeView1" runat="server" ExpandDepth="0" ShowLines="True" LineImagesFolder="~/Images_Common/TreeLineImages"
                                            ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event);">
                                        </asp:TreeView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Block After</td>
                                <td style="height: 24px" colspan="4">
                                    <table>
                                        <tr>
                                            <td rowspan="2" style="position:relative;">
                                                <asp:CheckBox ID="CheckBlock1" runat="server" AutoPostBack="false" />
                                                <span style="position:absolute;top:4px;left:20px;">Status</span>
                                            </td>
                                            <td rowspan="2">
                                                &nbsp;&nbsp;
                                            </td>
                                            <td rowspan="2">
                                                <div id="div2" runat="server" style="display: block;padding-left:30px;">
                                                    Value:
                                                    <asp:TextBox ID="TextCheckBlock1" runat="server" Width="30" Text="1"></asp:TextBox>
                                                    Click
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px; vertical-align: middle;">
                                    Block Click by same IP after
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <table>
                                        <tr>
                                            <td  style="position:relative;">
                                                <asp:CheckBox ID="chkboxSameIPBlockerOn" GroupName="blocker" TextAlign="Right" runat="server" />    
                                                <span style="position:absolute;top:4px;left:20px;">Status</span>                                      
                                            </td>
                                            <td rowspan="2">
                                                &nbsp;&nbsp;
                                            </td>
                                            <td rowspan="2">
                                                <div id="divtxtSameIPBlocker" runat="server" style="display: block;padding-left:30px;">
                                                    Value:
                                                    <asp:TextBox ID="txtSameIPBlocker" runat="server" Text="1" Width="30"></asp:TextBox>times
                                                    within <asp:TextBox ID="txtSameIPDays" runat="server" Text="1" Width="30"></asp:TextBox> Day(s)
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtDisplay" style="height: 24px">
                                </td>
                                <td style="height: 24px" colspan="4">
                                    <input type="button" value="Submit" onclick="javascript: return RuleCheck();" />
                                    <div style="display: none;">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                            ValidationGroup="EventVCG" />
                                    </div>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="height: 50px;">
            </div>
            <div id="Div1" style="color: Gray;">
                If Active Ad Networks in the top "Priority" level (1) do not have any ads available,
                then system looks in next highest "Priority" level (2). Continues till an ad is
                found. If no ad is found in all "Priority" levels then, continue to play the video
                content. If "Priority" levels are the same for Active Ad Networks, then the Ad Networks
                are randomly rotated.
            </div>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="hndAdnetworkid" runat="server" />
    <div style="display: none;">
        <asp:Button ID="btnSetStatus" runat="server" OnClick="btnSetStatus_Click" />
    </div>
    <script type="text/javascript">
        function RuleCheck() {
            document.getElementById('<%= btnSubmit.ClientID%>').click();
        }
    </script>
</asp:Content>

