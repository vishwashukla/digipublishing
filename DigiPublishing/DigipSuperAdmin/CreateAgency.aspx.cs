﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Web.Administration;

public partial class DigipSuperAdmin_CreateAgency : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Create Agency";
        if (!IsPostBack)
        {
            bindFeatures();
            bindTheme();
            bindSiteGenre();
            bindWidgets();
            resetControl();
        }
    }

    public void bindWidgets()
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT a.id,a.ApplicationName,b.Position FROM Brand_AppLists AS a"
                        + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                        + " WHERE b.status = 1"
                        + " ORDER BY a.ApplicationName asc";
        dt = dp.FillDataTable(strSql);
        dgrdWidgets.DataSource = dt;
        dgrdWidgets.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        
        string strDomain = string.Empty;
        string strTheme = string.Empty;
        string strDatabase = string.Empty;
        string strFeatures = string.Empty;
        string strLogoName = string.Empty;
        string strEmail = string.Empty;
        string strSource = string.Empty;
        string strStatus = string.Empty;
        string strLatitude = string.Empty;
        string strLongitude = string.Empty;
        strDomain = txtSubDomain.Text.Trim();
        strTheme = drpTheme.SelectedValue;
        strEmail = txtEmail.Text.Trim();
        HttpPostedFile strLogo = UploadLogo.PostedFile;
        strLogoName = strLogo.FileName;
        int index = strLogoName.LastIndexOf(@"\");
        strLogoName = strLogoName.Substring(index + 1);
        strSource = ddlSource.SelectedValue;
        strStatus = ddlStatus.SelectedValue;

        strLatitude = txtLatitude.Text.Trim();
        if (strLatitude == "")
        {
            strLatitude = "0.0";
        }
        strLongitude = txtLongitude.Text.Trim();
        if (strLatitude == "")
        {
            strLongitude = "0.0";
        }

        int AddTrackinCode = 0;
        if(chkTrackingCode.Checked)
        {
            AddTrackinCode = 1;
        }
        int Honeypotemail = 0;
        if (chkHoneypotemail.Checked)
        {
            Honeypotemail = 1;
        }

        strFeatures = utils.GetSelectedCheckBoxItems(chkFeatures);
        strFeatures.TrimEnd(',');

        //Create Agency config.config
        ConfigLoader.createDirectory(strDomain);

        string strconnection = @"Persist Security Info=False;Data Source=" + ConfigLoader.GetConnectionDataSource() + ";Initial Catalog=" + strDomain + ";User ID=" + ConfigLoader.GetConnectionUID() + ";Password=" + ConfigLoader.GetConnectionPWD() + "";
        DatabaseCreator objDBCreate = new DatabaseCreator();
        objDBCreate.CreateSQLDb(ConfigLoader.GetConnectionDataSource(), strDomain, ConfigLoader.GetConnectionUID(), ConfigLoader.GetConnectionPWD());

        objDBCreate.ExecuteSQLScript(@"" + Server.MapPath("~/SqlScript/sqlscript1.sql") + "", "Persist Security Info=False;User ID=" + ConfigLoader.GetConnectionUID() + ";pwd= " + ConfigLoader.GetConnectionPWD() + ";Initial Catalog=" + strDomain + "; Data Source=" + ConfigLoader.GetConnectionDataSource() + ";");
        objDBCreate.ExecuteSQLScript(@"" + Server.MapPath("~/SqlScript/sqlscript2.sql") + "", "Persist Security Info=False;User ID=" + ConfigLoader.GetConnectionUID() + ";pwd= " + ConfigLoader.GetConnectionPWD() + ";Initial Catalog=" + strDomain + "; Data Source=" + ConfigLoader.GetConnectionDataSource() + ";");
        objDBCreate.ExecuteSQLScript(@"" + Server.MapPath("~/SqlScript/sqlscript3.sql") + "", "Persist Security Info=False;User ID=" + ConfigLoader.GetConnectionUID() + ";pwd= " + ConfigLoader.GetConnectionPWD() + ";Initial Catalog=" + strDomain + "; Data Source=" + ConfigLoader.GetConnectionDataSource() + ";");

        if (strLogoName.ToString() != "")
        {
            UploadLogo.PostedFile.SaveAs(Server.MapPath(ReadConfig.LogoPath));
        }

        //Update Agency settings in config.config
        ReadConfig.SetConfigWithParam(HttpContext.Current.Request.PhysicalApplicationPath + "config/" + strDomain + "/Config.config",
             strconnection, strFeatures, strLogoName, strTheme, strEmail, strSource, strStatus, strLatitude, strLongitude);

        /// Password conversion 
        string strPwd = "";
        string strCalculatedPwd = "";
        string originalPassword = "";
        if (txtCPwd.Text.Trim() != "")
        {
            strPwd = txtCPwd.Text.Trim();
            originalPassword = strPwd + this.sault.Value.Trim();
            int saltSize = 2;
            Byte[] saltBytes = new byte[saltSize];
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);
            //Convert encoded bytes back to a 'readable' string
            strCalculatedPwd = BitConverter.ToString(encodedBytes).Replace("-", "");
        }

        string strSql = "insert into brand_domainname(domainname,modifydate,status,Address,Fax,domain_genre,IsAddTrackingCode,IsSendHoneypotemail) values ('" + strDomain + "',getdate()," + strStatus + ",null,'', " + this.ddlSiteGenre.SelectedValue + ","+AddTrackinCode+"," + Honeypotemail + ")";
        DBHelper dp1 = new DBHelper(strconnection, DBSType.MSSQL);

        if (dp.ExecuteNonQuery(strSql) > 0)
        {
            strSql = " INSERT INTO [" + strDomain + "].[dbo].[brand_user] "
            + "("
            + "[uname] "
            + ",[password] "
            + ",[email] "
            + ",[roles] "
            + ",[rating] "
            + ",[status] "
            + ",[fullname] "
            + ",[title] "
            + ",[c_date] "
            + ",[c_by] "
            + ",[m_by] "
            + ",[m_date] "
            + ",[header_text] "
            + ") "
            + "VALUES "
            + "('" + strEmail + "', "
            + "'" + strCalculatedPwd + "', "
            + "'" + strEmail + "', "
            + "'admin', "
            + "-1, "
            + "1, "
            + "'" + this.txtFullName.Text.Trim() + "', "
            + "'" + strDomain + "', "
            + "getdate(), "
            + "10, "
            + "1, "
            + "getdate(), "
            + "''); "

            + " INSERT INTO [" + strDomain + "].[dbo].[BRAND_BLOG_NEW] "
            + " ([blog_title] "
            + " ,[blog_status] "
            + " ,[blog_team_id] "
            + " ,[blog_subchannel_id] "
            + " ,[c_by] "
            + " ,[c_date] "
            + " ,[m_by] "
            + " ,[m_date]) "
            + " VALUES "
            + " ('" + strDomain + "' "
            + " ,3 "
            + " ,0 "
            + " ,1 "
            + " ,1 "
            + " ,getdate() "
            + " ,1 "
            + " ,getdate()); "


             + " INSERT INTO [" + strDomain + "].[dbo].[brand_settings] "
             + " ([features] "
             + " ,[featuredComp] "
             + " ,[defaultfeatureType] "
             + " ,[defaultfeature] "
             + " ,[defaultfeatureMulti],[selfRegistration],[subRegistrationStatus],[registerText],[captcha],[moderates],[RandomiseStatus]) "
             + "  VALUES "
             + " ('" + strFeatures + "' "
             + " ,'" + strFeatures + "' "
             + " ,1 "
             + " ,4 "
             + " ,'" + strFeatures + "',1,1,'Not already registered',1,'" + strFeatures + "',0); "

             //Insert Title in Application Settings
            + " INSERT INTO [" + strDomain + "].[dbo].[Brand_AppSettings] "
            + " ([name] "
            + " ,[value] "
            + " ,[description] "
            + " ,[type] "
            + " ,[IsDelete] "
            + " ,[c_by] "
            + " ,[c_date] "
            + " ,[m_by] "
            + " ,[m_date]) "
            + "  VALUES "
            + " ('title','','',1,0,1,getdate(),1,getdate());"

            //Insert USer status length in Application Settings
            + " INSERT INTO [" + strDomain + "].[dbo].[Brand_AppSettings] "
             + " ([name] "            
             + " ,[description] "
             + " ,[type] "
             + " ,[IsDelete] "
             + " ,[c_by] "
             + " ,[c_date] "
             + " ,[m_by] "
             + " ,[m_date]) "
             + "  VALUES "
             + " ('user is status length','User is status length','2',0,1,getdate(),1,getdate());";
            dp1.ExecuteNonQuery(strSql);

            //Insert values for widgets
            foreach (DataGridItem dgi in dgrdWidgets.Items)
            {
                CheckBox chkWidgets = (CheckBox)dgi.FindControl("chkWidgets");
                if (chkWidgets != null)
                {
                    if (chkWidgets.Checked)
                    {
                        DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
                        string strPosition = "none";
                        if (ddlwidgets != null)
                            strPosition = ddlwidgets.SelectedValue;
                        string strQuery = "SELECT a.*,b.* FROM Brand_AppLists AS a"
                                           + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                           + " WHERE a.id='" + dgi.Cells[0].Text + "'";
                        DataTable dt = dp.FillDataTable(strQuery);
                        if (dt.Rows.Count > 0)
                        {
                            strQuery = "insert into [" + strDomain + "].[dbo].[brand_AppList] ([id],[ApplicationName],[Version],[CodeBox]   "
                            + " ,[status],[Image],[cost],[Position]"
                            + " ,[Browser],[Referal],[Not_Referal],[Blocker],[height],[width],[SameIP_Status],[SameIP_Value],[BlockerValue],[OS])"
                            + "values ('" + dt.Rows[0]["id"] + "','"
                            + dt.Rows[0]["ApplicationName"] + "','"
                            + dt.Rows[0]["Version"] + "','" + dt.Rows[0]["CodeBox"] + "',"
                            + dt.Rows[0]["status"] + ",'"
                            + dt.Rows[0]["Image"] + "'," + dt.Rows[0]["cost"] + ",'"
                            + strPosition + "','"
                            + dt.Rows[0]["Browser"] + "','" + dt.Rows[0]["Referal"] + "','" + dt.Rows[0]["Not_Referal"] + "','"
                            + dt.Rows[0]["Blocker"] + "','" + dt.Rows[0]["height"] + "','" + dt.Rows[0]["width"] + "','" + dt.Rows[0]["SameIP_Status"] + "','" + dt.Rows[0]["SameIP_Value"] + "','" + dt.Rows[0]["BlockerValue"] + "','" + dt.Rows[0]["OS"] + "')";
                            dp1.ExecuteNonQuery(strQuery);
                        }
                    }
                }
            }
        }
        strSql = "insert into brand_Domain_N_DB_Mapping(DomainName,DBName) values ('" + strDomain + "','" + strDomain + "')";
        dp.ExecuteNonQuery(strSql);

        //Insert Domain name and Genre name in report database
        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
        string sqlreportDBDomainGenre = "EXEC [dbo].[SP_Brand_Insert_Domain_Genre] @Domain='" + strDomain + "',@Genre='" + this.ddlSiteGenre.SelectedItem + "'";
        dpreport.ExecuteNonQuery(sqlreportDBDomainGenre);
        bool issuccess = AddBinding(strDomain);
        if (issuccess)
            lblMsg.Text = "Settings saved successfully";
        else
            lblMsg.Text = "Settings saved successfully but an error occurred while adding bindings to IIS.";
        resetControl();
    }

    protected bool AddBinding(string hostName)
    {
        var flag = false;
        //if ((hostName.StartsWith("http") && hostName.Contains("www")) || hostName.StartsWith("www"))
        //{
        //    hostName = hostName.Split(new char[] { '.', ' ' }, 2, StringSplitOptions.RemoveEmptyEntries)[1];
        //}
        //else if (hostName.StartsWith("http"))
        //{
        //    if (hostName.StartsWith("https://"))
        //        hostName = hostName.Replace("https://", "").Trim();
        //    else if (hostName.StartsWith("http://"))
        //        hostName = hostName.Replace("http://", "").Trim();
        //}

        ServerManager server = new ServerManager();
        if (server.Sites != null && server.Sites.Count > 0)
        {
            try
            {
                var sitename = ConfigLoader.GetIISSiteName();
                string ip = "*";
                string port = "80";
                // binding without www
                Binding binding = server.Sites[sitename].Bindings.CreateElement("binding");
                binding.Protocol = "http";
                binding.BindingInformation = ip + ":" + port + ":" + hostName;
                server.Sites[sitename].Bindings.Add(binding);

                // binding with www
                binding = server.Sites[sitename].Bindings.CreateElement("binding");
                binding.Protocol = "http";
                binding.BindingInformation = ip + ":" + port + ":www." + hostName;
                server.Sites[sitename].Bindings.Add(binding);

                server.CommitChanges();
                flag = true;
            }
            catch
            {
                flag = false;
            }
        }
        return flag;
    }

    private void bindFeatures()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("col1", typeof(string));
        dt.Columns.Add("col2", typeof(Int32));

        foreach (string s in Enum.GetNames(typeof(features)))
        {
            int enumVal = (int)Enum.Parse(typeof(features), s);
            var name = s;
            if (s == features.registerlogin.ToString())
            {
                name = "Register / Login";
            }
            DataRow dr = dt.NewRow();
            dr[0] = name;
            dr[1] = enumVal;
            dt.Rows.Add(dr);
        }
        chkFeatures.DataSource = dt;
        chkFeatures.DataTextField = dt.Columns[0].ColumnName;
        chkFeatures.DataValueField = dt.Columns[1].ColumnName;
        chkFeatures.DataBind();
    }

    private void resetControl()
    {
        txtSubDomain.Text = "";
        drpTheme.SelectedValue = "-1";        
        txtFullName.Text = "";
        txtEmail.Text = "";
        ddlSource.SelectedValue = "1";
        txtLatitude.Text = "";
        txtLongitude.Text = "";
        chkTrackingCode.Checked = false;
        chkHoneypotemail.Checked = false;
        chkFeatures.ClearSelection();
    }

    private void bindTheme()
    {
        string strSql = "select id, name, value from brand_themes where status = 1 order by name";
        DataTable dt = dp.FillDataTable(strSql);

        if (dt != null)
        {
            drpTheme.DataSource = dt;
            drpTheme.DataTextField = "name";
            drpTheme.DataValueField = "value";
            drpTheme.DataBind();
            drpTheme.Items.Insert(0, new ListItem("Select Theme", "-1"));
            drpTheme.SelectedValue = "-1";
        }
    }

    private void bindSiteGenre()
    {
        string strSql = "select ID as value, genre_name as name from brand_sitegenre order by name";
        DataTable dt = dp.FillDataTable(strSql);
        if (dt != null)
        {
            ddlSiteGenre.DataSource = dt;
            ddlSiteGenre.DataTextField = "name";
            ddlSiteGenre.DataValueField = "value";
            ddlSiteGenre.DataBind();
            ddlSiteGenre.Items.Insert(0, new ListItem("Select Genre", "-1"));
            ddlSiteGenre.SelectedValue = "-1";
        }
    }
}