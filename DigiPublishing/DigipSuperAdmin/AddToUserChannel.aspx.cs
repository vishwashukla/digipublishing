﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_AddToUserChannel : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader .GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, System.EventArgs e)
    {
        lblmsg.Text = "";
        Page.Title = ReadConfig.getDomainUrl() + " - Add To Site ";
        if (!IsPostBack)
        {
            FillSite();
            PopulateSite();
        }
    }

    private void FillSite()
    {
        DataTable dt = dp.FillDataTable("select id,domainname from brand_domainname where status <> 2");
        lstSites.DataSource = dt;
        lstSites.DataTextField = "domainname";
        lstSites.DataValueField = "id";
        lstSites.DataBind();
     }

    private void PopulateSite()
    {
        string strQuery="SELECT SiteID = ISNULL(Stuff((SELECT ',' + convert(varchar,SiteID) AS [text()] FROM "
                        + " (SELECT SiteID FROM select_channel where channel_id=" + Convert.ToInt32(Request.QueryString["id"])
                        + " ) x For XML PATH ('')),1,1,''),'')";

        DataTable dtSelect = dp.FillDataTable(strQuery);
        if (dtSelect.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(dtSelect.Rows[0][0])))
            utils.SetSelectedListBoxItems(lstSites, Convert.ToString(dtSelect.Rows[0][0]));
    }

    protected void btnRemove_Click(object sender, System.EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"])))
        {
            int intChannelId = Convert.ToInt32(Request.QueryString["id"]);
            dp.ExecuteNonQuery("Delete from select_channel where channel_id=" + intChannelId);
            lblmsg.Text = "Channel has been succefully removed from all sites.";
            lstSites.ClearSelection();
        }
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"])))
        {
            int intChannelId = Convert.ToInt32(Request.QueryString["id"]);
            if (intChannelId > 0)
            {
                foreach (ListItem li in lstSites.Items)
                {
                    if (li.Selected)
                    {
                        dp.ExecuteNonQuery("delete from select_channel where SiteID=" + li.Value.ToString() + " and channel_id=" + intChannelId);
                        dp.ExecuteNonQuery("insert into select_channel (SiteID,channel_id) Values('" + li.Value.ToString() + "','" + intChannelId + "')");
                    }
                }
                lblmsg.Text = "Channel has been succefully added to selected sites.";
                lstSites.ClearSelection();
                PopulateSite();
            }
        }
    }
}