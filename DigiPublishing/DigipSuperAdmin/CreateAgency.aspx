﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="CreateAgency.aspx.cs" Inherits="DigipSuperAdmin_CreateAgency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="../JS/Password.js"></script>
    <div>
        <table width="100%">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblMsg" runat="server" CssClass="txtDisplay" ForeColor="Red" Width="431px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h4>
                        Create New Site</h4>
                </td>
            </tr>
            <tr>
                <td class="txtDisplay" style="height: 24px">
                    Site Name</td>
                <td style="height: 24px">
                    <asp:TextBox ID="txtSubDomain" CssClass="maintext" runat="server" MaxLength="28"
                        onkeyup="this.value=this.value.replace(/[' ']/g,'')"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSubDomain"
                        ErrorMessage="Sub Domain could not be left blank"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Themes
                </td>
                <td>
                    <asp:DropDownList ID="drpTheme" CssClass="maintext" runat="server" Width="152px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpTheme"
                        ErrorMessage="Select theme" InitialValue="-1"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Features
                </td>
                <td>
                    <br />
                    <table>
                        <tr style="vertical-align:top;">
                            <td style="width: 200px">
                                <span style="font-weight: bold">Select Features:</span>
                                <br />
                                <asp:CheckBoxList ID="chkFeatures" CssClass="maintext" runat="server">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Widgets
                </td>
                <td>
                    <asp:DataGrid ID="dgrdWidgets" runat="server" AutoGenerateColumns="false" GridLines="none"
                        ItemStyle-BackColor="white">
                        <Columns>
                            <asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkWidgets" Text='<%#Eval("ApplicationName") %>' runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlwidgets" runat="server" SelectedValue='<%#Eval("Position") %>'>
                                        <asp:ListItem Value="topheader">Top banner</asp:ListItem>
                                        <asp:ListItem Value="topbanner">Top content</asp:ListItem>
                                        <asp:ListItem Value="leftbottom">Left</asp:ListItem>
                                        <asp:ListItem Value="righttop">Right</asp:ListItem>
                                        <asp:ListItem Value="bottomright">Bottom</asp:ListItem>
                                        <asp:ListItem Value="pop_under">Pop-under</asp:ListItem>
                                        <asp:ListItem Value="subline">Subline</asp:ListItem>
                                        <asp:ListItem Value="midarticle">Mid Article</asp:ListItem>
                                        <asp:ListItem Value="none">None</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <br />
                </td>
            </tr>
           
            <tr>
                <td class="txtDisplay">
                    Full Name
                </td>
                <td>
                    <asp:TextBox ID="txtFullName" runat="server" CssClass="maintext"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    E-mail(Admin User Name)*</td>
                <td>
                    <asp:TextBox ID="txtEmail" CssClass="maintext" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Email could not be left blank"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Email formate is not correct" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td style="width: 17%;">
                    Password*
                    <input id="sault" type="hidden" name="sault" runat="server" />
                </td>
                <td><script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                    <asp:TextBox ID="txtPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                        runat="server" CssClass="maintext" Width="140" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;
                    <div style="width: 100%; color: red" id="dvPwd">
                    </div>
                    <asp:RequiredFieldValidator ID="rfvPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtPwd"
                        Display="Dynamic" ErrorMessage="please enter password"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" SetFocusOnError="true"
                        ControlToValidate="txtPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                        ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="text-align:left;">
                    <span id="Words">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:150px;height:10px;background-color:#dddddd;">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    Confirm&nbsp;password*</td>
                <td>
                    <asp:TextBox ID="txtCPwd" runat="server" CssClass="maintext" Width="140" TextMode="Password"
                        MaxLength="15"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd"
                        Display="Dynamic" ErrorMessage="please enter confirm password"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd"
                        Display="Dynamic" ErrorMessage="password & confirm password must match" ControlToCompare="txtPwd"></asp:CompareValidator></td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Logo
                </td>
                <td>
                    <asp:FileUpload ID="UploadLogo" CssClass="maintext" runat="server"></asp:FileUpload>
                </td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Site Genre</td>
                <td>
                    <asp:DropDownList ID="ddlSiteGenre" CssClass="maintext" runat="server" Width="152px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" InitialValue="-1"
                        ControlToValidate="ddlSiteGenre" ErrorMessage="Select the Site Genre"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Source</td>
                <td>
                    <asp:DropDownList ID="ddlSource" CssClass="maintext" runat="server" Width="79px">
                        <asp:ListItem Selected="true" Value="0">Closed</asp:ListItem>
                        <asp:ListItem Value="1">Open</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Status</td>
                <td>
                    <asp:DropDownList ID="ddlStatus" CssClass="maintext" runat="server" Width="79px">
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">In-Active</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Add Tracking Code</td>
                <td>
                    <asp:CheckBox ID="chkTrackingCode" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="txtDisplay">
                    Honeypot email</td>
                <td>
                    <asp:CheckBox ID="chkHoneypotemail" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="txtDisplay" style="height: 21px">
                    Latitude</td>
                <td style="height: 21px">
                    <asp:TextBox ID="txtLatitude" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revLatitude" runat="server" ControlToValidate="txtLatitude"
                        SetFocusOnError="true" Display="dynamic" ErrorMessage="please enter only numeric value"
                        ValidationExpression="\-?[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="txtDisplay" style="height: 21px">
                    Longitude</td>
                <td style="height: 21px">
                    <asp:TextBox ID="txtLongitude" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revLongitude" runat="server" ControlToValidate="txtLongitude"
                        SetFocusOnError="true" Display="dynamic" ErrorMessage="please enter only numeric value"
                        ValidationExpression="\-?[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="mainbutton" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

