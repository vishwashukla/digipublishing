﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_VideoChannelSearch : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Videos";
        lblMsg.Text = "";
        Session["txtChannelName"] = "";
        Session["txtChannelID"] = "";
        Session["selStatus"] = "";
        Session["SRegion"] = "";
        Session["SCountry"] = "";
        Session["allowNames"] = "";
        Session["Language"] = "";
        Session["selFormat"] = "";
        Session["SSites"] = "";

        if (!IsPostBack)
        {
            FillRegions();
            FillLanguage();
            FillGenres();
            FillSites();
            ddlgenres.Attributes.Add("onclick", "GetSites('" + ddlgenres.ClientID + "','" + ddlSites.ClientID + "')");
        }
    }

    private void FillRegions()
    {
        DataTable dt = dp.FillDataTable("SELECT region_name,region_id FROM bbc_region");
        ddlregions.DataSource = dt;
        ddlregions.DataTextField = "region_name";
        ddlregions.DataValueField = "region_id";
        ddlregions.DataBind();
        ddlregions.Items.Insert(0, new ListItem("-------Select Region-------", "-1"));
    }
    private void FillLanguage()
    {
        DataTable dt = dp.FillDataTable("select LanguageID,LanguageName from Brand_Languages order by LanguageName asc");
        ddlLanguage.DataSource = dt;
        ddlLanguage.DataTextField = "LanguageName";
        ddlLanguage.DataValueField = "LanguageID";
        ddlLanguage.DataBind();
    }
    private void FillGenres()
    {
        DataTable dt = dp.FillDataTable("SP_Brand_Select_GenreAll");
        ddlgenres.DataSource = dt;
        ddlgenres.DataTextField = "genre_name";
        ddlgenres.DataValueField = "ID";
        ddlgenres.DataBind();
    }

    private void FillSites()
    {
        DataTable dt = dp.FillDataTable("SP_Brand_Select_SitesAll");
        ddlSites.DataSource = dt;
        ddlSites.DataTextField = "domainname";
        ddlSites.DataValueField = "site_id";
        ddlSites.DataBind();
    }

    protected void ddlregions_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCountrys(ddlregions.SelectedItem.Value);
    }
    private void FillCountrys(string CID)
    {
        DataTable dt = dp.FillDataTable("SELECT country_name,country_id FROM country WHERE region_id=" + CID);
        ddlCountrys.DataSource = dt;
        ddlCountrys.DataTextField = "country_name";
        ddlCountrys.DataValueField = "country_id";
        ddlCountrys.DataBind();
        ddlCountrys.Items.Insert(0, new ListItem("-------Select Country------", "-1"));
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        try
        {
            string strFchannels = "";
            strFchannels = txtChannelID.Text;
            strFchannels = strFchannels.Replace(" ", "").Trim();
            strFchannels = strFchannels.Replace(",,", ",").Trim();
            strFchannels = strFchannels.Replace("'", "").Trim();
            strFchannels = strFchannels.TrimEnd(',').Trim();
            if (strFchannels.Replace(",", "") !="" && !utils.IsNumeric(strFchannels.Replace(",", "")))
            {
                lblMsg.Text = "Please eneter only numeric values separated by comma as 'Channel ID'.";
                return;
            }
            if (strFchannels != "")
                Session["txtChannelID"] = strFchannels;

            if (txtChannelName.Text.Trim() != "")
                Session["txtChannelName"] = txtChannelName.Text.Trim();

            if (chkallownames.Checked == true)
                Session["allowNames"] = "allow";

            string strFormats = utils.GetSelectedListBoxItems(ddlmsgformat);
            if (!string.IsNullOrWhiteSpace(strFormats))
            {
                strFormats = "'" + strFormats.Replace(",", "','") + "'";
                Session["selFormat"] = strFormats;
            }

            if (ddlregions.SelectedItem.Value != "-1")
                Session["SRegion"] = ddlregions.SelectedItem.Value;
            if (ddlCountrys.SelectedItem != null && ddlCountrys.SelectedItem.Value != "-1")
                Session["SCountry"] = ddlCountrys.SelectedValue.Trim();

            Session["Language"] = utils.GetSelectedListBoxItems(ddlLanguage);

            Session["SSites"] = utils.GetSelectedListBoxItems(ddlSites);

            if (selStatus.SelectedItem.Value != "-1")
                Session["selStatus"] = selStatus.SelectedValue.Trim();

            Response.Redirect("approve.aspx");
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }
    
    protected void lnkAddChannel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/DigipSuperAdmin/AddChannel.aspx");
    }
}