﻿using System;
using System.Data;
using System.Web;

public partial class DigipSuperAdmin_agencyLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
            string strLoginguid = string.Empty;
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["LoginGuid"])))
            {
                strLoginguid = Convert.ToString(Request.QueryString["LoginGuid"]);
            }
            string strQueryLogin = "select * from Brand_SuperAdmin_Login where LoginGUID='" + strLoginguid + "'";
            DataTable dtLoginGUID = dp.FillDataTable(strQueryLogin);
            if (dtLoginGUID.Rows.Count > 0)
            {
                string strSql = "SELECT TOP 1 uid,fullname,email,roles FROM Brand_User WHERE roles='admin'";
                DataTable dt = dp.FillDataTable(strSql);
                if (dt.Rows.Count > 0)
                {
                    Session["userid"] = Convert.ToString(dt.Rows[0]["uid"]);
                    Session["username"] = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["fullname"]));
                    Session["Email"] = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["email"]));
                    Session["role"] =Convert.ToString( dt.Rows[0]["roles"]);
                }
                string strDeleteGUID = "delete from Brand_SuperAdmin_Login where LoginGUID='" + strLoginguid + "'";
                dp.ExecuteNonQuery(strDeleteGUID);

                //Response.Redirect("http://" + HttpContext.Current.Request.Url.Host.ToString() + "/ondemandhome.aspx");
                Response.Redirect("http://" + HttpContext.Current.Request.Url.Host.ToString() + "/Admintask/Index.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}