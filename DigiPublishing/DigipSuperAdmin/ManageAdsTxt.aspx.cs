﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ManageAdsTxt : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage AdsTxt";
        lblMsg.Text = "";
        if (!IsPostBack)
        {
            BindAdsTxt();
        }
    }

    private void BindAdsTxt()
    {
        dgrdAdsTxt.DataSource = GetDomains();
        dgrdAdsTxt.DataBind();
        mvManageAdstxt.SetActiveView(viewAdstxt);
    }

    private DataTable GetDomains()
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT id,domainname,(Select genre_name from brand_sitegenre where ID=brand_domainname.domain_genre) as genre FROM brand_domainname where status<>2 order by domainname";
        dt = dp.FillDataTable(strSql);
        return dt;
    }

    protected void dgrdAdsTxt_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                ShowItem(e.Item.Cells[1].Text);
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    private void ShowItem(string DomainName)
    {
        try
        {
            resetControl();
            string path = Server.MapPath(@"~/adstxt/" + DomainName.ToLower() + "/ads.txt");
            if(File.Exists(path))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                string adstxtFileContents = File.ReadAllText(Server.MapPath(@"~/adstxt/" + DomainName.ToLower() + "/ads.txt"));
                this.txtAdsTxt.Text = adstxtFileContents;
            }
            else
            {
                if (!Directory.Exists(Server.MapPath(@"~/adstxt/")))
                {
                    Directory.CreateDirectory(Server.MapPath(@"~/adstxt/"));
                }
                if(!Directory.Exists(Server.MapPath(@"~/adstxt/" + DomainName.ToLower() + "/")))
                {
                    Directory.CreateDirectory(Server.MapPath(@"~/adstxt/" + DomainName.ToLower() + "/"));
                }
                if (!Directory.Exists(Server.MapPath(@"~/adstxt/www." + DomainName.ToLower() + "/")))
                {
                    Directory.CreateDirectory(Server.MapPath(@"~/adstxt/www." + DomainName.ToLower() + "/"));
                }
                File.Create(path);
            }
            this.lblDomainName.Text = DomainName;
            mvManageAdstxt.SetActiveView(viewEditAdstxt);
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetControl();
        mvManageAdstxt.SetActiveView(viewAdstxt);
    }

    private void resetControl()
    {
        lblDomainName.Text = "";
        txtAdsTxt.Text = "";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            System.IO.File.WriteAllText(Server.MapPath(@"~/adstxt/" + lblDomainName.Text.ToLower() + "/ads.txt"), this.txtAdsTxt.Text);
            System.IO.File.WriteAllText(Server.MapPath(@"~/adstxt/www." + lblDomainName.Text.ToLower() + "/ads.txt"), this.txtAdsTxt.Text);
            this.lblMsg.Text = "ads.txt file updated successfully";
            BindAdsTxt();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void dgrdAdsTxt_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        DataView dv = new DataView(GetDomains());
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)source;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }
}