﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PhotoUpload.aspx.cs" Inherits="DigipSuperAdmin_PhotoUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Channel Logo Upload</title>
</head>
<body>
    <form id="frmUpload" runat="server">
        <div style="margin-left:10px;margin-top:15px;">

            Upload logo for Channel: <asp:Label ID="lbName" runat="server" Font-Bold="true"></asp:Label>
            <br />
            <asp:Label ID="lblmsg" runat="Server" ForeColor="red"></asp:Label>
            <br />
            <asp:FileUpload ID="fp1" runat="server" Width="300" />
            <br />
            <span class="style1">Image size is restricted to 60px width and 60px height</span>
            <br /><br />
            <input type="submit" name="submit1" value="Submit" />
        </div>
    </form>
</body>
</html>
