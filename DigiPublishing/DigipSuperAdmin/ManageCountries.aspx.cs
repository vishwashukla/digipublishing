﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ManageCountries : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Countries";
        lblMsg.Text = "";
        if (!IsPostBack)
            gridBind();
    }

    public void gridBind()
    {
        dgCountries.DataSource = getCountryList();
        dgCountries.DataBind();
        mView.SetActiveView(vGrid);
    }

    private DataTable getCountryList()
    {
        DBCommand cmd = new DBCommand();
        string strQuery = " SELECT country.country_name, country.country_id , bbc_region.region_name , bbc_region.region_id "
                        + " FROM country INNER JOIN bbc_region ON country.region_id = bbc_region.region_id"
                        + " ORDER BY country_name";
        DataTable dt = new DataTable();
        try
        {
            dt = dp.FillDataTable(strQuery);
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
        return dt;
    }

    protected void dgCountries_EditCommand(object source, DataGridCommandEventArgs e)
    {      
        ResetControl();
        hdnCountryID.Value = e.Item.Cells[0].Text;
        txtCountry.Text = e.Item.Cells[1].Text;
        ListItem li = ddlRegion.Items.FindByValue(e.Item.Cells[3].Text);
        if (li != null)
            ddlRegion.SelectedValue = li.Value;

        mView.SetActiveView(vAddEdit);
    }

    public void populateRegion()
    {
        string strQuery = "SELECT region_name, region_id FROM bbc_region ORDER BY region_name";
        DataTable dtRegion = dp.FillDataTable(strQuery);
        ddlRegion.DataSource = dtRegion;
        ddlRegion.DataTextField = "region_name";
        ddlRegion.DataValueField = "region_id";
        ddlRegion.DataBind();
        ddlRegion.Items.Insert(0, new ListItem("--Select--", "-1"));
    }

    protected void dgCountries_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        string sQuery = "DELETE FROM country WHERE country_id=" + e.Item.Cells[0].Text;
        dp.ExecuteNonQuery(sQuery);
        lblMsg.Text = "Country deleted successfully.";
        gridBind();
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (countryExist())
        {
            lblMsg.Text = "Country already exists";
            mView.SetActiveView(vAddEdit);
            return;
        }
        if (!String.IsNullOrWhiteSpace(hdnCountryID.Value))
        {
            // For Edit
            string sQuery = "UPDATE country SET country_name='" + txtCountry.Text.Trim() + "', region_id=" + ddlRegion.SelectedValue + ", c_status='" + true + "', m_by=" + Convert.ToString(Session["userid"]) + ", m_date='" + DateTime.Now + "' WHERE country_id = " + hdnCountryID.Value + "";
            dp.ExecuteNonQuery(sQuery);
            lblMsg.Text = "Country successfully updated";
            gridBind();
        }
        else
        {
            // For New Entry
            string sQuery = "INSERT INTO country (country_name, region_id, c_status, c_date, c_by) VALUES ('" + txtCountry.Text.Trim() + "'," + ddlRegion.SelectedValue + ",'" + true + "', '" + DateTime.Now + "'," + Convert.ToString(Session["userid"]) + ")";
            dp.ExecuteNonQuery(sQuery);
            lblMsg.Text = "Country successfully added";
            gridBind();
        }
    }

    public bool countryExist()
    {
        string sQuery = "SELECT count(*) FROM country WHERE country_name='" + utils.formatStringForDBInsert(txtCountry.Text.Trim()) + "'"
        + " AND region_id=" + this.ddlRegion.SelectedValue + "";
        DataTable dt = dp.FillDataTable(sQuery);
        if (Convert.ToString(dt.Rows[0][0]) != "0")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        ResetControl();
        mView.SetActiveView(vAddEdit);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControl();
        mView.SetActiveView(vGrid);
    }

    public void ResetControl()
    {
        populateRegion();
        hdnCountryID.Value = "";
        ddlRegion.SelectedValue = "-1";
        txtCountry.Text = "";
    }
}