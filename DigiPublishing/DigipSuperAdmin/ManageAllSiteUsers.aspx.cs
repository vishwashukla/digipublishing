﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using DevExpress.Web.ASPxGridView;

public partial class DigipSuperAdmin_ManageAllSiteUsersTest : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        lblmessage.Text = "";
        Page.Title = ReadConfig.getDomainUrl() + " - Manage All Site Users";
        if (Request.QueryString["EditAllSiteUsersId"] == null && Request.QueryString["EditAllSiteUsersDomain"] == null)
        {
            BindGrid();
        }
        else
        {
            if (!IsPostBack)
            {
                HdnUID.Value = Convert.ToString(Request.QueryString["EditAllSiteUsersId"]);
                HdnDomain.Value = Convert.ToString(Request.QueryString["EditAllSiteUsersDomain"]);
                ResetControls();
                GetUserData(Convert.ToInt32(HdnUID.Value), HdnDomain.Value);
            }
        }
    }

    public void BindGrid()
    {
        string strSql = "EXEC [BRAND_SP_Select_All_Users] @pageNum=" + 1 + ",@itemperpage=" + 100000000;
        SQLDS.SelectCommand = strSql;
        SQLDS.ConnectionString = dp._connectionString;

        string DeleteQuery = "BRAND_SP_Delete_User";
        SQLDS.DeleteCommandType = SqlDataSourceCommandType.StoredProcedure;
        SQLDS.DeleteCommand = DeleteQuery;

        aspxAllSiteUsers.Settings.ShowFilterRowMenu = true;
        mvallusers.SetActiveView(vgrid);
    }


    protected string GetEditRowHtml(int visibleIndex)
    {
        string id = Convert.ToString(aspxAllSiteUsers.GetRowValues(visibleIndex, "uid"));
        string domain = Convert.ToString(aspxAllSiteUsers.GetRowValues(visibleIndex, "DomainName"));
        return string.Format("<a id='a_" + id + domain + "' href = '/DigipSuperAdmin/ManageAllSiteUsers.aspx?EditAllSiteUsersId=" + id + "&EditAllSiteUsersDomain=" + domain + "'>Edit</a> ");
    }

    protected string GetDeleteRowHtml(int visibleIndex)
    {
        string id = Convert.ToString(aspxAllSiteUsers.GetRowValues(visibleIndex, "uid"));
        string domain = Convert.ToString(aspxAllSiteUsers.GetRowValues(visibleIndex, "DomainName"));
        bool del = Convert.ToBoolean(aspxAllSiteUsers.GetRowValues(visibleIndex, "DEL"));
        if (del)
            return string.Format("<a id='a_" + id + domain + "' href =\"javascript:aspxGVDeleteRow('" + aspxAllSiteUsers.ClientID + "'," + visibleIndex + ");\">Delete</a> ");
        else
            return "";
    }

    protected void dgrdAllUsers_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            HdnUID.Value = Convert.ToString(e.Item.Cells[0].Text);
            HdnDomain.Value = e.Item.Cells[4].Text;
            ResetControls();
            GetUserData(Convert.ToInt32(e.Item.Cells[0].Text), e.Item.Cells[4].Text);
        }
        else if (e.CommandName == "Delete")
        {
            DBHelper dpsite = ReadConfig.getDomainDBHelper(e.Item.Cells[4].Text);
            string strSqlDelete = "Delete from brand_user where uid=" + Convert.ToInt32(e.Item.Cells[0].Text);
            dpsite.ExecuteNonQuery(strSqlDelete);
            BindGrid();
        }
    }

    private void GetUserData(int UID, string DomainName)
    {
        try
        {
            DBHelper dpsite = ReadConfig.getDomainDBHelper(DomainName);
            string SQLUser_Data = "select * from brand_user where uid=" + UID;
            DataTable dt = dpsite.FillDataTable(SQLUser_Data);

            if (dt.Rows.Count > 0)
            {
                txtName.Text = Convert.ToString(dt.Rows[0]["fullname"]);
                txtTitle.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["title"]));
                txtUserEmail.Text = Convert.ToString(dt.Rows[0]["email"]);
                ddlRoles.SelectedValue = "user";

                if (Convert.ToString(dt.Rows[0]["roles"]) != "")
                {
                    ListItem roles = ddlRoles.Items.FindByValue(Convert.ToString(dt.Rows[0]["roles"]));
                    if (roles != null)
                        ddlRoles.SelectedValue = Convert.ToString(dt.Rows[0]["roles"]);
                    if(Convert.ToString(dt.Rows[0]["roles"]).Trim().ToLower() == "admin" || Convert.ToString(dt.Rows[0]["roles"]).Trim().ToLower() == "super admin")
                    {
                        ddlRoles.Enabled = false;
                    }
                }

                txtUserPwd.Text = Convert.ToString(dt.Rows[0]["password"]);
                welcome_text.Value = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["header_text"]));
                mvallusers.SetActiveView(vedit);
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    public void SetFCKPath()
    {
        string strPath = Request.ApplicationPath;
        if (strPath == "/")
            welcome_text.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
        else
            welcome_text.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
    }

    protected void welcome_text_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedField(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this,
                                                    welcome_text.GetType(),
                                                    "FCKUpdater",
                                                    script,
                                                    true);
            ScriptManager.RegisterOnSubmitStatement(this,
                                                    welcome_text.GetType(),
                                                    "WebContentManagerEditorScript_" + this.welcome_text.ClientID,
                                                    "FCKUpdateLinkedField('" + this.welcome_text.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(welcome_text.GetType(),
                                                        "FCKUpdater",
                                                        script,
                                                        true);
            this.Page.ClientScript.RegisterOnSubmitStatement(welcome_text.GetType(),
                                                             "WebContentManagerEditorScript_" + this.welcome_text.ClientID,
                                                             "FCKUpdateLinkedField('" + this.welcome_text.ClientID + "');");
        }
    }

    private void ResetControls()
    {
        txtName.Text = "";
        txtUserEmail.Text = "";
        txtUserPwd.Text = "";
        ddlRoles.ClearSelection();
        welcome_text.Value = "";
        SetFCKPath();
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrWhiteSpace(HdnDomain.Value.Trim()))
            {
                DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(HdnDomain.Value.Trim()));
                
                string strQuery = "Select count(*) from brand_user where status=1 AND email='" + txtUserEmail.Text.Trim() + "'  and uid<>" + HdnUID.Value;
                DataTable dtduplicacy = dpsite.FillDataTable(strQuery);
                if (dtduplicacy.Rows.Count > 0 && Convert.ToInt32(dtduplicacy.Rows[0][0])!=0)
                {
                    lblmessage.Text = "Email address already exists.";
                    return;
                }

                string strSqlupdate = "UPDATE Brand_user SET fullname='" + utils.formatStringForDBInsert(txtName.Text.Trim())
                          + "',roles='" + Convert.ToString(ddlRoles.SelectedValue) + "',email='" + txtUserEmail.Text.Trim()
                          + "',title='" + utils.formatStringForDBInsert(txtTitle.Text.Trim())
                          + "',header_text='" + utils.formatStringForDBInsert(welcome_text.Value) + "' ";

                if (txtCPwd.Text.Trim() != "")
                {
                    /// Password conversion 
                    int saltSize = 2;
                    Byte[] saltBytes = new byte[saltSize];
                    Byte[] originalBytes;
                    Byte[] encodedBytes;
                    MD5 md5;
                    //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
                    md5 = new MD5CryptoServiceProvider();
                    originalBytes = ASCIIEncoding.Default.GetBytes(txtCPwd.Text.Trim() + this.sault.Value.Trim());
                    encodedBytes = md5.ComputeHash(originalBytes);
                    //Convert encoded bytes back to a 'readable' string
                    pwd.Value = BitConverter.ToString(encodedBytes).Replace("-", "");

                    strSqlupdate += ",password='" + pwd.Value + "'";
                }

                strSqlupdate += " WHERE uid=" + HdnUID.Value;
                dpsite.ExecuteNonQuery(strSqlupdate);
                BindGrid();
                lblmessage.Text = "Updated successfully.";
                Response.Redirect("~/DigipSuperAdmin/ManageAllSiteUsers.aspx");
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = Convert.ToString(ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/DigipSuperAdmin/ManageAllSiteUsers.aspx");
    }

    protected void welcome_text_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.welcome_text.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, welcome_text.GetType(), "FCKSource", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, welcome_text.GetType(), "WebContentManagerEditorSourceScript_" + this.welcome_text.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(welcome_text.GetType(), "FCKSource", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(welcome_text.GetType(), "WebContentManagerEditorSourceScript_" + this.welcome_text.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
    }
    protected void ToolbarExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetDataView(aspxAllSiteUsers);
        string csvtext = DataTableToCSV(dt, ',');

        var response = HttpContext.Current.Response;
        response.ClearContent();
        response.Clear();
        response.ClearHeaders();
        byte[] bytes = Encoding.ASCII.GetBytes(csvtext);
        using (var stream = new MemoryStream(bytes))
        {
            response.AddHeader("Content-Disposition", "attachment; filename=AllSiteUsers_"+DateTime.Now.ToString("MMddyyyyhhmmss")+".csv");
            response.AddHeader("Content-Length", stream.Length.ToString());
            response.ContentType = "text/plain";
            stream.WriteTo(response.OutputStream);
            response.End();
        }
    }

    public string DataTableToCSV(DataTable datatable, char seperator)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < datatable.Columns.Count; i++)
        {
            sb.Append(datatable.Columns[i]);
            if (i < datatable.Columns.Count - 1)
                sb.Append(seperator);
        }
        sb.AppendLine();
        foreach (DataRow dr in datatable.Rows)
        {
            for (int i = 0; i < datatable.Columns.Count; i++)
            {
                sb.Append(dr[i].ToString());

                if (i < datatable.Columns.Count - 1)
                    sb.Append(seperator);
            }
            sb.AppendLine();
        }
        return sb.ToString();
    }

    private DataTable GetDataView(ASPxGridView grid)
    {
        DataTable dt = new DataTable();
        foreach (GridViewColumn col in grid.VisibleColumns)
        {
            GridViewDataColumn dataColumn = col as GridViewDataColumn;
            if (dataColumn != null)
            {
                if (dataColumn.FieldName != "")
                {
                    dt.Columns.Add(dataColumn.FieldName);
                }
            }
        }
        for (int i = 0; i < grid.VisibleRowCount; i++)
        {
            DataRow row = dt.Rows.Add();
            foreach (DataColumn col in dt.Columns)
            {
                row[col.ColumnName] = grid.GetRowValues(i, col.ColumnName);
            }
        }
        return dt;
    }
}
