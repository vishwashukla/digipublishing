﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_SiteGenre : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Genres";
        lblMsg.Text = "";
        if (!IsPostBack)
            gridBind();
    }

    public void gridBind()
    {
        dgGenre.DataSource = getGenreList();
        dgGenre.DataBind();
        mView.SetActiveView(vGrid);
    }

    private DataTable getGenreList()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "BRAND_SEL_SITEGENRE";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        try
        {
            dt = dp.FillDataTable(cmd);
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
        return dt;
    }

    protected void dgGenre_EditCommand(object source, DataGridCommandEventArgs e)
    {
        hdnGenreID.Value = e.Item.Cells[0].Text;
        this.btnCreate.Text = "Save";
        populateField(Convert.ToInt32(hdnGenreID.Value));
        mView.SetActiveView(vAddEdit);
    }

    public void populateField(int ID)
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "BRAND_SEL_SITEGENRE";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.AddParameter("@id", ID);
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(cmd);
        if (dt.Rows.Count > 0)
            this.txtSiteGenre.Text = utils.formatStringForDBSelect(dt.Rows[0]["genre_name"].ToString());
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        ResetControl();
        mView.SetActiveView(vAddEdit);
    }

    private void ResetControl()
    {
        hdnGenreID.Value = "";
        this.txtSiteGenre.Text = "";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControl();
        mView.SetActiveView(vGrid);
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (siteGenreExist())
        {
            lblMsg.Text = "Genre already exists";
            mView.SetActiveView(vAddEdit);
            return;
        }

        // For Edit
        if (!String.IsNullOrWhiteSpace(hdnGenreID.Value))
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "BRAND_INS_SITEGENRE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.AddParameter("@id", Convert.ToInt32(hdnGenreID.Value));
            cmd.AddParameter("@genre_name", utils.formatStringForDBInsert(this.txtSiteGenre.Text));
            cmd.AddParameter("@m_by", Convert.ToInt32(Session["userid"]));
            dp.ExecuteNonQuery(cmd);
            lblMsg.Text = "Genre successfully updated";
            gridBind();
        }
        // For New Entry
        else
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "BRAND_INS_SITEGENRE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.AddParameter("@id", -1);
            cmd.AddParameter("@genre_name", utils.formatStringForDBInsert(this.txtSiteGenre.Text));
            cmd.AddParameter("@c_by", Convert.ToInt32(Session["userid"].ToString()));
            cmd.AddParameter("@m_by", -1);
            dp.ExecuteNonQuery(cmd);
            lblMsg.Text = "Genre successfully added";
            gridBind();
        }
    }

    public bool siteGenreExist()
    {
        string sQuery = "SELECT count(*) FROM brand_sitegenre "
        + " WHERE genre_name='" + utils.formatStringForDBInsert(txtSiteGenre.Text.Trim()) + "' and ID<>" + Convert.ToInt32(hdnGenreID.Value==""? "0": hdnGenreID.Value);
        DataTable dt = dp.FillDataTable(sQuery);
        if (dt.Rows[0][0].ToString() != "0")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void dgGenre_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        string sQuery = "DELETE FROM brand_sitegenre WHERE ID=" + e.Item.Cells[0].Text;
        dp.ExecuteNonQuery(sQuery);
        lblMsg.Text = "Genre deleted successfully.";
        gridBind();
    }
}