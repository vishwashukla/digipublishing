﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManagePrivacyAndTerm.aspx.cs" Inherits="DigipSuperAdmin_ManagePrivacyAndTerm" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="mvManagePrivacy" runat="server">
        <asp:View ID="viewPrivacy" runat="server">
            <div>
                <table style="width:100%;">
                    <tr>
                        <td></td>
                        <td>
                            <asp:HiddenField ID="hdnID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h4>Manage Privacy</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dgrdPrivacy" runat="server" GridLines="none" AutoGenerateColumns="False"
                                Width="795px" BorderWidth="1" AllowSorting="true" OnItemCommand="dgrdPrivacy_ItemCommand"
                                 OnSortCommand="dgrdPrivacy_SortCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="domainname" HeaderText="Domain Name"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Privacy">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkprivacy" runat="server" CommandName="Privacy" Text="Privacy" CommandArgument="Privacy"
                                                CssClass="toplink" CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Term">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkterm" runat="server" CommandName="Term" Text="Term" CommandArgument="Term"
                                                CssClass="toplink" CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="genre" HeaderText="Site Genre" SortExpression="genre"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:View>
        <asp:View ID="viewEditPrivacy" runat="server">
            <table style="width:100%;">
                <tr>
                    <td class="txtDisplay" style="height: 24px; width: 199px;">Domain Name</td>
                    <td style="height: 24px">
                        <asp:Label ID="lblDomainName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblPrivactTerm" runat="server"></asp:Label></td>
                    <td>
                        <FCKeditorV2:FCKeditor ID="FCKPrivacy" OnPreRender="FCKPrivacy_PreRender" runat="server" OnLoad="FCKPrivacy_Load" Height="500"></FCKeditorV2:FCKeditor>
                    </td>
                </tr>
                <tr><td></td>
                    <td >
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="Mngprivacy" CausesValidation="true" />
                    
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <script>
        function FCKUpdateLinkedField(id) {
            try {
                if (typeof (FCKeditorAPI) == "object") {
                    FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                }
            }
            catch (err) {
            }
        }
    </script>
</asp:Content>

