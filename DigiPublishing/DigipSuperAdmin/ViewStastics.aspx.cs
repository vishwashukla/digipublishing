﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Data.SqlTypes;
using System.Net;
using System.Collections.Generic;
using Microsoft.Web.Administration;

public partial class DigipSuperAdmin_ViewStastics : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Page.Title = ReadConfig.getDomainUrl() + " - View Statistics";

        if (!IsPostBack)
        {
            GetDomainDataBind();
            bindFeatures();
            bindWidgets();
            bindTheme();
            bindSiteGenre();
        }
    }

    /// <summary>
    /// Binding with domain names
    /// </summary>
    private void GetDomainDataBind()
    {
        dgrdDomain.DataSource = getDT();
        dgrdDomain.DataBind();
    }

    private DataTable getDT()
    {
        DataTable dt = new DataTable();
        // status 2 means converted to fulldomain
        string strSql = "SELECT brand_domainname.id,brand_domainname.domainname,brand_sitegenre.genre_name as genre FROM brand_domainname "
                        + " LEFT join brand_sitegenre on brand_sitegenre.ID=brand_domainname.domain_genre "
                        + " where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "'";
        dt = dp.FillDataTable(strSql);
        
        try
        {
            if (dt.Rows.Count > 0)
            {
                dt.Columns.Add("widgets", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["domainname"].ToString().Trim() != "")
                    {
                        dr["widgets"] = strGetWidgets(Convert.ToString(dr["domainname"]));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = Convert.ToString(ex.Message);
        }
        return dt;
    }

    private String strGetWidgets(string strDomain)
    {
        String strAname = "";
        try
        {
            DBHelper dph = new DBHelper();
            if (dph.CheckIfDBExist(strDomain))
            {
                string strSql = "SELECT ApplicationName = ISNULL(Stuff((SELECT ',\n' +ApplicationName AS [text()] FROM "
                            + " (SELECT ApplicationName FROM Brand_AppList WHERE status=1) x For XML PATH ('')),1,1,''),'')";
                DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomain);

                if (dp1.CheckIfDBExist(strDomain))
                {
                    DataTable dt = dp1.FillDataTable(strSql);
                    if (dt.Rows.Count > 0)
                    {
                        strAname = Convert.ToString(dt.Rows[0]["ApplicationName"]);
                    }
                }
            }
        }
        catch
        {
        }
        return strAname;
    }

    private void bindFeatures()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("col1", typeof(string));
        dt.Columns.Add("col2", typeof(Int32));

        foreach (string s in Enum.GetNames(typeof(features)))
        {
            int enumVal = (int)Enum.Parse(typeof(features), s);
            var name = s;
            if (s == features.registerlogin.ToString())
            {
                name = "Register / Login";
            }
            DataRow dr = dt.NewRow();
            dr[0] = name;
            dr[1] = enumVal;
            dt.Rows.Add(dr);
        }
        chkFeatures.DataSource = dt;
        chkFeatures.DataTextField = dt.Columns[0].ColumnName;
        chkFeatures.DataValueField = dt.Columns[1].ColumnName;
        chkFeatures.DataBind();
    }

    private void bindWidgets()
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT a.id,a.ApplicationName,b.Position FROM Brand_AppLists AS a"
                        + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                        + " WHERE b.status = 1"
                        + " ORDER BY a.ApplicationName asc";
        dt = dp.FillDataTable(strSql);
        dgrdWidgets.DataSource = dt;
        dgrdWidgets.DataBind();
    }

    private void bindTheme()
    {
        string strSql = "select id, name, value from brand_themes where status = 1 order by name";
        DataTable dt = dp.FillDataTable(strSql);
        drpTheme.Items.Clear();
        drpTheme.ClearSelection();
        if (dt != null)
        {
            drpTheme.DataSource = dt;
            drpTheme.DataTextField = "name";
            drpTheme.DataValueField = "value";
            drpTheme.DataBind();
            drpTheme.Items.Insert(0, new ListItem("Select Theme", "-1"));
            drpTheme.SelectedValue = "-1";
        }
    }

    private void bindSiteGenre()
    {
        string strSql = "select ID as value, genre_name as name from brand_sitegenre order by genre_name";
        DataTable dt = dp.FillDataTable(strSql);
        if (dt != null)
        {
            ddlSiteGenre.DataSource = dt;
            ddlSiteGenre.DataTextField = "name";
            ddlSiteGenre.DataValueField = "value";
            ddlSiteGenre.DataBind();
            ddlSiteGenre.Items.Insert(0, new ListItem("Select Genre", "-1"));
            ddlSiteGenre.ClearSelection();
        }
    }

    private void resetControl()
    {
        this.txtSubDomain.Text = "";
        drpTheme.ClearSelection();
        txtEmail.Text = "";
        ddlSiteGenre.ClearSelection();
        ddlSource.ClearSelection();
        ddl_random_widgets.SelectedValue = "Select";
        chkFeatures.ClearSelection();
        txtFullName.Text = "";
        txtLatitude.Text = "";
        txtLongitude.Text = "";
        this.hdnUID.Value = "-1";
        this.pwd.Value = "";
        chkTrackingCode.Checked = false;
        chkHoneypotemail.Checked = false;
    }

    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }

    protected void dgrdDomain_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = this.getDT();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    protected void lnkLogin_Click(object sender, CommandEventArgs e)
    {
        string strDomainName = Convert.ToString(e.CommandArgument);
        DBHelper dp1 =ReadConfig.getDomainDBHelper(strDomainName);
        string strLoginGUID = Convert.ToString(System.Guid.NewGuid());
        
        string sqlQueryLoginGUID = "insert into Brand_SuperAdmin_Login(LoginGUID)values('" + strLoginGUID + "')";
        dp1.ExecuteNonQuery(sqlQueryLoginGUID);
        string strLoginUrl = "http://" + Convert.ToString(e.CommandArgument) + "/DigipSuperAdmin/agencyLogin.aspx?LoginGuid=" + strLoginGUID;
        string fullURL = "window.open('" + strLoginUrl + "', '_blank');";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
       
    }

    protected void dgrdDomain_OnItemCommand(object source, DataGridCommandEventArgs e)
    {
        resetControl();
        string strDomainName = String.Empty;
        string strOnDemandFileSize = "0 bytes";
        string strOnDemandDBSize = "0";
        strDomainName = e.Item.Cells[1].Text.Trim();
        strDomainName = strDomainName.Replace("<Font color='red'>(limit!)</font>", "");

        if (e.CommandName == "Edit")
        {
            rfvPWD.Enabled = false;
            rfvCPWD.Enabled = false;
            txtSubDomain.Enabled = false;
            string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + strDomainName + "\\";
            DirectoryInfo dirInfo = new DirectoryInfo(strPath);
            if (dirInfo.Exists)
            {
                strOnDemandFileSize = Math.Round(GetFolderSize(strPath) / (1024 * 1024), 4) + " MB";
                DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomainName);
                DBCommand dbcmd = new DBCommand();
                dbcmd.CommandText = "Sp_spaceused";

                dbcmd.CommandType = CommandType.StoredProcedure;
                DataTable dtDBSize = dp1.FillDataTable(dbcmd);

                if (Convert.ToString(dtDBSize.Rows[0]["database_size"]) != "")
                {
                    strOnDemandDBSize = Convert.ToString(dtDBSize.Rows[0]["database_size"]);
                }

                lbDomainName.Text = e.Item.Cells[1].Text.Trim();
                ListItem liItem = ddlSiteGenre.Items.FindByText(e.Item.Cells[2].Text.Trim());
                if (liItem != null)
                    ddlSiteGenre.SelectedValue = liItem.Value;

                lblSize.Text = strOnDemandDBSize;
                lbFileSize.Text = strOnDemandFileSize;

                SiteDetails(strDomainName);

                var strSql = "SELECT IsAddTrackingCode,isnull(IsSendHoneypotemail,0) as IsSendHoneypotemail FROM brand_domainname "
                        + " where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "' AND domainname='" + strDomainName + "'";
                DataTable dt1 = dp.FillDataTable(strSql);
                chkTrackingCode.Checked = false;
                chkHoneypotemail.Checked = Convert.ToBoolean(Convert.ToInt32(dt1.Rows[0]["IsSendHoneypotemail"].ToString().Trim()));
                if (dt1.Rows[0]["IsAddTrackingCode"].ToString() != "")
                {
                    if(dt1.Rows[0]["IsAddTrackingCode"].ToString() == "1")
                    {
                        chkTrackingCode.Checked = true;
                    }
                }

                mView.SetActiveView(ViewEdit);
            }
            else
                lblMsg.Text = "Agency directory doesn't exist!";
        }
        if (e.CommandName == "Delete")
        {
            DeleteDomain(strDomainName, e.Item.Cells[0].Text.Trim());
        }
    }

    private void SiteDetails(string strDomainName)
    {
        this.txtSubDomain.Text = strDomainName;
        DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomainName);
        string strWidgetSuper = "SELECT a.id,a.ApplicationName,b.Position FROM Brand_AppLists AS a"
                               + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID";
        DataTable dtWidgetSuper = dp.FillDataTable(strWidgetSuper);
        if (dtWidgetSuper.Rows.Count > 0)
        {
            foreach (DataRow dr in dtWidgetSuper.Rows)
            {
                foreach (DataGridItem dgi in dgrdWidgets.Items)
                {
                    CheckBox chkWidgets = (CheckBox)dgi.FindControl("chkWidgets");
                    DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
                    if (dgi.Cells[0].Text == Convert.ToString(dr["id"]))
                    {
                        if (chkWidgets != null)
                            chkWidgets.Checked = false;
                        if (ddlwidgets != null)
                            if (Convert.ToString(dr["position"]) != "")
                                ddlwidgets.SelectedValue = Convert.ToString(dr["position"]);
                    }
                }
            }
        }

        string strquery = "SELECT id,ApplicationName,IsRandom,position FROM Brand_AppList";
        DataTable dtWidgets = dp1.FillDataTable(strquery);
        if (dtWidgets.Rows.Count > 0)
        {
            foreach (DataRow dr in dtWidgets.Rows)
            {
                foreach (DataGridItem dgi in dgrdWidgets.Items)
                {
                    CheckBox chkWidgets = (CheckBox)dgi.FindControl("chkWidgets");
                    DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
                    TextBox txtStartDate = (TextBox)dgi.FindControl("txtDate");
                    TextBox txtEndDate = (TextBox)dgi.FindControl("txtEndDate");
                    TextBox txtTargetImpression = (TextBox)dgi.FindControl("txtTargetImpression");
                    if (dgi.Cells[0].Text == Convert.ToString(dr["id"]))
                    {
                        if (chkWidgets != null)
                            chkWidgets.Checked = false;
                        if (ddlwidgets != null)
                            ddlwidgets.SelectedValue = null;
                        if (chkWidgets != null)
                            chkWidgets.Checked = true;
                        if (ddlwidgets != null)
                            if (Convert.ToString(dr["position"]) != "")
                                ddlwidgets.SelectedValue = Convert.ToString(dr["position"]);
                    }
                }
                //Rakesh 4020 Randem Select
                //Random selection

                //if (dr["IsRandom"] != null && Convert.ToString(dr["IsRandom"]) != "0")
                //{
                //    string strNewQuery = "select * from brand_Applist_Random where Value=1";
                //    DataTable dtRandaomWidgets = dp1.FillDataTable(strNewQuery);
                //    if (dtRandaomWidgets.Rows.Count > 0)
                //    {
                //        foreach (DataRow dRow in dtRandaomWidgets.Rows)
                //        {
                //            if (Convert.ToString(dr["position"]) == "topbanner" && Convert.ToString(dRow["Random_Position"]) == "random_topbanner" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "righttop" && Convert.ToString(dRow["Random_Position"]) == "random_right" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "leftbottom" && Convert.ToString(dRow["Random_Position"]) == "random_left" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "bottomright" && Convert.ToString(dRow["Random_Position"]) == "random_bottom" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "pop_under" && Convert.ToString(dRow["Random_Position"]) == "random_pop_under" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "topheader" && Convert.ToString(dRow["Random_Position"]) == "random_topheader" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "subline" && Convert.ToString(dRow["Random_Position"]) == "random_subline" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //            else if (Convert.ToString(dr["position"]) == "midarticle" && Convert.ToString(dRow["Random_Position"]) == "random_midarticle" && Convert.ToInt32(dRow["Value"]) == 1)
                //            {
                //                this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                //            }
                //        }
                //    }
                //}
                //End Rakesh 4020 Randem Select
            }
        }
        //Rakesh 4020 Randem Select
        string strNewQuery = "select * from brand_Applist_Random where Value=1";
        DataTable dtRandaomWidgets = dp1.FillDataTable(strNewQuery);
        if (dtRandaomWidgets.Rows.Count > 0)
        {
            foreach (DataRow dRow in dtRandaomWidgets.Rows)
            {
                if ( Convert.ToString(dRow["Random_Position"]) == "random_topbanner" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if (Convert.ToString(dRow["Random_Position"]) == "random_right" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if (Convert.ToString(dRow["Random_Position"]) == "random_left" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if (Convert.ToString(dRow["Random_Position"]) == "random_bottom" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if ( Convert.ToString(dRow["Random_Position"]) == "random_pop_under" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if ( Convert.ToString(dRow["Random_Position"]) == "random_topheader" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if ( Convert.ToString(dRow["Random_Position"]) == "random_subline" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
                else if ( Convert.ToString(dRow["Random_Position"]) == "random_midarticle" && Convert.ToInt32(dRow["Value"]) == 1)
                {
                    this.ddl_random_widgets.Items.FindByValue(Convert.ToString(dRow["Random_Position"])).Selected = true;
                }
            }
        }
        //End Rakesh 4020 Randem Select
        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + strDomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strPath);
        if (dirInfo.Exists)
        {
            Dictionary<string, string> ConfigNodes = ReadConfig.GetConfigNodes(strDomainName);
            if (ConfigNodes.Count > 0)
            {
                drpTheme.SelectedValue = "-1";
                if (ConfigNodes.ContainsKey("theme"))
                {
                    ListItem li = drpTheme.Items.FindByValue(ConfigNodes["theme"]);
                    if (li != null)
                        drpTheme.SelectedValue = li.Value;
                }

                if (ConfigNodes.ContainsKey("features"))
                {
                    string strFeature = string.Empty;
                    string strFeatures = "SELECT features FROM brand_settings where id = 1 ";
                    DataTable dtFeatures = dp1.FillDataTable(strFeatures);
                    if (dtFeatures.Rows.Count > 0 && !String.IsNullOrWhiteSpace(Convert.ToString(dtFeatures.Rows[0][0])))
                    {
                        strFeature = Convert.ToString(dtFeatures.Rows[0][0]);
                        if (!string.IsNullOrWhiteSpace(strFeature))
                        {
                            utils.SetSelectedCheckBoxItems(chkFeatures, strFeature);
                        }
                    }
                }
                //if (ConfigNodes.ContainsKey("email"))
                //{
                //    txtEmail.Text = ConfigNodes["email"];
                //}
                string strSqluser = "SELECT TOP 1 email FROM Brand_User WHERE roles='admin'";
                DataTable dtuser = dp1.FillDataTable(strSqluser);
                if (dtuser.Rows.Count > 0)
                {
                    txtEmail.Text = dtuser.Rows[0]["email"].ToString();
                }
                if (ConfigNodes.ContainsKey("email"))
                {
                    string str = ConfigNodes["email"];
                }
                if (ConfigNodes.ContainsKey("logo"))
                {
                    hdnLogo.Value = ConfigNodes["logo"];
                }
                if (ConfigNodes.ContainsKey("source"))
                {
                    ListItem liItem = ddlSource.Items.FindByValue(ConfigNodes["source"]);
                    if (liItem != null)
                        ddlSource.SelectedValue = liItem.Value;
                }
                if (ConfigNodes.ContainsKey("status"))
                {
                    ListItem liItem = ddlStatus.Items.FindByValue(ConfigNodes["status"]);
                    if (liItem != null)
                        ddlStatus.SelectedValue = liItem.Value;
                }
                if (ConfigNodes.ContainsKey("latitude"))
                {
                    this.txtLatitude.Text = ConfigNodes["latitude"];
                }
                if (ConfigNodes.ContainsKey("longitude"))
                {
                    this.txtLongitude.Text = ConfigNodes["longitude"];
                }
            }
          

            string strSql = "SELECT password,uid,fullname FROM brand_user WHERE email='" + txtEmail.Text.Trim() + "'";
            DataTable dtUser = dp1.FillDataTable(strSql);
            if (dtUser.Rows.Count > 0)
            {
                this.pwd.Value = Convert.ToString(dtUser.Rows[0]["password"]);
                this.hdnUID.Value = Convert.ToString(dtUser.Rows[0]["uid"]);
                this.txtFullName.Text = Convert.ToString(dtUser.Rows[0]["fullname"]);
            }
        }
    }  

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string strDomain = string.Empty;
        string strFeatures = string.Empty;
        string strLatitude = string.Empty;
        string strLongitude = string.Empty;
        DateTime strStartDate = SqlDateTime.MinValue.Value;
        DateTime strEndDate = SqlDateTime.MinValue.Value; 
        strDomain = this.txtSubDomain.Text;
        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + strDomain + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strPath);
        if (dirInfo.Exists)
        {
            try
            {
                strLatitude = txtLatitude.Text.Trim();
                if (strLatitude == "")
                {
                    strLatitude = "0.0";
                }
                strLongitude = txtLongitude.Text.Trim();
                if (strLatitude == "")
                {
                    strLongitude = "0.0";
                }
                int AddTrackinCode = 0;
                if (chkTrackingCode.Checked)
                {
                    AddTrackinCode = 1;
                }

                int Honeypotemail = 0;
                if (chkHoneypotemail.Checked)
                {
                    Honeypotemail = 1;
                }

                strFeatures = utils.GetSelectedCheckBoxItems(chkFeatures);
                strFeatures.TrimEnd(',');

                ReadConfig.UpdateSingleNodeInConfig(strDomain, "features", strFeatures);

                DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomain);
                string strString = "SELECT value FROM brand_AppSettings WHERE Name='user is status length' AND Type='2'";
                DataTable dtStatusLength = dp1.FillDataTable(strString);
                if (dtStatusLength.Rows.Count > 0)
                {
                    strString = "UPDATE brand_AppSettings SET m_by='" + Convert.ToString(Session["userid"]) + "',m_date=getdate() WHERE Name='user is status length' AND Type='2'";
                }
                else
                {
                    strString = "INSERT brand_AppSettings([name],[description],[type],[c_by]) values('user is status length','User is status length','2','" + Convert.ToString(Session["userid"]) + "')";
                }
                dp1.ExecuteNonQuery(strString);
                string strQuery = "truncate table brand_AppList";
                dp1.ExecuteNonQuery(strQuery);
                
                StringBuilder SBIdPosition = new StringBuilder();
                //Rakesh 4020
                foreach (ListItem lstItem in ddl_random_widgets.Items)
                {
                    int strRandomPosition = 0;
                    if (lstItem.Value != "Select")
                    {
                        if (lstItem.Selected != true)
                        {
                            strRandomPosition = 0;
                        }
                        else
                        {
                            strRandomPosition = 1;
                        }
                        string strSqlQuery = "update brand_Applist_Random set Value = '" + strRandomPosition + "' where Random_Position = '" + lstItem.Value + "'";
                        if (dp1.ExecuteNonQuery(strSqlQuery) == 0)
                        {
                            string strSqlInsQuery = "insert into brand_Applist_Random (Random_Position, Value) Values('" + lstItem.Value + "','" + strRandomPosition + "')";
                            dp1.ExecuteNonQuery(strSqlInsQuery);
                        }
                    }
                }
               // End Rakesh 4020

                foreach (DataGridItem dgi in dgrdWidgets.Items)
                {
                    CheckBox chkWidgets = (CheckBox)dgi.FindControl("chkWidgets");
                    if (chkWidgets != null && chkWidgets.Checked == true)
                    {
                        DropDownList ddlwidgets = (DropDownList)dgi.FindControl("ddlwidgets");
                        string strPosition = "none";
                        if (ddlwidgets != null)
                            strPosition = ddlwidgets.SelectedValue;

                        //Rakesh 4020
                        //int strRandomPosition = 0;
                        //foreach (ListItem lstItem in ddl_random_widgets.Items)
                        //{
                        //    if (lstItem.Value != "Select")
                        //    {
                        //        if (lstItem.Selected != true)
                        //        {
                        //            strRandomPosition = 0;
                        //        }
                        //        else
                        //        {
                        //            strRandomPosition = 1;
                        //            strRandom = "1";
                        //        }
                        //        string strSqlQuery = "update brand_Applist_Random set Value = '" + strRandomPosition + "' where Random_Position = '" + lstItem.Value + "'";
                        //        if (dp1.ExecuteNonQuery(strSqlQuery) == 0)
                        //        {
                        //            string strSqlInsQuery = "insert into brand_Applist_Random (Random_Position, Value) Values('" + lstItem.Value + "','" + strRandomPosition + "')";
                        //            dp1.ExecuteNonQuery(strSqlInsQuery);
                        //        }
                        //    }
                        //}
                        //End Rakesh 4020

                        string strRandom = "0";
                        foreach (ListItem lstItem in ddl_random_widgets.Items)
                        {
                            if (lstItem.Value != "Select")
                            {
                                if (lstItem.Selected == true && strPosition == "topheader" && lstItem.Value == "random_topheader")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "topbanner" && lstItem.Value == "random_topbanner")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "leftbottom" && lstItem.Value == "random_left")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "righttop" && lstItem.Value == "random_right")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "bottomright" && lstItem.Value == "random_bottom")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "pop_under" && lstItem.Value == "random_pop_under")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "subline" && lstItem.Value == "random_subline")
                                {
                                    strRandom = "1";
                                }
                                if (lstItem.Selected == true && strPosition == "midarticle" && lstItem.Value == "random_midarticle")
                                {
                                    strRandom = "1";
                                }
                            }
                        }

                        strQuery = "SELECT a.*,b.* FROM Brand_AppLists AS a"
                                   + " INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                   + " WHERE a.id='" + dgi.Cells[0].Text + "'";
                        DataTable dt = dp.FillDataTable(strQuery);
                        if (dt.Rows.Count > 0)
                        {
                            string bannercodebox = Convert.ToString(dt.Rows[0]["CodeBox"]);
                            string bannername = Convert.ToString(dt.Rows[0]["ApplicationName"]);
                            string strfinalvalue = string.Empty;
                            string strresult = string.Empty;
                            if (bannercodebox.Contains("change-genre.aspx"))
                            {
                                string strbannerq = "SELECT genre_name FROM brand_sitegenre where ID=(select domain_genre from brand_domainname where domainname='" + strDomain + "')";
                                DataTable dtbanner = dp.FillDataTable(strbannerq);
                                if (dtbanner.Rows.Count > 0)
                                {
                                    string genrevalue = Convert.ToString(dtbanner.Rows[0]["genre_name"]);
                                    if (genrevalue != null)
                                    {
                                        string url = "http://" + ReadConfig.getDomainUrl() + "/widgets/realbanner/change-genre.aspx?name=" + genrevalue + "";
                                        HttpWebRequest httpreq = (HttpWebRequest)WebRequest.Create(url);
                                        HttpWebResponse httpres = (HttpWebResponse)httpreq.GetResponse();
                                        StreamReader sr = new StreamReader(httpres.GetResponseStream());
                                        string results = sr.ReadToEnd();
                                        sr.Close();
                                        strresult = results;
                                    }
                                    if (bannername.Contains("left"))
                                    {
                                        strfinalvalue = "http://muk.247realmedia.com/RealMedia/ads/adstream_jx.ads/" + strresult + "/1'' + RNS + ''@Right?";
                                    }
                                    else if (bannername.Contains("right"))
                                    {
                                        strfinalvalue = "http://muk.247realmedia.com/RealMedia/ads/adstream_jx.ads/" + strresult + "/1'' + RNS + ''@Right1?";
                                    }
                                    else if (bannername.Contains("top"))
                                    {
                                        strfinalvalue = "http://muk.247realmedia.com/RealMedia/ads/adstream_jx.ads/" + strresult + "/1'' + RNS + ''@Top1?";
                                    }

                                    bannercodebox = bannercodebox.Replace("widgets/realbanner/change-genre.aspx", strfinalvalue);
                                    dt.Rows[0]["CodeBox"] = bannercodebox;
                                }
                            }
                            strQuery = "insert into brand_AppList ([id],[ApplicationName],[Version],[CodeBox]   "
                            + " ,[status],[Image],[cost],[IsRandom],[Position],[Region],[Country]"
                            + " ,[Browser],[Referal],[Not_Referal],[Blocker],[height],[width],[SameIP_Status],[SameIP_Value],[BlockerValue],[OS])"
                            + "values ('" + dt.Rows[0]["id"] + "','"
                            + dt.Rows[0]["ApplicationName"] + "','"
                            + dt.Rows[0]["Version"] + "','" + dt.Rows[0]["CodeBox"] + "',"
                            + dt.Rows[0]["status"] + ",'"
                            + dt.Rows[0]["Image"] + "'," + dt.Rows[0]["cost"] + ","
                            + strRandom + ",'" + strPosition + "','" + dt.Rows[0]["Region"] + "','" + dt.Rows[0]["Country"] + "','"
                            + dt.Rows[0]["Browser"] + "','" + dt.Rows[0]["Referal"] + "','" + dt.Rows[0]["Not_Referal"] + "','"
                            + dt.Rows[0]["Blocker"] + "','" + dt.Rows[0]["height"] + "','" + dt.Rows[0]["width"] + "','" + dt.Rows[0]["SameIP_Status"] + "','" + dt.Rows[0]["SameIP_Value"] + "','" + dt.Rows[0]["BlockerValue"] + "','" + dt.Rows[0]["OS"] + "')";
                            dp1.ExecuteNonQuery(strQuery);
                        }
                    }
                    DropDownList ddlwidgetsCache = (DropDownList)dgi.FindControl("ddlwidgets");
                    string StrDefaultPosition = "none";
                    if (ddlwidgetsCache != null)
                        StrDefaultPosition = ddlwidgetsCache.SelectedValue;
                    if (StrDefaultPosition != "topbanner")
                    {
                        if (Convert.ToString(SBIdPosition) == "")
                        {
                            SBIdPosition.Append(Convert.ToString(dgi.Cells[0].Text) + "," + StrDefaultPosition);
                        }
                        else
                        {
                            SBIdPosition.Append("," + Convert.ToString(dgi.Cells[0].Text) + "," + StrDefaultPosition);
                        }
                    }
                }
                try
                {
                    if (Convert.ToString(SBIdPosition) != "")
                    {
                        string url4 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitenottop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=" + this.txtSubDomain.Text;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url4);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        string url5 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitenottop&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=" + this.txtSubDomain.Text;
                        HttpWebRequest request5 = (HttpWebRequest)WebRequest.Create(url5);
                        HttpWebResponse response5 = (HttpWebResponse)request5.GetResponse();
                    }

                    string url0 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitetop&Cachekey1=WidgetTop&domainurl=" + this.txtSubDomain.Text;
                    HttpWebRequest request0 = (HttpWebRequest)WebRequest.Create(url0);
                    HttpWebResponse response0 = (HttpWebResponse)request0.GetResponse();
                    string url2 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitetop&Cachekey1=WidgetTop&domainurl=" + this.txtSubDomain.Text;
                    HttpWebRequest request2 = (HttpWebRequest)WebRequest.Create(url2);
                    HttpWebResponse response2 = (HttpWebResponse)request2.GetResponse();
                }
                catch{ }

                HttpPostedFile strLogo = UploadLogo.PostedFile;
                string strLogoName = strLogo.FileName;
                int index = strLogoName.LastIndexOf(@"\");
                strLogoName = strLogoName.Substring(index + 1);
                if (strLogoName != "")
                    UploadLogo.PostedFile.SaveAs(Server.MapPath("~/config/" + strDomain + "/" + strLogoName));
                else
                    strLogoName= hdnLogo.Value;

                string strIconName = Uploadicon20x20.PostedFile.FileName;
                strIconName = strIconName.Substring(strIconName.LastIndexOf(@"\") + 1);
                if (strIconName != "")
                {
                    Uploadicon20x20.PostedFile.SaveAs(Server.MapPath("~/config/" + strDomain + "/Icon20x20.png"));
                }

                //Update Agency settings in config.config
                ReadConfig.SetConfigWithParam(HttpContext.Current.Request.PhysicalApplicationPath + "config/" + strDomain + "/Config.config",
             "", "", strLogoName, drpTheme.SelectedValue, txtEmail.Text.Trim(), ddlSource.SelectedValue, ddlStatus.SelectedValue, strLatitude, strLongitude);

                //Update selected festures in DB
                string strSql = "update brand_settings set features = '" + strFeatures + "' where id = 1 ";
                dp1.ExecuteNonQuery(strSql);

                string strSqlgetfirstadminuser = "SELECT TOP 1 uid FROM Brand_User WHERE roles='admin'";
                DataTable dtfirstadminuser = dp1.FillDataTable(strSqlgetfirstadminuser);
                if (dtfirstadminuser.Rows.Count > 0)
                {
                    string updateMailId = "update brand_user set email ='" + txtEmail.Text.Trim() + "',uname='" + txtEmail.Text.Trim() + "' where roles='admin' and uid='" + Convert.ToString(dtfirstadminuser.Rows[0][0]) + "'";
                    dp1.ExecuteNonQuery(updateMailId);
                }

                strSql = "update brand_domainname set modifydate = getdate(),status = " + ddlStatus.SelectedValue + ", Address = null, Fax='', domain_genre=" + this.ddlSiteGenre.SelectedValue + ",IsAddTrackingCode=" + AddTrackinCode + ",IsSendHoneypotemail=" + Honeypotemail + " where domainname = '" + strDomain + "'";
                dp.ExecuteNonQuery(strSql);
                if (this.hdnUID.Value != "-1")
                {
                    if(txtCPwd.Text.Trim() != "")
                    {
                        UpdatePWD(strDomain);
                    }
                    string strSqlfullname = "Update brand_user SET fullname='" + utils.formatStringForDBInsert(this.txtFullName.Text) + "' WHERE uid='" + this.hdnUID.Value + "'";
                    DBHelper dpsite = ReadConfig.getDomainDBHelper(strDomain);
                    dpsite.ExecuteNonQuery(strSqlfullname);
                }

                //UPDATE Domain name and Genre name in report database
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                string strSiteGenre = string.Empty;
                if (this.ddlSiteGenre.SelectedValue == "-1")
                    strSiteGenre = "";
                else
                    strSiteGenre = this.ddlSiteGenre.SelectedItem.Text;
                string sqlreportDBDomainGenre = "EXEC [dbo].[SP_Brand_Update_Domain_Genre] @Domain='" + strDomain + "',@Genre='" + strSiteGenre + "'";
                dpreport.ExecuteNonQuery(sqlreportDBDomainGenre);

                lblMsg.Text = "Settings updated successfully";
                GetDomainDataBind();
                resetControl();
                mView.SetActiveView(Vgrid);
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
            }
        }
    }

    private void UpdatePWD(string strDomain)
    {
        string strPwd = "";
        string strCalculatedPwd = "";
        string originalPassword = "";
        if (txtCPwd.Text.Trim() != "")
        {
            pwd.Value = txtCPwd.Text.Trim();
            strPwd = txtCPwd.Text.Trim();
            originalPassword = strPwd + this.sault.Value.Trim();
            int saltSize = 2;
            Byte[] saltBytes = new byte[saltSize];
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);
            //Convert encoded bytes back to a 'readable' string
            strCalculatedPwd = BitConverter.ToString(encodedBytes).Replace("-", "");
        }
        else
        {
            strCalculatedPwd = pwd.Value;
        }
        string strSql = "Update brand_user SET password='" + strCalculatedPwd + "',fullname='" + utils.formatStringForDBInsert(this.txtFullName.Text) + "' WHERE uid='" + this.hdnUID.Value + "'";
        DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomain);
        dp1.ExecuteNonQuery(strSql);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetControl();
        mView.SetActiveView(Vgrid);
    }

    private void DeleteDomain(string strDomain, string strDomainID)
    {
        DBHelper db = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        string strQuery = "";
        try
        {
            if (strDomain != "")
            {
                string strDB = "";
                string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + strDomain + "\\";
                DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
                if (dirInfo.Exists)
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    strDomainPPath += "config.config";
                    xmlDoc.Load(strDomainPPath);

                    XmlNode node = xmlDoc.SelectSingleNode("/appSettings/connection");
                    if (node != null && !string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        strDB = node.InnerText.Replace(" ", "").Substring(node.InnerText.Replace(" ", "").IndexOf("InitialCatalog"));
                        strDB = strDB.Substring(strDB.IndexOf("=")+1);
                        strDB = strDB.Substring(0, strDB.IndexOf(";"));
                    }
                }

                if (strDB != "")
                {
                    DBCommand dbcmd = new DBCommand();
                    dbcmd.CommandText = "usp_killusers";
                    dbcmd.AddParameter("@dbname", strDB);
                    dbcmd.CommandType = CommandType.StoredProcedure;
                    db.ExecuteNonQuery(dbcmd);
                    strQuery = "DROP DATABASE [" + strDB + "]";
                    db.ExecuteNonQuery(strQuery);
                    GetDomainDataBind();
                    strQuery = "DELETE brand_domainname WHERE id=" + strDomainID;
                    db.ExecuteNonQuery(strQuery);

                    string strSqlDelDomainDBMapping = "DELETE brand_Domain_N_DB_Mapping WHERE DomainName='" + strDomain + "'";
                    dp.ExecuteNonQuery(strSqlDelDomainDBMapping);

                    //DELETE Domain name and Genre name in report database
                    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                    string sqlreportDBDomainGenre = "EXEC [dbo].[SP_Brand_DELETE_Domain_Genre] @Domain='" + strDomain + "'";
                    dpreport.ExecuteNonQuery(sqlreportDBDomainGenre);

                    // Delete a directory and all subdirectories with Directory static method...
                    if (System.IO.Directory.Exists(Server.MapPath("~/config/" + strDomain)))
                    {
                        System.IO.Directory.Delete(Server.MapPath("~/config/" + strDomain), true);
                        GetDomainDataBind();
                    }
                }
                bool issuccess = RemoveBinding(strDomain);
                if(issuccess)
                    lblMsg.Text = "Deleted successfully.";
                else
                    lblMsg.Text = "Deleted successfully but an error occurred while removing bindings from IIS.";
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text =ex.Message;
        }
    }

    private bool RemoveBinding(string siteName)
    {
        bool flag = false;
        try
        {
            List<int> lstindexes = new List<int>();
            var IISsitename = ConfigLoader.GetIISSiteName();
            ServerManager serverMgr = new ServerManager();
            for (int i = 0; i < serverMgr.Sites[IISsitename].Bindings.Count; i++)
            {
                var host = serverMgr.Sites[IISsitename].Bindings[i].Host.ToString().Trim().ToLower();
                if (host == siteName.Trim().ToLower() || host == "www." + siteName.Trim().ToLower())
                {
                    lstindexes.Add(i);
                }
            }
            var count = 0;
            foreach(var index in lstindexes)
            {
                var i = index - count;
                serverMgr.Sites[IISsitename].Bindings.RemoveAt(i);
                count++;
            }
            serverMgr.CommitChanges();
            flag = true;
        }
        catch
        {
            flag = false;
        }
        return flag;
    }

    protected bool AddBinding(string hostName)
    {
        var flag = false;
        //if ((hostName.StartsWith("http") && hostName.Contains("www")) || hostName.StartsWith("www"))
        //{
        //    hostName = hostName.Split(new char[] { '.', ' ' }, 2, StringSplitOptions.RemoveEmptyEntries)[1];
        //}
        //else if (hostName.StartsWith("http"))
        //{
        //    if (hostName.StartsWith("https://"))
        //        hostName = hostName.Replace("https://", "").Trim();
        //    else if (hostName.StartsWith("http://"))
        //        hostName = hostName.Replace("http://", "").Trim();
        //}

        ServerManager server = new ServerManager();
        if (server.Sites != null && server.Sites.Count > 0)
        {
            try
            {
                var sitename = ConfigLoader.GetIISSiteName();
                string ip = "*";
                string port = "80";
                // binding without www
                Binding binding = server.Sites[sitename].Bindings.CreateElement("binding");
                binding.Protocol = "http";
                binding.BindingInformation = ip + ":" + port + ":" + hostName;
                server.Sites[sitename].Bindings.Add(binding);

                // binding with www
                binding = server.Sites[sitename].Bindings.CreateElement("binding");
                binding.Protocol = "http";
                binding.BindingInformation = ip + ":" + port + ":www." + hostName;
                server.Sites[sitename].Bindings.Add(binding);

                server.CommitChanges();
                flag = true;
            }
            catch
            {
                flag = false;
            }
        }
        return flag;
    }

    protected void lbDomainName_Click(object sender, EventArgs e)
    {
        mView.SetActiveView(Vgrid);
    }

    protected void lbBack_Click(object sender, EventArgs e)
    {
        mView.SetActiveView(Vgrid);
    }

    #region Convert to Full Domain
    protected void btnConvert_Click(object sender, EventArgs e)
    {
        string fullAgencyName = txtFulldomainName.Text;
        string strDomainName = lbDomainName.Text;
        bool success = true;
        if (strDomainName != "" && fullAgencyName != "")
            success = RenameAgency(strDomainName, fullAgencyName);

        if (!success)
        {
            lblMsg.Text = "Agency with this name already exists.";
        }
        else
        {
            lblMsg.Text = "Agency converted to given full domain.";
            GetDomainDataBind();
            mView.SetActiveView(Vgrid);
        }
    }

    private bool RenameAgency(string agencyName, string fullDomainName)
    {
        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config\\";
        GC.Collect();
        GC.WaitForPendingFinalizers();
        if (System.IO.Directory.Exists(strPath + agencyName) && !System.IO.Directory.Exists(strPath + fullDomainName))
        {
            DBHelper dp1 = ReadConfig.getDomainDBHelper(agencyName);
            System.IO.Directory.Move(strPath + agencyName, strPath + fullDomainName);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "brand_SearchAndReplace";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@SearchStr", agencyName);
            cmd.Parameters.Add("@ReplaceStr", fullDomainName);
            dp1.ExecuteNonQuery(cmd);
            dp.ExecuteNonQuery("update brand_domainname set domainname_check = '" + Convert.ToString(fullDomainName) + "',status = 2 where domainname='" + Convert.ToString(agencyName) + "'");
            dp.ExecuteNonQuery("update brand_Domain_N_DB_Mapping set DomainName = '" + Convert.ToString(fullDomainName) + "' where DomainName='" + Convert.ToString(agencyName) + "'");

            DataTable dtexist = dp.FillDataTable("Select domainname from brand_domainname where domainname='" + Convert.ToString(fullDomainName) + "'");
            if(dtexist.Rows.Count > 0)
            {
                dp.ExecuteNonQuery("update brand_domainname set status = 1 where domainname='" + Convert.ToString(fullDomainName) + "'");
            }
            else
            {
                string strQuery = "insert into dbo.brand_domainname(domainname,modifydate,status,Address,Fax,domain_check,domainname_check)  select '" + fullDomainName + "',modifydate,1,null,Fax,domain_check,domainname_check from dbo.brand_domainname where domainname = '" + Convert.ToString(agencyName) + "'";
                dp.ExecuteNonQuery(strQuery);
            }

            bool issuccess = RemoveBinding(agencyName);

            bool issuccess2 = AddBinding(fullDomainName);

            return true;
        }
        else
            return false;
    }
    #endregion

    #region Export site info
    protected void lnkExportInfo_Click(object sender, EventArgs e)
    {
        dgr.HeaderStyle.Font.Bold = true;
        dgr.DataSource = GetExportRecords();
        dgr.DataBind();
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=site.xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        dgr.RenderControl(htmlWrite);
        Response.Write(Convert.ToString(stringWrite));
        Response.End();
    }

    private DataTable GetExportRecords()
    {
        DataTable dt = new DataTable();
        try
        {
            string strSql = "SELECT domainname,ISNULL(genre_name,'') AS site_genre,CASE WHEN status='1' THEN 'Active' ELSE 'In Active' END AS site_status FROM brand_domainname "
                        + " LEFT join brand_sitegenre on brand_sitegenre.ID=brand_domainname.domain_genre "
                        + " where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "'";
            dt = dp.FillDataTable(strSql);
            dt.Columns.Add("Email");
            dt.Columns.Add("titles");
            dt.Columns.Add("keywords");
            dt.Columns.Add("description");
            dt.Columns.Add("hd_space");
            dt.Columns.Add("Widget", typeof(string));
            dt.Columns.Add("Teams", typeof(string));
            dt.Columns.Add("Tot_Event", typeof(string));
            dt.Columns.Add("Tot_BP", typeof(string));
            dt.Columns.Add("Tot_Rss", typeof(string));
            dt.Columns.Add("Tot_Tag", typeof(string));
            dt.Columns.Add("Tot_User", typeof(string));
            dt.Columns.Add("Tot_Team", typeof(string));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string DomainName = Convert.ToString(dr["domainname"]);

                    try
                    {
                        //Title
                        dr["titles"] = GetTitleKeywordDesc(DomainName, "title");
                        //Keywords
                        dr["keywords"] = GetTitleKeywordDesc(DomainName, "keywords");
                        //Description
                        dr["description"] = GetTitleKeywordDesc(DomainName, "description");
                    }
                    catch (Exception ex)
                    {
                        this.lblmessage.Text = ex.Message;
                    }

                    //For Hardisk space
                    try
                    {
                        dr["hd_space"] = GetDiskSpace(DomainName);
                    }
                    catch (Exception ex)
                    {
                        this.lblmessage.Text = ex.Message;
                    }
                    
                    try
                    {
                        dr["Email"] = ReadConfig.GetEmail(DomainName);

                        //Bind with the current number of users, blog Posts, Events & Rss Feeds.
                        DBCommand com = new DBCommand();
                        com.CommandText = "SP_Brand_Count";
                        com.CommandType = CommandType.StoredProcedure;
                        com.Parameters.Add("@email", "GUEST@" + DomainName + "");
                        DBHelper dp1 = ReadConfig.getDomainDBHelper(DomainName);
                        DataTable dtCount = dp1.FillDataTable(com);
                        if (dtCount.Rows.Count > 0)
                        {
                            //Widgets
                            dr["Widget"] = Convert.ToString(dtCount.Rows[0]["ApplicationName"]);

                            //Count number of users
                            dr["Tot_User"] = Convert.ToString(dtCount.Rows[0]["UserCount"]);

                            //Count number of Blog posts
                            dr["Tot_BP"] = Convert.ToString(dtCount.Rows[0]["BlogCount"]);

                            //Count number of Rss Feeds
                            dr["Tot_Rss"] = Convert.ToString(dtCount.Rows[0]["RssCount"]);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }
        catch
        {
        }
        return dt;
    }

    private string GetTitleKeywordDesc(string strDomainName, string name)
    {
        string strValue = "";
        string strSql = "SELECT description FROM Brand_AppSettings WHERE IsDelete='False' AND name='" + name + "' AND Type=1";
        DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomainName);
        DataTable dt = dp1.FillDataTable(strSql);
        if (dt.Rows.Count > 0)
        {
            strValue = Convert.ToString(dt.Rows[0]["description"]);
        }
        return strValue;
    }

    private string GetDiskSpace(string strDomainName)
    {
        double onDemandFileSize = 0;
        double onDemandDBSize = 0;
        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + strDomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strPath);
        if (dirInfo.Exists)
        {
            onDemandFileSize = GetFolderSize(strPath) / (1024 * 1024);
            DBCommand dbcmd = new DBCommand();
            dbcmd.CommandText = "Sp_spaceused";
            dbcmd.CommandType = CommandType.StoredProcedure;
            DataTable dtDBSize = dp.FillDataTable(dbcmd);

            if (Convert.ToString(dtDBSize.Rows[0]["database_size"]) != "")
            {
                string strOnDemandDBSize = Convert.ToString(dtDBSize.Rows[0]["database_size"]);
                strOnDemandDBSize = strOnDemandDBSize.Replace("MB", "");
                onDemandDBSize = Convert.ToDouble(strOnDemandDBSize.Trim());
            }
        }
        double diskUsage = onDemandFileSize + onDemandDBSize;
        diskUsage = Math.Round(diskUsage, 4);
        string strDiskSpace = Convert.ToString(diskUsage) + " MB";
        return strDiskSpace;
    }

    private double GetFolderSize(string physicalPath)
    {
        double dblDirSize = 0;
        DirectoryInfo objDirInfo = new DirectoryInfo(physicalPath);
        Array arrChildFiles = objDirInfo.GetFiles();
        Array arrSubFolders = objDirInfo.GetDirectories();
        foreach (FileInfo objChildFile in arrChildFiles)
        {
            dblDirSize += objChildFile.Length;
        }
        foreach (DirectoryInfo objChildFolder in arrSubFolders)
        {
            dblDirSize += GetFolderSize(objChildFolder.FullName);
        }
        return dblDirSize;
    }    
    #endregion
}