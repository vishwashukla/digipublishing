﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageLanguages.aspx.cs" Inherits="DigipSuperAdmin_ManageLanguages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="MvLanguages" runat="server">
        <asp:View ID="viewLanguages" runat="server">
            <div>
                <h2>Manage Languages</h2>
            </div>
            <div style="text-align:right;">
                <asp:LinkButton ID="lnkAddNew" Text="Add Language" runat="server" OnClick="lnkAddNew_Click"></asp:LinkButton>
            </div>
            <div style="margin:25px 0 0 25px;">
                <asp:DataGrid ID="dgrdLanguage" runat="server" AutoGenerateColumns="false"
                 GridLines="None" AllowCustomPaging="false" AllowPaging="false" AllowSorting="false"
                  ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" 
                   OnItemCommand="dgrdLanguage_ItemCommand" BorderWidth="1"  BorderColor="black" BorderStyle="solid"
                    >
                    <Columns>
                        <asp:BoundColumn DataField="LanguageID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="LanguageName" HeaderText="Language"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%#Eval("LanguageID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Delete"  CommandArgument='<%#Eval("LanguageID") %>'
                                 OnClientClick="javascript:return confirm('Are you sure to delete this language?');" ></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </asp:View>
        <asp:View ID="viewAddEdit" runat="server">
            <div>
                <h2 id="hAddEdit" runat="server"></h2>
            </div>
            <table>
                <tr>
                    <td class="txtDisplay">
                        <b>Language Name</b></td>
                    <td>
                        <asp:TextBox ID="txtLanguage" runat="server" ValidationGroup="EventVCG"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFVtxtLanguage" runat="server" ControlToValidate="txtLanguage"
                            ErrorMessage="Please fill language name." ForeColor="Red" Display="dynamic" SetFocusOnError="true"
                            ValidationGroup="EventVCG">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" ValidationGroup="EventVCG" Text="Save" runat="server" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="LanguageID" runat="server" />
</asp:Content>