﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageAllSiteUsers_old.aspx.cs" Inherits="DigipSuperAdmin_ManageAllSiteUsers" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../JS/Password.js"></script>
    <div>
        <h4>Manage All Users</h4>
    </div>
    <asp:Label ID="lblmessage" runat="server" ForeColor="red"></asp:Label>
    <asp:MultiView ID="mvallusers" runat="server">
        <asp:View ID="vgrid" runat="server">
            <table style="width:100%;">
                <tr>
                    <td>
                        <%=sbPagination %>
                        <asp:DataGrid runat="server" ID="dgrdAllUsers" AllowPaging="false" AutoGenerateColumns="false"
                            AllowSorting="true" OnItemCommand="dgrdAllUsers_ItemCommand" OnSortCommand="dgrdAllUsers_SortCommand">
                            <Columns>
                                <asp:BoundColumn HeaderText="User ID" DataField="uid" SortExpression="uid"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Name" DataField="fullname" SortExpression="fullname"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Email" DataField="email" SortExpression="email"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Role" DataField="roles" SortExpression="roles"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Domain Name" DataField="DomainName" SortExpression="DomainName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Date of Registration" DataField="c_date" SortExpression="c_date"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btbEdit" Text="Edit" runat="server" CommandName="Edit"></asp:LinkButton>
                                        <span id="spandel" visible='<%#Convert.ToBoolean(Eval("DEL")) %>' runat="server">|</span><asp:LinkButton ID="btnDel" Text="Delete" runat="server" visible='<%#Convert.ToBoolean(Eval("DEL")) %>' CommandName="Delete" OnClientClick="return confirm('Are you sure you wish to delete?')"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <%=sbPagination %>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vedit" runat="server">
            <table class="table-full">
                <tr>
                    <td>Title</td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td>
                        <asp:DropDownList ID="ddlRoles" runat="server" CssClass="select" AutoPostBack="false">
                            <asp:ListItem Value="admin">admin</asp:ListItem>
                            <asp:ListItem Value="super admin">Super Admin</asp:ListItem>
                            <asp:ListItem Value="subscribe">subscribe</asp:ListItem>
                            <asp:ListItem Value="user">user</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Name*</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>                       
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true" ForeColor="Red"
                            ControlToValidate="txtname" ErrorMessage="Please enter name" ValidationGroup="MngUsrVCG">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Email*</td>
                    <td>
                        <asp:TextBox ID="txtUserEmail" runat="server"></asp:TextBox>&nbsp;        
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" SetFocusOnError="true" ForeColor="Red"
                            ControlToValidate="txtUserEmail" Display="Dynamic" ErrorMessage="invalid e-mail address! please re-enter"
                            ValidationGroup="MngUsrVCG" ValidationExpression="^[a-zA-Z]?[\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="fckeditor">
                            <FCKeditorV2:FCKeditor OnPreRender="welcome_text_PreRender" ID="welcome_text" runat="server" OnLoad="welcome_text_Load">
                            </FCKeditorV2:FCKeditor>
                        </div>
                        <div id="msg" runat="server" class="error"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset style="text-align: left; width: 510px">
                            <legend><span style="color: Black">Create password</span></legend>
                            <table class="table-full">
                                <tbody>

                                    <tr>
                                        <td>Password*
                                            <input id="sault" type="hidden" name="sault" runat="server" />
                                        </td>
                                        <td>
                                            <script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                                            <asp:TextBox ID="txtUserPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                                                runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;</td>
                                        <td>
                                            <div style="width: 100%; color: red" id="dvPwd">
                                            </div>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" SetFocusOnError="true" ForeColor="Red"
                                                ControlToValidate="txtUserPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                                                ValidationGroup="MngUsrVCG" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <span id="Words"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Confirm&nbsp;password*</td>
                                        <td>
                                            <asp:TextBox ID="txtCPwd" runat="server" TextMode="Password"></asp:TextBox>&nbsp;
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" Enabled="false" ForeColor="Red"
                                                Display="Dynamic" ErrorMessage="please enter confirm password" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" ForeColor="Red"
                                                Display="Dynamic" ErrorMessage="password & confirm password must match" ValidationGroup="MngUsrVCG" ControlToCompare="txtUserPwd"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 162px">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text="Update" CssClass="btnBgClass" ValidationGroup="MngUsrVCG"></asp:Button></td>
                                    <td>
                                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel"
                                            CssClass="btnBgClass"></asp:Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="pwd" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="HdnUID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="HdnDomain" runat="server"></asp:HiddenField>
</asp:Content>

