﻿using System;
using System.Data;
using System.Net;
using System.Text;
using System.Web.UI;

public partial class DigipSuperAdmin_CacheRemove : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + "Cache";
    }

    protected void btnWidgetChannelsList_Click(object sender, EventArgs e)
    {
        string strSqlDomains = "SELECT DomainName FROM brand_domainname where DomainName not in ('demo.digipublishing.net','demoopen.digipublishing.net','democlosed.digipublishing.net')";
        DataTable dtdomains = dp.FillDataTable(strSqlDomains);
        StringBuilder SB = new StringBuilder();
        if (dtdomains.Rows.Count > 0)
        {
            foreach (DataRow dr in dtdomains.Rows)
            {
                SB.Append(Convert.ToString(dr["DomainName"]) + ",");
            }
            ClerBSWidgetChannelsCache(SB.ToString().TrimEnd(','));
        }
    }

    protected void btnAd_Networks_Click(object sender, EventArgs e)
    {
        try
        {
            string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=adnetworkscache";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=adnetworkscache";
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch{ }
    }

    protected void btnWidgets_Click(object sender, EventArgs e)
    {
        StringBuilder SBDomain = new StringBuilder();
        string strSqlDomains = "SELECT DomainName FROM brand_domainname where DomainName not in ('demo.digipublishing.net','demoopen.digipublishing.net','democlosed.digipublishing.net')";
        DataTable dtdomains = dp.FillDataTable(strSqlDomains);
        if (dtdomains.Rows.Count > 0)
        {
            foreach (DataRow dr in dtdomains.Rows)
            {
                SBDomain.Append(Convert.ToString(dr["DomainName"]) + ",");
            }
        }
        StringBuilder SBIdPosition = new StringBuilder();
        string strSqlIdPosition = "SELECT a.id, b.Position FROM Brand_AppLists AS a INNER JOIN Brand_AppListRuleSettings AS b ON a.id = b.AppList_ID"
                                + "  where Position not in ('topbanner','pop_under','none') AND Position IS NOT NULL AND Position<>''";
        DataTable dtPositions = dp.FillDataTable(strSqlIdPosition);
        if (dtPositions.Rows.Count > 0)
        {
            foreach (DataRow dr in dtPositions.Rows)
            {
                SBIdPosition.Append(Convert.ToString(dr["id"]) + "," + Convert.ToString(dr["Position"]) + ",");
            }
        }
        if (Convert.ToString(SBIdPosition).TrimEnd(',') != "")
        {
            try
            {
                string url4 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitenottopsuperadmin&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=abc";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url4);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string url5 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitenottopsuperadmin&Cachekey3=WidgetAllPosition&Cachekey4=WidgetRandomAllPosition&IDposition=abc&domainurl=abc";
                HttpWebRequest request5 = (HttpWebRequest)WebRequest.Create(url5);
                HttpWebResponse response5 = (HttpWebResponse)request5.GetResponse();
            }
            catch{ }
        }

        try
        {
            string url0 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=widgetscacheofasitetopsuperadmin&Cachekey1=WidgetTop&domainurl=" + Convert.ToString(SBDomain).TrimEnd(',');
            HttpWebRequest request0 = (HttpWebRequest)WebRequest.Create(url0);
            HttpWebResponse response0 = (HttpWebResponse)request0.GetResponse();
            string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=widgetscacheofasitetopsuperadmin&Cachekey1=WidgetTop&domainurl=demo.digipublishing.net,demoopen.digipublishing.net,democlosed.digipublishing.net";
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch{ }
    }

    protected void btnArticles_Click(object sender, EventArgs e)
    {
        string strSqlDomains = "SELECT DomainName FROM brand_domainname where DomainName not in ('demo.digipublishing.net','demoopen.digipublishing.net','democlosed.digipublishing.net')";
        DataTable dtdomains = dp.FillDataTable(strSqlDomains);
        StringBuilder SBDomain = new StringBuilder();
        if (dtdomains.Rows.Count > 0)
        {
            foreach (DataRow dr in dtdomains.Rows)
            {
                SBDomain.Append(Convert.ToString(dr["DomainName"]) + ",");
            }
        }
        if (Convert.ToString(SBDomain).TrimEnd(',') != "")
        {
            try
            {
                string url = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=articlecache&BSDomains=demo.digipublishing.net,demoopen.digipublishing.net,democlosed.digipublishing.net";
                HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string url1 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=articlecache&BSDomains=" + Convert.ToString(SBDomain).TrimEnd(',');
                HttpWebRequest request1 = (HttpWebRequest)System.Net.WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch{ }
        }
    }

    private void ClerBSWidgetChannelsCache(string BSSiteNames)
    {
        try
        {
            string url = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=BSWidgetChannelsCache&BSDomains=demo.digipublishing.net,demoopen.digipublishing.net,democlosed.digipublishing.net";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string url1 = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=BSWidgetChannelsCache&BSDomains=" + BSSiteNames;
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(url1);
            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
        }
        catch{ }
    }
}