﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ViewSummaryStastics : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Summary View Statistics";

        if (!IsPostBack)
        {
            GetDomainDataBind("");
            bindSiteGenre();
        }
    }

    private void GetDomainDataBind(string strSiteGenre)
    {
        dgrdDomain.DataSource = getDT(strSiteGenre);
        dgrdDomain.DataBind();
    }

    private DataTable getDT(string strSiteGenre)
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT id,domainname,count(Channel_PlayList_id) AS PlayListItems "
                 + " FROM brand_domainname LEFT JOIN select_channel "
                 + " ON brand_domainname.id=select_channel.SiteID LEFT JOIN Channel_PlayList "
                 + " ON Channel_PlayList.channel_id=select_channel.channel_id "
                 + " WHERE status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "' ";

        if (strSiteGenre != "")
            strSql += " AND domain_genre=" + strSiteGenre;
        
        strSql += " GROUP BY id,domainname";

        dt = dp.FillDataTable(strSql);
        dt.Columns.Add("ActivePosts", typeof(int));
        dt.Columns.Add("InActivePosts", typeof(int));
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    DBHelper dpsite = ReadConfig.getDomainDBHelper(Convert.ToString(dr["domainname"]).Trim());
                    string strSqlPosts = "SELECT (SELECT Count(*) FROM BRAND_BLOG_POSTS Where Active_Inactive=1) AS Active,"
                                         + "(SELECT Count(*) FROM BRAND_BLOG_POSTS Where Active_Inactive=0) AS Inactive";
                    DataTable dtPosts = dpsite.FillDataTable(strSqlPosts);
                    if (dtPosts.Rows.Count > 0)
                    {
                        dr["ActivePosts"] = dtPosts.Rows[0]["Active"];
                        dr["InActivePosts"] = dtPosts.Rows[0]["Inactive"];
                    }
                }
                catch { }
            }
        }
        Session["summaryOndemandActivity"] = dt;
        return dt;
    }

    private void bindSiteGenre()
    {
        string strSql = "select ID as value, genre_name as name from brand_sitegenre";
        DataTable dt = dp.FillDataTable(strSql);
        ddlSiteGenre.DataSource = dt;
        ddlSiteGenre.DataTextField = "name";
        ddlSiteGenre.DataValueField = "value";
        ddlSiteGenre.DataBind();
        ddlSiteGenre.Items.Insert(0, new ListItem("--Select--", "-1"));
        ddlSiteGenre.SelectedValue = "-1";
    }
    protected void ddlSiteGenre_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.ddlSiteGenre.SelectedValue != "-1")
        {
            GetDomainDataBind(this.ddlSiteGenre.SelectedValue);
        }
        else
        {
            GetDomainDataBind("");
        }
    }
    protected void dgrdDomain_OnItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Login")
        {
            string strDomainName = Convert.ToString(e.CommandArgument);
            DBHelper dp1 = ReadConfig.getDomainDBHelper(strDomainName);
            string strLoginGUID = Convert.ToString(System.Guid.NewGuid());
            string sqlQueryLoginGUID = "insert into Brand_SuperAdmin_Login(LoginGUID)values('" + strLoginGUID + "')";
            dp1.ExecuteNonQuery(sqlQueryLoginGUID);
            string strLoginUrl = "http://" + Convert.ToString(e.CommandArgument) + "/DigipSuperAdmin/agencyLogin.aspx?LoginGuid=" + strLoginGUID;
            string fullURL = "window.open('" + strLoginUrl + "', '_blank');";
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        }
    }
    protected void dgrdDomain_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = (DataTable)Session["summaryOndemandActivity"];
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }
    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        { 
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }
    /// <summary>//
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }
}