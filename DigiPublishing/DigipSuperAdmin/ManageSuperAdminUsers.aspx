﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="ManageSuperAdminUsers.aspx.cs" Inherits="DigipSuperAdmin_ManageSuperAdminUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../JS/Password.js"></script>
    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
    <div style="vertical-align: top;">
        <h4>Manage Users</h4>
    </div>
    <asp:MultiView ID="mView" runat="server">
        <asp:View ID="vGrid" runat="server">
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:LinkButton ID="lnkbtnAdd" runat="server" Text="Add New" OnClick="lnkbtnAdd_OnClick" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgrdUsers" runat="server" OnEditCommand="dgrdUsers_EditCommand" GridLines="None"
                            AutoGenerateColumns="False" OnItemDataBound="dgrdUsers_ItemDataBound" OnDeleteCommand="dgrdUsers_DeleteCommand">
                            <Columns>
                                <asp:BoundColumn DataField="uid" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fullname" HeaderText="Name" SortExpression="fullname"></asp:BoundColumn>
                                <asp:BoundColumn DataField="roles" HeaderText="Role" SortExpression="roles"></asp:BoundColumn>
                                <asp:BoundColumn DataField="email" HeaderText="Email"></asp:BoundColumn>
                                <asp:BoundColumn DataField="user_ip" HeaderText="Last Login IP" />
                                <asp:ButtonColumn CommandName="Edit" Text="Edit"></asp:ButtonColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDel" runat="server" Text="Delete" CommandName="Delete" CssClass="toplink" OnClientClick="return confirm('Are you sure you wish to delete?')"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vAddEdit" runat="server">
            <table>
                <tr>
                    <td>Name*</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                        <div style="color: red" id="Div1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtname" Display="Dynamic" ErrorMessage="please enter name"
                                ValidationGroup="MngUsrVCG">
                        </asp:RequiredFieldValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Username*</td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" MaxLength="15"></asp:TextBox>
                        <div style="color: red" id="dvUserName">
                            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" SetFocusOnError="true" ForeColor="red"
                                ControlToValidate="txtUserName" Display="Dynamic" ErrorMessage="please enter username"
                                ValidationGroup="MngUsrVCG">
                        </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revUserName" runat="server" SetFocusOnError="true" ForeColor="red"
                                ControlToValidate="txtUserName" Display="Dynamic" ErrorMessage="only alphanumeric characters are allowed"
                                ValidationGroup="MngUsrVCG" ValidationExpression="[a-zA-Z0-9_.]*">
                    </asp:RegularExpressionValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Email*</td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>&nbsp;                                       
                   
                        <div class="error" id="dvCheckEmail" runat="server"></div>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" SetFocusOnError="true"
                            ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="please enter email-id" ForeColor="Red"
                            ValidationGroup="MngUsrVCG">
                    </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" SetFocusOnError="true" ForeColor="red"
                            ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="invalid e-mail address! please re-enter"
                            ValidationGroup="MngUsrVCG" ValidationExpression="^[a-zA-Z]?[\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$">
                    </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>Access Permissions</td>
                    <td>
                        <asp:CheckBoxList ID="ChkbxlstAccessPermiossions" CssClass="select" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset style="text-align: left; width: 510px">
                            <legend><span style="color: Black">Create password</span></legend>
                            <table class="table-full">
                                <tbody>

                                    <tr>
                                        <td>Password*                                       
                                        <input id="sault" type="hidden" name="sault" runat="server" />
                                        </td>
                                        <td>
                                            <script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                                            <asp:TextBox ID="txtPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                                                runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;</td>
                                        <td>
                                            <div style="color: red" id="dvPwd">
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtPwd" ForeColor="red"
                                                Display="Dynamic" ErrorMessage="please enter password" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revPWD" runat="server" SetFocusOnError="true" ForeColor="red"
                                                ControlToValidate="txtPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                                                ValidationGroup="MngUsrVCG" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <span id="Words"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Confirm&nbsp;password*</td>
                                        <td>
                                            <asp:TextBox ID="txtCPwd" runat="server" TextMode="Password"></asp:TextBox>&nbsp;</td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" ForeColor="red"
                                                Display="Dynamic" ErrorMessage="please enter confirm password" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" ForeColor="red"
                                                Display="Dynamic" ErrorMessage="password & confirm password must match" ValidationGroup="MngUsrVCG"
                                                ControlToCompare="txtPwd"></asp:CompareValidator></td>
                                    </tr>
                                </tbody> 
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCreate" OnClick="btnCreate_Click" runat="server" Text="Create" ValidationGroup="MngUsrVCG" CausesValidation="true"></asp:Button></td>
                    <td>
                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel"></asp:Button></td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="pwd" runat="server"></asp:HiddenField>
</asp:Content>

