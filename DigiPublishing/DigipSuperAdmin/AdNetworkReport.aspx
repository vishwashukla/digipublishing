﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingSuperAdmin.master" Theme="admin" AutoEventWireup="true" CodeFile="AdNetworkReport.aspx.cs" Inherits="DigipSuperAdmin_AdNetworkReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="Datepicker.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.js"></script>
    <script type="text/javascript">
        <%--$(function () {
            $('#<%=txtDate.ClientID%>').datepicker({
                minDate: parseInt('<%=MaxDays %>'),
                maxDate: 0,
            }).datepicker('setDate', new Date());
        });--%>

        function confirmSubmit() {
            if (confirm("Are you want to continue?")) {
                document.getElementById('<%=btnRecorddata.ClientID %>').click();
                return true;
            }
            else {
                return false;
            }
        }

        function confirmSubmitEmail() {
            if (confirm("Are you want to continue?")) {
                document.getElementById('<%=btnEmailReport.ClientID %>').click();
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <div style="vertical-align: top;">
        <h4>Ad Network Report</h4>
        <div style="display:none;">
            <asp:Button ID="btnRecorddata" runat="server" OnClick="btnRecorddata_Click" />
            <asp:Button ID="btnEmailReport" runat="server" OnClick="btnEmailReport_Click" />
        </div>
        <table style=" width:100%;">
            <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                </td>
                <td style="text-align: right;">
                    <asp:CheckBox ID="chkAllowAdnetworkData" runat="server" Text="Store ad network data" onclick="return confirmSubmit()" TextAlign="Right" />&nbsp;&nbsp;
                    <%--&nbsp;&nbsp;|&nbsp;&nbsp;<b>Date: </b><asp:TextBox ID="txtDate" runat="server"></asp:TextBox><b> (mm/dd/yyyy)</b>--%>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    <asp:Button ID="btnPurge" runat="server" Text="Purge Results" OnClientClick="return confirm('Are you sure to perform this operation?');" OnClick="btnPurge_Click"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="right">
                    <table>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Current server date time:
                            </td>
                            <td style="text-align: right; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblCurrentServerDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: 11px;">Last purge server date time:
                            </td>
                            <td style="text-align: left; padding-left: 5px; font-size: 11px;">
                                <asp:Label ID="lblLastPurgeDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Max Days To Store Data</b>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtMaxDays" runat="server" ValidationGroup="VGMaxDays"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtMaxDays" runat="server" Display="Dynamic" ControlToValidate="txtMaxDays"
                     ErrorMessage="required" ForeColor="red" ValidationGroup="VGMaxDays"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" id="rexNumber" controltovalidate="txtMaxDays" validationexpression="^[0-9]$" 
                    errormessage="Please enter a integer value." Display="Dynamic" ValidationGroup="VGMaxDays" />
                    <asp:Button ID="btnSaveMaxDays" runat="server" Text="Save" ValidationGroup="VGMaxDays" OnClick="btnSaveMaxDays_Click" />&nbsp;&nbsp;
                    <asp:CheckBox ID="chkAllowEmailReports" runat="server" Text="Allow Email Reports" onclick="return confirmSubmitEmail()" TextAlign="Right" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DataGrid ID="dgrdAdNetworkReport" runat="server" AllowSorting="true"
                      AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" HorizontalAlign="Center"
                        GridLines="Both" Width="100%" OnSortCommand ="dgrdAdNetworkReport_SortCommand"
                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundColumn HeaderText="Ad Network" DataField="network_name" SortExpression="network_name"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Impressions" DataField="TotImpressions" SortExpression="TotImpressions"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Ad Impressions" DataField="TotAdImpressions" SortExpression="TotAdImpressions"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Ad Start" DataField="TotAdStart" SortExpression="TotAdStart"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Ad Completed" DataField="TotAdCompleted" SortExpression="TotAdCompleted"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Clicks" DataField="TotClicks" SortExpression="TotClicks"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="VTR" SortExpression="VTR">
                                <ItemTemplate>
                                    <asp:Label ID="lblVTR" runat="server" Text='<%# Eval("VTR") %>'></asp:Label>%
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CTR on Imp" SortExpression="CTRImp">
                                <ItemTemplate>
                                    <asp:Label ID="lblCTRImp" runat="server" Text='<%# Eval("CTRImp") %>'></asp:Label>%
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CTR on Ad Start" SortExpression="CTRAdSt">
                                <ItemTemplate>
                                    <asp:Label ID="lblCTRAdSt" runat="server" Text='<%# Eval("CTRAdSt") %>'></asp:Label>%
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Fill Rate" SortExpression="Fillrate">
                                <ItemTemplate>
                                    <asp:Label ID="lblFillrate" runat="server" Text='<%# Eval("Fillrate") %>'></asp:Label>%
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>       
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    
        <div>
           <br />
           <br />
           Ad Network Report<br />
           Page Impressions:
           <br />
           Sum of page load for banner/videoAdNetwork
           <br />
           [An entry is recorded when banner/videoAdNetwork loads on page]<br />
           <br />
           Ad Impressions:
           <br />
           Sum of AdImpression for videoAdNetwork
           <br />
           [An entry is recorded at onImpressionEvent for VAST, at onAdFound for Acudeo and
           at AdImpression for VPAID]<br />
           <br />
           Ad Start:
           <br />
           Sum of Adstart for videoAdNetwork
           <br />
           [An entry is recorded when videoAdNetwork starts]<br />
           <br />
           Ad Completed:
           <br />
           Sum of AdCompleted for videoAdNetwork
           <br />
           [An entry is recorded when videoAdNetwork completes, banners do not show here]<br />
           <br />
           Clicks:
           <br />
           Sum of unblocked clicks on Banner / VideoAdNetwork<br />
           [An entry is recorded when user clicks on banner / videoAdNetwork until that is
           unclick able]<br />
           <br />
           VTR – View Through Rate:
           <br />
           (TotAdCompleted / TotAdStart)*100<br />
           [Banners do not show here]<br />
           <br />
           CTR on Imp:
           <br />
           For VideoAdNetwork is (TotClicks / TotAdImpressions)*100
           <br />
           For Banner is (TotClicks / TotImpressions)*100
           <br />
           <br />
           CTR on Ad Start:
           <br />
           (TotClicks / TotAdStart)*100
           <br />
           [Banners do not show here]<br />
           <br />
           Fill Rate:
           <br />
           (TotAdStart / TotAdImpressions)*100<br />
           [Banners do not show here]
       </div>
    </div>
</asp:Content>

