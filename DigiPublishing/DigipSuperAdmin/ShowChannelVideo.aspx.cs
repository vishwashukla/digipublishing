﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;

public partial class DigipSuperAdmin_ShowChannelVideo : System.Web.UI.Page
{
    public string strplaychannel = "";
    public string strflashchannel = "";
    public string strchanneltype = "";
    public string MYURL = "";
    public string MyFileURL = "";
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, System.EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - ShowChannelVideo";
        StringBuilder strHtml = new StringBuilder();
        
        int channelid = 0;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["id"]).Trim()))
            channelid = Convert.ToInt32(Request.QueryString["id"].Trim());

        if (channelid != 0)
        {
            DataTable dtChannel = dp.FillDataTable("Select channel_id,region_id,country_id,stream_url,email,channel_name,channel_desc"
                                + ",channel_URL,format,channel_logo_path,status,entry_date,system_ip"
                                + ",LanguageID,ChannelContent,Channelformat"
                                + ",ISNULL(PlaylistRandom,0) As PlaylistRandom,ISNULL(Playlist_Alternate,0) as Playlist_Alternate"
                                + ",ISNULL(CNBC_MRSS,0) CNBC_MRSS,ISNULL(ShowPlayList,0) as ShowPlayList"
                                + ",ISNULL(showRTMP,0) as showRTMP,RTMPFile from Channel Where channel_id=" + channelid);
            if (dtChannel.Rows.Count > 0)
            {
                string strFormat = "";
                string Channel_streamUrl = "";

                strHtml.AppendLine("<table width=\"100%\" border=\"0\" class=\"\">");
                strHtml.AppendLine("<tr>");
                strFormat = Convert.ToString(dtChannel.Rows[0]["format"]).ToLower().Trim();
                if (strFormat.IndexOf("flash") > -1 || strFormat.IndexOf("flv") > -1)
                    strFormat = "FLASH";

                Channel_streamUrl = Convert.ToString(dtChannel.Rows[0]["stream_url"]);
                if (Convert.ToString(dtChannel.Rows[0]["ShowPlayList"]) == "1" && Convert.ToInt32(dtChannel.Rows[0]["Playlist_Alternate"]) != 1)
                {
                    Channel_streamUrl = "http://ondemand.imello.com/DigipSuperAdmin/Getplaylist.aspx?Id=" + channelid;
                    if (Request.Url.Host.ToLower() == "localhost")
                    {
                        Channel_streamUrl = "http://" + Request.Url.Host + ":" + Request.Url.Port + "/DigipSuperAdmin/Getplaylist.aspx?Id=" + channelid;
                    }
                }

                strHtml.AppendLine("<td width=\"425\" valign=\"top\">");
                //here starts the new code
                strHtml.AppendLine("<a name='" + channelid + "' href=\"" + Convert.ToString(dtChannel.Rows[0]["channel_URL"]) + "\" target=\"_Blank\" rel='nofollow'><strong id='nameele'>" + Convert.ToString(dtChannel.Rows[0]["channel_name"]) + "</strong>&nbsp;</a></br>");
                //here below line is commented on Dec8 for to move down side people watch
                strHtml.AppendLine("<table border=\"0\">");
                strHtml.AppendLine("<tr>");
                strHtml.AppendLine("<td colspan=2 valing='top' align='center'><div id=\"MovieClip1\">");
                
                if (strFormat == "FLASH")
                {
                    strHtml.AppendLine("<script>var Channel_streamUrl='" + Channel_streamUrl + "';</script>");
                    strHtml.AppendLine("<div id=\"mainContainer\"><div id=\"wrapper\"></div></div>");
                }
                strHtml.AppendLine(strplaychannel);
                strplaychannel = "";
                strHtml.AppendLine("</div></td>");
                strHtml.AppendLine("</tr>");
                strHtml.AppendLine("<tr><td>&nbsp;</td></tr>");
                strHtml.AppendLine("</td></tr>");
                strHtml.AppendLine("</table>");
                strHtml.AppendLine("</br>");
                strHtml.AppendLine("</td>");
                strHtml.AppendLine("<td valign=\"top\" style=\"padding-top:0px\">");

                strHtml.AppendLine("<table border=\"0\" class=\"theight\">");

                strHtml.AppendLine("<tr><td><strong>Description:</strong></td></tr><tr><td><br></td></tr>");
                strHtml.AppendLine("<tr><td valign='top'><textarea id='txtdesc' name='txtdesc' style='height:120px' >" + Convert.ToString(dtChannel.Rows[0]["channel_desc"]).Trim() + "</textarea></td></tr>");
                strHtml.AppendLine("</table>");
                strHtml.AppendLine("</td>");
                strHtml.AppendLine("</tr></table>");
                strHtml.AppendLine("<div>");
                strHtml.AppendLine("</div>");
            }
            else
            {
                strHtml.AppendLine("<td align='center' valign=\"Middle\" colspan=\"2\">");
                strHtml.AppendLine("<B>The requested channel " + Request.QueryString["id"] + " has not been found</B>");
                strHtml.AppendLine("</td>");
            }
        }
        myclip.InnerHtml = Convert.ToString(strHtml);
    }

    protected void btnApprove_Click(object sender, System.EventArgs e)
    {
        Action(1);
    }

    protected void btnDecline_Click(object sender, System.EventArgs e)
    {
        Action(0);
    }

    private void Action(int status)
    {
        try
        {
            int id = 0;
            if (!string.IsNullOrWhiteSpace(Request.QueryString["id"]))
            {
                id = Convert.ToInt32(Convert.ToString(Request.QueryString["id"]).Trim());
            }
            if (id > 0)
            {
                string strdesc = "";
                if (!string.IsNullOrWhiteSpace(Request.Form["txtdesc"].Trim()))
                {
                    strdesc = Request.Form["txtdesc"].Trim().Replace("'", "''");
                }
                string sqlstr = "EXEC [dbo].[SP_Update_Channel_Status] @Status=" + status + ",@Desc='" + strdesc + "',@ChannelID=" + id;
                dp.ExecuteNonQuery(sqlstr);
                utils.ClearSitesSavedChannelCache(id);
                int newid = 0;
                newid = getExistingChannel(id);
                if (newid > 0)
                {
                    Response.Redirect("ShowChannelVideo.aspx?id=" + newid);
                }
                else
                {
                    myclip.InnerHtml = "channel testing has been completed.please select new page";
                    btnApprove.Visible = false;
                    btnDecline.Visible = false;
                }
            }
        }
        catch
        {
        }
    }

    private int getExistingChannel(int cid)
    {
        int channelID = 0;
        ArrayList myChannels = new ArrayList();
        string sqlstr = Convert.ToString(Session["query"]);
        DataTable dt = dp.FillDataTable(sqlstr);

        for(int i=0;i<dt.Rows.Count;i++)
        {
            if (i == dt.Rows.Count - 1)
                channelID = 0;
            else
            {
                if (cid == Convert.ToInt32(Convert.ToString(dt.Rows[i]["channelid"])))
                {
                    channelID = Convert.ToInt32(Convert.ToString(dt.Rows[i + 1]["channelid"]));
                    break;
                }
            }
        }

        int cpos = myChannels.IndexOf(cid);
        if (cpos == myChannels.Count - 1)
        {
            channelID = 0;
        }
        else
        {
            cpos = cpos + 1;
            channelID = Convert.ToInt32(Convert.ToString(myChannels[cpos]));
        }
        return channelID;
    }
}