﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DigipSuperAdmin_ManagePrivacyAndTerm : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Privacy & Term";
        lblMsg.Text = "";
        if (!IsPostBack)
        {
            BindPrivacy();
        }
    }

    private void BindPrivacy()
    {
        dgrdPrivacy.DataSource = GetDomains();
        dgrdPrivacy.DataBind();
        mvManagePrivacy.SetActiveView(viewPrivacy);
    }

    private DataTable GetDomains()
    {
        DataTable dt = new DataTable();
        string strSql = "SELECT id,domainname,(Select genre_name from brand_sitegenre where ID=brand_domainname.domain_genre) as genre FROM brand_domainname where status<>2 order by domainname";
        dt = dp.FillDataTable(strSql);
        return dt;
    }

    protected void dgrdPrivacy_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Term" || e.CommandName == "Privacy")
            {
                ShowItem(e.Item.Cells[1].Text, e.CommandName.ToString().Trim());
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    private void ShowItem(string DomainName,string Type)
    {
        try
        {
            SetFCKPath();
            resetControl();
            lblPrivactTerm.Text = Type;
            string path = Server.MapPath(@"~/privacyterm/" + DomainName.ToLower() + "/" + Type.ToLower().Trim() + ".html");
            if (File.Exists(path))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                string privactFileContents = File.ReadAllText(Server.MapPath(@"~/privacyterm/" + DomainName.ToLower() + "/" + Type.ToLower().Trim() + ".html"));
                this.FCKPrivacy.Value = privactFileContents;
            }
            else
            {
                if (!Directory.Exists(Server.MapPath(@"~/privacyterm/")))
                {
                    Directory.CreateDirectory(Server.MapPath(@"~/privacyterm/"));
                }
                if (!Directory.Exists(Server.MapPath(@"~/privacyterm/" + DomainName.ToLower() + "/")))
                {
                    Directory.CreateDirectory(Server.MapPath(@"~/privacyterm/" + DomainName.ToLower() + "/"));
                }
                File.Create(path);
            }
            this.lblDomainName.Text = DomainName;
            mvManagePrivacy.SetActiveView(viewEditPrivacy);
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetControl();
        mvManagePrivacy.SetActiveView(viewPrivacy);
    }

    private void resetControl()
    {
        lblDomainName.Text = "";
        FCKPrivacy.Value = "";
        lblPrivactTerm.Text = "";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            string Type = lblPrivactTerm.Text;
            System.IO.File.WriteAllText(Server.MapPath(@"~/privacyterm/" + lblDomainName.Text.ToLower() + "/" + Type.ToLower().Trim() + ".html"), this.FCKPrivacy.Value);
            this.lblMsg.Text = Type + " successfully updated.";
            BindPrivacy();
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void dgrdPrivacy_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        DataView dv = new DataView(GetDomains());
        SortField = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)source;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return Convert.ToBoolean(o);
        }
        set { ViewState["Sortascending"] = value; }
    }

    private void SetFCKPath()
    {
        string strPath = Request.ApplicationPath;
        if (strPath == "/")
        {
            FCKPrivacy.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
        }
        else
        {
            FCKPrivacy.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
        }
    }

    protected void FCKPrivacy_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedField(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this,
                                                    FCKPrivacy.GetType(),
                                                    "FCKUpdater",
                                                    script,
                                                    true);
            ScriptManager.RegisterOnSubmitStatement(this,
                                                    FCKPrivacy.GetType(),
                                                    "WebContentManagerEditorScript_" + this.FCKPrivacy.ClientID,
                                                    "FCKUpdateLinkedField('" + this.FCKPrivacy.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKPrivacy.GetType(),
                                                        "FCKUpdater",
                                                        script,
                                                        true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKPrivacy.GetType(),
                                                             "WebContentManagerEditorScript_" + this.FCKPrivacy.ClientID,
                                                             "FCKUpdateLinkedField('" + this.FCKPrivacy.ClientID + "');");
        }
    }

    protected void FCKPrivacy_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.FCKPrivacy.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, FCKPrivacy.GetType(), "FCKSource", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, FCKPrivacy.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKPrivacy.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKPrivacy.GetType(), "FCKSource", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKPrivacy.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKPrivacy.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
    }
}