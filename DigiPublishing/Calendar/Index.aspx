﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Calendar_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <div id="content-full">
        <h3>
            <asp:Label ID="lblCalendar" runat="server"></asp:Label></h3>
        <asp:Label ID="lblNoFeedSaved" CssClass="messages" runat="server" Visible="false" Text="No Calendar has been added by you yet..." />
        <asp:Label ID="lblmessage" runat="server" Text="" CssClass="error"></asp:Label>
        <div style="display: inline;">
            <asp:DataList ID="dlstCalendar" runat="server" ShowHeader="false" ShowFooter="false">
                <ItemTemplate>
                    <p> 
                        <strong>
                            <a id="lnkURL" target="_blank"><%# Eval("SUMMARY")%></a>
                        </strong>
                    </p>
                    <p>
                        <%# Eval("DTSTART")%> &nbsp;-&nbsp;<%# Eval("DTEND")%>
                    </p>
                    <p> 
                        <%# Eval("DESCRIPTION")%>
                    </p>
                </ItemTemplate>
            </asp:DataList>

            <asp:Panel ID="pnlPagingMain" runat="server">
                <div id="dlPaging" runat="server"></div>
                <div style="height: 30px; text-align: center; margin: 10px 0 0 0;">
                    <asp:Label ID="lblPageInfo" Font-Bold="true" Font-Size="11pt" runat="server"></asp:Label>
                </div>
            </asp:Panel>
            <div style="display: none;">
                <asp:Button ID="btnPaging" runat="server" OnClick="btnPaging_click" />
            </div>
            <asp:HiddenField ID="hdnPageindex" runat="server" />

        </div>

    </div>
    <script type="text/javascript">
        function PagingClick(indexpage) {
            document.getElementById("<%=hdnPageindex.ClientID %>").value = indexpage;
             document.getElementById("<%=btnPaging.ClientID %>").click();
         }
    </script>
</asp:Content>

