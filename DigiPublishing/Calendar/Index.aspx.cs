﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Calendar_Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Calendar ";

        lblmessage.Text = "";
        if (!utils.CheckFeature((int)features.Calendar))
                Response.Redirect("~/OndemandHome.aspx");
        if (!IsPostBack)
        {
            if (!string.IsNullOrWhiteSpace(Request["caltitle"]))
            {
                ShowCalendar(Convert.ToString(Request.QueryString["caltitle"]).TrimEnd('.').Replace('-', ' '));
            }
            else
                ShowAllCalendar();
        }
    }
    private void ShowCalendar(string title)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string strquery= "select TOP 1 Cal_id from Brand_Calendar WHERE REPLACE(REPLACE(REPLACE(REPLACE(title,'&','and'),'-',' '),'  ',' '),'.',' ') LIKE '%' + '"+HttpUtility.HtmlDecode(title)+"' + '%' AND status<>'0' ";
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(strquery);
        if (dt.Rows.Count > 0)
        {
            showCalendarDetail(Convert.ToString(dt.Rows[0]["Cal_id"]));
        }
    }
    private void ShowAllCalendar() 
    {
        Session["dtAllCalendar"] = null;
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        try
        { 
            string strquery = "select cal_id,Brand_Calendar.title as title FROM Brand_Calendar left join dbo.brand_user on Brand_Calendar.c_by = brand_user.uid "
                            + "where Brand_Calendar.status<>'0'  ORDER BY brand_calendar.c_date desc";
            DataTable dtFeatured = new DataTable();
            dtFeatured = dp.FillDataTable(strquery);
            if (dtFeatured.Rows.Count > 0)
            {
                Session["dtAllCalendar"] = dtFeatured;
            }
            BindItemsList();
        }
        catch (Exception)
        {
            pnlPagingMain.Visible = false;
        }
    }
    private DataTable LoadCalendar(string URL)
    {
        DataTable dtCalendar = new DataTable();
        try
        {
            if (URL.Contains("webcal://"))
            {
                URL = URL.Replace("webcal://", "http://");
            }
            dtCalendar.Columns.Add(new DataColumn("SUMMARY", typeof(string)));
            dtCalendar.Columns.Add(new DataColumn("DESCRIPTION", typeof(string)));
            dtCalendar.Columns.Add(new DataColumn("DTSTART", typeof(DateTime)));
            dtCalendar.Columns.Add(new DataColumn("DTEND", typeof(DateTime)));
            dtCalendar.Columns.Add(new DataColumn("DTSTART_TIME", typeof(string)));
            dtCalendar.Columns.Add(new DataColumn("DTEND_TIME", typeof(string)));
            //Create a WebRequest and WebResponse for get the Details.
            XmlDocument xd = new XmlDocument();
            HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(URL);
            HttpWebResponse HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();

            vCardReader vCR = new vCardReader();

            StreamReader myReader = new StreamReader(HttpWResp.GetResponseStream());
            string strFileData = myReader.ReadToEnd();
            string[] strArrFieldvalues = Regex.Split(strFileData.Trim(), "END:VEVENT");
            foreach (string FieldValue in strArrFieldvalues)
            {
                DataRow drNew = dtCalendar.NewRow();
                if (FieldValue.Contains("SUMMARY"))
                {
                    vCR.ParseLines(FieldValue);
                    string strSUMMARY = vCR.Summary;
                    drNew["SUMMARY"] = vCR.Summary;
                }
                if (FieldValue.Contains("DESCRIPTION"))
                {
                    vCR.ParseLines(FieldValue);
                    string strDESCRIPTION = vCR.DESCRIPTION;
                    drNew["DESCRIPTION"] = vCR.DESCRIPTION;
                }
                if (FieldValue.Contains("DTSTART"))
                {
                    vCR.ParseLines(FieldValue);
                    ArrayList DtArrlst = new ArrayList();
                    string strDTSTART = vCR.DTSTART;
                    string strStart = "";
                    string strStartTime = "";

                    if (strDTSTART != "")
                    {
                        if (!strDTSTART.Contains("T"))
                        {
                            strDTSTART += "T000000";
                        }
                        DtArrlst = getDate(strDTSTART);
                        strStart = DtArrlst[0].ToString();
                        strStartTime = DtArrlst[1].ToString();
                    }
                    if (!string.IsNullOrWhiteSpace(strStart))
                        drNew["DTSTART"] = Convert.ToDateTime(strStart);
                    drNew["DTSTART_TIME"] = strStartTime;
                }
                if (FieldValue.Contains("DTEND"))
                {
                    vCR.ParseLines(FieldValue);

                    ArrayList DtArrlst = new ArrayList();
                    string strDTEND = vCR.DTEND;
                    string strEnd = "";
                    string strEndTime = "";

                    if (strDTEND != "")
                    {
                        if (!strDTEND.Contains("T"))
                        {
                            strDTEND += "T000000";
                        }
                         DtArrlst = getDate(strDTEND);
                        strEnd = DtArrlst[0].ToString();
                        strEndTime = DtArrlst[1].ToString();
                    }

                    if (!string.IsNullOrWhiteSpace(strEnd))
                        drNew["DTEND"] = Convert.ToDateTime(strEnd);
                    drNew["DTEND_TIME"] = strEndTime;
                }
                if ((FieldValue.Contains("SUMMARY") || FieldValue.Contains("DESCRIPTION") || FieldValue.Contains("DTSTART") || FieldValue.Contains("DTEND"))
                    && drNew["DTSTART"] != DBNull.Value && Convert.ToDateTime(drNew["DTSTART"]) >= DateTime.Now)
                {
                    dtCalendar.Rows.Add(drNew);
                }
            }
            HttpWResp.Close();
            HttpWResp = null;
            HttpWReq = null;
            myReader.Close();
            myReader = null;
        }
        catch (Exception ex) { throw ex; }
        return dtCalendar;
    }
    private ArrayList getDate(string strDate)
    {
        string[] strDTArr = strDate.Split('T');
        ArrayList retArr = new ArrayList();

        string strDt = "";
        string strTt = "";
        if (strDTArr[0].Length == 8)
        {
            strDt = strDTArr[0].Insert(4, "-");
            strDt = strDt.Insert(7, "-");
        }
        if (strDTArr[1] != "")
        {
            strTt = strDTArr[1].Substring(0, strDTArr[1].Length);
            if (strTt.Length == 6)
            {
                strTt = strTt.Insert(2, ":");
                strTt = strTt.Insert(5, ":");
            }
            else if (strTt.Length > 6)
            {
                strTt = strTt.Substring(0, 6);
                strTt = strTt.Insert(2, ":");
                strTt = strTt.Insert(5, ":");
            }
        }
        DateTime dtDate = DateTime.Parse(strDt + " " + strTt);
        retArr.Add(Convert.ToString(dtDate));
        retArr.Add(strTt);
        return retArr;
    }
    #region Pagination
    PagedDataSource _PageDataSource = new PagedDataSource();
    protected void BindItemsList()
    {
        if (Session["dtAllCalendar"] != null)
        {
            DataView dv = ((DataTable)Session["dtAllCalendar"]).DefaultView;
            if (dv != null)
            {
                DataTable dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    int startIndex = 1 * CurrentPage;
                    int endIndex = startIndex + 1;
                    if (endIndex > dt.Rows.Count)
                    {
                        endIndex = dt.Rows.Count;
                    }
                }
                if (dt.Rows.Count <= 1)
                {
                    pnlPagingMain.Visible = false;
                }
                _PageDataSource.DataSource = dv;
                _PageDataSource.AllowPaging = true;
                _PageDataSource.PageSize = 1;
                _PageDataSource.CurrentPageIndex = CurrentPage;
                ViewState["TotalPages"] = _PageDataSource.PageCount;
                this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + _PageDataSource.PageCount;
                showCalendarDetail(Convert.ToString((dt.Rows[CurrentPage]["cal_id"])));
                this.doPaging();
            }
        }
    }
    public void showCalendarDetail(string id)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string strquery = "select cal_id,Brand_Calendar.title,link,c_by from Brand_Calendar where cal_id='" + id + "' and status=1";
        DataTable dt = dp.FillDataTable(strquery);
        if (dt.Rows.Count > 0)
        {
            lblCalendar.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["title"]));
            LoadCalendardata(Convert.ToString(dt.Rows[0]["link"]));
            lblNoFeedSaved.Visible = false;
        }
        else
        {
            //no feed saved Yet....
            lblNoFeedSaved.Visible = true;
        }
    }
    private void LoadCalendardata(string URL)
    {
       try
      {
            DataTable dtCal = LoadCalendar(URL);

            if (dtCal.Rows.Count > 0)
            {

                DataView dv = new DataView(dtCal);
                dv.Sort = "DTSTART";
                    this.dlstCalendar.DataSource = dv;
                    this.dlstCalendar.DataBind();

                    this.dlstCalendar.Visible = true;
                    this.lblmessage.Visible = false;
            }
            else
            {
                this.lblmessage.Visible = true;
                this.lblmessage.Text = "Calendar not available!";
                this.dlstCalendar.Visible = false;
            }
       }
        catch
       {
          this.lblmessage.Visible = true;
            this.lblmessage.Text = "Calendar not available!";
            this.dlstCalendar.Visible = false;
        }
    }
    private int CurrentPage
    {
        get
        {
            object objPage = ViewState["_CurrentPage"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPage"] = value; }
    } 
    private void doPaging()
    {
        StringBuilder SBPaging = new StringBuilder();
        DataTable dtAllCal = new DataTable();
        if (Session["dtAllCalendar"] != null)
        {
            dtAllCal = (DataTable)Session["dtAllCalendar"];
        }
        if (dtAllCal.Rows.Count > 0)
        {
            SBPaging.AppendLine("<ul class='ulPagingFeeds'>");
            for (int i = 0; i < dtAllCal.Rows.Count; i++)
            {
                SBPaging.AppendLine("<li class='liPagingFeeds'>");
                if (Convert.ToString(i) == Convert.ToString(CurrentPage))
                {
                    SBPaging.Append("<a class='feedpaging-disabled'>" + Convert.ToString(dtAllCal.Rows[i]["title"]) + "</a>");
                }
                else
                {
                    SBPaging.Append("<a onclick='PagingClick(" + i + ")' class='feedpaging-enabled'>" + Convert.ToString(dtAllCal.Rows[i]["title"]) + "</a>");
                }
                SBPaging.Append("</li>");
            }
            SBPaging.AppendLine("</ul>");
        }
        dlPaging.InnerHtml = SBPaging.ToString();
    }
    protected void btnPaging_click(object source, EventArgs e)
    {
        CurrentPage = Convert.ToInt16(Convert.ToString(hdnPageindex.Value));
        BindItemsList();
    }
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}