﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Links_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <style>
        .tableBookmark tr>td:nth-child(1){
            position:relative;
        }
    </style>
    <div id="content-full">
        <asp:Label ID="lblNoSavedbookmark" runat="server" Text="No link saved." ForeColor="red"></asp:Label>
        <asp:DataGrid ID="dgrdBookmark" ShowHeader="false" AllowSorting="true" GridLines="None" runat="server" AutoGenerateColumns="false" CssClass="tableBookmark" OnItemDataBound="dgrdBookmark_ItemDataBound">
            <Columns>
                <asp:BoundColumn DataField="Id" Visible="False"></asp:BoundColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <p>
                            <asp:Image AlternateText="1x2" runat="server" ImageUrl="~/Images_Common/1x2.gif" ID="MainLinkBlockImage" />
                            <a target="_blank" id="MainLink_<%# Container.ItemIndex %>" style="cursor:pointer;" onclick="javascript:GetLinkData('MainLinkData_','MainCurrentClicksLink_','_MainSameIPClicksLink','_MainLinkBlockImage','MainLink_','<%# Convert.ToString(Eval("Id"))%>',<%# Container.ItemIndex%>)" href="<%# utils.formatStringForDBSelect(Convert.ToString(Eval("BK_URL")))%>"<%#utils.formatStringForDBSelect(Convert.ToString(Eval("rel")))%>>
                                <b><%# utils.formatStringForDBSelect(Convert.ToString(Eval("BK_Title")))%></b></a>
                            <span id="MainLinkData_<%# Container.ItemIndex %>" style="display:none;"><%# Convert.ToString(Eval("Blocker"))%>,<%# Convert.ToString(Eval("BlockerValue"))%>,<%# Convert.ToString(Eval("SameIP_Status"))%>,<%# Convert.ToString(Eval("SameIP_Value"))%>,<%# Convert.ToString(Cache["SiteId"])%></span>
                            <span id="MainCurrentClicksLink_<%# Container.ItemIndex %>" style="display:none;"></span>
                            <asp:Label ID="MainSameIPClicksLink" runat="server"></asp:Label>
                            <br>
                            <%#utils.formatStringForDBSelect(Convert.ToString(Eval("BK_Desc")))%>
                        </p>
                        <div class="seperate"></div>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <asp:Panel ID="pnlPagingMain" runat="server">
            <div style="text-align:center;width:100%;">
                <table style="display:inline;">
                    <tr>
                        <td style="text-align:right;">
                            <asp:LinkButton ID="lbtnPrevious" Font-Bold="true" runat="server" CausesValidation="false" OnClick="lbtnPrevious_Click">...</asp:LinkButton>
                            &nbsp;&nbsp;
                        </td>
                        <td style="text-align:right;vertical-align:middle;">
                            <asp:DataList ID="dlPaging" runat="server" RepeatDirection="Horizontal" OnItemCommand="dlPaging_ItemCommand" OnItemDataBound="dlPaging_ItemDataBound">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnPaging" Font-Bold="true" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="Paging" Text='<%# Eval("PageText") %>'></asp:LinkButton>&nbsp;            
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                        <td style="text-align:left;">
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lbtnNext" Font-Bold="true" runat="server" CausesValidation="false" OnClick="lbtnNext_Click">...</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:30px;text-align:left;vertical-align:middle;">
                            <asp:Label ID="lblPageInfo" Font-Bold="true" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

