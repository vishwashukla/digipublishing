﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Links_Index : System.Web.UI.Page
{
    PagedDataSource _PageDataSource = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Links ";

        if (!utils.CheckFeature((int)features.Bookmark))
            Response.Redirect("~/OndemandHome.aspx");

        if(!IsPostBack)
            BindGridBookmarks();

    }

    public void BindGridBookmarks()
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        try
        {
            DataTable dtBookmark = new DataTable();
            DBCommand cmdBookmark = new DBCommand();
            cmdBookmark.CommandType = CommandType.StoredProcedure;
            cmdBookmark.CommandText = "SP_Brand_Select_Links";
            //cmdBookmark.CommandText = "select *,CASE WHEN LEN(ISNULL(BK_Title,''))> 50 THEN SUBSTRING(BK_Title,1,50)+'..' END AS BK_Title,"
            //                        + " CASE WHEN LEN(ISNULL(BK_Desc,''))> 1000 THEN SUBSTRING(BK_Desc,1,1000)+'..' END AS BK_Desc"
            //                        + ",isnull(nofollow,0) as nofollow"
            //                        + ",isnull(noindex,0) as noindex"
            //                        + ",isnull(noopener,0) as noopener"
            //                        + ",Blocker,BlockerValue,SameIP_Status,SameIP_Value "
            //                        + " from brand_bookmark WHERE Status='True' AND BK_Status<>'0' order by BK_SortOrder desc";
            dtBookmark = dp.FillDataTable(cmdBookmark);
            DBCommand cmd = new DBCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select isnull(RandomiseLinksInLink,0) as RandomiseLinksInLink,(case when isnull(NumberOfLinksinlink,0)>0 then NumberOfLinksinlink "
                + "else (SELECT COUNT(Id) FROM  brand_bookmark WHERE Status='True' AND BK_Status<>'0') end) as NumberOfLinksinlink  FROM brand_settings";
            DataTable dt = dp.FillDataTable(cmd);
            if (dtBookmark.Rows.Count > 0)
            {
                if (!dtBookmark.Columns.Contains("rel"))
                    dtBookmark.Columns.Add("rel");
                foreach (DataRow dr in dtBookmark.Rows)
                {
                    int nofollow = Convert.ToInt32(dr["nofollow"].ToString());
                    int noindex = Convert.ToInt32(dr["noindex"].ToString());
                    int noopener = Convert.ToInt32(dr["noopener"].ToString());
                    int noreferrer = Convert.ToInt32(dr["noreferrer"].ToString());//RK
                    string strrel = " rel=\"";
                    if (nofollow != 0)
                    {
                        strrel += "nofollow";
                    }
                    if (noindex != 0)
                    {
                        strrel += strrel == " rel=\"" ? "noindex" : " noindex";
                    }
                    if (noopener != 0)
                    {
                        strrel += strrel == " rel=\"" ? "noopener" : " noopener";
                    }
                    if(noreferrer !=0)//RK
                    {
                        strrel += strrel == " rel=\"" ? "noreferrer" : " noreferrer";
                    }
                    strrel += "\"";
                    dr["rel"] = strrel;
                }
            }
            StringBuilder bookmarkLink = new StringBuilder();
            if (dtBookmark.Rows.Count > 0)
            {
                dgrdBookmark.Visible = true;
                lblNoSavedbookmark.Visible = false;

                if(dt.Rows[0]["RandomiseLinksInLink"].ToString() == "1")
                {
                    pnlPagingMain.Visible = false;
                }

                DataView dv = dtBookmark.DefaultView;
                _PageDataSource.DataSource = dv;
                if (dt.Rows[0]["RandomiseLinksInLink"].ToString() == "1")
                {
                    _PageDataSource.AllowPaging = false;
                    _PageDataSource.PageSize = Convert.ToInt32(dt.Rows[0]["NumberOfLinksinlink"].ToString());
                    _PageDataSource.CurrentPageIndex = 0;
                }
                else
                {
                    _PageDataSource.AllowPaging = true;
                    _PageDataSource.PageSize = 30;
                    _PageDataSource.CurrentPageIndex = CurrentPage;
                }
                ViewState["TotalPages"] = _PageDataSource.PageCount;
                this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + _PageDataSource.PageCount;
                this.dgrdBookmark.DataSource = _PageDataSource;
                this.dgrdBookmark.DataBind();
                this.doPaging();
            }
            else
            {
                lblNoSavedbookmark.Visible = true;
                dgrdBookmark.Visible = false;
                pnlPagingMain.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblNoSavedbookmark.Text = ex.Message.ToString();
        }
    }

    protected void dgrdBookmark_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            String linkid = drv["Id"].ToString();
            String SiteId = Convert.ToString(Cache["SiteId_" + ReadConfig.getDomainUrl()]);
            int clickstatus = Convert.ToInt32(drv["Blocker"].ToString());
            int sameipstatus = Convert.ToInt32(drv["SameIP_Status"].ToString());
            int sameipvalue = Convert.ToInt32(drv["SameIP_Value"].ToString());
            int sameipclickcountlink = 0;

            if(clickstatus != 0 || sameipstatus != 0)
            {
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Select_SameIP_LinkClick";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", utils.GetUserIP());
                cmd.Parameters.Add("@LinkID", Convert.ToString(linkid));
                cmd.Parameters.Add("@DomainID", SiteId);
                cmd.Parameters.Add("@BlockStatus", 0);
                sameipclickcountlink = Convert.ToInt32(dpreport.ExecuteScalar(cmd));
            }

            int rowIndex = e.Item.ItemIndex;
            Label SummarySameIPClicksLink = (Label)e.Item.FindControl("MainSameIPClicksLink");
            SummarySameIPClicksLink.Text = sameipclickcountlink.ToString();
            SummarySameIPClicksLink.Style.Add(HtmlTextWriterStyle.Display, "none");

            Image SummaryLinkBlockImage = (Image)e.Item.FindControl("MainLinkBlockImage");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Display, "none");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.ZIndex, "9999");
            SummaryLinkBlockImage.Style.Add("float", "right");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Position, "absolute");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Left, "0");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Top, "0");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Width, "100%");
            SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Height, "100%");
            SummaryLinkBlockImage.Attributes.Add("onclick", "RecordClickLink(" + linkid + ", " + SiteId + ", 1)");
            if(sameipvalue <= 0)
            {
                sameipstatus = 0;
            }
            if (sameipstatus == 1 && sameipvalue <= sameipclickcountlink)
            {
                SummaryLinkBlockImage.Style.Add(HtmlTextWriterStyle.Display, "block");
                e.Item.Attributes["style"] = "display:none";
            }
        }
    }

    #region Private Properties
    private int CurrentPage
    {
        get
        {
            object objPage = ViewState["_CurrentPage"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPage"] = value; }
    }
    private int firstIndex
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndex"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndex"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndex"] = value; }
    }
    private int lastIndex
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndex"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndex"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndex"] = value; }
    }
    #endregion

    #region Pagination
    private void doPaging()
    {
        lbtnPrevious.Visible = true;
        lbtnNext.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        firstIndex = CurrentPage - (CurrentPage % 10);
        lastIndex = CurrentPage + (10 - (CurrentPage % 10));
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            firstIndex = lastIndex - 10;
            lbtnNext.Visible = false;
        }

        if (firstIndex <= 0)
        {
            firstIndex = 0;
            lbtnPrevious.Visible = false;
        }

        for (int i = firstIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
    }
    protected void lbtnPrevious_Click(object sender, EventArgs e)
    {
        lbtnPrevious.Visible = true;
        lbtnNext.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPage = CurrentPage - 10;
        firstIndex = CurrentPage - (CurrentPage % 10);
        lastIndex = CurrentPage + (10 - (CurrentPage % 10));
        CurrentPage = firstIndex;
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            lbtnNext.Visible = false;
        }

        if (firstIndex <= 0)
        {
            firstIndex = 0;
            lbtnPrevious.Visible = false;
            CurrentPage = 0;
        }

        for (int i = firstIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + Convert.ToString(ViewState["TotalPages"]);
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
        this.BindGridBookmarks();
    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {
        lbtnPrevious.Visible = true;
        lbtnNext.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPage = CurrentPage + 10;
        firstIndex = CurrentPage - (CurrentPage % 10);
        lastIndex = CurrentPage + (10 - (CurrentPage % 10));
        CurrentPage = firstIndex;
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            lbtnNext.Visible = false;
        }

        if (firstIndex <= 0)
        {
            firstIndex = 0;
            lbtnPrevious.Visible = false;
            CurrentPage = 0;
        }

        for (int i = firstIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfo.Text = "Page " + (CurrentPage + 1) + " of " + Convert.ToString(ViewState["TotalPages"]);
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
        this.BindGridBookmarks();
    }
    protected void dlPaging_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            CurrentPage = Convert.ToInt16(Convert.ToString(e.CommandArgument));
            this.BindGridBookmarks();
        }
    }
    protected void dlPaging_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");

        lnkbtnPage.PostBackUrl = "~/links/index.aspx";
        if (Convert.ToString(lnkbtnPage.CommandArgument) == Convert.ToString(CurrentPage))
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("font-size", "Medium");
            lnkbtnPage.Style.Add("color", "red");
            lnkbtnPage.Font.Bold = true;
        }
    }
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}