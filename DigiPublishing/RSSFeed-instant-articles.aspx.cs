﻿using System;
using System.Data;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using HtmlAgilityPack;

public partial class RSSFeed_instant_articles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        get();
    }

    protected void get()
    {
        DataTable dt = new DataTable();
        XmlTextWriter TextWriter = new XmlTextWriter(Response.OutputStream, Encoding.ASCII);
        Response.ContentType = "text/xml";

        DBHelper dp1 = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DBCommand strqry = new DBCommand();
        string strQuery = "SELECT blog_title,blog_id,post_id,post_title,post_description,BRAND_BLOG_POSTS.c_date "
                        + "FROM BRAND_BLOG_NEW LEFT OUTER JOIN BRAND_BLOG_POSTS ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id "
                        + "LEFT JOIN  brand_user ON BRAND_BLOG_POSTS.c_by = brand_user.uid "
                        + "WHERE blog_id=(Select  TOP 1 blog_id from BRAND_BLOG_NEW "
                        + "left join  brand_user on BRAND_BLOG_NEW.c_by = brand_user.uid "
                        + "Where BRAND_BLOG_NEW.blog_status <>2) "
                        + "and post_status<>'0' AND blog_subchannel_id=1 and BRAND_BLOG_POSTS.Active_Inactive=1 "
                        + "ORDER BY SortOrder DESC";

        strqry.CommandText = strQuery;
        strqry.CommandType = CommandType.Text;
        dt = dp1.FillDataTable(strqry);

        string strQuery1 = "SELECT description FROM Brand_AppSettings WHERE IsDelete='False' AND name='description' AND Type=1";
        DataTable dtData = dp1.FillDataTable(strQuery1);
        string metadata = "";
        if (dtData.Rows.Count > 0)
        {
            metadata = dtData.Rows[0]["description"].ToString();
        }

        TextWriter.Formatting = Formatting.Indented;
        TextWriter.WriteStartDocument();
        TextWriter.WriteStartElement("rss");
        TextWriter.WriteAttributeString("version", "2.0");
        TextWriter.WriteAttributeString("xmlns:content", "http://purl.org/rss/1.0/modules/content/");
        string DomainURL = ReadConfig.getDomainUrl();
        string logo = ReadConfig.GetLogo().Replace('~', ' ').Trim();

        TextWriter.WriteStartElement("channel");
        if (dt.Rows.Count > 0)
            TextWriter.WriteElementString("title", dt.Rows[0]["blog_title"].ToString());
        TextWriter.WriteElementString("link", "http://" + DomainURL);
        TextWriter.WriteElementString("description", metadata);
        TextWriter.WriteElementString("language", "en-us");
        TextWriter.WriteElementString("lastBuildDate", DateTime.Now.ToString());

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string str_postsdesc = "";
                string descplaintext = "";
                string firstImage = "";
                if (!string.IsNullOrWhiteSpace(dt.Rows[i]["post_description"].ToString()))
                {
                    str_postsdesc = utils.formatStringForURLSelect(dt.Rows[i]["post_description"].ToString());
                    descplaintext = Regex.Replace(dt.Rows[i]["post_description"].ToString(), @"<(.|\n)*?>", string.Empty).Replace("&nbsp;", "");
                    if (descplaintext.Length > 200)
                    {
                        descplaintext = descplaintext.Substring(0, 197) + "...";
                    }
                    str_postsdesc = str_postsdesc.Replace("src=\"/config/", "src=\"http://" + DomainURL + "/config/");
                    str_postsdesc = str_postsdesc.Replace("&nbsp;", "");
                    List<string> imagesrcs = getAllImageSrc(str_postsdesc);

                    if (imagesrcs != null)
                    {
                        if (imagesrcs.Count > 0)
                        {
                            var arr = imagesrcs[0].Split('/');
                            var imagename = arr[arr.Length - 1].Split('.')[0];

                            firstImage += "<figure>"
                                        + "<img alt=\"" + imagename.Replace("-", " ").Replace("_", " ") + "\" src=\"" + imagesrcs[0] + "\" />"
                                        + "<figcaption>" + imagename + "</figcaption>"
                                        + "</figure> ";
                        }
                    }
                }



                string strposttitle = utils.GeneratePostTitleForURL(utils.formatStringForURLSelect(Convert.ToString(dt.Rows[i]["post_title"]).Trim().Replace(' ', '-')));

                string newpostdesc = RemoveEmptyTags(str_postsdesc);
                newpostdesc = removeAllIdClassAndStyle(newpostdesc);
                newpostdesc = newpostdesc.Replace("<div", "<p").Replace("</div>", "</p>").Replace("<span", "<p").Replace("</span>", "</p>");
                newpostdesc = newpostdesc.Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("<br>", "").Replace("<br/>", "").Replace("<br >", "");
                newpostdesc = RemoveEmptyTags(newpostdesc);
                newpostdesc = removecontainerP(newpostdesc);
                newpostdesc = RemoveEmptyTags(newpostdesc);

                TextWriter.WriteStartElement("item");
                TextWriter.WriteElementString("title", utils.formatStringForURLSelect(dt.Rows[i]["post_title"].ToString()));
                TextWriter.WriteElementString("link", "http://" + DomainURL + "/features/" + strposttitle + ".aspx");
                TextWriter.WriteElementString("guid", GuidFromDate(Convert.ToDateTime(dt.Rows[i]["c_date"].ToString())));
                TextWriter.WriteElementString("pubDate", Convert.ToDateTime(dt.Rows[i]["c_date"].ToString()).ToString());
                TextWriter.WriteElementString("author", DomainURL);
                TextWriter.WriteElementString("description", descplaintext);
                TextWriter.WriteStartElement("content:encoded");
                string content = "<html lang=\"en\" prefix=\"op:http://media.facebook.com/op#\" >"
                                + "<head>"
                                + "<meta charset=\"utf-8\">"
                                + "<meta property=\"op:markup_version\" content=\"v1.0\">"
                                + "<link rel=\"canonical\" href=\"http://" + DomainURL + "/features/" + strposttitle + ".aspx\">"
                                + "<meta property=\"fb:use_automatic_ad_placement\" content=\"enable=true ad_density=default\">"
                                + "</head>"
                                + "<body>"
                                + "<article>"
                                + "<header>"
                                + "<h1>" + utils.formatStringForURLSelect(dt.Rows[i]["post_title"].ToString()) + "</h1>"
                                + "</header>"
                                + firstImage
                                + newpostdesc
                                + "<footer>"
                                + "</footer>"
                                + "</article>"
                                + "</body>"
                                + "</html>";
                TextWriter.WriteCData(content);
                TextWriter.WriteEndElement();
                TextWriter.WriteEndElement();
            }
        }
        TextWriter.WriteEndElement();
        TextWriter.WriteEndElement();
        TextWriter.WriteEndDocument();
        TextWriter.Flush();
        TextWriter.Close();
    }

    private string removecontainerP(string PostDesc)
    {
        string desc = "";
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(PostDesc);
        var mainnode = doc.DocumentNode;
        if (mainnode.ChildNodes.Count == 1)
        {
            foreach (HtmlNode h in mainnode.ChildNodes[0].ChildNodes)
            {
                desc += "<" + h.Name + ">" + h.InnerText + "</" + h.Name + ">";
            }
        }
        else
        {
            foreach (HtmlNode h in mainnode.ChildNodes)
            {
                desc += "<" + h.Name + ">" + h.InnerText + "</" + h.Name + ">";
            }
        }
        return desc;
    }

    private string removeAllIdClassAndStyle(string PostDesc)
    {
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(PostDesc);
        var all_text = doc.DocumentNode.SelectNodes("//div | //ul | //p | //table | //span");
        foreach (var node in all_text)
        {
            node.Attributes.Remove("class");
            node.Attributes.Remove("id");
            node.Attributes.Remove("style");
        }
        return doc.DocumentNode.OuterHtml;
    }

    private List<string> getAllImageSrc(string PostDesc)
    {
        List<string> list = new List<string>();
        foreach (Match m in Regex.Matches(PostDesc, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase | RegexOptions.Multiline))
        {
            string src = m.Groups[1].Value;
            list.Add(src);
        }
        return list;
    }

    private List<string[]> getAllAnchorTags(string PostDesc)
    {
        List<string[]> hrefTags = new List<string[]>();
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(PostDesc);
        var nodes = doc.DocumentNode.SelectNodes("//a[@href]");
        if (nodes != null)
        {
            foreach (HtmlNode link in nodes)
            {
                HtmlAttribute att = link.Attributes["href"];
                string[] alink = new string[3];
                alink[0] = att.Value;
                alink[1] = link.InnerText;
                alink[2] = link.InnerHtml;
                hrefTags.Add(alink);
            }
        }
        return hrefTags;
    }

    private string RemoveEmptyTags(string PostDesc)
    {
        Regex r = new Regex(@"<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:""[^""]*""|'[^']*'|[\w\-.:]+))?)*\s*/?>\s*</\1\s*>");
        while (r.IsMatch(PostDesc))
        {
            PostDesc = r.Replace(PostDesc, String.Empty);
        }
        return PostDesc;
    }

    private string GuidFromDate(DateTime dt)
    {
        var tempGuid = Guid.NewGuid();
        var bytes = tempGuid.ToByteArray();
        bytes[3] = (byte)dt.Year;
        bytes[2] = (byte)dt.Month;
        bytes[1] = (byte)dt.Day;
        bytes[0] = (byte)dt.Hour;
        bytes[5] = (byte)dt.Minute;
        bytes[4] = (byte)dt.Second;
        return (new Guid(bytes)).ToString().Replace("-", "");
    }
}