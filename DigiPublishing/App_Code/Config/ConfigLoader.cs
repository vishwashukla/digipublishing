using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.UI;


public class ConfigLoader
{

    public ConfigLoader()
    {
    }

    /// <summary>
    /// Method to get the Database Connection string
    /// </summary>
    /// <returns>Retruns connection string</returns>
    public static string GetConnectionString()
    {
        string strConnection = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["ConnectionString"])))
            strConnection = Convert.ToString(ConfigurationManager.AppSettings["ConnectionString"]).Trim();
        return strConnection;
    }

    public static string GetSAMainURL()
    {
        string strsaMainURL = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["saMainURL"])))
            strsaMainURL = Convert.ToString(ConfigurationManager.AppSettings["saMainURL"]).Trim();
        return strsaMainURL;
    }

    public static string GetErrorPageUrl()
    {
        string strErrorPageUrl = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["ErrorPageUrl"])))
            strErrorPageUrl = Convert.ToString(ConfigurationManager.AppSettings["ErrorPageUrl"]).Trim();
        return strErrorPageUrl;    
    }

    public static string GetAppName()
    {
        string strAppName = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["AppName"])))
            strAppName = Convert.ToString(ConfigurationManager.AppSettings["AppName"]).Trim();
        return strAppName;
    }

    public static string GetConnectionUID()
    {
        string strConnectionUID = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["strConnectionUID"])))
            strConnectionUID = Convert.ToString(ConfigurationManager.AppSettings["strConnectionUID"]).Trim();
        return strConnectionUID;
    }

    public static string GetConnectionPWD()
    {
        string strConnectionPWD = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["strConnectionPWD"])))
            strConnectionPWD = Convert.ToString(ConfigurationManager.AppSettings["strConnectionPWD"]).Trim();
        return strConnectionPWD;
    }

    public static string GetConnectionDataSource()
    {
        string strConnectionDataSource = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["strConnectionDataSource"])))
            strConnectionDataSource = Convert.ToString(ConfigurationManager.AppSettings["strConnectionDataSource"]).Trim();
        return strConnectionDataSource;
    }

    public static string GetReportDBConnectionString()
    {
        string strReportDBConnectionString = "";    
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["ReportDBConnectionString"])))
            strReportDBConnectionString = Convert.ToString(ConfigurationManager.AppSettings["ReportDBConnectionString"]).Trim();
        return strReportDBConnectionString;
    }

    public static string GetReportDBConnectionString2()
    {
        string strReportDBConnectionString = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["ReportDBConnectionString2"])))
            strReportDBConnectionString = Convert.ToString(ConfigurationManager.AppSettings["ReportDBConnectionString2"]).Trim();
        return strReportDBConnectionString;
    }

    public static int GetIsUseNewClickDB()
    {
        string strIsUseNewClickDB = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["IsUseNewClickDB"])))
            strIsUseNewClickDB = Convert.ToString(ConfigurationManager.AppSettings["IsUseNewClickDB"]).Trim();
        return Convert.ToInt32(strIsUseNewClickDB);
    }

    private static string getEmailId()
    {
        string EmailId  = string.Empty;
        DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        DataTable DT = dp.FillDataTable("select Top 1 isnull(email,'empty') as email from brand_user where roles='super admin'");
        EmailId = Convert.ToString(DT.Rows[0]["email"]);
        return EmailId;
    }
    public static string GetSupportEmailAddress()
    {
        string strSupportEmailAddress = string.Empty;
        strSupportEmailAddress = getEmailId();

        if (strSupportEmailAddress.Equals("empty", StringComparison.InvariantCultureIgnoreCase))
        {
             strSupportEmailAddress = string.Empty;
        }

        return strSupportEmailAddress;

     /*   if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["SupportEmailAddress"])))
            strSupportEmailAddress = Convert.ToString(ConfigurationManager.AppSettings["SupportEmailAddress"]).Trim();
        return strSupportEmailAddress; */
    }

    public static string GetSALoginPage()
    {
        string strGetSALoginPage = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["SALoginPage"])))
            strGetSALoginPage = Convert.ToString(ConfigurationManager.AppSettings["SALoginPage"]).Trim();
        return strGetSALoginPage;
    }

    public static string GetPageSize()
    {
        string strPageSize = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["PageSize"])))
            strPageSize = Convert.ToString(ConfigurationManager.AppSettings["PageSize"]).Trim();
        return strPageSize;
    }

    public static string GetLivePublicSite()
    {
        string strLivePublicSite = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["LivePublicSite"])))
            strLivePublicSite = Convert.ToString(ConfigurationManager.AppSettings["LivePublicSite"]).Trim();
        return strLivePublicSite;
    }

    public static string GetWebAppFullPath()
    {
        string strWebAppFullPath = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["WebAppFullPath"])))
            strWebAppFullPath = Convert.ToString(ConfigurationManager.AppSettings["WebAppFullPath"]).Trim();
        return strWebAppFullPath;
    }

    public static string GetWidgetTestDomainUrl()
    {
        string strWebAppFullPath = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["WidgetTestDomainUrl"])))
            strWebAppFullPath = Convert.ToString(ConfigurationManager.AppSettings["WidgetTestDomainUrl"]).Trim();
        return strWebAppFullPath;
    }

    public static string GetDigipURL()
    {
        string strDigipURL = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["DigipURL"])))
            strDigipURL = Convert.ToString(ConfigurationManager.AppSettings["DigipURL"]).Trim();
        return strDigipURL;
    }

    public static string GetImagesCDNurl()
    {
        string strImagesCDNurl = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["ImagesCDNurl"])))
            strImagesCDNurl = Convert.ToString(ConfigurationManager.AppSettings["ImagesCDNurl"]).Trim();
        return strImagesCDNurl;
    }

    public static string GetTVSiteCDNurl()
    {
        string strTVSiteCDNurl = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["TVSiteCDNurl"])))
            strTVSiteCDNurl = Convert.ToString(ConfigurationManager.AppSettings["TVSiteCDNurl"]).Trim();
        return strTVSiteCDNurl;
    }

    public static string GetDemoSitesFlv()
    {
        string strDemoSitesFlv = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["DemoSitesFlv"])))
            strDemoSitesFlv = Convert.ToString(ConfigurationManager.AppSettings["DemoSitesFlv"]).Trim();
        return strDemoSitesFlv;
    }

    public static string GetDemoSitesAniGif()
    {
        string strDemoSitesAniGif = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["DemoSitesAniGif"])))
            strDemoSitesAniGif = Convert.ToString(ConfigurationManager.AppSettings["DemoSitesAniGif"]).Trim();
        return strDemoSitesAniGif;
    }

    public static string GetTestDemoSite()
    {
        string strTestDemoSite = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["TestDemoSite"])))
            strTestDemoSite = Convert.ToString(ConfigurationManager.AppSettings["TestDemoSite"]).Trim();
        return strTestDemoSite;
    }

    public static string GetFlagCache()
    {
        string strFlagCache = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["FlagCache"])))
            strFlagCache = Convert.ToString(ConfigurationManager.AppSettings["FlagCache"]).Trim();
        return strFlagCache;
    }

    public static string GetGA_ID()
    {
        string strGA_ID = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["GA_ID"])))
            strGA_ID = Convert.ToString(ConfigurationManager.AppSettings["GA_ID"]).Trim();
        return strGA_ID;
    }

    public static string GetSS_WidgetName()
    {
        string strss_widgetname = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["ss_widgetname"])))
            strss_widgetname = Convert.ToString(ConfigurationManager.AppSettings["ss_widgetname"]).Trim();
        return strss_widgetname;
    }

    public static string GetIISSiteName()
    {
        string strIISSiteName = "";
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["IISSiteName"])))
            strIISSiteName = Convert.ToString(ConfigurationManager.AppSettings["IISSiteName"]).Trim();
        return strIISSiteName;
    }

    public static void createDirectory(string strDomain)
    {
        string strPath = HttpContext.Current.Request.PhysicalApplicationPath + "config/";
        DirectoryInfo dirInfo = new DirectoryInfo(strPath);
        dirInfo.CreateSubdirectory(strDomain);
        string strUploadDir = strDomain + "/upload";
        string strUserFilesDir = strDomain + "/upload/userfiles";
        string strTeamsDir = strDomain + "/upload/Teams";
        string stravatarDir = strDomain + "/upload/avatar";

        dirInfo.CreateSubdirectory(strUploadDir);
        dirInfo.CreateSubdirectory(strUserFilesDir);
        dirInfo.CreateSubdirectory(strTeamsDir);
        dirInfo.CreateSubdirectory(stravatarDir);

        Page p = new Page();
        string sourceFile = p.Server.MapPath("~/images_common/NoTeam.jpg");
        string destinationFile = p.Server.MapPath("~/Config/" + strDomain + "/upload/Teams/NoPhoto.jpg");
        System.IO.File.Copy(sourceFile, destinationFile);

        sourceFile = p.Server.MapPath("~/images_common/NoPhoto.jpg");
        destinationFile = p.Server.MapPath("~/Config/" + strDomain + "/upload/avatar/NoPhoto.jpg");
        System.IO.File.Copy(sourceFile, destinationFile);
        /*
                sourceFile = p.Server.MapPath("~/images_common/default.gif");
                destinationFile = p.Server.MapPath("~/Config/" + strDomain + "/upload/default.gif");
                System.IO.File.Copy(sourceFile, destinationFile);*/

        Stream streamfile = File.Create(dirInfo + "/" + strDomain + "/Config.config");
        StreamWriter sw = new StreamWriter(streamfile);
        sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
        sw.WriteLine("<appSettings>");
        sw.WriteLine("\t<connection></connection>");
        sw.WriteLine("\t<features></features>");
        sw.WriteLine("\t<logo></logo>");
        sw.WriteLine("\t<theme></theme>");
        sw.WriteLine("\t<email></email>");
        sw.WriteLine("\t<Source></Source>");
        sw.WriteLine("\t<Status></Status>");
        sw.WriteLine("\t<latitude></latitude>");
        sw.WriteLine("\t<longitude></longitude>");
        sw.WriteLine("</appSettings>");
        sw.Close();
        streamfile.Close();
    }       
}
