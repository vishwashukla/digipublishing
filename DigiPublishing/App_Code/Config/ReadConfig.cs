﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for ReadConfig
/// </summary>
public class ReadConfig
{
	public ReadConfig()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static Dictionary<string, string> GetConfigNodes(string DomainName)
    {
        Dictionary<string, string> nodeLst = new Dictionary<string, string>();
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                strDomainPPath += "config.config";
                xmlDoc.Load(strDomainPPath);

                XmlNode node = xmlDoc.SelectSingleNode("/appSettings");
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    nodeLst.Add(childNode.Name.ToLower(), childNode.InnerText);
                }
            }
            catch { }
        }
        return nodeLst;
    }

    public static string GetConnectionString(string DomainName)
    {
        string strConnection = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/connection");
            if (node != null)
                strConnection = node.InnerText;
        }
        return strConnection;
    }

    /// <summary>
    /// Method to get the Domain site features
    /// </summary>
    /// <returns></returns>
    public static string GetFeatures(string DomainName)
    {
        string features = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/features");
            if (node != null)
                features = node.InnerText;
        }
        return features;
    }

    /// <summary>
    /// Method to get the Domain site logo
    /// </summary>
    /// <returns></returns>
    public static string GetLogo(string DomainName)
    {
        string Logo = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/logo");
            if (node != null)
                Logo = node.InnerText;
        }
        return Logo;
    }

    public static string GetTheme(string DomainName)
    {
        string strTheme = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);
            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/theme");
            if (node != null)
                strTheme = node.InnerText;
        }
        return strTheme;
    }

    public static string GetEmail(string DomainName)
    {
        string stremail = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);
            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/email");
            if (node != null)
                stremail = node.InnerText;
        }
        return stremail;  

      /*  DBHelper dp = getDomainDBHelper(DomainName);
        DataTable dt = dp.FillDataTable("select  isnull(email,'empty') as Email from brand_user");
         string EmailId =  dt.Rows[0]["Email"].ToString() ;
         if (EmailId.Equals("empty", StringComparison.InvariantCultureIgnoreCase))
        {
            EmailId = string.Empty;
        }

         return EmailId; */
    }

    

    public static string GetLatitude(string DomainName)
    {
        string latitude = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);
            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/latitude");
            if (node != null)
                latitude = node.InnerText;
        }
        if (!string.IsNullOrWhiteSpace(latitude))
            return latitude;
        else
            return "0.0";
    }

    public static string GetLongitude(string DomainName)
    {
        string longitude = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + DomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);
            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/longitude");
            if (node != null)
                longitude = node.InnerText;
        }
        if (!string.IsNullOrWhiteSpace(longitude))
            return longitude;
        else
            return "0.0";
    }

    /// <summary>
    /// Gets the Theme Path.
    /// </summary>
    public static string LogoPath
    {
        get
        {
            return "~/config/" + ReadConfig.getDomainUrl() + "/" + ReadConfig.GetLogo();
        }
    }

    public static string UserFilesDir
    {
        get
        {
            return "~/config/" + ReadConfig.getDomainUrl() + "/upload/userfiles";
        }
    }

    /// <summary>
    /// Get the Data according the domain name
    /// </summary>
    public static DBHelper getDomainDBHelper(string strDomainName)
    {
        string strConnection = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + strDomainName + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/connection");
            if (node != null)
                strConnection = node.InnerText;
        }

        DBHelper dp1 = new DBHelper(strConnection, DBSType.MSSQL);
        return dp1;
    }

    /// <summary>
    /// To get requested domain name from URL
    /// </summary>
    /// <returns>Retruns title</returns>
    public static string getDomainUrl()
    {
        String URL;
        if (Convert.ToString(HttpContext.Current.Request.Url.Host) != "localhost")
        {
            URL = Convert.ToString(HttpContext.Current.Request.Url.Host);
            if (URL.ToLower().StartsWith("www"))
            {
                int count = URL.IndexOf('.');
                int len = URL.Length - 1;
                URL = URL.Substring(count + 1, (len - count));
            }
        }
        else
        {
            URL = ConfigLoader.GetSAMainURL();
        }
        return URL;
    }

    /// <summary>
    /// Method to get the Database Connection string
    /// </summary>
    /// <returns>Retruns connection string</returns>
    public static string GetConnectionString()
    {
        string strConnection = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + ReadConfig.getDomainUrl() + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/connection");
            if (node != null)
                strConnection = node.InnerText;
        }
        return strConnection;
    }

    /// <summary>
    /// Method to get the Domain site logo
    /// </summary>
    /// <returns></returns>
    public static string GetLogo()
    {
        string Logo = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + ReadConfig.getDomainUrl() + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/logo");
            if (node != null)
                Logo = node.InnerText;
        }
        return Logo;
    }

    public static string GetTheme()
    {
        string strTheme = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + ReadConfig.getDomainUrl() + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);
            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/theme");
            if (node != null)
                strTheme = node.InnerText;
        }
        return strTheme;
    }

    /// <summary>
    /// Method to get the Domain site features
    /// </summary>
    /// <returns></returns>
    public static string GetFeatures()
    {
        string features = string.Empty;
        string strDomainPPath = HttpContext.Current.Request.PhysicalApplicationPath + "Config\\" + ReadConfig.getDomainUrl() + "\\";
        DirectoryInfo dirInfo = new DirectoryInfo(strDomainPPath);
        if (dirInfo.Exists)
        {
            XmlDocument xmlDoc = new XmlDocument();
            strDomainPPath += "config.config";
            xmlDoc.Load(strDomainPPath);

            XmlNode node = xmlDoc.SelectSingleNode("/appSettings/features");
            if (node != null)
                features = node.InnerText;
        }
        return features;
    }

    public static string getEmailId()
    {
        string EmailId = string.Empty;
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable DT = dp.FillDataTable("select Top 1 isnull(email,'empty') as email from brand_user where roles='admin'");
        EmailId = Convert.ToString(DT.Rows[0]["email"]);
        return EmailId;
    }

    public static string GetEmail()
    {
        string strSupportEmailAddress = string.Empty;
        strSupportEmailAddress = getEmailId();
        if (strSupportEmailAddress.Equals("empty", StringComparison.InvariantCultureIgnoreCase))
        {
            strSupportEmailAddress = string.Empty;
        }
        return strSupportEmailAddress;
         
    }

    public static void UpdateSingleNodeInConfig(string DomainName,string NodeName,string NodeValue)
    {
        try
        {
            string strConfigFilePath = HttpContext.Current.Request.PhysicalApplicationPath + "config/" + DomainName + "/Config.config";
            XmlNode node;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strConfigFilePath);

            node = xmlDoc.SelectSingleNode("/appSettings/" + NodeName);
            if (node != null && NodeValue.Trim() != "")
                node.InnerText = NodeValue;
            xmlDoc.Save(strConfigFilePath);
        }
        catch { }
    }

    public static void SetConfigWithParam(string fileName, string strconnection, string strFeatures, string strLogoName,
        string strTheme, string strEmail, string strSource, string strStatus, string strLatitude, string strLongitude)
    {
        try
        {
            XmlNode node;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);

            node = xmlDoc.SelectSingleNode("/appSettings/connection");
            if (node != null && strconnection.Trim() != "")
                node.InnerText = strconnection;

            node = xmlDoc.SelectSingleNode("/appSettings/features");
            if (node != null && strFeatures!="")
                node.InnerText = strFeatures;

            node = xmlDoc.SelectSingleNode("/appSettings/logo");
            if (node != null)
                node.InnerText = strLogoName;

            node = xmlDoc.SelectSingleNode("/appSettings/theme");
            if (node != null)
                node.InnerText = strTheme;

            node = xmlDoc.SelectSingleNode("/appSettings/email");
            if (node != null)
                node.InnerText = strEmail;

            node = xmlDoc.SelectSingleNode("/appSettings/Source");
            if (node != null)
                node.InnerText = strSource;

            node = xmlDoc.SelectSingleNode("/appSettings/Status");
            if (node != null)
                node.InnerText = strStatus;

            node = xmlDoc.SelectSingleNode("/appSettings/latitude");
            if (node != null)
                node.InnerText = strLatitude;

            node = xmlDoc.SelectSingleNode("/appSettings/longitude");
            if (node != null)
                node.InnerText = strLongitude;

            xmlDoc.Save(fileName);
        }
        catch
        { }
    }
}