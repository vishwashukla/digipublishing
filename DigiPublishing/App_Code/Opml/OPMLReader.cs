using System;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;

public class OPMLReader
{
    public OPMLReader()
    {
    }


	public static string GetSafeAttributeValue(XmlNode nod, string attributeName)
	{
		string attrValue = String.Empty;
		try
		{
            attrValue = nod.Attributes[attributeName].Value;
		}
		catch
		{
		}
        return attrValue;

}

    public static DataSet OPMLToDataSet(string OPMLUrl)
    {
        DataSet ds = new DataSet();
        XmlDocument doc = new XmlDocument();
        // this Xml will never load directly into a DataSet because of the nested (duplicate) table names 
        //    (e.g., "outline/outline") so we load into XmlDocument first
        doc.Load(OPMLUrl);
        // Now get the  single level "outline" element stuff we want
        XmlNodeList nods = doc.SelectNodes("//outline[@xmlUrl!='']");
        // // Create the target XmlDocument
        XmlDocument doc2 = new XmlDocument();
        // give it a root element
        doc2.LoadXml("<items></items>");
        // import the nodelist
        foreach (XmlNode nod in nods)
        {
            XmlNode importNode = doc2.ImportNode(nod, true);
            doc2.DocumentElement.AppendChild(importNode);
        }

        string outerXml = doc2.OuterXml;
        byte[] b = Encoding.UTF8.GetBytes(outerXml);
        MemoryStream ms = new MemoryStream(b);
        ds.ReadXml(ms);
        return ds;
    }

    public static DataSet OPMLToDataSet(XmlNodeList nods)
    {
        DataSet ds = new DataSet();
        XmlDocument doc2 = new XmlDocument();
        // give it a root element
        doc2.LoadXml("<items></items>");
        // import the nodelist
        string text = "";
        string xmlUrl = "";
        string htmlUrl = "";
        //massage the nodes and do some substitution preprocessing
        foreach (XmlNode nod in nods)
        {
            XmlNode importNode = doc2.ImportNode(nod, true);
            try
            {
                text = GetSafeAttributeValue(importNode, "text");
            }
            catch
            {
            }
            if (text != "")
            {
                try
                {
                    XmlAttribute att = doc2.CreateAttribute("title");
                    att.Value = text;
                    importNode.Attributes.Append(att);
                    importNode.Attributes.Remove(importNode.Attributes["text"]);
                }
                catch
                {
                    throw new InvalidOperationException("outline element has no text / title attribute");
                }
                }
            try
            {
                xmlUrl = GetSafeAttributeValue(importNode, "xmlUrl");
                if (xmlUrl == "")
                {
                    xmlUrl = GetSafeAttributeValue(importNode, "url");
                    XmlAttribute att2 = doc2.CreateAttribute("xmlUrl");
                    att2.Value = xmlUrl;
                    importNode.Attributes.Append(att2);
                    importNode.Attributes.Remove(importNode.Attributes["url"]);
                }
            }
            catch
            {
            }
            try
            {
                htmlUrl = GetSafeAttributeValue(importNode, "htmlUrl");
                if (htmlUrl == "")
                {
                    htmlUrl = GetSafeAttributeValue(importNode, "xmlUrl");
                    XmlAttribute att3 = doc2.CreateAttribute("htmlUrl");
                    att3.Value = htmlUrl;
                    importNode.Attributes.Append(att3);
                }
            }
            catch
            {
            }
            doc2.DocumentElement.AppendChild(importNode);

        }

        string outerXml = doc2.OuterXml;
        byte[] b = Encoding.UTF8.GetBytes(outerXml);
        MemoryStream ms = new MemoryStream(b);
        ds.ReadXml(ms);
        return ds;
    }
}
