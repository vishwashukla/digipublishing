﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

public class Body
{
    public Body()
    {
    }

    public Body(XmlElement Element)
    {
        if (Element.Name.Equals("body", StringComparison.CurrentCultureIgnoreCase))
        {
            foreach (XmlNode Child in Element.ChildNodes)
            {
                try
                {
                    if (Child.Name.Equals("outline", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Outlines.Add(new Outline((XmlElement)Child));
                    }
                }
                catch { }
            }
        }
    }

    private List<Outline> _Outlines = null;

    public List<Outline> Outlines
    {
        get
        {
            if (_Outlines == null)
            {
                _Outlines = new List<Outline>();
            }
            return _Outlines;
        }
        set { _Outlines = value; }
    }

    public override string ToString()
    {
        StringBuilder BodyString=new StringBuilder();
        BodyString.Append("<body>");
        foreach (Outline Outline in Outlines)
        {
            BodyString.Append(Outline.ToString());
        }
        BodyString.Append("</body>");
        return BodyString.ToString();
    }
}
