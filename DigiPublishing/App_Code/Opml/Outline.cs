﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

public class Outline
{
    public Outline()
    {
    }

    public Outline(XmlElement Element)
    {
        if (Element.Name.Equals("outline", StringComparison.CurrentCultureIgnoreCase))
        {
            if (Element.Attributes["text"] != null)
            {
                Text = Element.Attributes["text"].Value;
            }
            else if (Element.Attributes["description"] != null)
            {
                Description = Element.Attributes["description"].Value;
            }
            else if (Element.Attributes["htmlUrl"] != null)
            {
                HTMLUrl = Element.Attributes["htmlUrl"].Value;
            }
            else if (Element.Attributes["type"] != null)
            {
                Type = Element.Attributes["type"].Value;
            }
            else if (Element.Attributes["language"] != null)
            {
                Language = Element.Attributes["language"].Value;
            }
            else if (Element.Attributes["title"] != null)
            {
                Title = Element.Attributes["title"].Value;
            }
            else if (Element.Attributes["version"] != null)
            {
                Version = Element.Attributes["version"].Value;
            }
            else if (Element.Attributes["xmlUrl"] != null)
            {
                XMLUrl = Element.Attributes["xmlUrl"].Value;
            }
            else if (Element.Attributes["tag"] != null)
            {
                Tag = Element.Attributes["tag"].Value;
            }
            foreach (XmlNode Child in Element.ChildNodes)
            {
                try
                {
                    if (Child.Name.Equals("outline", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Outlines.Add(new Outline((XmlElement)Child));
                    }
                }
                catch { }
            }
        }
    }
        
    private string _XMLUrl;
    private string _Version;
    private string _Title;
    private string _Language;
    private string _Text;
    private string _Description;
    private string _HTMLUrl;
    private string _Type;
    private string _Tag;
    private List<Outline> _Outlines = null;

    public List<Outline> Outlines
    {
        get
        {
            if (_Outlines == null)
            {
                _Outlines = new List<Outline>();
            }
            return _Outlines;
        }
        set { _Outlines = value; }
    }
        

    public string XMLUrl
    {
        get { return _XMLUrl; }
        set { _XMLUrl = value; }
    }

    public string Version
    {
        get { return _Version; }
        set { _Version = value; }
    }

    public string Title
    {
        get { return _Title; }
        set { _Title = value; }
    }

    public string Language
    {
        get { return _Language; }
        set { _Language = value; }
    }

    public string Type
    {
        get { return _Type; }
        set { _Type = value; }
    }
    public string Tag
    {
        get
        {
            return _Tag;
        }
        set
        {
            _Tag = value;
        }
    }
    public string HTMLUrl
    {
        get { return _HTMLUrl; }
        set { _HTMLUrl = value; }
    }

    public string Text
    {
        get { return _Text; }
        set { _Text = value; }
    }

    public string Description
    {
        get { return _Description; }
        set { _Description = value; }
    }

    public override string ToString()
    {
        StringBuilder OutlineString = new StringBuilder();
        OutlineString.Append("<outline text=\""+Text+"\"");
        if (!string.IsNullOrEmpty(XMLUrl))
        {
            OutlineString.Append(" xmlUrl=\"" + XMLUrl + "\"");
        }
        if (!string.IsNullOrEmpty(Version))
        {
            OutlineString.Append(" version=\"" + Version + "\"");
        }
        if (!string.IsNullOrEmpty(Title))
        {
            OutlineString.Append(" title=\"" + Title + "\"");
        }
        if (!string.IsNullOrEmpty(Language))
        {
            OutlineString.Append(" language=\"" + Language + "\"");
        }
        if (!string.IsNullOrEmpty(Type))
        {
            OutlineString.Append(" type=\"" + Type + "\"");
        }
        if (!string.IsNullOrEmpty(HTMLUrl))
        {
            OutlineString.Append(" htmlUrl=\"" + HTMLUrl + "\"");
        }
        if (!string.IsNullOrEmpty(Description))
        {
            OutlineString.Append(" description=\"" + Description + "\"");
        }
        if (!string.IsNullOrEmpty(Tag))
        {
            OutlineString.Append(" tag=\"" + Tag + "\"");
        }
        if (Outlines.Count > 0)
        {
            OutlineString.Append(">\r\n");
            foreach (Outline Outline in Outlines)
            {
                OutlineString.Append(Outline.ToString());
            }
            OutlineString.Append("</outline>\r\n");
        }
        else
        {
            OutlineString.Append(" />\r\n");
        }
        return OutlineString.ToString();
    }
}
