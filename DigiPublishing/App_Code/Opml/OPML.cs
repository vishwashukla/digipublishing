﻿using System;
using System.Text;
using System.Xml;

public class OPML
{
    public OPML()
    {

    }

    public OPML(XmlDocument Document)
    {
        foreach (XmlNode Children in Document.ChildNodes)
        {
            if (Children.Name.Equals("opml", StringComparison.CurrentCultureIgnoreCase))
            {
                foreach (XmlNode Child in Children.ChildNodes)
                {
                    if (Child.Name.Equals("body", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Body=new Body((XmlElement)Child);
                    }
                    else if (Child.Name.Equals("head", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Head = new Head((XmlElement)Child);
                    }
                }
            }
        }
    }

    public Body Body
    {
        get { return _Body; }
        set { _Body = value; }
    }

    public Head Head
    {
        get { return _Head; }
        set { _Head = value; }
    }

    private Body _Body;
    private Head _Head;

    public override string ToString()
    {
        StringBuilder OPMLString = new StringBuilder();
        OPMLString.Append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><opml version=\"2.0\">");
        OPMLString.Append(Head.ToString());
        OPMLString.Append(Body.ToString());
        OPMLString.Append("</opml>");
        return OPMLString.ToString();
    }
}
