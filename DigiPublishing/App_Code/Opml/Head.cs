﻿using System;
using System.Text;
using System.Xml;

public class Head
{

    public Head()
    {
    }

    public Head(XmlElement Element)
    {
        if (Element.Name.Equals("head", StringComparison.CurrentCultureIgnoreCase))
        {
            foreach (XmlNode Child in Element.ChildNodes)
            {
                try
                {
                    if (Child.Name.Equals("title", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Title = Child.InnerText;
                    }
                    else if (Child.Name.Equals("ownerName", StringComparison.CurrentCultureIgnoreCase))
                    {
                        OwnerName = Child.InnerText;
                    }
                    else if (Child.Name.Equals("ownerEmail", StringComparison.CurrentCultureIgnoreCase))
                    {
                        OwnerEmail = Child.InnerText;
                    }
                    else if (Child.Name.Equals("dateCreated", StringComparison.CurrentCultureIgnoreCase))
                    {
                        DateCreated = DateTime.Parse(Child.InnerText);
                    }
                    else if (Child.Name.Equals("dateModified", StringComparison.CurrentCultureIgnoreCase))
                    {
                        DateModified = DateTime.Parse(Child.InnerText);
                    }
                    else if (Child.Name.Equals("docs", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Docs = Child.InnerText;
                    }
                }
                catch { }
            }
        }
    }

    public string Title
    {
        get { return _Title; }
        set { _Title = value; }
    }

    public DateTime DateCreated
    {
        get { return _DateCreated; }
        set { _DateCreated = value; }
    }

    public DateTime DateModified
    {
        get { return _DateModified; }
        set { _DateModified = value; }
    }

    public string OwnerName
    {
        get { return _OwnerName; }
        set { _OwnerName = value; }
    }

    public string OwnerEmail
    {
        get { return _OwnerEmail; }
        set { _OwnerEmail = value; }
    }

    public string Docs
    {
        get { return _Docs; }
        set { _Docs = value; }
    }

    private string _Title="";
    private DateTime _DateCreated=DateTime.Now;
    private DateTime _DateModified=DateTime.Now;
    private string _OwnerName="";
    private string _OwnerEmail="";
    private string _Docs = "http://www.opml.org/spec2";

    public override string ToString()
    {
        StringBuilder HeadString = new StringBuilder();
        HeadString.Append("<head>");
        HeadString.Append("<title>" + Title + "</title>\r\n");
        HeadString.Append("<dateCreated>" + DateCreated.ToString("R") + "</dateCreated>\r\n");
        HeadString.Append("<dateModified>" + DateModified.ToString("R") + "</dateModified>\r\n");
        HeadString.Append("<ownerName>" + OwnerName + "</ownerName>\r\n");
        HeadString.Append("<ownerEmail>" + OwnerEmail + "</ownerEmail>\r\n");
        HeadString.Append("<docs>" + Docs + "</docs>\r\n");
        HeadString.Append("</head>\r\n");
        return HeadString.ToString();
    }
}
