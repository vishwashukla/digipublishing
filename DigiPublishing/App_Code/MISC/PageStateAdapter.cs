using System.Web.UI;
using System.Web.UI.Adapters;

/// <summary>
/// Summary description for PageStateAdapter
/// </summary>

public class PageStateAdapter : PageAdapter
{
    // (VIEWSTATE code is too long)
    //public override PageStatePersister GetStatePersister()
    //{
    //    return new SessionPageStatePersister(this.Page);
    //}

    private bool DesignMode
    {
        get
        {
            return (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv");
        }
    }

    private PageStatePersister _currentPageStatePersistor;
    public override PageStatePersister GetStatePersister()
    {
        if (this.DesignMode == true)
        {
            //avoid session persistor when in design mode
            return base.GetStatePersister();
        }
        else
        {
            if (_currentPageStatePersistor == null)
            {
                //PageStatePersistor property can be called many times per page
                //so hang onto the reference
                _currentPageStatePersistor = new SessionPageStatePersister(this.Page);
            }
            return _currentPageStatePersistor;
        }
    }

}

