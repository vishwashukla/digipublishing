﻿using System;
using System.Web;
using System.IO;
using System.Net;

/// <summary>
/// Summary description for CountryLookUp
/// </summary>
public class CountryLookUp
{
    public MemoryStream _MemoryStream;
    private static long CountryBegin = 16776960;
    private static string[] CountryName =
     {
		"N/A",		"Asia/Pacific Region",		"Europe",		"Andorra",		"United Arab Emirates",		"Afghanistan",		"Antigua and Barbuda",
		"Anguilla",		"Albania",		"Armenia",		"Netherlands Antilles",		"Angola",		"Antarctica",		"Argentina",		"American Samoa",
		"Austria",		"Australia",		"Aruba",		"Azerbaijan",		"Bosnia and Herzegovina",		"Barbados",		"Bangladesh",
		"Belgium",		"Burkina Faso",		"Bulgaria",		"Bahrain",		"Burundi",		"Benin",		"Bermuda",		"Brunei Darussalam",		"Bolivia",
		"Brazil",		"Bahamas",		"Bhutan",		"Bouvet Island",		"Botswana",		"Belarus",		"Belize",		"Canada",		"Cocos (Keeling) Islands",	
        "Congo, The Democratic Republic of the",		"Central African Republic",		"Congo",		"Switzerland",		"Cote D'Ivoire",		"Cook Islands",		"Chile",	
        "Cameroon",		"China",		"Colombia",		"Costa Rica",		"Cuba",		"Cape Verde",		"Christmas Island",		"Cyprus",
		"Czech Republic",		"Germany",		"Djibouti",		"Denmark",		"Dominica",		"Dominican Republic",		"Algeria",		"Ecuador",
		"Estonia",		"Egypt",		"Western Sahara",		"Eritrea",		"Spain",		"Ethiopia",		"Finland",		"Fiji",		"Falkland Islands (Malvinas)",
		"Micronesia, Federated States of",		"Faroe Islands",		"France",		"France, Metropolitan",		"Gabon",		"United Kingdom",
		"Grenada",		"Georgia",		"French Guiana",		"Ghana",		"Gibraltar",		"Greenland",		"Gambia",		"Guinea",
		"Guadeloupe",		"Equatorial Guinea",		"Greece",		"South Georgia and the South Sandwich Islands",		"Guatemala",		"Guam",
		"Guinea-Bissau",		"Guyana",		"Hong Kong",		"Heard Island and McDonald Islands",		"Honduras",		"Croatia",		"Haiti",
		"Hungary",		"Indonesia",		"Ireland",		"Israel",		"India",		"British Indian Ocean Territory",
		"Iraq",		"Iran, Islamic Republic of",		"Iceland",		"Italy",		"Jamaica",		"Jordan",		"Japan",		"Kenya",		"Kyrgyzstan",
		"Cambodia",		"Kiribati",		"Comoros",		"Saint Kitts and Nevis",		"Korea, Democratic People's Republic of",		"Korea, Republic of",
		"Kuwait",		"Cayman Islands",		"Kazakstan",		"Lao People's Democratic Republic",		"Lebanon",		"Saint Lucia",		"Liechtenstein",
		"Sri Lanka",		"Liberia",		"Lesotho",		"Lithuania",		"Luxembourg",		"Latvia",		"Libyan Arab Jamahiriya",		"Morocco",
		"Monaco",		"Moldova, Republic of",		"Madagascar",		"Marshall Islands",		"Macedonia, the Former Yugoslav Republic of",
		"Mali",		"Myanmar",		"Mongolia",		"Macao",		"Northern Mariana Islands",		"Martinique",		"Mauritania",		"Montserrat",
		"Malta",		"Mauritius",		"Maldives",		"Malawi",		"Mexico",		"Malaysia",		"Mozambique",		"Namibia",		"New Caledonia",
		"Niger",		"Norfolk Island",		"Nigeria",		"Nicaragua",		"Netherlands",		"Norway",		"Nepal",		"Nauru",		"Niue",
		"New Zealand",		"Oman",		"Panama",		"Peru",		"French Polynesia",		"Papua New Guinea",		"Philippines",		"Pakistan",
		"Poland",		"Saint Pierre and Miquelon",		"Pitcairn",		"Puerto Rico",		"Palestinian Territory, Occupied",		"Portugal",
		"Palau",		"Paraguay",		"Qatar",		"Reunion",		"Romania",		"Russian Federation",		"Rwanda",		"Saudi Arabia",
		"Solomon Islands",		"Seychelles",		"Sudan",		"Sweden",		"Singapore",		"Saint Helena",		"Slovenia",
		"Svalbard and Jan Mayen",		"Slovakia",		"Sierra Leone",		"San Marino",		"Senegal",		"Somalia",		"Suriname",
		"Sao Tome and Principe",		"El Salvador",		"Syrian Arab Republic",		"Swaziland",		"Turks and Caicos Islands",
		"Chad",		"French Southern Territories",		"Togo",		"Thailand",		"Tajikistan",		"Tokelau",		"Turkmenistan",
		"Tunisia",		"Tonga",		"Timor-Leste",		"Turkey",		"Trinidad and Tobago",		"Tuvalu",		"Taiwan, Province of China",
		"Tanzania, United Republic of",		"Ukraine",		"Uganda",		"United States Minor Outlying Islands",		"United States",
		"Uruguay",		"Uzbekistan",		"Holy See (Vatican City State)",		"Saint Vincent and the Grenadines",		"Venezuela",
		"Virgin Islands, British",		"Virgin Islands, U.S.",		"Vietnam",		"Vanuatu",		"Wallis and Futuna",		"Samoa",
		"Yemen",		"Mayotte",		"Yugoslavia",		"South Africa",		"Zambia",		"Montenegro",		"Zimbabwe",		"Anonymous Proxy",
		"Satellite Provider",		"Other",		"Aland Islands",		"Guernsey",		"Isle of Man",		"Jersey",		"Saint Barthelemy",		"Saint Martin"
	};

    private static string[] CountryCode = 
    {
		"--",		"AP",		"EU",		"AD",		"AE",		"AF",		"AG",		"AI",		"AL",		"AM",		"AN",
		"AO",		"AQ",		"AR",		"AS",		"AT",		"AU",		"AW",		"AZ",		"BA",		"BB",		"BD",
		"BE",		"BF",		"BG",		"BH",		"BI",		"BJ",		"BM",		"BN",		"BO",		"BR",		"BS",
		"BT",		"BV",		"BW",		"BY",		"BZ",		"CA",		"CC",		"CD",		"CF",		"CG",		"CH",
		"CI",		"CK",		"CL",		"CM",		"CN",		"CO",		"CR",		"CU",		"CV",		"CX",		"CY",
		"CZ",		"DE",		"DJ",		"DK",		"DM",		"DO",		"DZ",		"EC",		"EE",		"EG",		"EH",
		"ER",		"ES",		"ET",		"FI",		"FJ",		"FK",		"FM",		"FO",		"FR",		"FX",		"GA",
		"GB",		"GD",		"GE",		"GF",		"GH",		"GI",		"GL",		"GM",		"GN",		"GP",		"GQ",
		"GR",		"GS",		"GT",		"GU",		"GW",		"GY",		"HK",		"HM",		"HN",		"HR",		"HT",
		"HU",		"ID",		"IE",		"IL",		"IN",		"IO",		"IQ",		"IR",		"IS",		"IT",		"JM",
		"JO",		"JP",		"KE",		"KG",		"KH",		"KI",		"KM",		"KN",		"KP",		"KR",		"KW",
		"KY",		"KZ",		"LA",		"LB",		"LC",		"LI",		"LK",		"LR",		"LS",		"LT",		"LU",
		"LV",		"LY",		"MA",		"MC",		"MD",		"MG",		"MH",		"MK",		"ML",		"MM",		"MN",
		"MO",		"MP",		"MQ",		"MR",		"MS",		"MT",		"MU",		"MV",		"MW",		"MX",		"MY",
		"MZ",		"NA",		"NC",		"NE",		"NF",		"NG",		"NI",		"NL",		"NO",		"NP",		"NR",
		"NU",		"NZ",		"OM",		"PA",		"PE",		"PF",		"PG",		"PH",		"PK",		"PL",		"PM",
		"PN",	    "PR",		"PS",		"PT",		"PW",		"PY",		"QA",		"RE",		"RO",		"RU",		"RW",	
        "SA",		"SB",		"SC",		"SD",		"SE",		"SG",		"SH",		"SI",		"SJ",		"SK",		"SL",
		"SM",		"SN",		"SO",		"SR",		"ST",	    "SV",		"SY",		"SZ",		"TC",		"TD",		"TF",		
        "TG",		"TH",		"TJ",		"TK",		"TM",		"TN",		"TO",		"TL",		"TR",		"TT",		"TV",
		"TW",		"TZ",		"UA",		"UG",		"UM",		"US",		"UY",		"UZ",		"VA",		"VC",		"VE",
		"VG",		"VI",		"VN",		"VU",		"WF",		"WS",		"YE",		"YT",		"RS",		"ZA",		"ZM",
		"ME",		"ZW",		"A1",		"A2",		"O1",		"AX",		"GG",		"IM",		"JE",		"BL",		"MF"

	};

    public CountryLookUp(MemoryStream ms)
    {
        _MemoryStream = ms;

    }

    public CountryLookUp()
    {
        //------------------------------------------------------------------------------------------------
        // Load the passed in GeoIP Data file to the memorystream
        //------------------------------------------------------------------------------------------------
        string FileLocation = HttpContext.Current.Request.PhysicalApplicationPath + "CountryDataFile/GeoIP.dat";
        FileStream _FileStream = new FileStream(FileLocation, FileMode.Open, FileAccess.Read);

        _MemoryStream = new MemoryStream();

        byte[] _Byte = new byte[257];


        while (_FileStream.Read(_Byte, 0, _Byte.Length) != 0)
        {
            _MemoryStream.Write(_Byte, 0, _Byte.Length);

        }

        _FileStream.Close();

    }

    private long ConvertIPAddressToNumber(IPAddress _IPAddress)
    {

        //------------------------------------------------------------------------------------------------
        // Convert an IP Address, (e.g. 127.0.0.1), to the numeric equivalent
        //------------------------------------------------------------------------------------------------

        string[] _Address = _IPAddress.ToString().Split('.');

        if (_Address.GetUpperBound(0).ToString() == "3")
        {
            long valueaddress = 16777216 * Int32.Parse(_Address[0]) + 65536 * Int32.Parse(_Address[1]) + 256 * Int32.Parse(_Address[2]) + Int32.Parse(_Address[3]); ;
            return valueaddress;
        }
        else
        {
            return 0;
        }

    }

    private string ConvertIPNumberToAddress(long _IPNumber)
    {

        //------------------------------------------------------------------------------------------------
        // Convert an IP Number to the IP Address equivalent
        //------------------------------------------------------------------------------------------------

        string _IPNumberPart1 = Convert.ToString(Convert.ToInt32(_IPNumber / 16777216) % 256);
        string _IPNumberPart2 = Convert.ToString(Convert.ToInt32(_IPNumber / 65536) % 256);
        string _IPNumberPart3 = Convert.ToString(Convert.ToInt32(_IPNumber / 256) % 256);
        string _IPNumberPart4 = Convert.ToString(Convert.ToInt32(_IPNumber) % 256);

        return _IPNumberPart1 + "." + _IPNumberPart2 + "." + _IPNumberPart3 + "." + _IPNumberPart4;

    }

    public static MemoryStream FileToMemory()
    {

        //------------------------------------------------------------------------------------------------
        // Read a given file into a Memory Stream to return as the result
        //------------------------------------------------------------------------------------------------
        string FileLocation = HttpContext.Current.Request.PhysicalApplicationPath + "CountryDataFile/GeoIP.dat";
        FileStream _FileStream = new FileStream(FileLocation, FileMode.Open, FileAccess.Read);
        MemoryStream _MemStream = new MemoryStream();

        byte[] _Byte = new byte[257];


        while (_FileStream.Read(_Byte, 0, _Byte.Length) != 0)
        {
            _MemStream.Write(_Byte, 0, _Byte.Length);

        }

        _FileStream.Close();

        return _MemStream;

    }

    public string LookupCountryCode(IPAddress _IPAddress)
    {

        //------------------------------------------------------------------------------------------------
        // Look up the country code, e.g. US, for the passed in IP Address
        //------------------------------------------------------------------------------------------------

        return CountryCode[Convert.ToInt32(SeekCountry(0, ConvertIPAddressToNumber(_IPAddress), 31))];

    }

    public string LookupCountryCode(string _IPAddress)
    {

        //------------------------------------------------------------------------------------------------
        // Look up the country code, e.g. US, for the passed in IP Address
        //------------------------------------------------------------------------------------------------

        IPAddress _Address = null;


        try
        {
            _Address = IPAddress.Parse(_IPAddress);

        }
        catch
        {
            return "--";
        }

        return LookupCountryCode(_Address);

    }

    public string LookupCountryName(IPAddress addr)
    {

        //------------------------------------------------------------------------------------------------
        // Look up the country name, e.g. United States, for the IP Address
        //------------------------------------------------------------------------------------------------

        return CountryName[Convert.ToInt32(SeekCountry(0, ConvertIPAddressToNumber(addr), 31))];

    }

    public string LookupCountryName(string _IPAddress)
    {

        //------------------------------------------------------------------------------------------------
        // Look up the country name, e.g. United States, for the IP Address
        //------------------------------------------------------------------------------------------------

        IPAddress _Address;


        try
        {
            _Address = IPAddress.Parse(_IPAddress);


        }
        catch (Exception ex)
        {
            ex.Message.ToString();
            return "N/A";

        }

        return LookupCountryName(_Address);

    }

    private long vbShiftLeft(long Value, int Count)
    {
        long functionReturnValue = 0;

        //------------------------------------------------------------------------------------------------
        // Replacement for Bitwise operators which are missing in VB.NET,
        // these functions are present in .NET 1.1, but for developers
        // using 1.0, replacement functions must be implemented
        //------------------------------------------------------------------------------------------------

        int _Iterator = 0;

        functionReturnValue = Value;


        for (_Iterator = 1; _Iterator <= Count; _Iterator++)
        {
            functionReturnValue = functionReturnValue * 2;

        }
        return functionReturnValue;

    }

    private long vbShiftRight(long Value, int Count)
    {
        long functionReturnValue = 0;

        //------------------------------------------------------------------------------------------------
        // Replacement for Bitwise operators which are missing in VB.NET,
        // these functions are present in .NET 1.1, but for developers
        // using 1.0, replacement functions must be implemented
        //------------------------------------------------------------------------------------------------

        int _Iterator = 0;

        functionReturnValue = Value;


        for (_Iterator = 1; _Iterator <= Count; _Iterator++)
        {
            functionReturnValue = functionReturnValue / 2;

        }
        return functionReturnValue;

    }

    private long SeekCountry(long StartOffset, long IPNumber, int SearchDepth)
    {

        //------------------------------------------------------------------------------------------------
        // Seek the country position from the GeoIP data in the memory stream
        //------------------------------------------------------------------------------------------------

        byte[] _Buffer = new byte[7];
        long[] x = new long[3];


        if (SearchDepth == 0)
        {
        }


        try
        {
            _MemoryStream.Seek(6 * StartOffset, 0);
            _MemoryStream.Read(_Buffer, 0, 6);


        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

        int _Iterator = 0;


        for (_Iterator = 0; _Iterator <= 1; _Iterator++)
        {
            x[_Iterator] = 0;

            int j = 0;


            for (j = 0; j <= 2; j++)
            {
                int _Part = _Buffer[(_Iterator * 3 + j)];


                if (_Part < 0)
                {
                    _Part += 256;
                }

                x[_Iterator] += vbShiftLeft(_Part, (j * 8));

            }

        }


        if ((IPNumber & vbShiftLeft(1, SearchDepth)) > 0)
        {

            if (x[1] >= CountryBegin)
            {
                return x[1] - CountryBegin;

            }

            return SeekCountry(x[1], IPNumber, SearchDepth - 1);


        }
        else
        {

            if (x[0] >= CountryBegin)
            {
                return x[0] - CountryBegin;

            }

            return SeekCountry(x[0], IPNumber, SearchDepth - 1);

        }

    }

}