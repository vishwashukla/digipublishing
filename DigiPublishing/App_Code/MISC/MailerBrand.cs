﻿using System;
using System.Data;
using System.Net.Mail;
using System.Text;

/// <summary>
/// Summary description for MailerBrand
/// </summary>
/// 
/////////////////////////////////////////////////////////////////////////////
// Name:
// Purpose:
// Author: Digipublishing.net limited
// Modified by:
// Created: 01/07/2008
// RCS-ID:
// Copyright: © Digipublishing.net limited, 2008
// Licence:
/////////////////////////////////////////////////////////////////////////////
public class MailerBrand
{
    public MailerBrand()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool sendRegistrationPwd(string userEmail, string strPassword, string strUserFName, string errorMessage, string strfromAddress)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string strURL = "http://" + ReadConfig.getDomainUrl() + "/";
        string strImgPath = "<img alt=\"logo\" src=\"" + strURL + "Config/" + ReadConfig.getDomainUrl() + "/logo.gif \" />";
        strURL = "<a href=\"" + strURL + "login.aspx\" target=\"_blank\">" + strURL + "login.aspx</a>";
        string strEmailTo = "<a href=\"mailto:" + strfromAddress + "\"> [" + strfromAddress + "]</a>";
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        SmtpClient smtpClient = new SmtpClient();
        MailMessage message = new MailMessage();
        try
        {
            MailAddress fromAddress = new MailAddress(strfromAddress);
            //From address will be given as a MailAddress Object
            message.From = fromAddress;
            // To address collection of MailAddress
            message.To.Add(userEmail);
            message.Subject = ReadConfig.getDomainUrl().Trim().Trim(':') + " - Registration email ";
            // You can specify Address directly as string
            //Body can be Html or text format
            //Specify true if it  is html message                          
            message.IsBodyHtml = true;
            // Message body content
            string strMailContent = "";
            StringBuilder strMessageBody = new StringBuilder();
            strMessageBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
            strMessageBody.Append("<html><head>");
            strMessageBody.Append("<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
            strMessageBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
            strMessageBody.Append("<style>");
            strMessageBody.Append("body { font-family:tahoma; font-size:12px;color:#000000} ");
            strMessageBody.Append("div.footer{background-color:#eeeeee;border:1px solid #000000;width:90%;font-family:tahoma; font-size:10px;font-weight:bold;color:#000000;padding:10px;}");
            strMessageBody.Append("</style>");
            strMessageBody.Append("</head>");
            strMessageBody.Append("<body bgColor=#f6f6f6>");
            strMessageBody.Append("<div>");
            DBCommand dbcmd = new DBCommand();
            dbcmd.CommandText = "sp_AlertTemplate";
            dbcmd.CommandType = CommandType.StoredProcedure;
            dbcmd.Parameters.Add("@temp_title", "After Registration Notification");
            DataTable dt = new DataTable();
            dt = dp.FillDataTable(dbcmd);
            if (dt.Rows.Count > 0)
            {
                strMailContent = utils.formatStringForDBSelect(dt.Rows[0]["temp_content"].ToString().Trim()); ;
                strMessageBody.Append("Hi " + strUserFName + ",");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("Please find your login details as follows:  ");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("User Email: " + userEmail + "<br />");
                strMessageBody.Append("Password: " + strPassword + "<br />");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("Please click on the following link to access the site:");
                strMessageBody.Append("<br/>");
                strMessageBody.Append(strURL);
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("Regards,");
                strMessageBody.Append("<br/>");
                strMessageBody.Append(ConfigLoader.GetAppName() + " Support Team");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<br/>");
                strMessageBody.Append("<img alt=\"logo\" src=\"http://" + ReadConfig.getDomainUrl() + "/Config/" + ReadConfig.getDomainUrl() + "/logo.gif \" />");
                strMessageBody.Append("<br/>");
            }
            strMessageBody.Append("<br/>");
            strMessageBody.Append("</div></body></html>");
            message.Body = strMessageBody.ToString();
            smtpClient.Send(message);
            return true;
        }
        catch (Exception ex)
        {
            errorMessage = "Error sending the welcome email to the user." + ex.Message;
            return false;
        }
    }

    public static bool sendPasswordResetEmail(string userEmail, string strPassword, string strUserFName, string errorMessage, string strfromAddress, string strhosturl)
    {
       
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string strURL = "http://" + ReadConfig.getDomainUrl() + "/";
        string strEmailTo = "<a href=\"mailto:" + strfromAddress + "\"> [" + strfromAddress + "]</a>";
        string strwebhosturl = ReadConfig.getDomainUrl();
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        SmtpClient smtpClient = new SmtpClient();
        MailMessage message = new MailMessage();
        MailAddress fromAddress = new MailAddress(strfromAddress);
        //From address will be given as a MailAddress Object
        message.From = fromAddress;      

        // To address collection of MailAddress
        message.To.Add(userEmail);
        message.Subject = ConfigLoader.GetAppName() + " Forgotten Password ";
        // You can specify Address directly as string
        //Body can be Html or text format
        //Specify true if it  is html message
        message.IsBodyHtml = true;
        // Message body content
        string strMailContent = "";
        StringBuilder strMessageBody = new StringBuilder();
        strMessageBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
        strMessageBody.Append("<html><head>");
        strMessageBody.Append("<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
        strMessageBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
        strMessageBody.Append("<style>");
        strMessageBody.Append("body { font-family:tahoma; font-size:12px;color:#000000} ");
        strMessageBody.Append("div.footer{background-color:#eeeeee;border:1px solid #000000;width:90%;font-family:tahoma; font-size:10px;font-weight:bold;color:#000000;padding:10px;}");
        strMessageBody.Append("</style>");
        strMessageBody.Append("</head>");
        strMessageBody.Append("<body bgColor=#f6f6f6>");
        strMessageBody.Append("<div>");
        ////your Message
        DBCommand dbcmd = new DBCommand();
        dbcmd.CommandText = "sp_AlertTemplate";
        dbcmd.CommandType = CommandType.StoredProcedure;
        dbcmd.Parameters.Add("@temp_title", "Forgot Password email");
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(dbcmd);
        if (dt.Rows.Count > 0)
        {
            strMailContent = utils.formatStringForDBSelect(dt.Rows[0]["temp_content"].ToString().Trim());
            strMessageBody.Append("Hi " + strUserFName + ",");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("Your login details for " + strhosturl + " are as follows: ");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("User Email: " + userEmail + "<br />");
            strMessageBody.Append("Password: " + strPassword + "<br />");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("Please click on the following link to access the " + strhosturl + " and re-enter your login details.");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<a href='http://" + strhosturl + "' target='_blank'>http://" + strhosturl + "/</a>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("If you continue to have issues with your login please send an e-mail to <a href=\"mailto:" + strfromAddress + "\"> [" + strfromAddress + "]</a>.");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("Please note that this password is a temporary machine generated password. It is highly recommended that you change your password once you are logged in to the system, this can be done from the \"Edit My Profile\" page.");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("Thank you,");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("Support Team");
            strMessageBody.Append("<br/>");
            strMessageBody.Append("<br/>");
        }
        strMessageBody.Append("<br/>");
        strMessageBody.Append("</div></body></html>");
        message.Body = strMessageBody.ToString();
        // Send SMTP mail
        smtpClient.Send(message);
        return true;
    }

    public static void sendInvitation(string TouserEmail, string userName)
    {
        SmtpClient smtpClient = new SmtpClient();
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        MailMessage message = new MailMessage();
        message.IsBodyHtml = true;
        MailAddress fromAddress = new MailAddress(ReadConfig.GetEmail());
        message.From = fromAddress;
        StringBuilder strBody = new StringBuilder();
        string strURL = "http://" + ReadConfig.getDomainUrl() + "/";
        string strImgPath = "<img alt=\"logo\" src=\"" + strURL + "Config/" + ReadConfig.getDomainUrl() + "/logo.gif \" />";
        strBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
        strBody.Append("<html><head>");
        strBody.Append("<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
        strBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
        strBody.Append("<style type='text/css'>");
        strBody.Append("body { font-family:arial;font-size:12px;color:#000000;} ");
        strBody.Append("div.footer{background-color:#eeeeee;border:1px solid #000000;width:90%;font-family:tahoma; font-size:10px;font-weight:bold;color:#000000;padding:10px;}");
        strBody.Append("</style>");
        strBody.Append(userName + " added you as a friend");
        strBody.Append("<br/ ><br/ >");
        strBody.Append("To reply accept or decline this invite, follow the link below:");
        strBody.Append("<br/ >");
        strBody.Append("<a href=\"" + strURL + "odfriendinvite/index.aspx?ID=Invitations\">" + strURL + "odfriendinvite/index.aspx?ID=Invitations</a>.");
        strBody.Append("<br/ ><br/ >");
        strBody.Append("Want to control which emails you receive from <agency name>? Go to:");
        strBody.Append("<br/ >");
        strBody.Append("<a href=\"" + strURL + "OnDemandProfile/index.aspx?ID=ShowPreferences\">" + strURL + "OnDemandProfile/index.aspx?ID=ShowPreferences </a>.");
        strBody.Append("<br/ ><br/ >");
        strBody.Append(strImgPath);
        message.To.Add(TouserEmail);
        message.Subject = userName + " added you as a friend on " + ReadConfig.getDomainUrl();
        AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody.ToString(), HTMLType);
        message.AlternateViews.Add(HTMLView);
        smtpClient.Send(message);
        message.Dispose();
    }

    public static void sendSubscribeEmail(string fromemailaddress)
    {
        string toemailaddress = ReadConfig.GetEmail().Trim();
        string domain = ReadConfig.getDomainUrl();

        SmtpClient smtpClient = new SmtpClient();
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        MailMessage message = new MailMessage();
        message.IsBodyHtml = true;
        string strSub = domain + " - Subscribe email";

        MailAddress toAddress = new MailAddress(toemailaddress);
        MailAddress fromAddress = new MailAddress(fromemailaddress);
        message.From = fromAddress;
        message.To.Add(toAddress);
        message.ReplyToList.Add(fromAddress);
        message.Subject = strSub;
        StringBuilder strBody = new StringBuilder();

        strBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
        strBody.Append("<html><head>");
        strBody.Append("<meta http-equiv=Content-Type content=\"text/html); charset=iso-8859-1\">");
        strBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
        strBody.Append("<style type='text/css'>");
        strBody.Append(" table { font-family:arial;font-size:12px;color:#000000;} ");
        strBody.Append("</style>");
        strBody.Append("</head>");
        strBody.Append("<body>");
        strBody.Append("<div><b>" + fromAddress + "</b></div>");
        strBody.Append("</body></html>");

        AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody.ToString(), HTMLType);
        message.AlternateViews.Add(HTMLView);

        try
        {
            smtpClient.Send(message);
        }
        catch
        {
        }
        finally
        {
            message.Dispose();
        }
    }

    public static void sendResetPasswordEmail(string email,string password)
    {
        string fromemailaddress = ReadConfig.GetEmail().Trim();
        string domain = ReadConfig.getDomainUrl();

        SmtpClient smtpClient = new SmtpClient();
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        MailMessage message = new MailMessage();
        message.IsBodyHtml = true;
        string strSub = domain + " - Password reset";

        MailAddress toAddress = new MailAddress(email);
        MailAddress fromAddress = new MailAddress(fromemailaddress);
        message.From = fromAddress;
        message.To.Add(toAddress);
        message.ReplyToList.Add(fromAddress);
        message.Subject = strSub;
        StringBuilder strBody = new StringBuilder();

        strBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
        strBody.Append("<html><head>");
        strBody.Append("<meta http-equiv=Content-Type content=\"text/html); charset=iso-8859-1\">");
        strBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
        strBody.Append("<style type='text/css'>");
        strBody.Append(" table { font-family:arial;font-size:12px;color:#000000;} ");
        strBody.Append("</style>");
        strBody.Append("</head>");
        strBody.Append("<body>");
        strBody.Append("<div><b>New Password: </b>"+password+"</div>");
        strBody.Append("</body></html>");

        AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody.ToString(), HTMLType);
        message.AlternateViews.Add(HTMLView);

        try
        {
            smtpClient.Send(message);
        }
        catch
        {
        }
        finally
        {
            message.Dispose();
        }
    }

    public static bool SendEmailMessage(string strFrom, string strTo, string strSubject, string strbody)
    {
        bool IsOk = false;
        try
        {
            SmtpClient smtpClient;
            string smtpServer = "localhost";
            string FromName = strFrom;
            string SendTo = strTo;
            string Subject = strSubject;
            string str_body = "<html><body><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"2\">"
                        + strbody + "</font></body></html>";
            MailMessage MailMsg = new MailMessage(FromName, SendTo, Subject, str_body);
            MailMsg.BodyEncoding = Encoding.Default;
            MailMsg.Priority = MailPriority.High;
            MailMsg.IsBodyHtml = true;
            smtpClient = new SmtpClient(smtpServer);
            smtpClient.Send(MailMsg);
            IsOk = true;
        }
        catch (Exception ex)
        {
        }
        return IsOk;
    }
}
