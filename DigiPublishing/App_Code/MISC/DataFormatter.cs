using System;
using System.Collections;
using System.Globalization;

	/// <summary>
	/// Summary description for DataFormatter.
	/// </summary>
	public class DataFormatter
	{
		private string search;
		private string sub;
		private string result;
		private string var;
		private string inputString;
		private int i;
		private string[] strReplace = new string[2];
		private ArrayList alReplace = new ArrayList();
		public static string[] monthArray = {"January", 
												"Feberaury",
												"March",
												"April",
												"May",
												"June",
												"July",
												"August",
												"September",
												"October",
												"November",
												"December"};



		public DataFormatter()
		{
			alReplace = new ArrayList();
			result = " ";
			var = " ";
			inputString = " ";
			i = 0;
		}

		public void setDefaultInputSearch()
		{
			alReplace = new ArrayList();
            strReplace = new String[2];
            strReplace[0] = "'s";
            strReplace[1] = "&rsquo;s";
            alReplace.Add(strReplace);

            strReplace = new String[2];
            strReplace[0] = "'";
            strReplace[1] = "&rsquo;";
            alReplace.Add(strReplace);

            strReplace = new String[2];
            strReplace[0] = "\"";
            strReplace[1] = "&quot;";
            alReplace.Add(strReplace);
            
		}
    
		public void clearDefaultSearch()
		{
			alReplace = new ArrayList();
		}
    
		public void setDefaultOutputSearch()
		{
			alReplace = new ArrayList();
            strReplace = new String[2];
            strReplace[0] = "&rsquo;s";
            strReplace[1] = "'s";
            alReplace.Add(strReplace);

            strReplace = new String[2];
            strReplace[0] = "&rsquo;";
            strReplace[1] = "'";
            alReplace.Add(strReplace);

            strReplace = new String[2];
            strReplace[0] = "&quot;";
            strReplace[1] = "\"";
            alReplace.Add(strReplace);
			
			strReplace = new String[2];
            strReplace[0] = "&#0146;";
            strReplace[1] = "'";
            alReplace.Add(strReplace);

		}
       
		public void setSearch(String strSearch, String strReplaceVal)
		{
			strReplace = new String[2];
			strReplace[0] = strSearch;
			strReplace[1] = strReplaceVal;
			alReplace.Add(strReplace);
		}

		public ArrayList getSearch()
		{
			return alReplace;
		}

		public void setInputString(string strInput)
		{
			inputString = strInput;
		}

		public string getInputString()
		{
			var = inputString;
			for(int j=0;j<alReplace.Count;j++) 
			{
				strReplace = (string[]) alReplace[j];
				search = strReplace[0].Trim();
				sub = strReplace[1].Trim();
				do
				{
					i = var.IndexOf(search);
					if(i != -1)
					{
						result = var.Substring(0, i);
						result = result + sub;
						result = result + var.Substring(i + search.Length);
						var = result;
					}
				} while(i != -1);                                         
			}
			inputString = var;
			var = "";
			return inputString;
		}
    
		public String getInputString(String strInputString)
		{
			var = strInputString;
			String strOutput = "";
			for(int j=0;j<alReplace.Count;j++) 
			{
				strReplace = (string[])alReplace[j];
				search = strReplace[0].Trim();
				sub = strReplace[1].Trim();
				do
				{
					i = var.IndexOf(search);
					if(i != -1)
					{
						result = var.Substring(0, i);
						result = result + sub;
						result = result + var.Substring(i + search.Length);
						var = result;
					}
				} while(i != -1);                                         
			}
			strOutput = var;
			var = "";
			return strOutput ;
		}

		public static string getDate(string format)
		{
			DateTime dt = System.DateTime.Today;
			return getDate(format,dt);
		}

		public static string getDate(string format, DateTime dt)
		{
			string formattedDate = dt.ToString(format);			
			return formattedDate;
		}

		public static DateTime ParseStringToDate(string strDate, string strFormat)
		{
			CultureInfo ci = new CultureInfo("sl-SI", true); 
			ci.DateTimeFormat.DateSeparator = "/"; 
			ci.DateTimeFormat.ShortDatePattern = strFormat; //"MM/dd/yyyy";
			DateTime dtVar = DateTime.Parse(strDate,ci);
			return dtVar;
		}
	}
