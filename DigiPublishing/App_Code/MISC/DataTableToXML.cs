﻿using System;
using System.Data;
using System.IO;

/// <summary>
/// Summary description for DataTableToXML
/// </summary>
public class DataTableToXML
{
    public static string ConvertDataTableToXml(DataTable table)
    {
        DataTable dt = SetValueForNull(table);
        string xmlString = string.Empty;
        using (TextWriter writer = new StringWriter())
        {
            dt.WriteXml(writer, true);
            xmlString = writer.ToString();
        }
        return xmlString;
    }

    private static DataTable SetValueForNull(DataTable dt)
    {
        int i, j;
        for (i = 0; i < dt.Columns.Count; i++)
        {
            for (j = 0; j < dt.Rows.Count; j++)
            {
                if (dt.Columns[i].DataType.ToString() == "System.Int32" || dt.Columns[i].DataType.ToString() == "System.Single" || dt.Columns[i].DataType.ToString() == "System.Double" || dt.Columns[i].DataType.ToString() == "System.Decimal")
                {
                    if (dt.Rows[j][i] == DBNull.Value)
                        dt.Rows[j][i] = 0;
                }
                else if (dt.Columns[i].DataType.ToString() == "System.String")
                {
                    if (dt.Rows[j][i] == DBNull.Value || dt.Rows[j][i].ToString().Trim() == "")
                        dt.Rows[j][i] = "";
                }
            }
        }
        return dt;
    }
}