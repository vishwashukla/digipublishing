using System.Text.RegularExpressions;

/// <summary>
/// Summary description for vCardReader
/// </summary>
public class vCardReader
{
    public vCardReader()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region Singlar Properties

    private string _name;
    private string _email;
    private string _summary;
    private string _descrition;
    private string _dstart;
    private string _dend;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string Email
    {
        get { return _email; }
        set { _email = value; }
    }
    
    public string Summary
    {
        get { return _summary; }
        set { _summary = value; }
    }

    public string DESCRIPTION
    {
        get { return _descrition; }
        set { _descrition = value; }
    }

    public string DTSTART
    {
        get { return _dstart; }
        set { _dstart = value; }
    }

    public string DTEND
    {
        get { return _dend; }
        set { _dend = value; }
    }
    #endregion

    /// <summary>
    /// Analyze vCard structures.
    /// </summary>
    public void ParseLines(string s)
    {
        RegexOptions options = RegexOptions.IgnoreCase |
            RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace;

        Regex regex;
        Match m;

        ///Name
        regex = new Regex(@"(?<strElement>(Name))   (:(?<strName>[^\n\r]*))", options);
        m = regex.Match(s);
        if (m.Success)
            Name = m.Groups["strName"].Value;

        ///Emails
        regex = new Regex(@"(?<strElement>(Email))   (:(?<strEmail>[^\n\r]*))", options);
        m = regex.Match(s);
        if (m.Success)
            Email = m.Groups["strEmail"].Value;


        regex = new Regex(@"(?<strElement>(SUMMARY))   (:(?<strSUMMARY>[^\n\r]*))", options);
        m = regex.Match(s);
        if (m.Success)
            Summary = m.Groups["strSUMMARY"].Value;
        regex = new Regex(@"((?<strElement>(DESCRIPTION)) 
                    (;*(?<strAttr>(HOME|WORK)))*  (;(?<strPref>(PREF)))* 
                    (;[^:]*)*  (:(?<strDESCRIPTION>[^\n\r]*)))", options);

        m = regex.Match(s);
        if (m.Success)
            DESCRIPTION = m.Groups["strDESCRIPTION"].Value;

        ////DStarts
        regex = new Regex(@"((?<strElement>(DTSTART)) 
                    (;*(?<strAttr>(HOME|WORK)))*  (;(?<strPref>(PREF)))* 
                    (;[^:]*)*  (:(?<strDTSTART>[^\n\r]*)))", options);
        m = regex.Match(s);
        if (m.Success)
            DTSTART = m.Groups["strDTSTART"].Value;

        ////DEnd
        regex = new Regex(@"((?<strElement>(DTEND)) 
                    (;*(?<strAttr>(HOME|WORK)))*  (;(?<strPref>(PREF)))* 
                    (;[^:]*)*  (:(?<strDTEND>[^\n\r]*)))", options);
        m = regex.Match(s);
        if (m.Success)
            DTEND = m.Groups["strDTEND"].Value;
    }
}
