﻿
public enum features
{
    Calendar = 3,
    Blog = 4,
    RSS = 5,
    Bookmark = 6,
    Search = 7,
    RSSSummary = 8,
    BookMarkSummary = 9,
    MostPopular = 10,
    BlogPostSummary = 11,
    registerlogin = 12,
    Share = 20,
    Subscribe = 21,
}