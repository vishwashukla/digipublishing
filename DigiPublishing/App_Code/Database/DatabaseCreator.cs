﻿using System;

using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;

/// <summary>
/// Summary description for DatabaseCreator
/// </summary>
/////////////////////////////////////////////////////////////////////////////
// Name:
// Purpose:
// Author: Digipublishing.net limited
// Modified by:
// Created: 01/07/2008
// RCS-ID:
// Copyright: © Digipublishing.net limited, 2008
// Licence:
/////////////////////////////////////////////////////////////////////////////
public class DatabaseCreator
{
    private string DBServer = "";
    private string DBName = "";
    protected bool bolDidPreviouslyConnect = false;
    private string SQL_CONNECTION_STRING = ConfigLoader.GetConnectionString();
    protected string connectionString;
    private string fileUrl = "";
    private int timeout = 600;

    public DatabaseCreator()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private bool CreateInstanceDb(string strUname, string strPassword)
    {
        string strSQL = "IF EXISTS (SELECT [name] FROM Master..sysdatabases WHERE [name] = N'" + DBName + "')" +
                                " BEGIN" +
                                " EXEC dbo.clearDBUsers [" + DBName + "]" +
                                " DROP DATABASE [" + DBName + "]" +
                                " END" +
                                " CREATE DATABASE [" + DBName + "]";


        bool bolIsConnecting = true;
        while (bolIsConnecting)
        {
            try
            {
                //' The SqlConnection class allows you to communicate with SQL Server.
                //' The constructor accepts a connection string as an argument.  This
                //' connection string uses Integrated Security, which means that you 
                //' must have a login in SQL Server, or be part of the Administrators
                //' group for this to work.
                SqlConnection northwindConnection = new SqlConnection(SQL_CONNECTION_STRING);

                //' A SqlCommand object is used to execute the SQL commands.
                SqlCommand cmd = new SqlCommand(strSQL, northwindConnection);

                //' Open the connection, execute the command, and close the 
                //' connection. It is more efficient to ExecuteNonQuery when data is 
                //' not being returned.
                northwindConnection.Open();
                cmd.ExecuteNonQuery();
                northwindConnection.Close();

                //' Data has been successfully submitted, so break out of the loop
                //' and close the status form.
                bolIsConnecting = false;
                bolDidPreviouslyConnect = true;
                //'bolDidCreateTable = True
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
            catch (Exception)
            {
                if (connectionString == SQL_CONNECTION_STRING)
                { }
                else
                {
                    return false;
                }
            }
        }
        return true;
    }

    //Public Function CreateSQLDb(ByVal strServerName As String, ByVal strDBName As String, Optional ByVal strUserName As String = "", Optional ByVal strPass As String = "") As Boolean
    public bool CreateSQLDb(string strServerName, string strDBName, string strUserName, string strPass)
    {
        DBName = strDBName;
        DBServer = strServerName;
        if (strUserName.Trim().Length > 0)
        {
            SQL_CONNECTION_STRING = "User ID=" + strUserName + ";Pwd=" + strPass + ";Data Source=" + strServerName + ";database=;Persist Security Info=False;";
        }
        else
        {
            SQL_CONNECTION_STRING = "Server=" + DBServer + ";" + "DataBase=;" + "Integrated Security=SSPI;";
        }
        connectionString = SQL_CONNECTION_STRING;
        if (CreateInstanceDb(strUserName, strPass))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool ExecuteSQLScript(string SQlFilePath, string ConStr)
    {
        SqlConnection conn = null;
        try
        {
            //' read file 
            fileUrl = SQlFilePath;
            WebRequest request = WebRequest.Create(fileUrl);
            StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());

            //' Create new connection to database 
            conn = new SqlConnection(ConStr);
            conn.Open();

            while (!sr.EndOfStream)
            {
                StringBuilder sb = new StringBuilder();
                SqlCommand cmd = conn.CreateCommand();

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    if (s != null && s.ToUpper().Trim().Equals("GO"))
                    {
                        break;
                    }
                    sb.AppendLine(s);
                }

                //' Execute T-SQL against the target database 
                cmd.CommandText = sb.ToString();
                cmd.CommandTimeout = timeout;
                cmd.ExecuteNonQuery();
            }
            conn.Close();
            return true;
        }
        //End Using
        //'' Done
        //Return True
        catch (Exception)
        {
            //' Error            
            return false;
        }
    }
}
