using System;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.SqlClient;

public enum DBSType
{
    MSSQL,
    OLEDB,
    ODBC
}

public class DBHelper
{

    // Define a static logger variable so that it references the
    // Logger instance named "MyApp".
    //private static readonly ILog log = LogManager.GetLogger(typeof(DBHelper));

    #region Variables
    /// <summary>
    /// Database type
    /// </summary>
    public DBSType _databaseType = DBSType.MSSQL;

    /// <summary>
    /// Connection string
    /// </summary>
    public string _connectionString = "";
    #endregion

    #region Properties
    /// <summary>
    /// Get or set database type
    /// </summary>
    public DBSType DatabaseType
    {
        get { return _databaseType; }
        set { this._databaseType = value; }
    }

    /// <summary>
    /// Get or set connection sting
    /// </summary>
    public string ConnectionString
    {
        get { return _connectionString; }
        set { this._connectionString = value; }
    }
    #endregion

    #region Constructor
    /// <summary>
    /// Contructor for DB helper 
    /// </summary>
    /// <param name="strConnectionString">Connection string for connecting to the database</param>
    /// <param name="dbtType">Type Database to use</param>
    public DBHelper(string strConnectionString, DBSType dbtType)
    {
        this.ConnectionString = strConnectionString;
        this.DatabaseType = dbtType;
    }


    public DBHelper()
    {
    }
    #endregion

    #region Public Members

    #region ExecuteNonQuery



    public int ExecuteNonQuery(string strCommandText)
    {
        if (strCommandText == null || strCommandText.Length == 0)
        {
            ////log.Error("DBHelper:Public Members:ExecuteNonQuery:: null command text");
            throw new ArgumentNullException("CommandText");
        }
        DBCommand currCmd = new DBCommand();
        currCmd.CommandText = strCommandText;
        currCmd.CommandType = CommandType.Text;
        return ExecuteNonQuery(currCmd);
    }

    public int ExecuteNonQuery(DBCommand dbcCommand)
    {
        return ExecuteNonQuery(dbcCommand, null);
    }

    public int ExecuteNonQuery(DBCommand dbcCommand, IDbTransaction transaction)
    {
        int retVal = -1;
        using (IDbConnection conn = this.GetConnection())
        {
            conn.Open();
            IDbCommand cmd = this.GetCommand(dbcCommand);
            cmd.Connection = conn;
            cmd.Transaction = transaction;
            retVal = cmd.ExecuteNonQuery();
            conn.Close();
        }
        return retVal;
    }
    #endregion

    #region ExecuteScalar
    public object ExecuteScalar(string strCommandText)
    {
        if (strCommandText == null || strCommandText.Length == 0)
        {
            ////log.Error("DBHelper:Public Members:ExecuteScalar:: null command text");
            throw new ArgumentNullException("CommandText");
        }
        DBCommand currCmd = new DBCommand();
        currCmd.CommandText = strCommandText;
        currCmd.CommandType = CommandType.Text;
        return ExecuteScalar(currCmd);
    }

    public object ExecuteScalar(DBCommand dbcCommand)
    {
        return ExecuteScalar(dbcCommand, null);
            
    }

    public object ExecuteScalar(DBCommand dbcCommand, IDbTransaction transaction)
    {
        object retval = null;
        using (IDbConnection conn = this.GetConnection())
        {
            conn.Open();
            IDbCommand cmd = this.GetCommand(dbcCommand);
            cmd.Connection = conn;
            cmd.Transaction = transaction;
            retval = cmd.ExecuteScalar();
            conn.Close();
        }
        return retval;
    }
        
    #endregion

    #region ExecuteReader

    public IDataReader ExecuteReader(string strCommandText)
    {
        if (strCommandText == null || strCommandText.Length == 0)
        {
                throw new ArgumentNullException("CommandText");
        }
        DBCommand currCmd = new DBCommand();
        currCmd.CommandText = strCommandText;
        currCmd.CommandType = CommandType.Text;
        return ExecuteReader(currCmd);
    }

    public IDataReader ExecuteReader(DBCommand dbcCommand)
    {
        return ExecuteReader(dbcCommand, null);
    }

    public IDataReader ExecuteReader(DBCommand dbcCommand, IDbTransaction transaction)
    {
        IDataReader retReader;
        IDbConnection conn = this.GetConnection();          
        conn.Open();
        IDbCommand cmd = this.GetCommand(dbcCommand);
        cmd.Connection = conn;
        cmd.Transaction = transaction;
        retReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
               
        return retReader;
    }



    #endregion

    #region FillDatatable

    public DataTable FillDataTable(string strCommandText)
    {
        if (strCommandText == null || strCommandText.Length == 0)
        {
            //log.Error("DBHelper:Public Members:FillDataTable:: null command text");
            throw new ArgumentNullException("CommandText");
        }
        DBCommand currCmd = new DBCommand();
        currCmd.CommandText = strCommandText;
        currCmd.CommandType = CommandType.Text;
        return FillDataTable(currCmd);          
    }

    public DataTable FillDataTable(DBCommand dbcCommand)
    {
        IDbCommand cmd = this.GetCommand(dbcCommand);
        cmd.Connection = this.GetConnection();
        DataTable dt = null;
        try
        {
            IDataAdapter dataAdapter = this.GetDataAdapter(cmd);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dt = ds.Tables[0];
        }
        catch(Exception ex) 
        {
            throw new Exception(ex.Message);
        }
        return dt;
    }

    #endregion

    #region Clean Datatable for Null
        
    public DataTable CleanDataTable(DataTable dtIn)
    {
        for (int j = 0; j < dtIn.Rows.Count; j++)
        {
            DataRow dr = dtIn.Rows[j];
            for (int i = 0; i < dr.ItemArray.Length; i++)
            {
                if (dr[i] == null || dr[i].ToString().ToLower().Trim() == "null")
                    dr[i] = "";
            }
            dr.AcceptChanges();
        }
        dtIn.AcceptChanges();            
        return dtIn;
    }
        
    #endregion

    #region FillDataSet

    public DataSet FillDataSet(string strCommandText)
    {
        if (strCommandText == null || strCommandText.Length == 0)
        {
            //log.Error("DBHelper:Public Members:FillDataSet:: null command text");
            throw new ArgumentNullException("CommandText");
        }
        DBCommand currCmd = new DBCommand();
        currCmd.CommandText = strCommandText;
        currCmd.CommandType = CommandType.Text;
        return FillDataSet(currCmd);    

    }

    public DataSet FillDataSet(DBCommand dbcCommand)
    {
        IDbCommand cmd = this.GetCommand(dbcCommand);
        cmd.Connection = this.GetConnection();
        DataSet ds = new DataSet();
        try
        {
            IDataAdapter dataAdapter = this.GetDataAdapter(cmd);
            dataAdapter.Fill(ds);  
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return ds;

    }
    #endregion

    #region DiscoverSpParameterSet

    public IDbDataParameter[] DiscoverSpParameterSet(string strSpName, bool includeReturnValueParameter)
    {
        if (strSpName == null || strSpName.Length == 0)
        {
            //log.Error("DBHelper:Public Members:DiscoverSpParameterSet:: null sp Name text");
            throw new ArgumentNullException("CommandText");
        }

        DBCommand dbCmd = new DBCommand();
        dbCmd.CommandText = strSpName;
        dbCmd.CommandType = CommandType.StoredProcedure;

        IDbCommand cmd = this.GetCommand(dbCmd);

        IDbConnection connection = this.GetConnection();
        cmd.Connection = connection;
        connection.Open();
        switch (this.DatabaseType)
        {
            case DBSType.MSSQL:
                SqlCommandBuilder.DeriveParameters((SqlCommand)cmd);
                break;
            case DBSType.OLEDB:
                OleDbCommandBuilder.DeriveParameters((OleDbCommand)cmd);
                break;
            case DBSType.ODBC:
                OdbcCommandBuilder.DeriveParameters((OdbcCommand)cmd);
                break;
            default:
                SqlCommandBuilder.DeriveParameters((SqlCommand)cmd);
                break;
        }
        connection.Close();

        if (!includeReturnValueParameter)
        {
            cmd.Parameters.RemoveAt(0);
        }

        IDbDataParameter[] discoveredParameters = new IDbDataParameter[cmd.Parameters.Count];

        cmd.Parameters.CopyTo(discoveredParameters, 0);

        // Init the parameters with a DBNull value
        foreach (IDbDataParameter discoveredParameter in discoveredParameters)
        {
            discoveredParameter.Value = DBNull.Value;
        }
        return discoveredParameters;
    }
    #endregion

    #endregion

    #region Private Members

    /// <summary>
    /// Method to get the valid databse connection 
    /// </summary>
    /// <returns>Database connection based on the database type(of type DBSType) selected</returns>
    private IDbConnection GetConnection()
    {
        if (_connectionString == null || _connectionString.Length == 0)
        {
            //log.Error("DBHelper:Private Members:GetConnection:: null connection string");
            throw new ArgumentNullException("connectionString");
        }

        switch (this.DatabaseType)
        {
            case DBSType.MSSQL:
                return new SqlConnection(this.ConnectionString);
            //break;
            case DBSType.OLEDB:
                return new OleDbConnection(this.ConnectionString);
            //break;
            case DBSType.ODBC:
                return new OdbcConnection(this.ConnectionString);
            // break;
            default:
                return new SqlConnection(this.ConnectionString);
            //break;
        }
    }

    private IDbCommand GetCommand(DBCommand dbcCommand)
    {
        if (dbcCommand == null)
        {
            //log.Error("DBHelper:Private Members:GetCommand:: null DBCommand");
            throw new ArgumentNullException("DBCommand");
        }

        if (dbcCommand.CommandText == null || dbcCommand.CommandText.Length == 0)
        {
            //log.Error("DBHelper:Private Members:GetCommand:: null CommandText");
            throw new ArgumentNullException("CommandText");
        }
        return GetCommand(dbcCommand, null);
    }

    private IDbCommand GetCommand(DBCommand dbcCommand, IDbTransaction transaction)
    {
        IDbCommand cmd = null;
        if (transaction != null)
        {
            if (transaction.Connection == null)
            {
                //log.Error("DBHelper:Private Members:GetCommand:: null tranaction not open");
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            }
        }
            
        switch (this.DatabaseType)
        {
            case DBSType.MSSQL:
                cmd = new SqlCommand();
                cmd.CommandText = dbcCommand.CommandText;
                cmd.CommandType = dbcCommand.CommandType;
                cmd.Connection = this.GetConnection();
                cmd.Transaction = transaction;

                foreach (string key in dbcCommand.Parameters.Keys)
                {
                    SqlParameter sqlParam = new SqlParameter();
                    sqlParam.ParameterName = key;
                    sqlParam.SqlValue = dbcCommand.Parameters[key];
                    cmd.Parameters.Add(sqlParam);
                }
                dbcCommand.Parameters.Clear();

                return cmd;
            //break;
            case DBSType.OLEDB:
                cmd = new OleDbCommand();
                cmd.CommandText = dbcCommand.CommandText;
                cmd.CommandType = dbcCommand.CommandType;
                cmd.Connection = this.GetConnection();
                cmd.Transaction = transaction;

                foreach (string key in dbcCommand.Parameters.Keys)
                {
                    OleDbParameter sqlParam = new OleDbParameter();
                    sqlParam.ParameterName = key;
                    sqlParam.Value = dbcCommand.Parameters[key];
                    cmd.Parameters.Add(sqlParam);
                }
                dbcCommand.Parameters.Clear();
                return cmd;
            //break;
            case DBSType.ODBC:
                cmd = new OdbcCommand();
                cmd.CommandText = dbcCommand.CommandText;
                cmd.CommandType = dbcCommand.CommandType;
                cmd.Connection = this.GetConnection();
                cmd.Transaction = transaction;

                foreach (string key in dbcCommand.Parameters.Keys)
                {
                    OdbcParameter sqlParam = new OdbcParameter();
                    sqlParam.ParameterName = key;
                    sqlParam.Value = dbcCommand.Parameters[key];
                    cmd.Parameters.Add(sqlParam);
                }
                dbcCommand.Parameters.Clear();
                return cmd;
            //break;
            default:
                cmd = new SqlCommand();
                cmd.CommandText = dbcCommand.CommandText;
                cmd.CommandType = dbcCommand.CommandType;
                cmd.Connection = this.GetConnection();
                cmd.Transaction = transaction;

                foreach (string key in dbcCommand.Parameters.Keys)
                {
                    SqlParameter sqlParam = new SqlParameter();
                    sqlParam.ParameterName = key;
                    sqlParam.SqlValue = dbcCommand.Parameters[key];
                    cmd.Parameters.Add(sqlParam);
                }
                dbcCommand.Parameters.Clear();
                return cmd;
            //break;
        }
    }

    private IDataAdapter GetDataAdapter(IDbCommand idbCommand)
    {
        IDataAdapter dataAdapter = null;
        switch (this.DatabaseType)
        {
            case DBSType.MSSQL:
                dataAdapter =  new SqlDataAdapter((SqlCommand)idbCommand);
                break;
            case DBSType.OLEDB:
                dataAdapter = new OleDbDataAdapter((OleDbCommand)idbCommand);
                break;
            case DBSType.ODBC:
                dataAdapter = new OdbcDataAdapter((OdbcCommand)idbCommand);
                break;
            default:
                dataAdapter = new SqlDataAdapter((SqlCommand)idbCommand);
                break;
        }
        return dataAdapter;
    }
    #endregion

    public bool CheckIfDBExist(string strDomain)
    {
        bool dbexist = false;
        string connectionstring = ReadConfig.GetConnectionString(strDomain);
        string DBName = connectionstring.Replace("Initial Catalog=", "^").Split('^')[1].Split(';')[0].Trim();
        connectionstring = connectionstring.Replace(DBName, "master");
        string query = "DECLARE @dbname nvarchar(128) SET @dbname = N'" + DBName 
            + "' IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = @dbname OR name = @dbname))) "
            + "SELECT '1' AS Data ELSE SELECT '0' AS Data";

        DBHelper dp = new DBHelper(connectionstring, DBSType.MSSQL);
        DataTable dt = dp.FillDataTable(query);
        if(dt.Rows.Count > 0)
        {
            if(dt.Rows[0]["data"].ToString().Trim() == "1")
            {
                dbexist = true;
            }
        }
        
        return dbexist;
    }
}