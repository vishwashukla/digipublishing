using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Security.Cryptography;

/// <summary>
/// Containing functions,which are frequently used throughout the application
/// </summary>
public class utils
{
    public utils()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// Methos to execute Select query
    /// </summary>
    /// <param name="QueryString">SQL query string</param>
    /// <param name="lblMsg">Label control in which the error message is to be display</param>
    /// <returns>Retrun datatable of the content recived on execution of Query</returns>
    public static DataTable RunQuery(string QueryString, Label lblMsg)
    {
        string errorMessage = "";
        bool bIsError = false;
        DataTable dt = RunQuery(QueryString, errorMessage, bIsError);
        if (bIsError)
        {
            lblMsg.Text = errorMessage;
        }
        return dt;
    }

    /// <summary>
    /// Method to execute Select query
    /// </summary>
    /// <param name="QueryString">SQL query string</param>
    /// <param name="errorMessage">Error message to display</param>
    /// <param name="bIsError">boolean variable, set true when error occurs</param>
    /// <returns>Retruns datatable of the content received from execution of Query</returns>
    public static DataTable RunQuery(string QueryString, string errorMessage, bool bIsError)
    {
        string ConnectionString = ReadConfig.GetConnectionString();
        SqlConnection DBConnection = new SqlConnection(ConnectionString);
        SqlDataAdapter DBAdapter;
        DataTable dt = new DataTable();
        try
        {
            // Run the query and create a DataSet.
            DBAdapter = new SqlDataAdapter(QueryString, DBConnection);
            DBAdapter.Fill(dt);
            // Close the database connection.
            DBConnection.Close();
        }
        catch (Exception ex)
        {
            // Close the database connection if it is still open.
            if (DBConnection.State == ConnectionState.Open)
                DBConnection.Close();
            errorMessage = "Error while executing query "
                + QueryString
                + ". The error returned is :: "
                + ex.Message;
            bIsError = true;
        }
        return dt;
    }

    /// <summary>
    /// Method to execute Insert/Delete/Update query
    /// </summary>
    /// <param name="QueryString">SQL querystring</param>
    /// <param name="errorMessage">Label control to display error</param>
    /// <returns>Return true if the query was executed successfully otherwise false</returns>
    public static bool ExecuteNonQuery(String QueryString, Label errorMessage)
    {
        string ConnectionString = ReadConfig.GetConnectionString();
        SqlCommand command = new SqlCommand();
        try
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                command.Connection = connection;
                command.CommandText = QueryString;
                command.CommandType = CommandType.Text;
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
            }

        }
        catch (Exception ex)
        {
            if (command.Connection.State == ConnectionState.Open)
                command.Connection.Close();
            errorMessage.Text = "Error while executing query "
                + QueryString
                + ". The error returned is :: "
                + ex.Message;
            return false;
        }
        return true;
    }

    /// <summary>
    /// Methods to format String to save in db
    /// </summary>
    /// <param name="strVal">String which format has to change</param>
    /// <returns>Retruns Formatted String</returns> 
    public static string formatStringForDBInsertWidget(string strVal)
    {
        DataFormatter df = new DataFormatter();
        df.setDefaultInputSearch();
        string strOut = df.getInputString(strVal);
        return strOut;
    }

    public static string formatStringForDBInsert(string strVal)
    {
        strVal = strVal.Replace("<a", "<a target='_blank'");
        strVal = strVal.Replace("<A", "<A target='_blank'");

        strVal = strVal.Replace(" href=\"", "href=\"../redirect.aspx?url=");
        strVal = strVal.Replace(" href='", "href='../redirect.aspx?url=");
        strVal = strVal.Replace(" HREF=\"", "HREF=\"../redirect.aspx?url=");
        strVal = strVal.Replace(" HREF='", "HREF='../redirect.aspx?url=");
        DataFormatter df = new DataFormatter();
        df.setDefaultInputSearch();
        string strOut = df.getInputString(strVal);
        return strOut;
    }

    /// <summary>
    /// Methods to get the formatted String in Original form.
    /// </summary>
    /// <param name="strVal">String which format has to be rechanged.</param>
    /// <returns>Retruns Original String</returns>
    public static string formatStringForDBSelect(string strVal)
    {
        DataFormatter df = new DataFormatter();
        df.setDefaultOutputSearch();
        string strOut = df.getInputString(strVal);
        return strOut;
    }

    static Random random = new Random();
    public static string GenerateRandomCode()
    {
        string s = "";
        for (int i = 0; i < 6; i++)
            s = String.Concat(s, random.Next(10).ToString());
        return s;
    }

    /// <summary>
    /// Method to execute Insert/Delete/Update query
    /// </summary>
    /// <param name="QueryString">SQL querystring</param>
    /// <param name="errorMessage">Label control to display error</param>
    /// <returns>Return true if the query was executed successfully otherwise false</returns>
    public static bool ExecuteNonQueryTrans(String QueryString, Label errorMessage)
    {
        string ConnectionString = ReadConfig.GetConnectionString();
        SqlTransaction myTransaction = default(SqlTransaction);
        try
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                myTransaction = connection.BeginTransaction();
                SqlCommand mCmd = new SqlCommand();
                mCmd.Connection = connection;
                mCmd.Transaction = myTransaction;
                mCmd.CommandText = QueryString;
                mCmd.CommandType = CommandType.Text;
                mCmd.ExecuteNonQuery();
                myTransaction.Commit();
            }
        }
        catch (Exception ex)
        {
            myTransaction.Rollback();
            errorMessage.Text = "Error while executing query "
                + QueryString
                + ". The error returned is :: "
                + ex.Message;
            return false;
        }
        return true;
    }

    public static String buildPagination(String totalResults, int startindex, int totalitems, string pagename)
    {
        int aInt = Int32.Parse(totalResults);
        StringBuilder pagination = new StringBuilder();
        int selectedPage = 1;
        int start_index = 1;
        int num_pages = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(aInt) / totalitems));

        selectedPage = (startindex - 1) / totalitems;
        String ret_qstr = "";
        foreach (string key in HttpContext.Current.Request.QueryString)
        {
            String value = HttpContext.Current.Request.QueryString[key];
            if (!(key.Equals("startIndex") || key.Equals("devkey")))
            {
                if (!key.Equals("FD"))
                {
                    ret_qstr += key + "=" + value + "&";
                }
            }
        }
        ret_qstr = ret_qstr.Replace("&", "&amp;");

        for (int jj = 0; jj <= num_pages - 1; jj++)
        {
            //for each page, increment the start-index by page size
            String page_url = ret_qstr + "startIndex=" + start_index;
            int pnum = jj + 1;
            if (pnum <= num_pages)
            {
                if (selectedPage == (pnum - 1))
                {
                    pagination.Append("<a class=\"pagination-clicked\" > " + pnum + " </a>");
                }
                else
                {
                    pagination.Append("<a href=\"" + pagename + "?" + page_url + "\"> " + pnum + " </a>");
                }
            }
            start_index += totalitems;
        }

        //adding next previous URL here
        if ((selectedPage + 1) > 1)
        {
            String page_url = ret_qstr + "startIndex=" + (startindex - totalitems);
            pagination.Insert(0, "<a href=\"" + pagename + "?" + page_url + "\"> Previous </a>");
        }
        if (selectedPage < num_pages)
        {
            if (num_pages != 1)
            {
                if (num_pages != (selectedPage + 1))
                {
                    String page_url = ret_qstr + "startIndex=" + (startindex + totalitems);
                    pagination.Append("<a href=\"" + pagename + "?" + page_url + "\"> Next </a>");
                }
            }
        }
        return pagination.ToString();
    }

    /// <summary>
    /// Methods to get the formatted String in Original form.
    /// </summary>
    /// <param name="strVal">String which format has to be rechanged.</param>
    /// <returns>Retruns Original String</returns>
    public static string formatStringForURLSelect(string strVal)
    {
        strVal = Regex.Replace(strVal, "&rsquo;", "'");
        strVal = Regex.Replace(strVal, "&#0146;", "'");
        strVal = Regex.Replace(strVal, "&quot;", "\"");
        strVal = Regex.Replace(strVal, "#58", ":");
        strVal = Regex.Replace(strVal, "#33", "!");
        strVal = Regex.Replace(strVal, "#64", "@");
        strVal = Regex.Replace(strVal, "#36", "£");
        strVal = Regex.Replace(strVal, "#37", "%");
        strVal = Regex.Replace(strVal, "#94", "$");
        strVal = Regex.Replace(strVal, "#95", "_");
        strVal = Regex.Replace(strVal, "#123", "{");
        strVal = Regex.Replace(strVal, "#125", "}");
        strVal = Regex.Replace(strVal, "#60", "<");
        strVal = Regex.Replace(strVal, "#62", ">");
        strVal = Regex.Replace(strVal, "#44", ",");
        strVal = Regex.Replace(strVal, "#47", "/");
        return strVal;
    }

    public static string GeneratePostTitleForURL(object Title)
    {
        string strTitle = utils.formatStringForDBSelect(Convert.ToString(Title));
        //Trim Start and End Spaces.
        strTitle = strTitle.Trim();
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%e2%80%93", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%e2%80%98", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%e2%80%99", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%e2%80%a6", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%e8", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%%e9", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%f3", "-"));
        strTitle = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Server.UrlEncode(strTitle).Replace("%%f4", "-"));
        //Trim "-" Hyphen
        strTitle = strTitle.Trim('-');

        strTitle = strTitle.ToLower();
        char[] chars = @"$%#—@á£!’‘*?—;‘:~+=()[]{}|\<>,/^"".“”".ToCharArray();

        //Replace Special-Characters
        for (int i = 0; i < chars.Length; i++)
        {
            string strChar = Convert.ToString(chars.GetValue(i));
            if (strTitle.Contains(strChar))
            {
                strTitle = strTitle.Replace(strChar, "-");
            }
        }

        strTitle = strTitle.Replace("&", "and");
        strTitle = strTitle.Replace(".", "-");
        strTitle = strTitle.Replace(" ", "-");
        while (strTitle.Contains("--"))
        {
            strTitle = strTitle.Replace("--", "-");
        }
        //Trim Start and End Spaces.
        strTitle = strTitle.Trim();

        //Trim "-" Hyphen
        strTitle = strTitle.Trim('-');

        return strTitle;
    }

    public static string GetSelectedCheckBoxItems(CheckBoxList checkBoxList)
    {
        string retval = string.Empty;

        try
        {
            retval = checkBoxList.Items.Cast<ListItem>().Where(item => item.Selected).Aggregate(retval, (current, item) => current + (item.Value + ','));
            retval = retval.TrimEnd(',');
        }
        catch (Exception)
        {
        }

        return retval;
    }

    public static void SetSelectedCheckBoxItems(CheckBoxList checkBoxList, string CommaDelimitedValue)
    {
        try
        {
            string[] arrValues = CommaDelimitedValue.TrimEnd(',').Split(',');

            string retval = string.Empty;
            retval = arrValues.ToList().Where(item => checkBoxList.Items.FindByValue(item) != null).Aggregate(retval, (current, item) => current + (item + ','));
            retval = retval.TrimEnd(',');
            if (retval != "")
                retval.Split(',').ToList().ForEach(r => checkBoxList.Items.FindByValue(r).Selected = true);
        }
        catch (Exception)
        {
        }
    }

    public static void SetSelectedListBoxItems(ListBox checkBoxList, string CommaDelimitedValue)
    {
        try
        {
            string[] arrValues = CommaDelimitedValue.TrimEnd(',').Split(',');

            string retval = string.Empty;
            retval = arrValues.ToList().Where(item => checkBoxList.Items.FindByValue(item) != null).Aggregate(retval, (current, item) => current + (item + ','));
            retval = retval.TrimEnd(',');
            if (retval != "")
                retval.Split(',').ToList().ForEach(r => checkBoxList.Items.FindByValue(r).Selected = true);
        }
        catch (Exception)
        {
        }
    }

    public static void SetSelectedListBoxText(ListBox checkBoxList, string CommaDelimitedValue)
    {
        try
        {
            string[] arrValues = CommaDelimitedValue.TrimEnd(',').Split(',');

            string retval = string.Empty;
            retval = arrValues.ToList().Where(item => checkBoxList.Items.FindByText(item) != null).Aggregate(retval, (current, item) => current + (item + ','));
            retval = retval.TrimEnd(',');
            if (retval != "")
                retval.Split(',').ToList().ForEach(r => checkBoxList.Items.FindByText(r).Selected = true);
        }
        catch (Exception)
        {
        }
    }

    public static string GetSelectedListBoxItems(ListBox lstbx)
    {
        string retval = string.Empty;

        try
        {
            retval = lstbx.Items.Cast<ListItem>().Where(item => item.Selected).Aggregate(retval, (current, item) => current + (item.Value + ','));
            retval = retval.TrimEnd(',');
        }
        catch (Exception)
        {
        }

        return retval;
    }

    public static string GetSelectedChildTreeNodes(TreeNode tn)
    {
        string retval = string.Empty;

        try
        {
            retval = tn.ChildNodes.Cast<TreeNode>().Where(node => node.Checked).Aggregate(retval, (current, node) => current + (node.Value + ','));
            retval = retval.TrimEnd(',');
        }
        catch (Exception)
        {
        }

        return retval;
    }

    public static string GetOS()
    {
        string strOS_type;
        string strBrowser = HttpContext.Current.Request.ServerVariables["http_user_agent"].ToString().ToUpper();
        if ((strBrowser.IndexOf("MAC") > 0))
        {
            strOS_type = "MAC";
        }
        else
        {
            strOS_type = "WINDOWS";
        }
        return strOS_type;
    }

    public static bool CheckFeature(int featureType)
    {
        return ReadConfig.GetFeatures().Split(',').Contains(Convert.ToString(featureType));
    }

    public static void ClearSitesSavedChannelCache(int channelid)
    {
        string strSql = "SELECT domainname = ISNULL(Stuff((SELECT ',' + domainname AS [text()] FROM "
                     + " (SELECT domainname FROM brand_domainname where id in(SELECT SiteID FROM select_channel where channel_id='" + channelid + "')"
                     + " ) x For XML PATH ('')),1,1,''),'')";

        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        DataTable DtBSSites = dpSA.FillDataTable(strSql);
        if (DtBSSites.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(DtBSSites.Rows[0][0])))
        {
            string SBBSSites = Convert.ToString(DtBSSites.Rows[0][0]);
            try
            {
                string url = ConfigLoader.GetLivePublicSite() + "CacheRemove.aspx?type=BSWidgetChannelsCache&BSDomains=" + SBBSSites.ToString();
                HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                string url1 = ConfigLoader.GetWebAppFullPath() + "CacheRemove.aspx?type=BSWidgetChannelsCache&BSDomains=digipdemo.com";
                HttpWebRequest request1 = (HttpWebRequest)System.Net.WebRequest.Create(url1);
                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            }
            catch (Exception ex) { }
        }
    }

    private static string charReplace(string strTitle)
    {
        if (strTitle.Contains(":"))
            strTitle = strTitle.Replace(":", "s-m");
        if (strTitle.Contains("_"))
            strTitle = strTitle.Replace("_", "u-s");
        if (strTitle.Contains("."))
            strTitle = strTitle.Replace(".", "d-o-t");
        if (strTitle.Contains("|"))
            strTitle = strTitle.Replace("|", "b-a-r");
        if (strTitle.Contains(">") || strTitle.Contains(" > "))
            strTitle = strTitle.Replace(">", "g-t-r");
        if (strTitle.Contains("<") || strTitle.Contains(" < "))
            strTitle = strTitle.Replace("<", "l-s-r");
        if (strTitle.Contains("*"))
            strTitle = strTitle.Replace("*", "s?r");
        return strTitle;

    }

    public static DataTable LoadRSS(string URL, string name)
    {
        //URL = "http://api.breakingnews.com/api/v1/item/?format=rss";
        DataTable dtRss = new DataTable();
        try
        {
            dtRss.Columns.Add(new DataColumn("title", typeof(string)));
            dtRss.Columns.Add(new DataColumn("link", typeof(string)));
            dtRss.Columns.Add(new DataColumn("pubdate", typeof(string)));
            dtRss.Columns.Add(new DataColumn("creator", typeof(string)));
            dtRss.Columns.Add(new DataColumn("description", typeof(string)));
            dtRss.Columns.Add(new DataColumn("titleURL", typeof(string)));
            DataRow dr;

            XmlDocument xd = new XmlDocument();

            using (var webpage = new WebClient())
            {
                //try
                //{
                //    webpage.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
                //    byte[] data = webpage.DownloadData(URL);
                //    xd.Load(new MemoryStream(data));
                //}
                //catch (Exception)
                //{
                //Create a WebRequest and WebResponse for get the Details.
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(URL);
                    HttpWReq.Timeout = 40000; // Time out is set for 40 secs to bypass the default time out which is 100,000 milliseconds (100 seconds)
                    HttpWebResponse HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();

                    //Load the Stream to XmlDocument
                    xd.Load(HttpWResp.GetResponseStream());

                    HttpWResp.Close();
                    HttpWResp = null;
                    HttpWReq = null;
                //}
            }

            XmlNodeList xnl;
            XmlNode xn;

            //Select the rss/channel/item
            xnl = xd.SelectNodes("rss/channel/item");

            //Select the feed/entry/
            if (xnl.Count == 0)
            {
                XmlNamespaceManager mngr = new XmlNamespaceManager(xd.NameTable);
                mngr.AddNamespace("p", "http://www.w3.org/2005/Atom");
                xnl = xd.DocumentElement.SelectNodes("//p:entry", mngr);
            }

            if (xnl.Count == 0)
            {
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xd.NameTable);
                nsmgr.AddNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
                nsmgr.AddNamespace("slash", "http://purl.org/rss/1.0/modules/slash/");
                nsmgr.AddNamespace("taxo", "http://purl.org/rss/1.0/modules/taxonomy/");
                nsmgr.AddNamespace("dc", "http://purl.org/dc/elements/1.1/");
                nsmgr.AddNamespace("syn", "http://purl.org/rss/1.0/modules/syndication/");
                nsmgr.AddNamespace("admin", "http://webns.net/mvcb/");
                nsmgr.AddNamespace("def", "http://purl.org/rss/1.0/");
                nsmgr.AddNamespace("feedburner", "http://rssnamespace.org/feedburner/ext/1.0");
                xnl = xd.SelectNodes("/rdf:RDF/def:item", nsmgr);
            }

            //Load RSS Detail
            for (int i = 0; i < xnl.Count; i++)
            {
                xn = xnl[i];
                dr = dtRss.NewRow();

                try
                {
                    dr["title"] = xn["title"].InnerText;
                }
                catch
                {
                    dr["title"] = "";

                }
                try
                {
                    dr["titleURL"] = utils.GeneratePostTitleForURL(xn["title"].InnerText).Replace("&", "and").Replace("-", " ").Replace("|", " ").Replace(":", " ").Replace(".", " ").Replace("(", "").Replace(")", "").Replace("   ", "  ").Replace("  ", " ");
                }
                catch
                {
                    dr["titleURL"] = "";

                }
                try
                {
                    string word = xn["title"].InnerText.ToString().Trim();
                    bool containUnicode = false;
                    for (int x = 0; x < word.Length; x++)
                    {
                        if (char.GetUnicodeCategory(word[x]) == UnicodeCategory.OtherLetter)
                        {
                            containUnicode = true;
                            break;
                        }
                    }
                    if (!containUnicode)
                    {
                        //dr["link"] ="../feedItems/" + charReplace(name) + "/" + utils.GeneratePostTitleForURL(word);
                        dr["link"] = System.Web.HttpUtility.HtmlEncode("/feedItems/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(name)) + "/" + utils.GeneratePostTitleForURL(word));
                    }
                    else
                    {
                        if (xn["link"].Attributes["href"] != null)
                            dr["link"] = xn["link"].Attributes["href"].Value;
                        else if (xn["link"].InnerText != null)
                            dr["link"] = xn["link"].InnerText;
                    }
                }
                catch
                {
                    dr["link"] = "";
                }

                try
                {
                    if (xn["pubDate"] != null)
                        dr["pubdate"] = xn["pubDate"].InnerText;
                    else if (xn["published"] != null)
                        dr["pubdate"] = xn["published"].InnerText;
                }
                catch
                {
                    dr["pubdate"] = "";
                }
                if (xn["author"] != null)
                {
                    if ((xn["author"] != null) || (xn["author"].InnerText != ""))

                        dr["creator"] = xn["author"].InnerText;
                }

                else
                {
                    dr["creator"] = xn["link"].InnerText;
                }
                try
                {
                    if (xn["description"] != null)
                    {
                        dr["description"] = xn["description"].InnerText.Replace("<a", "<a target='_blank'").Replace("<A", "<A target='_blank'");
                        dr["description"] = GetCorrectectRssDescription(Convert.ToString(dr["description"]), URL);
                    }
                    else if (xn["content"] != null)
                    {
                        dr["description"] = xn["content"].InnerText.Replace("<a", "<a target='_blank'").Replace("<A", "<A target='_blank'");
                        dr["description"] = GetCorrectectRssDescription(Convert.ToString(dr["description"]), URL);
                    }
                    else if (xn["summary"] != null)
                    {
                        if (xn["summary"].InnerText.ToString().Trim().ToLower() == xn["title"].InnerText.ToString().Trim().ToLower())
                        {
                            dr["description"] = Convert.ToDateTime(dr["pubdate"].ToString()).ToString();
                            dr["description"] = GetCorrectectRssDescription(Convert.ToString(dr["description"]), URL);
                        }
                        else
                        {
                            dr["description"] = xn["summary"].InnerText.Replace("<a", "<a target='_blank'").Replace("<A", "<A target='_blank'");
                            dr["description"] = GetCorrectectRssDescription(Convert.ToString(dr["description"]), URL);
                        }
                    }
                }
                catch
                {
                    dr["description"] = "";
                }

                dtRss.Rows.Add(dr);
                //
            }
        }
        catch (Exception ex) { throw ex; }
        return dtRss;
    }

    private static string GetCorrectectRssDescription(string RssDesc, string rssHostUrl)
    {
        rssHostUrl = "http://" + rssHostUrl.Replace("http://", "").Replace("/", "^").Split('^')[0];
        RssDesc = RssDesc.Replace("<a", "<a target='_blank'");
        RssDesc = RssDesc.Replace("<A", "<A target='_blank'");
        RssDesc = RssDesc.Replace("../", "^");
        RssDesc = RssDesc.Replace("^^^^^^^^^^", "/");
        RssDesc = RssDesc.Replace("^^^^^^^^^", "/");
        RssDesc = RssDesc.Replace("^^^^^^^^", "/");
        RssDesc = RssDesc.Replace("^^^^^^^", "/");
        RssDesc = RssDesc.Replace("^^^^^^", "/");
        RssDesc = RssDesc.Replace("^^^^^", "/");
        RssDesc = RssDesc.Replace("^^^^", "/");
        RssDesc = RssDesc.Replace("^^^", "/");
        RssDesc = RssDesc.Replace("^^", "/");
        RssDesc = RssDesc.Replace("^", "/");

        RssDesc = RssDesc.Replace("href=\"%E2%80%9C", "href=\"");
        RssDesc = RssDesc.Replace("HREF=\"%E2%80%9C", "href=\"");
        RssDesc = RssDesc.Replace("href='%E2%80%9C", "href='");
        RssDesc = RssDesc.Replace("HREF='%E2%80%9C", "href='");

        RssDesc = RssDesc.Replace("href=\"%5B%20", "href=\"");
        RssDesc = RssDesc.Replace("HREF=\"%5B%20", "href=\"");
        RssDesc = RssDesc.Replace("href='%5B%20", "href='");
        RssDesc = RssDesc.Replace("HREF='%5B%20", "href='");

        RssDesc = RssDesc.Replace("href=\"%28", "href=\"");
        RssDesc = RssDesc.Replace("HREF=\"%28", "href=\"");
        RssDesc = RssDesc.Replace("href='%28", "href='");
        RssDesc = RssDesc.Replace("HREF='%28", "href='");

        RssDesc = RssDesc.Replace("href=\"(", "href=\"");
        RssDesc = RssDesc.Replace("HREF=\"(", "href=\"");
        RssDesc = RssDesc.Replace("href='(", "href='");
        RssDesc = RssDesc.Replace("HREF='(", "href='");

        RssDesc = RssDesc.Replace("href=\"/", "href=\"" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("HREF=\"/", "href=\"" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("src=\"/", "src=\"" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("SRC=\"/", "src=\"" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("href='/", "href='" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("HREF='/", "href='" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("src='/", "src='" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("SRC='/", "src='" + rssHostUrl + "/");
        RssDesc = RssDesc.Replace("href=\"www", "href=\"http://www");
        RssDesc = RssDesc.Replace("HREF=\"www", "href=\"http://www");
        RssDesc = RssDesc.Replace("href=\"WWW", "href=\"http://www");
        RssDesc = RssDesc.Replace("HREF=\"WWW", "href=\"http://www");
        RssDesc = RssDesc.Replace("href='www", "href='http://www");
        RssDesc = RssDesc.Replace("HREF='www", "href='http://www");
        RssDesc = RssDesc.Replace("href='WWW", "href='http://www");
        RssDesc = RssDesc.Replace("HREF='WWW", "href='http://www");

        RssDesc = RssDesc.Replace("HREF='http:/", "href='http://");
        RssDesc = RssDesc.Replace("HREF='HTTP:/", "href='http://");
        RssDesc = RssDesc.Replace("HREF=\"http:/", "href=\"http://");
        RssDesc = RssDesc.Replace("HREF=\"HTTP:/", "href=\"http://");

        RssDesc = RssDesc.Replace("href='http:/", "href='http://");
        RssDesc = RssDesc.Replace("href='HTTP:/", "href='http://");
        RssDesc = RssDesc.Replace("href=\"http:/", "href=\"http://");
        RssDesc = RssDesc.Replace("href=\"HTTP:/", "href=\"http://");

        return RssDesc;
    }

    public static bool isMobile()
    {
        //GETS THE CURRENT USER CONTEXT
        HttpContext context = HttpContext.Current;

        //FIRST TRY BUILT IN ASP.NT CHECK
        if (context.Request.Browser.IsMobileDevice)
        {
            return true;
        }
        //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
        if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
        {
            return true;
        }
        //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
        if (context.Request.ServerVariables["HTTP_ACCEPT"] != null && context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
        {
            return true;
        }
        //AND FINALLY CHECK THE HTTP_USER_AGENT 
        //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
        if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains("android"))
            {
                return false;
            }
            //Create a list of all mobile types
            string[] mobiles = {"midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource", "240x320"
                                , "opwv", "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian"
                                , "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-"
                                , "SEC-", "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel", "lg"
                                , "eric", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch"
                                , "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal"
                                , "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "dddi"
                                , "moto", "iphone"
		                    };
            //Loop through each item in the list created above 
            //and check if the header contains that text
            foreach (string s in mobiles)
            {
                if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(s.ToLower()))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static bool ReferalCheck(string strReferal, string strNotReferal)
    {
        bool flag = true;
        if (strReferal == "" && strNotReferal == "")
        {
            flag = true;
        }
        else
        {
            string Myreferer = HttpContext.Current.Request.Url.ToString();
            if (strReferal.Contains(","))
            {
                flag = false;
                string[] arrReferal = strReferal.Split(',');
                for (int i = 0; i < arrReferal.Length; i++)
                {
                    if (Myreferer.Contains(arrReferal[i].Trim()))
                    {
                        flag = true;
                    }
                }
            }
            else
            {
                if (strReferal != "")
                {
                    flag = false;
                    if (Myreferer.Contains(strReferal.Trim()))
                    {
                        flag = true;
                    }
                }
            }

            if (strNotReferal.Contains(","))
            {
                string[] arrNotReferal = strNotReferal.Split(',');
                for (int i = 0; i < arrNotReferal.Length; i++)
                {
                    if (Myreferer.Contains(arrNotReferal[i].Trim()))
                    {
                        flag = false;
                    }
                }
            }
            else
            {
                if (strNotReferal != "")
                {
                    if (Myreferer.Contains(strNotReferal.Trim()))
                    {
                        flag = false;
                    }
                }
            }
        }
        return flag;
    }

    public static bool IsHomePage()
    {
        bool isHomePage = true;
        string pageurl = HttpContext.Current.Request.Url.OriginalString.ToString().Trim().ToLower();
        pageurl = pageurl.Replace("http://", "").Replace("www.", "");
        string homepage = "";
        if (pageurl.Contains("/"))
        {
            homepage = pageurl.Split('/')[1];
        }
        if (homepage == "")
        {
            isHomePage = false;
        }
        else
        {
            if (homepage == "ondemandhome.aspx")
            {
                isHomePage = false;
            }
            else
            {
                isHomePage = true;
            }
        }
        return isHomePage;
    }

    public static string GetBrowser()
    {
        string currentBrowser = "";
        try
        {
            if (HttpContext.Current.Request.UserAgent.ToLower().Contains("iphone"))
            {
                currentBrowser = "iphone";
            }
            else if (HttpContext.Current.Request.UserAgent.ToLower().Contains("ipad"))
            {
                currentBrowser = "ipad";
            }
            else
            {
                currentBrowser = Convert.ToString(HttpContext.Current.Request.Browser.Browser);
                if (currentBrowser == "AppleMAC-Safari")
                {
                    string useragent = HttpContext.Current.Request.UserAgent;
                    if (useragent.Contains("Chrome"))
                        currentBrowser = "Chrome";
                    else
                        currentBrowser = "Safari";
                }
                if (currentBrowser.ToLower() == "internetexplorer")
                    currentBrowser = "IE";
            }
        }
        catch { }
        return Convert.ToString(currentBrowser);
    }

    public static string GetUserIP()
    {
        string ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
        if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
        {
            ipAddress = "180.151.95.10";
        }
        return ipAddress;
    }

    public static string GetUserAgent()
    {
        string useragent = HttpContext.Current.Request.UserAgent;
        return useragent;
    }

    public static string Getcountry()
    {
        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        string ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
        if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
        {
            ipAddress = "180.151.95.10";
        }
        string strcountry_id = "000";
        CountryLookUp _CountryLookUp = new CountryLookUp();
        string strCountry = _CountryLookUp.LookupCountryName(ipAddress);

        DataTable dtCoun = new DataTable();
        if (HttpContext.Current.Cache["dtCoun"] != null)
        {
            dtCoun = (DataTable)HttpContext.Current.Cache["dtCoun"];
        }
        else
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "select country_id,country_name from Country";
            cmd.CommandType = CommandType.Text;
            dtCoun = dpSA.FillDataTable(cmd);
            HttpContext.Current.Cache.Insert("dtCoun", dtCoun, null, DateTime.Now.AddDays(1), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        if (dtCoun.Rows.Count > 0)
        {
            foreach (DataRow dr in dtCoun.Rows)
            {
                if (Convert.ToString(dr["country_name"]) == strCountry)
                {
                    strcountry_id = Convert.ToString(dr["country_id"]).PadLeft(3, '0');
                }
            }
        }
        return strcountry_id;
    }

    public static string GetOperatingSystem()
    {
        string reqStrb = HttpContext.Current.Request.ServerVariables["http_user_agent"].ToString().ToLower();
        var lstWin = new List<string> { "windows 10.0", "windows nt 10.0", "windows 8.1", "windows nt 6.3", "windows 8", "windows nt 6.2", "windows 7", "windows nt 6.1", "windows nt 6.0", 
                              "windows nt 5.2", "windows nt 5.1", "windows xp", "windows nt 5.0", "windows 2000", "win 9x 4.90", "windows me", "windows 98", "win98", "windows 95", 
                              "win95", "windows_95", "windows nt 4.0", "winnt4.0", "winnt", "windows nt", "windows ce", "win16" };
        var lstMAC = new List<string> { "mac os", "macppc", "macintel", "mac_powerpc", "macintosh" };
        var lstiOS = new List<string> { "iphone", "ipad", "ipod" };

        string Android = "android";
        string Linux = "linux";

        if (reqStrb.Contains(Android))
            return Android;
        else if (reqStrb.Contains(Linux))
            return Linux;
        else
        {
            var str = "";
            foreach (string item in lstWin)
            {
                if (reqStrb.Contains(item))
                {
                    str = "windows";
                    break;
                }
            }

            foreach (string item in lstMAC)
            {
                if (reqStrb.Contains(item))
                {
                    str = "macintosh";
                    break;
                }
            }

            foreach (string item in lstiOS)
            {
                if (reqStrb.Contains(item))
                {
                    str = "ios";
                    break;
                }
            }

            if (str != "")
                return str;
            else
                return "others";
        }
    }

    public static int GetStartStopDomainsReport()
    {
        int startstop = 0;
        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        if (HttpContext.Current.Cache["chkStratDomainReport"] != null)
        {
            startstop = Convert.ToInt32(HttpContext.Current.Cache["chkStratDomainReport"]);
        }
        else
        {
            string strSql = "SELECT ISNULL(Sart_Stop_Domain_Report,0) AS Sart_Stop_Domain_Report FROM brand_Record_Data";
            DataTable dtStartStopDomainsReport = dpSA.FillDataTable(strSql);
            if (dtStartStopDomainsReport.Rows.Count > 0)
            {
                startstop = Convert.ToInt32(dtStartStopDomainsReport.Rows[0]["Sart_Stop_Domain_Report"].ToString());
            }
            HttpContext.Current.Cache["chkStratDomainReport"] = startstop;
        }
        return startstop;
    }

    public static int GetStartADNetworkAndWidgetReport()
    {
        int intAllowAdnetworkData = 0;
        if (HttpContext.Current.Cache["chkRecordADData"] != null)
        {
            intAllowAdnetworkData = Convert.ToInt32(HttpContext.Current.Cache["chkRecordADData"]);
        }
        else
        {
            DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            DataTable dtcheck = dpSA.FillDataTable("select isnull(Start_Stop,0) as Start_Stop from brand_Record_Data");
            if (dtcheck.Rows.Count > 0)
            {
                intAllowAdnetworkData = Convert.ToInt32(dtcheck.Rows[0]["Start_Stop"].ToString());
            }
            HttpContext.Current.Cache["chkRecordADData"] = intAllowAdnetworkData;
        }
        return intAllowAdnetworkData;
    }

    public static void Setimperssion(int banid, string type)
    {
        if (GetStartADNetworkAndWidgetReport() == 1)
        {
            if (banid != 0)
            {
                string ClientIP = Convert.ToString(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Insert_Page_Impression";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", ClientIP);
                cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd.Parameters.Add("@Domain", HttpContext.Current.Request.Url.Host);
                cmd.Parameters.Add("@Type", type);
                cmd.Parameters.Add("@RefererURL", Convert.ToString(HttpContext.Current.Session["Referer_URL"]));
                dpreport.ExecuteNonQuery(cmd);
            }
        }
    }

    public static int SameIPClicks(int bannerid, string Type, int SameIPDays)
    {
        int SameIPCount = 0;
        if (GetStartADNetworkAndWidgetReport() == 1)
        {
            string ClientIP = Convert.ToString(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
            if (ConfigLoader.GetIsUseNewClickDB() == 0)
            {
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                DataTable dt = new DataTable();
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Select_SameIP_Click";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", ClientIP);
                cmd.Parameters.Add("@WidgetID", Convert.ToString(bannerid));
                cmd.Parameters.Add("@BlockStatus", "0");
                cmd.Parameters.Add("@Type", Type);
                dt = dpreport.FillDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    SameIPCount = Convert.ToInt32(dt.Rows[0]["widgetclick"]);
                }
            }
            else
            {
                DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
                DataTable dt = new DataTable();
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Select_SameIP_Click";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", ClientIP);
                cmd.Parameters.Add("@WidgetID", Convert.ToString(bannerid));
                cmd.Parameters.Add("@Days", SameIPDays);
                cmd.Parameters.Add("@Type", Type);
                dt = dpreport.FillDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    SameIPCount = Convert.ToInt32(dt.Rows[0]["widgetclick"]);
                }
            }
        }
        return SameIPCount;
    }

    public static void updateWidgetCount(int WidgetId)
    {
        if (WidgetId != 0)
        {
            try
            {
                if (utils.GetStartStopDomainsReport() == 1)
                {
                    DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
                    DBCommand cmdCountWidget = new DBCommand();
                    cmdCountWidget.CommandText = "SP_Brand_Update_CountWidget";
                    cmdCountWidget.CommandType = CommandType.StoredProcedure;
                    cmdCountWidget.AddParameter("@AppId", WidgetId);
                    cmdCountWidget.AddParameter("@Domain", ReadConfig.getDomainUrl());
                    dpreport.ExecuteNonQuery(cmdCountWidget);
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }

    public static bool IsNumeric(string Text)
    {
        if (!string.IsNullOrWhiteSpace(Text))
        {
            Text = Text.Trim();
            bool bResult = Regex.IsMatch(Text, @"^\d+$");
            return bResult;
        }
        else
            return false;
    }
    
    public static string formatStringForDBblogInsert(string strVal)
    {
        strVal = strVal.Replace("<a", "<a target='_blank'");
        strVal = strVal.Replace("<A", "<A target='_blank'");
        strVal = strVal.Replace("href=\"../redirect.aspx?url=", " href=\"");
        strVal = strVal.Replace("href='../redirect.aspx?url=", " href='");
        strVal = strVal.Replace("HREF=\"../redirect.aspx?url=", " HREF=\"");
        strVal = strVal.Replace("HREF='../redirect.aspx?url=", " HREF='");
        DataFormatter df = new DataFormatter();
        df.setDefaultInputSearch();
        string strOut = df.getInputString(strVal);
        return strOut;
    }

    public static string LookUpDetails(string ipAddress)
    {
        CountryLookUp _CountryLookUp = new CountryLookUp();
        string _CountryName = null;
        string _UserIPAddress = ipAddress;
        {
            _CountryName = _CountryLookUp.LookupCountryName(_UserIPAddress);
        }
        return _CountryName;
    }

    public static void SubscribeUser(string emailaddress)
    {
        string strDomain = ReadConfig.getDomainUrl();

        string strSql = "DECLARE @COUNT INT;SET @COUNT=(SELECT COUNT(*) FROM brand_user WHERE email=@emailaddress and roles='subscribe');IF @COUNT=0 BEGIN INSERT INTO brand_user "
            + "([uname],[password],[email],[roles],[rating],[status],[fullname],[title],[c_date],[c_by],[m_by],[m_date],[header_text]) "
            + "VALUES(@emailaddress,'',@emailaddress,'subscribe',-1,1,@emailaddress,'" + strDomain + "', "
            + "getdate(),1,1,getdate(),'');END";

        SqlConnection con = new SqlConnection(ReadConfig.GetConnectionString());
        con.Open();

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = strSql;
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;

        SqlParameter search = new SqlParameter();
        search.ParameterName = "@emailaddress";
        search.Value = emailaddress;
        cmd.Parameters.Add(search);

        cmd.ExecuteNonQuery();
        con.Close();
    }

    public static void RegisterUser(string emailaddress,string password)
    {
        string strDomain = ReadConfig.getDomainUrl();

        string strSql = "INSERT INTO brand_user "
            + "([uname],[password],[email],[roles],[rating],[status],[fullname],[title],[c_date],[c_by],[m_by],[m_date],[header_text]) "
            + "VALUES(@emailaddress,@Password,@emailaddress,'user',-1,1,@emailaddress,'" + strDomain + "', "
            + "getdate(),1,1,getdate(),'')";

        SqlConnection con = new SqlConnection(ReadConfig.GetConnectionString());
        con.Open();

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = strSql;
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;

        SqlParameter search = new SqlParameter();
        search.ParameterName = "@emailaddress";
        search.Value = emailaddress;
        cmd.Parameters.Add(search);

        search = new SqlParameter();
        search.ParameterName = "@Password";
        search.Value = password;
        cmd.Parameters.Add(search);

        cmd.ExecuteNonQuery();
        con.Close();
    }

    public static bool UpdatePassword(string userid, string password)
    {
        SqlConnection con = new SqlConnection(ReadConfig.GetConnectionString());
        con.Open();
        try
        {
            string strDomain = ReadConfig.getDomainUrl();
            string strSql = "Update brand_user set password=@Password,m_by=@uid,m_date=getdate() where uid=@uid";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;

            SqlParameter search = new SqlParameter();
            search.ParameterName = "@uid";
            search.Value = userid;
            cmd.Parameters.Add(search);

            search = new SqlParameter();
            search.ParameterName = "@Password";
            search.Value = password;
            cmd.Parameters.Add(search);

            cmd.ExecuteNonQuery();
            con.Close();
            return true;
        }
        catch
        {
            con.Close();
            return false;
        }
    }

    public static bool UpdateEmail(string userid, string Email)
    {
        SqlConnection con = new SqlConnection(ReadConfig.GetConnectionString());
        con.Open();
        try
        {
            string strDomain = ReadConfig.getDomainUrl();
            string strSql = "Update brand_user set email=@Email,m_by=@uid,m_date=getdate() where uid=@uid";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = strSql;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;

            SqlParameter search = new SqlParameter();
            search.ParameterName = "@uid";
            search.Value = userid;
            cmd.Parameters.Add(search);

            search = new SqlParameter();
            search.ParameterName = "@Email";
            search.Value = Email;
            cmd.Parameters.Add(search);

            cmd.ExecuteNonQuery();
            con.Close();
            return true;
        }
        catch
        {
            con.Close();
            return false;
        }
    }

    public static DataTable GetUserDetail(string id)
    {
        string strDomain = ReadConfig.getDomainUrl();

        string strSql = "Select * from brand_user where uid=" + id;

        SqlConnection con = new SqlConnection(ReadConfig.GetConnectionString());
        con.Open();

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = strSql;
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;

        DataTable dt = new DataTable();

        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;
    }

    public static string GetcountryName()
    {
        string ipAddress = GetUserIP();
        CountryLookUp _CountryLookUp = new CountryLookUp();
        string strCountry = _CountryLookUp.LookupCountryName(ipAddress);
        return strCountry;
    }

    public static string GetBrowserName()
    {
        return HttpContext.Current.Request.Browser.Browser;
    }

    public static string EncryptPassword(string text)
    {
        return GetMD5(text);
    }

    private static string GetMD5(string text)
    {
        byte[] hashValue;
        byte[] message = Encoding.UTF8.GetBytes(text);

        MD5 hashString = new MD5CryptoServiceProvider();
        string hex = "";
        hashValue = hashString.ComputeHash(message);
        foreach (byte x in hashValue)
        {
            hex += String.Format("{0:x2}", x);
        }
        return hex;
    }

    public static string GetRandomPassword(int length)
    {
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var result = new string(
            Enumerable.Repeat(chars, length)
                      .Select(s => s[random.Next(s.Length)])
                      .ToArray());

        return result;
    }

    public static string GetMetaType(string strVal)
    {
        if(strVal == "True")
        {
            return "meta property";
        }
        else
        {
            return "meta name";
        }
    }

    public static void SetdHoneyPotEmail(HttpServerUtility Server,HttpRequest Request)
    {
        DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        DataTable dt = dpSA.FillDataTable("select IsNull(IsSendHoneypotemail,0) as IsSendHoneypotemail from brand_domainname where domainname='" + ReadConfig.getDomainUrl() + "'");
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["IsSendHoneypotemail"].ToString() == "1")
            {
                string strbody = "<table width=\"60%\" border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"5\">";
                strbody = strbody + "<tr><td>IpAddress</td><td>:</td><td>" + Request.ServerVariables["REMOTE_ADDR"].ToString() + "</td></tr>";
                strbody = strbody + "<tr><td>Date Time</td><td>:</td><td>" + DateTime.Now.ToString("dd MMMM yyyy") + "</td></tr>";
                strbody = strbody + "<tr><td>Browser type</td><td>:</td><td>" + "Type-- " + Request.Browser.Type + ", Name--" + Request.Browser.Browser + ", Version--" + Request.Browser.Version + "</td></tr>";
                strbody = strbody + "<tr><td>Operating System</td><td>:</td><td>" + utils.GetOperatingSystem() + "</td></tr>";
                strbody = strbody + "<tr><td>Snooped</td><td>:</td><td>http://" + ReadConfig.getDomainUrl() + Request.RawUrl + "</td></tr>";
                strbody = strbody + "<tr><td>GEO</td><td>:</td><td>" + utils.GetcountryName() + "</td></tr>";
                strbody = strbody + "<tr><td>UserAgent</td><td>:</td><td>" + utils.GetUserAgent() + "</td></tr>";
                strbody = strbody + "</table>";

                string strFromName = "support@" + ReadConfig.getDomainUrl();
                string strsendto = ReadConfig.GetEmail().Trim();
                string strsubject = ReadConfig.getDomainUrl() + " - snooper details";
                MailerBrand.SendEmailMessage(strFromName, strsendto, strsubject, strbody);
            }
        }
        Server.Transfer("~/filenotfound.htm");
    }
}