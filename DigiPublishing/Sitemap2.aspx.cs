﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

public partial class Sitemap2 : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetRss();
            get();
        }
    }

    private void GetRss()
    {
        try
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_Rss_Sitemap";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtRss = dp.FillDataTable(cmd);
            dtRss.Columns.Add("url", typeof(string));
            if (dtRss.Rows.Count > 0)
            {
                StringBuilder strTitle = new StringBuilder();
                foreach (DataRow dr in dtRss.Rows)
                {
                    strTitle.Append("<a href='" + "http://" + ReadConfig.getDomainUrl() + "/updates/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dr["title"]))) + "'>"
                                + utils.formatStringForDBSelect(Convert.ToString(dr["title"])) + "</a>" + ", ");
                }
                //this.lblRss.Text = strTitle.ToString().Substring(0, strTitle.Length - 2);
            }
            else
            {
                //divRss.Visible = false;
            }
        }
        catch (Exception ex)
        {
            //lblmessage.Text = ex.Message.ToString();
        }
    }


    private string GetStringForValue(string str)
    {
        str = str.Replace("&", "&amp;");
        str = str.Replace("'", "&apos;");
        str = str.Replace("\"", " &quot;");
        str = str.Replace(">", "&gt;");
        str = str.Replace("<", "&lt;");
        return str;
    }

    private void get()
    {
        DataTable dt = new DataTable();
        XmlTextWriter TextWriter = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);
        Response.ContentType = "text/xml";

        TextWriter.Formatting = Formatting.Indented;
        TextWriter.WriteStartDocument();
        TextWriter.WriteStartElement("urlset");
        TextWriter.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
        string DomainURL = ReadConfig.getDomainUrl();

        TextWriter.WriteStartElement("url");
        TextWriter.WriteElementString("loc", "http://" + DomainURL);
        TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
        TextWriter.WriteElementString("changefreq", "daily");
        TextWriter.WriteElementString("priority", "0.5");
        TextWriter.WriteEndElement();

        string[] strArrFeatures = ReadConfig.GetFeatures().Split(',');

        //Blog
        if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Blog)))))
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_Blogs_Sitemap";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtBlogs = dp.FillDataTable(cmd);
            if (dtBlogs.Rows.Count > 0)
            {
                StringBuilder strBlog = new StringBuilder();
                foreach (DataRow dr in dtBlogs.Rows)
                {
                    if (Convert.ToString(dr["post_title"]) != "")
                    {
                        string strTitle = utils.formatStringForURLSelect(dr["post_title"].ToString());
                        string strurl = GetStringForValue("http://" + DomainURL + "/features/" + utils.GeneratePostTitleForURL(strTitle) + ".aspx");
                        TextWriter.WriteStartElement("url");
                        TextWriter.WriteElementString("loc", strurl);
                        TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
                        TextWriter.WriteElementString("changefreq", "daily");
                        TextWriter.WriteElementString("priority", "0.5");
                        TextWriter.WriteEndElement();
                    }
                }
            }
        }

        //BookMark
        if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Bookmark)))))
        {
            TextWriter.WriteStartElement("url");
            TextWriter.WriteElementString("loc", "http://" + DomainURL + "/links/index.aspx");
            TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
            TextWriter.WriteElementString("changefreq", "daily");
            TextWriter.WriteElementString("priority", "0.5");
            TextWriter.WriteEndElement();

        }

        //RSS
        if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.RSS)))))
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_Rss_Sitemap";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtRss = dp.FillDataTable(cmd);
            dtRss.Columns.Add("url", typeof(string));
            if (dtRss.Rows.Count > 0)
            {
                StringBuilder strTitle = new StringBuilder();
                foreach (DataRow dr in dtRss.Rows)
                {
                    string strurl = GetStringForValue("http://" + DomainURL + "/updates/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dr["title"]))));
                    TextWriter.WriteStartElement("url");
                    TextWriter.WriteElementString("loc", strurl);
                    TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
                    TextWriter.WriteElementString("changefreq", "daily");
                    TextWriter.WriteElementString("priority", "0.5");
                    TextWriter.WriteEndElement();
                }
            }
        }

        TextWriter.WriteStartElement("url");
        TextWriter.WriteElementString("loc", "http://" + DomainURL + "/contactUs.aspx");
        TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
        TextWriter.WriteElementString("changefreq", "daily");
        TextWriter.WriteElementString("priority", "0.5");
        TextWriter.WriteEndElement();

        TextWriter.WriteStartElement("url");
        TextWriter.WriteElementString("loc", "http://" + DomainURL + "/Terms.aspx");
        TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
        TextWriter.WriteElementString("changefreq", "daily");
        TextWriter.WriteElementString("priority", "0.5");
        TextWriter.WriteEndElement();

        TextWriter.WriteStartElement("url");
        TextWriter.WriteElementString("loc", "http://" + DomainURL + "/PrivacyPolicy.aspx");
        TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
        TextWriter.WriteElementString("changefreq", "daily");
        TextWriter.WriteElementString("priority", "0.5");
        TextWriter.WriteEndElement();

        TextWriter.WriteStartElement("url");
        TextWriter.WriteElementString("loc", "http://" + DomainURL + "/RSSFeed.aspx");
        TextWriter.WriteElementString("lastmod", DateTime.Now.ToString("yyyy-MM-dd"));
        TextWriter.WriteElementString("changefreq", "daily");
        TextWriter.WriteElementString("priority", "0.5");
        TextWriter.WriteEndElement();

        TextWriter.WriteEndDocument();
        TextWriter.Flush();
        TextWriter.Close();
    }


}