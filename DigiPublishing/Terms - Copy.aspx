﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="Terms - Copy.aspx.cs" Inherits="terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <style type="text/css">
        body
        {
            font-family:Calibiri;
            font-size:15px;
        }
        p
        {
            margin-top:10px;
        }
        h3
        {
            font-size:20px;
            font-weight:bold;
        }
        ul
        {
            list-style-position: outside;
            list-style-type: disc;
            margin-left:20px;
            margin-top:10px;
        }
        a
        { 
            color:blue;
        }
    </style>
    <div class="main">
      <h3 style="text-align:center;">Terms and Conditions</h3>
  
      <div class="terms">
        <p> </p>
        <p><strong><br/>
        Terms and Conditions</strong></p>
        <p>These terms and conditions outline the  rules and regulations for the use of this website.</p>
        <p>By accessing this website we assume you accept these terms and conditions. Do not continue to use this website if you do not agree to take all of the terms and conditions stated on this website.</p>
        <p>&nbsp;</p>
        <p><strong>By using this service, you agree to these simple points:</strong></p>
        <p>We may use third-party advertising companies to serve ads when you visit our website. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, please see: opt out <a href="http://optout.networkadvertising.org" target="_blank">http://optout.networkadvertising.org</a></p>
        <p>We may share information with partners. They may communicate offers to you but you should be able to unsubscribe.</p>
        <p>We respond to legal processes and exercise our legal rights or defend against legal claims.</p>
        <p>We share investigate and may take action regarding illegal activities and violations as required by law.</p>
        <p>If you register or subscribe or contact us then we may email newsletters or related offers to you and you can unsubscribe.</p>
        <p>Our data is stored with data back-ups in order to provide services. </p>
        <p>We keep your information and data may remain on our system after you have deleted information or your account. </p>
        <p>We don't access private accounts unless requested by you, or if required by law, or to maintain our system, or to protect us or the public.</p>
        <p>We may display content from other sites and services here on our site.</p>
        <p>Offensive material or action is not tolerated and we may use your personal information (name, address, employer, college, e-mail, ISP, IP) to prevent future action.</p>
        <p>If you see or know of any material that you do not think should be on our site, please do contact us and we investigate immediatly and the content maybe removed.</p>
        <p>If you have registered or subscribed with a service we provide then accounts can be deleted if you contact us.</p>
        <p>We reserve the right to transfer your information if our company is sold or merges with another organisation.</p>
          <p>&nbsp;</p>
        <p><strong>Log Data</strong></p>
        <p>We want to inform you that whenever you visit our website, we collect information that your browser sends to us that is called Log Data. This Log Data may include information such as your computer&rsquo;s Internet Protocol (&quot;IP&quot;) address, browser version, browser UserAgent, pages of our website Service that you visit, the time and date of your visit, the time spent on pages, GEO location, bandwidth, login frequency, content viewed, ads viewed, content clicks, user interaction and other statistics that may be collected so we can improve our website or our partners services.</p>
        <p>&nbsp;</p>
          <p><strong>Cookies</strong></p>
        <p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your computer&rsquo;s hard drive.</p>
        <p>Our website uses these &quot;cookies&quot; to collection information and to improve our Service. You have the option to either accept or refuse these cookies, and know when a cookie is being sent to your computer. If you choose to refuse our cookies, you may not be able to use some portions of our Service.</p>
        <p>We employ the use of cookies. By accessing this website, you agreed to use cookies in agreement with this website's Privacy Policy.</p>
        <p>Most interactive websites use cookies to let us retrieve the user&rsquo;s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>
        <p>We may set cookies on your computer to know when you visit our website or other websites.</p>
        <p>If your browser doesn't accept cookies, you may not be able to fully use our website. </p>
        <p>We may allow other companies that show advertisements on our pages to set and access cookies on your computer, this policy does not apply to them.</p>
        <p>&nbsp;</p>
        <p><strong>License</strong></p>
        <p>Unless otherwise stated, this website and/or its licensors own the intellectual property rights for all material on this website. All intellectual property rights are reserved. You may access this from this website for your own personal use subjected to restrictions set in these terms and conditions.</p>
          <p>&nbsp;</p>
        <p><strong>You must not:</strong></p>
        <p> Republish material from this website<br/>
          Sell, rent or sub-license material from this website<br/>
          Reproduce, duplicate or copy material from this website<br/>
          Redistribute content from this website</p>
        <p>&nbsp;</p>
        <p><strong>This Agreement shall begin on the date hereof.</strong></p>
        <p>Parts of this website offer an opportunity for users to post information in certain areas of the website. We do not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of this website,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, this website shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website. We reserve the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these terms and conditions.</p>
        <p>&nbsp;</p>
        <p><strong>You warrant and represent that:</strong></p>
        <p> You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;<br>
          The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;<br>
          The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy<br>
          The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</p>
        <p>You hereby grant this website a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p>
        <p>&nbsp;</p>
        <p><strong>Hyperlinking to our Content</strong></p>
        <p>You  may link to our home page, to publications or to other website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party&rsquo;s site.</p>
        <p>&nbsp;</p>
        <p><strong>Content Liability</strong></p>
        <p>We shall not be hold responsible for any content that appears on your website. You agree to protect and defend us against all claims that is rising on your website. No link(s) should appear on any website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>
        <p>&nbsp;</p>
        <p><strong>Reservation of Rights</strong></p>
        <p>We reserve the right to request that you remove all links or any particular link to our website. You approve to immediately remove all links to our website upon request. We also reserve the right to amen these terms and conditions and it&rsquo;s linking policy at any time. By continuously linking to our website, you agree to be bound to and follow these linking terms and conditions.</p>
        <p>&nbsp;</p>
        <p><strong>Removal of links from our website</strong></p>
        <p>If you find any link on our website that is offensive for any reason, you are free to contact us. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p>
        <p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>
        <p>&nbsp;</p>
          <p><strong>Disclaimer</strong></p>
        <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>
        <p> limit or exclude our or your liability for death or personal injury;<br/>
          limit or exclude our or your liability for fraud or fraudulent misrepresentation;<br/>
          limit any of our or your liabilities in any way that is not permitted under applicable law; or<br/>
          exclude any of our or your liabilities that may not be excluded under applicable law.</p>
        <p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>
        <p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
        <p>&nbsp;</p>
        <p><strong>Children&rsquo;s Privacy</strong></p>
        <p>Our Services do not address anyone under the age of 18. We do not knowingly collect personal identifiable information from children under 18. In the case we discover that a child under 18 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>
        <p>&nbsp;</p>
          <p><strong>
          Changes to This Terms and Conditions</strong></p>
        <p>We may update our terms and conditions from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new terms and conditions on this page. These changes are effective immediately, after they are posted on this page. Our terms and conditions was last updated on 25 May 2018.</p>
        <p>&nbsp;</p>
          <p><strong> Contact Us</strong></p>
        <p>If you have any questions or suggestions about our terms and conditions, do not hesitate to contact us. This website operates within the laws of England.</p>
      </div>
    </div>
</asp:Content>

