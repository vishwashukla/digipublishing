﻿using System;
using System.Data;
using System.Web.UI;
using System.Security.Cryptography;
using System.Text;

public partial class ForgotPassword : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Forgot Password";
        lblmessage.Text = "";

        if (!this.IsPostBack)
        {
            this.Session["CaptchaImageText"] = utils.GenerateRandomCode();
        }
        this.imgLogo.Src = ReadConfig.LogoPath;
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Request.Url.Host).ToLower().Trim() == ConfigLoader.GetSAMainURL())
        {
            Response.Redirect("~/" + ConfigLoader.GetSALoginPage());
        }
        else
        {
            Response.Redirect("~/login.aspx");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Request.Url.Host).ToLower().Trim() == ConfigLoader.GetSAMainURL())
        {
            Response.Redirect("~/" + ConfigLoader.GetSALoginPage());
        }
        else
        {
            Response.Redirect("~/login.aspx");
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        if (IsEmailExists(txtEmailID.Text.Trim(),ref dt))
            return;
        if (Session["CaptchaImageText"] != null)
        {
            if (CodeNumberTextBox.Text == Session["CaptchaImageText"].ToString())
            {
                try
                {
                    string strCalculatedPwd = "";
                    RandomPassword rd = new RandomPassword();
                    string strPassword = RandomPassword.Generate(10);
                    int saltSize = 2;
                    Byte[] saltBytes = new byte[saltSize];
                    Byte[] originalBytes;
                    Byte[] encodedBytes;
                    MD5 md5;
                    //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
                    md5 = new MD5CryptoServiceProvider();
                    originalBytes = ASCIIEncoding.Default.GetBytes(RandomPassword.Generate(10));
                    encodedBytes = md5.ComputeHash(originalBytes);
                    //Convert encoded bytes back to a 'readable' string
                    strCalculatedPwd = BitConverter.ToString(encodedBytes).Replace("-", "");
                    
                    if (dt.Rows.Count > 0)
                    {
                        if (!MailerBrand.sendPasswordResetEmail(Convert.ToString(dt.Rows[0]["email"]), strPassword, utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["fullname"])), lblmessage.Text, ReadConfig.GetEmail(), ReadConfig.getDomainUrl()))
                        {
                            lblmessage.Text = "Failure sending password to the user";
                            return;
                        }
                        string strUpdate = "Update brand_user set password = '" + strCalculatedPwd + "' where uid=" + Convert.ToString(dt.Rows[0]["uid"]);
                        dp.ExecuteNonQuery(strUpdate);
                        mvMain.SetActiveView(viewPWDMailmsg);
                    }
                    else
                        lblmessage.Text = "User doesn't exist!";
                }
                catch (Exception ex)
                {
                    lblmessage.Text = ex.Message.ToString();
                }
            }
            else
            {
                CodeNumberTextBox.Text = "";
                Session["CaptchaImageText"] = utils.GenerateRandomCode();
                lblmessage.Text = "please enter valid verification code";
            }
        }
    }

    /// <summary>
    /// This function checks whether enterd LoginName exists or not
    /// </summary>
    /// <param name="LoginName">LoginName of user</param>
    private bool IsEmailExists(string LoginName,ref DataTable dt)
    {
        bool flag = false;
        try
        {
            string strQuery = "Select * from brand_user where email='" + LoginName.Trim() + "' and status=1";
            dt = dp.FillDataTable(strQuery);
            if (dt.Rows.Count <= 0)
            {
                lblmessage.Text = "Unregistered Email Address, The email address you entered has not been registered.";
                CodeNumberTextBox.Text = "";
                Session["CaptchaImageText"] = utils.GenerateRandomCode();
                return flag;
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
        return flag;
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}