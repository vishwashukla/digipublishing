﻿function UNBlockedClick(id) {
    RecordData("&banID=" + id);
}
function BlockedClick(id) {
    RecordData("&banIDBlocked=" + id);
}
function RecordData(str) {
    var url = WebServiceDomain + "Banner_Click_UPD.aspx?DomainForGA=" + DomainForGA + "&REMOTE_ADDR=" + REMOTE_ADDR + "&Referer_URL=" + Referer_URL + "&Campaign_Source=" + Campaign_Source + str;
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", url, true);
    xhttp.send();
}
function SetDisplay(id, val) {
    if (id.indexOf("imageMidArticleBlock") == -1) {
        var a = document.getElementById(id);
        if (a != null) a.style.display = val;
    }
    else {
        var a = document.getElementsByClassName("midArticle_ads_Box");
        for (var i = 0; i < a.length; i++) {
            var ctr = a[i].getElementsByTagName("img");
            for (var j = 0; j < ctr.length; j++) {
                if (ctr[j].id.indexOf("imageMidArticleBlock") > -1) {
                    ctr[j].style.display = val;
                }
            }
        }
    }
}
function SameIpClickCheck(IPStatus, IPValue, SameIPCount, imgid, widid, bName) {
    if (IPStatus == 1 && IPValue <= SameIPCount)
        SetDisplay(imgid, "block");
    if (IPStatus == 1 && IPValue < SameIPCount)
        BlockedClick(widid);
    else {
        try {
            if (ISSendGABannerClick == "1") {

                if (bName != "") {
                    if (typeof _gaq != "undefined") {
                        _gaq.push(['_trackEvent', 'BannerClick', bName, DomainForGA]);
                    }
                    else if (typeof ga != "undefined") {
                        ga('send', 'event', 'BannerClick', bName, DomainForGA);
                    }
                }
            }
        } catch (err) { }
        UNBlockedClick(widid);
    }
}
function CurrentClickCheck(IPStatus, IPValue, SameIPCount, BlockerCount, imgid, widid, BlockerValue, bName) {
    if (BlockerCount >= BlockerValue)
        SetDisplay(imgid, "block");
    if (BlockerCount > BlockerValue)
        BlockedClick(widid);
    else
        SameIpClickCheck(IPStatus, IPValue, SameIPCount, imgid, widid, bName);
}
//Widget Click
function WidgetClick(id, Ftype, IPStatus, IPValue, BlockerValue, imgid, bName, rowindex, PosSameIPCount, PosCount) {
    var strSameIPCount = PosSameIPCount + "_" + rowindex;
    var strPosCount = PosCount + "_" + rowindex;
    window[strSameIPCount] += 1;
    if (Ftype == 0) {
        window[strPosCount] += 1;
        CurrentClickCheck(IPStatus, IPValue, window[strSameIPCount], window[strPosCount], imgid, id, BlockerValue, bName);
    }
    else if (Ftype == 1)
        SameIpClickCheck(IPStatus, IPValue, window[strSameIPCount], imgid, id, bName);
    else if (Ftype == 2)
        BlockedClick(id);
}
///Mid article
function ClickMidArticle(id, Ftype, IPStatus, IPValue, BlockerValue, imgid, bName) {
    SameIPCountMidArticle += 1;
    if (Ftype == 0) {
        MidArticleBlockerCount += 1;
        CurrentClickCheck(IPStatus, IPValue, SameIPCountMidArticle, MidArticleBlockerCount, imgid, id, BlockerValue, bName);
    }
    else if (Ftype == 1)
        SameIpClickCheck(IPStatus, IPValue, SameIPCountMidArticle, imgid, id, bName);
    else if (Ftype == 2)
        BlockedClick(id);
}
$(document).ready(function () {
    var height = $('#top_header_ad_box').height();
    if (DomainForGA.toLowerCase().indexOf("herstage.com") > -1) {
        $('#header').css('height', height + 45);
        $('.nav-box-in-header').css('margin-top', height - 55);
        $('#aside').css('top', height+60);
       
    }

});
//Record banner impression in database
function SetImperssion_Banners() {
    $.ajax({
        type: "Post",
        async: true,
        url: WebServiceDomain + "Services/AdNetworkReport.asmx/Setimperssion",
        data: "{'AdNetworkIDs':'" + BannerIDs + "' ,'type':'banner','REMOTE_ADDR':'" + REMOTE_ADDR + "','domainnameCS':'" + DomainForGA + "','Referer_URL':'" + Referer_URL + "','strAllowAdnetworkData':'" + strAllowAdnetworkData + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) { },
        error: function (e) {
            //alert(e.responseText);
        }
    });
}

//Links
var BlockerLink, BlockerValueLink, SameIPStatusLink, SameIPValueLink, CurrentClickLink, SameIPClickLink, DId = 0;
function GetLinkData(ClickDataSpanId, CurrentClickSpanId, SameIPCLickId, BlockImageId, Id, LinkId, index) {
    var spandta = document.getElementById(ClickDataSpanId + index).innerText.split(",");

    BlockerLink = parseInt(spandta[0]);
    BlockerValueLink = parseInt(spandta[1]);
    if (BlockerValueLink <= 0) {
        BlockerLink = 0;
    }
    SameIPStatusLink = parseInt(spandta[2]);
    SameIPValueLink = parseInt(spandta[3]);
    if (SameIPValueLink <= 0) {
        SameIPStatusLink = 0;
    }

    DId = parseInt(spandta[4]);
    var curclickspandata = document.getElementById(CurrentClickSpanId + index).innerText;
    CurrentClickLink = curclickspandata == "" ? 0 : parseInt(curclickspandata);

    var linkclicked = document.getElementById(Id + index);
    var blockimage, spansameipclicccount;
    var allimages = linkclicked.parentElement.getElementsByTagName("img");
    for (var i = 0; i < allimages.length; i++) {
        if (allimages[i].getAttribute("id").indexOf(BlockImageId) != -1) {
            blockimage = allimages[i];
        }
    }
    var allspans = linkclicked.parentElement.getElementsByTagName("span");
    for (var i = 0; i < allspans.length; i++) {
        if (allspans[i].getAttribute("id").indexOf(SameIPCLickId) != -1) {
            spansameipclicccount = allspans[i];
        }
    }
    var cursameipclickspandata = spansameipclicccount.innerText;
    SameIPClickLink = cursameipclickspandata == "" ? 0 : parseInt(cursameipclickspandata);

    SameIPClickLink++;
    CurrentClickLink++;
    spansameipclicccount.innerText = SameIPClickLink + "";
    document.getElementById(CurrentClickSpanId + index).innerText = CurrentClickLink + "";
    var isSameIPBlockReachedLink = false;
    if (SameIPStatusLink == 0) {
        if (BlockerLink == 1) {
            if (BlockerLink == 1 && BlockerValueLink <= CurrentClickLink) {
                //set image display block
                //blockimage.style.display = "block";
                blockimage.parentElement.parentElement.style.display = "none";
            }
            if (BlockerLink == 1 && BlockerValueLink < CurrentClickLink) {
                //blocked click
                //RecordClickLink(LinkId, DId, 1);
            }
            else {
                //unblocked click
                RecordClickLink(LinkId, DId, 0);
            }
        }
        else {
            RecordClickLink(LinkId, DId, 0);
        }
    }
    else {
        if (SameIPStatusLink == 1 && SameIPValueLink <= SameIPClickLink) {
            //set image display block
            isSameIPBlockReachedLink = true;
            //blockimage.style.display = "block";
            blockimage.parentElement.parentElement.style.display = "none";
        }
        if (SameIPStatusLink == 1 && SameIPValueLink < SameIPClickLink) {
            //blocked click
            //RecordClickLink(LinkId,DId,1);
        }
        else {
            if (!isSameIPBlockReachedLink) {
                if (BlockerLink == 1) {
                    if (BlockerLink == 1 && BlockerValueLink <= CurrentClickLink) {
                        //set image display block
                        //blockimage.style.display = "block";
                        blockimage.parentElement.parentElement.style.display = "none";
                    }
                    if (BlockerLink == 1 && BlockerValueLink < CurrentClickLink) {
                        //blocked click
                        //RecordClickLink(LinkId, DId, 1);
                    }
                    else {
                        //unblocked click
                        RecordClickLink(LinkId, DId, 0);
                    }
                }
                else {
                    RecordClickLink(LinkId, DId, 0);
                }
            }
            else {
                RecordClickLink(LinkId, DId, 0);
            }
        }
    }
}
function RecordClickLink(lid, did, blockstatus) {
    var url = WebServiceDomain + "Link_Click_UPD.aspx?linkID=" + lid + "&domainID=" + did + "&blockstatus=" + blockstatus;
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", url, true);
    xhttp.send();
}