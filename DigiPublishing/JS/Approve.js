﻿function processResponse() {
	
    if (httpRequester.readyState == 4) {
        if (httpRequester.responseText.indexOf('invalid') == -1) {
            document.getElementById('divcountry').innerHTML = httpRequester.responseText;
            couldProcess = false;
        }
    }
}

var couldProcess = false;
function fillCountryCbo(tRegion) {
    var serverSideURL = "AjaxCountry_Admin.aspx?region=";
    var messageID = tRegion;
    if (!couldProcess && httpRequester) {
        httpRequester.open("GET", serverSideURL + messageID, true);
        httpRequester.onreadystatechange = processResponse;
        couldProcess = true;
        httpRequester.send(null);
    }
}

function getHTTPRequestObject() {
    var xmlHttpRequest;
    if (!xmlHttpRequest && typeof XMLHttpRequest != 'undefined') {
        try {
            xmlHttpRequest = new XMLHttpRequest();
        }
        catch (exception) {
            xmlHttpRequest = false;
        }
    }
    return xmlHttpRequest;
}
var httpRequester = getHTTPRequestObject(); // Create the xml http object on the page load

function confirmingSubmit(id) {
    var agree = confirm("Are you sure, you want to change the image?");
    if (agree)
        window.open("PhotoUpload.aspx?id=" + id, "xx", "width=400,height=150");
}

function ShowChannelVideo(id, cname) {
    window.open("../DigipSuperAdmin/ShowChannelVideo.aspx?id=" + id + "&cname=" + cname, "xx", "width=600,height=600");
}

function confirmSubmit(id, pno, sort, order, action) {
    if (confirm("Are you sure you wish to continue?")) {
        form1.action = "Approve.aspx?id=" + id + "&action=" + action + "&pno=" + pno + "&sort=" + sort + "&order=" + order;
        form1.method = "post";
        form1.submit();
    }
}

function confirmingSubmitaddUserChannel(id) {
    window.open("AddToUserChannel.aspx?id=" + id, "xx", "width=450,height=300");
}