﻿// JScript File
//Show addedElement JavaScript Function

var numi = "";
var num;
var count;
function showaddElement(id, txtvalue) {

    var data = txtvalue.split('>>')//19>>1,2,3;4,5,6

    var txtid = data[0];
    var txtvalues = data[1];
    if (id == txtid) {
        var countvalue = txtvalues.split(';');
        count = countvalue.length;
    }
    numi = document.getElementById('ContentPlaceHolder1_addMore');


    if (count > 0) {
        numi.value = count;
        num = (count - 1) + 2;
        numi.value = num;

        var newdiv = document.createElement('div');
        var showDiv = document.getElementById('myDiv');
        for (var i = 0; i < count; i++) {
            var newdiv = document.createElement('div');
            var divIdName = 'my' + (i + 1) + 'Div';
            newdiv.setAttribute('id', divIdName);

            var cvalue = countvalue[i].split(',');
            newdiv.innerHTML = newdiv.innerHTML + '<input type="text" value="' + cvalue[0] + '" id="txtVideoUrl' + (i + 1) + '" disabled="true"/>&nbsp;';
            newdiv.innerHTML = newdiv.innerHTML + '<input type="text" value="' + cvalue[1] + '" id="txtClickThroughUrl' + (i + 1) + '" disabled="true"/>&nbsp;';
            newdiv.innerHTML = newdiv.innerHTML + '<input type="text" value="' + cvalue[2] + '" id="txtImpressionsUrl' + (i + 1) + '" disabled="true"/>&nbsp;';
            newdiv.innerHTML = newdiv.innerHTML + '<input type="button" value="Populate" id="btnRemove' + (i + 1) + '" onclick="removeElement(\'' + divIdName + '\',this)"/>';
            newdiv.innerHTML = newdiv.innerHTML + '<br>';

            showDiv.appendChild(newdiv);
        }
    }
    else {
        num = (document.getElementById('ContentPlaceHolder1_addMore').value - 1) + 2;
        numi.value = num;
    }
}
//addElement JavaScript Function
function addElement() {

    var orgDV = document.getElementById('myDiv');
    GetchildControls(orgDV);
    var newdiv = document.createElement('div');
    var divIdName = 'my' + num + 'Div';
    newdiv.setAttribute('id', divIdName);
    //var numi;
    newdiv.innerHTML = newdiv.innerHTML + '<input type="text" value="" id="txtVideoUrl' + num + '" />&nbsp;';
    newdiv.innerHTML = newdiv.innerHTML + '<input type="text" value="" id="txtClickThroughUrl' + num + '" />&nbsp;';
    newdiv.innerHTML = newdiv.innerHTML + '<input type="text" value="" id="txtImpressionsUrl' + num + '" />&nbsp;';
    newdiv.innerHTML = newdiv.innerHTML + '<input type="button" value="Populate" id="btnRemove' + num + '" onclick="removeElement(\'' + divIdName + '\',this)"/>';

    num = (num - 1) + 2;
    numi.value = num;

    orgDV.appendChild(newdiv);
}
function GetchildControls(orgDV) {

    var divs = orgDV.getElementsByTagName("div");
    for (var i = 0; i < divs.length; i++) {
        dv = document.getElementById(divs[i].id);

        var collection = dv.getElementsByTagName('input');
        for (var x = 0; x < collection.length; x++) {
            if (collection[x].type.toUpperCase() == 'TEXT')
                document.getElementById(collection[x].id).disabled = true;
        }
    }
}

function GetchildControlValues() {
    var orgDV = document.getElementById('myDiv');
    var divs = orgDV.getElementsByTagName("div");

    var hdnValues = document.getElementById("ContentPlaceHolder1_hdnValues");
    hdnValues.value = "";

    for (var i = 0; i < divs.length; i++) {
        dv = document.getElementById(divs[i].id);

        var collection = dv.getElementsByTagName('input');
        for (var x = 0; x < collection.length; x++) {
            if (collection[x].type.toUpperCase() == 'TEXT') {
                if (hdnValues.value == "") {
                    if (document.getElementById(collection[x].id).value != "")
                        hdnValues.value = document.getElementById(collection[x].id).value;
                }
                else
                    if (document.getElementById(collection[x].id).value != "")
                        hdnValues.value = hdnValues.value + "," + document.getElementById(collection[x].id).value;
            }
            if (collection[x].type.toUpperCase() == 'BUTTON') {
                if (hdnValues.value != "")
                    hdnValues.value = hdnValues.value + "BUTTON";
            }
        }
    }
    hdnValues.value = hdnValues.value.substring(0, hdnValues.value.lastIndexOf("BUTTON"));
}

//removeElement JavaScript Function
function removeElement(divNum, btn) {
    var orgDV = document.getElementById('myDiv');
    var olddiv = document.getElementById(divNum);
    if (btn.value.toLowerCase() == 'populate') {
        if (olddiv != null) {
            var collection = olddiv.getElementsByTagName('input');
            for (var x = 0; x < collection.length; x++) {
                if (collection[x].type.toUpperCase() == 'TEXT')
                    document.getElementById(collection[x].id).disabled = false;

                if (collection[x].type.toUpperCase() == 'BUTTON')
                    document.getElementById(collection[x].id).value = "Remove";
            }
        }
    }
    else if (btn.value.toLowerCase() == 'remove') {
        orgDV.removeChild(olddiv);
    }
}