function passwordStrengthCheck(pwd, rfvCPWD) {
    var passwd = document.getElementById(pwd).value;
    if (passwd.length > 0) {
        rfvCPWD.enabled = true;
    }
    var description = new Array();
    description[0] = '<table border=0><tr valign=top><td><table><tr valign=top><td height=10 width=30 bgcolor=#ff0000><\/td><td height=5 width=120 bgcolor=#dddddd><\/td><\/tr><\/table><\/td><td class=bold>Weakest<\/td><\/tr><\/table>';
    description[1] = '<table border=0><tr valign=top><td><table><tr valign=top><td height=10 width=60 bgcolor=#bb0000><\/td><td height=5 width=90 bgcolor=#dddddd><\/td><\/tr><\/table><\/td><td class=bold>Weak<\/td><\/tr><\/table>';
    description[2] = '<table border=0><tr valign=top><td><table><tr valign=top><td height=10 width=90 bgcolor=#ff9900><\/td><td height=5 width=60 bgcolor=#dddddd><\/td><\/tr><\/table><\/td><td class=bold>Medium<\/td><\/tr><\/table>';
    description[3] = '<table border=0><tr valign=top><td><table><tr valign=top><td height=10 width=120 bgcolor=#00bb00><\/td><td height=5 width=30 bgcolor=#dddddd><\/td><\/tr><\/table><\/td><td class=bold>Strong<\/td><\/tr><\/table>';
    description[4] = '<table border=0><tr valign=top><td><table><tr valign=top><td height=10 width=150 bgcolor=#00ee00><\/td><\/tr><\/table><\/td><td class=bold>Strongest<\/td><\/tr><\/table>';
    description[5] = '<table border=0><tr valign=top><td><table><tr valign=top><td height=10 width=150 bgcolor=#dddddd><\/td><\/tr><\/table><\/td><td class=bold><\/td><\/tr><\/table>';

    var intScore = 0
    var strVerdict = 0

    // PASSWORD LENGTH
    if (passwd.length == 0 || !passwd.length)                         // length 0
    {
        intScore = -1
    }
    else if (passwd.length > 0 && passwd.length < 5) // length between 1 and 4
    {
        intScore = (intScore + 3)
    }
    else if (passwd.length > 4 && passwd.length < 8) // length between 5 and 7
    {
        intScore = (intScore + 6)
    }
    else if (passwd.length > 7 && passwd.length < 12)// length between 8 and 15
    {
        intScore = (intScore + 12)
    }
    else if (passwd.length > 11)                    // length 16 or more
    {
        intScore = (intScore + 15)
    }


    // LETTERS (Not exactly implemented as dictacted above because of my limited understanding of Regex)
    if (passwd.match(/[a-z]/))                              // [verified] at least one lower case letter
    {
        intScore = (intScore + 1)
    }

    if (passwd.match(/[A-Z]/))                              // [verified] at least one upper case letter
    {
        intScore = (intScore + 5)
    }

    // NUMBERS
    if (passwd.match(/\d+/))                                 // [verified] at least one number
    {
        intScore = (intScore + 5)
    }



    // COMBOS
    if (passwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))        // [verified] both upper and lower case
    {
        intScore = (intScore + 2)
    }

    if (passwd.match(/(\d.*\D)|(\D.*\d)/))                    // [FAILED] both letters and numbers, almost works because an additional character is required
    {
        intScore = (intScore + 2)
    }

    // [verified] letters, numbers, and special characters


    if (intScore == -1) {
        strVerdict = description[5];
    }
    else if (intScore > -1 && intScore < 12) {
        strVerdict = description[0];
    }
    else if (intScore > 11 && intScore < 17) {
        strVerdict = description[1];
    }
    else if (intScore > 16 && intScore < 25) {
        strVerdict = description[2];
    }
    else if (intScore > 24 && intScore < 35) {
        strVerdict = description[3];
    }
    else {
        strVerdict = description[4];
    }

    document.getElementById("Words").innerHTML = (strVerdict);
}

function alphanumericPwd() {
    var getdiv = document.getElementById("dvPwd");
    if (((event.keyCode < 65) || (event.keyCode > 90)) && ((event.keyCode < 97) || (event.keyCode > 122)) && ((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 45) && (event.keyCode != 95) && (event.keyCode != 46)) {
        getdiv.innerHTML = "<Font color='red'>only alphanumeric characters are allowed</Font>";
        return false;
    }
    else {
        getdiv.innerHTML = "";
    }
}



