﻿
function Browserall(rdbBrowserAll, lstBrowsers) {
    var x = document.getElementById(rdbBrowserAll);
    if (x.checked) {
        var Browser = document.getElementById(lstBrowsers);
        for (i = 0; i < Browser.options.length; i++) {
            if (Browser.options[i].value != "-1") {
                Browser.options[i].selected = true;
            }
        }
    }
}

function BrowserSel(rdbBrowserSel, lstBrowsers) {
    var x = document.getElementById(rdbBrowserSel);
    if (x.checked) {
        var Browser = document.getElementById(lstBrowsers);
        for (i = 0; i < Browser.options.length; i++) {
            if (Browser.options[i].value != "-1") {
                Browser.options[i].selected = false;
            }
        }
    }
}

function client_OnTreeNodeChecked(e) {
    var obj;
    if (window.event) {
        obj = window.event.srcElement || window.event.target;
    }
    else {
        obj = e.srcElement || e.target;
    }
    var treeNodeFound = false;
    var checkedState;
    if (obj.tagName == "INPUT" && obj.type == "checkbox") {
        var treeNode = obj;
        checkedState = treeNode.checked;
        do {
            obj = obj.parentNode;
        }
        while (obj.tagName != "TABLE")
        var parentTreeLevel = obj.rows[0].cells.length;
        var parentTreeNode = obj.rows[0].cells[0];
        var tables = obj.parentNode.getElementsByTagName("TABLE");
        var numTables = tables.length
        if (numTables >= 1) {
            for (i = 0; i < numTables; i++) {
                if (tables[i] == obj) {
                    treeNodeFound = true;
                    i++;
                    if (i == numTables) {
                        return;
                    }
                }
                if (treeNodeFound == true) {
                    var childTreeLevel = tables[i].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel) {
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                    }
                    else {
                        return;
                    }
                }
            }
        }
    }
}


//Get site(s) from selected genre(s)
function GetSites(lstGenre, lstSites) {
    var Genre = document.getElementById(lstGenre);
    var GenreIds = "";
    for (i = 0; i < Genre.options.length; i++) {
        if (Genre.options[i].value != "-1") {
            if (Genre.options[i].selected == true) {
                if (GenreIds == "") {
                    GenreIds = Genre.options[i].value;
                }
                else {
                    GenreIds = GenreIds + "','" + Genre.options[i].value;
                }
            }
        }
    }
    SelSites(GenreIds, lstSites);
}
function SelSites(id, lstSites) {
    var XMLhttpObj = false;
    if (typeof XMLHttpRequest != 'undefined') {
        XMLhttpObj = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        try {
            XMLhttpObj = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch (e) {
            try {
                XMLhttpObj = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (e)
            { }
        }
    }
    if (!XMLhttpObj) return;

    XMLhttpObj.onreadystatechange = function () {
        if (XMLhttpObj.readyState == 4) { // when request is complete
            var Coun = XMLhttpObj.responseText;
            var arrCoun = Coun.split(",");

            var sites = document.getElementById(lstSites);
            for (k = 0; k < sites.options.length; k++) {
                sites.options[k].selected = false;
            }
            for (var j = 0; j < arrCoun.length; j++) {
                for (i = 0; i < sites.options.length; i++) {
                    if (sites.options[i].value == arrCoun[j]) {
                        sites.options[i].selected = true;
                    }
                }
            }
        }
    };

    url = "GetSitesOnGenre.aspx?GenreIDS=" + id;
    XMLhttpObj.open('GET', url, true);
    XMLhttpObj.send(null);
}

function confirmSubmit(id, hndAdnetworkid, btnSetStatus) {
    var agree = confirm("Are you sure you wish to continue?");
    if (agree) {
        var hndAdnetworkid = document.getElementById(hndAdnetworkid);
        if (hndAdnetworkid != null)
            hndAdnetworkid.value = id;
        var btnSetStatus = document.getElementById(btnSetStatus);
        if (btnSetStatus != null)
            btnSetStatus.click();
        return true;
    }
    else {
        return false;
    }
}