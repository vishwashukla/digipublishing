﻿<%@ WebService Language="C#" Class="AdNetworkReport" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[ScriptService]
public class AdNetworkReport  : System.Web.Services.WebService {

    DBHelper dp = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);

    [WebMethod(Description = "To Save Banner/Ad impression in DB")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Setimperssion(string AdNetworkIDs, string type, string REMOTE_ADDR, string domainnameCS, string Referer_URL, string strAllowAdnetworkData)
    {
        AdNetworkIDs = AdNetworkIDs.TrimEnd(',');
        if (GetStartADNetworkAndWidgetReport(strAllowAdnetworkData) == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_AdNetworks_Impression";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@UserIP", REMOTE_ADDR);
            cmd.Parameters.Add("@AdNetworkIDs", AdNetworkIDs);
            cmd.Parameters.Add("@Domain", domainnameCS);
            cmd.Parameters.Add("@Type", type);
            cmd.Parameters.Add("@RefererURL", Referer_URL);
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    private int GetStartADNetworkAndWidgetReport(string strAllowAdnetworkData)
    {
        int intAllowAdnetworkData = 0;
        if (!string.IsNullOrWhiteSpace(strAllowAdnetworkData))
        {
            intAllowAdnetworkData = Convert.ToInt32(strAllowAdnetworkData);
        }
        else
        {
            DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            DataTable dtcheck = dpSA.FillDataTable("select isnull(Start_Stop,0) as Start_Stop from brand_Record_Data");
            if (dtcheck.Rows.Count > 0)
                intAllowAdnetworkData = Convert.ToInt32(dtcheck.Rows[0]["Start_Stop"].ToString());
        }
        return intAllowAdnetworkData;
    }
}