﻿<%@ WebService Language="C#" Class="RSSFeed" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Xml;
using Newtonsoft.Json;
using System.Text;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[ScriptService]
public class RSSFeed : System.Web.Services.WebService
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);

    [WebMethod(Description = "Get requested RSS or all RSS")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetRSS(string rssname, string rssitem, string RequestedPage)
    {
        String StrResponse = "";
        if (!string.IsNullOrWhiteSpace(rssname))
            StrResponse = ShowRSS(System.Web.HttpUtility.UrlDecode(rssname).TrimEnd('.').Replace('-', ' '), System.Web.HttpUtility.UrlDecode(rssitem).TrimEnd('.').Replace('-', ' '));
        else
        {
            int CurrentPage = 0;
            if (!string.IsNullOrWhiteSpace(RequestedPage))
                CurrentPage = Convert.ToInt32(RequestedPage);
            StrResponse = ShowAllRss(CurrentPage);
        }

        XmlDocument xdoc = new XmlDocument();
        xdoc.LoadXml(StrResponse);
        return JsonConvert.SerializeXmlNode(xdoc);
    }

    /// <summary>
    /// Method to show RSS feed details 
    /// </summary>
    /// <param name="title"></param>
    /// <param name="rssitem"></param>
    private string ShowRSS(string title, string rssitem)
    {
        DBCommand strQuery = new DBCommand();
        strQuery.CommandType = CommandType.StoredProcedure;
        strQuery.CommandText = "SP_Brand_Select_Top1_Feed_Brand_Rss";
        strQuery.AddParameter("@title", utils.formatStringForDBInsert(title));
        DataTable dt = dp.FillDataTable(strQuery);
        string RssTitle = String.Empty;
        if (dt.Rows.Count > 0)
        {
            RssTitle = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["title"]));
        }

        StringBuilder SBPaging = new StringBuilder();
        string CurrentPageCount = "";
        try
        {
            DBCommand strSql = new DBCommand();
            strSql.CommandType = CommandType.StoredProcedure;
            strSql.CommandText = "SP_Brand_Select_Rss";
            DataTable dtAllRss = new DataTable();
            dtAllRss = dp.FillDataTable(strSql);
            int CurrentPage = -1;
            if (dtAllRss.Rows.Count > 0)
            {
                SBPaging.AppendLine("<ul class='ulPagingFeeds'>");
                for (int i = 0; i < dtAllRss.Rows.Count; i++)
                {
                    string currRssTitle = utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"]));
                    if(currRssTitle == RssTitle)
                    {
                        CurrentPage = i;
                    }
                    SBPaging.AppendLine("<li class='liPagingFeeds'>");
                    if (i == CurrentPage)
                    {
                        SBPaging.Append("<a class='feedpaging-disabled'>" + utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])) + "</a>");
                    }
                    else
                    {
                        string url = "http://" + ReadConfig.getDomainUrl() + "/updates/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])));
                        //SBPaging.Append("<a onclick='PagingClick(" + i + ")' class='feedpaging-enabled'>" + utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])) + "</a>");
                        SBPaging.Append("<a href='" + url + "' class='feedpaging-enabled'>" + utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])) + "</a>");
                    }
                    SBPaging.Append("</li>");
                }
                SBPaging.AppendLine("</ul>");
                CurrentPageCount = "Page " + (CurrentPage + 1) + " of " + dtAllRss.Rows.Count;
            }
        }
        catch
        {
        }

        String StrResponse = "<Result><Message>Server processing error!</Message></Result>";
        try
        {
            if (dt.Rows.Count > 0)
            {
                RssTitle = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["title"]));

                try
                {
                    DataTable dtRss = utils.LoadRSS(Convert.ToString(dt.Rows[0]["link"]), RssTitle);
                    if (dtRss.Rows.Count > 0)
                    {
                        dtRss.TableName = "RSSitem";
                        string strxml = String.Empty;
                        if (!string.IsNullOrWhiteSpace(rssitem))
                        {
                            DataView dv = new DataView(dtRss);
                            dv.RowFilter = "titleURL like '%" + Convert.ToString(rssitem).Trim().Replace("'", "''") + "%'";
                            strxml = DataTableToXML.ConvertDataTableToXml(dv.ToTable()).Replace("<Result>", "").Replace("</Result>", "");

                            StrResponse = "<Result><Message>success</Message>"
                                        + "<RssTitle>" + RssTitle + "</RssTitle>"
                                        + strxml;
                            if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                                StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

                            if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                                StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
                            StrResponse += "</Result>";
                        }
                        else
                        {
                            strxml = DataTableToXML.ConvertDataTableToXml(dtRss).Replace("<Result>", "").Replace("</Result>", "");

                            StrResponse = "<Result><Message>success</Message>"
                                        + "<RssTitle>" + RssTitle + "</RssTitle>"
                                        + strxml;
                            if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                                StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

                            if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                                StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
                            StrResponse += "</Result>";
                        }
                    }
                    else
                    {
                        StrResponse = "<Result><Message>Feed not available!</Message><RssTitle>" + RssTitle + "</RssTitle>";
                        if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                            StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

                        if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                            StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
                        StrResponse += "</Result>";
                    }

                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("timed out"))
                    {
                        StrResponse = "<Result><Message>Sorry the Content Feed is taking too long to load.</Message><RssTitle>" + RssTitle + "</RssTitle>";
                        if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                            StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

                        if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                            StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
                        StrResponse += "</Result>";
                    }
                    else
                    {
                        //StrResponse = "<Result><Message>"+ex.Message+"</Message><RssTitle>" + RssTitle + "</RssTitle></Result>";
                        StrResponse = "<Result><Message>Feed not available!</Message><RssTitle>" + RssTitle + "</RssTitle>";
                        if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                            StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

                        if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                            StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
                        StrResponse += "</Result>";
                    }
                }
            }
            else
            {
                StrResponse = "<Result><Message>Feed does not exist.</Message>";
                if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                    StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

                if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                    StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
                StrResponse += "</Result>";
            }
        }
        catch
        {
            StrResponse = "<Result><Message>Feed not available!</Message>";
            if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
                StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

            if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
                StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";
            StrResponse += "</Result>";
        }
        return StrResponse;
    }

    private string ShowAllRss(int CurrentPage)
    {
        String StrResponse = "<Result>";
        StringBuilder SBPaging = new StringBuilder();
        string CurrentPageCount = "";
        try
        {
            DBCommand strSql = new DBCommand();
            strSql.CommandType = CommandType.StoredProcedure;
            strSql.CommandText = "SP_Brand_Select_Rss";
            DataTable dtAllRss = new DataTable();
            dtAllRss = dp.FillDataTable(strSql);
            if (dtAllRss.Rows.Count > 0)
            {
                SBPaging.AppendLine("<ul class='ulPagingFeeds'>");
                for (int i = 0; i < dtAllRss.Rows.Count; i++)
                {
                    SBPaging.AppendLine("<li class='liPagingFeeds'>");
                    if (i == CurrentPage)
                    {
                        SBPaging.Append("<a class='feedpaging-disabled'>" + utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])) + "</a>");
                    }
                    else
                    {
                        string url = "http://" + ReadConfig.getDomainUrl() + "/updates/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])));
                        //SBPaging.Append("<a onclick='PagingClick(" + i + ")' class='feedpaging-enabled'>" + utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])) + "</a>");
                        SBPaging.Append("<a href='" + url + "' class='feedpaging-enabled'>" + utils.formatStringForDBSelect(Convert.ToString(dtAllRss.Rows[i]["title"])) + "</a>");
                    }
                    SBPaging.Append("</li>");
                }
                SBPaging.AppendLine("</ul>");
                CurrentPageCount = "Page " + (CurrentPage + 1) + " of " + dtAllRss.Rows.Count;

                string id = Convert.ToString(dtAllRss.Rows[CurrentPage]["feed_id"]);

                strSql = new DBCommand();
                strSql.CommandType = CommandType.StoredProcedure;
                strSql.CommandText = "SP_Brand_Select_BrandRss_OnFeedId";
                strSql.AddParameter("@FeedId", id);
                DataTable dt = dp.FillDataTable(strSql);
                string RssTitle = String.Empty;
                if (dt.Rows.Count > 0)
                {
                    RssTitle = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["title"]));

                    try
                    {
                        DataTable dtRss = utils.LoadRSS(Convert.ToString(dt.Rows[0]["link"]), RssTitle);
                        if (dtRss.Rows.Count > 0)
                        {
                            dtRss.TableName = "RSSitem";
                            string strxml = DataTableToXML.ConvertDataTableToXml(dtRss).Replace("<Result>", "").Replace("</Result>", "");

                            StrResponse += "<Message>success</Message>"
                                        + "<RssTitle>" + RssTitle + "</RssTitle>"
                                        + strxml;
                        }
                        else
                            StrResponse += "<Message>Feed not available!</Message>";
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.ToLower().Contains("timed out"))
                            StrResponse += "<Message>Sorry the Content Feed is taking too long to load.</Message><RssTitle>" + RssTitle + "</RssTitle>";
                        else
                            StrResponse += "<Message>Feed not available!</Message><RssTitle>" + RssTitle + "</RssTitle>";
                    }
                }
                else
                    StrResponse += "<Message>Feed does not exists.</Message>";
            }
            else
                StrResponse += "<Message>No RSS feeds added.</Message>";
        }
        catch
        {
            StrResponse += "<Message>Feed not available!</Message>";
        }
        if (!StrResponse.Contains("<Message>"))
            StrResponse += "<Message>Server processing error!</Message>";

        if (!string.IsNullOrWhiteSpace(SBPaging.ToString()))
            StrResponse += "<Paging>" + System.Web.HttpUtility.HtmlEncode(SBPaging.ToString()) + "</Paging>";

        if (!string.IsNullOrWhiteSpace(CurrentPageCount.ToString()))
            StrResponse += "<CurrentPageCount>" + CurrentPageCount.ToString() + "</CurrentPageCount>";

        StrResponse += "</Result>";
        return StrResponse;
    }

}