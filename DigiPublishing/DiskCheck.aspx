﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiskCheck.aspx.cs" Inherits="DiskCheck" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <span><b>C Drive</b></span>
        <table>
            <tr>
                <td>
                    <span>Total Storage:</span>
                </td>
                <td>
                    <label id="lbltotal" runat="server"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <span>Available Storage:</span>
                </td>
                <td>
                    <label id="lblavailabe" runat="server"></label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
