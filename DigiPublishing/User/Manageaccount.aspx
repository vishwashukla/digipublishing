﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="Manageaccount.aspx.cs" Inherits="User_Manageaccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <script type="text/javascript">
        function ShowEmailedit()
        {
            document.getElementById("<%=trEmail.ClientID%>").style.display = "none";
            document.getElementById("<%=trEmail1.ClientID%>").style.display = "none";
            document.getElementById("<%=trEmailEdit1.ClientID%>").style.display = "table-row";
        }
        function HideEditEmail() {
            document.getElementById("<%=trEmail.ClientID%>").style.display = "table-row";
            document.getElementById("<%=trEmail1.ClientID%>").style.display = "table-row";
            document.getElementById("<%=trEmailEdit1.ClientID%>").style.display = "none";
        }
        function ShowPasswordedit() {
            document.getElementById("<%=trPassword.ClientID%>").style.display = "none";
            document.getElementById("<%=trPassword1.ClientID%>").style.display = "none";
            document.getElementById("<%=trPasswordEdit.ClientID%>").style.display = "table-row";
        }
        function HideEditPassword() {
            document.getElementById("<%=trPassword.ClientID%>").style.display = "table-row";
            document.getElementById("<%=trPassword1.ClientID%>").style.display = "table-row";
            document.getElementById("<%=trPasswordEdit.ClientID%>").style.display = "none";
        }
    </script>
    <div id="content-full">
        <h2>Account Settings</h2>
        <hr />
        <asp:Label ID="msgUserSetting" runat="server" ForeColor="Red"></asp:Label>
        <table class="UserSettings">
            <tr>
                <td>
                    <label>Username</label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblUsername"></asp:Label>
                </td>
            </tr>
            <tr id="trEmail1" runat="server">
                <td>
                    <label>Email Address</label>
                </td>
            </tr>
            <tr id="trEmail" runat="server">
                <td>
                    <asp:Label runat="server" ID="lblEmail"></asp:Label>
                    <a href="javascript:void(0);" onclick="ShowEmailedit()">Change</a>
                </td>
            </tr>
            <tr id="trEmailEdit1" class="usersettingbuttons" runat="server" style="display:none;">
                <td>
                    <div>
                        <label>Email Address</label>
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="64" CssClass="txtLoginRegister" placeholder="Enter Email" ValidationGroup="vgEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server"
                                    ErrorMessage="Please enter email." ForeColor="Red" Display="Dynamic" ValidationGroup="vgEmail"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ControlToValidate="txtEmail" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$"
                                ID="regvEmail" runat="server" Display="Dynamic" ForeColor="Red" ErrorMessage="Invalid email address" ValidationGroup="vgEmail"></asp:RegularExpressionValidator>
                        <br />
                        <label>Password</label>
                        <asp:TextBox ID="txtPasswordEmail" runat="server" TextMode="Password" MaxLength="10" placeholder="Enter Password" CssClass="txtLoginRegister" ValidationGroup="vgEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPassword" ControlToValidate="txtPasswordEmail" runat="server"
                                    ErrorMessage="Please enter password." ForeColor="Red" Display="Dynamic" ValidationGroup="vgEmail"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Button ID="btnChangeEmail" runat="server" Text="Change Email" OnClick="btnChangeEmail_Click" CssClass="btnSuscribe" ValidationGroup="vgEmail" />
                        <input type="button" value="Cancel" class="btnCancel" onclick="HideEditEmail()" />
                    </div>
                </td>
            </tr>
            <tr id="trPassword1" runat="server">
                <td>
                    <label>Password</label>
                </td>
            </tr>
            <tr id="trPassword" runat="server">
                <td>
                    <asp:Label runat="server" ID="lblPassword"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnPassword" />
                    <a href="javascript:void(0);" onclick="ShowPasswordedit()">Change</a>
                </td>
            </tr>
            <tr id="trPasswordEdit" runat="server" style="display:none;">
                <td class="usersettingbuttons">
                    <div>
                        <label>Current Password</label>
                        <asp:TextBox ID="txtcurrentPassword" runat="server" TextMode="Password" MaxLength="10" placeholder="Enter Password" CssClass="txtLoginRegister" ValidationGroup="vgPassword"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtcurrentPassword" ControlToValidate="txtcurrentPassword" runat="server"
                                    ErrorMessage="Please enter current password." ForeColor="Red" Display="Dynamic" ValidationGroup="vgPassword"></asp:RequiredFieldValidator>
                        <br />
                        <label>New Password</label>
                        <asp:TextBox ID="txtnewPassword" runat="server" TextMode="Password" MaxLength="10" placeholder="Enter New Password" CssClass="txtLoginRegister" ValidationGroup="vgPassword"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtnewPassword" ControlToValidate="txtnewPassword" runat="server"
                                    ErrorMessage="Please enter new password." ForeColor="Red" Display="Dynamic" ValidationGroup="vgPassword"></asp:RequiredFieldValidator>
                        <br />
                        <label>Re-enter New Password</label>
                        <asp:TextBox ID="txtconfirmPassword" runat="server" TextMode="Password" MaxLength="10" placeholder="Re-enter New Password" CssClass="txtLoginRegister" ValidationGroup="vgPassword"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtconfirmPassword" runat="server"
                                    ErrorMessage="Please enter re-enter password." ForeColor="Red" Display="Dynamic" ValidationGroup="vgPassword"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Button ID="btnChangePasword" runat="server" Text="Change Password" OnClick="btnChangePasword_Click" CssClass="btnSuscribe" ValidationGroup="vgPassword" />
                        <input type="button" value="Cancel" class="btnCancel" onclick="HideEditPassword()" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

