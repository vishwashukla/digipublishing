﻿using System;
using System.Data;
using System.Web.UI;

public partial class User_Manageaccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Manage Account";
        if(Session["userid"] == null || Convert.ToString(Session["userid"] ) == "0")
        {
            Response.Redirect("~/Default.aspx");
        }
        msgUserSetting.Text = "";
        if(!IsPostBack)
        {
            LoadAccountSettings();
        }

    }

    private void LoadAccountSettings()
    {
        DataTable dt = utils.GetUserDetail(Session["userid"].ToString());
        if(dt.Rows.Count > 0)
        {
            lblUsername.Text = dt.Rows[0]["uname"].ToString();
            lblEmail.Text = dt.Rows[0]["email"].ToString();
            txtEmail.Text = dt.Rows[0]["email"].ToString();
            lblPassword.Text = "••••••••••••••";
            hdnPassword.Value = dt.Rows[0]["password"].ToString();
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }

    protected void btnChangeEmail_Click(object sender, EventArgs e)
    {
        msgUserSetting.Text = "";
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string encodedpassword = utils.EncryptPassword(txtPasswordEmail.Text.Trim());
        string userEmail = txtEmail.Text.ToString();
        if(encodedpassword == hdnPassword.Value.ToString())
        {
            string userid = Session["userid"].ToString();
            string strQueryEmailExist = "Select * from brand_user where email=@Email and roles<>'subscribe' and uid<>"+userid;
            DBCommand cmd = new DBCommand();
            cmd.CommandText = strQueryEmailExist;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@Email", userEmail);
            DataTable dt = dp.FillDataTable(cmd);
            if(dt.Rows.Count > 0)
            {
                msgUserSetting.Text = "Email already in use. Please enter another email.";
                trEmailEdit1.Style.Add("display", "table-row");
                trEmail.Style.Add("display", "none");
                trEmail1.Style.Add("display", "none");
            }
            else
            {
                utils.UpdateEmail(userid, userEmail);
                LoadAccountSettings();
                msgUserSetting.Text = "Email updated.";
                trEmailEdit1.Style.Add("display", "none");
                trEmail.Style.Add("display", "table-row");
                trEmail1.Style.Add("display", "table-row");
            }
        }
        else
        {
            msgUserSetting.Text = "Please enter correct password to update email.";
            trEmailEdit1.Style.Add("display", "table-row");
            trEmail.Style.Add("display", "none");
            trEmail1.Style.Add("display", "none");
        }
    }
    protected void btnChangePasword_Click(object sender, EventArgs e)
    {
        msgUserSetting.Text = "";
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        string encodedpassword = utils.EncryptPassword(txtcurrentPassword.Text.Trim());
        string newpassword = txtnewPassword.Text.ToString().Trim();
        string confirmpassword = txtconfirmPassword.Text.ToString().Trim();
        if (newpassword == confirmpassword)
        {
            if (encodedpassword == hdnPassword.Value.ToString())
            {
                string userid = Session["userid"].ToString();
                string encodednewpassword = utils.EncryptPassword(newpassword);
                utils.UpdatePassword(userid, encodednewpassword);
                LoadAccountSettings();
                msgUserSetting.Text = "Password updated.";
                trPasswordEdit.Style.Add("display", "none");
                trPassword.Style.Add("display", "table-row");
                trPassword1.Style.Add("display", "table-row");
            }
            else
            {
                msgUserSetting.Text = "Incorrect current password.";
                trPasswordEdit.Style.Add("display", "table-row");
                trPassword.Style.Add("display", "none");
                trPassword1.Style.Add("display", "none");
            }
        }
        else
        {
            msgUserSetting.Text = "New password and Re-enter new password does not match.";
            trPasswordEdit.Style.Add("display", "table-row");
            trPassword.Style.Add("display", "none");
            trPassword1.Style.Add("display", "none");
        }
    }
}