using System;
using System.Data;

public partial class CacheRemove : System.Web.UI.Page
{
    private static string[] OperatingSys = { "android", "windows", "macintosh", "linux", "ios", "other" };

    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Request.QueryString["type"]) == "widgetscache")
        {
            RemoveCacheKeyWidgetsTop(Convert.ToString(Request.QueryString["Cachekey1"]),true);
            RemoveCacheKeyWidgetsTop(Convert.ToString(Request.QueryString["Cachekey1"]),false);
            RemoveCacheKeyWidgetsNotTop(Convert.ToInt32(Convert.ToString(Request.QueryString["id"])), Convert.ToString(Request.QueryString["Cachekey3"]), Convert.ToString(Request.QueryString["position"]));
            RemoveCacheKeyWidgetsNotTop(Convert.ToInt32(Convert.ToString(Request.QueryString["id"])), Convert.ToString(Request.QueryString["Cachekey4"]), Convert.ToString(Request.QueryString["position"]));
        }
        if (Convert.ToString(Request.QueryString["type"]) == "rsssummary")
        {
            RemoveRssCache();
        }
        if (Convert.ToString(Request.QueryString["type"]) == "digipchannelplaylist")
        {
            if (Request.QueryString["ChannelID"] != null)
            {
                string strCacheKeyChannelDataPlaylist = "ChannelDataPlaylistDigip" + Convert.ToString(Request.QueryString["ChannelID"]);
                string strCacheKeyChannelPlaylist = "ChannelPlaylistDigip" + Convert.ToString(Request.QueryString["ChannelID"]);
                if (Cache[strCacheKeyChannelDataPlaylist] != null)
                {
                    Cache.Remove(strCacheKeyChannelDataPlaylist);
                }
                if (Cache[strCacheKeyChannelPlaylist] != null)
                {
                    Cache.Remove(strCacheKeyChannelPlaylist);
                }
            }
        }
        if (Convert.ToString(Request.QueryString["type"]) == "adnetworkscache")
        {
            RemoveCacheKeyAdsAll();
        }
        if (Convert.ToString(Request.QueryString["type"]) == "chkRecordData")
        {
            if (Cache["chkRecordADData"] != null)
            {
                Cache.Remove("chkRecordADData");
            }
            if (Cache["dtserverMaintainance"] != null)
            {
                Cache.Remove("dtserverMaintainance");
            }
            if (Cache["chkStratDomainReport"] != null)
            {
                Cache.Remove("chkStratDomainReport");
            }
        }
        if (Convert.ToString(Request.QueryString["type"]) == "dtserverMaintainance")
        {
            if (Cache["dtserverMaintainance"] != null)
            {
                Cache.Remove("dtserverMaintainance");
            }
            if (Cache["VideoImpression"] != null)
            {
                Cache.Remove("VideoImpression");
            }
            if (Cache["OutboundLink"] != null)
            {
                Cache.Remove("OutboundLink");
            }
            if (Cache["VideoAdStart"] != null)
            {
                Cache.Remove("VideoAdStart");
            }
            if (Cache["VideoAdFinish"] != null)
            {
                Cache.Remove("VideoAdFinish");
            }
            if (Cache["BannerClick"] != null)
            {
                Cache.Remove("BannerClick");
            }
            if (Cache["VideoAdClick"] != null)
            {
                Cache.Remove("VideoAdClick");
            }
        }
        if (Convert.ToString(Request.QueryString["type"]) == "BSWidgetChannelsCache")
        {
            RemoveWidgetChannelsCache(Convert.ToString(Request.QueryString["BSDomains"]));
        }
        if (Convert.ToString(Request.QueryString["type"]) == "widgetscacheofasitetop")
        {
            RemoveCacheKeyWidgetsTopOfASite(Convert.ToString(Request.QueryString["Cachekey1"]), Convert.ToString(Request.QueryString["domainurl"]),true);
            RemoveCacheKeyWidgetsTopOfASite(Convert.ToString(Request.QueryString["Cachekey1"]), Convert.ToString(Request.QueryString["domainurl"]),false);
        }
        if (Convert.ToString(Request.QueryString["type"]) == "widgetscacheofasitenottop")
        {
            RemoveCacheKeyWidgetsNotTopOfASite(Convert.ToString(Request.QueryString["Cachekey3"]), Convert.ToString(Request.QueryString["IDposition"]), Convert.ToString(Request.QueryString["domainurl"]));
            RemoveCacheKeyWidgetsNotTopOfASite(Convert.ToString(Request.QueryString["Cachekey4"]), Convert.ToString(Request.QueryString["IDposition"]), Convert.ToString(Request.QueryString["domainurl"]));
        }
        if (Convert.ToString(Request.QueryString["type"]) == "widgetscacheofasitetopsuperadmin")
        {
            RemoveCacheKeyWidgetsTopOfASiteSuperAdmin(Convert.ToString(Request.QueryString["Cachekey1"]), Convert.ToString(Request.QueryString["domainurl"]),true);
            RemoveCacheKeyWidgetsTopOfASiteSuperAdmin(Convert.ToString(Request.QueryString["Cachekey1"]), Convert.ToString(Request.QueryString["domainurl"]),false);
        }
        if (Convert.ToString(Request.QueryString["type"]) == "widgetscacheofasitenottopsuperadmin")
        {
            if (Convert.ToString(Request.QueryString["domainurl"]).ToLower() == "abc")
            {
                RemoveCacheKeyWidgetsNotTopOfASiteSuperAdmin1(Convert.ToString(Request.QueryString["Cachekey3"]), Convert.ToString(Request.QueryString["IDposition"]), Convert.ToString(Request.QueryString["domainurl"]));
                RemoveCacheKeyWidgetsNotTopOfASiteSuperAdmin1(Convert.ToString(Request.QueryString["Cachekey4"]), Convert.ToString(Request.QueryString["IDposition"]), Convert.ToString(Request.QueryString["domainurl"]));
            }
            else
            {
                RemoveCacheKeyWidgetsNotTopOfASiteSuperAdmin(Convert.ToString(Request.QueryString["Cachekey3"]), Convert.ToString(Request.QueryString["IDposition"]), Convert.ToString(Request.QueryString["domainurl"]));
                RemoveCacheKeyWidgetsNotTopOfASiteSuperAdmin(Convert.ToString(Request.QueryString["Cachekey4"]), Convert.ToString(Request.QueryString["IDposition"]), Convert.ToString(Request.QueryString["domainurl"]));
            }
        }
        if (Convert.ToString(Request.QueryString["type"]) == "articlecache")
        {
            RemoveArticlesCache(Convert.ToString(Request.QueryString["BSDomains"]));
        }
    }

    private void RemoveRssCache()
    {
        DBCommand cmddom = new DBCommand();
        cmddom.CommandText = "SP_Brand_Select_Domain_Name";
        cmddom.CommandType = CommandType.StoredProcedure;
        DataTable dtDomain = dp.FillDataTable(cmddom);
        foreach (DataRow drdomain in dtDomain.Rows)
        {
            string keycache = "rsssummary-" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim();
            if (Cache[keycache] != null)
            {
                Cache.Remove(keycache);
            }
        }
    }

    private void RemoveCacheKeyWidgetsTop(string keyname,bool Israndom)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);

        DBCommand cmddom = new DBCommand();
        cmddom.CommandText = "SP_Brand_Select_Domain_Name";
        cmddom.CommandType = CommandType.StoredProcedure;
        DataTable dtDomain = dp.FillDataTable(cmddom);
        foreach (DataRow drcoun in dtcountry.Rows)
        {
            foreach (DataRow drdomain in dtDomain.Rows)
            {
                foreach (string strOS in OperatingSys)
                {
                    string keycacheWidget1 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    string keycacheWidget2 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    string keycacheWidget3 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    string keycacheWidget4 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    string keycacheWidget5 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    string keycacheWidget6 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    string keycacheWidget7 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                    if (Cache[keycacheWidget1] != null)
                    {
                        Cache.Remove(keycacheWidget1);
                    }
                    if (Cache[keycacheWidget2] != null)
                    {
                        Cache.Remove(keycacheWidget2);
                    }
                    if (Cache[keycacheWidget3] != null)
                    {
                        Cache.Remove(keycacheWidget3);
                    }
                    if (Cache[keycacheWidget4] != null)
                    {
                        Cache.Remove(keycacheWidget4);
                    }
                    if (Cache[keycacheWidget5] != null)
                    {
                        Cache.Remove(keycacheWidget5);
                    }
                    if (Cache[keycacheWidget6] != null)
                    {
                        Cache.Remove(keycacheWidget6);
                    }
                    if (Cache[keycacheWidget7] != null)
                    {
                        Cache.Remove(keycacheWidget7);
                    }
                }
            }
        }
        foreach (DataRow drdomain in dtDomain.Rows)
        {
            string keycacheWidget8 = "SortBlogPosts" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim();
            if (Cache[keycacheWidget8] != null)
            {
                Cache.Remove(keycacheWidget8);
            }
            string keycacheWidget9 = "SortBlogPostsHome" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim();
            if (Cache[keycacheWidget9] != null)
            {
                Cache.Remove(keycacheWidget9);
            }
        }
    }
    
    private void RemoveCacheKeyWidgetsNotTop(int id, string keyname, string position)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);

        DBCommand cmddom = new DBCommand();
        cmddom.CommandText = "SP_Brand_Select_Domain_Name";
        cmddom.CommandType = CommandType.StoredProcedure;
        DataTable dtDomain = dp.FillDataTable(cmddom);
        foreach (DataRow drcoun in dtcountry.Rows)
        {
            foreach (DataRow drdomain in dtDomain.Rows)
            {
                foreach (string strOS in OperatingSys)
                {
                    string keycacheWidget1 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;
                    string keycacheWidget2 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;
                    string keycacheWidget3 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;
                    string keycacheWidget4 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;
                    string keycacheWidget5 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;
                    string keycacheWidget6 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;
                    string keycacheWidget7 = keyname + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + id + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + position.Trim() + strOS;

                    if (Cache[keycacheWidget1] != null)
                    {
                        Cache.Remove(keycacheWidget1);
                    }
                    if (Cache[keycacheWidget2] != null)
                    {
                        Cache.Remove(keycacheWidget2);
                    }
                    if (Cache[keycacheWidget3] != null)
                    {
                        Cache.Remove(keycacheWidget3);
                    }
                    if (Cache[keycacheWidget4] != null)
                    {
                        Cache.Remove(keycacheWidget4);
                    }
                    if (Cache[keycacheWidget5] != null)
                    {
                        Cache.Remove(keycacheWidget5);
                    }
                    if (Cache[keycacheWidget6] != null)
                    {
                        Cache.Remove(keycacheWidget6);
                    }
                    if (Cache[keycacheWidget7] != null)
                    {
                        Cache.Remove(keycacheWidget7);
                    }
                }
            }
        }

        foreach (DataRow drdomain in dtDomain.Rows)
        {
            string keycacheWidget8 = "SortBlogPosts" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim();
            if (Cache[keycacheWidget8] != null)
            {
                Cache.Remove(keycacheWidget8);
            }
            string keycacheWidget9 = "SortBlogPostsHome" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim();
            if (Cache[keycacheWidget9] != null)
            {
                Cache.Remove(keycacheWidget9);
            }
        }
    }
    
    private void RemoveCacheKeyAdsAll()
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);

        DBCommand cmddom = new DBCommand();
        cmddom.CommandText = "SP_Brand_Select_Domain_Name";
        cmddom.CommandType = CommandType.StoredProcedure;
        DataTable dtDomain = dp.FillDataTable(cmddom);
        foreach (DataRow drcoun in dtcountry.Rows)
        {
            foreach (DataRow drdomain in dtDomain.Rows)
            {
                foreach (string strOS in OperatingSys)
                {
                    string keypageload1 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keypageload2 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keypageload3 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keypageload4 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keypageload5 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keynotpageload1 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keynotpageload2 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keynotpageload3 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keynotpageload4 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    string keynotpageload5 = "AllNetworks" + Convert.ToString(drdomain["DomainName"]).ToLower().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + strOS;
                    if (Cache[keypageload1] != null)
                    {
                        Cache.Remove(keypageload1);
                    }
                    if (Cache[keypageload2] != null)
                    {
                        Cache.Remove(keypageload2);
                    }
                    if (Cache[keypageload3] != null)
                    {
                        Cache.Remove(keypageload3);
                    }
                    if (Cache[keypageload4] != null)
                    {
                        Cache.Remove(keypageload4);
                    }
                    if (Cache[keypageload5] != null)
                    {
                        Cache.Remove(keypageload5);
                    }
                    if (Cache[keynotpageload1] != null)
                    {
                        Cache.Remove(keynotpageload1);
                    }
                    if (Cache[keynotpageload2] != null)
                    {
                        Cache.Remove(keynotpageload2);
                    }
                    if (Cache[keynotpageload3] != null)
                    {
                        Cache.Remove(keynotpageload3);
                    }
                    if (Cache[keynotpageload4] != null)
                    {
                        Cache.Remove(keynotpageload4);
                    }
                    if (Cache[keynotpageload5] != null)
                    {
                        Cache.Remove(keynotpageload5);
                    }
                }
            }
        }
    }
    private void RemoveWidgetChannelsCache(string BSDomains)
    {
        string[] BSDomainsArray = BSDomains.Split(',');
        if (BSDomainsArray.Length > 0)
        {
            for (int i = 0; i < BSDomainsArray.Length; i++)
            {
                string domainurl = Convert.ToString(BSDomainsArray[i]);
                string keyWidgetAdrotator = "WidgetAdRotator" + Convert.ToString(domainurl).Trim();
                if (Cache[keyWidgetAdrotator] != null)
                {
                    Cache.Remove(keyWidgetAdrotator);
                }
            }
        }
    }
    
    private void RemoveCacheKeyWidgetsTopOfASite(string keyname, string site,bool Israndom)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);
        foreach (DataRow drcoun in dtcountry.Rows)
        {
            foreach (string strOS in OperatingSys)
            {
                string keycacheWidget1 = keyname + site.ToLower().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                string keycacheWidget2 = keyname + site.ToLower().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                string keycacheWidget3 = keyname + site.ToLower().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                string keycacheWidget4 = keyname + site.ToLower().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                string keycacheWidget5 = keyname + site.ToLower().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                string keycacheWidget6 = keyname + site.ToLower().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                string keycacheWidget7 = keyname + site.ToLower().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                if (Cache[keycacheWidget1] != null)
                {
                    Cache.Remove(keycacheWidget1);
                }
                if (Cache[keycacheWidget2] != null)
                {
                    Cache.Remove(keycacheWidget2);
                }
                if (Cache[keycacheWidget3] != null)
                {
                    Cache.Remove(keycacheWidget3);
                }
                if (Cache[keycacheWidget4] != null)
                {
                    Cache.Remove(keycacheWidget4);
                }
                if (Cache[keycacheWidget5] != null)
                {
                    Cache.Remove(keycacheWidget5);
                }
                if (Cache[keycacheWidget6] != null)
                {
                    Cache.Remove(keycacheWidget6);
                }
                if (Cache[keycacheWidget7] != null)
                {
                    Cache.Remove(keycacheWidget7);
                }
            }
        }
        string keycacheWidget8 = "SortBlogPosts" + site.ToLower().Trim();
        if (Cache[keycacheWidget8] != null)
        {
            Cache.Remove(keycacheWidget8);
        }
        string keycacheWidget9 = "SortBlogPostsHome" + site.ToLower().Trim();
        if (Cache[keycacheWidget9] != null)
        {
            Cache.Remove(keycacheWidget9);
        }
    }
    
    private void RemoveCacheKeyWidgetsNotTopOfASite(string keyname, string Id_position, string site)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);

        DBCommand cmdidposition = new DBCommand();
        cmdidposition.CommandText = "select a.id,b.Position from Brand_AppLists AS a inner join Brand_AppListRuleSettings as b on b.AppList_ID=a.id";
        cmdidposition.CommandType = CommandType.Text;
        DataTable dtidposition = dp.FillDataTable(cmdidposition);

        if (dtidposition.Rows.Count > 0)
        {
            foreach (DataRow dridposition in dtidposition.Rows)
            {
                foreach (DataRow drcoun in dtcountry.Rows)
                {
                    foreach (string strOS in OperatingSys)
                    {
                        string keycacheWidget1 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        string keycacheWidget2 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        string keycacheWidget3 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        string keycacheWidget4 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        string keycacheWidget5 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        string keycacheWidget6 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        string keycacheWidget7 = keyname + site.ToLower().Trim() + dridposition["id"].ToString().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                        if (Cache[keycacheWidget1] != null)
                        {
                            Cache.Remove(keycacheWidget1);
                        }
                        if (Cache[keycacheWidget2] != null)
                        {
                            Cache.Remove(keycacheWidget2);
                        }
                        if (Cache[keycacheWidget3] != null)
                        {
                            Cache.Remove(keycacheWidget3);
                        }
                        if (Cache[keycacheWidget4] != null)
                        {
                            Cache.Remove(keycacheWidget4);
                        }
                        if (Cache[keycacheWidget5] != null)
                        {
                            Cache.Remove(keycacheWidget5);
                        }
                        if (Cache[keycacheWidget6] != null)
                        {
                            Cache.Remove(keycacheWidget6);
                        }
                        if (Cache[keycacheWidget7] != null)
                        {
                            Cache.Remove(keycacheWidget7);
                        }
                    }
                }
            }
        }

        string keycacheWidget8 = "SortBlogPosts" + site.ToLower().Trim();
        if (Cache[keycacheWidget8] != null)
        {
            Cache.Remove(keycacheWidget8);
        }
        string keycacheWidget9 = "SortBlogPostsHome" + site.ToLower().Trim();
        if (Cache[keycacheWidget9] != null)
        {
            Cache.Remove(keycacheWidget9);
        }
    }
    
    private void RemoveCacheKeyWidgetsTopOfASiteSuperAdmin(string keyname, string site,bool Israndom)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);
        string[] ArrarySite = site.Split(',');
        if (ArrarySite.Length > 0)
        {
            for (int i = 0; i < ArrarySite.Length; i++)
            {
                foreach (DataRow drcoun in dtcountry.Rows)
                {
                    foreach (string strOS in OperatingSys)
                    {
                        string keycacheWidget1 = keyname + ArrarySite[i].ToLower().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        string keycacheWidget2 = keyname + ArrarySite[i].ToLower().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        string keycacheWidget3 = keyname + ArrarySite[i].ToLower().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        string keycacheWidget4 = keyname + ArrarySite[i].ToLower().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        string keycacheWidget5 = keyname + ArrarySite[i].ToLower().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        string keycacheWidget6 = keyname + ArrarySite[i].ToLower().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        string keycacheWidget7 = keyname + ArrarySite[i].ToLower().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + Israndom + strOS;
                        if (Cache[keycacheWidget1] != null)
                        {
                            Cache.Remove(keycacheWidget1);
                        }
                        if (Cache[keycacheWidget2] != null)
                        {
                            Cache.Remove(keycacheWidget2);
                        }
                        if (Cache[keycacheWidget3] != null)
                        {
                            Cache.Remove(keycacheWidget3);
                        }
                        if (Cache[keycacheWidget4] != null)
                        {
                            Cache.Remove(keycacheWidget4);
                        }
                        if (Cache[keycacheWidget5] != null)
                        {
                            Cache.Remove(keycacheWidget5);
                        }
                        if (Cache[keycacheWidget6] != null)
                        {
                            Cache.Remove(keycacheWidget6);
                        }
                        if (Cache[keycacheWidget7] != null)
                        {
                            Cache.Remove(keycacheWidget7);
                        }
                    }
                }
            }
        }
        string keycacheWidget8 = "SortBlogPosts" + site.ToLower().Trim();
        if (Cache[keycacheWidget8] != null)
        {
            Cache.Remove(keycacheWidget8);
        }
        string keycacheWidget9 = "SortBlogPostsHome" + site.ToLower().Trim();
        if (Cache[keycacheWidget9] != null)
        {
            Cache.Remove(keycacheWidget9);
        }
    }
    
    private void RemoveCacheKeyWidgetsNotTopOfASiteSuperAdmin(string keyname, string Id_position, string site)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);

        DBCommand cmdidposition = new DBCommand();
        cmdidposition.CommandText = "select a.id,b.Position from Brand_AppLists AS a inner join Brand_AppListRuleSettings as b on b.AppList_ID=a.id";
        cmdidposition.CommandType = CommandType.Text;
        DataTable dtidposition = dp.FillDataTable(cmdidposition);

        string[] ArrarySite = site.Split(',');

        if (ArrarySite.Length > 0)
        {
            for (int j = 0; j < ArrarySite.Length; j++)
            {
                if (dtidposition.Rows.Count > 0)
                {
                    foreach (DataRow dridposition in dtidposition.Rows)
                    {
                        foreach (DataRow drcoun in dtcountry.Rows)
                        {
                            foreach (string strOS in OperatingSys)
                            {
                                string keycacheWidget1 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget2 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget3 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget4 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget5 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget6 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget7 = keyname + ArrarySite[j].ToLower().Trim() + dridposition["id"].ToString().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                if (Cache[keycacheWidget1] != null)
                                {
                                    Cache.Remove(keycacheWidget1);
                                }
                                if (Cache[keycacheWidget2] != null)
                                {
                                    Cache.Remove(keycacheWidget2);
                                }
                                if (Cache[keycacheWidget3] != null)
                                {
                                    Cache.Remove(keycacheWidget3);
                                }
                                if (Cache[keycacheWidget4] != null)
                                {
                                    Cache.Remove(keycacheWidget4);
                                }
                                if (Cache[keycacheWidget5] != null)
                                {
                                    Cache.Remove(keycacheWidget5);
                                }
                                if (Cache[keycacheWidget6] != null)
                                {
                                    Cache.Remove(keycacheWidget6);
                                }
                                if (Cache[keycacheWidget7] != null)
                                {
                                    Cache.Remove(keycacheWidget7);
                                }
                            }
                        }
                    }
                }

                string keycacheWidget8 = "SortBlogPosts" + ArrarySite[j].ToLower().Trim();
                if (Cache[keycacheWidget8] != null)
                {
                    Cache.Remove(keycacheWidget8);
                }
                string keycacheWidget9 = "SortBlogPostsHome" + ArrarySite[j].ToLower().Trim();
                if (Cache[keycacheWidget9] != null)
                {
                    Cache.Remove(keycacheWidget9);
                }
            }
        }
    }
    
    private void RemoveCacheKeyWidgetsNotTopOfASiteSuperAdmin1(string keyname, string Id_position, string site)
    {
        DBCommand cmdcoun = new DBCommand();
        cmdcoun.CommandText = "SP_Brand_Select_CounryIds_All";
        cmdcoun.CommandType = CommandType.StoredProcedure;
        DataTable dtcountry = dp.FillDataTable(cmdcoun);

        DBCommand cmdidposition = new DBCommand();
        cmdidposition.CommandText = "select a.id,b.Position from Brand_AppLists AS a inner join Brand_AppListRuleSettings as b on b.AppList_ID=a.id";
        cmdidposition.CommandType = CommandType.Text;
        DataTable dtidposition = dp.FillDataTable(cmdidposition);

        DBCommand cmddom = new DBCommand();
        cmddom.CommandText = "SP_Brand_Select_Domain_Name";
        cmddom.CommandType = CommandType.StoredProcedure;
        DataTable dtDomain = dp.FillDataTable(cmddom);

        if (dtDomain.Rows.Count > 0)
        {
            foreach (DataRow drdomain in dtDomain.Rows)
            {
                if (dtidposition.Rows.Count > 0)
                {
                    foreach (DataRow dridposition in dtidposition.Rows)
                    {
                        foreach (DataRow drcoun in dtcountry.Rows)
                        {
                            foreach (string strOS in OperatingSys)
                            {
                                string keycacheWidget1 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "Chrome" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget2 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "IE" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget3 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "Firefox" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget4 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "Opera" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget5 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "Safari" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget6 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "iphone" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                string keycacheWidget7 = keyname + drdomain["DomainName"].ToString().ToLower().Trim() + dridposition["id"].ToString().Trim() + "ipad" + Convert.ToString(drcoun["country_id"]).Trim() + dridposition["Position"].ToString().Trim() + strOS;
                                if (Cache[keycacheWidget1] != null)
                                {
                                    Cache.Remove(keycacheWidget1);
                                }
                                if (Cache[keycacheWidget2] != null)
                                {
                                    Cache.Remove(keycacheWidget2);
                                }
                                if (Cache[keycacheWidget3] != null)
                                {
                                    Cache.Remove(keycacheWidget3);
                                }
                                if (Cache[keycacheWidget4] != null)
                                {
                                    Cache.Remove(keycacheWidget4);
                                }
                                if (Cache[keycacheWidget5] != null)
                                {
                                    Cache.Remove(keycacheWidget5);
                                }
                                if (Cache[keycacheWidget6] != null)
                                {
                                    Cache.Remove(keycacheWidget6);
                                }
                                if (Cache[keycacheWidget7] != null)
                                {
                                    Cache.Remove(keycacheWidget7);
                                }
                            }
                        }
                    }
                }
                string keycacheWidget8 = "SortBlogPosts" + drdomain["DomainName"].ToString().ToLower().Trim();
                if (Cache[keycacheWidget8] != null)
                {
                    Cache.Remove(keycacheWidget8);
                }
                string keycacheWidget9 = "SortBlogPostsHome" + drdomain["DomainName"].ToString().ToLower().Trim();
                if (Cache[keycacheWidget9] != null)
                {
                    Cache.Remove(keycacheWidget9);
                }
            }
        }
    }
    private void RemoveArticlesCache(string BSDomains)
    {
        string[] BSDomainsArray = BSDomains.Split(',');
        if (BSDomainsArray.Length > 0)
        {
            for (int i = 0; i < BSDomainsArray.Length; i++)
            {
                string domainurl = Convert.ToString(BSDomainsArray[i]).Trim();

                string KeySortBlogPosts = "SortBlogPosts" + domainurl.ToLower();
                if (Cache[KeySortBlogPosts] != null)
                    Cache.Remove(KeySortBlogPosts);
                string keycacheWidget9 = "SortBlogPostsHome" + domainurl.ToLower();
                if (Cache[keycacheWidget9] != null)
                {
                    Cache.Remove(keycacheWidget9);
                }
            }
        }
    }
}
