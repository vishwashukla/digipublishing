<%@ Page Language="C#" MasterPageFile="~/brandtemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
        <asp:View ID="vwComment" runat="server">        
            <asp:Label ID="lblMSg" runat="server" CssClass="messages"></asp:Label>
            <table>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblUsername" Text="Name"></asp:Label>&nbsp;
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtUsername"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtUsername" runat="server" ID="rqfUsernameMsg" 
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="VGCUS"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblEmail" Text="Email"></asp:Label>&nbsp;
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqfEmailMsg" ControlToValidate="txtEmail" runat="server"
                            ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="VGCUS"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ControlToValidate="txtEmail" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                            ID="regEmail" runat="server" ErrorMessage="Invalid email address" ValidationGroup="VGCUS"></asp:RegularExpressionValidator></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblComment" Text="Comment"></asp:Label>&nbsp;
                    </td>
                    <td>
                        <asp:TextBox runat="server" Width="500px" Height="150px" ID="txtComment" TextMode="MultiLine"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtComment" ID="rqfCommnetMsg"
                            runat="server" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="VGCUS"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <img src="captcha.aspx" id="imgcaptcha" alt="captcha" /><br />
                        <asp:TextBox ID="txtCaptcha" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCaptcha" ControlToValidate="txtCaptcha"
                            runat="server" Display="dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="VGCUS"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSend" Text="Submit" runat="server" OnClick="btnSend_Click" ValidationGroup="VGCUS"/></td>
                    <td>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vwMessage" runat="server">
            <p>
                Comments have been sent to the site owner.</p>
        </asp:View>
    </asp:MultiView>
</asp:Content>