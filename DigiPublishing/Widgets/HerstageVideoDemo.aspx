<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HerstageVideoDemo.aspx.cs" Inherits="HerstageVideoDemo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" src="/JS/jquery.3.1.1.min.js"></script>
    <script type="text/javascript" src="/Widgets/JS/ima3.js"></script>
    <style type="text/css">
        #contentElement,.videoContainer {
            float:left;
        }

        #playlist_container {
            float:left;
            width: 180px;
            height: 443px;
            overflow: auto;
            padding-left: 1px;
            padding-top: 1px;
            padding-bottom: 1px;
            background-color: #000000;
            text-align:left;
            overflow:hidden;
        }

        .vids {
            border-bottom: 1px solid #555;
            float: left;
            margin: 0;
            padding: 5px;
            width: 175px;
            color: #ffffff;
        }

        .vids:hover {
            background: #ffffff;
            color: #000000;
        }

        .title {
            font-weight: bold;
            text-decoration:underline;
        }

        .src {
            font-weight: normal;
            font-size:13px;
        }

        video::-internal-media-controls-download-button {
            display:none;
        }

        video::-internal-media-controls-pause-button {
            background-color:red;
        }

        video::-webkit-media-controls-enclosure {
            overflow:hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px); /* Adjust as needed */
            background-color: #000000;
            color: #ffffff;
        }

        video::-webkit-media-controls-current-time-display,video::-webkit-media-controls-time-remaining-display
        {
            color: #ffffff;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <center>
    <div style="width: 721px;height:450px;">
        <div id="mainContainer">
            <div id="wrapper">
            </div>
            <div id="adContainer"></div>
        </div>
    </div>
    <div id="events" style="width: 719px;">
	    <div id="aderror" style="width:719px;height:30px;text-align:center;border:solid 1px black;">
	        <b>Ad Error</b>
	    </div>
	    <div id="impression" style="width:719px;height:30px;text-align:center;border:solid 1px black;border-top-width:0px">
	        <b>Impression</b>
	    </div>
	    <div id="adstart" style="width:719px;height:30px;text-align:center;border:solid 1px black;border-top-width:0px;">
	        <b>Ad Start</b>
	    </div>
	    <div id="adend" style="width:719px;height:30px;text-align:center;border:solid 1px black;border-top-width:0px;">
	        <b>Ad Finish</b>
	    </div>
	    <div id="adclick" style="width:719px;height:30px;text-align:center;border:solid 1px black;border-top-width:0px;">
	        <b>Ad Click</b>
	    </div>
	</div>
    </center>
        <script type="text/javascript">
        var tag = '<%=Tag %>';
        var VideoIDCurrent = "";
        var VideoNameCurrent = "";
        var VideoDescCurrent = "";
        var url = escape(window.top.location.href);
        var currentindex = 0;
        var playListLength = 0;
        var playlistitemindex = 0;
        var scrollapi,scrollelement,videoContent,xmlDocPlaylist;
        var strlightcolor = "#ffffff";
        var adDisplayContainer, adsLoader, contentEndedListener, adsManager;
        var filePath = "";

        function UpdateColors() {
            $(".currentvideo").css("background-color", strlightcolor).css("color", "#000000");
            $(".currentvideo").mouseenter(function () {
                $(this).css("background-color", strlightcolor).css("color", "#000000");
            }).mouseleave(function () {
                $(this).css("background-color", "#000000").css("color", strlightcolor);
            });

            if (!$(".vids").hasClass("currentvideo"))
                $(".vids").css("background-color", "#000000").css("color", "#ffffff");
            $(".vids").mouseenter(function () {
                $(this).css("background-color", strlightcolor).css("color", "#000000");
            }).mouseleave(function () {
                if (!$(this).hasClass("currentvideo"))
                    $(this).css("background-color", "#000000").css("color", "#ffffff");
            });
        }
        $(document).ready(UpdateColors);

        function CallService() {
            var stringURL = document.location.href.split("http://")[1].split("/")[0];
            //var streamURLtest = "http://" + stringURL + "/widgets/TestPlaylist.xml";
            var streamURLtest = "http://" + stringURL + "/widgets/GetPlaylist.aspx?id=23";
            var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    xmlDocPlaylist = xhttp.responseXML;
                    playListLength = parseInt(xmlDocPlaylist.getElementsByTagName("track").length);
                }
            }
            xhttp.open("GET", streamURLtest, false);
            xhttp.send();

            playlistitemindex = 0;
            AddPlayList();
            videoContent = document.getElementById('contentElement');
            PlayWidget();
        }

        function AddPlayList() {
            var track = xmlDocPlaylist.getElementsByTagName("item");
            var startindex = playlistitemindex;
            var strvideo = "<div class='videoContainer'>";
            for (var i = 0; i < track.length; i++) {
                if (i >= startindex) {
                    playlistitemindex++;
                    var tracktitle = track[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
                    var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
                    if (tracklocation.indexOf(".mp4") > 0) {
                        var strinnerhtml = "<video style='background:#000' id='contentElement' width='540px' ";
                        strinnerhtml += "height='445px' controls autoplay onended='LoadNextPlayListItem()' muted='muted'><source id='mp4src' src='" + tracklocation + "' type='video/mp4'></source></video>";
                        strvideo += strinnerhtml + "</div>";
                        break;
                    }
                }
            }
            strvideo += "<div id=\"playlist_container\">"
            for (var i = 0; i < track.length; i++) {
                var tracktitle = track[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
                var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
                var trackdesc = "";
                if (track[i].getElementsByTagName("description")[0].childNodes.length > 0) {
                    trackdesc = track[i].getElementsByTagName("description")[0].childNodes[0].nodeValue;
                }
                if (tracklocation.indexOf(".mp4") > 0) {
                    strvideo += "<div onmouseover=\"this.style.cursor='pointer'\" onclick=\"LoadSelectedPlayListItem(" + i + ");\""
                        + " class=\"vids\" id=\"vids_" + i + "\"><div class=\"ch_info\"><span class=\"title\">" + tracktitle + "</span><br />"
                        + "<span class=\"src\">" + trackdesc + "</span></div></div>";
                }
            }

            strvideo += "</div>"
            document.getElementById('wrapper').innerHTML = strvideo;
            $('#vids_' + (playlistitemindex - 1)).addClass("currentvideo");
        }

        function LoadNextPlayListItem() {
            LoadSelectedPlayListItem(playlistitemindex);
        }

        function LoadSelectedPlayListItem(index) {
            cleardivcolor();
            currentindex = playlistitemindex - 1;
            playlistitemindex = index;
            if (playListLength == playlistitemindex) {
                playlistitemindex = 0;
            }
            playItem();
            PlayWidget();
        }

        function playItem() {
            videoContent = document.getElementById('contentElement');
            var srcelement = document.getElementById('mp4src');
            videoContent.pause();
            var track = xmlDocPlaylist.getElementsByTagName("track");
            var startindex = playlistitemindex;
            for (var i = 0; i < track.length; i++) {
                if (i >= startindex) {
                    playlistitemindex++;
                    var tracklocation = track[index].getElementsByTagName("enclosure")[0].getAttribute("url");
                    srcelement.src = tracklocation;
                    break;
                }
            }
            videoContent.load();
            $('#vids_' + currentindex).removeClass("currentvideo");
            $('#vids_' + currentindex).css("background-color", "#000000").css("color", "#ffffff");
            $('#vids_' + (playlistitemindex - 1)).addClass("currentvideo");
            UpdateColors();
        }

        function PlayWidget() {
            SetFirstItemPath(playlistitemindex);
            var NewTimeGuid = "";
            for (var i = 0; i < 8 ; i++)
                NewTimeGuid += Math.floor((Math.random() * 10));
            tag = tag.replace("[page_url]", url).replace("[PAGE_URL]", url);
            tag = tag.replace("[timestamp]", NewTimeGuid).replace("[TIMESTAMP]", NewTimeGuid).replace("[random]", NewTimeGuid).replace("[RANDOM]", NewTimeGuid);
            tag = tag.replace("[video_id]", VideoIDCurrent).replace("[VIDEO_ID]", VideoIDCurrent);
            tag = tag.replace("[video_title]", VideoNameCurrent).replace("[VIDEO_TITLE]", VideoNameCurrent);
            tag = tag.replace("[video_desc]", VideoDescCurrent).replace("[VIDEO_DESC]", VideoDescCurrent);
            tag = tag.replace("[video_file_url]", encodeURIComponent(filePath)).replace("[VIDEO_FILE_URL]", encodeURIComponent(filePath));
            tag = tag.replace("[w]", "750");
            tag = tag.replace("[h]", "445");
            tag = tag.replace(/^\s+|\s+$/g, '');

            adDisplayContainer = new google.ima.AdDisplayContainer(
            document.getElementById('adContainer'), videoContent);
            // Must be done as the result of a user action on mobile
            adDisplayContainer.initialize();

            adsLoader = new google.ima.AdsLoader(adDisplayContainer);

            // Add event listeners
            adsLoader.addEventListener(
                google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
                onAdsManagerLoaded,
                false);
            adsLoader.addEventListener(
                google.ima.AdErrorEvent.Type.AD_ERROR,
                onAdError,
                false);

            // An event listener to tell the SDK that our content video
            // is completed so the SDK can play any post-roll ads.
            contentEndedListener = function () { adsLoader.contentComplete(); LoadSelectedPlayListItem(playlistitemindex); };
            videoContent.onended = contentEndedListener;
            // Request video ads.
            var adsRequest = new google.ima.AdsRequest();
            if (tag.indexOf("http://") == -1 && tag.indexOf("https://") == -1) {
                var stringURL = document.location.href.split("http://")[1].split("/")[0];
                if (tag.startsWith("/")) {
                    adsRequest.adTagUrl = "http://" + stringURL + tag;
                }
                else {
                    adsRequest.adTagUrl = "http://" + stringURL + "/" + tag;
                }
            }
            else {
                adsRequest.adTagUrl = tag;
            }

            // Specify the linear and nonlinear slot sizes. This helps the SDK to
            // select the correct creative if multiple are returned.
            adsRequest.linearAdSlotWidth = 640;
            adsRequest.linearAdSlotHeight = 400;
            adsRequest.nonLinearAdSlotWidth = 640;
            adsRequest.nonLinearAdSlotHeight = 150;
            adsLoader.requestAds(adsRequest);
        }

        function onAdError(adErrorEvent) {
            // Handle the error logging and destroy the AdsManager
            console.log(adErrorEvent.getError());
            document.getElementById("aderror").style.backgroundColor = "green";
            if (adsManager != null)
                adsManager.destroy();
            videoContent.play();
        }

        function onAdsManagerLoaded(adsManagerLoadedEvent) {
            // Get the ads manager.
            adsManager = adsManagerLoadedEvent.getAdsManager(videoContent);  // See API reference for contentPlayback

            // Add listeners to the required events.
            adsManager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, onAdError);
            adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, onContentPauseRequested);
            adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, onContentResumeRequested);
            adsManager.addEventListener(google.ima.AdEvent.Type.COMPLETE, function () {
                adDisplayContainer.destroy();
                document.getElementById("adend").style.backgroundColor = "green";
                videoContent.play();
            });

            adsManager.addEventListener(google.ima.AdEvent.Type.LOADED, function () {
                document.getElementById("impression").style.backgroundColor = "green";
            });
            adsManager.addEventListener(google.ima.AdEvent.Type.STARTED, function () {
                document.getElementById("adstart").style.backgroundColor = "green";
            });
            adsManager.addEventListener(google.ima.AdEvent.Type.CLICK, function () {
                document.getElementById("adclick").style.backgroundColor = "green";
                adsManager.resume();
            });


            try {
                // Initialize the ads manager. Ad rules playlist will start at this time.
                adsManager.init(721, 445, google.ima.ViewMode.NORMAL);
                // Call start to show ads. Single video and overlay ads will
                // start at this time; this call will be ignored for ad rules, as ad rules
                // ads start when the adsManager is initialized.
                adsManager.start();
            } catch (adError) {
                // An error may be thrown if there was a problem with the VAST response.
            }
        }

        function onContentPauseRequested() {
            // This function is where you should setup UI for showing ads (e.g.
            // display ad timer countdown, disable seeking, etc.)
            videoContent.removeEventListener('ended', contentEndedListener);
            videoContent.pause();
        }

        function onContentResumeRequested() {
            // This function is where you should ensure that your UI is ready
            // to play content.
            videoContent.addEventListener('ended', contentEndedListener);
            videoContent.play();
        }

        function SetFirstItemPath(index)
        {
            var track = xmlDocPlaylist.getElementsByTagName("track");
            if (track.length > 0 && track.length > index) {
                filePath = track[index].getElementsByTagName("location")[0].childNodes[0].nodeValue;
                VideoIDCurrent = track[index].getElementsByTagName("id")[0].childNodes[0].nodeValue;
                VideoNameCurrent = track[index].getElementsByTagName("title")[0].childNodes[0].nodeValue;
                VideoDescCurrent = track[index].getElementsByTagName("annotation")[0].childNodes[0].nodeValue;
            }
        }

        function cleardivcolor()
        {
            document.getElementById("aderror").style.backgroundColor = "white";
            document.getElementById("impression").style.backgroundColor = "white";
            document.getElementById("adstart").style.backgroundColor = "white";
            document.getElementById("adend").style.backgroundColor = "white";
            document.getElementById("adclick").style.backgroundColor = "white";
        }
        CallService();
    </script>
    </form>
</body>
</html>
