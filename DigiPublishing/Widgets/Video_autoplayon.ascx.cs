using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class autoplayon_ascx : System.Web.UI.UserControl
{
    DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string strImageUrl = "";
    public string IsMobileCS = "";
    public string strChannelFormat = string.Empty;
    public string strChannelName = string.Empty;
    public string strChannelID = string.Empty;
    public string strChannelIDNext = string.Empty;
    public string strFLVPATH = "";
    public string MyFileURL = "";
    public string isShowPlayList = "";
    public string AllAdsString = "";
    public string lbldesc = "";
    public string strlightcolor = "#ff0000";
         
    protected void Page_Load(object sender, EventArgs e)
    {
        string TVSiteCDNurl = ConfigLoader.GetTVSiteCDNurl();
        HtmlControl top_ad_box = (HtmlControl)this.Parent.FindControl("top_ad_box5");
        if (top_ad_box.Visible == true)
        {
            DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
            string domain = ReadConfig.getDomainUrl();

            if (domain == ConfigLoader.GetSAMainURL())
            {
                strImageUrl = "../images_common/vtv-loading.gif";
            }
            else
            {
                if (domain.ToLower().Contains("demo.") || domain.ToLower().Contains("demoopen.") || domain.ToLower().Contains("democlosed.") || domain.ToLower().Contains(ConfigLoader.GetTestDemoSite()))
                {
                    if (ConfigLoader.GetDemoSitesAniGif() != "")
                        strImageUrl = ConfigLoader.GetImagesCDNurl() + ConfigLoader.GetDemoSitesAniGif();
                    else
                        strImageUrl = ConfigLoader.GetImagesCDNurl() + domain + ".gif";
                }
                else
                {
                    strImageUrl = ConfigLoader.GetImagesCDNurl() + domain + ".gif";
                }
            }
            IsMobileCS = Convert.ToString(utils.isMobile());
            if (!IsPostBack)
            {
                Session.Remove("DTPlayList");
            }
            //remove next line if gif image going to be used with player
            strImageUrl = "";
            GetChannel();

            var strdomainhost = domain.ToLower().Replace("www.", "").Trim();
            if (strdomainhost == "herstage.com")
                strlightcolor = "#FF7AE5";
            if (strdomainhost == "asiannewsfeed.com")
                strlightcolor = "#15A1A1";
            if (strdomainhost == "australiahop.com")
                strlightcolor = "#F5F5F5";
            if (strdomainhost == "chinalucky8.com")
                strlightcolor = "#FFFFFF";
            if (strdomainhost == "hongkongpan.com")
                strlightcolor = "#F6E8CD";
            if (strdomainhost == "japanhai.com")
                strlightcolor = "#BB141A";
            if (strdomainhost == "korea7s.com")
                strlightcolor = "#18459A";
            if (strdomainhost == "singaporeanbiz.com")
                strlightcolor = "#ECEEE9";
            if (strdomainhost == "singaporefriendly.com")
                strlightcolor = "#9A9B9D";
            if (strdomainhost == "singaporemusicguide.com")
                strlightcolor = "#6A2B80";
            if (strdomainhost == "singaporenewsblog.com")
                strlightcolor = "#B85FA5";
            if (strdomainhost == "oneurope.info")
                strlightcolor = "#E28424";
            if (strdomainhost == "usaevent.net")
                strlightcolor = "#E7258B";
            if (strdomainhost == "hub-nz.com")
                strlightcolor = "#2D58A3";
            if (strdomainhost == "indo-beat.com")
                strlightcolor = "#ED1C24";
            if (strdomainhost == "y-thai.net")
                strlightcolor = "#87C440";
            if (strdomainhost == "imanila.net")
                strlightcolor = "#1A6BB5";
            if (strdomainhost == "visitviet.com")
                strlightcolor = "#3A1715";
            if (strdomainhost == "gomalaysia.info")
                strlightcolor = "#B32D33";
            if (strdomainhost == "skylabs.info")
                strlightcolor = "#007F97";
            if (strdomainhost == "whatbiz.co")
                strlightcolor = "#107FB9";
        }
    }

    private void GetChannel()
    {
        var strSql = "SELECT id FROM brand_domainname "
                       + " where status <> 2 AND domainname<>'" + ConfigLoader.GetSAMainURL() + "' AND domainname='" + ReadConfig.getDomainUrl() + "'";
        DataTable dtSiteID = dpSA.FillDataTable(strSql);
        byte siteID = 0;
        if (dtSiteID.Rows.Count > 0)
        {
            siteID = Convert.ToByte(dtSiteID.Rows[0]["id"].ToString());
        }
        else{
            siteID = 2;
        }
        buildChannelList(siteID);
        getNetworksFromDB(siteID.ToString().PadLeft(3, '0'));
    }

    private DataTable GetAPI(byte siteID)
    {
        DataTable dtGetApi = new DataTable();
        DataTable dtChannels = new DataTable();

        string keyWidgetAdrotator = "WidgetAdRotator" + ReadConfig.getDomainUrl();
        if (HttpContext.Current.Cache[keyWidgetAdrotator] != null)
        {
            dtChannels = (DataTable)HttpContext.Current.Cache[keyWidgetAdrotator];
        }
        else
        {
            DBCommand cmdchannel = new DBCommand();
            cmdchannel.CommandText = "Sp_Select_Channels_Ad_rotator";
            cmdchannel.CommandType = CommandType.StoredProcedure;
            cmdchannel.Parameters.Add("@SiteID", Convert.ToString(siteID));
            dtChannels = dpSA.FillDataTable(cmdchannel);
            HttpContext.Current.Cache[keyWidgetAdrotator] = dtChannels;
        }

        dtGetApi.Columns.Add("ChannelID", typeof(string));
        dtGetApi.Columns.Add("ChannelName", typeof(string));
        dtGetApi.Columns.Add("ChannelStreamURL", typeof(string));
        dtGetApi.Columns.Add("ChannelDescription", typeof(string));
        dtGetApi.Columns.Add("ChannelFormat", typeof(string));
        dtGetApi.Columns.Add("PlayList", typeof(string));

        bool ismobiledevice = utils.isMobile();
        if (dtChannels.Rows.Count > 0)
        {
            foreach (DataRow drChannels in dtChannels.Rows)
            {
                bool showonmobile = true;
                if (ismobiledevice && Convert.ToString(drChannels["PlayList"]) == "0")
                {
                    if (!Convert.ToString(drChannels["stream_url"]).ToLower().Contains(".mp4"))
                    {
                        showonmobile = false;
                    }
                }
                if (showonmobile)
                {
                    DataRow dr = dtGetApi.NewRow();
                    string strChannel_Format = Convert.ToString(drChannels["Format"]).ToLower().Trim();

                    dr["ChannelID"] = Convert.ToString(drChannels["channel_id"]);
                    dr["ChannelName"] = Convert.ToString(drChannels["channel_name"]).Replace("&", "&amp;");
                    dr["ChannelDescription"] = Convert.ToString(drChannels["channel_desc"]).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
                    dr["PlayList"] = Convert.ToString(drChannels["PlayList"]);
                    if (Convert.ToString(drChannels["PlayList"]) == "1")
                    {
                        var domainhost = Request.Url.Host;
                        if (Request.Url.Port != 80 && Request.Url.Port != 443)
                        {
                            domainhost += ":" + Request.Url.Port;
                        }
                        dr["ChannelStreamURL"] = "http://" + domainhost + "/" + "widgets/Getplaylist.aspx?Id=" + Convert.ToString(drChannels["channel_id"]);
                    }
                    else
                    {
                        dr["ChannelStreamURL"] = Convert.ToString(drChannels["stream_url"]);
                    }
                    if ((Convert.ToString(strChannel_Format).IndexOf("flash") >= 0 || Convert.ToString(strChannel_Format).IndexOf("flv") >= 0))
                    {
                        dr["ChannelFormat"] = "FLASH";
                    }
                    else if (Convert.ToString(strChannel_Format).IndexOf("rtmp") >= 0 || Convert.ToString(strChannel_Format).IndexOf("RTMP") >= 0)
                    {
                        dr["ChannelFormat"] = "RTMP";
                    }
                    dtGetApi.Rows.Add(dr);
                }
            }
        }
        return dtGetApi;
    }

    private void buildChannelList(byte siteID)
    {
        DataTable dt = GetAPI(siteID);
        if (dt.Rows.Count > 0)
        {
            strChannelName = Convert.ToString(dt.Rows[0]["ChannelName"]);
            lbldesc = Server.HtmlDecode(Convert.ToString(dt.Rows[0]["ChannelDescription"]));
            strFLVPATH = Convert.ToString(dt.Rows[0]["ChannelStreamurl"]);
            strChannelID = Convert.ToString(dt.Rows[0]["ChannelID"]);
            strChannelFormat = Convert.ToString(dt.Rows[0]["ChannelFormat"]);
            if (dt.Rows.Count > 1)
                strChannelIDNext = Convert.ToString(dt.Rows[1]["ChannelID"]);

            isShowPlayList = Convert.ToString(dt.Rows[0]["PlayList"]);
            if (strFLVPATH != "" && strChannelFormat.ToUpper() == "RTMP")
            {
                string strDdlValue = strFLVPATH;
                int slpos = strDdlValue.Trim().LastIndexOf("/");
                slpos = slpos + 1;
                strFLVPATH = strDdlValue.Trim().Substring(0, slpos).Trim();
                MyFileURL = strDdlValue.Trim().Substring(slpos).Trim();
            }
            else if (strChannelFormat.ToUpper() == "FLASH")
            {
                if (isShowPlayList == "1")
                    strFLVPATH = "";
            }
        }
    }

    private void getNetworksFromDB(string SiteId)
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string widgetOS = utils.GetOperatingSystem();
        DataTable TagData = new DataTable();

        string KeyRandomAllAds = "AllNetworks" + ReadConfig.getDomainUrl().Trim() + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + widgetOS.Trim();
        if (HttpContext.Current.Cache[KeyRandomAllAds] != null)
        {
            TagData = (DataTable)HttpContext.Current.Cache[KeyRandomAllAds];
        }
        else
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SELECT a.AdNetwork_ID AS AdNetwork_ID,a.AdNetwork,b.Tag,b.priority,b.Referal,b.Not_Referal,b.Plugin,"
                            + " isnull(b.Not_On_HomePage,0) as Not_On_HomePage,"
                            + " isnull(b.Blocker,0) as Blocker,isnull(b.SameIP_Status,0) as SameIP_Status,"
                            + " isnull(b.SameIP_Value,0) as SameIP_Value,isnull(b.SameIP_Days,0) as SameIP_Days,isnull(b.BlockerValue,0) as BlockerValue,isnull(b.Ad_click_play,0) as Ad_click_play,"
                            + " (SELECT Count(*) FROM Brand_AdNetworkRuleSettings as c INNER JOIN Brand_AdNetwork as d ON c.AdNetwork_ID = d.AdNetwork_ID"
                            + " WHERE c.Active_Inactive='1' and c.priority <>0 and c.priority=b.priority"
                            + " AND (c.Country  like '%" + widgetconid + "%' or c.Country='' or c.Country is null)"
                            + " AND (c.Targeted_DomainURLs like '%" + SiteId + "%' or c.Targeted_DomainURLs='' or c.Targeted_DomainURLs is null or c.Targeted_DomainURLs='-1')"
                            + " AND (c.Browser like '%" + widgetBrowser + "%' OR c.Browser like '%0%')) As CountPriority"
                            + " FROM Brand_AdNetworkRuleSettings as b INNER JOIN Brand_AdNetwork as a ON b.AdNetwork_ID = a.AdNetwork_ID "
                            + " WHERE b.Active_Inactive='1' and b.priority <>0"
                            + " AND (b.Country  like '%" + widgetconid + "%' or b.Country='' or b.Country is null)"
                            + " AND (b.Targeted_DomainURLs like '%" + SiteId + "%' or b.Targeted_DomainURLs='' or b.Targeted_DomainURLs is null or b.Targeted_DomainURLs='-1')"
                            + " AND (b.Browser like '%" + widgetBrowser + "%' OR b.Browser like '%0%')"
                            + " AND (b.OS like '%" + widgetOS + "%' OR b.OS ='' OR b.OS is null) ORDER BY b.priority";
            cmd.CommandType = CommandType.Text;
            TagData = dpSA.FillDataTable(cmd);
            if (ConfigLoader.GetFlagCache() == "1")
            {
                HttpContext.Current.Cache[KeyRandomAllAds] = TagData;
            }
        }
        AllAdsString = GetAllAdsString(TagData, widgetconid, widgetBrowser, SiteId,widgetOS);
    }

    protected string GetAllAdsString(DataTable dt, string CountryID, string BrowserWidget, string SiteID, string OS)
    {
        string ClientIP = Convert.ToString(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
        StringBuilder strAds = new StringBuilder();
        StringBuilder strTags = new StringBuilder();
        strTags.Append("<ADXML>");
        if (dt.Rows.Count > 0)
        {
            int indexto = Convert.ToInt32(dt.Rows[0]["CountPriority"]);
            int k = 0;
            while (indexto <= dt.Rows.Count)
            {
                strTags.Append(GetPriorityString(k, indexto, dt, Convert.ToInt32(dt.Rows[k]["priority"])));
                k = indexto;
                if (k < dt.Rows.Count)
                {
                    indexto = indexto + Convert.ToInt32(dt.Rows[k]["CountPriority"]);
                }
                else
                {
                    break;
                }
            }
        }
        strTags.Append("</ADXML>");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (strAds.ToString() != "")
                {
                    strTags.Length = 0;
                    strTags.Append("BLANK_TAG");
                }
                int SameIP_Days = Convert.ToInt32(dr["SameIP_Days"].ToString());
                int adSameIPCount = utils.SameIPClicks(Convert.ToInt32(dr["AdNetwork_ID"]), "ad", SameIP_Days);
                string adclickplay = "true";
                if (Convert.ToInt32(dr["Ad_click_play"].ToString()) == 1)
                {
                    adclickplay = "false";
                }
                bool isHomePage = true;
                if (Convert.ToString(dr["Not_On_HomePage"]) == "1")
                {
                    isHomePage = utils.IsHomePage();
                }
                if (CheckTrgettingOptions(Convert.ToString(dr["Referal"]), Convert.ToString(dr["Not_Referal"]), isHomePage))
                {
                    strAds.Append(Convert.ToString(dr["AdNetwork_ID"]) + "$#$" + Convert.ToString(dr["AdNetwork"]) + "$#$" + strTags + "$#$" + Convert.ToString(dr["Plugin"]) + "$#$" + adclickplay + "$#$" + Convert.ToString(dr["Blocker"]) + "$#$" + Convert.ToString(dr["BlockerValue"]) + "$#$" + Convert.ToString(dr["SameIP_Status"]) + "$#$" + Convert.ToString(dr["SameIP_Value"]) + "$#$" + adSameIPCount + "$#$");
                    //utils.Setimperssion(Convert.ToInt32(dr["AdNetwork_ID"]), "ad");
                }
            }
        }
        return strAds.ToString();
    }

    private bool CheckTrgettingOptions(string referal, string notreferal, bool homepage)
    {
        bool FlagTargetting = false;
        if (utils.ReferalCheck(referal, notreferal))
        {
            if (homepage)
            {
                FlagTargetting = true;
            }
        }
        return FlagTargetting;
    }

    private string GetPriorityString(int startindex, int endindex, DataTable dt, int priority)
    {
        StringBuilder SbAdTags = new StringBuilder();
        SbAdTags.Append("<priority" + priority + ">");
        for (int j = startindex; j < endindex; j++)
        {
            bool isHomePage = true;
            if (Convert.ToString(dt.Rows[j]["Not_On_HomePage"]) == "1")
            {
                isHomePage = utils.IsHomePage();
            }
            if (isHomePage)
            {
                SbAdTags.Append(Convert.ToInt32(Convert.ToString(dt.Rows[j]["AdNetwork_ID"])) + "^^^" + Convert.ToString(dt.Rows[j]["Tag"]).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent()) + "^*^");
            }
        }
        SbAdTags.Append("</priority" + priority + ">");
        return SbAdTags.ToString();
    }
}