using System;
using System.Data;
using System.Web.UI;

public partial class HerstageVideoDemo : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string AdNetworkID = "";
    public string Tag = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Ad Network Test";
        if (Request.QueryString["adnetworkid"] != null && Convert.ToString(Request.QueryString["adnetworkid"]) != "" && Convert.ToString(Request.QueryString["adnetworkid"]) != "0")
        {
            string SqlSelect = "SELECT BARS.Tag,BARS.Plugin,BA.AdNetwork_ID FROM Brand_AdNetwork AS BA "
                            + "INNER JOIN Brand_AdNetworkRuleSettings AS BARS ON BA.AdNetwork_ID = BARS.AdNetwork_ID "
                            + "WHERE BA.AdNetwork_ID=" + Convert.ToString(Request.QueryString["adnetworkid"]);
            DataTable dt = dp.FillDataTable(SqlSelect);
            if (dt.Rows.Count > 0)
            {
                AdNetworkID = Convert.ToString(dt.Rows[0]["AdNetwork_ID"]);
                Tag = Convert.ToString(dt.Rows[0]["Tag"]).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent());
            }
        }
    }
    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}
