﻿using System;
using System.Data;
using System.Text;
using System.Web;

public partial class Widget_HTML5_IMA_SDK_Video_autoplayon_aspx : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string stradValue = string.Empty;
        stradValue = funAd("none", widgetconid, widgetBrowser);
        if (stradValue != "")
        {
            widget_Grid_none.InnerHtml = stradValue;
        }

        DataTable dtserverMaintainance = new DataTable();
        if (Cache["dtserverMaintainance"] != null)
        {
            dtserverMaintainance = (DataTable)Cache["dtserverMaintainance"];
        }
        else
        {
            DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            string strsqlMaintainance = "select isnull(Server_Maintainance,0) as Server_Maintainance,isnull(Start_Stop,0) as Start_Stop,VideoImpression,OutboundLink,VideoAdStart,VideoAdFinish,BannerClick,VideoAdClick"
                + " from brand_Record_Data";
            dtserverMaintainance = dpSA.FillDataTable(strsqlMaintainance);
            Cache["dtserverMaintainance"] = dtserverMaintainance;
        }

        if (Cache["chkRecordADData"] == null)
        {
            Cache["chkRecordADData"] = Convert.ToByte(dtserverMaintainance.Rows[0]["Start_Stop"]);
        }
        if (Cache["VideoImpression"] == null)
        {
            Cache["VideoImpression"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoImpression"]);
        }
        if (Cache["OutboundLink"] == null)
        {
            Cache["OutboundLink"] = Convert.ToByte(dtserverMaintainance.Rows[0]["OutboundLink"]);
        }
        if (Cache["VideoAdStart"] == null)
        {
            Cache["VideoAdStart"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoAdStart"]);
        }
        if (Cache["VideoAdFinish"] == null)
        {
            Cache["VideoAdFinish"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoAdFinish"]);
        }
        if (Cache["BannerClick"] == null)
        {
            Cache["BannerClick"] = Convert.ToByte(dtserverMaintainance.Rows[0]["BannerClick"]);
        }
        if (Cache["VideoAdClick"] == null)
        {
            Cache["VideoAdClick"] = Convert.ToByte(dtserverMaintainance.Rows[0]["VideoAdClick"]);
        }
    }

    private string funAd(string strPosition, string widgetconid, string widgetBrowser)
    {
        StringBuilder strAdBox = new StringBuilder();

        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_BrandAppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        strQueryAdBox.AddParameter("@Position", strPosition);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);

        if (dt_AppID.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_AppID.Rows)
            {
                strAdBox.AppendLine("<div id=\"" + Convert.ToString(dr["ApplicationName"]) + "\" class=\"" + Convert.ToString(dr["ApplicationName"]) + "\" >");
                strAdBox.AppendLine(HttpUtility.HtmlDecode(utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]))));
                strAdBox.AppendLine("</div>");
            }
        }
        return Convert.ToString(strAdBox);
    }
}