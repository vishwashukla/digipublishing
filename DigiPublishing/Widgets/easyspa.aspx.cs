﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Xml;
using Newtonsoft.Json;

public partial class Widgets_easyspa : System.Web.UI.Page
{
    public string StaffJson = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ConfigurationManager.AppSettings["EasySpaConnectionString"].ToString(), DBSType.MSSQL);
        string ImageDomain = ConfigurationManager.AppSettings["EasySpaimagedomain"].ToString();
        string strquery = "select * from (SELECT  (case WHEN SS.thumb_profile='' Then '' WHEN SS.thumb_profile IS NULL Then '' ELSE ('" + ImageDomain + "' +  SS.thumb_profile) END) + ','+"
                        + "(SELECT ISNULL(STUFF((SELECT ',' + '" + ImageDomain + "' + spa_user_image_path "
                        + "FROM spa_user_images WHERE spa_user_image_status = 'A' and spa_user_image_user_id=SU.user_id FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, ''),''))"
                        + " as PFIMG, "
                        + "CASE WHEN (SS.spa_staff_phone_country_code IS NOT NULL AND SS.spa_staff_phone IS NOT NULL "
                        + "AND SS.spa_staff_phone_country_code<>'' AND SS.spa_staff_phone<>'') THEN ('+'+REPLACE(SS.spa_staff_phone_country_code,'+','')+  REPLACE(REPLACE(SS.spa_staff_phone,'-',''),' ','')) "
                        + "ELSE '' END as PN, "
                        + "SU.user_id AS UID,SU.user_name as UN, "
                        + "CASE WHEN SS.ss_business_name='' THEN SU.user_name WHEN SS.ss_business_name IS NULL THEN SU.user_name ELSE SS.ss_business_name END as BN, "
                        + "CASE WHEN SS.ss_homeservice_status='Y' THEN 1 ELSE 0 END as STHS, "
                        + "CASE WHEN SS.ss_customer_to_spa_status='Y' THEN 1 ELSE 0 END as CTSS, "
                        + "ISNULL(SS.spa_staff_website,'') AS WS, "
                        + "ISNULL(SS.ss_fb_url,'') AS FB, "
                        + "ISNULL(SS.ss_twitter_url,'') AS TW, "
                        + "SS.spa_staff_email as EMAIL, "
                        + "C.country_currency_code as CCC, "
                        + "C.country_currency_symbol as CCS, "
                        + "ISNULL(SS.spa_staff_address1,'')+' '+  ISNULL(SS.spa_staff_address2,'')+' '+  ISNULL(SS.spa_staff_address3,'') as ADDR, "
                        + "ISNULL((SELECT STUFF((SELECT ',' +Convert(varchar(50), spa_staff_opening_day)+'-'+Convert(varchar(50),spa_staff_opening_from)+'-' "
                        + "+Convert(varchar(50), spa_staff_opening_to) as OpeningDate FROM spa_staff_opening "
                        + "where spa_staff_id =SS.spa_staff_id and spa_staff_opening_active ='A' FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') as OpeningList),'') "
                        + "as OpeningList, "
                        + "'" + ImageDomain + "/uploads/wr' +convert(varchar,(SELECT ROUND(isnull(Convert(float, sum(ratings))/Convert(float, count(ratings)), 0), 0) "
                        + "FROM customer_review as CR INNER JOIN spa_user AS SU ON  CR.customer_id = SU.user_reference_id "
                        + "WHERE spa_staff_id = SS.spa_staff_id and customer_review_active = 'A' AND SU.user_reference_table = 'customer'))+'.png' AS AverageRating, "
                        + "( SELECT STUFF((SELECT '##$,$##'"
                        + "+ Convert(nvarchar(500),SSSN.service_description "
                        + "+'##$-$##'+ ('" + ImageDomain + "' + SC.service_category_icon_thumb)"
                        + "+'##$-$##'+ Convert(nvarchar(50),SSSN.service_duration) "
                        + "+'##$-$##'+ Convert(nvarchar(50),SSSN.service_cost) "
                        + "+'##$-$##'+ Convert(nvarchar(50),SSSN.service_name)) as spaservies FROM spa_staff_service_new AS SSSN "
                        + "Inner Join service_category AS SC on SC.service_category_id = SSSN.service_categoryid "
                        + "where  SSSN.service_spa_staff_id = SS.spa_staff_id "
                        + "and SC.service_category_status = 'A' "
                        + "FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 7, '') As ServiceList) "
                        + "as ServiceListNew, "
                        + "SS.spa_staff_description AS SSDESC, "
                        + "convert(float,SS.spa_staff_lat) as LAT, "
                        + "convert(float,SS.spa_staff_long) as LNG "
                        + "FROM Spa_staff as SS "
                        + "INNER JOIN spa_user AS SU "
                        + "ON (SU.user_reference_id = SS.spa_staff_id AND SU.user_reference_table='staff') "
                        + "INNER JOIN country AS C "
                        + "ON SS.country_id = C.country_id "
                        + "WHERE SS.spa_staff_active='A' "
                        + "and (convert(float,SS.spa_staff_lat) is not null and convert(float,SS.spa_staff_lat)<>0) "
                        + "and (convert(float,SS.spa_staff_long) is not null and convert(float,SS.spa_staff_long)<>0)) t "
                        + "where t.ServiceListNew IS NOT NULL and (t.STHS=1 OR t.CTSS=1) "
                        + "and (t.EMAIL is not null and t.EMAIL<>'') and (t.PN is not null and t.PN<>'')";

        //Response.Write(strquery);

        DataTable dt = dp.FillDataTable(strquery);
        string strxml = ConvertDataTableToXml(dt);
        XmlDocument xdoc = new XmlDocument();
        xdoc.LoadXml(strxml);
        StaffJson = JsonConvert.SerializeXmlNode(xdoc);
        hdnJson.Value = StaffJson;
        //return new System.IO.MemoryStream(Encoding.UTF8.GetBytes(JSONresult));
    }

    private string ConvertDataTableToXml(DataTable table)
    {
        DataTable dt = SetValueForNull(table);
        string xmlString = string.Empty;
        using (TextWriter writer = new StringWriter())
        {
            dt.WriteXml(writer, true);
            xmlString = writer.ToString();
        }
        return xmlString;
    }

    private DataTable SetValueForNull(DataTable dt)
    {
        int i, j;
        for (i = 0; i < dt.Columns.Count; i++)
        {
            for (j = 0; j < dt.Rows.Count; j++)
            {
                if (dt.Columns[i].DataType.ToString() == "System.Int32" || dt.Columns[i].DataType.ToString() == "System.Single" || dt.Columns[i].DataType.ToString() == "System.Double" || dt.Columns[i].DataType.ToString() == "System.Decimal")
                {
                    if (dt.Rows[j][i] == DBNull.Value)
                        dt.Rows[j][i] = 0;
                }
                else if (dt.Columns[i].DataType.ToString() == "System.String" || dt.Columns[i].DataType.ToString() == "System.Text")
                {
                    if (dt.Rows[j][i] == DBNull.Value || dt.Rows[j][i].ToString().Trim() == "")
                        dt.Rows[j][i] = "";
                    else
                        dt.Rows[j][i] = utils.formatStringForDBSelect(dt.Rows[j][i].ToString()).Replace("%2B", "+").Replace("%2b", "+").Replace("%26", "&");
                }
            }
        }
        return dt;
    }
}