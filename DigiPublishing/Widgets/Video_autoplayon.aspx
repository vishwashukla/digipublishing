﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Video_autoplayon.aspx.cs" Inherits="Widget_HTML5_IMA_SDK_Video_autoplayon_aspx" %>

<!DOCTYPE html>
<%@ Register Src="~/Widgets/Video_autoplayon.ascx" TagPrefix="VW" TagName="VideoWidget6" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="/JS/jquery.3.1.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        var DomainForGA = '<%=Convert.ToString(System.Web.HttpContext.Current.Request.Url.Host) %>';
        var WebServiceDomain = 'http://' + DomainForGA + '/';
        if (WebServiceDomain.indexOf("localhost") > -1) {
            WebServiceDomain = '<%= Convert.ToString(ConfigurationManager.AppSettings["WebServiceDomain"]) %>';
        }
        var REMOTE_ADDR = '<%= Convert.ToString(Request.ServerVariables["REMOTE_ADDR"])%>';
        var Referer_URL = '<%=Convert.ToString(Session["Referer_URL"])%>';
        var UserAgent = '<%= Convert.ToString(Request.UserAgent)%>';
        var ISSendGAVideoImpression = '<%= Convert.ToString(HttpContext.Current.Cache["VideoImpression"])%>';
        var ISSendGAOutboundLink = '<%= Convert.ToString(HttpContext.Current.Cache["OutboundLink"])%>';
        var ISSendGAVideoAdStart = '<%= Convert.ToString(HttpContext.Current.Cache["VideoAdStart"])%>';
        var ISSendGAVideoAdFinish = '<%= Convert.ToString(HttpContext.Current.Cache["VideoAdFinish"])%>';
        var ISSendGABannerClick = '<%= Convert.ToString(HttpContext.Current.Cache["BannerClick"])%>';
        var ISSendGAVideoAdClick = '<%= Convert.ToString(HttpContext.Current.Cache["VideoAdClick"])%>';
        var Campaign_Source = '<%= Convert.ToString(Session["Campaign_Source"])%>';
        var strAllowAdnetworkData = '<%= Convert.ToString(HttpContext.Current.Cache["chkRecordADData"])%>';

        var GAID = '<%= ConfigLoader.GetGA_ID()%>';
        var UserHostAddress = '<%= Request.UserHostAddress %>';
        var Time = '<%= System.DateTime.Now.ToString("M/d/yyyy h:m:s.ffffff tt") %>';
    </script>

    <div id="widget_Grid_none" runat="server">
    </div>

    <div>
        <div id="top_ad_box5" runat="server" visible="true" class="ads top_ad_box">
            <VW:VideoWidget6 ID="UCvideowiget6" runat="server"></VW:VideoWidget6>
        </div>
    </div>
    </form>
</body>
</html>
