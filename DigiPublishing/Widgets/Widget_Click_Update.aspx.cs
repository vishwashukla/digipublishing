using System;
using System.Data;

public partial class Widget_HTML5_IMA_SDK_Widget_Click_Update_HTML5 : System.Web.UI.Page
{
    public string digip_domain = string.Empty;
    public string UserAgent = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        string ClientIP = "";
        string Referer_URL = "";
        string Campaign_Source = "";

        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["UserAgent"])))
        {
            UserAgent = Convert.ToString(Request.QueryString["UserAgent"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["REMOTE_ADDR"])))
        {
            ClientIP = Convert.ToString(Request.QueryString["REMOTE_ADDR"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["DomainForGA"])))
        {
            digip_domain = Convert.ToString(Request.QueryString["DomainForGA"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["Referer_URL"])))
        {
            Referer_URL = Convert.ToString(Request.QueryString["Referer_URL"]);
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["Campaign_Source"])))
        {
            Campaign_Source = Convert.ToString(Request.QueryString["Campaign_Source"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["WidIDblock"])) && Convert.ToInt32(Request.QueryString["WidIDblock"]) != 0)
            UpdateAdnetworkClick(Convert.ToInt32(Request.QueryString["WidIDblock"]), dp, ClientIP, Referer_URL, Campaign_Source, Convert.ToInt32(Request.QueryString["ClickDays"]));

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["WidIDblock1"])) && Convert.ToInt32(Request.QueryString["WidIDblock1"]) != 0 && !string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["maxval"])))
            UpdateAdnetworkClickBlocked(Convert.ToInt32(Request.QueryString["WidIDblock1"]), Convert.ToInt32(Request.QueryString["maxval"]), dp, ClientIP, Referer_URL, Campaign_Source);

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["adstartid"])) && Convert.ToInt32(Request.QueryString["adstartid"]) != 0)
            CountAdstart(Convert.ToInt32(Request.QueryString["adstartid"]));

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["adfinishid"])) && Convert.ToInt32(Request.QueryString["adfinishid"]) != 0)
            CountAdfinish(Convert.ToInt32(Request.QueryString["adfinishid"]));

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["adimpressionid"])) && Convert.ToInt32(Request.QueryString["adimpressionid"]) != 0)
            CountAdImpression(Convert.ToInt32(Request.QueryString["adimpressionid"]));
    }

    public void UpdateAdnetworkClick(int banid,DBHelper dp, string ClientIP, string Referer_URL, string Campaign_Source,int clickdays)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            string sqlSel = "Select UserAgent FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            string useragent = ""; 
            if (dt.Rows.Count > 0)
                useragent = Convert.ToString(dt.Rows[0]["UserAgent"]).Trim();

            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", ClientIP);
            cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
            cmd.Parameters.Add("@DIGIP_Domain", digip_domain);
            cmd.Parameters.Add("@Referer_URL", Referer_URL);
            cmd.Parameters.Add("@Type", "ad");
            cmd.Parameters.Add("@block_status", "0");
            cmd.Parameters.Add("@Maxvalue", "0");
            cmd.Parameters.Add("@Campaign_Source", Campaign_Source);
            if (useragent.Trim() == "1")
            {
                cmd.Parameters.Add("@UserAgent", UserAgent);
            }
            dpreport.ExecuteNonQuery(cmd);

            if (ConfigLoader.GetIsUseNewClickDB() == 1)
            {
                DBHelper dpreport2 = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
                DBCommand cmd2 = new DBCommand();
                cmd2.CommandText = "SP_Brand_Insert_ClickInfo";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.Add("@IPAdress", ClientIP);
                cmd2.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd2.Parameters.Add("@DIGIP_Domain", digip_domain);
                cmd2.Parameters.Add("@Type", "ad");
                dpreport2.ExecuteNonQuery(cmd2);
            }
        }
        int adSameIPCount = utils.SameIPClicks(banid, "ad", clickdays);
        Response.Write(adSameIPCount.ToString());
    }

    public void UpdateAdnetworkClickBlocked(int widID, int Maxvalue, DBHelper dp, string ClientIP, string Referer_URL, string Campaign_Source)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            string sqlSel = "Select UserAgent FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            string useragent = "";
            if (dt.Rows.Count > 0)
            {
                useragent = dt.Rows[0]["UserAgent"].ToString().Trim();
            }
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", ClientIP);
            cmd.Parameters.Add("@WidgetID", Convert.ToString(widID));
            cmd.Parameters.Add("@DIGIP_Domain", digip_domain);
            cmd.Parameters.Add("@Referer_URL", Referer_URL);
            cmd.Parameters.Add("@Type", "ad");
            cmd.Parameters.Add("@block_status", "1");
            cmd.Parameters.Add("@Maxvalue", Convert.ToString(Maxvalue));
            cmd.Parameters.Add("@Campaign_Source", Campaign_Source);
            if (useragent.Trim() == "1")
            {
                cmd.Parameters.Add("@UserAgent", UserAgent);
            }
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void CountAdstart(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_AdStart_Count";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ADID", Convert.ToString(widID));
            cmd.Parameters.Add("@Type", "ad");
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void CountAdImpression(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_Ad_Impression";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ADID", Convert.ToString(widID));
            cmd.Parameters.Add("@Type", "ad");
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void CountAdfinish(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_AdFinish_Count";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ADID", Convert.ToString(widID));
            cmd.Parameters.Add("@Type", "ad");
            dpreport.ExecuteNonQuery(cmd);
        }
    }
}
