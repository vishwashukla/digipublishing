﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Widgets.aspx.cs" Inherits="Widgets_aspx_Wedget" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <script type="text/javascript">
        var DomainForGA = '<%=Convert.ToString(System.Web.HttpContext.Current.Request.Url.Host) %>';
        var WebServiceDomain = 'http://' + DomainForGA + '/';
        if (DomainForGA == "localhost"){
            WebServiceDomain = 'http://localhost:2740/';
        }
        var REMOTE_ADDR = '<%= Convert.ToString(Request.ServerVariables["REMOTE_ADDR"])%>';
        var Referer_URL = '<%=Convert.ToString(Session["Referer_URL"])%>';
        var Campaign_Source = '<%= Convert.ToString(Session["Campaign_Source"])%>';
        var UserAgent = '<%= Convert.ToString(Request.UserAgent)%>';
        var strAllowAdnetworkData = '<%= Convert.ToString(HttpContext.Current.Cache["chkRecordADData"])%>';
        var ISSendGABannerClick = '<%= Convert.ToString(HttpContext.Current.Cache["BannerClick"])%>';
        var BannerIDs = '<%= BannerIDs%>';
        var GAID = '<%= ConfigLoader.GetGA_ID()%>';
        var UserHostAddress = '<%= Request.UserHostAddress %>';
        var Time = '<%= System.DateTime.Now.ToString("M/d/yyyy h:m:s.ffffff tt") %>';
        var WidgetBlockerCount = 0;
        var WidgetSameIPCount = <%= WidgetSameIPCount%>;
    </script>
</head>
<body>
    <script type="text/javascript" src="../../JS/jquery.1.8.0.min.js"></script>
    <div id ="widget_box" runat="server"></div>
    <script type="text/javascript">
        function UNBlockedClick(id) {
            RecordData("&banID=" + id);
        }
        function BlockedClick(id) {
            RecordData("&banIDBlocked=" + id);
        }
        function RecordData(str) {
            var url = WebServiceDomain + "Banner_Click_UPD.aspx?DomainForGA=" + DomainForGA + "&REMOTE_ADDR=" + REMOTE_ADDR + "&Referer_URL=" + Referer_URL + "&Campaign_Source=" + Campaign_Source + str;
            var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
            xhttp.onreadystatechange = function () { }
            xhttp.open("GET", url, true);
            xhttp.send();
        }
        function SetDisplay(id, val) {
            var a = document.getElementById(id);
            if (a != null) a.style.display = val;
        }
        function SameIpClickCheck(IPStatus, IPValue, SameIPCount, imgid, widid, bName) {
            if (IPStatus == 1 && IPValue <= SameIPCount)
                SetDisplay(imgid, "block");
            if (IPStatus == 1 && IPValue < SameIPCount)
                BlockedClick(widid);
            else {
                //try {
                //    if (ISSendGABannerClick == "1") {

                //        if (bName != "") {
                //            if (typeof _gaq != "undefined") {
                //                _gaq.push(['_trackEvent', 'BannerClick', bName, DomainForGA]);
                //            }
                //            else if (typeof ga != "undefined") {
                //                ga('send', 'event', 'BannerClick', bName, DomainForGA);
                //            }
                //        }
                //    }
                //} catch (err) { }
                UNBlockedClick(widid);
            }
        }
        function CurrentClickCheck(IPStatus, IPValue, SameIPCount, BlockerCount, imgid, widid, BlockerValue, bName) {
            if (BlockerCount >= BlockerValue)
                SetDisplay(imgid, "block");
            if (BlockerCount > BlockerValue)
                BlockedClick(widid);
            else
                SameIpClickCheck(IPStatus, IPValue, SameIPCount, imgid, widid, bName);
        }
        //Widget Click
        //WidgetClick('461',0,1,5,2,'Imgblockerwidget_0','test_banner_digip_right_160x600','0','WidgetSameIPCount','WidgetBlockerCount')
        function WidgetClick(id, Ftype, IPStatus, IPValue, BlockerValue, imgid, bName, rowindex) {
            WidgetSameIPCount += 1;
            if (Ftype == 0) {
                WidgetBlockerCount += 1;
                CurrentClickCheck(IPStatus, IPValue, WidgetSameIPCount, WidgetBlockerCount, imgid, id, BlockerValue, bName);
            }
            else if (Ftype == 1)
                SameIpClickCheck(IPStatus, IPValue, WidgetSameIPCount, imgid, id, bName);
            else if (Ftype == 2)
                BlockedClick(id);
        }

        //Record banner impression in database
        function SetImperssion_Banners() {
            $.ajax({
                type: "Post",
                async: true,
                url: WebServiceDomain + "Services/AdNetworkReport.asmx/Setimperssion",
                data: "{'AdNetworkIDs':'" + BannerIDs + "' ,'type':'banner','REMOTE_ADDR':'" + REMOTE_ADDR + "','domainnameCS':'" + DomainForGA + "','Referer_URL':'" + Referer_URL + "','strAllowAdnetworkData':'" + strAllowAdnetworkData + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { },
                error: function (e) {
                    //alert(e.responseText);
                }
            });
        }
    </script>
    <script type="text/javascript">
        if (strAllowAdnetworkData == "1")
            SetImperssion_Banners();
    </script>
</body>
</html>
