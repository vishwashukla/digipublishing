﻿using System;
using System.Data;
using System.Text;
using System.Web;

public partial class Widgets_aspx_Wedget : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string strAppPath;
    public string strDomainConfigPath;
    public string BannerIDs = string.Empty;
    string strDomainName;
    public string domainhost;
    public string ssResponsePID;
    public string WidgetSameIPCount;

    protected void Page_Load(object sender, EventArgs e)
    {          
        string ID =  Request.QueryString["aspxUrl"];
        //ID = "MjAxNjExMDgxNzIyNDc3NTA";        
        if (ID != null)
        {
            DataTable dt = new DataTable();
            string strSql = " select  AppList_ID from Brand_AppListRuleSettings where aspxUrl = '" + ID + "' ";
            dt = dp.FillDataTable(strSql);
            GetWidgets(dt.Rows[0][0].ToString());    
        }
    }

    private void GetWidgets(string id)
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string widgetOS = utils.GetOperatingSystem();
        string stradValue = string.Empty;
        widget_box.InnerHtml = funAdRandom(widgetconid, widgetBrowser, id, widgetOS);
    }


    private string funAdRandom(string widgetconid, string widgetBrowser, string id, string widgetOS)
    {
        StringBuilder strAdBox = new StringBuilder();
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        int App_Id = Convert.ToInt32(id);

        string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
        strSQLRule = SqlQueryMain;
        DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        dtAdBox = dp1.FillDataTable(strSQLRule);


        if (dtAdBox.Rows.Count > 0)
        {
            if (utils.ReferalCheck(Convert.ToString(dtAdBox.Rows[0]["Referal"]), Convert.ToString(dtAdBox.Rows[0]["Not_Referal"])))
            {
                strAdBox.Append(GetfunAd(dtAdBox.Rows[0], 0));
            }
        }
        return Convert.ToString(strAdBox);
    }


    private string GetfunAd(DataRow dr, int rowindex)
    {
        string stradimage = "";
        int IPStatus = 0;
        int IPValue = 0;
        int IPDays = 0;
        int Blocker = 0;
        string ApplicationName = Convert.ToString(dr["ApplicationName"]);
        int BlockerValue = 0;
        int BanID = Convert.ToInt32(dr["id"]);
        int height = 0;
        int width = 0;
        string codebox = string.Empty;

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Status"])))
        {
            IPStatus = Convert.ToInt32(dr["SameIP_Status"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Value"])))
        {
            IPValue = Convert.ToInt32(dr["SameIP_Value"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Days"])))
        {
            IPDays = Convert.ToInt32(dr["SameIP_Days"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["Blocker"])))
        {
            Blocker = Convert.ToInt32(dr["Blocker"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["BlockerValue"])))
        {
            BlockerValue = Convert.ToInt32(dr["BlockerValue"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["height"])))
        {
            height = Convert.ToInt32(dr["height"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["width"])))
        {
            width = Convert.ToInt32(dr["width"].ToString().Trim());
        }

        string strclickfunction = "";
        stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "Imgblockerwidget", "WidgetClick", rowindex, ref strclickfunction);
        
        Random random = new Random();
        int cacheBuster = random.Next(10000000, 100000000);
        codebox = utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"]));
        string pageUrl = Request.Url.Scheme + "://" + Request.Url.Host + Request.RawUrl;
        string newcodebox = codebox.Replace("[Host_URL]", strDomainName).Replace("[CACHEBUSTER]", Convert.ToString(cacheBuster)).Replace("[page_url]", pageUrl).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent());
        StringBuilder strAdBox = new StringBuilder();
        if (height == 0 && width == 0)
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"position:relative;\" " + strclickfunction + ">" + stradimage
                + HttpUtility.HtmlDecode(newcodebox) + "</div>");
        }
        else
        {
            strAdBox.Append("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"height:auto;width:" + width + "px;position:relative;\" " + strclickfunction + ">" + stradimage
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(newcodebox)) + "</div>");
        }
        return strAdBox.ToString();
    }


    private string Get_Image_RegisterScriptBlock_And_Attach_Click_Event(int BanID, int IPStatus, int IPValue, int IPDays, int Blocker, int BlockerValue, string ApplicationName
        , string imageid, string clickfunname, int rowindex, ref string clickfunction)
    {

        int SameIPCount = utils.SameIPClicks(BanID, "banner", IPDays);
        BannerIDs += BanID + ",";     
        string strdis = "none";
        string type = "0";
        if (IPStatus == 1 && IPValue <= SameIPCount && BanID != 0){
            strdis = "block";
            type = "2";
        }
        if (strdis == "none" && Blocker != 1)
            type = "1";
        clickfunction = "onclick=\"javascript:" + clickfunname + "('" + BanID + "'," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",'" + imageid + "_" + rowindex
            + "','" + ApplicationName + "','" + rowindex + "')\"";
        string stradimage = "<img  alt='1x2' id ='" + imageid + "_" + rowindex + "' style='display:" + strdis + ";z-index:9999;float:right;position:absolute;left:0;top:0;width:100%; height:100%;' "
            + "src='../images_common/1x2.gif' />";
         WidgetSameIPCount = SameIPCount.ToString();
        return stradimage;
    }
}