﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="easyspa.aspx.cs" Inherits="Widgets_easyspa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="easyspa.css" />
    <link rel="stylesheet" href="slick/slick.css" />
    <link rel="stylesheet" href="slick/slick-theme.css" />
    <script src="../JS/jquery.3.1.1.min.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
</head>
<body onload="LoadMap()">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiFBbrEIDFfPPUz2tsPTcHYRatWTYoDhA"></script>
    <form id="form1" runat="server">
        <div>
            <div class="bgBlack">
                <h2>Find: Hair, Makeup, Massage, Nail Care, Skincare, Waxing</h2>
            </div>
            <div id="mapspa"></div>
            <div class="containereasyspa">
                <div id="spadetail" style="display: none;">
                    <h2 id="BUsinessName" class="b-c m-b-s"></h2>
                    <div class="s-c h-s" id="servicesContainer">
                        <div id="divservices" class="alldivservices m-b-s"></div>
                    </div>
                    <div class="bgwhite d-c">
                        <div id="divimg" class="m-b-s"></div>
                        <div id="divInformation" class="dib">
                            <table class="table-info">
                                <tr>
                                    <td colspan="5" style="border-width:0px!important;padding-left:0px;"><b>Information List : </b></td>
                                </tr>
                                <tr>
                                    <td><b>Address : </b></td>
                                    <td colspan="4">
                                        <div id="divaddr" class="m-b-s"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Website URL : </b></td>
                                    <td colspan="4">
                                        <div id="divweb" class="m-b-s">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Facebook URL : </b></td>
                                    <td colspan="4">
                                        <div id="divfb" class="m-b-s"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Home Visit : </b></td>
                                    <td class="center"><b>Go to Spa : </b></td>
                                    <td class="center"><b>Call : </b></td>
                                    <td class="center"><b>Email : </b></td>
                                    <td class="center"><b>Rating : </b></td>
                                </tr>
                                <tr>
                                    <td class="center">
                                        <div id="divhomevisit" class="dvinlineshows">
                                            <img id="imgtickhv" class="imgtickcross" src="../Images_Common/tick.png" alt="tick" style="display: none;" />
                                            <img id="imgcrosshv" class="imgtickcross" src="../Images_Common/cross.png" alt="cross" style="display: none;" />
                                        </div>
                                    </td>
                                    <td class="center">
                                        <div id="divspavisit" class="dvinlineshows">
                                            <img id="imgticksv" class="imgtickcross" src="../Images_Common/tick.png" alt="tick" style="display: none;" />
                                            <img id="imgcrosssv" class="imgtickcross" src="../Images_Common/cross.png" alt="cross" style="display: none;" />
                                        </div>
                                    </td>
                                    <td class="center">
                                        <div id="divcall" class="dvinline">
                                            <span id="shownum" class="show" onclick="ShowNumber()">Show Number</span>
                                            <span id="num"></span>
                                        </div>
                                    </td>
                                    <td class="center">
                                        <div id="divemail" class="dvinline">
                                            <span id="showemail" class="show" onclick="ShowEmail()">Show Email</span>
                                            <span id="email"></span>
                                        </div>
                                    </td>
                                    <td class="center">
                                        <div id="divrating" class="m-b-s">
                                            <img id="rating" alt="rating" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Description : </b></td>
                                    <td colspan="4">
                                        <div id="divdesc" class="m-b-s"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divopening" class="dib">
                        </div>
                    </div>
                    <div class="bgBlack">
                        <h2>&nbsp;</h2>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnJson" runat="server" />
        </div>
    </form>
    <script type="text/javascript" src="easyspa.js"></script>
</body>
</html>
