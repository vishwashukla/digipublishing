﻿var map;
function ShowNumber() {
    document.getElementById("num").style.display = "inline";
    document.getElementById("shownum").style.display = "none";
}
function ShowEmail() {
    document.getElementById("email").style.display = "inline";
    document.getElementById("showemail").style.display = "none";
}
function LoadMap() {
    var staffJSon = JSON.parse(document.getElementById("hdnJson").value);
    var centerLatLng = { lat: 34.072895, lng: -95.918469 };
    var mapOptions = {
        center: centerLatLng,
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        minZoom: 2,
        maxZoom: 19,
        zoomControl: true,
        fullscreenControl: false
    }
    map = new google.maps.Map(document.getElementById("mapspa"), mapOptions);
    google.maps.event.addListener(map, 'idle', function () {
        var currcenter = map.center;
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            centerLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(centerLatLng);
        });
    }
    AddMarkers(staffJSon.NewDataSet.Table);
    window.parent.document.getElementById('ifrmeasyspa').height = document.body.offsetHeight + "px";
}
function AddMarkers(data) {
    for (var i = 0; i < data.length; i++) {
        var sdata = data[i];
        var lat = parseFloat(sdata.LAT);
        var lng = parseFloat(sdata.LNG);
        var latLng = new google.maps.LatLng(lat, lng);
        data[i].marker = createMarker(lat, lng, sdata);
        data[i].marker.setMap(map);
    }
}
function IsValidUrl(url) {
    return /((ftp|https?):\/\/)?(www\.)?[a-z0-9\-\.]{3,}\.[a-z]{3}$/
    .test(url);
}
function createMarker(lat, lng, sdata) {
    var latLng = new google.maps.LatLng(lat, lng);
    var marker = new google.maps.Marker({
        position: latLng,
        data: sdata
    });
    google.maps.event.addListener(marker, 'click', function () {
        document.getElementById("spadetail").style.display = "none";
        var sdata = marker.data;
        document.getElementById("BUsinessName").innerHTML = sdata.BN;
        if (sdata.ServiceListNew != null && sdata.ServiceListNew != "") {
            var arrservices = sdata.ServiceListNew.split("##$,$##");
            var htmlservices = "";
            var width = 0;
            for (var i = 0; i < arrservices.length; i++) {
                var itemservices = arrservices[i].split("##$-$##");
                if (itemservices.length >= 5) {
                    htmlservices += "<div class=\"divservices bgwhite\">";
                    htmlservices += "<div class=\"serviceimg\"><img class=\"serviceimages\" alt=\"serviceimage\" id=\"servicePic" + i + "\" src=\"" + itemservices[1] + "\" /></div>";
                    htmlservices += "<div class=\"servicename\">" + itemservices[4] + "</div>";
                    htmlservices += "<div class=\"servicedesc\">" + itemservices[0] + "</div>";
                    htmlservices += "<div class=\"serviceduration\"><span>Service Duration:</span><span class=\"f-r\">" + itemservices[2] + " mins</span></div>";
                    htmlservices += "<div class=\"servicecost\"><span>Service Cost:</span><span class=\"f-r\">" + ((sdata.CCC == '' || sdata.CCC == null) ? '' : ' ' + sdata.CCC + ' ') + ((sdata.CCS == '' || sdata.CCS == null) ? '' : sdata.CCS) + itemservices[3] + "</span></div>";
                    htmlservices += "</div>";
                }
                width += 255;
            }
            try {
                if ($('.alldivservices').hasClass('slick-initialized')) {
                    $('.alldivservices').slick('destroy');
                    $('.slick-slide').remove();
                }
            }
            catch (e){ }
            document.getElementById("divservices").innerHTML = htmlservices;
            document.getElementById("divservices").style.display = "block";
        }
        else {
            document.getElementById("divservices").style.display = "none";
        }

        if (sdata.PFIMG != null && sdata.PFIMG != "") {
            var arrimages = sdata.PFIMG.split(",");
            var htmlimages = "";
            for (var i = 0; i < arrimages.length; i++) {
                if (arrimages[i] != "")
                    htmlimages += "<div class=\"divprofimages\"><img class=\"profimages\" alt=\"profile\" id=\"profilePic" + i + "\" src=\"" + arrimages[i] + "\" /></div>"
            }
            document.getElementById("divimg").innerHTML = htmlimages;
            document.getElementById("divimg").style.display = "block";
        }
        else {
            document.getElementById("divimg").style.display = "none";
        }
        if (sdata.ADDR != null && sdata.ADDR != "") {
            document.getElementById("divaddr").innerHTML = sdata.ADDR;
            document.getElementById("divaddr").style.display = "block";
        }
        else {
            document.getElementById("divaddr").style.display = "none";
        }
        if (sdata.WS != null && sdata.WS != "") {// && IsValidUrl(sdata.WS)
            if (sdata.WS.indexOf("http://") > -1 || sdata.WS.indexOf("https://") > -1)
            {
                document.getElementById("divweb").innerHTML = "<a target=\"_blank\" href=\"" + sdata.WS + "\">" + sdata.WS + "</a>";
            }
            else
            {
                document.getElementById("divweb").innerHTML = "<a target=\"_blank\" href=\"http://" + sdata.WS + "\">http://" + sdata.WS + "</a>";
            }
            document.getElementById("divweb").style.display = "block";
            document.getElementById("divweb").parentElement.parentElement.style.display = "table-row";
        }
        else {
            document.getElementById("divweb").style.display = "none";
            document.getElementById("divweb").parentElement.parentElement.style.display = "none";
        }
        if (sdata.FB != null && sdata.FB != "") {// && IsValidUrl(sdata.FB)
            if (sdata.FB.indexOf("http://") > -1 || sdata.FB.indexOf("https://") > -1) {
                document.getElementById("divfb").innerHTML = "<a target=\"_blank\" href=\"" + sdata.FB + "\">" + sdata.FB + "</a>";
            }
            else {
                document.getElementById("divfb").innerHTML = "<a target=\"_blank\" href=\"http://" + sdata.FB + "\">http://" + sdata.FB + "</a>";
            }
            document.getElementById("divfb").style.display = "block";
            document.getElementById("divfb").parentElement.parentElement.style.display = "table-row";
        }
        else {
            document.getElementById("divfb").style.display = "none";
            document.getElementById("divfb").parentElement.parentElement.style.display = "none";
        }
        if (sdata.STHS != null && sdata.STHS != "") {
            if (parseInt(sdata.STHS) == 1) {
                document.getElementById("imgticksv").style.display = "inline";
                document.getElementById("imgcrosssv").style.display = "none";
            }
            else {
                document.getElementById("imgticksv").style.display = "none";
                document.getElementById("imgcrosssv").style.display = "inline";
            }
            document.getElementById("divhomevisit").style.display = "inline-block";
        }
        else {
            document.getElementById("divhomevisit").style.display = "none";
        }
        if (sdata.CTSS != null && sdata.CTSS != "") {
            if (parseInt(sdata.CTSS) == 1) {
                document.getElementById("imgtickhv").style.display = "inline";
                document.getElementById("imgcrosshv").style.display = "none";
            }
            else {
                document.getElementById("imgtickhv").style.display = "none";
                document.getElementById("imgcrosshv").style.display = "inline";
            }
            document.getElementById("divspavisit").style.display = "inline-block";
        }
        else {
            document.getElementById("divspavisit").style.display = "none";
        }

        if (sdata.PN != null && sdata.PN != "") {
            document.getElementById("num").innerHTML = sdata.PN;
            document.getElementById("divcall").style.display = "inline-block";
            document.getElementById("num").style.display = "none";
            document.getElementById("shownum").style.display = "inline";
        }
        else {
            document.getElementById("divcall").style.display = "none";
        }
        if (sdata.EMAIL != null && sdata.EMAIL != "") {
            document.getElementById("email").innerHTML = sdata.EMAIL;
            document.getElementById("divemail").style.display = "inline-block";
            document.getElementById("email").style.display = "none";
            document.getElementById("showemail").style.display = "inline";
        }
        else {
            document.getElementById("divemail").style.display = "none";
        }
        if (sdata.SSDESC != null && sdata.SSDESC != "") {
            document.getElementById("divdesc").innerHTML = sdata.SSDESC;
            document.getElementById("divdesc").style.display = "block";
        }
        else {
            document.getElementById("divdesc").style.display = "none";
        }
        if (sdata.OpeningList != null && sdata.OpeningList != "") {
            var arropeninglist = sdata.OpeningList.split(",");
            var strhtml = "<table class=\"opening\"><tr><td colspan=\"3\" style=\"border-width:0px!important;padding-left:0px;\"><b>Opening Hours : </b></td></tr>"
            if (arropeninglist.length > 0) {
                strhtml += "<tr>";
                strhtml += "<th>Day</th>"
                strhtml += "<th>From</th>"
                strhtml += "<th>To</th>"
                strhtml += "</tr>";
            }
            for (var i = 0; i < arropeninglist.length; i++) {
                var olitem = arropeninglist[i];
                if (olitem.indexOf("-") > -1) {
                    var daydata = olitem.split("-");
                    strhtml += "<tr>";
                    strhtml += "<td class=\"day\">" + daydata[0] + "</td>"
                    strhtml += "<td>" + daydata[1].replace(":0am", "am").replace(":00am", "am").replace(":0pm", "pm").replace(":00pm", "pm") + "</td>"
                    strhtml += "<td>" + daydata[2].replace(":0am", "am").replace(":00am", "am").replace(":0pm", "pm").replace(":00pm", "pm") + "</td>"
                    strhtml += "</tr>";
                }
            }
            strhtml += "</table>";
            document.getElementById("divopening").innerHTML = strhtml;
            document.getElementById("divopening").style.display = "inline-block";
        }
        else {
            document.getElementById("divopening").style.display = "none";
        }

        if (sdata.AverageRating != null && sdata.AverageRating != "") {
            document.getElementById("rating").setAttribute("src", sdata.AverageRating);
            document.getElementById("divrating").style.display = "block";
        }
        else {
            document.getElementById("divrating").style.display = "none";
        }

        document.getElementById("spadetail").style.display = "block";
        if ($("#divservices").width() < width) {
            $('.alldivservices').slick({
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                dots: false,
                adaptiveHeight: false,
                centerMode: false,
                variableWidth: true
            });
        }
        window.parent.document.getElementById('ifrmeasyspa').height = document.body.offsetHeight + "px";
    });
    return marker;
}