<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Video_autoplayon.ascx.cs" Inherits="autoplayon_ascx" %>

<script type="text/javascript" src="/Widgets/JS/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="/Widgets/JS/mwheelIntent.js"></script>
<script type="text/javascript" src="/Widgets/JS/jquery.mousewheel.js"></script>

<link rel="stylesheet" href="/Widgets/Video.css" /> 

<div style="width: 750px;height:445px;">
    <div id="DivimageblockMain" style="position: relative;margin-left:8px;width:100%;height:100%;">
        <img alt="1x2" id="imageblockVideoWidget" src="/Widgets/images_common/1x2.gif" />
        <div id="Divimageblock">
            <div id="mainContainer">
                <div id="wrapper">
                </div>
                <div id="adContainer"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var IsMobileCS = '<%=IsMobileCS %>';
    var strImageUrlCS = "<%=strImageUrl %>";
    var fileCS = '';
    var AllAdsStringCS = '<%=AllAdsString %>';
    var streamURLCS = '<%=strFLVPATH %>';
    var formatCS = '<%=strChannelFormat %>';
    var strChannelIDCurrentCS = '<%=strChannelID %>';
    var strChannelIDNextCS = '<%=strChannelIDNext %>';
    var isShowPlayListCS = '<%=isShowPlayList %>';
    var strFLVPATHCS = '<%=strFLVPATH %>';
    var MyFileURLCS = '<%=MyFileURL %>';
    var strChannelNameCS = '<%=strChannelName %>';
    var strChannelIDCS = '<%=strChannelID %>';
    var domainnameCS = '<%=Convert.ToString(HttpContext.Current.Request.Url.Host) %>';
    var lbldescCS = '<%=lbldesc %>';
    var strlightcolor = '<%=strlightcolor %>';
</script>
<script type="text/javascript" src="/Widgets/JS/ima3.js"></script>
<script type="text/javascript" src="/Widgets/JS/PlayVideo.js"></script>
<script type="text/javascript">
    if (strAllowAdnetworkData == "1")
        SetImperssion_AdNetworks();

    CallService();
</script>
