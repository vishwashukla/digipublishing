﻿var AllAdsString = '';
var adstartcount = 0;
var adclickcount = 0;
var VideoBlockerCount = 0;
var adstatus = 1;
var flag = 0;
var count = 0;
var streamURL = '';
var format = '';
var strChannelIDCurrent = '';
var strChannelIDNext = '';

var file = "";

var ADNetworkID = 0;
var adnetwork_name = "";
var tag = "";
var plugin = "";
var adclickplay = "";
var adblocker = "";
var adstrBlockerValue = 0;
var adSameIPStatus = "";
var adSameIPValue = 0;
var adSameIPCount = 0;
var adSameIPDays = 0;
var isShowPlayList = '';
var strFLVPATH = '';
var MyFileURL = '';
var strChannelName = '';
var strChannelID = '';
var domainname = '';
var lbldesc = '';
var ltchannels = '';

var tagarray = '';
var tagXMLString = '';
var CountPriority = 0;


var filePath = "";
var VideoIDCurrent = "";
var VideoNameCurrent = "";
var VideoDescCurrent = "";

var arrayindex = 0;
var playlistitemindex = 0;
var array;
var arrayID;
var xmlDocPlaylist = null;
var startcount = 0;
var jwplayerInstance;
var playListLength = 0;
var videoContent;
var scrollelement;
var scrollapi;
var hasad = true;
var currentindex = 0;
var adDisplayContainer;
var adsLoader;
var contentEndedListener;
var adsManager;

var widgetfolder = "Widgets";
function UpdateColors() {
    $(".currentvideo").css("background-color", "#000000").css("color", strlightcolor);
    $(".currentvideo").mouseenter(function () {
        $(this).css("background-color", strlightcolor).css("color", "#000000");
    }).mouseleave(function () {
        $(this).css("background-color", "#000000").css("color", strlightcolor);
    });

    if (!$(".vids").hasClass("currentvideo"))
        $(".vids").css("background-color", "#000000").css("color", "#ffffff");
    $(".vids").mouseenter(function () {
        $(this).css("background-color", strlightcolor).css("color", "#000000");
    }).mouseleave(function () {
        if (!$(this).hasClass("currentvideo"))
            $(this).css("background-color", "#000000").css("color", "#ffffff");
    });

    $(".jspDrag").mouseenter(function () {
        $(this).css("background-color", strlightcolor);
    }).mouseleave(function () {
        $(this).css("background-color", "#ffffff");
    });
}
$(document).ready(UpdateColors);

function SetFirstItemPath(index) {
    if (xmlDocPlaylist != null) {
        var track = xmlDocPlaylist.getElementsByTagName("item");
        if (track.length > 0 && track.length > index) {
            filePath = track[index].getElementsByTagName("enclosure")[0].getAttribute("url");
            VideoIDCurrent = track[index].getElementsByTagName("guid")[0].childNodes[0].nodeValue;
            VideoNameCurrent = track[index].getElementsByTagName("title")[0].childNodes[0].nodeValue;
            if (track[index].getElementsByTagName("description")[0].childNodes[0] != null)
                VideoDescCurrent = track[index].getElementsByTagName("description")[0].childNodes[0].nodeValue;
            else
                VideoDescCurrent = '';
        }
    }
}

function ReplaceTag(tagarrayreplace, strtoreplace) {
    while (tagarrayreplace.indexOf(strtoreplace) >= 0) {
        tagarrayreplace = tagarrayreplace.replace(strtoreplace, "");
    }
    return tagarrayreplace;
}

function GetStreamURL(chid) {
    var stringURL = document.location.href.split("http://")[1].split("/")[0];
    return streamURLCS = 'http://' + stringURL + '/' + widgetfolder + '/Getplaylist.aspx?Id=' + chid;
}

///////////////////////First Method Called//////////////////////////////////////////
function CallService() {
    AllAdsString = AllAdsStringCS;
    streamURL = streamURLCS;
    format = formatCS;
    strChannelIDCurrent = strChannelIDCurrentCS;
    strChannelIDNext = strChannelIDNextCS;
    isShowPlayList = isShowPlayListCS;
    strFLVPATH = strFLVPATHCS;
    MyFileURL = MyFileURLCS;
    strChannelName = strChannelNameCS;
    strChannelID = strChannelIDCS;
    domainname = domainnameCS;
    lbldesc = lbldescCS;

    if (isShowPlayList == "1" && (format == "flash" || format == "FLASH")) {
        streamURL = GetStreamURL(strChannelIDCurrent);
        strFLVPATH = GetStreamURL(strChannelIDCurrent);
    }

    var streamURLtest = strFLVPATH;
    if (streamURLtest.indexOf("Getplaylist.aspx?id=") > 0) {
        var stringURL = document.location.href.split("http://")[1].split("/")[0];
        streamURLtest = "http://" + stringURL + "/" + streamURLtest.split("http://")[1].split("/")[1] + "/" + streamURLtest.split("http://")[1].split("/")[2];

    }
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            xmlDocPlaylist = xhttp.responseXML;
            playListLength = parseInt(xmlDocPlaylist.getElementsByTagName("item").length);
        }
    }
    xhttp.open("GET", streamURLCS, false);
    xhttp.send();

    playlistitemindex = 0;
    AddPlayList();
    videoContent = document.getElementById('contentElement');
    PlayWidget();
}

///////////////////////Play Widget//////////////////////////////////////////
function PlayWidget() {
    if (AllAdsString != "") {
        if (AllAdsString.split("$#$").length > 0)
            tagXMLString = AllAdsString.split("$#$")[2];
        CountPriority++;
        var splitvalopen = "<priority" + CountPriority + ">";
        var splitvalclose = "</priority" + CountPriority + ">";
        if (tagXMLString.split(splitvalopen)[1] != null)
            tagarray = tagXMLString.split(splitvalopen)[1].split(splitvalclose)[0];
        SearchAllAds(tagarray);
    }
    else {
        ADNetworkID = 0;
        adnetwork_name = "";
        tag = "";
        plugin = "";
        adclickplay = "true";
        hasad = false;
        videoContent.play();
    }
}

function onAdError(adErrorEvent) {
    // Handle the error logging and destroy the AdsManager
    console.log(adErrorEvent.getError());
    adsManager.destroy();
    //videoContent.play();
    SearchForNextAd();
}

function onAdsManagerLoaded(adsManagerLoadedEvent) {
    // Get the ads manager.
    adsManager = adsManagerLoadedEvent.getAdsManager(videoContent);  // See API reference for contentPlayback

    // Add listeners to the required events.
    adsManager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR,onAdError);
    adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED,onContentPauseRequested);
    adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, onContentResumeRequested);
    adsManager.addEventListener(google.ima.AdEvent.Type.COMPLETE, function () {
        adDisplayContainer.destroy();
        AdFinishDIGIP();
        videoContent.play();
    });

    adsManager.addEventListener(google.ima.AdEvent.Type.LOADED, function () {
        AdImpressionCountDIGIP(ADNetworkID);
    });
    adsManager.addEventListener(google.ima.AdEvent.Type.STARTED, function () {
        AdStartDIGIP();
    });
    adsManager.addEventListener(google.ima.AdEvent.Type.CLICK, function () {
        AdClickDIGIP();
        adsManager.resume();
    });
    

    try {
        // Initialize the ads manager. Ad rules playlist will start at this time.
        adsManager.init(720, 445, google.ima.ViewMode.NORMAL);
        // Call start to show ads. Single video and overlay ads will
        // start at this time; this call will be ignored for ad rules, as ad rules
        // ads start when the adsManager is initialized.
        adsManager.start();
    } catch (adError) {
        // An error may be thrown if there was a problem with the VAST response.
    }
}

function onContentPauseRequested() {
    // This function is where you should setup UI for showing ads (e.g.
    // display ad timer countdown, disable seeking, etc.)
    videoContent.removeEventListener('ended', contentEndedListener);
    videoContent.pause();
}

function onContentResumeRequested() {
    // This function is where you should ensure that your UI is ready
    // to play content.
    videoContent.addEventListener('ended', contentEndedListener);
    videoContent.play();
}

function AddPlayList() {
    var track = xmlDocPlaylist.getElementsByTagName("item");
    var startindex = playlistitemindex;
    var strvideo = "<div class='videoContainer'>";
    for (var i = 0; i < track.length; i++) {
        if (i >= startindex) {
            playlistitemindex++;
            var tracktitle = track[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
            var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
            if (tracklocation.indexOf(".mp4") > 0) {
                var strinnerhtml = "<video style='background:#000' id='contentElement' width='540px' ";
                strinnerhtml += "height='445px' controls autoplay onended='LoadNextPlayListItem()' muted='muted'><source id='mp4src' src='" + tracklocation + "' type='video/mp4'></source></video>";
                strvideo += strinnerhtml + "</div>";
                break;
            }
        }
    }
    strvideo += "<div id=\"playlist_container\">"
    for (var i = 0; i < track.length; i++) {
        var tracktitle = track[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
        var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
        var trackdesc = "";
        if (track[i].getElementsByTagName("description")[0].childNodes.length > 0) {
            trackdesc = track[i].getElementsByTagName("description")[0].childNodes[0].nodeValue;
        }
        if (tracklocation.indexOf(".mp4") > 0) {
            strvideo += "<div onmouseover=\"this.style.cursor='pointer'\" onclick=\"LoadSelectedPlayListItem(" + i + ");\""
                + " class=\"vids\" id=\"vids_" + i + "\"><div class=\"ch_info\"><span class=\"title\">" + tracktitle + "</span><br />"
                + "<span class=\"src\">" + trackdesc + "</span></div></div>";
        }
    }

    strvideo += "</div>"
    document.getElementById('wrapper').innerHTML = strvideo;
    scrollelement = $('#playlist_container').jScrollPane();
    scrollapi = scrollelement.data('jsp');
    $('#vids_' + (playlistitemindex - 1)).addClass("currentvideo");
    scrollapi.scrollToElement('#vids_' + (playlistitemindex - 1), true, false);
}

function LoadNextPlayListItem() {
    LoadSelectedPlayListItem(playlistitemindex);
}

function LoadSelectedPlayListItem(index) {
    currentindex = playlistitemindex - 1;
    playlistitemindex = index;
    if (playListLength == playlistitemindex) {
        playlistitemindex = 0;
    }
    if(hasad){
        CountPriority = 0;
        playItem();
        PlayWidget();
    }
    else {
        playItem();
    }
}

function playItem()
{
    videoContent = document.getElementById('contentElement');
    var srcelement = document.getElementById('mp4src');
    videoContent.pause();
    var track = xmlDocPlaylist.getElementsByTagName("item");
    var startindex = playlistitemindex;
    for (var i = 0; i < track.length; i++) {
        if (i >= startindex) {
            playlistitemindex++;
            var tracklocation = track[i].getElementsByTagName("enclosure")[0].getAttribute("url");
            srcelement.src = tracklocation;
            break;
        }
    }
    videoContent.load();
    if (!hasad) {
        videoContent.play();
    }
    $('#vids_' + currentindex).removeClass("currentvideo");
    $('#vids_' + currentindex).css("background-color", "#000000").css("color", "#ffffff");
    $('#vids_' + (playlistitemindex - 1)).addClass("currentvideo");
    scrollapi.scrollToElement('#vids_' + (playlistitemindex - 1), true, false);
    UpdateColors();
}


////////////////If There Are Ads//////////////////////
function SearchAllAds(arrayoftagid) {
    SetFirstItemPath(playlistitemindex);
    var url = escape(window.top.location.href);
    var ArrayAllAds = AllAdsString.split("$#$");
    var indexallads = 0;
    var ArrayAllTags = arrayoftagid.split("^*^");
    var index = 0;
    if (ArrayAllTags.length > 1)
        index = ArrayAllTags.length - 1;

    var n = Math.floor((Math.random() * index) + 0);
    var idandtag = ArrayAllTags[n];
    if (idandtag != null && idandtag != "" && idandtag.indexOf("^^^") > -1) {
        ADNetworkID = idandtag.split("^^^")[0];
        tagMain = idandtag.split("^^^")[1];
        for (var i = 0; i < ArrayAllAds.length; i = i + 10) {
            if (ADNetworkID == ArrayAllAds[i])
                indexallads = i;
        }
    }

    if (idandtag != null && idandtag != "" && idandtag.indexOf("^^^") > -1 && indexallads < ArrayAllAds.length) {

        ADNetworkID = ArrayAllAds[indexallads];
        adnetwork_name = ArrayAllAds[indexallads + 1];
        plugin = ArrayAllAds[indexallads + 3];
        adclickplay = ArrayAllAds[indexallads + 4];
        adblocker = ArrayAllAds[indexallads + 5];
        adstrBlockerValue = parseInt(ArrayAllAds[indexallads + 6]);
        adSameIPStatus = ArrayAllAds[indexallads + 7];
        adSameIPValue = parseInt(ArrayAllAds[indexallads + 8]);
        adSameIPCount = parseInt(ArrayAllAds[indexallads + 9]);
        adSameIPDays = parseInt(ArrayAllAds[indexallads + 15]);

        var NewTimeGuid = "";
        for (var i = 0; i < 8 ; i++)
            NewTimeGuid += Math.floor((Math.random() * 10));
        tag = tagMain;
        tag = tag.replace("[page_url]", url).replace("[PAGE_URL]", url);
        tag = tag.replace("[timestamp]", NewTimeGuid).replace("[TIMESTAMP]", NewTimeGuid).replace("[random]", NewTimeGuid).replace("[RANDOM]", NewTimeGuid);
        tag = tag.replace("[video_id]", VideoIDCurrent).replace("[VIDEO_ID]", VideoIDCurrent);
        tag = tag.replace("[video_title]", VideoNameCurrent).replace("[VIDEO_TITLE]", VideoNameCurrent);
        tag = tag.replace("[video_desc]", VideoDescCurrent).replace("[VIDEO_DESC]", VideoDescCurrent);
        tag = tag.replace("[video_file_url]", encodeURIComponent(filePath)).replace("[VIDEO_FILE_URL]", encodeURIComponent(filePath));
        tag = tag.replace("[w]", "750");
        tag = tag.replace("[h]", "445");
        tag = tag.replace(/^\s+|\s+$/g, '');

        if (plugin == "ova" || plugin == "OVA") {
            adDisplayContainer = new google.ima.AdDisplayContainer(
            document.getElementById('adContainer'), videoContent);
            // Must be done as the result of a user action on mobile
            adDisplayContainer.initialize();

            adsLoader = new google.ima.AdsLoader(adDisplayContainer);

            // Add event listeners
            adsLoader.addEventListener(
                google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
                onAdsManagerLoaded,
                false);
            adsLoader.addEventListener(
                google.ima.AdErrorEvent.Type.AD_ERROR,
                onAdError,
                false);

            // An event listener to tell the SDK that our content video
            // is completed so the SDK can play any post-roll ads.
            contentEndedListener = function () { adsLoader.contentComplete(); LoadSelectedPlayListItem(playlistitemindex); };
            videoContent.onended = contentEndedListener;
            // Request video ads.
            var adsRequest = new google.ima.AdsRequest();
            //adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?' +
            //    'sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&' +
            //    'impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&' +
            //    'cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=';
            //if (tag.contains("test1.xml") || tag.contains("test2.xml") || tag.contains("test3.xml") || tag.contains("test4.xml") 
            //    || tag.contains("test5.xml") || tag.contains("test6.xml") || tag.contains("test7.xml") || tag.contains("test8.xml"))
            if (tag.indexOf("http://") == -1 && tag.indexOf("https://") == -1)
            {
                var stringURL = document.location.href.split("http://")[1].split("/")[0];
                if (tag.startsWith("/")) {
                    adsRequest.adTagUrl = "http://" + stringURL + tag;
                }
                else {
                    adsRequest.adTagUrl = "http://" + stringURL + "/" + tag;
                }
            }
            else {
                adsRequest.adTagUrl = tag;
            }

            // Specify the linear and nonlinear slot sizes. This helps the SDK to
            // select the correct creative if multiple are returned.
            adsRequest.linearAdSlotWidth = 640;
            adsRequest.linearAdSlotHeight = 400;
            adsRequest.nonLinearAdSlotWidth = 640;
            adsRequest.nonLinearAdSlotHeight = 150;

            adsLoader.requestAds(adsRequest);
        }
        else {
            SearchForNextAd();
        }
    }
    else {
        if (CountPriority < 10)
            PlayWidget();
        else {
            CountPriority = 0;
            tagarray = '';
            ADNetworkID = 0;
            adnetwork_name = "";
            tagMain = "";
            plugin = "";
            adclickplay = "true";
            videoContent.play();
        }
    }
}

function SearchForNextAd() {
    var replacestring = ADNetworkID + "^^^" + tagMain + "^*^";
    tagarray = ReplaceTag(tagarray, replacestring);
    adDisplayContainer.destroy();
    SearchAllAds(tagarray);
}

///////////// AD Events TO DB and Click Blocking//////////////////////
function SetImperssion_AdNetworks() {
    var ArrayAllAds = AllAdsStringCS.split("$#$");
    var AdIDs = '';
    for (var i = 0; i < ArrayAllAds.length; i = i + 10)
        AdIDs += ArrayAllAds[i] + ',';
    $.ajax({
        type: "Post",
        async: true,
        url: WebServiceDomain + "Services/AdNetworkReport.asmx/Setimperssion",
        data: "{'AdNetworkIDs':'" + AdIDs + "' ,'type':'ad','REMOTE_ADDR':'" + REMOTE_ADDR + "','domainnameCS':'" + domainnameCS + "','Referer_URL':'" + Referer_URL + "','strAllowAdnetworkData':'" + strAllowAdnetworkData + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) { },
        error: function (err) {
            //alert(err.responseText);
        }
    });
}

function AdStartDIGIP() {
    AdStartCount(ADNetworkID);
}

function AdClickDIGIP() {
    var currentadSameIPCount = adSameIPCount + 1;
    ClickCount1(ADNetworkID, adstatus);
    var el = document.getElementById('DivimageblockMain');
    var img = document.getElementById("imageblockVideoWidget");
    img.style.display = "none";
    el.setAttribute("onclick", null);
    VideoBlockerCount += 1;

    if (adSameIPStatus == "1" && currentadSameIPCount >= adSameIPValue) {
        img.style.display = "block";
        el.setAttribute("onclick", "adclickBlocker()");
    }
    if (adblocker == "1" && VideoBlockerCount >= adstrBlockerValue) {
        img.style.display = "block";
        el.setAttribute("onclick", "adclickBlocker1()");
    }
}

function AdFinishDIGIP() {
    AdFinishCount(ADNetworkID);
    var el = document.getElementById('DivimageblockMain');
    document.getElementById("imageblockVideoWidget").style.display = "none";
    el.onclick = "";
}

function adstartBlocker() {
    adstartcount += 1;
    if (adstartcount >= 1)
        updatervideo(ADNetworkID, adSameIPValue, adstatus);
}

function adclickBlocker() {
    adclickcount += 1;
    if (adclickcount > 1)
        updatervideo(ADNetworkID, adSameIPValue, adstatus);
}

function adclickBlocker1() {
    VideoBlockerCount += 1;
    updatervideo(ADNetworkID, VideoBlockerCount, adstatus);
}

function ClickCount1(id) {
    try {
        if (ISSendGAVideoAdClick == "1") {
            if (typeof _gaq != "undefined") {
                _gaq.push(['_trackEvent', 'VideoAdClick', adnetwork_name, domainname]);
            }
            else if (typeof ga != "undefined") {
                ga('send', 'event', 'VideoAdClick', adnetwork_name, domainname);
            }
        }
    }
    catch (err) { }
    adSameIPCount += 1;
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200)
            adSameIPCount = parseInt(xhttp.responseText.replace(/^\s+|\s+$/g, ''));
    }
    xhttp.open("GET", WebServiceDomain + widgetfolder + "/Widget_Click_Update.aspx?UserAgent=" + UserAgent + "&DomainForGA=" + DomainForGA + "&REMOTE_ADDR=" + REMOTE_ADDR + "&Referer_URL=" + Referer_URL + "&Campaign_Source=" + Campaign_Source + "&WidIDblock=" + id + "&ClickDays=" + adSameIPDays, true);
    xhttp.send();
}

function updatervideo(id, maxval) {
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }

    xhttp.open("GET", WebServiceDomain + widgetfolder + "/Widget_Click_Update.aspx?UserAgent=" + UserAgent + "&DomainForGA=" + DomainForGA + "&REMOTE_ADDR=" + REMOTE_ADDR + "&Referer_URL=" + Referer_URL + "&Campaign_Source=" + Campaign_Source + "&WidIDblock1=" + id + "&maxval=" + maxval, true);
    xhttp.send();
}

function AdStartCount(id) {
    try {
        if (ISSendGAVideoAdStart == "1") {
            if (typeof _gaq != "undefined") {
                _gaq.push(['_trackEvent', 'VideoAdStart', adnetwork_name, domainname]);
            }
            else if (typeof ga != "undefined") {
                ga('send', 'event', 'VideoAdStart', adnetwork_name, domainname);
            }
        }
    }
    catch (err) { }
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", WebServiceDomain + widgetfolder + "/Widget_Click_Update.aspx?adstartid=" + id, true);
    xhttp.send();
}

function AdFinishCount(id) {
    try {
        if (ISSendGAVideoAdFinish == "1") {
            if (typeof _gaq != "undefined") {
                _gaq.push(['_trackEvent', 'VideoAdFinish', adnetwork_name, domainname]);
            }
            else if (typeof ga != "undefined") {
                ga('send', 'event', 'VideoAdFinish', adnetwork_name, domainname);
            }
        }
    }
    catch (err) { }
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", WebServiceDomain + widgetfolder + "/Widget_Click_Update.aspx?adfinishid=" + id, true);
    xhttp.send();
}

function AdImpressionCountDIGIP(id) {
    var el = document.getElementById('DivimageblockMain');
    if (adSameIPStatus == "1" && adSameIPCount >= adSameIPValue) {
        document.getElementById("imageblockVideoWidget").style.display = "block";
        el.setAttribute("onclick", "adstartBlocker()");
    }
    else {
        document.getElementById("imageblockVideoWidget").style.display = "none";
        el.setAttribute("onclick", null);
    }

    if (adblocker == "1" && VideoBlockerCount >= adstrBlockerValue) {
        document.getElementById("imageblockVideoWidget").style.display = "block";
        el.setAttribute("onclick", "adstartBlocker()");
    }

    try {
        if (ISSendGAVideoImpression == "1") {
            if (typeof _gaq != "undefined") {
                _gaq.push(['_trackEvent', 'VideoImpression', adnetwork_name, domainname]);
            }
            else if (typeof ga != "undefined") {
                ga('send', 'event', 'VideoImpression', adnetwork_name, domainname);
            }
        }
    }
    catch (err) { }
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", WebServiceDomain + widgetfolder + "/Widget_Click_Update.aspx?adimpressionid=" + id, true);
    xhttp.send();
}
///////////// AD Events TO DB and Click Blocking//////////////////////