﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class terms : System.Web.UI.Page
{
    public string termFileContents;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Terms";
        string domain = ReadConfig.getDomainUrl().ToLower().Replace("www.", "").Trim();
        if (File.Exists(Server.MapPath(@"~/privacyterm/" + domain.ToLower() + "/term.html")))
            termFileContents = File.ReadAllText(Server.MapPath(@"~/privacyterm/" + domain.ToLower() + "/term.html"));
        else
            termFileContents = "";
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}