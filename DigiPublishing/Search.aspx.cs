﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Search : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["q"] != null && Convert.ToString(Request.QueryString["q"]) != "")
        {
            HtmlMeta metaTag = new HtmlMeta();
            metaTag.Name = "robots";
            metaTag.Content = "noindex,nofollow";
            this.Page.Header.Controls.Add(metaTag);
            string searchText = Request.QueryString["q"].ToString().Trim().Split('#')[0];

            DBHelper dpsite = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
            DBCommand strQuery = new DBCommand();
            strQuery.CommandText = "select SearchResultPageCode,defaultSearch from [brand_settings]";
            strQuery.CommandType = CommandType.Text;
            DataTable dt = dpsite.FillDataTable(strQuery);
            string searhpagecode = utils.formatStringForDBSelect(dt.Rows[0]["SearchResultPageCode"].ToString());
            SearchResult.InnerHtml = searhpagecode.ToString();
            
        }
        else
        {
            if (!IsPostBack && Session["q"] != null)
            {
                string searchText = Session["q"].ToString().Trim();
                TextBox txtSearch = (TextBox)this.Master.FindControl("txtSearch");
                txtSearch.Text = searchText;

                SqlConnection con = new SqlConnection(ReadConfig.GetConnectionString());
                con.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select A.* from BRAND_BLOG_POSTS AS A INNER JOIN BRAND_BLOG_NEW AS B ON B.blog_id = A.posts_blog_id where (A.post_title like '%'+@Search1+'%' OR A.post_description like '%'+@Search2+'%') and A.post_status<>'0' and A.Active_Inactive=1 ORDER BY NEWID()";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                SqlParameter search = new SqlParameter();
                search.ParameterName = "@Search1";
                search.Value = searchText;
                cmd.Parameters.Add(search);

                search = new SqlParameter();
                search.ParameterName = "@Search2";
                search.Value = searchText;
                cmd.Parameters.Add(search);

                SqlDataAdapter adp = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                adp.Fill(dt);

                con.Close();

                StringBuilder SB = new StringBuilder();
                SB.AppendLine("<table><tbody>");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        SB.AppendLine("<tr><td>");
                        string postURL = "";
                        if (Convert.ToString(dr["post_title"]) != "")
                        {
                            string strTitle = utils.formatStringForURLSelect(dr["post_title"].ToString());
                            postURL = "http://" + ReadConfig.getDomainUrl() + "/features/" + utils.GeneratePostTitleForURL(strTitle) + ".aspx";
                            SB.Append("<div class=\"searchtitle lblBlogPostsTitle\"><a href=\"" + postURL + "\">" + strTitle + "</a></div>");
                        }
                        if (Convert.ToString(dr["post_description"]) != "")
                        {
                            string strDesc = utils.formatStringForURLSelect(dr["post_description"].ToString());
                            if (strDesc.Contains("<img"))
                            {
                                var strimage = strDesc.Replace("<img", "^").Split('^')[1].Split('>')[0];
                                string imagesrc = "";
                                if (strimage.Contains("src='"))
                                {
                                    imagesrc = strimage.Replace("src='", "^").Split('^')[1].Split('\'')[0];
                                }
                                else
                                {
                                    imagesrc = strimage.Replace("src=\"", "^").Split('^')[1].Split('"')[0];
                                }
                                int lastindexslash = imagesrc.LastIndexOf('/');
                                string newsrc = imagesrc.Substring(lastindexslash + 1, imagesrc.Length - 1 - lastindexslash);
                                int lastindexdot = newsrc.LastIndexOf('.');
                                string imagealt = newsrc.Substring(0, lastindexdot).Replace("-", " ").Replace("_", " ");
                                SB.Append("<div class=\"searchimg\"><a href=\"" + postURL + "\"><img src=\"" + imagesrc + "\" alt=\"" + imagealt + "\" /></a></div>");
                            }
                            string strDescText = StripHTML(strDesc);
                            if (strDescText.Length > 300)
                            {
                                strDescText = strDescText.Substring(0, 295) + " [...]";
                            }
                            SB.Append("<div class=\"searchdesc\">" + strDescText + "</div>");
                        }
                        SB.AppendLine("</td></tr>");
                        SB.AppendLine("<tr><td style=\"height:15px;\">");
                        SB.AppendLine("</td></tr>");
                    }
                }
                else
                {
                    SB.AppendLine("<tr style=\"border:0px!important;\"><td>");
                    SB.AppendLine("<div style=\"text-align:center;\"><h2>Nothing Found</h2></div>");
                    SB.AppendLine("<div>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</div>");
                    SB.AppendLine("</td></tr>");
                }
                SB.AppendLine("</tbody></table>");
                SearchResult.InnerHtml = SB.ToString();
            }
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["q"] != null && Convert.ToString(Request.QueryString["q"]) != "")
        {
            this.MasterPageFile = "~/BrandTemplates/Search.master";
        }
    }

    private string StripHTML(string input)
    {
        string strReturn = Regex.Replace(input, "<!--.*?-->", String.Empty, RegexOptions.Singleline);
        strReturn = Regex.Replace(strReturn, "<.*?>", String.Empty);
        return strReturn;
        //HtmlGenericControl createDiv = new HtmlGenericControl("DIV");
        //createDiv.InnerHtml = strReturn;
        //return createDiv.InnerText;
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}