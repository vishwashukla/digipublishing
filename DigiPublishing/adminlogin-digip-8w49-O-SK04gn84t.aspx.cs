﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;

public partial class adminlogin_digip_8w49_O_SK04gn84t : System.Web.UI.Page
{
    public static int cntLogin = 0;
    private const string secureCookiesPreName = "__s_";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - login";

        /* Code to make this page non-accessible except by Super Admin*/
        if (Request.Url.Host.ToString() != "localhost")
        {
            if (Request.Url.Host.ToString() != ConfigLoader.GetSAMainURL())                //Commented to run application on local
                Response.Redirect(ConfigLoader.GetErrorPageUrl());
        }
        //Commented to run application on local
        /*******************************************************************************/
                                
        if (!IsPostBack)
        {
            this.imgLogo.Src = "~/Images_Common/1x2.gif";//ReadConfig.LogoPath;
            this.Session["CaptchaImageText"] =utils.GenerateRandomCode();
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        if (Session["CaptchaImageText"] != null)
        {
            if (CodeNumberTextBox.Text == Session["CaptchaImageText"].ToString())
            {
                DBCommand strSql = new DBCommand();
                strSql.CommandText = "SP_Brand_Select_Password_OnRules";
                strSql.CommandType = CommandType.StoredProcedure;
                strSql.AddParameter("@uname", txtUserName.Text.Trim());
                string strPassword = "";
                try
                {
                    DataTable dtUserPassword = dp.FillDataTable(strSql);
                    if(dtUserPassword.Rows.Count>0)
                        strPassword = dtUserPassword.Rows[0][0].ToString();
                }
                catch (Exception)
                {
                    strPassword = "";
                }

                int saltSize = 2;
                Byte[] saltBytes = new byte[saltSize];
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5;
                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
                md5 = new MD5CryptoServiceProvider();
                originalBytes = ASCIIEncoding.Default.GetBytes(this.txtPasswd.Text.Trim());
                encodedBytes = md5.ComputeHash(originalBytes);
                //Convert encoded bytes back to a 'readable' string
                this.txtPasswd.Text = BitConverter.ToString(encodedBytes).Replace("-", "");

                if (strPassword == this.txtPasswd.Text.ToUpper())
                {
                    DBCommand cmd = new DBCommand();
                    cmd.CommandText = "BRAND_LOGIN_SP";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameter("@uname", txtUserName.Text);
                    try
                    {
                        DataTable dt = dp.FillDataTable(cmd);
                        if (dt.Rows.Count > 0)
                        {
                            Session["userid"] = dt.Rows[0]["uid"].ToString();
                            Session["username"] = utils.formatStringForDBSelect(dt.Rows[0]["fullname"].ToString());
                            Session["Email"] = utils.formatStringForDBSelect(dt.Rows[0]["email"].ToString());
                            Session["role"] = dt.Rows[0]["roles"].ToString();
                            
                            if (Session["role"].ToString() == "super admin")
                                Response.Redirect("DigipSuperAdmin/DigipAdminIndex.aspx");
                            else
                                Response.Redirect("OndemandHome.aspx");
                            cntLogin = 0;
                        }
                    }
                    catch (Exception e1)
                    {
                        lblMSg.Text = e1.Message.ToString();
                    }
                }
                else
                {
                    CodeNumberTextBox.Text = "";
                    Session["CaptchaImageText"] = utils.GenerateRandomCode();
                    lblMSg.Text = "Your login ID or Password has been entered incorrectly.";
                    cntLogin++;
                    if (cntLogin >= 3)
                    {
                        sendMail();
                    }
                }
            }
            else
            {
                CodeNumberTextBox.Text = "";
                Session["CaptchaImageText"] = utils.GenerateRandomCode();
                lblMSg.Text = "please enter valid verification code.";
                cntLogin++;
                if (cntLogin >= 3)
                {
                    sendMail();
                }
            }
        }
    }
    public void sendMail()
    {
        try
        {
            SmtpClient smtpClient = new SmtpClient();
            System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            MailAddress toAddress = new MailAddress(ReadConfig.GetEmail());
            string strContent = txtUserName.Text;
            message.From = toAddress;
            message.To.Add(toAddress);
            message.Subject = "Login Alert";

            string strDateTime = DateTime.Now.ToString();
            string str_ipaddress = Request.ServerVariables["REMOTE_ADDR"];
            string strbody = "";
            string stropsystem = stropsystem = utils.GetOS();
            strbody = strbody + "<table width=\"60%\" border=\"0\" align=\"center\">";
            strbody = strbody + "<tr><td colspan=3 align=\"left\"><b>A person is trying to login with the user name: <strong>" + utils.formatStringForDBInsert(txtUserName.Text) + "</strong></b></td></tr>";
            strbody = strbody + "<tr><td>Number of attempts</td><td>:</td><td><strong>" + (cntLogin + 1) + "</strong></td></tr>";
            strbody = strbody + "<tr><td>IpAddress</td><td>:</td><td><strong>" + str_ipaddress + "</strong></td></tr>";
            strbody = strbody + "<tr><td>Date Time</td><td>:</td><td><strong>" + strDateTime + "</strong></td></tr>";
            strbody = strbody + "<tr><td>Browser type</td><td>:</td><td>" + "Type-- <strong>" + Request.Browser.Type + "</strong>, Name--<strong>" + Request.Browser.Browser + "</strong>, Version--<strong>" + Request.Browser.Version + "</strong></td></tr>";
            strbody = strbody + "<tr><td>Operating System</td><td>:</td><td><strong>" + stropsystem + "</strong></td></tr>";
            strbody = strbody + "<tr><td>Snooped</td><td>:</td><td><strong>http://" + ReadConfig.getDomainUrl() + "</strong></td></tr>";
            strbody = strbody + "</table>";

            strContent = strbody;
            message.Body = strContent;
            smtpClient.Send(message);
        }
        catch (Exception ex)
        {
            lblMSg.Text = ex.Message.ToString();
        }
    }

    protected void lnkForgotPwd_Click(object sender, EventArgs e)
    {
        Response.Redirect("forgotPassword.aspx");
    }

    protected override void OnPreInit(EventArgs e)
    {
        if (Request.Url.Host.ToString() == "localhost")
        {
            base.Theme = "simon_template_v8";
        }
        else
        {
            base.Theme = ReadConfig.GetTheme();
        }
        base.OnPreInit(e);
    }
}
