using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Linq;

public partial class SiteMap : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Sitemap and Index";

        lblmessage.Text = "";
        if (!IsPostBack)
        {
            GetBlogs();
            GetBookMarks();
            GetRss();
            GetCalendar();
            showAccFeatures();
        }
    }

    private void GetBlogs()
    {
        try
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_Blogs_Sitemap";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtBlogs = dp.FillDataTable(cmd);
            string domainurl = ReadConfig.getDomainUrl();
            if (Convert.ToString(Request.Url.Host) == "localhost")
            {
                domainurl = Request.Url.Host + ":" + Request.Url.Port;
            }
            if (dtBlogs.Rows.Count > 0)
            {
                StringBuilder strBlog = new StringBuilder();
                foreach (DataRow dr in dtBlogs.Rows)
                {
                    if (Convert.ToString(dr["post_title"]) != "")
                    {
                        string strTitle = utils.formatStringForURLSelect(dr["post_title"].ToString());
                        strBlog.Append("<a href=\"" + "http://" + domainurl + "/features/" + utils.GeneratePostTitleForURL(strTitle) + ".aspx" + "\">" + strTitle + "</a>" + "<br/>");
                    }
                }
                this.lblBlogs.Text = strBlog.ToString().Substring(0, strBlog.Length - 1);
                lblNoBlogs.Visible = false;
            }
            else
            {
                lblNoBlogs.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    private void GetBookMarks()
    {
        try
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_Bookmarks_Sitemap";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtBookmark = dp.FillDataTable(cmd);
            dtBookmark.Columns.Add("url", typeof(string));
            if (dtBookmark.Rows.Count > 0)
            {
                StringBuilder strTitle = new StringBuilder();
                foreach (DataRow dr in dtBookmark.Rows)
                {
                    strTitle.Append("<a href='" + "http://" + ReadConfig.getDomainUrl() + "/links/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dr["BK_Title"]))) + "'" + "" + ">"
                                + utils.formatStringForDBSelect(Convert.ToString(dr["BK_Title"])) + "</a>" + ", ");
                }
                this.lblBookmarks.Text = strTitle.ToString().Substring(0, strTitle.Length - 2);
                lblNoBookmarks.Visible = false;
            }
            else
            {
                lblNoBookmarks.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    private void GetRss()
    {
        try
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_Rss_Sitemap";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtRss = dp.FillDataTable(cmd);
            dtRss.Columns.Add("url", typeof(string));
            if (dtRss.Rows.Count > 0)
            {
                StringBuilder strTitle = new StringBuilder();
                foreach (DataRow dr in dtRss.Rows)
                {
                    strTitle.Append("<a href='" + "http://" + ReadConfig.getDomainUrl() + "/updates/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dr["title"]))) + "'>"
                                + utils.formatStringForDBSelect(Convert.ToString(dr["title"])) + "</a>" + ", ");
                }
                this.lblRss.Text = strTitle.ToString().Substring(0, strTitle.Length - 2);
            }
            else
            {
                divRss.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    private void GetCalendar()
    {
        try 
        {
          string strquery="select Cal_id,title from Brand_Calendar WHERE status<>'0'";
            DataTable dtCal = dp.FillDataTable(strquery);
            dtCal.Columns.Add("url", typeof(string)); 
            if (dtCal.Rows.Count > 0)
            {
                StringBuilder strTitle = new StringBuilder();
                foreach (DataRow dr in dtCal.Rows)
                {
                    strTitle.Append("<a href='" + "http://" + ReadConfig.getDomainUrl() + "/Calendarevents/" + utils.GeneratePostTitleForURL(utils.formatStringForDBSelect(Convert.ToString(dr["title"]))) + "'>"
                                + utils.formatStringForDBSelect(Convert.ToString(dr["title"])) + "</a>" + ", ");
                }
                this.lblCalendar.Text = strTitle.ToString().Substring(0, strTitle.Length - 2);
            }
            else
            {
                divCalendar.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }
    public void showAccFeatures()
    {
        string[] strArrFeatures = ReadConfig.GetFeatures().Split(',');

        //Blog
        if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Blog)))))
            this.divBlogs.Style["display"] = "inline";
        //RSS
        if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.RSS)))))
            this.divRss.Style["display"] = "inline";
        //BookMark
        //if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Bookmark)))))
        //    this.divBookmarks.Style["display"] = "inline";
        //Calendar
        if (strArrFeatures.Contains(Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Calendar)))))
            this.divCalendar.Style["display"] = "inline";
    }
    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}