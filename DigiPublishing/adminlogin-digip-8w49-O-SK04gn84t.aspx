﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="adminlogin-digip-8w49-O-SK04gn84t.aspx.cs" Inherits="adminlogin_digip_8w49_O_SK04gn84t" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
	<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
    <link rel="shortcut icon" href="Images_Common/favicon.ico" type="image/x-icon" />
</head>
<body>
    <!--- BEGIN MAIN BODY --->
    <div id="content-wrapper-holder">
        <div class="top-bkg-content">
        </div>
        <div id="content-wrapper-logon">
            <div class="imageLogo">
                <img id="imgLogo" alt="1x2" src="~/Images_Common/1x2.gif" runat="server" />
            </div>
            <form id="form1" runat="server">
                <div class="details">
                    <asp:Label ID="lblMSg" runat="server" CssClass="messages"></asp:Label>
                    <br />
                    <div id="divLogin" runat="server">
                        <div class="details-entry">
                            Username<br />
                            <asp:TextBox ID="txtUserName" CssClass="textbox" runat="server"></asp:TextBox><br />
                            Password<br />
                            <asp:TextBox ID="txtPasswd" CssClass="textbox" runat="server" TextMode="Password"></asp:TextBox><br />
                            <br />
                            <div id="dvCapcha" runat="server" style="width:100%;">
                                <img id="imgcaptcha" runat="server" src="Captcha.aspx" style="width:100%;" alt="Captcha" />
                                Enter the code shown
                                <asp:TextBox ID="CodeNumberTextBox" CssClass="textbox" runat="server"></asp:TextBox><br />
                            </div>
                            <br />
                            <div style="width:100%;text-align:center;">
                                <asp:Button runat="server" OnClick="btnLogin_Click" ID="btnLogin" Text="Login" CssClass="btnBgClassSA" />
                                <br />
                                <asp:LinkButton ID="lnkForgotPwd" runat="server" Text="Forgot Password?" OnClick="lnkForgotPwd_Click"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="bottom-bkg-content">
        </div>
    </div>
    <!--- END OF MAIN BODY --->
</body>
</html>