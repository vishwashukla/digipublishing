<%@ Page Language="C#" MasterPageFile="~/brandtemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="PrivacyPolicy - Copy.aspx.cs" Inherits="PrivacyPolicy" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
<style type="text/css">
body
{
    font-family:Calibiri;
    font-size:15px;
}
p
{
    margin-top:10px;
}
h3
{
    font-size:20px;
    font-weight:bold;
}
ul
{
    list-style-position: outside;
    list-style-type: disc;
    margin-left:20px;
    margin-top:10px;
}
a
{ 
    color:blue;
}
</style>
<div class="main">
  <h3 style="text-align:center;">Privacy Policy</h3>
  
  <div class="terms">
    <p><strong>Privacy Policy</strong></p>
    <p>This website  provides the 'Service'.</p>
    <p>This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>
    <p>If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. Personal Data is processed in the capacity of a Data Controller, as defined in the General Data Protection Regulation (GDPR). We will not use or share your information with anyone except as described in this Privacy Policy.</p>
    <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at this website, unless otherwise defined in this Privacy Policy.</p>
    <p><strong><br/>Information Collection and Use</strong></p>
    <p>For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to your name, email, phone, and your comments. The information that we collect will be used to contact or identify you.<br>
      </p>
    <p><strong>Log Data</strong></p>
    <p>We want to inform you that whenever you visit our website, we collect information that your browser sends to us that is called Log Data. This Log Data may include information such as your computer&rsquo;s Internet Protocol (&quot;IP&quot;) address, browser version, browser UserAgent, pages of our website Service that you visit, the time and date of your visit, the time spent on pages, GEO location, bandwidth, login frequency, content viewed, ads viewed, content clicks, user interaction and other statistics that may be collected so we can improve our website or our partners services.</p>
    <p><br/>
      <strong>Cookies</strong></p>
    <p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your computer&rsquo;s hard drive.</p>
    <p>Our website uses these &quot;cookies&quot; to collection information and to improve our Service. You have the option to either accept or refuse these cookies, and know when a cookie is being sent to your computer. If you choose to refuse our cookies, you may not be able to use some portions of our Service.</p>
    <p>We employ the use of cookies. By accessing this website, you agreed to use cookies in agreement with this website's Privacy Policy.</p>
    <p>Most interactive websites use cookies to let us retrieve the user&rsquo;s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>
    <p>We may set cookies on your computer to know when you visit our website or other websites.</p>
    <p>If your browser doesn't accept cookies, you may not be able to fully use our website. </p>
    <p>We may allow other companies that show advertisements on our pages to set and access cookies on your computer, this policy does not apply to them.</p>
    <p><strong><br/>
      Service Providers</strong></p>
    <p>We may employ third-party companies and individuals due to the following reasons:</p>
    <p> To facilitate our Service;<br/>
      To provide the Service on our behalf;<br/>
      To perform Service-related services; or<br/>
      To assist us in analyzing how our Service is used.</p>
    <p>We want to inform our Service users that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.<br>
      </p>
    <p><strong>Security</strong></p>
    <p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>
    <p><strong><br/>
      Links to Other Sites</strong></p>
    <p>Our Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>
    <p><strong>Children&rsquo;s Privacy</strong></p>
    <p>Our Services do not address anyone under the age of 18. We do not knowingly collect personal identifiable information from children under 18. In the case we discover that a child under 18 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>
    <p><strong><br/>
      Changes to This Privacy Policy</strong></p>
    <p>We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page. Our privacy policy was last updated on 25 May 2018.</p>
    <p><strong><br/>
      Contact Us</strong></p>
    <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us. This website operates within the laws of England.</p>
  </div>
</div>
</asp:Content>