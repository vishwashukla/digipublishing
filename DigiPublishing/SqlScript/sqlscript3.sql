/******************************************************************************************************************/
/* Start : Database Stored  Procedures*/
/****** Object:  StoredProcedure [dbo].[brand_SearchAndReplace]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[brand_SearchAndReplace]
(
	@SearchStr nvarchar(100),
	@ReplaceStr nvarchar(100)
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @TableName nvarchar(256), @ColumnName nvarchar(128), @SearchStr2 nvarchar(110), @SQL nvarchar(4000), @RCTR int
	SET  @TableName = ''
	SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%','''')
	SET @RCTR = 0

	WHILE @TableName IS NOT NULL
	BEGIN
		SET @ColumnName = ''
		SET @TableName = 
		(
			SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
			FROM 	INFORMATION_SCHEMA.TABLES
			WHERE 		TABLE_TYPE = 'BASE TABLE'
				AND	QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName
				AND	OBJECTPROPERTY(
						OBJECT_ID(
							QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
							 ), 'IsMSShipped'
						       ) = 0
		)

		WHILE (@TableName IS NOT NULL) AND (@ColumnName IS NOT NULL)
		BEGIN
			SET @ColumnName =
			(
				SELECT MIN(QUOTENAME(COLUMN_NAME))
				FROM 	INFORMATION_SCHEMA.COLUMNS
				WHERE 		TABLE_SCHEMA	= PARSENAME(@TableName, 2)
					AND	TABLE_NAME	= PARSENAME(@TableName, 1)
					AND	DATA_TYPE IN ('char', 'varchar', 'nchar', 'nvarchar')
					AND	QUOTENAME(COLUMN_NAME) > @ColumnName
			)
	
			IF @ColumnName IS NOT NULL
			BEGIN
				SET @SQL=	'UPDATE ' + @TableName + 
						' SET ' + @ColumnName 
						+ ' =  REPLACE(' + @ColumnName + ', ' 
						+ QUOTENAME(@SearchStr, '''') + ', ' + QUOTENAME(@ReplaceStr, '''') + 
						') WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2
				EXEC (@SQL)
				SET @RCTR = @RCTR + @@ROWCOUNT
			END
		END	
	END

	SELECT 'Replaced ' + CAST(@RCTR AS varchar) + ' occurence(s)' AS 'Outcome'
END
GO
/****** Object:  StoredProcedure [dbo].[BRAND_FEATURED_SP]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BRAND_FEATURED_SP]            
 @type int=-1,      
 @BrandID int=-1,      
 @UserId int=-1      
AS       
 BEGIN       
  IF(@type=3)  --blogs      
   BEGIN      
    SELECT distinct BRAND_BLOG_POSTS.*,BRAND_BLOG_NEW.blog_team_id,brand_user.fullname,
	(Select Count(*) from dbo.BRAND_POST_COMMENTS where coment_post_id=post_id) AS blog_comments,
	BRAND_BLOG_NEW.blog_id,BRAND_BLOG_NEW.blog_status FROM BRAND_BLOG_POSTS       
	LEFT JOIN brand_user ON BRAND_BLOG_POSTS.c_By=brand_user.uid        
	LEFT JOIN BRAND_BLOG_NEW ON BRAND_BLOG_POSTS.posts_blog_id=BRAND_BLOG_NEW.blog_id      
	WHERE BRAND_BLOG_POSTS.post_status=1 and Active_Inactive = 1  
   END   
  IF(@type=5)  --bookmark      
   BEGIN      
    SELECT distinct brand_bookmark.*,brand_user.fullname,    
    CASE WHEN LEN(BK_Title)>40 THEN SUBSTRING(BK_Title,1,40)+'...' ELSE BK_Title END AS BKTitle
	,isnull(nofollow,0) as nofollow
	,isnull(noindex,0) as noindex
	,isnull(noopener,0) as noopener
	FROM brand_bookmark       
    LEFT JOIN brand_user ON brand_bookmark.C_By=brand_user.uid       
    WHERE  brand_bookmark.Status=1          
   END      
  IF(@type=10)  --RSS      
   BEGIN      
    SELECT distinct Brand_Rss.*,brand_user.fullname,
    CASE WHEN LEN(Brand_Rss.title)>40 THEN SUBSTRING(Brand_Rss.title,1,40)+'...' ELSE Brand_Rss.title END AS RSSTitle FROM Brand_Rss       
    LEFT JOIN brand_user ON Brand_Rss.C_By=brand_user.uid
   END    
 END
GO
/****** Object:  StoredProcedure [dbo].[SP_BRAND_UPDATE_USER]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_BRAND_UPDATE_USER]      
 @pwd nvarchar(50),@email nvarchar(max),@header_text nvarchar(max),
 @fullname nvarchar(50),@title nvarchar(50),
 @ALTER_by int,@roles nvarchar(50),           
 @uid int=-1,@Permissions varchar(max)=''      
AS               
    
 IF(@uid=-1)              
  BEGIN      
   DECLARE @ID int      
   INSERT INTO brand_user              
    ([password],[email],[header_text],[fullname],[title],[c_by],[roles])      
   VALUES              
    (@pwd,@email,@header_text,@fullname,@title,@ALTER_by,@roles);      
   SET @ID=(Select Scope_Identity());      
            
   IF @Permissions<>''      
   BEGIN      
    DECLARE @Pos1 BIGINT,      
      @OldPos1 BIGINT=1,      
      @CSVList1 Varchar(max) =@Permissions      
      
    SET  @Pos1= CHARINDEX(',', @CSVList1, @OldPos1)      
      
    WHILE   @Pos1 < LEN(@CSVList1)      
    BEGIN      
     SET  @Pos1 = CHARINDEX(',', @CSVList1, @OldPos1)      
     INSERT INTO brand_UserPermissions VALUES(LTRIM(RTRIM(SUBSTRING(@CSVList1, @OldPos1, @Pos1 - @OldPos1))),@ID)      
     SET  @OldPos1 = @Pos1 + 1      
    END      
   END      
   SELECT @ID;      
  END      
      
 ELSE          
  BEGIN        
   UPDATE brand_user              
   SET                   
    [password]=@pwd,[email]=@email,[header_text]=@header_text,[fullname]=@fullname,[title]=@title,[m_by]=@ALTER_by,[roles]=@roles
    ,[m_date]=getdate() WHERE [uid]=@uid;       
       
   IF @Permissions<>''      
   BEGIN      
    DELETE brand_UserPermissions WHERE UID=@uid;      
    DECLARE @Pos2 BIGINT,      
      @OldPos2 BIGINT=1,      
      @CSVList2 Varchar(max) =@Permissions      
      
    SET  @Pos2= CHARINDEX(',', @CSVList2, @OldPos2)      
      
    WHILE   @Pos2 < LEN(@CSVList2)      
    BEGIN      
     SET  @Pos2 = CHARINDEX(',', @CSVList2, @OldPos2)      
     INSERT INTO brand_UserPermissions VALUES(LTRIM(RTRIM(SUBSTRING(@CSVList2, @OldPos2, @Pos2 - @OldPos2))),@uid)      
     SET  @OldPos2 = @Pos2 + 1      
    END      
   END      
  END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Update_RSSFeeds]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE SP_Brand_Update_RSSFeeds  
@title nvarchar(200),  
@link nvarchar(max),  
@rssTags nvarchar(max),  
@m_by int,  
@feed_id int=-1  
AS  
BEGIN  
 IF @feed_id <>-1  
 BEGIN  
  DECLARE @Count int=0  
  select @Count=COUNT(*) from Brand_Rss where REPLACE(Title,'&nbsp;','')=@title AND feed_id<>@feed_id  
  IF @Count>0  
   BEGIN  
    SELECT 'YES' AS IsExist  
   END  
  ELSE  
   BEGIN  
    UPDATE [Brand_Rss]  
      SET [title]=@title  
      ,[link]=@link  
      ,[rssTags]=@rssTags  
      ,[m_by]=@m_by  
      ,[m_date] = getdate()  
      WHERE feed_id=@feed_id  
        
    SELECT 'NO' AS IsExist  
   END   
 END   
END  
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Top30_BookMark]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Top30_BookMark]     
AS       
Begin   
  DECLARE @IsRandom INT;
  DECLARE @TopNumber INT;   
  select  @IsRandom=isnull(RandomiseLinksInSummary,0),@TopNumber=case when isnull(NumberOfLinksInSummary,0)>0 then NumberOfLinksInSummary else 30 end FROM brand_settings
	IF @IsRandom=1
		BEGIN
			  Select top (@TopNumber) Id,BK_Title,BK_URL,c_by,Blocker,BlockerValue,SameIP_Status,SameIP_Value
			  ,isnull(nofollow,0) as nofollow
			  ,isnull(noindex,0) as noindex
			  ,isnull(noopener,0) as noopener
			  ,ISNULL(noreferrer,0) as noreferrer
			  from brand_bookmark           
			  WHERE Status='True' AND BK_Status<>'0' order by NEWID()
		END
	ELSE
		BEGIN
			 Select top (@TopNumber) Id,BK_Title,BK_URL,c_by,Blocker,BlockerValue,SameIP_Status,SameIP_Value
			  ,isnull(nofollow,0) as nofollow
			  ,isnull(noindex,0) as noindex
			  ,isnull(noopener,0) as noopener
			  ,ISNULL(noreferrer,0) as noreferrer
			  from brand_bookmark           
			  WHERE Status='True' AND BK_Status<>'0' order by bk_sortorder desc
		END
end
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Top10_Rss]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Top10_Rss]    
AS          
BEGIN          
select feed_id,CASE WHEN LEN(Brand_Rss.title)>30 THEN SUBSTRING(Brand_Rss.title,1,30)+'...' ELSE Brand_Rss.title END AS title,      
description,link,Brand_Rss.rssTags,Brand_Rss.c_by from Brand_Rss         
left join dbo.brand_user on Brand_Rss.c_by = brand_user.uid         
where feed_status<>'0'      
order by Brand_Rss.sortorder DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Top1_Feed_Brand_Rss]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Top1_Feed_Brand_Rss]          
@title nvarchar(200)          
AS          
BEGIN          
select TOP 1 * from Brand_Rss           
WHERE LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(title,'&','and'),'-',' '),'|',' '),':',' '),'.',' '),'(',''),')',''),'   ','  '),'  ',' ')) LIKE '%' + LOWER(REPLACE(@title,'&','and')) + '%'            
AND feed_status<>'0'          
END 
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Top1_BRAND_BLOG_POSTS]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Top1_BRAND_BLOG_POSTS]    
@PostId int    
AS    
BEGIN    
SELECT  top 1 post_id,post_title,fullname,post_description,
blog_status,blog_id,blog_team_id,blog_title,brand_user.uid, Post_QLink_Id,    
BRAND_BLOG_POSTS.c_date as CreatedOn,BRAND_BLOG_POSTS.c_by,post_tags,     
posts_blog_id,post_tags,SortOrder,post_Latitude,post_Longitude     
FROM BRAND_BLOG_POSTS     
LEFT JOIN BRAND_BLOG_NEW ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id     
left join brand_user on BRAND_BLOG_POSTS.c_by=brand_user.uid      
where post_status<>'0' AND post_id=@PostId and BRAND_BLOG_POSTS.Active_Inactive=1 ORDER BY CreatedOn DESC    
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Top1_Brand_AppList_OnPosition]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Top1_Brand_AppList_OnPosition]
@Position varchar(200),
@Country nvarchar(max),
@Browser varchar(max)
AS
BEGIN
SELECT TOP 1 id,ApplicationName,CodeBox,country,Browser,Referal,Not_Referal
,Blocker,SameIP_Status,SameIP_Value,BlockerValue FROM Brand_AppList 
WHERE status=1 and Position =@Position and IsRandom=1 and 
(Country  like '%' + @Country + '%' or Country='' or Country is null) 
and Browser like '%' + @Browser + '%' ORDER BY NEWID()
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Top1_Brand_AppList]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Top1_Brand_AppList]  
@Country nvarchar(max),  
@Browser varchar(max)  
AS  
BEGIN  
SELECT id,ApplicationName,CodeBox,Country,Browser,Referal,Not_Referal,
Blocker,SameIP_Status,SameIP_Value FROM Brand_AppList 
WHERE status=1 and (Position ='topbanner' OR Position = 'Top_not_user') and 
IsRandom=1 and (Country  like '%' + @Country + '%' or Country='' 
or Country is null) and Browser like '%' + @Browser + '%' 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Rss_Sitemap]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_Brand_Select_Rss_Sitemap]
AS
BEGIN
	select feed_id,title from Brand_Rss
    WHERE feed_status<>'0' ORDER BY SortOrder DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Rss]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Rss]          
AS          
BEGIN          
select * FROM Brand_Rss where feed_status<>'0' ORDER BY SortOrder DESC  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Features_For_RSS_Feed]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Features_For_RSS_Feed]  
AS    
BEGIN    
SELECT post_title,post_description,blog_title,BRAND_BLOG_POSTS.c_date as CreatedOn     
FROM BRAND_BLOG_POSTS     
LEFT JOIN BRAND_BLOG_NEW ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id       
where post_status<>'0' and BRAND_BLOG_POSTS.Active_Inactive=1 ORDER BY CreatedOn DESC    
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Features]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_Brand_Select_Features]
AS
BEGIN
select features,defaultfeature,defaultfeatureType from brand_settings
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Default_Feature]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Default_Feature]
AS
BEGIN
select defaultfeature,defaultfeatureType,defaultfeatureMulti,SearchKeyword from brand_settings
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_BrandRss_OnFeedId]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_Brand_Select_BrandRss_OnFeedId]
@FeedId  int
AS
BEGIN
select feed_id,Brand_Rss.title,description,link,c_by from Brand_Rss 
where feed_id=@FeedId
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_BrandAppList_OnPosition]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_BrandAppList_OnPosition]  
@Country nvarchar(max),  
@Browser varchar(20),
@Position varchar(30)
AS  
BEGIN  
SELECT id,ApplicationName,CodeBox,Country,Browser,Referal,Not_Referal,Blocker,SameIP_Status,SameIP_Value     
FROM Brand_AppList WHERE status=1 and Position =@Position and (','+Country+','  like '%,' + @Country + ',%' or Country='' or Country is null)     
and (','+Browser+',' like '%,' + @Browser + ',%' OR Browser like '%0%')    
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_BRAND_BLOG_POSTS]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_BRAND_BLOG_POSTS]    
AS    
BEGIN    
   SELECT post_title,post_id,post_latitude,post_latitude,post_longitude FROM BRAND_BLOG_POSTS  
   WHERE Active_Inactive=1  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Brand_AppList]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Brand_AppList]
@Country nvarchar(max),
@Browser varchar(max)
AS
BEGIN
SELECT id,ApplicationName,CodeBox,Country,Browser,Referal,Not_Referal,Blocker,SameIP_Status,SameIP_Value FROM Brand_AppList WHERE status=1 and (Position ='topbanner' OR Position = 'Top_not_user') and 
(Country  like '%' + @Country + '%' or Country='' or Country is null) and Browser like '%' + @Browser + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Bookmarks_Sitemap]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_Brand_Select_Bookmarks_Sitemap]
AS
BEGIN
	select Id,BK_Title 
	,Blocker,BlockerValue,SameIP_Status,SameIP_Value
	from brand_bookmark
	WHERE Status='True' AND BK_Status<>'0' order by BK_SortOrder desc
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Blogs_Sitemap]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_Brand_Select_Blogs_Sitemap]
AS
BEGIN
	Select BRAND_BLOG_POSTS.post_id,
	BRAND_BLOG_POSTS.post_title from BRAND_BLOG_NEW 
	Left Join BRAND_BLOG_POSTS 
	on BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id
	where BRAND_BLOG_POSTS.Active_Inactive=1 
	ORDER BY  BRAND_BLOG_POSTS.SortOrder DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Blog_N_Posts]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Brand_Select_Blog_N_Posts]    
AS      
BEGIN      
SELECT CASE WHEN LEN(blog_title)>50 THEN SUBSTRING(blog_title,1,50)+'...' ELSE blog_title END AS blog_title,blog_id,post_id,    
CASE WHEN LEN(post_title)>50 THEN SUBSTRING(post_title,1,50)+'...' ELSE post_title END AS post_title,REPLACE(post_description,'<ul>','<ul style=''list-style-type:square;margin-left: 24px; margin-right: 0px;''>') AS post_description    
 FROM BRAND_BLOG_NEW LEFT OUTER JOIN BRAND_BLOG_POSTS ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id       
 LEFT JOIN  brand_user ON BRAND_BLOG_POSTS.c_by = brand_user.uid       
 WHERE blog_id=(Select  TOP 1 blog_id from BRAND_BLOG_NEW     
left join  brand_user on BRAND_BLOG_NEW.c_by = brand_user.uid       
 Where BRAND_BLOG_NEW.blog_status <>2)     
 and post_status<>'0' AND blog_subchannel_id=1 and BRAND_BLOG_POSTS.Active_Inactive=1    
 ORDER BY SortOrder DESC     
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_InsertUpdate_BookMark]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_Brand_InsertUpdate_BookMark]    
@BK_Title nvarchar(500),      
@BK_URL nvarchar(550),     
@BK_Tags nvarchar(max)=null,    
@BK_Desc nvarchar(max),   
@c_by int,      
@id int =-1,
@IsGridEdit int =-1,
@nofollow int=0,
@noindex int=0,
@noopener int=0,
@noreferrer int=0,
@Blocker int=0,
@BlockerValue int=0,
@SameIPStatus int=0,
@SameIPValue int=0
AS      
BEGIN      
 DECLARE @Count  int=0      
 DECLARE @MSG varchar(100)      
 DECLARE @Action_Done bit=0      
 IF @id<>-1      
  BEGIN      
   SELECT @Count=COUNT(id) FROM brand_bookmark WHERE BK_Title=@BK_Title AND Status=1 AND id<>@id      
   IF @Count>0      
    BEGIN      
     SET @MSG='Bookmark title already exists,please enter another title.'      
    END      
   ELSE         
    BEGIN      
		IF @IsGridEdit=1
			BEGIN   
				update dbo.brand_bookmark     
				set BK_Title=@BK_Title,    
				BK_URL=@BK_URL,    
				BK_Desc=@BK_Desc,  
				m_by=@c_by,
				nofollow=@nofollow,
				noindex=@noindex,
				noopener=@noopener,
				noreferrer=@noreferrer,
				Blocker=@Blocker,
				BlockerValue=@BlockerValue,
				SameIP_Status=@SameIPStatus,
				SameIP_Value=@SameIPValue     
				WHERE id=@id      
				SET @Action_Done=1      
				SET @MSG='Bookmark updated successfully.'  
			END
		ELSE
			BEGIN    
				update dbo.brand_bookmark     
				set BK_Title=@BK_Title,    
				BK_URL=@BK_URL,    
				BK_Tags=@BK_Tags,    
				BK_Desc=@BK_Desc,       
				m_by=@c_by,
				nofollow=@nofollow,
				noindex=@noindex,
				noopener=@noopener,
				noreferrer=@noreferrer,
				Blocker=@Blocker,
				BlockerValue=@BlockerValue,
				SameIP_Status=@SameIPStatus,
				SameIP_Value=@SameIPValue    
				WHERE id=@id      
				SET @Action_Done=1      
				SET @MSG='Bookmark updated successfully.'  
			END    
    END        
  END      
 ELSE      
  BEGIN      
   SELECT @Count=COUNT(id) FROM brand_bookmark WHERE BK_Title=@BK_Title AND Status=1    
   IF @Count>0      
    BEGIN      
     SET @MSG='Bookmark title already exists,please enter another title.'      
    END      
   ELSE      
    BEGIN      
    insert into dbo.brand_bookmark(BK_Title, BK_URL, BK_Tags, BK_Desc, c_by, Status,c_date,BK_SortOrder,nofollow,noindex,noopener,noreferrer)    
    VALUES(@BK_Title,@BK_URL,@BK_Tags,@BK_Desc,@c_by,1,getdate(),(SELECT ISNULL(MAX(BK_SortOrder),0)+1 FROM brand_bookmark),@nofollow,@noindex,@noopener,@noreferrer)      
    SET @Action_Done=1      
    SET @MSG='Bookmark added successfully.'      
    SET @id= (Select Scope_Identity())      
    END      
  END      
 SELECT @MSG AS MSG,@Action_Done AS Action,@id AS ID    
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_InsertUpdate_AppSettings]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_Brand_InsertUpdate_AppSettings]  
@name nvarchar(200),  
@description nvarchar(max),  
@c_by int, 
@MetaType int =0, 
@id int =-1  
AS  
BEGIN  
 DECLARE @Count  int=0  
 DECLARE @MSG varchar(100)  
 DECLARE @Action_Done bit=0  
 IF @id<>-1  
  BEGIN  
   SELECT @Count=COUNT(id) FROM Brand_AppSettings WHERE name=@name AND IsDelete<>'True' AND id<>@id  
   IF @Count>0  
    BEGIN  
     SET @MSG='Name already exists,please enter another name.'  
    END  
   ELSE  
    BEGIN  
     UPDATE Brand_AppSettings SET   
     name=@name,description=@description,MetaType=@MetaType,m_by=@c_by,m_date =getdate()  
     WHERE id=@id  
     SET @Action_Done=1  
     SET @MSG='Updated successfully.'  
    END  
  END  
 ELSE  
  BEGIN  
   SELECT @Count=COUNT(id) FROM Brand_AppSettings WHERE name=@name AND IsDelete<>'True'  
   IF @Count>0  
    BEGIN  
     SET @MSG='Name already exists,please enter another name.'  
    END  
   ELSE  
    BEGIN  
     INSERT INTO Brand_AppSettings(name,description,c_by,type,MetaType)  
     VALUES(@name,@description,@c_by,1,@MetaType)  
     SET @Action_Done=1  
     SET @MSG='Added successfully.'  
    END  
  END  
 SELECT @MSG,@Action_Done  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Insert_RSSFeeds]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE SP_Brand_Insert_RSSFeeds  
@title nvarchar(200),  
@link nvarchar(max),  
@rssTags nvarchar(max),  
@c_by int
AS  
BEGIN  
 DECLARE @Count int=0  
 select @Count=COUNT(*) from Brand_Rss where REPLACE(Title,'&nbsp;','')=@title
 IF @Count>0  
  BEGIN  
   SELECT 'YES' AS IsExist,0 AS ID  
  END  
 ELSE  
  BEGIN  
   INSERT INTO [Brand_Rss]  
           ([title]  
           ,[link]  
           ,[rssTags]  
           ,[c_by]
           ,[SortOrder])  
   VALUES  
           (@title  
           ,@link  
           ,@rssTags  
           ,@c_by
           ,(SELECT ISNULL(MAX(SORTORDER),0)+1 FROM BRAND_RSS))  
             
   SELECT 'NO' AS IsExist,SCOPE_IDENTITY() AS ID  
  END    
END  
GO
/****** Object:  StoredProcedure [dbo].[SP_Brand_Count]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_Brand_Count]  
@email NVARCHAR(150)  
AS  
BEGIN  
 SELECT 
 (SELECT ApplicationName = ISNULL(Stuff((SELECT ',' +ApplicationName AS [text()] FROM (SELECT ApplicationName FROM Brand_AppList WHERE status=1) x For XML PATH ('')),1,1,''),'')) AS ApplicationName
,(SELECT count(*) AS COUNTING FROM brand_user WHERE (roles <> 'super admin') AND (email <> @email) AND status=1) AS UserCount
,(SELECT count(*) AS COUNTING  FROM BRAND_BLOG_POSTS INNER JOIN brand_user ON BRAND_BLOG_POSTS.C_By=brand_user.uid
INNER JOIN BRAND_BLOG_NEW ON BRAND_BLOG_POSTS.posts_blog_id=BRAND_BLOG_NEW.blog_id WHERE brand_user.status = 1 AND blog_status = 3) AS BlogCount
,(SELECT count(*) AS COUNTING FROM Brand_Rss INNER JOIN brand_user ON Brand_Rss.C_By=brand_user.uid WHERE brand_user.status = 1) AS RssCount
END
GO
/****** Object:  StoredProcedure [dbo].[sp_AlertTemplate]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_AlertTemplate] 
	@temp_title varchar(100) = ''
AS
BEGIN
if(@temp_title <> '')
	begin
		SELECT * FROM brand_alerttemplate WHERE temp_title=@temp_title order by temp_title
	end   
	else
		begin
			SELECT * FROM brand_alerttemplate ORDER BY temp_title
		end
END
GO
/****** Object:  StoredProcedure [dbo].[brand_SelectBookmark]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[brand_SelectBookmark]   
as       
BEGIN     
  select ID,BK_Title,BK_Desc,BK_URL,BK_Tags,Blocker,BlockerValue,SameIP_Status,SameIP_Value,
  CASE WHEN LEN(REPLACE(BK_SortOrder,' ',''))=0 THEN 0 ELSE ISNULL(BK_SortOrder,0) END AS BK_SortOrder,
  CASE WHEN ISNULL(nofollow,0)=0 THEN 'false' ELSE 'true' END as nofollow,
  CASE WHEN ISNULL(noindex,0)=0 THEN 'false' ELSE 'true' END as noindex,
  CASE WHEN ISNULL(noopener,0)=0 THEN 'false' ELSE 'true' END as noopener,
  CASE WHEN ISNULL(noreferrer,0)=0 THEN 'false' ELSE 'true' END as noreferrer
  from brand_bookmark             
  WHERE Status='True' AND BK_Status<>'0' order by BK_SortOrder desc    
END
GO
/****** Object:  StoredProcedure [dbo].[BRAND_SEL_USER_SP]    Script Date: 02/12/2015 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BRAND_SEL_USER_SP]        
 @roles nvarchar(20) = 'All',        
 @uid int=-1,        
 @status nvarchar(20)='0'        
AS        
BEGIN  
 IF(@uid=-1)        
 BEGIN        
  IF(@roles<>'All')        
   SELECT * FROM dbo.brand_user where roles<>'super admin' and uid <> '0'        
  ELSE         
  BEGIN        
   IF(@status='0')        
    SELECT * FROM dbo.brand_user        
   ELSE        
    SELECT uid,status,roles,email,c_date,user_ip,email,user_ip,        
    (select TOP 1 CASE WHEN uname IS NULL OR uname ='' THEN fullname ELSE uname END from brand_user where uid =A.c_by) AS CreatedBy,        
    (select TOP 1 CASE WHEN uname IS NULL OR uname ='' THEN fullname ELSE uname END from brand_user where uid =A.m_by) AS ModifiedBy,      
    CASE WHEN uname IS NULL OR uname ='' THEN fullname ELSE uname END AS username,      
    (SELECT count(*) FROM brand_user WHERE (roles <> 'super admin') AND (email <> 'GUEST') AND status=1) AS COUNTING      
    FROM dbo.brand_user A where [status]=@status        
  END        
 END        
 ELSE IF(@uid=-2)        
  SELECT MAX([uid])as uid FROM dbo.brand_user        
 ELSE          
  SELECT *,
  (SELECT AP_ID = ISNULL(Stuff((SELECT ',' + convert(varchar,AP_ID) AS [text()] FROM        
  (SELECT AP_ID FROM brand_UserPermissions where UID=A.uid) x For XML PATH ('')),1,1,''),'')) AS UserPermissions,        
  (SELECT AP_ID = ISNULL(Stuff((SELECT ',' + convert(varchar,AP_ID) AS [text()] FROM        
  (SELECT AP_ID FROM [ondemand.imello.com].dbo.brand_SuperAdminPermissions where UID=A.uid) x For XML PATH ('')),1,1,''),'')) AS SuperAdminPermissions        
  FROM dbo.brand_user A where [uid]=@uid and [status]= 1  
END
GO
/****** Object:  StoredProcedure [dbo].[BRAND_Is_Apply_ShieldSquare_New]    Script Date: 20/04/2017 07:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BRAND_Is_Apply_ShieldSquare_New]      
@WidgetName varchar(20)          
AS              
BEGIN        
	Select ID FROM brand_AppList WHERE ApplicationName=@WidgetName  
END
/*GO*/

GO

/****** Object:  StoredProcedure [dbo].[SP_Brand_Select_Links]    Script Date: 8/28/2018 4:17:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Brand_Select_Links]     
AS       
Begin   
  DECLARE @IsRandom INT;
  DECLARE @TopNumber INT;   
  select  @IsRandom=isnull(RandomiseLinksInLink,0),@TopNumber=case when NumberOfLinksinlink>0 then NumberOfLinksinlink else (SELECT COUNT(Id) FROM  brand_bookmark WHERE Status='True' AND BK_Status<>'0') end FROM brand_settings
	IF @IsRandom=1
		BEGIN
			  Select * into #temp_bookmark from (select Id,BK_URL,CASE WHEN LEN(ISNULL(BK_Title,''))> 50 THEN SUBSTRING(BK_Title,1,50)+'..' ELSE BK_Title END AS BK_Title,
				CASE WHEN LEN(ISNULL(BK_Desc,''))> 1000 THEN SUBSTRING(BK_Desc,1,1000)+'..' ELSE BK_Desc END AS BK_Desc,
				isnull(nofollow,0) as nofollow,
				isnull(noindex,0) as noindex,
				isnull(noopener,0) as noopener,
				ISNULL(noreferrer,0) as noreferrer,
				Blocker,BlockerValue,SameIP_Status,SameIP_Value
				from brand_bookmark WHERE Status='True' AND BK_Status<>'0') T	
			  
			  select top (@TopNumber) * from #temp_bookmark order by NEWID(); 
			  
			  Drop table #temp_bookmark;
		END
	ELSE
		BEGIN
			select top (@TopNumber) Id,BK_URL,CASE WHEN LEN(ISNULL(BK_Title,''))> 50 THEN SUBSTRING(BK_Title,1,50)+'..' ELSE BK_Title END AS BK_Title,
				CASE WHEN LEN(ISNULL(BK_Desc,''))> 1000 THEN SUBSTRING(BK_Desc,1,1000)+'..' ELSE BK_Desc END AS BK_Desc,
				isnull(nofollow,0) as nofollow,
				isnull(noindex,0) as noindex,
				isnull(noopener,0) as noopener,
				ISNULL(noreferrer,0) as noreferrer,
				Blocker,BlockerValue,SameIP_Status,SameIP_Value
				from brand_bookmark WHERE Status='True' AND BK_Status<>'0' order by BK_SortOrder desc	
		END
end
GO
/* End : Database Stored  Procedures */
/******************************************************************************************************************/