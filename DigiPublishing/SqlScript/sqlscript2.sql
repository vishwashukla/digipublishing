/******************************************************************************************************************/
/* Start : Database Functions*/
/****** Object:  UserDefinedFunction [dbo].[fnCountCharactersInString]    Script Date: 02/12/2015 07:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[fnCountCharactersInString] 
(
    @StringToTest nvarchar(max),
    @CharactersToCount nvarchar(max)
)
RETURNS int
AS

BEGIN

    DECLARE @EditableCharacterCount int,
            @Looper int

    SELECT @EditableCharacterCount = 0,
           @Looper = 1

    WHILE @Looper <= LEN(@StringToTest)

        SELECT @EditableCharacterCount = @EditableCharacterCount +
               CASE
                   WHEN CHARINDEX (SUBSTRING (@StringToTest, @Looper, 1), @CharactersToCount) > 0
                       THEN 1
                   ELSE 0
               END,
               @Looper = @Looper + 1

    RETURN @EditableCharacterCount
End
GO
/****** Object:  UserDefinedFunction [dbo].[DelimitedToTable]    Script Date: 02/12/2015 07:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[DelimitedToTable](@DelimitedString nvarchar(max), @Delimiter nvarchar(32))
RETURNS @Values TABLE
(
  ident int not null identity primary key clustered,
  theValue nvarchar(4000)
)
AS
BEGIN
  DECLARE @e int, @i int, @Count int

  SET @e = 1
  SET @i = 1
  SET @DelimitedString = @DelimitedString + @Delimiter
  SET @Count = (SELECT dbo.fnCountCharactersInString(@DelimitedString, @Delimiter))

  WHILE (@e <> 0 AND @i <= @Count)
  BEGIN
    SET @e = charindex(@Delimiter, @DelimitedString)
    IF (@e <> 0)
    BEGIN
      INSERT INTO @Values(theValue)
      SELECT substring(@DelimitedString, 1, @e -1)
      SET @DelimitedString = right(@DelimitedString, len(@DelimitedString) -@e)
    END
    SET @i = @i +1
  END
  RETURN
END
GO
/* End : Database Functions */
/******************************************************************************************************************/