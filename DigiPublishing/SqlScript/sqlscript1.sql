/******************************************************************************************************************/
/* Start : Tables */
/****** Object:  Table [dbo].[brand_UserPermissions]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[brand_UserPermissions](
	[AP_ID] [int] NULL,
	[UID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[brand_user]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[brand_user](
	[uid] [int] IDENTITY(1,1) NOT NULL,
	[uname] [nvarchar](max) NULL,
	[password] [nvarchar](50) NOT NULL,
	[email] [nvarchar](max) NOT NULL,
	[roles] [nvarchar](50) NULL,
	[rating] [int] NULL,
	[status] [bit] NOT NULL,
	[fullname] [nvarchar](max) NULL,
	[title] [nvarchar](50) NULL,
	[c_date] [datetime] NOT NULL,
	[c_by] [int] NULL,
	[m_by] [int] NULL,
	[m_date] [datetime] NULL,
	[header_text] [nvarchar](max) NULL,
	[user_ip] [nvarchar](50) NULL,
 CONSTRAINT [PK_brand_user] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:InActive;1:Active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'brand_user', @level2type=N'COLUMN',@level2name=N'status'
GO
/****** Object:  Table [dbo].[Brand_SuperAdmin_Login]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Brand_SuperAdmin_Login](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoginGUID] [varchar](200) NULL,
 CONSTRAINT [PK_Brand_SuperAdmin_Login] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[brand_settings]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[brand_settings](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[features] [varchar](200) NULL,
	[featuredComp] [varchar](200) NULL,
	[defaultfeatureType] [int] NULL,
	[defaultfeature] [int] NULL,
	[defaultfeatureMulti] [varchar](200) NULL,
	[selfRegistration] [int] NULL,
	[subRegistrationStatus] [int] NULL,
	[registerText] [varchar](250) NULL,
	[captcha] [int] NULL,
	[moderates] [varchar](500) NULL,
	[registrationText] [varchar](500) NULL,
	[registrationMsg] [varchar](500) NULL,
	[ActivityReportStatus] [int] NULL,
	[ActivityReportDateTime] [datetime] NULL,
	[SinglePageCode] [varchar](max) NULL,
	[RandomiseStatus] bit,
	[BlogsNUmberInSummary] [int] NULL,
	[SearchKeyword] [varchar](max) NULL,
	[RandomiseLinksInSummary] bit,
	[NumberOfLinksInSummary] [int] NULL,
	[RandomiseLinksInLink] bit,
	[NumberOfLinksinlink] [int] NULL,
	[SearchResultPageCode] [nvarchar](MAX) NULL,
	[defaultSearch] [int] NULL,
 CONSTRAINT [PK_brand_settings] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'2::Allow users to self register,3::Admin to register users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'brand_settings', @level2type=N'COLUMN',@level2name=N'selfRegistration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1::Active;0In-Active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'brand_settings', @level2type=N'COLUMN',@level2name=N'subRegistrationStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1::CAPTCHA code required for login,2::CAPTCHA code not required for login' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'brand_settings', @level2type=N'COLUMN',@level2name=N'captcha'
GO
/****** Object:  Table [dbo].[Brand_Rss]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand_Rss](
	[feed_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](200) NULL,
	[description] [nvarchar](max) NULL,
	[link] [nvarchar](max) NULL,
	[user_template] [int] NULL,
	[c_date] [datetime] NULL,
	[c_by] [int] NULL,
	[m_date] [datetime] NULL,
	[m_by] [int] NULL,
	[rssTags] [nvarchar](max) NULL,
	[feed_status] [int] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_Brand_Rss] PRIMARY KEY CLUSTERED 
(
	[feed_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BRAND_POST_COMMENTS]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BRAND_POST_COMMENTS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[coment_post_id] [int] NULL,
	[comment_tags] [nvarchar](max) NULL,
	[comment_desc] [nvarchar](max) NULL,
	[comment_blog_id] [int] NULL,
	[c_by] [int] NULL,
	[c_date] [datetime] NULL,
	[comment_status] [int] NULL,
	[Delete_Item_mod] [int] NULL,
 CONSTRAINT [PK_BRAND_POST_COMMENTS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[brand_bookmark]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[brand_bookmark](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BK_Title] [nvarchar](500) NULL,
	[BK_URL] [nvarchar](550) NULL,
	[BK_Tags] [nvarchar](max) NULL,
	[BK_Desc] [nvarchar](max) NULL,
	[c_by] [int] NULL,
	[c_date] [datetime] NULL,
	[Status] [bit] NULL,
	[SubChannelId] [int] NULL,
	[BK_Status] [int] NULL,
	[BK_SortOrder] [int] NULL,
	[m_by] [int] NULL,
	[nofollow] [int] NULL,
	[noindex] [int] NULL,
	[noopener] [int] NULL,
	[Blocker] [tinyint] NOT NULL,
	[BlockerValue] [int] NOT NULL,
	[SameIP_Status] [int] NOT NULL,
	[SameIP_Value] [int] NOT NULL,
	[noreferrer] [int] NULL,
 CONSTRAINT [PK_brand_bookmark] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BRAND_BLOG_POSTS]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BRAND_BLOG_POSTS](
	[post_id] [int] IDENTITY(1,1) NOT NULL,
	[post_title] [nvarchar](200) NULL,
	[post_description] [nvarchar](max) NULL,
	[post_tags] [nvarchar](max) NULL,
	[posts_blog_id] [int] NULL,
	[post_status] [int] NULL,
	[c_by] [int] NULL,
	[c_date] [datetime] NULL,
	[m_by] [int] NULL,
	[m_date] [datetime] NULL,
	[Active_Inactive] [bit] NULL,
	[Post_QLink_Id] [int] NULL,
	[Delete_Item_mod] [int] NULL,
	[SortOrder] [int] NULL,
	[post_latitude] [decimal](20, 10) NULL,
	[post_longitude] [decimal](20, 10) NULL,
	[Owner_Email] [varchar](200) NULL,
 CONSTRAINT [PK_BRAND_BLOG_POSTS] PRIMARY KEY CLUSTERED 
(
	[post_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1==Active; 2== Delete;' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BRAND_BLOG_POSTS', @level2type=N'COLUMN',@level2name=N'post_status'
GO
/****** Object:  Table [dbo].[BRAND_BLOG_NEW]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BRAND_BLOG_NEW](
	[blog_id] [int] IDENTITY(1,1) NOT NULL,
	[blog_title] [nvarchar](255) NULL,
	[blog_status] [int] NULL,
	[blog_team_id] [int] NULL,
	[blog_subchannel_id] [int] NULL,
	[c_by] [int] NULL,
	[c_date] [datetime] NULL,
	[m_by] [int] NULL,
	[m_date] [datetime] NULL,
 CONSTRAINT [PK_BRAND_BLOG_NEW] PRIMARY KEY CLUSTERED 
(
	[blog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1==Active; 2== Inactive;' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BRAND_BLOG_NEW', @level2type=N'COLUMN',@level2name=N'blog_status'
GO
/****** Object:  Table [dbo].[Brand_AppSettings]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand_AppSettings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
	[value] [nvarchar](500) NULL,
	[description] [nvarchar](max) NULL,
	[type] [int] NULL,
	[IsDelete] [bit] NULL,
	[c_by] [int] NULL,
	[c_date] [datetime] NULL,
	[m_by] [int] NULL,
	[m_date] [datetime] NULL,
	[IsMetatag] [bit] NULL,
	[MetaType] [bit] NULL,
 CONSTRAINT [PK_Brand_AppSettings] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Meta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand_AppSettings', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Not Deleted; 1:Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand_AppSettings', @level2type=N'COLUMN',@level2name=N'IsDelete'
GO
/****** Object:  Table [dbo].[brand_Applist_Random]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[brand_Applist_Random](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Random_Position] [varchar](50) NULL,
	[Value] [int] NULL,
 CONSTRAINT [PK_brand_Applist_Random] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Brand_AppList]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Brand_AppList](
	[id] [bigint] NOT NULL,
	[ApplicationName] [varchar](300) NULL,
	[ContactEmail] [varchar](300) NULL,
	[ApplicationWebsite] [varchar](50) NULL,
	[Version] [varchar](50) NULL,
	[CodeBox] [varchar](max) NULL,
	[RegIPAddress] [varchar](50) NULL,
	[status] [int] NULL,
	[description] [varchar](max) NULL,
	[Image] [varchar](100) NULL,
	[cost] [real] NULL,
	[IsDefault] [int] NULL,
	[Position] [varchar](200) NULL,
	[IsRandom] [int] NOT NULL,
	[MarktoDelete] [int] NULL,
	[MarktoDeleteID] [int] NULL,
	[VastValues] [varchar](max) NULL,
	[Region] [nvarchar](50) NULL,
	[Country] [nvarchar](max) NULL,
	[CountryStatus] [int] NULL,
	[flag] [varchar](2) NULL,
	[Device] [varchar](max) NULL,
	[Browser] [varchar](max) NULL,
	[FBKey] [nvarchar](max) NULL,
	[Referal] [varchar](max) NULL,
	[Not_Referal] [varchar](max) NULL,
	[Blocker] [varchar](2) NULL,
	[height] [int] NULL,
	[width] [int] NULL,
	[SameIP_Status] [varchar](2) NULL,
	[SameIP_Value] [int] NULL,
	[SameIP_Days] [int] NULL,
	[BlockerValue] [int] NULL,
	[OS] [varchar](max) NULL,
 CONSTRAINT [PK_Brand_AppList] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-active,0 - Inactive, 2,- deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand_AppList', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'//default component =  1 , all other = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand_AppList', @level2type=N'COLUMN',@level2name=N'IsDefault'
GO
/****** Object:  Table [dbo].[brand_alerttemplate]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[brand_alerttemplate](
	[temp_id] [int] IDENTITY(1,1) NOT NULL,
	[temp_title] [nvarchar](50) NULL,
	[temp_type] [varchar](50) NULL,
	[temp_content] [nvarchar](max) NULL,
	[temp_desc] [nvarchar](500) NULL,
 CONSTRAINT [PK_brand_alerttemplate] PRIMARY KEY CLUSTERED 
(
	[temp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Brand_Abuse]    Script Date: 02/12/2015 07:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Brand_Abuse](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[Reason] [varchar](250) NULL,
	[Status] [int] NULL,
	[Created_By] [int] NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [int] NULL,
	[Modified_Date] [datetime] NULL,
	[content_id] [bigint] NULL,
	[content_type] [int] NULL,
	[content_premode] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'//1-blog post, 2- blog comment, 3-user, 4-bookmark,5-wiki,6-RSS,7-Team,8-Event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand_Abuse', @level2type=N'COLUMN',@level2name=N'content_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'// premode - 1, moderation - 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Brand_Abuse', @level2type=N'COLUMN',@level2name=N'content_premode'
GO

/****** Object:  Table [dbo].[Brand_Calendar]    Script Date: 02/24/2015 16:00:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Brand_Calendar](
	[Cal_Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Link] [nvarchar](max) NULL,
	[c_date] [datetime] NULL,
	[c_by] [int] NULL,
	[m_date] [datetime] NULL,
	[m_by] [int] NULL,
	[Status] [bit] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_Brand_Calendar] PRIMARY KEY CLUSTERED 
(
	[Cal_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Brand_Calendar] ADD  CONSTRAINT [DF_Brand_Calendar_c_date]  DEFAULT (getdate()) FOR [c_date]
GO

ALTER TABLE [dbo].[Brand_Calendar] ADD  CONSTRAINT [DF_Brand_Calendar_Status]  DEFAULT ((1)) FOR [Status]
GO

/****** Object:  Default [DF_Brand_Abuse_Status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_Abuse] ADD  CONSTRAINT [DF_Brand_Abuse_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  Default [DF_Brand_Abuse_Created_Date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_Abuse] ADD  CONSTRAINT [DF_Brand_Abuse_Created_Date]  DEFAULT (getdate()) FOR [Created_Date]
GO
/****** Object:  Default [DF_Brand_Abuse_content_premode]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_Abuse] ADD  CONSTRAINT [DF_Brand_Abuse_content_premode]  DEFAULT ((0)) FOR [content_premode]
GO
/****** Object:  Default [DF_Brand_AppList_status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_Brand_AppList_dafault]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_dafault]  DEFAULT ((0)) FOR [IsDefault]
GO
/****** Object:  Default [DF_Brand_AppList_Position]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_Position]  DEFAULT ('none') FOR [Position]
GO
/****** Object:  Default [DF_Brand_AppList_IsRandom]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_IsRandom]  DEFAULT ((0)) FOR [IsRandom]
GO
/****** Object:  Default [DF_Brand_AppList_MarktoDelete]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_MarktoDelete]  DEFAULT ((0)) FOR [MarktoDelete]
GO
/****** Object:  Default [DF_Brand_AppList_height]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_height]  DEFAULT ((0)) FOR [height]
GO
/****** Object:  Default [DF_Brand_AppList_width]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppList] ADD  CONSTRAINT [DF_Brand_AppList_width]  DEFAULT ((0)) FOR [width]
GO
/****** Object:  Default [DF_Brand_AppSettings_IsDelete]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppSettings] ADD  CONSTRAINT [DF_Brand_AppSettings_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
/****** Object:  Default [DF_Brand_AppSettings_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppSettings] ADD  CONSTRAINT [DF_Brand_AppSettings_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
/****** Object:  Default [DF__Brand_App__IsMet__47A6A41B]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_AppSettings] ADD  DEFAULT ((0)) FOR [IsMetatag]
GO
/****** Object:  Default [DF_BRAND_BLOG_NEW_blog_status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_NEW] ADD  CONSTRAINT [DF_BRAND_BLOG_NEW_blog_status]  DEFAULT ((1)) FOR [blog_status]
GO
/****** Object:  Default [DF_BRAND_BLOG_NEW_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_NEW] ADD  CONSTRAINT [DF_BRAND_BLOG_NEW_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
/****** Object:  Default [DF_BRAND_BLOG_POSTS_post_status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_POSTS] ADD  CONSTRAINT [DF_BRAND_BLOG_POSTS_post_status]  DEFAULT ((1)) FOR [post_status]
GO
/****** Object:  Default [DF_BRAND_BLOG_POSTS_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_POSTS] ADD  CONSTRAINT [DF_BRAND_BLOG_POSTS_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
/****** Object:  Default [DF_BRAND_BLOG_POSTS_featuredBlogPost]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_POSTS] ADD  CONSTRAINT [DF_BRAND_BLOG_POSTS_featuredBlogPost]  DEFAULT ((0)) FOR [Active_Inactive]
GO
/****** Object:  Default [DF_BRAND_BLOG_POSTS_Post_QLink_Id]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_POSTS] ADD  CONSTRAINT [DF_BRAND_BLOG_POSTS_Post_QLink_Id]  DEFAULT ((0)) FOR [Post_QLink_Id]
GO
/****** Object:  Default [DF_BRAND_BLOG_POSTS_Delete_Item_mod]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_POSTS] ADD  CONSTRAINT [DF_BRAND_BLOG_POSTS_Delete_Item_mod]  DEFAULT ((0)) FOR [Delete_Item_mod]
GO
/****** Object:  Default [DF_BRAND_BLOG_POSTS_SortOrder]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_BLOG_POSTS] ADD  CONSTRAINT [DF_BRAND_BLOG_POSTS_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
/****** Object:  Default [DF_brand_bookmark_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
/****** Object:  Default [DF_brand_bookmark_Status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_brand_bookmark_BK_Status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_BK_Status]  DEFAULT ((1)) FOR [BK_Status]
GO
/****** Object:  Default [DF_BRAND_POST_COMMENTS_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_POST_COMMENTS] ADD  CONSTRAINT [DF_BRAND_POST_COMMENTS_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
/****** Object:  Default [DF_BRAND_POST_COMMENTS_comment_abuse_status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_POST_COMMENTS] ADD  CONSTRAINT [DF_BRAND_POST_COMMENTS_comment_abuse_status]  DEFAULT ((1)) FOR [comment_status]
GO
/****** Object:  Default [DF_BRAND_POST_COMMENTS_Delete_Item_mod]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[BRAND_POST_COMMENTS] ADD  CONSTRAINT [DF_BRAND_POST_COMMENTS_Delete_Item_mod]  DEFAULT ((0)) FOR [Delete_Item_mod]
GO
/****** Object:  Default [DF_Brand_Rss_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_Rss] ADD  CONSTRAINT [DF_Brand_Rss_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
/****** Object:  Default [DF_Brand_Rss_feed_status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_Rss] ADD  CONSTRAINT [DF_Brand_Rss_feed_status]  DEFAULT ((1)) FOR [feed_status]
GO
/****** Object:  Default [DF_Brand_Rss_SortOrder]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[Brand_Rss] ADD  CONSTRAINT [DF_Brand_Rss_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
/****** Object:  Default [DF_brand_settings_ActivityReportStatus]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_ActivityReportStatus]  DEFAULT ((0)) FOR [ActivityReportStatus]
GO
/****** Object:  Default [DF_brand_settings_ActivityReportStatus]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_RandomiseStatus]  DEFAULT ((0)) FOR [RandomiseStatus]
GO
/****** Object:  Default [DF_brand_settings_ActivityReportDateTime]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_ActivityReportDateTime]  DEFAULT (getdate()) FOR [ActivityReportDateTime]
GO
/****** Object:  Default [DF_bbc_user_status]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_user] ADD  CONSTRAINT [DF_bbc_user_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_bbc_user_c_date]    Script Date: 02/12/2015 07:15:33 ******/
ALTER TABLE [dbo].[brand_user] ADD  CONSTRAINT [DF_bbc_user_c_date]  DEFAULT (getdate()) FOR [c_date]
GO
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_Blocker]  DEFAULT ((0)) FOR [Blocker]
GO
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_BlockerValue]  DEFAULT ((0)) FOR [BlockerValue]
GO
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_SameIP_Status]  DEFAULT ((0)) FOR [SameIP_Status]
GO
ALTER TABLE [dbo].[brand_bookmark] ADD  CONSTRAINT [DF_brand_bookmark_SameIP_Value]  DEFAULT ((0)) FOR [SameIP_Value]
GO
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_BlogsNUmberInSummary]  DEFAULT ((0)) FOR [BlogsNUmberInSummary]
GO
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_RandomiseLinksInSummary]  DEFAULT ((0)) FOR [RandomiseLinksInSummary]
GO
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_NumberOfLinksInSummary]  DEFAULT ((0)) FOR [NumberOfLinksInSummary]
GO
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_defaultSearch]  DEFAULT ((0)) FOR [defaultSearch]
GO
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_RandomiseLinksInLink]  DEFAULT ((0)) FOR [RandomiseLinksInLink]
GO
ALTER TABLE [dbo].[brand_settings] ADD  CONSTRAINT [DF_brand_settings_NumberOfLinksinlink]  DEFAULT ((0)) FOR [NumberOfLinksinlink]
GO
/* End : Tables */
/******************************************************************************************************************/