using System;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Text;

public partial class widgets_Widget_Click_Update : System.Web.UI.Page
{
    public string ClientIP = null;
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string Referer_URL = "";
    public string Campaign_Source = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["IPTime"] != null && Convert.ToString(Request.QueryString["IPTime"]) != "")
        {
            string strsqlMaintainance = "select VideoImpression,OutboundLink,VideoAdStart,VideoAdFinish,BannerClick,VideoAdClick from brand_Record_Data";
            DataTable dtserverMaintainance = dp.FillDataTable(strsqlMaintainance);
            string ResponseString = "";
            ResponseString = Request.UserHostAddress.ToString();
            ResponseString += "#$#" + System.DateTime.Now.ToString("M/d/yyyy h:m:s.ffffff tt");
            ResponseString += "#$#" + Convert.ToString(ConfigurationManager.AppSettings["GA_ID"]);
            ResponseString += "#$#" + Convert.ToString(utils.GetBrowser());
            ResponseString += "#$#" + Convert.ToString(utils.Getcountry());
            ResponseString += "#$#" + Convert.ToString(dtserverMaintainance.Rows[0]["VideoImpression"]);
            ResponseString += "#$#" + Convert.ToString(dtserverMaintainance.Rows[0]["OutboundLink"]);
            ResponseString += "#$#" + Convert.ToString(dtserverMaintainance.Rows[0]["VideoAdStart"]);
            ResponseString += "#$#" + Convert.ToString(dtserverMaintainance.Rows[0]["VideoAdFinish"]);
            ResponseString += "#$#" + Convert.ToString(dtserverMaintainance.Rows[0]["BannerClick"]);
            ResponseString += "#$#" + Convert.ToString(dtserverMaintainance.Rows[0]["VideoAdClick"]);
            ResponseString += "#$#" + ReadConfig.GetFeatures().Replace(",","^");
            ResponseString += "#$#" + Convert.ToString(utils.GetOperatingSystem());
            ResponseString += "#$#" + Convert.ToString(utils.GetUserIP());
            ResponseString += "#$#" + Convert.ToString(utils.GetUserAgent());
            Response.Write(ResponseString);
        }

        ClientIP = Convert.ToString(Request.ServerVariables["REMOTE_ADDR"]);

        if (Session["Referer_URL"] != null)
        {
            Referer_URL = Convert.ToString(Session["Referer_URL"]);
        }

        if (Session["Campaign_Source"] != null)
        {
            Campaign_Source = Convert.ToString(Session["Campaign_Source"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["WidIDblock"])) 
            && Convert.ToInt32(Request.QueryString["WidIDblock"]) != 0)
        {
            UpdateAdnetworkClick(Convert.ToInt32(Request.QueryString["WidIDblock"]));
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["WidIDblock1"])) 
            && !string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["maxval"])))
        {
            if (Convert.ToInt32(Request.QueryString["WidIDblock1"]) != 0 
                && Convert.ToInt32(Request.QueryString["maxval"]) != 0)
            {
                UpdateAdnetworkClickBlocked(Convert.ToInt32(Request.QueryString["WidIDblock1"])
                    , Convert.ToInt32(Request.QueryString["maxval"]));
            }
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["adstartid"])) 
            && Convert.ToInt32(Request.QueryString["adstartid"]) != 0)
        {
            CountAdstart(Convert.ToInt32(Request.QueryString["adstartid"]));
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["adfinishid"])) 
            && Convert.ToInt32(Request.QueryString["adfinishid"]) != 0)
        {
            CountAdfinish(Convert.ToInt32(Request.QueryString["adfinishid"]));
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["adimpressionid"])) 
            && Convert.ToInt32(Request.QueryString["adimpressionid"]) != 0)
        {
            CountAdImpression(Convert.ToInt32(Request.QueryString["adimpressionid"]));
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["AdPageImp"])))
        {
            //UpdateADPageimpression(Convert.ToInt32(Request.QueryString["AdPageImp"]));
            UpdateADPageimpressionNew(Convert.ToString(Request.QueryString["AdPageImp"]));
        }

        if (Request.QueryString["UName"] != null && Request.QueryString["UEmail"] != null && Request.QueryString["Comment"] != null)
        {
            ContactUS(Convert.ToString(Request.QueryString["UName"]), Convert.ToString(Request.QueryString["UEmail"]), Convert.ToString(Request.QueryString["Comment"]));
        }

        if (Request.QueryString["SUBEmail"] != null)
        {
            try
            {
                //MailerBrand.sendSubscribeEmail(Convert.ToString(Request.QueryString["SUBEmail"]));
                utils.SubscribeUser(Convert.ToString(Request.QueryString["SUBEmail"]));
                Response.Write("Success");
            }
            catch
            {
                Response.Write("Fail");
            }
        }

        if (Request.QueryString["FeedTitle"] != null && Request.QueryString["FeedLink"] != null)
        {
            DataTable dtRss = new DataTable();
            try
            {
                dtRss = utils.LoadRSS(Convert.ToString(Request.QueryString["FeedLink"]), utils.formatStringForDBSelect(Convert.ToString(Request.QueryString["FeedTitle"])));
            }
            catch
            {
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////
            if (!dtRss.Columns.Contains("link"))
            {
                dtRss.Columns.Add("link", typeof(string));
                dtRss.Columns.Add("pubdate", typeof(string));
                dtRss.Columns.Add("creator", typeof(string));
                dtRss.Columns.Add("description", typeof(string));
                dtRss.Columns.Add("title", typeof(string));
            }
            if (dtRss.Rows.Count <= 0)
            {
                DataRow drdtRss = dtRss.NewRow();
                drdtRss["link"] = "";
                drdtRss["pubdate"] = "";
                drdtRss["creator"] = "";
                drdtRss["description"] = "Feed is not available";
                drdtRss["title"] = "";
                dtRss.Rows.Add(drdtRss);
            }
            StringBuilder SBFeed = new StringBuilder();
            SBFeed.Append("<tbody>");
            foreach (DataRow drRss in dtRss.Rows)
            {
                SBFeed.Append("<tr><td><p><strong><a target=\"_blank\" href=\"" + Convert.ToString(drRss["link"]) + "\">" + Convert.ToString(drRss["title"]) + "</a></strong></p>");
                SBFeed.Append("<p>" + Convert.ToString(drRss["pubdate"]) + "</p>");
                SBFeed.Append("<p>" + Server.HtmlEncode(Convert.ToString(drRss["description"])) + "</p></td></tr>");
            }
            SBFeed.AppendLine("</tbody>");
            Response.Write(SBFeed.ToString());
        }
    }

    public void UpdateAdnetworkClick(int banid)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            string digip_domain = Request.Url.Host.ToString();
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", ClientIP);
            cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
            cmd.Parameters.Add("@DIGIP_Domain", Request.Url.Host);
            cmd.Parameters.Add("@Referer_URL", Referer_URL);
            cmd.Parameters.Add("@Type", "ad");
            cmd.Parameters.Add("@block_status", "0");
            cmd.Parameters.Add("@Maxvalue", "0");
            cmd.Parameters.Add("@Campaign_Source", Campaign_Source);
            dpreport.ExecuteNonQuery(cmd);

            if (ConfigLoader.GetIsUseNewClickDB() == 1)
            {
                DBHelper dpreport2 = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
                DBCommand cmd2 = new DBCommand();
                cmd2.CommandText = "SP_Brand_Insert_ClickInfo";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.Add("@IPAdress", ClientIP);
                cmd2.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd2.Parameters.Add("@DIGIP_Domain", digip_domain);
                cmd2.Parameters.Add("@Type", "ad");
                dpreport2.ExecuteNonQuery(cmd2);
            }
        }
    }

    public void UpdateAdnetworkClickBlocked(int widID, int Maxvalue)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", ClientIP);
            cmd.Parameters.Add("@WidgetID", Convert.ToString(widID));
            cmd.Parameters.Add("@DIGIP_Domain", Request.Url.Host);
            cmd.Parameters.Add("@Referer_URL", Referer_URL);
            cmd.Parameters.Add("@Type", "ad");
            cmd.Parameters.Add("@block_status", "1");
            cmd.Parameters.Add("@Maxvalue", Convert.ToString(Maxvalue));
            cmd.Parameters.Add("@Campaign_Source", Campaign_Source);
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void CountAdstart(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_AdStart_Count";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ADID", Convert.ToString(widID));
            cmd.Parameters.Add("@Type", "ad");
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void CountAdImpression(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_Ad_Impression";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ADID", Convert.ToString(widID));
            cmd.Parameters.Add("@Type", "ad");
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void CountAdfinish(int widID)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_AdFinish_Count";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ADID", Convert.ToString(widID));
            cmd.Parameters.Add("@Type", "ad");
            dpreport.ExecuteNonQuery(cmd);
        }
    }

    public void UpdateADPageimpression(int banid)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            try
            {
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Insert_Page_Impression";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", ClientIP);
                cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd.Parameters.Add("@Domain", Request.Url.Host);
                cmd.Parameters.Add("@Type", "ad");
                cmd.Parameters.Add("@RefererURL", Referer_URL);
                dpreport.ExecuteNonQuery(cmd);
            }
            catch
            {
            }
        }
    }

    public void UpdateADPageimpressionNew(string AdNetworkIDs)
    {
        if (utils.GetStartADNetworkAndWidgetReport() == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            try
            {
                DBCommand cmd = new DBCommand();
                AdNetworkIDs = AdNetworkIDs.TrimEnd(',');
                cmd.CommandText = "SP_Brand_Insert_AdNetworks_Impression";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", ClientIP);
                cmd.Parameters.Add("@AdNetworkIDs", AdNetworkIDs);
                cmd.Parameters.Add("@Domain", Request.Url.Host);
                cmd.Parameters.Add("@Type", "ad");
                cmd.Parameters.Add("@RefererURL", Referer_URL);
                dpreport.ExecuteNonQuery(cmd);
            }
            catch
            {
            }
        }
    }

    public void ContactUS(string Uname, string EmailFrom, string comment)
    {
        bool flag = true;
        SmtpClient smtpClient = new SmtpClient();
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        MailMessage message = new MailMessage();
        message.IsBodyHtml = true;
        MailAddress toAddress = new MailAddress(ReadConfig.GetEmail());
        string strFrom = utils.formatStringForDBInsert(EmailFrom);
        string strDomian = ReadConfig.getDomainUrl();
        string strSub = strDomian + " - Contact Form";
        MailAddress fromAddress = new MailAddress(strFrom, Uname);
        message.From = fromAddress;
        message.To.Add(toAddress);
        message.Subject = strSub;
        string strIPaddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        //message.ReplyTo = new MailAddress(strFrom, Uname);
        message.ReplyToList.Add(new MailAddress(strFrom, Uname));
        string strBody = string.Empty;

        strBody = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
        strBody += "<html><head>";
        strBody += "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">";
        strBody += "<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>";
        strBody += "<style type='text/css'>";
        strBody += " table { font-family:arial;font-size:12px;color:#000000;} ";
        strBody += "</style>";
        strBody += "</head>";
        strBody += "<BODY><Table><tr><td>Name:" + utils.formatStringForDBInsert(Uname.Trim()) + "</td></tr>";
        strBody += "<tr><td> From: " + strFrom + "</td></tr>";
        strBody += "<tr><td> IP: " + strIPaddress + "</td></tr>";
        strBody += "<br/>";
        strBody += "<tr><td> Message: " + comment.Trim() + "</td></tr>";
        strBody += "<br/>";
        strBody += "</Table></BODY></html>";

        AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody, HTMLType);
        message.AlternateViews.Add(HTMLView);

        try
        {
            smtpClient.Send(message);
        }
        catch
        {
            flag = false;
        }
        finally
        {
            message.Dispose();
        }
        if (flag == false)
        {
            Response.Write("Fail");
        }
        else
        {
            Response.Write("Success");
        }
    }
}
