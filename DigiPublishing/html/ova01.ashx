<%@ WebHandler Language="C#" Class="wrapper" %>

using System;
using System.Data;
using System.Web;
using System.Text.RegularExpressions;

public class wrapper : IHttpHandler 
{
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string adclickplay = "true";
    public void ProcessRequest (HttpContext context) 
    {

        string AdTag = getNetworksFromDB();
	    string aaaa = "\"playOnce\": true,\"autoPlay\": " + adclickplay + ",\"debug\": {\"debugger\": \"firebug\",\"levels\": \"fatal, config, vast_template,";
	    aaaa += "vpaid,http_calls,tracking_events,playlist,api, segment_formation\"},\"canFireAPICalls\":true,";
        aaaa += "\"ads\": {\"pauseOnClickThrough\":false,\"notice\": { \"show\": true },\"displayCompanions\":false,\"restoreCompanions\":false,\"companions\":[{\"id\":\"companion\",\"width\":\"300\",\"height\":\"250\",\"resourceType\":\"html\"}],";
        aaaa += "\"servers\": [ {\"type\": \"direct\",\"apiAddress\":";
	    string bbbb = "}],\"schedule\": [{\"position\": \"pre-roll\"}]}}";
        context.Response.ContentType = "text/xml";
        context.Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><config>");
        context.Response.Write("<ova.title>Example 1 A pre-roll ad only</ova.title>");
        context.Response.Write("<ova.json>");
        context.Response.Write("<![CDATA[{"+aaaa + "\""+AdTag+"\"" + bbbb + "}]]>");
        context.Response.Write("</ova.json>");
        context.Response.Write("</config>");
        
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

    public string getNetworksFromDB()
    {
        string strbreakmedia = "";
        string strquery = "select Tag,isnull(Ad_click_play,0) as Ad_click_play from Brand_AdNetworkRuleSettings where AdNetwork_ID=" + HttpContext.Current.Request.QueryString["id"] + "";
        DataTable dtstart = dp.FillDataTable(strquery);
        if (dtstart.Rows.Count > 0)
        {
            strbreakmedia = Convert.ToString(dtstart.Rows[0]["Tag"]);
            strbreakmedia = ReplaceTimestamp(strbreakmedia);
            if (Convert.ToInt32(dtstart.Rows[0]["Ad_click_play"].ToString()) == 0)
            {
                adclickplay = "true";
            }
            else if (Convert.ToInt32(dtstart.Rows[0]["Ad_click_play"].ToString()) == 1)
            {
                adclickplay = "false";
            }
        }
		string url = "http://" + HttpContext.Current.Request.Url.Host;
		strbreakmedia = strbreakmedia.Replace("[page_url]", HttpUtility.UrlEncode(url));
        strbreakmedia = strbreakmedia.Replace("[PAGE_URL]", HttpUtility.UrlEncode(url));
        return strbreakmedia;
    }

public string NewTimeGuid()
    {
        string strNewId1 = null;
        strNewId1 = System.Guid.NewGuid().ToString();
        strNewId1 = strNewId1.Replace("_", "");
        strNewId1 = strNewId1.Replace("-", "");
        Regex rgx = new Regex("[^\\d]");
        strNewId1 = rgx.Replace(strNewId1, "");
        strNewId1 = strNewId1.Substring(1, 8);

        return strNewId1;
    }

    public string ReplaceTimestamp(string strV)
    {
        strV = strV.Replace("[timestamp]", NewTimeGuid());
        strV = strV.Replace("[TIMESTAMP]", NewTimeGuid());
        strV = strV.Replace("[random]", NewTimeGuid());
        strV = strV.Replace("[RANDOM]", NewTimeGuid());
        return strV;
    }

}