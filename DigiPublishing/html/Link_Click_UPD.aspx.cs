using System;
using System.Data;

public partial class Link_Click_UPD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["linkID"])))
        {
            if (Convert.ToInt32(Request.QueryString["linkID"]) != 0 && Convert.ToInt32(Request.QueryString["domainID"]) != 0)
            {
                UpdateLinkclick(Convert.ToInt32(Request.QueryString["linkID"]), Convert.ToInt32(Request.QueryString["domainID"]), Convert.ToInt32(Request.QueryString["blockstatus"]));
            }
        }
        if (!String.IsNullOrWhiteSpace(Convert.ToString(Request.QueryString["linkIDSI"])))
        {
            if (Convert.ToInt32(Request.QueryString["linkIDSI"]) != 0 && Convert.ToInt32(Request.QueryString["domainID"]) != 0)
            {
                Response.Write(SameIPLinkclick(Convert.ToInt32(Request.QueryString["linkIDSI"]), Convert.ToInt32(Request.QueryString["domainID"]), Convert.ToString(Request.QueryString["spanid"])));
            }
        }
    }
    public void UpdateLinkclick(int linkid, int did, int blockstatus)
    { 
        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Insert_LinkClickInfo";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@IPAdress", utils.GetUserIP());
        cmd.Parameters.Add("@LinkID", Convert.ToString(linkid));
        cmd.Parameters.Add("@DIGIP_Domain", did);
        cmd.Parameters.Add("@block_status", blockstatus);
        dpreport.ExecuteNonQuery(cmd);
    }
    public string SameIPLinkclick(int linkid, int did,string spanid)
    {
        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "SP_Brand_Select_SameIP_LinkClick";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserIP", utils.GetUserIP());
        cmd.Parameters.Add("@LinkID", Convert.ToString(linkid));
        cmd.Parameters.Add("@DomainID", did);
        cmd.Parameters.Add("@BlockStatus", 0);
        int sameipclickcountlink = Convert.ToInt32(dpreport.ExecuteScalar(cmd));
        return spanid + "," + sameipclickcountlink.ToString();
    }
}
