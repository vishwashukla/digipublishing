using System;
using System.Data;
using System.Web;

public partial class Banner_Click_UPD : System.Web.UI.Page
{
    public string ClientIP = string.Empty;
    DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
    public string Referer_URL = string.Empty;
    public int intAllowAdnetworkData = 0;
    public string Campaign_Source = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        intAllowAdnetworkData = utils.GetStartADNetworkAndWidgetReport();
        ClientIP = Convert.ToString(Request.ServerVariables["REMOTE_ADDR"]);
        if (Convert.ToString(Session["Referer_URL"]) != "")
        {
            Referer_URL = Convert.ToString(Session["Referer_URL"]);
        }
        if (Session["Campaign_Source"] != null)
        {
            Campaign_Source = Convert.ToString(Session["Campaign_Source"]);
        }
        else
        {
            Campaign_Source = "";
        }
        if (Convert.ToString(Request.QueryString["banID"]) != "")
        {
            if (Convert.ToInt32(Request.QueryString["banID"]) != 0)
            {
                Updatewidgetclick(Convert.ToInt32(Request.QueryString["banID"]));
            }
        }
        if (Convert.ToString(Request.QueryString["banIDBlocked"]) != "")
        {
            if (Convert.ToInt32(Request.QueryString["banIDBlocked"]) != 0)
            {
                UpdatewidgetclickBlocked(Convert.ToInt32(Request.QueryString["banIDBlocked"]));
            }
        }
        if (Convert.ToString(Request.QueryString["BannerPageImp"]) != "" && Convert.ToInt32(Request.QueryString["BannerPageImp"]) != 0)
        {
            UpdateBannerPageimpression(Convert.ToInt32(Request.QueryString["BannerPageImp"]));
        }
        
        if (Convert.ToString(Request.QueryString["SameIPClicksBanner"]) != "" && Convert.ToInt32(Request.QueryString["SameIPClicksBanner"]) != 0
            && Convert.ToString(Request.QueryString["SameIPDays"]) != "" && Convert.ToInt32(Request.QueryString["SameIPDays"]) != 0)
        {
            SameIPClicksBanner(Convert.ToInt32(Request.QueryString["SameIPClicksBanner"]), Convert.ToInt32(Request.QueryString["SameIPDays"]));
        }
        if (Convert.ToString(Request.QueryString["SameIPClicksAd"]) != "" && Convert.ToString(Request.QueryString["SameIPDays"]) != "" && Convert.ToInt32(Request.QueryString["SameIPClicksAd"]) != 0 && Convert.ToInt32(Request.QueryString["SameIPDays"]) != 0)
        {
            SameIPClicksAd(Convert.ToInt32(Request.QueryString["SameIPClicksAd"]), Convert.ToInt32(Request.QueryString["SameIPDays"]));
        }
        
    }
    public void Updatewidgetclick(int banid)
    {
        if (intAllowAdnetworkData == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            int blocked = 0;
            string Type = "";
            Type = "banner";
            string digip_domain = Request.Url.Host.ToString();
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", Convert.ToString(ClientIP));
            cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
            cmd.Parameters.Add("@DIGIP_Domain", Convert.ToString(digip_domain));
            cmd.Parameters.Add("@Referer_URL", Convert.ToString(Referer_URL));
            cmd.Parameters.Add("@Type", Convert.ToString(Type));
            cmd.Parameters.Add("@block_status", Convert.ToString(blocked));
            cmd.Parameters.Add("@Maxvalue", Convert.ToString("0"));
            cmd.Parameters.Add("@Campaign_Source", Convert.ToString(Campaign_Source));
            dpreport.ExecuteNonQuery(cmd);

            if (ConfigLoader.GetIsUseNewClickDB() == 1)
            {
                DBHelper dpreport2 = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
                DBCommand cmd2 = new DBCommand();
                cmd2.CommandText = "SP_Brand_Insert_ClickInfo";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.Add("@IPAdress", ClientIP);
                cmd2.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd2.Parameters.Add("@DIGIP_Domain", digip_domain);
                cmd2.Parameters.Add("@Type", Type);
                dpreport2.ExecuteNonQuery(cmd2);
            }
        }
    }
    public void UpdatewidgetclickBlocked(int widID)
    {
        if (intAllowAdnetworkData == 1)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            string digip_domain = Request.Url.Host.ToString();
            int blocked = 1;
            string Type = "";
            Type = "banner";
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Insert_ClickInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IPAdress", Convert.ToString(ClientIP));
            cmd.Parameters.Add("@WidgetID", Convert.ToString(widID));
            cmd.Parameters.Add("@DIGIP_Domain", Convert.ToString(digip_domain));
            cmd.Parameters.Add("@Referer_URL", Convert.ToString(Referer_URL));
            cmd.Parameters.Add("@Type", Convert.ToString(Type));
            cmd.Parameters.Add("@block_status", Convert.ToString(blocked));
            cmd.Parameters.Add("@Campaign_Source", Convert.ToString(Campaign_Source));
            dpreport.ExecuteNonQuery(cmd);
        }
    }
    public void UpdateBannerPageimpression(int banid)
    {
        if (intAllowAdnetworkData == 1)
        {
            string Type = null;
            Type = "banner";
            string digip_domain = Request.Url.Host.ToString();
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            try
            {
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "SP_Brand_Insert_Page_Impression";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserIP", Convert.ToString(ClientIP));
                cmd.Parameters.Add("@WidgetID", Convert.ToString(banid));
                cmd.Parameters.Add("@Domain", Convert.ToString(digip_domain));
                cmd.Parameters.Add("@Type", Convert.ToString(Type));
                cmd.Parameters.Add("@RefererURL", Convert.ToString(HttpContext.Current.Session["Referer_URL"]));
                dpreport.ExecuteNonQuery(cmd);
            }
            catch
            {
            }
        }
    }

    public void SameIPClicksBanner(int bannerid, int SameIPDays)
    {
        if (ConfigLoader.GetIsUseNewClickDB() == 0)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            int SameIPCount = 0;
            DataTable dt = new DataTable();
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_SameIP_Click";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@UserIP", Convert.ToString(ClientIP));
            cmd.Parameters.Add("@WidgetID", Convert.ToString(bannerid));
            cmd.Parameters.Add("@BlockStatus", Convert.ToString("0"));
            cmd.Parameters.Add("@Type", Convert.ToString("banner"));
            dt = dpreport.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                SameIPCount = Convert.ToInt32(dt.Rows[0]["widgetclick"]);
            }
            Response.Write(Convert.ToString(SameIPCount));
        }
        else
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
            int SameIPCount = 0;
            DataTable dt = new DataTable();
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_SameIP_Click";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@UserIP", Convert.ToString(ClientIP));
            cmd.Parameters.Add("@WidgetID", Convert.ToString(bannerid));
            cmd.Parameters.Add("@Days", SameIPDays);
            cmd.Parameters.Add("@Type", Convert.ToString("banner"));
            dt = dpreport.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                SameIPCount = Convert.ToInt32(dt.Rows[0]["widgetclick"]);
            }
            Response.Write(Convert.ToString(SameIPCount));
        }
    }

    public void SameIPClicksAd(int adid, int SameIPDays)
    {
        if (ConfigLoader.GetIsUseNewClickDB() == 0)
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);
            int maxcount = 0;
            DataTable dt = new DataTable();
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_SameIP_Click";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@UserIP", Convert.ToString(ClientIP));
            cmd.Parameters.Add("@WidgetID", Convert.ToString(adid));
            cmd.Parameters.Add("@BlockStatus", Convert.ToString("0"));
            cmd.Parameters.Add("@Type", Convert.ToString("ad"));
            dt = dpreport.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                maxcount = Convert.ToInt32(dt.Rows[0]["widgetclick"]);
            }
            Response.Write(Convert.ToString(maxcount));
        }
        else
        {
            DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString2(), DBSType.MSSQL);
            int SameIPCount = 0;
            DataTable dt = new DataTable();
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "SP_Brand_Select_SameIP_Click";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@UserIP", Convert.ToString(ClientIP));
            cmd.Parameters.Add("@WidgetID", Convert.ToString(adid));
            cmd.Parameters.Add("@Days", SameIPDays);
            cmd.Parameters.Add("@Type", Convert.ToString("ad"));
            dt = dpreport.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                SameIPCount = Convert.ToInt32(dt.Rows[0]["widgetclick"]);
            }
            Response.Write(Convert.ToString(SameIPCount));
        }
    }
}
