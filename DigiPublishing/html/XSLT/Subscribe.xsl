<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="DigipDomain"/>
<xsl:template match="/">
  <h2 id="Subscribe_Heading" class="surfSubscribe">Subscribe to <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" /> Email</h2>
  <div class="subscribebox">
    <div id="divSubscribeDesc">Enter your email address to subscribe to <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" /> and receive notifications of new posts by email.</div>
    <div id="divSubscribeForm">
      <input type="text" id="txtsubscribeemail" class="txtSubscribe" placeholder="Email address" />
      <span id="rqfEmailSubscribe" style="color:Red;display:none;">Please enter email.</span>
      <span id="regEmailSubscrib" style="color:Red;display:none;">Invalid email address</span>
      <span id="lblsubsmsg" style="color:Green;display:none;">Subscription is successful.</span>
      <input type="submit" value="Subscribe" id="btnSuscribe" class="btnSuscribe" onclick="javascript:return Subscribe();" />
    </div>
  </div>
</xsl:template>
</xsl:stylesheet> 
