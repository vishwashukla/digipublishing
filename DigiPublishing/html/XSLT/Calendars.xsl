<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:param name="CalendarIDParam"></xsl:param>
  <xsl:template match="/">
    <h3>
      <span id="ContentPlaceHolderMain_lblCalendar">
        <xsl:for-each select ="Calendars/Calendar">
          <xsl:if test="number($CalendarIDParam)=number(CalendarID)">
            <xsl:value-of select="CalendarTitle" disable-output-escaping="yes"/>
          </xsl:if>
        </xsl:for-each>
      </span>
    </h3>
    <div style="display: inline;">
      <xsl:for-each select ="Calendars/Calendar">
        <xsl:if test="number($CalendarIDParam)=number(CalendarID)">
          <table id="ContentPlaceHolderMain_dlstCalendar" border="0" style="border-collapse:collapse;">
            <tbody>
              <xsl:for-each select="Item">
                <tr>
                  <td>
                    <p>
                      <strong>
                        <a target="_blank">
                          <xsl:value-of select="title" disable-output-escaping="yes"/>
                        </a>
                      </strong>
                    </p>
                    <p>
                      <xsl:if test="description != 'Calendar not available!'">
                        <xsl:value-of select="DTSTART" disable-output-escaping="yes"/>
                        <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;-&nbsp;]]></xsl:text>
                        <xsl:value-of select="DTEND" disable-output-escaping="yes"/>
                      </xsl:if>
                    </p>
                    <p>
                      <xsl:if test="description = 'Calendar not available!'">
                        <xsl:attribute name="style">
                          <xsl:text disable-output-escaping="yes"><![CDATA[color:red]]></xsl:text>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="description" disable-output-escaping="yes"/>
                    </p>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </xsl:if>
      </xsl:for-each>
      <div id="ContentPlaceHolderMain_pnlPagingMain">
        <div id="ContentPlaceHolderMain_dlPaging">
          <ul class="ulPagingFeeds">
            <xsl:for-each select="Calendars/Calendar">
              <li class="liPagingFeeds">
                <a>
                  <xsl:choose>
                    <xsl:when test="number($CalendarIDParam)=number(CalendarID)">
                      <xsl:attribute name="class">
                        <xsl:text disable-output-escaping ="yes"><![CDATA[feedpaging-disabled]]></xsl:text>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="class">
                        <xsl:text disable-output-escaping ="yes"><![CDATA[feedpaging-enabled]]></xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="href">
                        <xsl:text disable-output-escaping ="yes"><![CDATA[Calendars.html?calendarid=]]></xsl:text><xsl:value-of select="CalendarID"/>
                      </xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:value-of disable-output-escaping="yes" select="CalendartitleForLink"/>
                </a>
              </li>
            </xsl:for-each>
          </ul>
        </div>
        <xsl:variable name="Count" select="count(Calendars/Calendar)"/>
        <xsl:for-each select="Calendars/Calendar">
          <xsl:if test="number($CalendarIDParam)=number(CalendarID)">
            <div style="height: 30px; text-align: center; margin: 10px 0 0 0;">
              <span id="ContentPlaceHolderMain_lblPageInfo" style="font-size:11pt;font-weight:bold;">
                Page <xsl:value-of select="position()" /> of <xsl:value-of select="$Count"/>
              </span>
            </div>
          </xsl:if>
        </xsl:for-each>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>