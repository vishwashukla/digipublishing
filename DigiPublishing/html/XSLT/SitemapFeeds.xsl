<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:template match="/">
    <xsl:if test="count(Updates/Update) &gt; 0">
      <h3 class="HeaderClass">
        Feeds
      </h3>
    </xsl:if>
    <br/>
    <p>
      <span id="ContentPlaceHolderMain_lblRss">
        <xsl:for-each select="Updates/Update">
          <xsl:if test="position() &gt; 1">
            <xsl:text disable-output-escaping ="yes"><![CDATA[, ]]></xsl:text>
          </xsl:if>
          <a>
            <xsl:attribute name ="href">
              <xsl:text disable-output-escaping="yes"><![CDATA[Feeds.html?feedid=]]></xsl:text><xsl:value-of select="UpdateID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="UpdateTitle"/>
          </a>
        </xsl:for-each>
      </span>
    </p>
    <p>
      <br/>
      </p>
  </xsl:template>
</xsl:stylesheet> 
