<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:param name="Page"/>
  <xsl:param name="PageSize"/>
  <xsl:template name="pagingnumbers">
    <xsl:param name="intcount" select="0"/>
    <xsl:param name="maxvalue" select="0"/>
    <xsl:if test="$intcount &lt; $maxvalue">
      <td>
        <a id="ctl00_ContentPlaceHolderMain_OndBlogs1_dlPaging_ctl01_lnkbtnPaging">
          <xsl:choose>
            <xsl:when test="number($intcount) = number($Page)">
              <xsl:attribute name="style">
                <xsl:text disable-output-escaping="yes"><![CDATA[font-size:Medium;font-weight:bold;color:red;]]></xsl:text>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="style">
                <xsl:text disable-output-escaping="yes"><![CDATA[cursor:pointer;font-size:Medium;font-weight:bold;]]></xsl:text>
              </xsl:attribute>
              <xsl:attribute name="href">
                <xsl:text disable-output-escaping="yes"><![CDATA[Links.html?page=]]></xsl:text><xsl:value-of select="$intcount"/>
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:value-of disable-output-escaping="yes" select="$intcount"/>
        </a>
        <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
      </td>
      <xsl:call-template name="pagingnumbers">
        <xsl:with-param name="intcount" select="$intcount + 1"/>
        <xsl:with-param name="maxvalue" select="$maxvalue"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <xsl:template match="/">
    <xsl:variable name="mycount" select="count(Links/Link)"/>
    <xsl:variable name="selectedRowCount" select="floor((number($mycount)-1) div number($PageSize))+1"/>
    <table border="0" id="ContentPlaceHolderMain_dgrdBookmark" style="border-collapse:collapse;" class="tableBookmark">
      <tbody id="dgrdBookmarkBody">
        <xsl:for-each select="Links/Link">
          <xsl:if test="position() &gt;= (number(number($Page)-1) * number($PageSize)) + 1">
            <xsl:if test="position() &lt;= number($PageSize) + (number($PageSize) * number(number($Page)-1))">
              <tr class="MainLinktr">
                <td style="width:400px;position:relative;" class="MainLinktd">
                  <xsl:attribute name="id">
                    <xsl:text disable-output-escaping="yes"><![CDATA[MainLinktd_]]></xsl:text>
                    <xsl:value-of select="ALinkID" disable-output-escaping="yes"/>
                  </xsl:attribute>
                  <p>
                    <xsl:value-of select="Alink" disable-output-escaping="yes"/>
                  </p>
                  <div class="seperate"></div>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>
      </tbody>
    </table>
    <xsl:if test="number($selectedRowCount) &gt; 1">
      <div id="ctl00_ContentPlaceHolderMain_SocialBookmark1_pnlPagingMain">
        <table width="100%">
          <tbody>
            <tr align="center">
              <td>
                <table>
                  <tbody>
                    <tr>
                      <td align="right">
                        <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;]]></xsl:text>
                      </td>
                      <td align="center" valign="middle">
                        <table id="ctl00_ContentPlaceHolderMain_OndBlogs1_dlPaging" border="0" style="border-collapse:collapse;">
                          <tbody>
                            <tr>
                              <xsl:variable name="firstnumber">
                                <xsl:if test="number(number($Page) mod 10) = 0">
                                  <xsl:value-of select="number(floor(number(number($Page)-1) div 10) * 10) + 1"/>
                                </xsl:if>
                                <xsl:if test="number(number($Page) mod 10) &gt; 0">
                                  <xsl:value-of select="number(floor(number($Page) div 10) * 10)+1"/>
                                </xsl:if>
                              </xsl:variable>

                              <xsl:variable name="lastnumber">
                                <xsl:if test="$firstnumber + 10 &lt;= $selectedRowCount">
                                  <xsl:value-of select="$firstnumber + 10"/>
                                </xsl:if>
                                <xsl:if test="$firstnumber + 10 &gt; $selectedRowCount">
                                  <xsl:value-of select="$selectedRowCount+1"/>
                                </xsl:if>
                              </xsl:variable>

                              <xsl:variable name="firstindex">
                                <xsl:if test="number($Page) &lt;= 10">
                                  <xsl:value-of select="number(1)" />
                                </xsl:if>
                                <xsl:if test="number($Page) &gt; 10">
                                  <xsl:value-of select="number(number($Page)-10)" />
                                </xsl:if>
                              </xsl:variable>

                              <xsl:variable name="lastindex">
                                <xsl:if test="number($Page)+10 &gt;= number($selectedRowCount)">
                                  <xsl:value-of select="number(number($selectedRowCount))" />
                                </xsl:if>
                                <xsl:if test="number($Page)+10 &lt; number($selectedRowCount)">
                                  <xsl:value-of select="number(number($Page)+10)" />
                                </xsl:if>
                              </xsl:variable>

                              <xsl:if test="number($firstnumber) &gt; 1">
                                <td>
                                  <a id="ctl00_ContentPlaceHolderMain_OndBlogs1_dlPaging_ctl05_lnkbtnPaging" style="cursor:pointer;font-size:Medium;font-weight:bold;">
                                    <xsl:attribute name="href">
                                      <xsl:text disable-output-escaping="yes"><![CDATA[Links.html?page=]]></xsl:text><xsl:value-of select="number($firstindex)"/>
                                    </xsl:attribute>
                                    <xsl:text disable-output-escaping="yes"><![CDATA[...]]></xsl:text>
                                  </a>
                                  <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                                </td>
                              </xsl:if>

                              <xsl:call-template name="pagingnumbers">
                                <xsl:with-param name="intcount" select="$firstnumber"/>
                                <xsl:with-param name="maxvalue" select="$lastnumber"/>
                              </xsl:call-template>

                              <xsl:if test="number($lastnumber) &lt;= number($selectedRowCount)">
                                <td>
                                  <a id="ctl00_ContentPlaceHolderMain_OndBlogs1_dlPaging_ctl05_lnkbtnPaging" style="cursor:pointer;font-size:Medium;font-weight:bold;">
                                    <xsl:attribute name="href">
                                      <xsl:text disable-output-escaping="yes"><![CDATA[Links.html?page=]]></xsl:text><xsl:value-of select="number($lastindex)"/>
                                    </xsl:attribute>
                                    <xsl:text disable-output-escaping="yes"><![CDATA[...]]></xsl:text>
                                  </a>
                                  <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                                </td>
                              </xsl:if>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td align="left">
                        <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;]]></xsl:text>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="3" align="center" style="height: 30px" valign="middle">
                        <span id="ctl00_ContentPlaceHolderMain_SocialBookmark1_lblPageInfo" style="font-size:Medium;font-weight:bold;">
                          Page <xsl:value-of select="number($Page)" /> of <xsl:value-of select="number($selectedRowCount)"/>
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
