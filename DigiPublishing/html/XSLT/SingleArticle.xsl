<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="Page"/>
  <xsl:param name="ArticleID"/>
  <xsl:param name="PageURL"/>
  <xsl:param name="PostedBY"/>
  <xsl:template name="pagingnumbers">
    <xsl:param name="intcount" select="0"/>
    <xsl:param name="maxvalue" select="0"/>
    <xsl:if test="$intcount &lt; $maxvalue">
      <td>
        <a>
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes"><![CDATA[ContentPlaceHolderMain_dlPaging_lnkbtnPaging_]]></xsl:text><xsl:value-of select="$intcount"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="number($intcount) = number($Page)">
              <xsl:attribute name="style">
                <xsl:text disable-output-escaping="yes"><![CDATA[font-size:Medium;font-weight:bold;color:red;]]></xsl:text>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="style">
                <xsl:text disable-output-escaping="yes"><![CDATA[cursor:pointer;font-size:Medium;font-weight:bold;]]></xsl:text>
              </xsl:attribute>
              <xsl:attribute name="href">
                <xsl:text disable-output-escaping="yes"><![CDATA[Index.html?page=]]></xsl:text><xsl:value-of select="$intcount"/>
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:value-of disable-output-escaping="yes" select="$intcount"/>
        </a>
        <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
      </td>
      <xsl:call-template name="pagingnumbers">
        <xsl:with-param name="intcount" select="$intcount + 1"/>
        <xsl:with-param name="maxvalue" select="$maxvalue"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <xsl:template match="/">
    <xsl:param name="PageSize" select="Articles/Article/PostPerPage"/>
    <xsl:variable name="mycount" select="count(Articles/Article)"/>
    <xsl:variable name="selectedRowCount" select="floor((number($mycount)-1) div $PageSize)+1"/>
    <table id="ContentPlaceHolderMain_dlstBlog" border="0" style="border-collapse:collapse;">
      <tbody>
        <xsl:for-each select="Articles/Article">
          <xsl:if test="PostID=$ArticleID">
            <tr>
              <td>
                <table width="100%">
                  <tbody>
                    <tr>
                      <td>
                        <h1 style="font-size:Medium;font-weight:bold;" class="lblBlogPostsTitle">
                          <xsl:value-of select="Title"/>
                        </h1>
                        <br />
                        <div style="display: inline;">
                          <br />
                          <div class="st_blogpost_container">
                            <xsl:value-of select="Description" disable-output-escaping="yes"/>
                          </div>
                          <br />
                        </div>
                      </td>
                    </tr>
                    <tr align="left">
                      <td colspan="2">
                        <xsl:value-of select="Logo" disable-output-escaping="yes"/>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </tbody>
    </table>
    <div id="ContentPlaceHolderMain_pnlPagingMain">
      <table width="100%">
        <tbody>
          <tr align="center">
            <td>
              <table>
                <tbody>
                  <tr>
                    <td align="center" valign="middle">
                      <table id="ContentPlaceHolderMain_dlPaging" border="0" style="border-collapse:collapse;">
                        <tbody>
                          <tr>
                            <xsl:variable name="FN">
                              <xsl:if test="number(number($Page) mod 10) = 0">
                                <xsl:value-of select="number(floor(number(number($Page)-1) div 10) * 10) + 1"/>
                              </xsl:if>
                              <xsl:if test="number(number($Page) mod 10) &gt; 0">
                                <xsl:value-of select="number(floor(number($Page) div 10) * 10)+1"/>
                              </xsl:if>
                            </xsl:variable>
                            <xsl:variable name="LN">
                              <xsl:if test="$FN + 10 &lt;= $selectedRowCount">
                                <xsl:value-of select="$FN + 10"/>
                              </xsl:if>
                              <xsl:if test="$FN + 10 &gt; $selectedRowCount">
                                <xsl:value-of select="$selectedRowCount+1"/>
                              </xsl:if>
                            </xsl:variable>
                            <xsl:variable name="FI">
                              <xsl:if test="number($Page) &lt;= 10">
                                <xsl:value-of select="number(1)" />
                              </xsl:if>
                              <xsl:if test="number($Page) &gt; 10">
                                <xsl:value-of select="number(number($Page)-10)" />
                              </xsl:if>
                            </xsl:variable>
                            <xsl:variable name="LI">
                              <xsl:if test="number($Page)+10 &gt;= number($selectedRowCount)">
                                <xsl:value-of select="number(number($selectedRowCount))" />
                              </xsl:if>
                              <xsl:if test="number($Page)+10 &lt; number($selectedRowCount)">
                                <xsl:value-of select="number(number($Page)+10)" />
                              </xsl:if>
                            </xsl:variable>
                            <xsl:if test="number($FN) &gt; 1">
                              <td>
                                <a id="ContentPlaceHolderMain_dlPaging_lnkbtnPaging_first" style="cursor:pointer;font-size:Medium;font-weight:bold;">
                                  <xsl:attribute name="href">
                                    <xsl:text disable-output-escaping="yes"><![CDATA[Index.html?page=]]></xsl:text><xsl:value-of select="number($FI)"/>
                                  </xsl:attribute>
                                  <xsl:text disable-output-escaping="yes"><![CDATA[...]]></xsl:text>
                                </a>
                                <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                              </td>
                            </xsl:if>
                            <xsl:call-template name="pagingnumbers">
                              <xsl:with-param name="intcount" select="$FN"/>
                              <xsl:with-param name="maxvalue" select="$LN"/>
                            </xsl:call-template>
                            <xsl:if test="number($LN) &lt;= number($selectedRowCount)">
                              <td>
                                <a id="ContentPlaceHolderMain_dlPaging_lnkbtnPaging_last" style="cursor:pointer;font-size:Medium;font-weight:bold;">
                                  <xsl:attribute name="href">
                                    <xsl:text disable-output-escaping="yes"><![CDATA[Index.html?page=]]></xsl:text><xsl:value-of select="number($LI)"/>
                                  </xsl:attribute>
                                  <xsl:text disable-output-escaping="yes"><![CDATA[...]]></xsl:text>
                                </a>
                                <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                              </td>
                            </xsl:if>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" style="height: 30px;" valign="middle">
                      <span id="ContentPlaceHolderMain_lblPageInfo" style="font-size:Medium;font-weight:bold;">
                        Page <xsl:value-of select="number($Page)" /> of <xsl:value-of select="number($selectedRowCount)"/>
                      </span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </xsl:template>
</xsl:stylesheet>