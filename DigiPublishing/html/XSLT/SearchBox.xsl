<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="DigipDomain"/>
<xsl:template match="/">
  <div class="searchbox">
    <div id="divSearchForm">
      <div class="dvSearch">
        <input type="text" id="txtSearch" class="txtSearch" placeholder="Search" maxlength="24" />
        <input type="image" id="btnsearch" class="btnSearch" src="../Images_Common/search.png" onclick="javascript:return SearchFeature();" />
      </div>
      <span id="rfvSearch" style="color:Red;display:none;">Please enter text to search.</span>
    </div>
  </div>
</xsl:template>
</xsl:stylesheet> 
