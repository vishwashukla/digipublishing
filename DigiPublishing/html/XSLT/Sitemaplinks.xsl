<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:template match="/">
    <xsl:if test="count(Links/Link) &gt; 0">
      <h3 class="HeaderClass">
        Links
      </h3>
    </xsl:if>
    <br />
    <p>
      <span id="ContentPlaceHolderMain_lblBookmarks">
        <xsl:for-each select="Links/Link">
          <xsl:if test="position() &gt; 1">
            <xsl:text disable-output-escaping ="yes"><![CDATA[, ]]></xsl:text>
          </xsl:if>
          <xsl:value-of select="SiteMaplink"/>
        </xsl:for-each>
      </span>
    </p>
    <p>
      <br/>
    </p>
  </xsl:template>
</xsl:stylesheet> 
