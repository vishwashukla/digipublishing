﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:param name="OnFeatures"/>
  <xsl:template match="/">
    <li id="nav-home">
      <div id="divHome" style="display: none;">
        <a id="ctl00_lnkHome" style="cursor: pointer;" href="Index.html">
          Home
        </a>
      </div>
    </li>
    <li id="nav-blog">
      <xsl:choose>
        <xsl:when test="contains($OnFeatures, '^4^')">
          <div id="divImageBlog" >
            <a id="ctl00_lnkBlog" style="cursor: pointer;" href="Index.html">
              Features
            </a>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div id="divImageBlog1" >
            <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </li>
    <li id="nav-rss">
      <xsl:choose>
        <xsl:when test="contains($OnFeatures, '^5^')">
          <div id="divImageRssreader">
            <a id="ctl00_lnkRssreader" style="cursor: pointer;" href="Feeds.html">
              Feeds
            </a>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div id="divImageRssreader1" >
            <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </li>
    <li id="nav-book">
      <xsl:choose>
        <xsl:when test="contains($OnFeatures, '^6^')">
          <div id="divImageBookmark">
            <a id="ctl00_lnkBookmark" style="cursor: pointer;" href="Links.html">
              Links
            </a>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div id="divImageBookmark1" >
            <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </li>
    <li id="Li1">
      <xsl:choose>
        <xsl:when test="contains($OnFeatures, '^3^')">
          <div id="divImageCalendar">
            <a id="ctl00_lnkCalendar" style="cursor: pointer;" href="Calendars.html">
              Calendar
            </a>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div id="divImageCalendar1" >
            <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </li>
    <li id="nav-getintouch">
      <div id="divGetInTouch">
        <a id="ctl00_lnkGetInTouch" href="ContactUs.html">Get in touch</a>
      </div>
    </li>
  </xsl:template>
</xsl:stylesheet>
