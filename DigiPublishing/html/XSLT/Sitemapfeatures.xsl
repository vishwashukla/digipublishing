<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:template match="/">
    <xsl:if test="count(Articles/Article) &gt; 0">
      <h3 class="HeaderClass">
        Features
      </h3>
    </xsl:if>
    <br />
    <p>
      <span id="ContentPlaceHolderMain_lblBlogs">
        <xsl:for-each select="Articles/Article">
          <xsl:variable name="PageSize" select="PostPerPage"></xsl:variable>
          <xsl:variable name="Page" select="floor((number(position())-1) div $PageSize)"></xsl:variable>
          <xsl:if test="position() &gt; 1">
            <br/>
          </xsl:if>
          <a>
            <xsl:attribute name ="href">
              <xsl:text disable-output-escaping="yes"><![CDATA[Index.html?page=]]></xsl:text><xsl:value-of select="number($Page)+1"/><xsl:text disable-output-escaping="yes"><![CDATA[&featureid=]]></xsl:text><xsl:value-of select="PostID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TitleForLink"/>
          </a>
        </xsl:for-each>
      </span>
    </p>
    <p>
      <br />
    </p>
  </xsl:template>
</xsl:stylesheet> 
