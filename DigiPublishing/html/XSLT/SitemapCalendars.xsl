<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:template match="/">
    <xsl:if test="count(Calendars/Calendar) &gt; 0">
      <h3 class="HeaderClass">
        Calendars
      </h3>
    </xsl:if>
    <br/>
    <p>
      <span id="ContentPlaceHolderMain_lblCalendar">
        <xsl:for-each select="Calendars/Calendar">
          <xsl:if test="position() &gt; 1">
            <xsl:text disable-output-escaping ="yes"><![CDATA[, ]]></xsl:text>
          </xsl:if>
          <a>
            <xsl:attribute name ="href">
              <xsl:text disable-output-escaping ="yes"><![CDATA[Calendars.html?calendarid=]]></xsl:text>
              <xsl:value-of select="CalendarID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CalendarTitle"/>
          </a>
        </xsl:for-each>
      </span>
    </p>
    <p>
      <br/>
      </p>
  </xsl:template>
</xsl:stylesheet> 
