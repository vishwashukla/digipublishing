<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:param name="UpdateIDParam"></xsl:param>
  <xsl:template match="/">
    <h4>
      <span id="ContentPlaceHolderMain_lblRss">
        <xsl:for-each select ="Updates/Update">
          <xsl:if test="number($UpdateIDParam)=number(UpdateID)">
            <xsl:value-of select="UpdateTitle" disable-output-escaping="yes"/>
          </xsl:if>
        </xsl:for-each>
      </span>
    </h4>
    <br />
    <xsl:for-each select ="Updates/Update">
      <xsl:if test="number($UpdateIDParam)=number(UpdateID)">
        <table id="ContentPlaceHolderMain_dlstFeed" border="0" style="border-collapse:collapse;">
          <tr>
            <td>
              <img alt="loading" id="img_loading">
                <xsl:attribute name="src">
                  <xsl:text disable-output-escaping="yes"><![CDATA[/Images_Common/loading.gif]]></xsl:text>
                </xsl:attribute>
              </img>
            </td>
          </tr>
        </table>
      </xsl:if>
    </xsl:for-each>
    <div id="ContentPlaceHolderMain_pnlPagingMain">
        <div id="ContentPlaceHolderMain_dlPaging">
          <ul class="ulPagingFeeds">
            <xsl:for-each select="Updates/Update">
              <li class="liPagingFeeds">
                <a>
                  <xsl:choose>
                    <xsl:when test="number($UpdateIDParam)=number(UpdateID)">
                      <xsl:attribute name="class">
                        <xsl:text disable-output-escaping="yes"><![CDATA[feedpaging-disabled]]></xsl:text>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="class">
                       <xsl:text disable-output-escaping="yes"><![CDATA[ feedpaging-enabled]]></xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="href">
                        <xsl:text disable-output-escaping="yes"><![CDATA[Feeds.html?feedid=]]></xsl:text><xsl:value-of select="UpdateID"/>
                      </xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:value-of disable-output-escaping="yes" select="UpdatetitleForLink"/>
                </a>
              </li>
            </xsl:for-each>
          </ul>
        </div>
        <xsl:variable name="Count" select="count(Updates/Update)"/>
        <xsl:for-each select="Updates/Update">
          <xsl:if test="number($UpdateIDParam)=number(UpdateID)">
            <div style="height: 30px; text-align: center; margin: 10px 0 0 0;">
              <span id="ContentPlaceHolderMain_lblPageInfo" style="font-size:11pt;font-weight:bold;">
                Page <xsl:value-of select="position()" /> of <xsl:value-of select="$Count"/>
              </span>
            </div>
          </xsl:if>
        </xsl:for-each>
      </div>
  </xsl:template>
</xsl:stylesheet>