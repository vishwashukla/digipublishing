<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="DigipDomain"/>
<xsl:template match="/">
  <h2 id="ctl00_BookmarkSummary_Heading" class="surfLinks">
    Suggested Places For You<br></br><span class="SummaryByClass">by <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" /></span>
  </h2>
  <table class="surfLinksContainer" border="0" id="ctl00_drgdList" style="border-collapse:collapse;">
    <tbody id="surfLinksContainerBody">
      <xsl:for-each select="Root/Links">
        <xsl:variable name="index" select="position() - 1"/>
        <tr class="surfLinktr">
          <td>
            <img alt="Icon20x20" class="img">
              <xsl:attribute name="src">
                <xsl:text disable-output-escaping="yes"><![CDATA[../Config/]]></xsl:text>
                <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" />
                <xsl:text disable-output-escaping="yes"><![CDATA[/Icon20x20.png]]></xsl:text>
              </xsl:attribute>
            </img>
          </td>
         
          <td style="position:relative;" class="SummaryLinktd">
              <xsl:attribute name="id">
                <xsl:text disable-output-escaping="yes"><![CDATA[SummaryLinktd_]]></xsl:text><xsl:value-of select="ALinkID" disable-output-escaping="yes"/>
              </xsl:attribute>
              <xsl:value-of select="ALink" disable-output-escaping="yes"/>
          </td>
        </tr>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>
</xsl:stylesheet> 
