﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:param name="DigipDomain"/>
  <xsl:param name="FooterDate"/>
  <xsl:param name="OnFeatures"/>
  <xsl:template name="image">
    <img alt="Icon20x20" class="imageiconfooter">
      <xsl:attribute name="src">
        <xsl:text disable-output-escaping="yes"><![CDATA[../Config/]]></xsl:text>
        <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" />
        <xsl:text disable-output-escaping="yes"><![CDATA[/Icon20x20.png]]></xsl:text>
      </xsl:attribute>
    </img>
  </xsl:template>
  <xsl:template match="/">
    <table id="footerlinks1" border="0" width="100%" style="position:relative;">
      <tr>
				<td class="footer-sep" colspan="3">
				</td>
			</tr>
      <tr style="vertical-align:top;">
        <td style="text-align:left;" class="fooler-td">
          <div id="divHomeFooter1">
            <xsl:call-template name="image"></xsl:call-template>
            <a id="ctl00_lnkHomeFooter1" href="Index.html">Home</a>
          </div>
          <xsl:if test="contains($OnFeatures, '^4^')">
            <div id="divImageBlogFooter1">
              <xsl:call-template name="image"></xsl:call-template>
              <a id="ctl00_lnkBlogFooter1" href="Index.html">Features</a>
            </div>
          </xsl:if>
          <xsl:if test="contains($OnFeatures, '^3^')">
            <div id="divImageCalendarFooter1">
              <xsl:call-template name="image"></xsl:call-template>
              <a id="ctl00_lnkCalendarFooter1" href="Calendars.html">Calendar</a>
            </div>
          </xsl:if>
          <xsl:if test="contains($OnFeatures, '^5^')">
            <div id="divImageRssreaderFooter1">
              <xsl:call-template name="image"></xsl:call-template>
              <a id="ctl00_lnkRssreaderFooter1" href="Feeds.html">Feeds</a>
            </div>
          </xsl:if>
          <xsl:if test="contains($OnFeatures, '^6^')">
            <div id="divImageBookmarkFooter1">
              <xsl:call-template name="image"></xsl:call-template>
              <a id="ctl00_lnkBookmarkFooter1" href="Links.html">Links</a>
            </div>
          </xsl:if>
        </td>
        <td class="footer-col fooler-td">
          <div id="divGetInTouchFooter1">
            <xsl:call-template name="image"></xsl:call-template>
            <a id="ctl00_lnkGetInTouchFooter1" href="ContactUs.html">Get in touch</a>
          </div>
          <div id="divTermsFooter1">
            <xsl:call-template name="image"></xsl:call-template>
            <a href="Terms.html" target="_parent" style="font-style: normal; font-weight: lighter">Terms</a>
          </div>
          <div id="divprivacyPolicyFooter1">
            <xsl:call-template name="image"></xsl:call-template>
            <a href="PrivacyPolicy.html" target="_parent" style="font-style: normal; font-weight: lighter">Privacy Policy</a>
          </div>
          <div id="divSiteMapFooter1">
            <xsl:call-template name="image"></xsl:call-template>
            <a href="Sitemap.html" target="_parent" style="font-style: normal; font-weight: lighter">Sitemap</a>
          </div>
          <div id="divRssFooter1">
            <xsl:call-template name="image"></xsl:call-template>
            <a id="footerRssFeed1" target="_parent" style="font-style: normal; font-weight: lighter">
              <xsl:attribute name="href">
                <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" />
                <xsl:text disable-output-escaping="yes"><![CDATA[/RSSFeed.xml]]></xsl:text>
              </xsl:attribute>
              RSS
            </a>
          </div>
        </td>
        <td class="footer-col fooler-td">
          <div id="CopyRight1">
            Copyright © <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" />
          </div>
          <div id="DateFooter1">
            <xsl:value-of disable-output-escaping="yes" select="$FooterDate" />
          </div>
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
