<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="DigipDomain"/>
<xsl:template match="/">
  <h2 id="MostPopular_Heading" class="surfMostPopular">
    Most Popular
  </h2>
  <table class="surfLinksContainer" border="0" id="dgrdMostPopular" style="border-collapse:collapse;">
    <tbody>
      <xsl:for-each select="Root/Features">
        <tr>
          <td class="MostPopularNum">
            <xsl:value-of select="position()" disable-output-escaping="yes"/>
          </td>
          <td>
            <span>
              <xsl:value-of select="FeatureLink" disable-output-escaping="yes"/>
            </span>
          </td>
        </tr>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>
</xsl:stylesheet> 
