<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="DigipDomain"/>
<xsl:template match="/">
  <h2 id="ctl00_rssSummaryHeading" class="surfNews">
    More News Stories<br></br><span class="SummaryByClass">by <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" />
  </span>
  </h2>
  <table class="surfNewsContainer" border="0" id="ctl00_dgrdSurfRSS" style="border-collapse:collapse;">
    <tbody>
      <xsl:for-each select="Root/Updates">
        <tr>
          <td>
            <img alt="Icon20x20" class="img">
              <xsl:attribute name="src">
                <xsl:text disable-output-escaping="yes"><![CDATA[../Config/]]></xsl:text>
                <xsl:value-of disable-output-escaping="yes" select="$DigipDomain" />
                <xsl:text disable-output-escaping="yes"><![CDATA[/Icon20x20.png]]></xsl:text>
              </xsl:attribute>
            </img>
          </td>
         
          <td>
            <a>
              <xsl:attribute name="href">
                <xsl:text disable-output-escaping="yes"><![CDATA[Feeds.html?feedid=]]></xsl:text><xsl:value-of select="UpdateID"/>
              </xsl:attribute>
              <xsl:value-of select="UpdateTitle" disable-output-escaping="yes"/>
            </a>
          </td>
        </tr>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>
</xsl:stylesheet> 
