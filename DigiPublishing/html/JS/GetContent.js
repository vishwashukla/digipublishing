var UserHostAddress = '', Time = '', GAID = '', UserBrowser = '', UserCountryID = '', UserOS = '', topurl = document.location.href;
//widget vars
var featureson = "";
//General
var SameIPCountMidArticle = 0, MidArticleBlockerCount = 0;
var ISSendGAVideoImpression = "0";
var ISSendGAOutboundLink = "0";
var ISSendGAVideoAdStart = "0";
var ISSendGAVideoAdFinish = "0";
var ISSendGABannerClick = "0";
var ISSendGAVideoAdClick = "0";
var UserIP = "";
var UserAgent = "";

var XMLWidgets;

function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("btnbacktotop").style.display = "block";
    } else {
        document.getElementById("btnbacktotop").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function backtotopFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
function ReturnObj(url, Callback) {
    var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var a = (xmlhttp.responseXML == null) ? xmlhttp.responseText : xmlhttp.responseXML;
            Callback(a);
        }

    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
function ReturnObjSync(url) {
    var a = "";
    var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            a = (xmlhttp.responseXML == null) ? xmlhttp.responseText : xmlhttp.responseXML;
        }

    }
    xmlhttp.open("GET", url, false);
    xmlhttp.send();
    return a;
}
function GetDomainName() {
    var stringURL = document.location.href;
    if (stringURL.indexOf("localhost") > -1)
        return "whatbiz.co";
    var bits = stringURL.split("http://");
    var noHTTP = bits[bits.length - 1];
    var site = noHTTP.split("/");
    return site[0].replace("www.", "");
}
var DomainForGA = GetDomainName();
XMLWidgets = ReturnObjSync(DomainForGA + "/Widgets.xml");
var initResp = ReturnObjSync("Widget_Click_Update.aspx?IPTime=YES");
var res = initResp.split("#$#");
UserBrowser = res[3];
UserOS = res[12];
UserCountryID = res[4];
UserIP = res[13];
UserAgent = res[14];
function Init(type) {
    //var callback = function (response) {
    var res = initResp.split("#$#");
    UserHostAddress = res[0];
    Time = res[1];
    GAID = res[2];
    //UserBrowser = res[3];
    //UserCountryID = res[4];
    ISSendGAVideoImpression = res[5];
    ISSendGAOutboundLink = res[6];
    ISSendGAVideoAdStart = res[7];
    ISSendGAVideoAdFinish = res[8];
    ISSendGABannerClick = res[9];
    ISSendGAVideoAdClick = res[10];
    //UserOS = res[12];
    featureson = "^" + res[11] + "^";
    //UserIP = res[13];
    //UserAgent = res[14];
    if (type == 'link') {
        LoadLinks();
    }
    if (type == 'blogpostsummary') {
        LoadBlogPostSummary();
    }
    if (type == 'calendar') {
        LoadCalendar();
    }
    if (type == 'contactus') {
        LoadRecommended();
    }
    if (type == 'feeds') {
        LoadFeeds();
    }
    if (type == 'home') {
        LoadHome();
    }
    if (type == 'index') {
        LoadArticles();
    }
    if (type == 'privacy') {
        LoadRecommended();
    }
    if (type == 'search') {
        LoadSearces();
    }
    if (type == 'sitemap') {
        LoadSiteMap();
    }
    if (type == 'term') {
        LoadRecommended();
    }
    // };
    //ReturnObj("Widget_Click_Update.aspx?IPTime=YES", callback);
}
//Init();
function FooterDate() {
    var dt = new Date();
    var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return "Date: " + (dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate()) + "  " + month[dt.getMonth()] + " " + dt.getFullYear();
}

function GetQS(key) {
    //Get querystring  
    var Qs = window.location.search.substring(1).replace(/^\s+|\s+$/g, '');
    var a = "0";
    if (Qs != "") {
        var qsArr = Qs.split("&");
        for (var i = 0; i < qsArr.length; i++)
            if (qsArr[i].split("=")[0] == key)
                a = qsArr[i].split("=")[1];
    }
    return a;
}
function ApplyXsltOnXml(Fxml, Fxsl, id, PCount, PNames, PValues) {
    if (this.supportActiveX = ("ActiveXObject" in window)) {
        var xslt = new ActiveXObject("Msxml2.XSLTemplate.6.0"), xsldoc = new ActiveXObject("Msxml2.FreeThreadedDOMDocument.6.0"), xmldoc = new ActiveXObject("Msxml2.DOMDocument.6.0");
        xsldoc.async = false;
        xsldoc.load(Fxsl);
        xmldoc.async = false;
        xmldoc.load(DomainForGA + "/" + Fxml);
        if (xsldoc.parseError.errorCode == 0 && xmldoc.parseError.errorCode == 0) {
            xslt.stylesheet = xsldoc;
            var xslproc = xslt.createProcessor();
            xslproc.input = xmldoc;
            if (PCount != 0)
                for (i = 0; i < PCount; i++)
                    xslproc.addParameter(PNames.split(',')[i], PValues.split(',')[i]);
            xslproc.transform();
            document.getElementById(id).innerHTML = xslproc.output;
        }
    }
    else if (document.implementation && document.implementation.createDocument) {
        var xml = ReturnObjSync(DomainForGA + "/" + Fxml);
        var xsl = ReturnObjSync(Fxsl);
        var xslProc = new XSLTProcessor();
        xslProc.importStylesheet(xsl);
        if (PCount != 0)
            for (i = 0; i < PCount; i++)
                xslProc.setParameter(null, PNames.split(',')[i], PValues.split(',')[i]);
        var ex = new XMLSerializer().serializeToString(xslProc.transformToFragment(xml, document));
        if (ex.indexOf("XML Parsing Error:") == -1)
            document.getElementById(id).innerHTML = ex.replace(/&amp;/gi, "&").replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
        /*var callback = function (xml) {
            var callback2 = function (xsl) {
                var xslProc = new XSLTProcessor();
                xslProc.importStylesheet(xsl);
                if (PCount != 0)
                    for (i = 0; i < PCount; i++)
                        xslProc.setParameter(null, PNames.split(',')[i], PValues.split(',')[i]);
                var ex = new XMLSerializer().serializeToString(xslProc.transformToFragment(xml, document));
                if (ex.indexOf("XML Parsing Error:") == -1)
                    document.getElementById(id).innerHTML = ex.replace(/&amp;/gi, "&").replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
            }
            ReturnObj(Fxsl, callback2);
        }
        ReturnObj(DomainForGA + "/" + Fxml, callback);*/
    }
}
function OSCheck(UO, WO) {
    var flag = true;
    if (WO != "") {
        flag = false;
        WO = "," + WO + ",";
        if (WO.indexOf("," + UO + ",") > -1)
            flag = true;
    }
    return flag;
}
function CountryCheck(UC, WC) {
    var flag = true;
    if (WC != "") {
        flag = false;
        WC = "," + WC + ",";
        if (WC.indexOf("," + UC + ",") > -1)
            flag = true;
    }
    return flag;
}
function ReferalCheck(RE, NR) {
    var flag = true;
    if (RE != "") {
        flag = false;
        var aRE = RE.split(',');
        for (var i = 0; i < aRE.length; i++) {
            aRE[i] = aRE[i].replace(/^\s+|\s+$/g, '');
            if (topurl.indexOf(aRE[i]) > -1)
                flag = true;
        }
    }
    if (NR != "") {
        var aNR = NR.split(',');
        for (var i = 0; i < aNR.length; i++) {
            aNR[i] = aNR[i].replace(/^\s+|\s+$/g, '');
            if (topurl.indexOf(aNR[i]) > -1)
                flag = false;
        }
    }
    return flag;
}
function SetDisplay(id, val) {
    if (id.indexOf("imageMidArticleBlock") == -1) {
        var a = document.getElementById(id);
        if (a != null) a.style.display = val;
    }
    else {
        var a = document.getElementsByClassName("midArticle_ads_Box");
        for (var i = 0; i < a.length; i++) {
            var ctr = a[i].getElementsByTagName("img");
            for (var j = 0; j < ctr.length; j++) {
                if (ctr[j].id.indexOf("imageMidArticleBlock") > -1) {
                    ctr[j].style.display = val;
                }
            }
        }
    }
}
function RecordData(str) {
    var url = "Banner_Click_UPD.aspx?" + str;
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", url, true);
    xhttp.send();
}
function GABanner(ename, bname, dname) {
    try {
        if (bname != "") {
            if (typeof _gaq != "undefined") {
                _gaq.push(['_trackEvent', ename, bname, dname]);
            }
            else if (typeof ga != "undefined") {
                ga('send', 'event', ename, bname, dname);
            }
        }
    } catch (err) { }
}

//SiteMap
function LoadSiteMap() {
    LoadRecommended();
    ApplyXsltOnXml('Articles.xml', 'XSLT/Sitemapfeatures.xsl', "ContentPlaceHolderMain_divBlogs", 0, "", "");
    ApplyXsltOnXml('Updates.xml', 'XSLT/SitemapFeeds.xsl', "ContentPlaceHolderMain_divRss", 0, "", "");
    ApplyXsltOnXml('Links.xml', 'XSLT/Sitemaplinks.xsl', "ContentPlaceHolderMain_divBookmarks", 0, "", "");
    ApplyXsltOnXml('Calendars.xml', 'XSLT/SitemapCalendars.xsl', "ContentPlaceHolderMain_divCalendar", 0, "", "");
    contentHeight();
}
//ContactUS
function ContactUS() {
    var a = true;
    var User = document.getElementById("ctl00_ContentPlaceHolderMain_txtUsername").value;
    var Email = document.getElementById("ctl00_ContentPlaceHolderMain_txtEmail").value;
    var Comment = document.getElementById("ctl00_ContentPlaceHolderMain_txtComment").value;
    SetDisplay("spantxtUsername", "none");
    SetDisplay("spantxtEmail", "none");
    SetDisplay("spaninvalidEmail", "none");
    SetDisplay("spantxtComment", "none");
    if (User == "") {
        SetDisplay("spantxtUsername", "block");
        a = false;
    }
    if (Email == "") {
        SetDisplay("spantxtEmail", "block");
        a = false;
    }
    else if (!(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/).test(Email)) {
        SetDisplay("spaninvalidEmail", "block");
        a = false;
    }
    if (Comment == "") {
        SetDisplay("spantxtComment", "block");
        a = false;
    }
    if (!a)
        return a;
    var callback = function (b) {
        var ctrl = document.getElementById("content-wrapper");
        if (b == "Fail")
            ctrl.innerHTML = "<p>An error occured.</p>";
        else if (b == "Success")
            ctrl.innerHTML = "<p>Comments have been sent to the site owner.</p>";
    };
    ReturnObj("Widget_Click_Update.aspx?UName=" + User + "&UEmail=" + Email + "&Comment=" + Comment, callback);
    return false;
}
//Features
function LoadArticles() {
    document.title = DomainForGA + " : Articles";
    LoadRecommended();
    var p = parseInt(GetQS("page"));
    var ID = parseInt(GetQS("featureid"));
    var xsl = 'XSLT/Articles.xsl';
    if (ID != 0)
        xsl = 'XSLT/SingleArticle.xsl';
    if (p == 0) p = 1;
    ApplyXsltOnXml('Articles.xml', xsl, "blogpost", 4, "Page,ArticleID,PageURL,PostedBY"
        , (p + "," + ID + "," + DomainForGA + "," + DomainForGA.split(".")[0]));
    if (XMLWidgets.getElementsByTagName("MidWidgets").length > 0) {
        var callback = function (res) {
            var MidBannerid = parseInt(nodeval(XMLWidgets.getElementsByTagName("MidWidgets")[0].getElementsByTagName("MidWidget")[0], "id"));
            var SameIPDaysmidarticle = parseInt(nodeval(XMLWidgets.getElementsByTagName("MidWidgets")[0].getElementsByTagName("MidWidget")[0], "SameIPDays"));
            SameIPCountMidArticle = parseInt(res);
            var sameIPValueMidArticle = parseInt(nodeval(XMLWidgets.getElementsByTagName("MidWidgets")[0].getElementsByTagName("MidWidget")[0], "SameIPValue"));
            var sameIPStatusMidArticle = parseInt(nodeval(XMLWidgets.getElementsByTagName("MidWidgets")[0].getElementsByTagName("MidWidget")[0], "SameIPStatus"));
            RecordData("BannerPageImp=" + MidBannerid);
            if (sameIPStatusMidArticle == 1 && sameIPValueMidArticle <= SameIPCountMidArticle)
                SetDisplay("imageMidArticleBlock", "block");

            contentHeight();
        };
        ReturnObj("Banner_Click_UPD.aspx?SameIPClicksBanner=" + MidBannerid + "&SameIPDays=" + SameIPDaysmidarticle, callback)
    }
}
function trimStart(character, string) {
    var startIndex = 0;
    while (string[startIndex] === character) {
        startIndex++;
    }
    return string.substr(startIndex);
}
//Blog Post Summary
function LoadBlogPostSummary() {
    document.title = DomainForGA + " : Articles Summary";
    LoadRecommended();
    var callback = function (xmlarticles) {
        var article = xmlarticles.getElementsByTagName("Article");
        if (article.length > 0) {
            var callback2 = function (xmlHomepage) {
                var endindex = parseInt(xmlHomepage.getElementsByTagName("BlogsInSummary")[0].childNodes[0].nodeValue);
                if (endindex == 0)
                    endindex = 10;
                if (endindex > article.length)
                    endindex = article.length;
                var countBPS = 0;
                var strHTML = "";
                for (var i = 0; i < endindex; i++) {
                    var postTitle = nodeval(article[i], "Title");
                    var postId = nodeval(article[i], "PostID");
                    var PostPerPage = parseInt(nodeval(article[i], "PostPerPage"));
                    var posturl = "Index.html?page=" + (parseInt(i / PostPerPage) + 1) + "&featureid=" + postId;
                    var postdesc = nodeval(article[i], "Description");
                    var imageSrc = "";
                    var imagealt = "";
                    if (postdesc.indexOf("<img ") != -1) {
                        var a = postdesc.replace("<img ", "^").split('^')[1];
                        var b = a.replace("src=", "^").split('^')[1];
                        if (b.indexOf("\"") == 0) {
                            imageSrc = trimStart('"', b.split('"')[1]);
                        }
                        else {
                            imageSrc = trimStart('"', b.split('\'')[1]);
                        }
                        var newsrc = imageSrc.split("/")[imageSrc.split("/").length - 1];
                        imagealt = newsrc.split(".")[0].split("-").join(" ").split("_").join(" ");
                    }
                    var tmp = document.createElement("DIV");
                    tmp.innerHTML = postdesc;
                    var plaindesc = tmp.textContent.trim() || tmp.innerText.trim() || "";
                    if (plaindesc.length > 300) {
                        plaindesc = plaindesc.substring(0, 297) + "...";
                    }
                    countBPS++;
                    if (i % 3 == 0) {
                        strHTML += "<tr>";
                    }
                    strHTML += "<td style=\"padding:5px 5px 15px 5px;vertical-align: top;\">";
                    strHTML += "<a href=\"" + posturl + "\">";
                    strHTML += "<div style=\"width:212px;height:109px;\">";
                    strHTML += "<img name=\"homepage1_s1\" src=\"" + imageSrc + "\" class=\"blogimg\" alt=\"" + imagealt + "\" border=\"0\" height=\"109\" width=\"211\">";
                    strHTML += "</div>";
                    strHTML += "<p class=\"style2\" style=\"margin:0px;margin-top:10px;\">";
                    strHTML += "<span style=\"color: rgb(128, 128, 128);\">";
                    strHTML += "<strong>";
                    strHTML += "<span style=\"font-size: larger;\">" + postTitle + "</span>";
                    strHTML += "</strong>";
                    strHTML += "</span>";
                    strHTML += "</p>";
                    strHTML += "<p class=\"style2\" style=\"margin:0px\">&nbsp;</p>";
                    strHTML += "<p class=\"style2\" style=\"margin:0px\">";
                    strHTML += plaindesc;
                    strHTML += "</p>";
                    strHTML += "</a>";
                    strHTML += "</td>";
                    if (i == endindex - 1) {
                        if (countBPS == 3) {
                            strHTML += "</tr>";
                        }
                        else if (countBPS == 2) {
                            strHTML += "<td></td></tr>";
                        }
                        else if (countBPS == 1) {
                            strHTML += "<td></td><td></td></tr>";
                        }
                    }
                    else {
                        if (countBPS == 3) {
                            strHTML += "</tr>";
                            countBPS = 0;
                        }
                    }
                }
                document.getElementById("tableBPS").innerHTML = strHTML;
                contentHeight();
            };
            ReturnObj(DomainForGA + "/Home.xml", callback2);
        }
        else {
            contentHeight();
        }
    };
    ReturnObj((DomainForGA + "/Articles.xml"), callback);
}
//Recomended
function LoadRecommended() {
    LoadLogo_Favicon();
    var url = window.top.location.toString().toLowerCase();
    var xml = "LatestLinksUpdates.xml";
    if (featureson.indexOf("^7^") != -1) {
        ApplyXsltOnXml(xml, 'XSLT/SearchBox.xsl', "Searchbox", 1, "DigipDomain", DomainForGA);
    }
    else {
        SetDisplay("Searchbox", "none");
    }
    if (featureson.indexOf("^8^") != -1 && url.indexOf("/contactus.html") == -1) {
        ApplyXsltOnXml(xml, 'XSLT/LatestUpdates.xsl', "surfNewsBox", 1, "DigipDomain", DomainForGA);
    }
    else {
        SetDisplay("surfNewsBox", "none");
    }
    if (featureson.indexOf("^9^") != -1 && url.indexOf("/contactus.html") == -1) {
        ApplyXsltOnXml(xml, 'XSLT/LatestLinks.xsl', "surfLinksBox", 1, "DigipDomain", DomainForGA);

        //here random
        var Linksxml = ReturnObjSync((DomainForGA + "/LatestLinksUpdates.xml"));
        var Random = nodeval(Linksxml.getElementsByTagName("LinksSetting")[0], "Random");
        var Top = parseInt(nodeval(Linksxml.getElementsByTagName("LinksSetting")[0], "Top"));
        var old_tbody = document.getElementById("surfLinksContainerBody");
        var arrayTrs = old_tbody.getElementsByClassName("surfLinktr");
        var newarrayTrs;
        if (Random == 'True') {
            arrayTrs = Array.prototype.slice.call(arrayTrs);
            newarrayTrs = shuffle(arrayTrs);
        }
        else {
            newarrayTrs = arrayTrs;
        }

        var new_tbody = document.createElement('tbody');
        new_tbody.id = "surfLinksContainerBody";
        var endelementindex = Top < newarrayTrs.length ? Top : newarrayTrs.length;
        for (var i = 0; i < endelementindex; i++) {
            new_tbody.appendChild(newarrayTrs[i]);
        }
        old_tbody.parentNode.replaceChild(new_tbody, old_tbody);

        var surfLinksBox = document.getElementById("surfLinksBox");
        var alltd = surfLinksBox.getElementsByClassName("SummaryLinktd");
        surfLinksBox.style.display = "none";
        for (var i = 0; i < alltd.length; i++) {
            var linkid = alltd[i].getAttribute("id").replace("SummaryLinktd_", "");
            var allspan = alltd[i].getElementsByTagName("span");
            var lastspan = allspan[allspan.length - 1];
            var sid = lastspan.getAttribute("id").replace("_SummarySameIPClicksLink", "");
            var firstspan = allspan[0];
            var blockdata = firstspan.innerText.split(",");
            var did = blockdata[4];
            var lblockdata1 = document.getElementById("SummaryLinkData_" + sid).innerText.split(",");
            var sis1 = parseInt(lblockdata1[2]);
            var blocker = parseInt(lblockdata1[0]);
            console.log("------", lblockdata1);
            if (sis1 == 1 || blocker == 1) {
                var urltemp = "Link_Click_UPD.aspx?linkIDSI=" + linkid + "&domainID=" + did + "&spanid=" + sid;
                var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
                xhttp.onreadystatechange = function () {
                    if (xhttp.readyState == 4 && xhttp.status == 200) {
                        var res = xhttp.responseText.split(",");
                        var spanid = res[0];
                        var clickval = parseInt(res[1]);
                        var lblockdata = document.getElementById("SummaryLinkData_" + spanid).innerText.split(",");
                        var sis = parseInt(lblockdata[2]);
                        var siv = parseInt(lblockdata[3]);
                        if (siv <= 0) {
                            sis = 0;
                        }
                        if (sis == 1 && siv <= clickval) {
                            document.getElementById(spanid + "_SummaryLinkBlockImage").parentElement.parentElement.style.display = "none";
                        }
                        document.getElementById(spanid + "_SummarySameIPClicksLink").innerText = clickval;
                    }
                }
                xhttp.open("GET", urltemp, false);
                xhttp.send();
            }
        }
        surfLinksBox.style.display = "block";
    }
    else {
        SetDisplay("surfLinksBox", "none");
    }
    if (featureson.indexOf("^10^") != -1) {
        ApplyXsltOnXml(xml, 'XSLT/MostPopular.xsl', "MostPopularBox", 1, "DigipDomain", DomainForGA);
    }
    else {
        SetDisplay("MostPopularBox", "none");
    }
    if (featureson.indexOf("^21^") != -1) {
        ApplyXsltOnXml(xml, 'XSLT/Subscribe.xsl', "Subscribe", 1, "DigipDomain", DomainForGA);
    }
    else {
        SetDisplay("Subscribe", "none");
    }
    if (featureson.indexOf("^20^") == -1) {
        SetDisplay("div_share", "none");
    }
    ApplyXsltOnXml(xml, 'XSLT/Header.xsl', "nav", 1, "OnFeatures", featureson);
    ApplyXsltOnXml(xml, 'XSLT/Footer.xsl', "footerdiv", 3, "DigipDomain,FooterDate,OnFeatures", DomainForGA + "," + FooterDate() + "," + featureson);
    ApplyXsltOnXml(xml, 'XSLT/Footer2.xsl', "footerdiv2", 3, "DigipDomain,FooterDate,OnFeatures", DomainForGA + "," + FooterDate() + "," + featureson);
    scrollFunction();
    contentHeight();
}
//Links
function LoadLinks() {
    document.title = DomainForGA + " : Links";
    LoadRecommended();
    var p = parseInt(GetQS("page"));
    if (p == 0) p = 1;
    var PageSize = 30;
    var Linksxml = ReturnObjSync((DomainForGA + "/Links.xml"));
    var Random = nodeval(Linksxml.getElementsByTagName("LinksSetting")[0], "Random");
    var Top = parseInt(nodeval(Linksxml.getElementsByTagName("LinksSetting")[0], "Top"));
    if (Top != 0) {
        PageSize = Top;
    }
    ApplyXsltOnXml('Links.xml', 'XSLT/Links.xsl', "content-full", 2, "Page,PageSize", p.toString() + "," + PageSize);
    //here
    var surfLinksBox = document.getElementById("content-full");
    surfLinksBox.style.display = "none";


    var old_tbody = document.getElementById("dgrdBookmarkBody");
    var arrayTrs = old_tbody.getElementsByClassName("MainLinktr");
    var newarrayTrs;
    if (Random == 'True') {
        arrayTrs = Array.prototype.slice.call(arrayTrs);
        newarrayTrs = shuffle(arrayTrs);

    }
    else {
        newarrayTrs = Array.prototype.slice.call(arrayTrs);
    }

    if (Top != 0) {
        if (document.getElementById("ctl00_ContentPlaceHolderMain_SocialBookmark1_pnlPagingMain") != null)
            document.getElementById("ctl00_ContentPlaceHolderMain_SocialBookmark1_pnlPagingMain").style.display = "none";
    }

    var new_tbody = document.createElement('tbody');
    new_tbody.id = "dgrdBookmarkBody";
    var endelementindex = Top == 0 ? newarrayTrs.length : (Top < newarrayTrs.length ? Top : newarrayTrs.length);

    for (var i = 0; i < endelementindex; i++) {
        new_tbody.appendChild(newarrayTrs[i]);
    }
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);


    var alltd = surfLinksBox.getElementsByClassName("MainLinktd");
    for (var i = 0; i < alltd.length; i++) {
        var linkid = alltd[i].getAttribute("id").replace("MainLinktd_", "");
        var allspan = alltd[i].getElementsByTagName("span");
        var lastspan = allspan[allspan.length - 1];
        var sid = lastspan.getAttribute("id").replace("_MainSameIPClicksLink", "");
        var firstspan = allspan[1];
        var blockdata = firstspan.innerText.split(",");
        var did = blockdata[4];
        var lblockdata1 = document.getElementById("MainLinkData_" + sid).innerText.split(",");
        var sis1 = parseInt(lblockdata1[2]);
        var blocker = parseInt(lblockdata1[0]);
        if (sis1 == 1 || blocker == 1) {
            var url = "Link_Click_UPD.aspx?linkIDSI=" + linkid + "&domainID=" + did + "&spanid=" + sid;
            var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    var res = xhttp.responseText.split(",");
                    var spanid = res[0];
                    var clickval = parseInt(res[1]);
                    var lblockdata = document.getElementById("MainLinkData_" + spanid).innerText.split(",");
                    var sis = parseInt(lblockdata[2]);
                    var siv = parseInt(lblockdata[3]);
                    if (siv <= 0) {
                        sis = 0;
                    }
                    if (sis == 1 && siv <= clickval) {
                        document.getElementById(spanid + "_MainLinkBlockImage").parentElement.parentElement.style.display = "none";
                    }
                    document.getElementById(spanid + "_MainSameIPClicksLink").innerText = clickval;
                }
            }
            xhttp.open("GET", url, false);
            xhttp.send();
        }
    }
    surfLinksBox.style.display = "block";
    contentHeight();
}
//Feeds
function LoadFeeds() {
    document.title = DomainForGA + " : Updates";
    LoadRecommended();
    var callback = function (xml) {
        var id = GetQS("feedid");
        var feedtitle = "";
        var feedlink = "";
        if (id == "0") {
            id = nodeval(xml.getElementsByTagName("Update")[0], "UpdateID");
            feedtitle = nodeval(xml.getElementsByTagName("Update")[0], "UpdateTitle");
            feedlink = nodeval(xml.getElementsByTagName("Update")[0], "UpdateLink");
        }
        else {
            var updatenodes = xml.getElementsByTagName("Update");
            for (var i = 0; i < updatenodes.length; i++) {
                var upId = nodeval(xml.getElementsByTagName("Update")[i], "UpdateID");
                if (upId == id) {
                    feedtitle = nodeval(xml.getElementsByTagName("Update")[i], "UpdateTitle");
                    feedlink = nodeval(xml.getElementsByTagName("Update")[i], "UpdateLink");
                    break;
                }
            }
        }

        ApplyXsltOnXml("Updates.xml", 'XSLT/Updates.xsl', "content-full", 1, "UpdateIDParam", id);

        var url = "Widget_Click_Update.aspx?FeedTitle=" + feedtitle + "&FeedLink=" + feedlink;
        var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        var a = "";
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("ContentPlaceHolderMain_dlstFeed").innerHTML = xmlhttp.responseText.replace(/&amp;/gi, "&").replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
        contentHeight();
    };
    ReturnObj((DomainForGA + "/Updates.xml"), callback);
}
//Calendar
function LoadCalendar() {
    document.title = DomainForGA + " : Calendar";
    LoadRecommended();
    var callback = function (xml) {
        var id = GetQS("calendarid");
        if (id == "0") { id = nodeval(xml.getElementsByTagName("Calendar")[0], "CalendarID"); }
        ApplyXsltOnXml("Calendars.xml", 'XSLT/Calendars.xsl', "content-full", 1, "CalendarIDParam", id);
        contentHeight();
    }
    ReturnObj((DomainForGA + "/Calendars.xml"), callback);
}
//Search
function LoadSearces() {
    document.title = DomainForGA + " : Search";
    LoadRecommended();
    var S = getCookie("q");//GetQS("q");
    document.getElementById("txtSearch").value = S;
    var callback = function (xml) {
        var articles = xml.getElementsByTagName("Article");
        var postsPerpage = 0;
        var strHtml = "";
        if (articles.length > 0) {
            postsPerpage = parseInt(articles[0].getElementsByTagName("PostPerPage")[0].childNodes[0].nodeValue);
            strHtml += "<table>";
            var IsSearchFound = false;
            for (var i = 0; i < articles.length; i++) {
                var postTitle = articles[i].getElementsByTagName("TitleForLink")[0].childNodes[0].nodeValue;
                var postDesc = articles[i].getElementsByTagName("Description")[0].childNodes[0].nodeValue;
                if (postTitle.toLowerCase().indexOf(S.toLowerCase()) > -1 || postDesc.toLowerCase().indexOf(S.toLowerCase()) > -1) {
                    if (!IsSearchFound)
                        IsSearchFound = true;
                    var postId = articles[i].getElementsByTagName("PostID")[0].childNodes[0].nodeValue;
                    var page = parseInt(i / 10) + 1;
                    var posturl = "Index.html?page=" + page + "&featureid=" + postId;
                    strHtml += "<tr><td>";
                    strHtml += "<div class=\"searchtitle lblBlogPostsTitle\"><a href=\"" + posturl + "\">" + postTitle + "</a></div>";
                    if (postDesc != "") {
                        if (postDesc.indexOf("<img") > -1) {
                            var strimage = postDesc.split("<img")[1].split(">")[0];
                            var imagesrc = "";
                            var imagealt = "";
                            if (strimage.indexOf("src='") > -1) {
                                imagesrc = strimage.split("src='")[1].split("'")[0];
                            }
                            else {
                                imagesrc = strimage.split("src=\"")[1].Split("\"")[0];
                            }
                            var newsrc = imageSrc.split("/")[imageSrc.split("/").length - 1];
                            imagealt = newsrc.split(".")[0].split("-").join(" ").split("_").join(" ");
                            strHtml += "<div class=\"searchimg\"><a href=\"" + posturl + "\"><img src=\"" + imagesrc + "\" alt=\"" + imagealt + "\" /></a></div>";
                        }
                        var strDescText = StripHTML(postDesc);
                        if (strDescText.length > 300) {
                            strDescText = strDescText.substring(0, 295) + " [...]";
                        }
                        strHtml += "<div class=\"searchdesc\">" + strDescText + "</div>";
                    }
                    strHtml += "</td></tr>";
                    strHtml += "<tr><td style=\"height:15px;\">";
                    strHtml += "</td></tr>";
                }
            }
            if (!IsSearchFound) {
                strHtml += "<tr style=\"border:0px!important;\"><td>";
                strHtml += "<div style=\"text-align:center;\"><h2>Nothing Found</h2></div>";
                strHtml += "<div>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</div>";
                strHtml += "</td></tr>";
            }
            strHtml += "</table>";
        }
        document.getElementById("ContentPlaceHolderMain_SearchResult").innerHTML = strHtml;
        contentHeight();
    };
    ReturnObj((DomainForGA + "/Articles.xml"), callback);
}
function StripHTML(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}
//HomePage
function LoadHome() {
    document.title = DomainForGA + " : Home";
    LoadRecommended();
    var callback = function (xmldoc) {
        var codeval = nodeval(xmldoc, "Code");
        document.getElementById("content-wrapper").innerHTML = "<div class=\"padding\">" + codeval + "</div>";
        contentHeight();
    }
    ReturnObj(DomainForGA + "/Home.xml", callback);
}
//Favicon,Logo
function LoadLogo_Favicon() {
    var callback = function (xmldoc) {
        var ImageUrl = nodeval(xmldoc, "Logo");
        var newsrc = ImageUrl.split("/")[ImageUrl.split("/").length - 1];
        var imagealt = newsrc.split(".")[0].split("-").join(" ").split("_").join(" ");
        var HtmlLogo = "<a href='default.html' id='logotitle' title='" + DomainForGA + "'><img src='" + ImageUrl + "' id='imgLogo' alt='" + imagealt + "'/></a>";
        document.getElementById("logobox").innerHTML = HtmlLogo;
        var link = document.createElement('link');
        link.rel = 'shortcut icon';
        link.href = nodeval(xmldoc, "Theme").replace("/base.css", "/images/favicon.ico");
        document.getElementsByTagName('head')[0].appendChild(link);
    }
    ReturnObj(DomainForGA + "/LatestLinksUpdates.xml", callback);

}
//Theme
function ApplyTheme() {
    var callback = function (xmldoc) {
        var CSS = nodeval(xmldoc, "Theme");
        if (document.createStyleSheet)
            document.createStyleSheet(CSS);
        else {
            var styles = "@import url('" + CSS + "');";
            var newSS = document.createElement('link');
            newSS.rel = 'stylesheet';
            newSS.href = 'data:text/css,' + escape(styles);
            document.getElementsByTagName("head")[0].appendChild(newSS);
        }
    }
    ReturnObj((DomainForGA + "/LatestLinksUpdates.xml"), callback);
}
//Widgets

function nodeval(n, name) {
    return n.getElementsByTagName(name)[0].childNodes[0].nodeValue.replace(/^\s+|\s+$/g, '');
}
function checkTargetting(n) {
    return (ReferalCheck(nodeval(n, "Referal"), nodeval(n, "NotReferal"))
        && ("," + nodeval(n, "Browser") + ",").indexOf("," + UserBrowser + ",") > -1
        && CountryCheck(UserCountryID, nodeval(n, "Country"))
        && OSCheck(UserOS, nodeval(n, "OS")));
}
function Codebox(node, name) {
    var CacheBuster = "";
    for (var i = 0; i < 8; i++)
        CacheBuster += Math.floor((Math.random() * 10));
    var strcodebox = node.getElementsByTagName(name)[0].childNodes[0].nodeValue;//toLowerCase();
    strcodebox = strcodebox.replace(/^\s+|\s+$/g, '').split("&quot;").join("\"").split("&rsquo;").join("'");
    strcodebox = strcodebox.split("src=\"config/").join("src=\"/config/").split("src='config/").join("src='/config/");
    strcodebox = strcodebox.split("[CACHEBUSTER]").join(CacheBuster);
    //RK 2019-07-04
    strcodebox = strcodebox.split("[cachebuster]").join(CacheBuster);
    strcodebox = strcodebox.split("[Host_URL]").join("http://" + window.top.location.host);
    strcodebox = strcodebox.split("[Host_Url]").join("http://" + window.top.location.host);
    strcodebox = strcodebox.split("[page_url]").join(window.top.location.toString().split("#")[0]);
    strcodebox = strcodebox.split("[ip]").join(UserIP);
    strcodebox = strcodebox.split("[ua]").join(UserAgent);
    return strcodebox;
}
function GetIndex(PName, CName, RName) {
    var a = [];
    var node = XMLWidgets.getElementsByTagName(PName);
    if (node.length > 0) {
        var nodes = node[0].getElementsByTagName(CName);
        var AIndexes = [];
        for (var i = 0; i < nodes.length; i++) {
            if (checkTargetting(nodes[i]))
                AIndexes.push(parseInt(i));
        }
        if (AIndexes.length > 0) {
            var rnd = Math.round(Math.random() * (AIndexes.length - 1));
            var RN = XMLWidgets.getElementsByTagName("Random")[0];
            if (RN.childNodes.length > 0 && nodeval(RN, RName) == "1")
                a.push(AIndexes[rnd]);
            else
                a = AIndexes;
        }
    }
    return a;
}
function NonePositionWidgets() {
    var a = "";
    if (XMLWidgets.getElementsByTagName("NoneWidgets").length > 0) {
        var nodes = XMLWidgets.getElementsByTagName("NoneWidgets")[0].getElementsByTagName("NoneWidget");
        var strwidgethtml = "";
        for (var i = 0; i < nodes.length; i++) {
            var n = nodes[i];
            var b = nodeval(n, "ApplicationName");
            a += "<div id='" + b + "' class='" + b + "'>" + Codebox(n, "CodeBox") + "</div>";
        }
    }
    contentHeight();
    return a;
}
function Top_Right_Bottom_Left_Widgets(PName, CName, RName, ClickFunc, ImageID, SameIPCount, PosCount) {
    var a = "";
    var ArrIndexes = GetIndex(PName, CName, RName);
    if (ArrIndexes.length > 0) {
        for (var i = 0; i < ArrIndexes.length; i++) {
            var n = XMLWidgets.getElementsByTagName(PName)[0].getElementsByTagName(CName)[parseInt(ArrIndexes[i])];
            var IPStatus = parseInt(nodeval(n, "SameIPStatus"));
            var IPValue = parseInt(nodeval(n, "SameIPValue"));
            var IPDays = parseInt(nodeval(n, "SameIPDays"));
            var blocker = parseInt(nodeval(n, "Blocker"));
            var BlockerValue = parseInt(nodeval(n, "BlockerValue"));
            var BanName = nodeval(n, "ApplicationName");
            var Bid = parseInt(nodeval(n, "id"));
            var width = parseInt(nodeval(n, "width"));
            var height = parseInt(nodeval(n, "height"));
            var strSameIPCount = SameIPCount + "_" + ArrIndexes[i];
            window[strSameIPCount] = parseInt(ReturnObjSync("Banner_Click_UPD.aspx?SameIPClicksBanner=" + Bid + "&SameIPDays=" + IPDays));
            window[PosCount + "_" + ArrIndexes[i]] = 0;
            var dis = "none";
            if (IPStatus == 1 && IPValue <= parseInt(window[strSameIPCount]) && Bid != 0)
                dis = "block";
            var image = "<img alt='1x2' id='" + ImageID + "_" + ArrIndexes[i] + "' style='display:" + dis + "; z-index: 9999; float: right; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;' src='../images_common/1x2.gif' />";
            var type = "0";
            if (dis == "block")
                type = "2";
            else if (dis == "none" && blocker != 1)
                type = "1";
            if (width == 0 && height == 0)
                a += "<div id='" + BanName + "' class='" + BanName + "' style='height:fit-content;width:fit-content;width:intrinsic;width:-moz-max-content;width:-webkit-max-content;height:intrinsic;height:-moz-max-content;height:-webkit-max-content;position:relative;' onclick='" + ClickFunc + "(" + Bid + "," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",\""
                    + ImageID + "_" + ArrIndexes[i] + "\",\"" + BanName + "\"," + parseInt(ArrIndexes[i]) + ",\"" + SameIPCount + "\",\"" + PosCount + "\")'>" + image + Codebox(n, "CodeBox") + "</div>";
            else
                a += "<div id='" + BanName + "' class='" + BanName + "' style='height:fit-content;height:intrinsic;height:-moz-max-content;height:-webkit-max-content;width:" + width + "px;position:relative;' onclick='" + ClickFunc + "(" + Bid + "," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",\""
                    + ImageID + "_" + ArrIndexes[i] + "\",\"" + BanName + "\"," + parseInt(ArrIndexes[i]) + ",\"" + SameIPCount + "\",\"" + PosCount + "\")'>" + image + Codebox(n, "CodeBox") + "</div>";

            //GABanner('BannerImpression', BanName, DomainForGA);
            RecordData("BannerPageImp=" + Bid);
        }
    }
    return { AWidgetCode: a };
}
function Popunder_Subline_Widgets(PName, CName, RName) {
    var a = "";
    var i = GetIndex(PName, CName, RName);
    if (i != "") {
        var n = XMLWidgets.getElementsByTagName(PName)[0].getElementsByTagName(CName)[parseInt(i)];
        bName = nodeval(n, "ApplicationName");
        var bid = nodeval(n, "id");
        a = Codebox(n, "CodeBox");
        //GABanner('BannerImpression', bName, DomainForGA);
        RecordData("BannerPageImp=" + bid);
    }
    return a;
}
function Video_Widget() {
    var a = "", node = XMLWidgets.getElementsByTagName("VideoWidgets");
    if (node.length > 0) {
        var nodes = node[0].getElementsByTagName("VideoWidget");
        for (var i = 0; i < nodes.length; i++) {
            var n = nodes[i];
            var VWname = nodeval(n, "ApplicationName");
            if (VWname.indexOf("video") != -1 && checkTargetting(n)) {
                SetDisplay("top_ad_box", "block");
                a = "<div id='" + VWname + "' class='" + VWname + "'><div id='adspace'><iframe width='767px' height='460px' scrolling='no' frameborder='0' allowTransparency='true' style='background-color:Transparent; border:0;' src='Video.html'></iframe></div></div>";
            }
        }
    }
    contentHeight();
    return a;
}
//Click functions
function UNBlockedClick(id) {
    RecordData("banID=" + id);
}
function BlockedClick(id) {
    RecordData("banIDBlocked=" + id);
}
function SameIpClickCheck(IPStatus, IPValue, SameIPCount, imgid, widid, bName) {
    if (IPStatus == 1 && IPValue <= SameIPCount)
        SetDisplay(imgid, "block");
    if (IPStatus == 1 && IPValue < SameIPCount)
        BlockedClick(widid);
    else {
        if (ISSendGABannerClick == "1") {
            GABanner('BannerClick', bName, DomainForGA);
        }
        UNBlockedClick(widid);
    }
}
function CurrentClickCheck(IPStatus, IPValue, SameIPCount, BlockerCount, imgid, widid, BlockerValue, bName) {
    if (BlockerCount >= BlockerValue)
        SetDisplay(imgid, "block");
    if (BlockerCount > BlockerValue)
        BlockedClick(widid);
    else
        SameIpClickCheck(IPStatus, IPValue, SameIPCount, imgid, widid, bName);
}
function WidgetClick(id, Ftype, IPStatus, IPValue, BlockerValue, imgid, bName, aindex, PosSameIPCount, PosCount) {
    var strSameIPCount = PosSameIPCount + "_" + aindex;
    var strPosCount = PosCount + "_" + aindex;
    window[strSameIPCount] += 1;
    if (Ftype == 0) {
        window[strPosCount] += 1;
        CurrentClickCheck(IPStatus, IPValue, window[strSameIPCount], window[strPosCount], imgid, id, BlockerValue, bName);
    }
    else if (Ftype == 1)
        SameIpClickCheck(IPStatus, IPValue, window[strSameIPCount], imgid, id, bName);
    else if (Ftype == 2)
        BlockedClick(id);
}
//Top
function TopWidget() {
    var a = Top_Right_Bottom_Left_Widgets("TopWidgets", "TopWidget", "TopRandom", "WidgetClick", "Imagetop", "SameIPCountTop", "TopCount");
    if (a.AWidgetCode == "") {
        document.getElementById("top_header_ad_box").style.display = "none";
    }
    contentHeight();
    return a.AWidgetCode;
}
//Right
function RightWidget() {
    var a = Top_Right_Bottom_Left_Widgets("RightWidgets", "RightWidget", "RightRandom", "WidgetClick", "Imageright", "SameIPCountRight", "RightCount");
    if (a.AWidgetCode == "") {
        document.getElementById("right_ad_box").style.display = "none";
    }
    contentHeight();
    return a.AWidgetCode;
}
//Left
function LeftWidget() {
    var a = Top_Right_Bottom_Left_Widgets("LeftWidgets", "LeftWidget", "LeftRandom", "WidgetClick", "Imageleft", "SameIPCountLeft", "LeftCount");
    if (a.AWidgetCode == "") {
        document.getElementById("left_ad_box").style.display = "none";
    }
    contentHeight();
    return a.AWidgetCode;
}
//Bottom
function BottomWidget() {
    var a = Top_Right_Bottom_Left_Widgets("BottomWidgets", "BottomWidget", "BottomRandom", "WidgetClick", "Imagebottom", "SameIPCountBottom", "BottomCount");
    if (a.AWidgetCode == "") {
        document.getElementById("bottom_ad_box").style.display = "none";
    }
    contentHeight();
    return a.AWidgetCode;
}
//Popup
function PopWidget() {
    var a = Top_Right_Bottom_Left_Widgets("PopWidgets", "PopWidget", "PopunderRandom", "WidgetClick", "Imagepopunder", "SameIPCountPopunder", "PopunderCount");
    if (a.AWidgetCode == "") {
        document.getElementById("pop_under_ad_box").style.display = "none";
    }
    contentHeight();
    return a.AWidgetCode;
    //return Popunder_Subline_Widgets("PopWidgets", "PopWidget", "PopunderRandom");
}
//Subline
function SublineWidget() {
    var a = Top_Right_Bottom_Left_Widgets("SublineWidgets", "SublineWidget", "SublineRandom", "WidgetClick", "Imagesubline", "SameIPCountsubline", "SublineCount");
    if (a.AWidgetCode == "") {
        document.getElementById("subline_ad_box").style.display = "none";
    }
    contentHeight();
    return a.AWidgetCode;
    //return Popunder_Subline_Widgets("SublineWidgets", "SublineWidget", "SublineRandom");
}
///Mid article
function ClickMidArticle(id, Ftype, IPStatus, IPValue, BlockerValue, imgid, bName) {
    SameIPCountMidArticle += 1;
    if (Ftype == 0) {
        MidArticleBlockerCount += 1;
        CurrentClickCheck(IPStatus, IPValue, SameIPCountMidArticle, MidArticleBlockerCount, imgid, id, BlockerValue, bName);
    }
    else if (Ftype == 1)
        SameIpClickCheck(IPStatus, IPValue, SameIPCountMidArticle, imgid, id, bName);
    else if (Ftype == 2)
        BlockedClick(id);
}
//Subscribe
function Subscribe() {
    var a = true;
    var Email = document.getElementById("txtsubscribeemail").value;
    SetDisplay("rqfEmailSubscribe", "none");
    SetDisplay("regEmailSubscrib", "none");
    SetDisplay("lblsubsmsg", "none");
    if (Email == "") {
        SetDisplay("rqfEmailSubscribe", "block");
        a = false;
    }
    else if (!(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/).test(Email)) {
        SetDisplay("regEmailSubscrib", "block");
        a = false;
    }

    if (!a)
        return a;
    var callback = function (b) {
        var ctrl = document.getElementById("lblsubsmsg");
        if (b == "Fail")
            ctrl.innerHTML = "An error occured.";
        else if (b == "Success")
            ctrl.innerHTML = "Subscription is successful.";
        SetDisplay("lblsubsmsg", "block");
    };
    ReturnObj("Widget_Click_Update.aspx?SUBEmail=" + Email, callback);
    return false;
}
//Search
function SearchFeature() {
    var a = true;
    var SearchText = document.getElementById("txtSearch").value;
    SetDisplay("rfvSearch", "none");
    if (SearchText == "") {
        SetDisplay("rfvSearch", "block");
        a = false;
    }

    if (!a)
        return a;
    setCookie("q", SearchText, (30 * 60 * 1000))
    window.open("Search.html", "_self");
    return false;
}

function setCookie(cname, cvalue, extime) {
    var d = new Date();
    d.setTime(d.getTime() + extime);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function contentHeight() {
    if (document.getElementById("aside")) {
        var AsideHeight = parseInt(document.getElementById("aside").offsetHeight);
        var heightMinus = 0;
        if (document.getElementById("top_header_ad_box")) {
            if (document.getElementById("top_header_ad_box").style.display == "block") {
                heightMinus += 90;
            }
        }
        if (document.getElementById("top_ad_box")) {
            if (document.getElementById("top_ad_box").style.display == "block") {
                heightMinus += 774;
            }
        }
        heightMinus += 20;
        if (document.getElementById("content-wrapper")) {
            document.getElementById("content-wrapper").style.minHeight = (AsideHeight - heightMinus) + "px";
        }
    }
}

window.onscroll = function () { scrollFunction() };



//Links
var BlockerLink, BlockerValueLink, SameIPStatusLink, SameIPValueLink, CurrentClickLink, SameIPClickLink, DId = 0;
function GetLinkData(ClickDataSpanId, CurrentClickSpanId, SameIPCLickId, BlockImageId, Id, LinkId, index) {
    var spandta = document.getElementById(ClickDataSpanId + index).innerText.split(",");
    BlockerLink = parseInt(spandta[0]);
    BlockerValueLink = parseInt(spandta[1]);
    if (BlockerValueLink <= 0) {
        BlockerLink = 0;
    }
    SameIPStatusLink = parseInt(spandta[2]);
    SameIPValueLink = parseInt(spandta[3]);
    if (SameIPValueLink <= 0) {
        SameIPStatusLink = 0;
    }
    DId = parseInt(spandta[4]);
    var curclickspandata = document.getElementById(CurrentClickSpanId + index).innerText;
    CurrentClickLink = curclickspandata == "" ? 0 : parseInt(curclickspandata);

    var linkclicked = document.getElementById(Id + index);
    var blockimage, spansameipclicccount;
    var allimages = linkclicked.parentElement.getElementsByTagName("img");
    for (var i = 0; i < allimages.length; i++) {
        if (allimages[i].getAttribute("id").indexOf(BlockImageId) != -1) {
            blockimage = allimages[i];
        }
    }
    var allspans = linkclicked.parentElement.getElementsByTagName("span");
    for (var i = 0; i < allspans.length; i++) {
        if (allspans[i].getAttribute("id").indexOf(SameIPCLickId) != -1) {
            spansameipclicccount = allspans[i];
        }
    }
    var cursameipclickspandata = spansameipclicccount.innerText;
    SameIPClickLink = cursameipclickspandata == "" ? 0 : parseInt(cursameipclickspandata);

    SameIPClickLink++;
    CurrentClickLink++;
    spansameipclicccount.innerText = SameIPClickLink + "";
    document.getElementById(CurrentClickSpanId + index).innerText = CurrentClickLink + "";
    var isSameIPBlockReachedLink = false;
    if (SameIPStatusLink == 0) {
        if (BlockerLink == 1) {
            if (BlockerLink == 1 && BlockerValueLink <= CurrentClickLink) {
                //set image display block
                //blockimage.style.display = "block";
                blockimage.parentElement.parentElement.style.display = "none";
            }
            if (BlockerLink == 1 && BlockerValueLink < CurrentClickLink) {
                //blocked click
                //RecordClickLink(LinkId, DId, 1);
            }
            else {
                //unblocked click
                RecordClickLink(LinkId, DId, 0);
            }
        }
        else {
            RecordClickLink(LinkId, DId, 0);
        }
    }
    else {
        if (SameIPStatusLink == 1 && SameIPValueLink <= SameIPClickLink) {
            //set image display block
            isSameIPBlockReachedLink = true;
            //blockimage.style.display = "block";
            blockimage.parentElement.parentElement.style.display = "none";
        }
        if (SameIPStatusLink == 1 && SameIPValueLink < SameIPClickLink) {
            //blocked click
            //RecordClickLink(LinkId,DId,1);
        }
        else {
            if (!isSameIPBlockReachedLink) {
                if (BlockerLink == 1) {
                    if (BlockerLink == 1 && BlockerValueLink <= CurrentClickLink) {
                        //set image display block
                        //blockimage.style.display = "block";
                        blockimage.parentElement.parentElement.style.display = "none";
                    }
                    if (BlockerLink == 1 && BlockerValueLink < CurrentClickLink) {
                        //blocked click
                        //RecordClickLink(LinkId, DId, 1);
                    }
                    else {
                        //unblocked click
                        RecordClickLink(LinkId, DId, 0);
                    }
                }
                else {
                    RecordClickLink(LinkId, DId, 0);
                }
            }
            else {
                RecordClickLink(LinkId, DId, 0);
            }
        }
    }
}
function RecordClickLink(lid, did, blockstatus) {
    var url = "Link_Click_UPD.aspx?linkID=" + lid + "&domainID=" + did + "&blockstatus=" + blockstatus;
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function () { }
    xhttp.open("GET", url, true);
    xhttp.send();
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}