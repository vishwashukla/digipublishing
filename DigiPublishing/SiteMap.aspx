<%@ Page Language="C#" MasterPageFile="~/brandtemplates/DigiPublishing.master" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" 
Inherits="SiteMap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">

<asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label><br /><br />
    <div id="divBlogs" runat="server" style="display:none;">
        <h3 class="HeaderClass">
            Features</h3><br />
        
        <p>
            <asp:Label ID="lblBlogs" runat="server"></asp:Label>
        </p>
        <p>
            <asp:Label ID="lblNoBlogs" runat="server" Text="No Feature" Visible="false"></asp:Label><br />    
        </p>
         <p>&nbsp;</p>
    </div> 

    <div id="divBookmarks" runat="server" style="display:none;" visible="false">
        <h3 class="HeaderClass">
            Links</h3><br />   
        
        <p>
            <asp:Label ID="lblBookmarks" runat="server"></asp:Label>
        </p>
        <p>
            <asp:Label ID="lblNoBookmarks" runat="server" Text="No Links" Visible="false"></asp:Label><br />  
        </p>
         <p>&nbsp;</p>
    </div> 

    <div id="divRss" runat="server" style="display:none;">
        <h3 class="HeaderClass">
            Feeds</h3><br />
        <p> 
            <asp:Label ID="lblRss" runat="server"></asp:Label>
        </p>
        <p>
            <br />   
        </p>
         <p>&nbsp;</p>
    </div>
    
     <div id="divCalendar" runat="server" style="display:none;"> 
        <h3 class="HeaderClass">
            Calendars </h3><br />
        <p>
            <asp:Label ID="lblCalendar" runat="server"></asp:Label>
        </p>
        <p>
            <br />   
        </p>
         <p>&nbsp;</p>
    </div>

    <div id="divTerm" runat="server"> 
        <h3 class="HeaderClass">
            Terms </h3><br />
        <p>
            <a href="Terms.aspx">Terms</a>
        </p>
        <p>
            <br />   
        </p>
         <p>&nbsp;</p>
    </div>

    <div id="divPrivacy" runat="server"> 
        <h3 class="HeaderClass">
            Privacy Policy </h3><br />
        <p>
            <a href="PrivacyPolicy.aspx">Privacy Policy</a>
        </p>
        <p>
            <br />   
        </p>
         <p>&nbsp;</p>
    </div>

</asp:Content>

