using System;
using System.Net.Mail;
using System.Text;
using System.Web.UI;

public partial class ContactUs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Get in touch";
        lblMSg.Text = "";
        if (!this.IsPostBack)
        {
            this.Session["CaptchaImageText"] = utils.GenerateRandomCode();
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (Session["CaptchaImageText"] != null)
        {
            if (txtCaptcha.Text == Session["CaptchaImageText"].ToString())
            {
                try
                {
                    SmtpClient smtpClient = new SmtpClient();
                    System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
                    MailMessage message = new MailMessage();
                    message.IsBodyHtml = true;
                    string emailaddress = ReadConfig.GetEmail().Trim();
                    if (emailaddress == "")
                    {
                        emailaddress = "sites@digipublishing.net";
                    }
                    MailAddress toAddress = new MailAddress(emailaddress);
                    string strFrom = txtEmail.Text.ToString().Trim();
                    if (strFrom == "")
                    {
                        lblMSg.Text = "Please enter email address.";
                        return;
                    }
                    string strDomian = ReadConfig.getDomainUrl();
                    string strSub = strDomian + " - Contact Form";
                    MailAddress fromAddress = new MailAddress(strFrom, txtUsername.Text);
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = strSub;
                    string strIPaddress = utils.GetUserIP();



                    message.ReplyToList.Add(new MailAddress(strFrom, txtUsername.Text));
                    StringBuilder strBody = new StringBuilder();

                    strBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
                    strBody.Append("<html><head>");
                    strBody.Append("<meta http-equiv=Content-Type content=\"text/html); charset=iso-8859-1\">");
                    strBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
                    strBody.Append("<style type='text/css'>");
                    strBody.Append(" table { font-family:arial;font-size:12px;color:#000000;} ");
                    strBody.Append("</style>");
                    strBody.Append("</head>");
                    strBody.Append("<BODY><Table><tr><td>Name:" + utils.formatStringForDBInsert(txtUsername.Text.Trim()) + "</td></tr>");
                    strBody.Append("<tr><td> From: " + strFrom + "</td></tr>");
                    strBody.Append("<tr><td> IP: " + strIPaddress + "</td></tr>");
                    strBody.Append("<br/>");
                    strBody.Append("<tr><td> Message: " + txtComment.Text.Trim() + "</td></tr>");
                    strBody.Append("<tr><td> Country: " + utils.GetcountryName() + "</td></tr>");
                    strBody.Append("<tr><td> OS: " + utils.GetOperatingSystem() + "</td></tr>");
                    strBody.Append("<tr><td> Browser: " + utils.GetBrowserName() + "</td></tr>");
                    strBody.Append("<tr><td> Useragent: " + utils.GetUserAgent() + "</td></tr>");
                    strBody.Append("<br/>");
                    strBody.Append("</Table></BODY></html>");

                    AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody.ToString(), HTMLType);
                    message.AlternateViews.Add(HTMLView);

                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.ToString());
                    }
                    finally
                    {
                        message.Dispose();
                    }

                    txtComment.Text = "";
                    mView.SetActiveView(vwMessage);
                }
                catch (Exception ex)
                {
                    lblMSg.Text = ex.Message.ToString();
                }
            }
            else
            {
                txtCaptcha.Text = "";
                Session["CaptchaImageText"] = utils.GenerateRandomCode();
                lblMSg.Text = "please enter valid verification code";
            }
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}
