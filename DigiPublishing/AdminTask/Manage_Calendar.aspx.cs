﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Net;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections; 

public partial class AdminTask_Manage_Calendar : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : Calendar";
        lblmessage.Text = "";
        if (!IsPostBack)
            GridBind();
    }

    private void GridBind()
    {
        try
        {
            dgrdCalendar.DataSource = GetData();
            dgrdCalendar.DataBind();
        }
        catch (Exception ex)
        {
           lblmessage.Text = ex.Message.ToString();
        }
    }

    private DataTable GetData()
    {
        string strQuery = "SELECT * FROM Brand_Calendar WHERE Status='true' ORDER BY SortOrder DESC";
        DataTable dt = dp.FillDataTable(strQuery);
        return dt;
    }

    protected void dgrdCalendar_OnItemCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Up"))
        {
            if (e.Item.Cells[0].Text.Trim() != "")
            {
                int ID = Convert.ToInt32(e.Item.Cells[0].Text.Trim());

                string strQuery = "DECLARE @SortOrder1 int=0"
                                + " DECLARE @SortOrder2 int=0"
                                + " SET @SortOrder1=(SELECT ISNULL(SORTORDER,0) FROM Brand_Calendar WHERE Cal_id=" + ID + ")"
                                + " SET @SortOrder2=(SELECT ISNULL(MIN(SORTORDER),0) FROM Brand_Calendar WHERE SORTORDER>@SortOrder1)"
                                + " UPDATE Brand_Calendar SET SORTORDER=@SortOrder1 WHERE SORTORDER=@SortOrder2"
                                + " UPDATE Brand_Calendar SET SORTORDER=@SortOrder2 WHERE Cal_id=" + ID;
                dp.ExecuteNonQuery(strQuery);
                GridBind();
            }
        }
         if (e.CommandName.Equals("Down"))
        {
            if (e.Item.Cells[0].Text.Trim() != "")
            {
                int ID = Convert.ToInt32(e.Item.Cells[0].Text.Trim());
                string strQuery = "DECLARE @SortOrder1 int=0"
                                + " DECLARE @SortOrder2 int=0"
                                + " SET @SortOrder1=(SELECT ISNULL(SORTORDER,0) FROM Brand_Calendar WHERE Cal_id=" + ID + ")"
                                + " SET @SortOrder2=(SELECT ISNULL(MAX(SORTORDER),0) FROM Brand_Calendar WHERE SORTORDER<@SortOrder1)"
                                + " UPDATE Brand_Calendar SET SORTORDER=@SortOrder1 WHERE SORTORDER=@SortOrder2"
                                + " UPDATE Brand_Calendar SET SORTORDER=@SortOrder2 WHERE Cal_id=" + ID;
                dp.ExecuteNonQuery(strQuery);
                GridBind();
            }
        }
        if (e.CommandName == "Edit")
        {
            try
            {
                SetBlank();
                hdnID.Value = e.Item.Cells[0].Text;
                Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                if (lblTitle != null)
                    txtTitle.Text = lblTitle.Text;
                Label lblFeedURL = (Label)e.Item.FindControl("lblFeedURL");
                if (lblFeedURL != null)
                    txtCalURL.Text = lblFeedURL.Text;
                mView.SetActiveView(vAdd);
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message.ToString();
            }
        }
        if (e.CommandName == "Delete")
        {
            try
            {
                string strQuery = "UPDATE Brand_Calendar SET Status='false' WHERE Cal_Id='" + e.Item.Cells[0].Text + "'";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Deleted successfully.";
                GridBind();
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message.ToString();
            }
        }
    }

    protected void btnAddiCal_Click(object sender, EventArgs e)
    {
        SetBlank();
        this.btnSubmit.Text = "create";
        mView.SetActiveView(vAdd);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!ValidateFields())
            return;
        if (txtTitle.Text.Trim() != "" && txtCalURL.Text.Trim() != "")
        {
            string strQuery = "";
            string Calurl = txtCalURL.Text.ToLower(); 
            if (this.btnSubmit.Text == "update")
            {
                strQuery = " UPDATE Brand_Calendar"
                          + " SET "
                          + " title='" + utils.formatStringForDBInsert(txtTitle.Text) + "'"
                          + " ,link='" + utils.formatStringForDBInsert(Calurl.Trim()) + "'"
                          + " ,m_date =" + "getdate()"
                          + " ,m_by=" + Convert.ToString(Session["userid"])
                          + " WHERE cal_id=" + this.hdnID.Value;
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Updated  successfully.";
            }
            else
            {
                strQuery = "insert into Brand_Calendar("
                + "title"
                + ",link"
                +",c_by"
                + ",status"
                + ",SortOrder)"
                + "values ('" + utils.formatStringForDBInsert(txtTitle.Text.Trim()) + "'"
                + ",'" + utils.formatStringForDBInsert(Calurl.Trim()) + "'"
                +","  + Convert.ToString(Session["userid"])
                + ",1"
                + ",(SELECT ISNULL(MAX(SORTORDER),0)+1 FROM Brand_Calendar))";
                dp.ExecuteNonQuery(strQuery);
             lblmessage.Text = "Added successfully.";
            }
            GridBind();
            mView.SetActiveView(Vgrid);
        }
    }
    private bool ValidateFields()
    {
        string strQuery = "DECLARE @MSG varchar(100)=''"
                    + " SET @MSG=(SELECT CASE WHEN COUNT(*)>0 THEN 'Title already exists.' ELSE '' END FROM Brand_calendar where status=1 AND title='" + txtTitle.Text.Trim() + "'";
        if (!string.IsNullOrWhiteSpace(hdnID.Value))
            strQuery += "  and cal_id<>" + hdnID.Value;
        strQuery += ")"
                    + " SELECT @MSG as MSG";
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0][0])))
        {
            lblmessage.Text = Convert.ToString(dt.Rows[0][0]);
            return false;
        }
        return true;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetBlank();
        mView.SetActiveView(Vgrid);
    }
    protected void dgrdCalendar_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        DataGrid dgrd = (DataGrid)sender;
        dgrd.CurrentPageIndex = e.NewPageIndex;
        DataTable dt = GetData();
        DataView dv = new DataView(dt);
        dv.Sort = SortField;

        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrd.DataSource = dv;
        dgrd.DataBind();
    }
    protected void dgrdCalendar_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataGrid dgrdOne = (DataGrid)sender;
        DataTable dt = GetData();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }
    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }
    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }

    private void SetBlank()
    {
        txtTitle.Text = "";
        txtCalURL.Text = "";
        btnSubmit.Text = "update";
        hdnID.Value = "";
    }
   
}