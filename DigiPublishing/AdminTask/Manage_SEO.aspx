﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_SEO.aspx.cs" Inherits="AdminTask_Manage_SEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="content-full">
        <h3 class="HeaderClass">Manage Meta Tags</h3>
        <asp:Label ID="lblmessage" runat="server" Text="" CssClass="error"></asp:Label>
        <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
            <asp:View ID="Vgrid" runat="server">
               <asp:DataGrid ID="dgrdTitle" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnEditCommand="dgrdTitle_OnEditCommand">
                    <Columns>
                        <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Name" SortExpression="name">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#utils.formatStringForDBSelect(Convert.ToString(Eval("name"))) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Contents">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#utils.formatStringForDBSelect(Convert.ToString(Eval("description"))) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:ButtonColumn Text="Edit" HeaderText="Action" CommandName="Edit"></asp:ButtonColumn>
                    </Columns>
                </asp:DataGrid>               
                <div class="AdminRightMenu">
                    <asp:LinkButton ID="lbAddNew" runat="server" CssClass="toplink" OnClick="lbAddNew_Click" Text="Add New"></asp:LinkButton>
                </div>
               <asp:DataGrid ID="dgrdSEO" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnSortCommand="dgrdSEO_SortCommand"
                OnItemCommand="dgrdSEO_OnItemCommand" AllowPaging="True" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrdSEO_PageIndexChanged" PageSize="20">
                    <Columns>
                        <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Name" SortExpression="name">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#utils.formatStringForDBSelect(Convert.ToString(Eval("name"))) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType" runat="server" Text='<%#utils.GetMetaType(Convert.ToString(Eval("MetaType"))) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Contents">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#utils.formatStringForDBSelect(Convert.ToString(Eval("description"))) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Action">
                            <ItemTemplate>
                                &nbsp;&nbsp;<asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" CommandName="Delete" Visible='<%#Convert.ToBoolean(Eval("Visible"))%>' OnClientClick="return confirm('Are you sure,you want to delete this record?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <asp:CheckBox runat="server" ID="chkMetatag"  Text =" NO FOLLOW(Check box so search engines won't index site ) " 
                AutoPostBack="true" Checked="false" OnCheckedChanged="chkMetatag_Checked"/>
            </asp:View>
            <asp:View ID="vAdd" runat="server">
                <table class="table-full">
                    <tr>
                        <td>
                            Meta-Name</td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" MaxLength="200"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Meta-Type</td>
                        <td>
                            <asp:DropDownList ID="drpType" runat="server" AutoPostBack="false">
                                <asp:ListItem Text="meta name" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="meta property" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Meta-Content
                        </td>
                        <td>
                            <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label runat="server" Font-Size="Smaller" ID="lblMaxChar"></asp:Label>
                            <br/>
                            You have <input readonly="readonly" runat="server" type="text" name="countdown" size="4" id="txtCountdown"/> characters left.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">                    
                            <div id="div1" runat="server" style="display: inline;">
                                <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="btnBgClass" OnClientClick="return SetDisable();"></asp:Button>
                            </div>
                            <div id="div2" runat="server" style="display: none;">
                                <asp:Button ID="btnSaving" Enabled="false" runat="server" Text="Please wait..." CssClass="btnBgClass"></asp:Button>
                            </div>
                            <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CssClass="btnBgClass"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>    
    <script type="text/javascript">
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }

        function SetDisable() {
            var Comment = document.getElementById('<%= txtName.ClientID%>').value;
            if (Comment == '')
                return false;
            else {
                document.getElementById('<%= div1.ClientID%>').style.display = 'none';
                document.getElementById('<%= div2.ClientID%>').style.display = 'inline';
                return true;
            }
        }
    </script>
</asp:Content>

