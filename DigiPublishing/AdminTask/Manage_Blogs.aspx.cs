﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Text;

public partial class AdminTask_Manage_Blogs : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : Blogs";
        lblmessage.Text = "";

        if (!IsPostBack)
        {
            lblBlogtitle.Text = ReadConfig.getDomainUrl();
            bindChkBxRandomiseStatus();
            BindBlogPosts();
        }
    }

    private void BindBlogPosts()
    {
        DataTable dtPost = getPostList(1);
        if (dtPost != null)
        {
            int totalblogs = dtPost.Rows.Count;
            int pagesize = dgrdBlogPosts.PageSize;
            int currentpage = dgrdBlogPosts.CurrentPageIndex;
            int totalpages = (totalblogs / pagesize) + ((totalblogs % pagesize) == 0 ? 0 : 1);
            if(totalpages > 0 && (totalpages-1) < currentpage)
            {
                dgrdBlogPosts.CurrentPageIndex = totalpages - 1;
            }
            dgrdBlogPosts.DataSource = dtPost;
            dgrdBlogPosts.DataBind();
        }
        mView.SetActiveView(Vgrid);
    }

    private void bindChkBxRandomiseStatus()
    {
        string strQuery = "Select  ISNULL(RandomiseStatus,0) as RandomiseStatus from brand_settings";      
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(strQuery);
        chbxRandomiseBlogOrder.Checked = Convert.ToBoolean(dt.Rows[0]["RandomiseStatus"]); 
    }

    protected DataTable getPostList(int blogId)
    {
        DataTable dt = new DataTable();
        try
        {
            string strQuery = "SELECT post_id,posts_blog_id,post_title,fullname,ISNULL(SortOrder,0) AS SortOrder,"
            + " BRAND_BLOG_POSTS.c_by,BRAND_BLOG_POSTS.c_date,ISNULL(Active_Inactive,0) AS Active_Inactive,(SELECT count(*) FROM BRAND_BLOG_POSTS) AS COUNTING, "
            + " (SELECT COUNT(*) FROM BRAND_POST_COMMENTS WHERE coment_post_id=post_id) AS CommentsCount,"
            + " (SELECT COUNT(DISTINCT c_by) FROM BRAND_POST_COMMENTS WHERE coment_post_id=post_id) AS CommentsUserCount"
            + " FROM BRAND_BLOG_POSTS "
            + " INNER JOIN BRAND_BLOG_NEW ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id "
            + " LEFT JOIN brand_user ON uid=BRAND_BLOG_POSTS.c_by where posts_blog_id =" + blogId + " ORDER BY BRAND_BLOG_POSTS.SortOrder DESC";

            dt = dp.FillDataTable(strQuery);
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
        return dt;
    }
  
    protected void dgrdBlogPosts_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            hdBlogPostId.Value = e.Item.Cells[1].Text.Trim();
            if (e.CommandName.Equals("Preview") || e.CommandName.Equals("View") )
            {
                chbxRandomiseBlogOrder.Visible = false;
                BindBlogDetails();
            }
            if( e.CommandName.Equals("Edit"))
            {
                chbxRandomiseBlogOrder.Visible = false;
                hdEditSource.Value = "grid";
                ShowEditDetails();
            }
            if (e.CommandName == "Delete")
            {
                string strQuery = string.Empty;
                strQuery = "delete BRAND_BLOG_POSTS where post_id=" + hdBlogPostId.Value + ";"
                        + " delete BRAND_POST_COMMENTS where coment_post_id=" + hdBlogPostId.Value + ";";
                dp.ExecuteNonQuery(strQuery);

                //Cache Code 
                ClearCache();

                BindBlogPosts();
            }
            if (e.CommandName.Equals("Up"))
            {
                if (e.Item.Cells[1].Text.Trim() != "")
                {
                    int PostID = Convert.ToInt32(e.Item.Cells[1].Text.Trim());

                    string strQuery = "DECLARE @SortOrder1 int=0"
                                    + " DECLARE @SortOrder2 int=0"
                                    + " SET @SortOrder1=(SELECT ISNULL(SORTORDER,0) FROM BRAND_BLOG_POSTS WHERE POST_ID=" + PostID + ")"
                                    + " SET @SortOrder2=(SELECT ISNULL(MIN(SORTORDER),0) FROM BRAND_BLOG_POSTS WHERE SORTORDER>@SortOrder1)"
                                    + " UPDATE BRAND_BLOG_POSTS SET SORTORDER=@SortOrder1 WHERE SORTORDER=@SortOrder2"
                                    + " UPDATE BRAND_BLOG_POSTS SET SORTORDER=@SortOrder2 WHERE POST_ID=" + PostID;
                    dp.ExecuteNonQuery(strQuery);

                    //Cache Code 
                    ClearCache();

                    BindBlogPosts();
                }
            }
            if (e.CommandName.Equals("Down"))
            {
                if (e.Item.Cells[1].Text.Trim() != "")
                {
                    int PostID = Convert.ToInt32(e.Item.Cells[1].Text.Trim());

                    string strQuery = "DECLARE @SortOrder1 int=0"
                                    + " DECLARE @SortOrder2 int=0"
                                    + " SET @SortOrder1=(SELECT ISNULL(SORTORDER,0) FROM BRAND_BLOG_POSTS WHERE POST_ID=" + PostID + ")"
                                    + " SET @SortOrder2=(SELECT ISNULL(MAX(SORTORDER),0) FROM BRAND_BLOG_POSTS WHERE SORTORDER<@SortOrder1)"
                                    + " UPDATE BRAND_BLOG_POSTS SET SORTORDER=@SortOrder1 WHERE SORTORDER=@SortOrder2"
                                    + " UPDATE BRAND_BLOG_POSTS SET SORTORDER=@SortOrder2 WHERE POST_ID=" + PostID;
                    dp.ExecuteNonQuery(strQuery);

                    //Cache Code 
                    ClearCache();
                    BindBlogPosts();
                }
            }
        }
    }

    private void BindBlogDetails()
    {
      
        if (hdBlogPostId.Value != "")
        {
            string strSql = "SELECT  post_id,post_title,fullname,post_description,BRAND_BLOG_POSTS.c_by,BRAND_BLOG_POSTS.c_date,post_tags, "
                            + " (SELECT COUNT(*) FROM BRAND_POST_COMMENTS WHERE coment_post_id=post_id) AS CommentsCount,"
                            + " (SELECT COUNT(DISTINCT c_by) FROM BRAND_POST_COMMENTS WHERE coment_post_id=post_id) AS CommentsUserCount,"
                            + " (select features from brand_settings) AS Features"
                            + " FROM BRAND_BLOG_POSTS "
                            + " INNER JOIN BRAND_BLOG_NEW ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id "
                            + " left join brand_user on BRAND_BLOG_POSTS.c_by=brand_user.uid  "
                            + " where post_id=" + hdBlogPostId.Value + " ORDER BY c_date DESC";

            DataTable dt = dp.FillDataTable(strSql);
            if (dt.Rows.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["post_id"])))
                {
                    hdBlogPostId.Value = Convert.ToString(dt.Rows[0]["post_id"]);

                    string strPostTitle = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["post_title"]));
                    if (strPostTitle.Length > 50)
                        strPostTitle = strPostTitle.Substring(0, 50) + "...";
                    lblBlogPostTitle.Text = strPostTitle;

                    DateTime dat = Convert.ToDateTime(Convert.ToString(dt.Rows[0]["c_date"]));
                    lblPostingDetails.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["fullname"])) + ", " + Convert.ToString(dat.Day) + "." + Convert.ToString(dat.Month) + "." + Convert.ToString(dat.Year);

                    string postDesc = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["post_description"]));
                    lbPostDescription.Text = postDesc.Replace("<ul>", "<ul style='list-style-type:square'>");

                    if (Convert.ToString(dt.Rows[0]["post_tags"]).Length > 50)
                        lbPostTags.Text = "<b>Tags: </b>" + Convert.ToString(dt.Rows[0]["post_tags"]).Substring(0, 49) + "...";
                    else
                        lbPostTags.Text = "<b>Tags: </b>" + Convert.ToString(dt.Rows[0]["post_tags"]);

                    lblCommentsCount.Text = Convert.ToString(dt.Rows[0]["CommentsCount"]);

                    dgrdComment.DataSource = getBlogcomment(this.hdBlogPostId.Value);
                    dgrdComment.DataBind();

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["Features"])))
                    {
                        string[] strArrFeatures = Convert.ToString(dt.Rows[0]["Features"]).Split(',');
                        postcomment.Visible = true;
                        dvComments.Visible = true;
                    }
                    else
                    {
                        postcomment.Visible = false;
                        dvComments.Visible = false;
                    }
                }
            }
             mView.SetActiveView(vShowBlog);
        }
        else
            BindBlogPosts();
    }

    private void ShowEditDetails()
    {
        SetFCKPath();

        string strSql = "SELECT  post_id,post_title,post_description,post_Latitude,post_Longitude,post_tags FROM BRAND_BLOG_POSTS "
                        + " INNER JOIN BRAND_BLOG_NEW ON BRAND_BLOG_NEW.blog_id = BRAND_BLOG_POSTS.posts_blog_id "
                        + " left join brand_user on BRAND_BLOG_POSTS.c_by=brand_user.uid  "
                        + " where post_id=" + hdBlogPostId.Value + " ORDER BY BRAND_BLOG_POSTS.c_date DESC";

        DataTable dt = dp.FillDataTable(strSql);
        if (dt.Rows.Count > 0)
        {
            this.txtTitle.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["post_title"]));
            this.FCKDesc.Value = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["post_description"]));
            this.txtTags.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["post_tags"]));
            if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["post_Latitude"]))
                && Convert.ToDecimal(dt.Rows[0]["post_Latitude"]) != 0)
            {
                this.txtLatitude.Text = Convert.ToString(dt.Rows[0]["post_Latitude"]);
            }
            else
            {
                txtLatitude.Text = ReadConfig.GetLatitude(ReadConfig.getDomainUrl());
            }
            if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["post_Longitude"]))
                && Convert.ToDecimal(dt.Rows[0]["post_Longitude"]) != 0)
            {
                this.txtLongitude.Text = Convert.ToString(dt.Rows[0]["post_Longitude"]);
            }
            else
            {
                txtLongitude.Text = ReadConfig.GetLongitude(ReadConfig.getDomainUrl());
            }
        }
        mView.SetActiveView(vEditBlog);
        btnUpdate.Visible = true;
        btnUpdate.Text = "update";
        btncreateupdate.Value = "update";
    }

    protected void dgrdBlogPosts_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        DataGrid dgrdAdminBlogPage = (DataGrid)sender;
        dgrdAdminBlogPage.CurrentPageIndex = e.NewPageIndex;
        DataTable dtPost = getPostList(1);
        DataView dv = new DataView(dtPost);
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdAdminBlogPage.DataSource = dv;
        dgrdAdminBlogPage.DataBind();
    }

    protected void dgrdBlogPosts_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = getPostList(1);
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    protected void Blog_Event(Object sender, EventArgs e)
    {
        LinkButton blogEventBtn = (LinkButton)sender;
        if (!string.IsNullOrWhiteSpace(hdBlogPostId.Value))
        {
            if (blogEventBtn.CommandName.Equals("edit"))
            {
                chbxRandomiseBlogOrder.Visible = false;
                hdEditSource.Value = "blog";
                ShowEditDetails();
            }
            if (blogEventBtn.CommandName.Equals("delete"))
            {
                string strQuery = string.Empty;
                strQuery = "delete BRAND_BLOG_POSTS where post_id=" + hdBlogPostId.Value + ";"
                        + " delete BRAND_POST_COMMENTS where coment_post_id=" + hdBlogPostId.Value + ";";
                dp.ExecuteNonQuery(strQuery);
                //Cache Code 
                ClearCache();
                BindBlogPosts();
                chbxRandomiseBlogOrder.Visible = true;
            }
        }
        else
            BindBlogPosts();
    }

    public DataTable getBlogcomment(string id)
    {
        DataTable dt = new DataTable();
        if (id != "")
        {
            string strSql = " SELECT id, coment_post_id, comment_desc, coment_post_id, comment_tags,BRAND_POST_COMMENTS.c_date, BRAND_POST_COMMENTS.c_by, " +
                            " brand_user.fullname FROM BRAND_POST_COMMENTS  left join brand_user " +
                            " on BRAND_POST_COMMENTS.c_by=brand_user.uid where coment_post_id=" + id + "  ";
            dt = dp.FillDataTable(strSql);
        }
        return dt;
    }
    
    protected void dgrdBlogComment_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        DataGrid dgrdComment = (DataGrid)sender;
        hdnCommentId.Value = e.Item.Cells[0].Text;
        string commentPostID = e.Item.Cells[1].Text;
        if (e.CommandName == "delete")
        {
            string strSql = "DELETE FROM BRAND_POST_COMMENTS where id=" + hdnCommentId.Value + " ";
            dp.ExecuteNonQuery(strSql);
            lblmessage.Text = "comment deleted successfully";

            BindBlogDetails();
            dgrdComment.DataSource = getBlogcomment(commentPostID);
            dgrdComment.DataBind();
        }
    }

    private void SetBlank()
    {
        txtTitle.Text = "";
        FCKDesc.Value = "";
        txtTags.Text = "";
        txtLatitude.Text = "";
        txtLongitude.Text = "";
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            txtTags.Text = txtTags.Text.Trim().Replace(";", ",");
            string[] tags = txtTags.Text.Split(',');
            int tagcnt = 0;
            foreach (string str in tags)
            {
                if (str.Trim().Length > 0)
                    tagcnt++;
                else
                    continue;
                if (str.Length > 25)
                {
                    lblmessage.Text = "Tag length should not exceeded more than 25 characters";
                    mView.SetActiveView(vEditBlog);
                    return;
                }
            }

            string strQuery = "";
            decimal Latitude = 0.0m;
            if (txtLatitude.Text.Trim() != "")
                Latitude = Convert.ToDecimal(txtLatitude.Text.Trim());
            decimal Longitude = 0.0m;
            if (txtLongitude.Text.Trim() != "")
                Longitude = Convert.ToDecimal(txtLongitude.Text.Trim());

            if (this.btnUpdate.Text == "update")
            {
                strQuery = " UPDATE BRAND_BLOG_POSTS "
                           + " SET "
                           + " post_title='" + utils.formatStringForDBInsert(txtTitle.Text) + "'"
                           + " ,post_description='" + utils.formatStringForDBblogInsert(FCKDesc.Value) + "'"
                           + " ,m_by=" + Convert.ToString(Session["userid"])
                           + " ,post_tags='" + utils.formatStringForDBInsert(txtTags.Text) + "'"
                           + " ,Post_QLink_Id='-1'"
                           + " ,post_latitude=" + Latitude
                           + " ,post_longitude=" + Longitude
                           + " WHERE post_id=" + this.hdBlogPostId.Value;
                dp.ExecuteNonQuery(strQuery);
            }
            else
            {
                strQuery = "insert into BRAND_BLOG_POSTS ("
                        + " post_title"
                        + " ,post_description"
                        + " ,c_by"
                        + " ,post_tags"
                        + " ,posts_blog_id"
                        + " ,Post_QLink_Id"
                        + " ,SortOrder"
                        + " ,post_latitude"
                        + " ,post_longitude) "
                        + " values('" + utils.formatStringForDBInsert(txtTitle.Text) + "'"
                        + ",'" + utils.formatStringForDBblogInsert(FCKDesc.Value) + "'"
                        + "," + Convert.ToString(Session["userid"])
                        + ",'" + utils.formatStringForDBInsert(txtTags.Text) + "'"
                        + ",1"
                        + ",'-1'"
                        + ",(SELECT ISNULL(MAX(SORTORDER),0)+1 FROM BRAND_BLOG_POSTS)"
                        + "," + Latitude
                        + "," + Longitude
                        + ");" 
                        +" SELECT SCOPE_IDENTITY(); ";
                this.hdBlogPostId.Value = Convert.ToString(dp.ExecuteScalar(strQuery));
            } 
            //Cache Code  
            ClearCache();

            lblmessage.Text = "Action performed successfully.";
            BindBlogPosts();
            SetBlank();
            chbxRandomiseBlogOrder.Visible = true;
            this.hdBlogPostId.Value = "-1";
            return;
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetBlank();
        if (hdEditSource.Value.ToLower() == "blog")
            mView.SetActiveView(vShowBlog);
        else
        {
            chbxRandomiseBlogOrder.Visible = true;
            mView.SetActiveView(Vgrid);
        }
    }

    protected void lnkBackBlog_Click(object sender, EventArgs e)
    {
        SetBlank();
        chbxRandomiseBlogOrder.Visible = true;
        mView.SetActiveView(Vgrid);
    }

    protected void lnkbtnAddPost_OnClick(object source, EventArgs e)
    {
        SetBlank();
        txtLatitude.Text = ReadConfig.GetLatitude(ReadConfig.getDomainUrl());
        txtLongitude.Text = ReadConfig.GetLongitude(ReadConfig.getDomainUrl());
        this.btnUpdate.Text = "create";
        btncreateupdate.Value = "create";
        btnUpdate.Visible = true;
        chbxRandomiseBlogOrder.Visible = false;
        hdEditSource.Value = "grid";
        SetFCKPath();
        mView.SetActiveView(vEditBlog);
    }

    protected void btnAddCmt_OnClick(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrWhiteSpace(this.hdBlogPostId.Value))
            {
                if (hdnCommentId.Value.Trim() != "")
                {
                    string strSql = "Update BRAND_POST_COMMENTS set comment_desc = '" + utils.formatStringForDBInsert(txtEditorComment.Text) + "' where id =" + hdnCommentId.Value;
                    dp.ExecuteNonQuery(strSql);
                }
                else
                {
                    string strSql = "insert into BRAND_POST_COMMENTS(coment_post_id,comment_desc,c_by,comment_blog_id) values(" + this.hdBlogPostId.Value + ",'" + utils.formatStringForDBInsert(txtEditorComment.Text) + "'," + Convert.ToString(Session["userid"]) + ",1);"
                                    + " SELECT SCOPE_IDENTITY(); ";
                    string postID = Convert.ToString(dp.ExecuteScalar(strSql));
                }

                this.lblmessage.Text = "comment posted successfully";
                txtEditorComment.Text = "";
                divAddComments.Style.Value = "display:none";
                BindBlogDetails();
            }
            else
                BindBlogPosts();
        }
        catch (Exception ex)
        {
            this.lblmessage.Text = ex.Message.ToString();
        }
    }

    protected void btnFeaturedUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string BlogPostsID_Checked = string.Empty;
            string BlogPostsID_UnChecked = string.Empty;
            foreach (DataGridItem dgrdItem in dgrdBlogPosts.Items)
            {
                if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkFeatured = (CheckBox)dgrdItem.FindControl("chkFeatured");
                    if (chkFeatured != null)
                    {
                        if (chkFeatured.Checked == true)
                            BlogPostsID_Checked += dgrdItem.Cells[1].Text.Trim() + ",";
                        else
                            BlogPostsID_UnChecked += dgrdItem.Cells[1].Text.Trim() + ",";
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(BlogPostsID_Checked.Trim(',')))
                dp.ExecuteNonQuery("update BRAND_BLOG_POSTS set Active_Inactive = 1 where post_id IN (" + BlogPostsID_Checked.Trim(',') + ")");
            if (!string.IsNullOrWhiteSpace(BlogPostsID_UnChecked.Trim(',')))
                dp.ExecuteNonQuery("update BRAND_BLOG_POSTS set Active_Inactive = 0 where post_id IN (" + BlogPostsID_UnChecked.Trim(',') + ")");

             
            

            //Cache Code 
            ClearCache();

            BindBlogPosts();
            lblmessage.Text = "Active blog posts saved successfully";
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void btnDeleteUpdate_Click(object sender, EventArgs e)
    {
        string BlogPostsID = string.Empty;
        try
        {
            foreach (DataGridItem dgrdItem in dgrdBlogPosts.Items)
            {
                if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkDelete = (CheckBox)dgrdItem.FindControl("chkDelete");
                    if (chkDelete != null && chkDelete.Checked == true)
                        BlogPostsID += dgrdItem.Cells[1].Text.Trim() + ",";
                }
            }
            if (!string.IsNullOrWhiteSpace(BlogPostsID.Trim(',')))
            {
                string strQuery = "delete BRAND_BLOG_POSTS where post_id IN (" + BlogPostsID.Trim(',') + ");"
                                + " delete BRAND_POST_COMMENTS where coment_post_id IN  (" + BlogPostsID.Trim(',') + ");";
                dp.ExecuteNonQuery(strQuery);
            }

            //Cache Code 
            ClearCache();

            lblmessage.Text = "Selecetd blog posts deleted successfully";
            BindBlogPosts();
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    private string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    private bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }

    //Cache Code 
    private void ClearCache()
    {
        string KeySortBlogPosts = "SortBlogPosts" + ReadConfig.getDomainUrl();
        if (Cache[KeySortBlogPosts] != null)
            Cache.Remove(KeySortBlogPosts);
        string KeySortBlogPostsHome = "SortBlogPostsHome" + ReadConfig.getDomainUrl();
        if (Cache[KeySortBlogPostsHome] != null)
            Cache.Remove(KeySortBlogPostsHome);
        string Keymostpopular = "mostpopular-" + ReadConfig.getDomainUrl();
        if (Cache[Keymostpopular] != null)
            Cache.Remove(Keymostpopular);
        string Key = "TOTALNUMBEROFBLOGPOST_" + ReadConfig.getDomainUrl();
        if (Cache[Key] != null)
            Cache.Remove(Key);
    }

    private void SetFCKPath()
    {
        string strPath = Request.ApplicationPath;
        if (strPath == "/")
        {
            FCKDesc.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
        }
        else
        {
            FCKDesc.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
        }
    }
    
    protected void FCKDesc_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedField(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this,
                                                    FCKDesc.GetType(),
                                                    "FCKUpdater",
                                                    script,
                                                    true);
            ScriptManager.RegisterOnSubmitStatement(this,
                                                    FCKDesc.GetType(),
                                                    "WebContentManagerEditorScript_" + this.FCKDesc.ClientID,
                                                    "FCKUpdateLinkedField('" + this.FCKDesc.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKDesc.GetType(),
                                                        "FCKUpdater",
                                                        script,
                                                        true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKDesc.GetType(),
                                                             "WebContentManagerEditorScript_" + this.FCKDesc.ClientID,
                                                             "FCKUpdateLinkedField('" + this.FCKDesc.ClientID + "');");
        }
    }

    protected void chbxRandomiseBlogOrder_CheckedChanged(object sender, EventArgs e)
    {
        dp.ExecuteNonQuery("update brand_settings set RandomiseStatus= " + Convert.ToInt32(chbxRandomiseBlogOrder.Checked) + ";");
    }

    protected void FCKDesc_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.FCKDesc.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, FCKDesc.GetType(), "FCKSource", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, FCKDesc.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKDesc.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKDesc.GetType(), "FCKSource", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKDesc.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKDesc.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
    }

    protected void lnkExportBlogPosts_Click(object sender, EventArgs e)
    {
        try
        {
            string strQuery = "SELECT ROW_NUMBER() OVER (order by [SortOrder]) as [SNo.],[post_title],[post_description],[posts_blog_id],[post_status]"
           + ",[Active_Inactive],[SortOrder],[post_latitude],[post_longitude],'' as ImageUrls FROM BRAND_BLOG_POSTS";
            DataTable dt = new DataTable();
            dt = dp.FillDataTable(strQuery);
            if (dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    string imageurls = "";
                    string desc = utils.formatStringForDBSelect(dr["post_description"].ToString());
                    foreach (Match m in Regex.Matches(desc, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase | RegexOptions.Multiline))
                    {
                        string src = m.Groups[1].Value;
                        imageurls += src + "$";
                    }
                    imageurls = imageurls.TrimEnd('$');
                    dr["ImageUrls"] = imageurls;
                    string strdesc = Server.HtmlEncode(dr["post_description"].ToString().Replace(ReadConfig.getDomainUrl(),"[newDomain]").Replace("\n","<br/>"));
                    dr["post_description"] = strdesc;

                    string post_title = utils.formatStringForDBInsert(dr["post_title"].ToString());
                    dr["post_title"] = post_title;
                }
            }

            string csvdata = DataTableToCSV(dt, ',');

            //DataGrid dgrdExport = new DataGrid();
            //dgrdExport.DataSource = dt;
            //dgrdExport.DataBind();
            string fileName = "BlogPosts-" + ReadConfig.getDomainUrl() + ".csv";//DateTime.Now.ToString("yyyyMMddHHmmss")
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.Charset = "";
            Response.ContentType = "text/csv";
            //StringWriter StringWriter = new System.IO.StringWriter();
            //HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);
            //dgrdExport.RenderControl(HtmlTextWriter);
            Response.Write(csvdata);
            Response.End();
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void lnkbtnImpExp_Click(object sender, EventArgs e)
    {
        mView.SetActiveView(VImportExport);
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        if (fileImport.Value != null && fileImport.Value != "")
        {
            string strFileExt = fileImport.PostedFile.FileName.Substring(fileImport.PostedFile.FileName.LastIndexOf(".") + 1);
            string sheetName = fileImport.PostedFile.FileName.Substring(0, fileImport.PostedFile.FileName.LastIndexOf("."));
            string strFileName = System.DateTime.UtcNow.ToFileTimeUtc().ToString() + "." + strFileExt;
            string strFilePath = Server.MapPath("~/Config/" + ReadConfig.getDomainUrl() + "/upload/userfiles/" + strFileName);
            fileImport.PostedFile.SaveAs(strFilePath);

            DataTable dtCsv = new DataTable();


            using (StreamReader sr = new StreamReader(strFilePath))
            {
                while (!sr.EndOfStream)
                {
                    string Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Length - 1; i++)
                    {
                        //split each row with comma to get individual values  
                        if (i == 0)
                        {
                            string[] rowValues = rows[i].Split(',');
                            for (int j = 0; j < rowValues.Length; j++)
                            {
                                dtCsv.Columns.Add(rowValues[j].Replace("\r","")); //add headers  
                            }
                        }
                        else
                        {
                            string[] rowValues = rows[i].Replace("\",\"","\"^\"").Split('^');
                            DataRow dr = dtCsv.NewRow();
                            for (int k = 0; k < rowValues.Length; k++)
                            {
                                if(k==2)
                                {
                                    dr[k] = utils.formatStringForDBSelect(Server.HtmlDecode(rowValues[k].Trim('"').ToString())).Replace("[newDomain]", ReadConfig.getDomainUrl());
                                }
                                else
                                {
                                    dr[k] = utils.formatStringForDBSelect(rowValues[k].Trim('"').ToString());
                                }
                            }
                            dtCsv.Rows.Add(dr); //add other rows  
                        }
                    }
                }
            }


            if (dtCsv.Rows.Count > 0)
            {
                this.dgrdImpBlogPosts.DataSource = dtCsv;
                this.dgrdImpBlogPosts.DataBind();
                this.dgrdImpBlogPosts.Visible = true;
                this.btnImpblogPostSave.Visible = true;
                this.btnImpblogPostSaveAll.Visible = true;
                this.btnImpblogPostSave2.Visible = true;
                this.btnImpblogPostSaveAll2.Visible = true;
            }
        }
    }

    protected void btnImpblogPostSave_Click(object sender, EventArgs e)
    {
        string strQuery = "";
        string images = "";
        try
        {
            foreach (DataGridItem dgrdItem in dgrdImpBlogPosts.Items)
            {
                if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkPost = (CheckBox)dgrdItem.FindControl("chkPost");

                    if (chkPost != null && chkPost.Checked)
                    {
                        strQuery += "insert into dbo.BRAND_BLOG_POSTS(post_title, post_description,posts_blog_id,post_status,Active_Inactive,SortOrder,post_latitude,post_longitude, c_by) values"
                                   + " ('" + utils.formatStringForDBInsert(dgrdItem.Cells[1].Text.Trim()) + "','" + utils.formatStringForDBInsert(dgrdItem.Cells[2].Text.Trim())
                                   + "',1,1,0,'" + utils.formatStringForDBInsert(dgrdItem.Cells[3].Text.Trim()) + "','" 
                                   + utils.formatStringForDBInsert(dgrdItem.Cells[5].Text.Trim()) + "','" + utils.formatStringForDBInsert(dgrdItem.Cells[6].Text.Trim()) 
                                   + "'," + Convert.ToString(Session["userid"]) + ";";

                        images += utils.formatStringForDBSelect(dgrdItem.Cells[4].Text.Trim()) + ",";

                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(strQuery))
            {
                dp.ExecuteNonQuery(strQuery);
                CopyImages(images);
                bindChkBxRandomiseStatus();
                BindBlogPosts();
                ClearCache();
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void btnImpblogPostSaveAll_Click(object sender, EventArgs e)
    {
        string strQuery = "";
        string images = "";
        try
        {
            foreach (DataGridItem dgrdItem in dgrdImpBlogPosts.Items)
            {
                strQuery += "insert into dbo.BRAND_BLOG_POSTS(post_title, post_description,posts_blog_id,post_status,Active_Inactive,SortOrder,post_latitude,post_longitude, c_by) values"
                        + " ('" + utils.formatStringForDBInsert(dgrdItem.Cells[1].Text.Trim()) + "','" + utils.formatStringForDBInsert(dgrdItem.Cells[2].Text.Trim())
                        + "',1,1,0,'" + utils.formatStringForDBInsert(dgrdItem.Cells[3].Text.Trim()) + "','"
                        + utils.formatStringForDBInsert(dgrdItem.Cells[5].Text.Trim()) + "','" + utils.formatStringForDBInsert(dgrdItem.Cells[6].Text.Trim())
                        + "'," + Convert.ToString(Session["userid"]) + ");";
                images += utils.formatStringForDBSelect(dgrdItem.Cells[4].Text.Trim()) + "$";
            }
            if (!string.IsNullOrWhiteSpace(strQuery))
            {
                dp.ExecuteNonQuery(strQuery);
                CopyImages(images);
                bindChkBxRandomiseStatus();
                BindBlogPosts();
                ClearCache();
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    private string DataTableToCSV(DataTable datatable, char seperator)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < datatable.Columns.Count; i++)
        {
            sb.Append(datatable.Columns[i]);
            if (i < datatable.Columns.Count - 1)
                sb.Append(seperator);
        }
        sb.AppendLine();
        foreach (DataRow dr in datatable.Rows)
        {
            for (int i = 0; i < datatable.Columns.Count; i++)
            {
                sb.Append("\""+dr[i].ToString()+"\"");

                if (i < datatable.Columns.Count - 1)
                    sb.Append(seperator);
            }
            sb.AppendLine();
        }
        return sb.ToString();
    }

    private void CopyImages(string Images)
    {
        if(Images.Trim().Trim('$').Trim('"') != "")
        {
            string[] arrimages = Images.Trim().Trim('$').Trim('"').Split('$');
            for(int i=0;i < arrimages.Length; i++)
            {
                string imagepath = arrimages[i].Trim().Trim('$').Trim('"');
                if(imagepath != "")
                {
                    string originalImagepath = Server.MapPath(imagepath);
                    if(File.Exists(originalImagepath))
                    {
                        string filename = imagepath.Substring(imagepath.LastIndexOf("/") + 1);
                        string copyimagepath = "/config/"+ ReadConfig.getDomainUrl()  + "/upload/userfiles/"+ filename;
                        copyimagepath = Server.MapPath(copyimagepath);
                        File.Copy(originalImagepath, copyimagepath, true);
                    }
                }
            }
        }
    }
}