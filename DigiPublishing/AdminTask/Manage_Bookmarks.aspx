﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_Bookmarks.aspx.cs" Inherits="AdminTask_Manage_Bookmarks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">    
<div class="content-full">    
<h3 class="HeaderClass">Manage Bookmark</h3>
    <script>
        function UserDeleteConfirmation() {
            return confirm("Are you sure you want to delete all bookmarks?");
        }
    </script>
    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
    <div class="AdminRightMenu">
        <asp:LinkButton ID="btnAddBookMark" OnClick="btnAddBookMark_Click" runat="server" CssClass="toplink" Text="Add Bookmark">
        </asp:LinkButton>
        <p>
            <asp:LinkButton ID="lnkbtnImpExp" runat="server" Text="Import/Export" OnClick="lnkbtnImpExp_Click" />
        </p>
    </div>
    <table style="border-collapse:collapse;" border="1">
        <tr>
            <td style="background-color:white!important;padding:5px;text-align:right;border:1px solid gray;">
                <asp:CheckBox ID="chbxRandomiseLinkOrderinSummary" Text="Randomise links in Summary" TextAlign="Left" runat="server" AutoPostBack="true" 
                    OnCheckedChanged="chbxRandomiseLinkOrderinSummary_CheckedChanged" />
            </td>
            <td style="background-color:white!important;padding:5px;text-align:right;border:1px solid gray;">
                Bookmark in Summary &nbsp;: &nbsp;<asp:TextBox ID="txtNumberOfLinksInSummary" runat="server" MaxLength="2"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtNumberOfLinksInSummary" runat="server" ErrorMessage="Numbers Only" 
                        ValidationExpression="\d+" ValidationGroup="vgbookmarkinsummry" ForeColor="Red"></asp:RegularExpressionValidator>
            </td>
            <td style="background-color:white!important;padding:5px;border:1px solid gray;">
                 <asp:Button ID="SaveSummaryDetail" runat="server" Text="Save" OnClick="SaveSummaryDetail_Click" />
            </td>
        </tr>
        <tr>
            <td style="background-color:white!important;padding:5px;text-align:right;border:1px solid gray;">
                <asp:CheckBox ID="chbxRandomiseLinkOrderinLinks" Text="Randomise links" TextAlign="Left" runat="server" AutoPostBack="true" 
                    OnCheckedChanged="chbxRandomiseLinkOrderinLinks_CheckedChanged" />
            </td>
            <td style="background-color:white!important;padding:5px;text-align:right;border:1px solid gray;">
                Number of Links&nbsp; : &nbsp;
                <asp:TextBox ID="txtNumberOfLinksInLinks" runat="server" MaxLength="2"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtNumberOfLinksInLinks" runat="server" ErrorMessage="Numbers Only"
                    ValidationExpression="\d+" ValidationGroup="vgbookmarkinsummry" ForeColor="Red"></asp:RegularExpressionValidator>
            </td>
            <td style="background-color:white!important;padding:5px;border:1px solid gray;">
                <asp:Button ID="btnLinksDetail" runat="server" Text="Save" OnClick="btnLinksDetail_Click" />
            </td>
        </tr>
    </table>
    <br />
    <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
        <asp:View ID="vGrid" runat="server">
            <table border="0" style="width:100%;border:0px!important">
                <tbody>
                    <tr>
                        <td rowspan="2" style="width:58%;text-align:right;padding:0px;padding-right:10px;background-color:white!important;">
                            <h3 style="margin:0px;">Settings For All Bookmarks</h3>
                        </td>
                        <td style="width:10%;text-align:center;padding:2px;border:1px solid black;">
                            <table>
                                <tr>
                                    <td colspan="3">Rel</td>
                                </tr>
                                <tr>
                                    <td>nofollow</td>
                                    <td>noindex</td>
                                    <td>noopener</td>
                                    <td>noreferrer</td> <!--RK-->
                                </tr>
                            </table>
                        </td>
                        <td style="width:15%;text-align:center;padding:2px;border:1px solid black;">
                            Click
                        </td>
                        <td style="width:15%;text-align:center;padding:2px;border:1px solid black;">
                            Click IP
                        </td>
                        <td style="width:5%;text-align:center;padding:2px;border:1px solid black;">
                            Action
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%;text-align:center;padding:2px;border:1px solid black;background-color:white!important;">
                            <table>
                                <tr>
                                    <td style="background-color:white!important;">
                                        <asp:CheckBox ID="chknofollowall" runat="server" />
                                    </td>
                                    <td style="background-color:white!important;">
                                        <asp:CheckBox ID="chknoindexall" runat="server" />
                                    </td>
                                    <td style="background-color:white!important;">
                                        <asp:CheckBox ID="chknoopenerall" runat="server" />
                                    </td>
                                    <td style="background-color:white!important;"><!--RK-->
                                        <asp:CheckBox ID="chknoreferrerall" runat="server" />
                                    </td><!--RK-->
                                </tr>
                            </table>
                        </td>
                        <td style="width:15%;text-align:center;padding:2px;border:1px solid black;background-color:white!important;">
                            <asp:CheckBox ID="chkStatusClickAll" runat="server"></asp:CheckBox>&nbsp;&nbsp;
                            <asp:TextBox ID="txtValueClickAll" runat="server" Width="30px" Text='0'></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtValueClickAll" runat="server" ErrorMessage="Numbers Only" 
                                ValidationExpression="\d+" ValidationGroup="vgbookmarkAll" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Value must be greater than zero." ControlToValidate="txtValueClickAll" 
                                MinimumValue="1" MaximumValue="1000" Type="Integer" ValidationGroup="vgbookmarkAll" ForeColor="Red" Display="Dynamic"></asp:RangeValidator>
                        </td>
                        <td style="width:15%;text-align:center;padding:2px;border:1px solid black;background-color:white!important;">
                            <asp:CheckBox ID="chkStatusClickIPAll" runat="server"></asp:CheckBox>&nbsp;&nbsp;
                            <asp:TextBox ID="txtValueClickIPAll" runat="server" Width="30px" Text='0'></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtValueClickIPAll" runat="server" ErrorMessage="Numbers Only" 
                                ValidationExpression="\d+" ValidationGroup="vgbookmarkAll" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Value must be greater than zero." ControlToValidate="txtValueClickIPAll" 
                                MinimumValue="1" MaximumValue="1000" Type="Integer" ValidationGroup="vgbookmarkAll" ForeColor="Red" Display="Dynamic"></asp:RangeValidator>
                        </td>
                        <td style="width:5%;text-align:center;padding:2px;border:1px solid black;background-color:white!important;">
                            <asp:LinkButton ID="lnkdeleteAll" runat="server" CssClass="toplink" Text="delete" OnClientClick="if(!UserDeleteConfirmation())return false;" OnClick="lnkdeleteAll_Click"></asp:LinkButton>                                        
                        </td>
                    </tr>
                </tbody>
            </table> 
            <div style="position:absolute;right:15px;top:260px;">
                <asp:Button ID="btnSaveForAll" runat="server" CssClass="btnBgClass" ValidationGroup="vgbookmarkAll" Text="Save for All" OnClick="btnSaveForAll_Click" />
            </div>
            <hr />
            <asp:DataGrid ID="grdBookmark" runat="server" AutoGenerateColumns="false" AllowPaging="True" PageSize="20"
                 PagerStyle-Mode="NumericPages" OnPageIndexChanged="grdBookmark_PageIndexChanged" OnItemCommand="grdBookmark_ItemCommand">
                <Columns>
                    <asp:BoundColumn DataField="Id" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn ItemStyle-Width="3%">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnUP" runat="server" AlternateText="upimg" ImageUrl="~/images_common/upimg.gif" CommandName="Up" />
                            <asp:ImageButton ID="imgbtnDown" runat="server" AlternateText="DownImg" ImageUrl="~/images_common/DownImg.gif" CommandName="Down" />
                        </ItemTemplate>
                    </asp:TemplateColumn>                                
                    <asp:TemplateColumn HeaderText="Bookmark" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <asp:TextBox ID="txtEditBookmark" runat="server" Width="95%" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("BK_Title"))) %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn> 
                    <asp:TemplateColumn HeaderText="Description" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <asp:TextBox ID="txtEditDescription" runat="server" Width="95%" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("BK_Desc"))) %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="URL" ItemStyle-Width="25%">
                        <ItemTemplate>
                            <asp:TextBox ID="txtEditURL" runat="server" Width="98%" Text='<%# Eval("BK_URL") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>   
                    <asp:TemplateColumn HeaderText="Rel" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td style="background-color:white!important;">
                                        <asp:CheckBox ID="chknofollow" runat="server" Checked='<%#Convert.ToBoolean(Eval("nofollow")) %>' />
                                    </td>
                                    <td style="background-color:white!important;">
                                        <asp:CheckBox ID="chknoindex" runat="server" Checked='<%#Convert.ToBoolean(Eval("noindex")) %>' />
                                    </td>
                                    <td style="background-color:white!important;">
                                        <asp:CheckBox ID="chknoopener" runat="server" Checked='<%#Convert.ToBoolean(Eval("noopener")) %>' />
                                    </td>
                                    <td style="background-color:white!important">
                                        <asp:CheckBox ID="chknoreferrer" runat="server" Checked='<%# Convert.ToBoolean(Eval("noreferrer")) %>' /><!--RK-->
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateColumn> 
                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Click" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkStatusClick" runat="server" Checked='<%#Convert.ToBoolean(Eval("Blocker")) %>'></asp:CheckBox>&nbsp;&nbsp;
                            <asp:TextBox ID="txtValueClick" runat="server" Width="30px" Text='<%#Eval("BlockerValue") %>'></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtValueClick" runat="server" ErrorMessage="Numbers Only" 
                                ValidationExpression="\d+" ValidationGroup="vgbookmarklist" ForeColor="Red"></asp:RegularExpressionValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Click IP" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkStatusClickIP" runat="server" Checked='<%#Convert.ToBoolean(Eval("SameIP_Status")) %>'></asp:CheckBox>&nbsp;&nbsp;
                            <asp:TextBox ID="txtValueClickIP" runat="server" Width="30px" Text='<%#Eval("SameIP_Value") %>'></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtValueClickIP" runat="server" ErrorMessage="Numbers Only" 
                                ValidationExpression="\d+" ValidationGroup="vgbookmarklist" ForeColor="Red"></asp:RegularExpressionValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action" ItemStyle-Width="5%">
                        <ItemTemplate>                                    
                            <asp:LinkButton ID="lnkdelete" runat="server" CssClass="toplink" CommandName="deleteBookmark" Text="delete" OnClientClick="return confirm('Are you sure to delete this record.');" ></asp:LinkButton>                                        
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
            <div class="AdminRightMenu">
                <asp:Button ID="btnBookMarks" OnClick="btnBookMarks_Click" CssClass="btnBgClass"  ValidationGroup="vgbookmarklist" runat="server" Text="Save"></asp:Button>
            </div>                        
            <asp:Label ID="lblNoSavedbookmark" runat="server" CssClass="error"></asp:Label>
        </asp:View>
        <asp:View ID="vAddBookMark" runat="server">
            <strong><asp:Label ID="lblAddBookMark" runat="server" text="Add bookmark"></asp:Label></strong>
            <table>
                <tr>
                    <td>
                        Title
                    </td>
                    <td>
                        <asp:TextBox ID="txttitle" runat="server" MaxLength="50" ValidationGroup="vldGrpBookmark"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="revTitle" runat="server" ControlToValidate="txttitle" ForeColor="Red"
                            ErrorMessage="please enter bookmark title" ValidationGroup="vldGrpBookmark" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        URL
                    </td>
                    <td>
                        <asp:TextBox ID="txtURL" runat="server" MaxLength="150" ValidationGroup="vldGrpBookmark"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvURL" runat="server" ControlToValidate="txtURL" ForeColor="Red"
                            ErrorMessage="please enter bookmark url" ValidationGroup="vldGrpBookmark" Display="Dynamic"></asp:RequiredFieldValidator><br />
                       <asp:RegularExpressionValidator ID="revURL" runat="server" ControlToValidate="txtURL" ForeColor="Red"
                            Display="Dynamic" ErrorMessage="please enter valid URL" SetFocusOnError="True"
                            ValidationExpression="(((http(s)?://)|([\w-]))+(\.))+[\w-]+(/[\w- ./?%&=]*)?" ValidationGroup="vldGrpBookmark"></asp:RegularExpressionValidator>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Description
                    </td>
                    <td>
                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="2" ValidationGroup="vldGrpBookmark"></asp:TextBox>   
                    </td>
                </tr>
                <tr>
                    <td>
                        nofollow
                    </td>
                    <td>
                        <asp:CheckBox ID="chknofollowadd" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td>
                        noindex
                    </td>
                    <td>
                        <asp:CheckBox ID="chknoindexadd" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td>
                       noopener
                    </td>
                    <td>
                        <asp:CheckBox ID="chknoopenerwadd" runat="server" />
                    </td>
                </tr>
                <tr><!--RK-->
                    <td>
                        noreferrer
                    </td>
                    <td>
                        <asp:CheckBox ID="chknoreferreradd" runat="server" />
                    </td>
                </tr><!--RK-->
                <tr>
                    <td>
                        Tags
                    </td>
                    <td>
                        <asp:TextBox ID="txtTags" runat="server" MaxLength="200"></asp:TextBox><br />
                        ( Separate by comma )
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnCreate" OnClick="btnUpdate_Click" runat="server" CssClass="btnBgClass"
                            Text="add" ValidationGroup="vldGrpBookmark"></asp:Button>
                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" CssClass="btnBgClass"
                            Text="cancel"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="VImportExport" runat="server">
            <div>
            <table>
                    <tr>
                        <td>
                           <h4>Import your Bookmarks</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <p>Select an file: <input id="fileImport" name="fileImport" type="file" runat="server" />                       
                        </p>
                        <p>
                          <asp:Button ID="btnImport" runat="server" Text="Upload" CssClass="btnBgClass" OnClick="btnImport_Click" />
                        </p>
                          <div style="width: 765px; overflow: auto;">
                              <asp:DataGrid ID="dgrdImpBookmark" runat="server" Visible="false" AllowPaging="true" PageSize="100" PagerStyle-Mode="NumericPages" 
                                  OnPageIndexChanged="dgrdImpBookmark_PageIndexChanged">
                                <Columns>                                
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                         <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkBookmark', this.checked)"/>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkBookmark"  runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Title" DataField="bookmark_title"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="URL" DataField="bookmark_url"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Desc" DataField="bookmark_desc"></asp:BoundColumn>
                                </Columns>
                              </asp:DataGrid>
                          </div> 
                          <br />
                          <asp:Button ID="btnImpbkmarkSave" CssClass="btnBgClass" Text="Import Selected" Visible="false" runat="server" OnClick="btnImpbkmarkSave_Click"/>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnImpbkmarkSaveAll" CssClass="btnBgClass" Text="Import All" Visible="false" runat="server" OnClick="btnImpbkmarkSaveAll_Click"/>
                            <p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                            <h4>Export your bookmarks</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <p>
                            <asp:LinkButton id="lnkExport" onclick="lnkExport_Click" runat="server" Text="Export the List"></asp:LinkButton>
                        </p>
                        </td>
                    </tr>
                </table>
        </div>
        </asp:View> 
    </asp:MultiView>
    </div>
    <asp:HiddenField ID="hdBookMarkID" runat="server" Value="-1"/>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID);  //generated control name starts with a colon
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal;
                }
            }
        }
    </script>
</asp:Content>

