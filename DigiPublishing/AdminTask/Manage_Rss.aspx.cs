﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminTask_Manage_Rss : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : RSS";
        lblMsg.Text = "";

        if (!IsPostBack)
            BindRssGrid();
    }

    /// <summary>
    /// Method to bind data grid with RSS data
    /// </summary>
    public void BindRssGrid()
    {
        DataTable dt = getSavedFeeds();

        this.drgdRssReader.DataSource = dt;
        this.drgdRssReader.DataBind();

        MVRssReader.SetActiveView(ViewRssGrid);
    }

    /// <summary>
    /// Return data table containing data for RSS Feeds
    /// </summary>
    /// <param name="strUserId"></param>
    /// <returns></returns>
    protected DataTable getSavedFeeds()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "[SP_Brand_Select_Rss]";
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = dp.FillDataTable(cmd);
        return dt;
    }

    /// <summary>
    /// Method Event to add new RSS Feed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddFeed_Click(object sender, EventArgs e)
    {
        resetValues();
    }

    /// <summary>
    /// Method event to create new RSS Feed URL
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        txtTags.Text = txtTags.Text.Trim().Replace(";", ",");
        string[] tags = txtTags.Text.Split(',');

        int tagcnt = 0;
        foreach (string str in tags)
        {
            if (str.Trim().Length > 0)
                tagcnt++;
            else
                continue;
            if (str.Length > 25)
            {
                lblMsg.Text = "Tag length should not exceeded more than 25 characters";
                return;
            }
        }

        if (txtTitle.Text.Trim() != "" && txtLink.Text.Trim() != "")
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "[SP_Brand_Insert_RSSFeeds]";
            cmd.AddParameter("@title", utils.formatStringForDBInsert(txtTitle.Text.Trim()));
            cmd.AddParameter("@link",utils.formatStringForDBInsert(txtLink.Text.Trim()));
            cmd.AddParameter("@rssTags",utils.formatStringForDBInsert(txtTags.Text));
            cmd.AddParameter("@c_by",Convert.ToInt32(Session["userid"]));
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = dp.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["IsExist"])) && Convert.ToString(dt.Rows[0]["IsExist"]) == "NO")
                {
                    ClearCache();
                }
                else
                {
                    lblMsg.Text = "RSS feed already exists";
                    return;
                }
            }
            this.lblMsg.Text = "Added successfully !";
            BindRssGrid();
            resetValues();
        }
    }

    /// <summary>
    /// Method event to cancel the updating or creating RSS Feed URL
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        resetValues();
        MVRssReader.SetActiveView(ViewRssGrid);
    }

    /// <summary>
    /// Method to reset all text field values
    /// </summary>
    private void resetValues()
    {
        this.txtTitle.Text = "";
        this.txtLink.Text = "";
        this.txtTags.Text = "";
        MVRssReader.SetActiveView(VAddRss);
    }

    /// <summary>
    /// Method to go back to RSS grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBackRss_Click(object sender, EventArgs e)
    {
        BindRssGrid();
    }

    /// <summary>
    /// This method is called when item is clicked in data grid drgdRssReader
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drgdRssReader_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Up"))
        {
            if (e.Item.Cells[0].Text.Trim() != "")
            {
                int ID = Convert.ToInt32(e.Item.Cells[0].Text.Trim());

                string strQuery = "DECLARE @SortOrder1 int=0"
                                + " DECLARE @SortOrder2 int=0"
                                + " SET @SortOrder1=(SELECT ISNULL(SORTORDER,0) FROM Brand_Rss WHERE feed_id=" + ID + ")"
                                + " SET @SortOrder2=(SELECT ISNULL(MIN(SORTORDER),0) FROM Brand_Rss WHERE SORTORDER>@SortOrder1)"
                                + " UPDATE Brand_Rss SET SORTORDER=@SortOrder1 WHERE SORTORDER=@SortOrder2"
                                + " UPDATE Brand_Rss SET SORTORDER=@SortOrder2 WHERE feed_id=" + ID;
                dp.ExecuteNonQuery(strQuery);
                BindRssGrid();
                ClearCache();
            }
        }
        if (e.CommandName.Equals("Down"))
        {
            if (e.Item.Cells[0].Text.Trim() != "")
            {
                int ID = Convert.ToInt32(e.Item.Cells[0].Text.Trim());

                string strQuery = "DECLARE @SortOrder1 int=0"
                                + " DECLARE @SortOrder2 int=0"
                                + " SET @SortOrder1=(SELECT ISNULL(SORTORDER,0) FROM Brand_Rss WHERE feed_id=" + ID + ")"
                                + " SET @SortOrder2=(SELECT ISNULL(MAX(SORTORDER),0) FROM Brand_Rss WHERE SORTORDER<@SortOrder1)"
                                + " UPDATE Brand_Rss SET SORTORDER=@SortOrder1 WHERE SORTORDER=@SortOrder2"
                                + " UPDATE Brand_Rss SET SORTORDER=@SortOrder2 WHERE feed_id=" + ID;
                dp.ExecuteNonQuery(strQuery);
                BindRssGrid();
                ClearCache();
            }
        }
        if (e.CommandName.Equals("delete"))
        {
            string strSql = "DELETE FROM Brand_Rss WHERE feed_id=" + e.Item.Cells[0].Text;
            dp.ExecuteNonQuery(strSql);
            BindRssGrid();
            ClearCache();
        }

        if (e.CommandName.Equals("showRss"))
        {
            TextBox txtRssLink = (TextBox)e.Item.FindControl("txtRssLink");
            TextBox txtRssTitle = (TextBox)e.Item.FindControl("txtRssTitle");
            if (txtRssLink != null && !string.IsNullOrWhiteSpace(txtRssLink.Text.Trim()))
            {
                try
                {
                    this.dlstFeed.DataSource = utils.LoadRSS(txtRssLink.Text.Trim(), utils.formatStringForDBSelect(txtRssTitle.Text.Trim()));
                    this.dlstFeed.DataBind();
                    MVRssReader.SetActiveView(VShowRss);
                }
                catch
                {
                    //lblMsg.Text = Convert.ToString(ex.Message);
                    lblMsg.Text = "Feed not available.";
                }
            }
        }
    }

    protected void btnUpdateRssReaderFeatures_Click(object sender, EventArgs e)
    {
        try
        {
            UpdateRSS(drgdRssReader);
            ClearCache();
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
        BindRssGrid();
    }

    private void UpdateRSS(DataGrid dgrdOne)
    {
        foreach (DataGridItem dgrdItem in dgrdOne.Items)
        {
            if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
            {
                int Rss_Id = 0;
                if (!string.IsNullOrWhiteSpace(dgrdItem.Cells[0].Text.Trim()))
                    Rss_Id = Convert.ToInt32(dgrdItem.Cells[0].Text.Trim());
                string strTitle = "";
                string strLink = "";
                string strTag = "";
                TextBox txtRssTitle = (TextBox)dgrdItem.FindControl("txtRssTitle");
                TextBox txtRssLink = (TextBox)dgrdItem.FindControl("txtRssLink");
                TextBox txtTag = (TextBox)dgrdItem.FindControl("txtTag");
                strTitle = txtRssTitle.Text.Trim();
                strLink = txtRssLink.Text.Trim();
                strTag = txtTag.Text.Trim();
                //Call function to update rssfeed
                updateRssFeed(strTitle, strLink, strTag, Rss_Id);
            }
        }
    }

    private void updateRssFeed(string strTitle, string strLink, string strTag, int Rss_Id)
    {
        try
        {
            string[] tags = strTag.Split(',');

            int tagcnt = 0;
            foreach (string str in tags)
            {
                if (str.Trim().Length > 0)
                    tagcnt++;
                else
                    continue;
                if (str.Length > 25)
                {
                    lblMsg.Text = "Tag length should not exceeded more than 25 characters.";
                    return;
                }
            }

            if (strTitle != "" && strLink != "" && Rss_Id!=0)
            {
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "[SP_Brand_Update_RSSFeeds]";
                cmd.AddParameter("@title", utils.formatStringForDBInsert(strTitle));
                cmd.AddParameter("@link", utils.formatStringForDBInsert(strLink));
                cmd.AddParameter("@rssTags", utils.formatStringForDBInsert(strTag));
                cmd.AddParameter("@m_by", Convert.ToInt32(Session["userid"]));
                cmd.AddParameter("@feed_id", Rss_Id);
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = dp.FillDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    if (string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["IsExist"])) && Convert.ToString(dt.Rows[0]["IsExist"]) != "NO")
                    {
                        lblMsg.Text = "RSS feed already exists.";
                        return;
                    }
                }
                this.lblMsg.Text = "Updated successfully.";
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = Convert.ToString(ex.Message);
        }
    }

    protected void drgdRssReader_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        DataGrid dgrd = (DataGrid)sender;
        dgrd.CurrentPageIndex = e.NewPageIndex;
        DataTable dt = new DataTable();
        if (dgrd.ID.Contains("drgdRssReader"))
            dt = getSavedFeeds();
        DataView dv = new DataView(dt);
        dv.Sort = SortField;

        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrd.DataSource = dv;
        dgrd.DataBind();
    }

    protected void drgdRssReader_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataGrid dgrdOne = (DataGrid)sender;
        DataTable dt = new DataTable();
        if (dgrdOne.ID.Contains("drgdRssReader"))
            dt = getSavedFeeds();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }

    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }

    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }

    #region Import/Export
    protected void lnkImportExport_Click(object sender, EventArgs e)
    {
        MVRssReader.SetActiveView(VImportExport);
        this.lblMsg.Text = "<h3 class=\"\">Import/Export</h3>";
    }

    protected void btnImportOpml_Click(object sender, EventArgs e)
    {
        if(!string.IsNullOrWhiteSpace(fileImportOpml.Value.Trim()))
        {
            string strFileExt = fileImportOpml.PostedFile.FileName.Substring(fileImportOpml.PostedFile.FileName.LastIndexOf(".") + 1);
            string strFileName = System.DateTime.UtcNow.ToFileTimeUtc().ToString() + "." + strFileExt;
            string strPath = Server.MapPath("~/Config/" + ReadConfig.getDomainUrl() + "/upload/userfiles/" + strFileName);
            fileImportOpml.PostedFile.SaveAs(strPath);

            FileStream textFile = new FileStream(strPath, FileMode.Open, FileAccess.ReadWrite);
            StreamReader srFile = new StreamReader(textFile);
            string line = null;
            StringBuilder textToWrite = new StringBuilder();
            int i = 0;
            while ((line = srFile.ReadLine()) != null)
            {
                line = line.Replace("&", "&amp;");
                if (i == 0)
                {
                    textToWrite.Append(line);
                }
                else
                {
                    textToWrite.Append("\n" + line);
                }
                i++;
            }
            textToWrite.Append("\n" + "\n");
            textFile.Close();
            FileStream newTxtFile = new FileStream(strPath, FileMode.Create, FileAccess.Write);
            StreamWriter srNewFile = new StreamWriter(newTxtFile);
            srNewFile.Write(textToWrite.ToString());
            srNewFile.Close();
            newTxtFile.Close();

            string[] strextension = strFileName.Split('.');
            if (strextension[1] == "opml")
            {
                DataSet ds = OPMLReader.OPMLToDataSet(strPath);

                if (ds.Tables[0].Rows.Count > 0)
                    btnSubmit.Visible = true;
                else
                    btnSubmit.Visible = false;
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Add("title_text", typeof(string));
                    dt.Columns.Add("tagging", typeof(string));
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["title"]).Trim()))
                                dr["title_text"] = Convert.ToString(dr["title"]).Trim();
                            else
                                dr["title_text"] = Convert.ToString(dr["text"]).Trim();
                        }
                        catch
                        {
                            dr["title_text"] = "";
                        }
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["tag"]).Trim()))
                                dr["tagging"] = Convert.ToString(dr["tag"]).Trim();
                            else
                                dr["tagging"] = "";
                        }
                        catch
                        {
                            dr["tagging"] = "";
                        }
                    }
                    dt.AcceptChanges();
                    dgrdOPML.DataSource = dt;
                    dgrdOPML.DataBind();
                }
                else
                    dgrdOPML.SelectedIndex = -1;
            }
            else
                lblMsg.Text = "please select only opml file.";
        }
        else
            lblMsg.Text = "please browse a file";
    }

    protected void lnkExportOpml_Click(object sender, EventArgs e)
    {
         OPML opmlobj = new OPML();
         Head headobj = new Head();
         Body bodyobj = new Body();
         headobj.Title =Convert.ToString(Session["username"]) + " subscriptions in " + ReadConfig.getDomainUrl() + " Reader";
         headobj.DateCreated = DateTime.Now;
         DataTable dtMyFeeds = new DataTable();
         dtMyFeeds = getSavedFeeds();
         if (dtMyFeeds.Rows.Count > 0)
         {
             foreach (DataRow dr in dtMyFeeds.Rows)
             {
                 string strTitle = utils.formatStringForDBSelect(Convert.ToString(dr["title"]).Trim()).Replace("&", "");
                 string strTag = "";
                 if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["rssTags"]).Trim()))
                     strTag = utils.formatStringForDBSelect(Convert.ToString(dr["rssTags"]).Trim()).Replace("&", "");

                 Outline outlineobj = new Outline();
                 outlineobj.Text = strTitle;
                 outlineobj.XMLUrl = utils.formatStringForDBSelect(Convert.ToString(dr["link"]).Trim()).Replace("&", "&amp;");
                 outlineobj.Title = strTitle;
                 outlineobj.Type = "rss";
                 outlineobj.Description = utils.formatStringForDBSelect(Convert.ToString(dr["description"]).Trim()).Replace("&", "");
                 outlineobj.Tag = strTag;
                 bodyobj.Outlines.Add(outlineobj);
             }
         }
         opmlobj.Head = headobj;
         opmlobj.Body = bodyobj;
         Response.AddHeader("Content-Disposition", "attachment; filename=" + headobj.Title.Replace(" ", "_") + ".opml");
         Response.ContentType = "text/x-opml";
         Response.Charset = "";
         Response.Write(opmlobj.ToString());
         Response.End();
    }

    protected void dgrdOPML_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataView dv = new DataView(getSavedFeeds());
            dv.RowFilter = "link like '%" + e.Item.Cells[2].Text.Trim() + "%'";
            if(dv.Count>0)
                e.Item.BackColor = System.Drawing.Color.Pink;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            int count = 0;
            int prevRSS = 0;
            string IsExist = string.Empty;
            string IsTitleBlank = string.Empty;

            string sqlStr = "SELECT count(*) AS COUNTING FROM Brand_Rss "
                          + "INNER JOIN brand_user ON Brand_Rss.C_By=brand_user.uid "
                          + "WHERE brand_user.status = 1 "
                          + "AND Brand_Rss.C_By='" + Convert.ToString(Session["userid"]) + "'";
            DataTable dtCount = dp.FillDataTable(sqlStr);
            if (dtCount.Rows.Count > 0)
                prevRSS = Convert.ToInt32(dtCount.Rows[0]["COUNTING"]);

            foreach (DataGridItem dgrdItem in dgrdOPML.Items)
            {
                if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkRSS = (CheckBox)dgrdItem.FindControl("chkRSS");
                    if (chkRSS.Checked)
                    {
                        TextBox txt_Title = (TextBox)dgrdItem.FindControl("txt_Title");
                        string strTitle = utils.formatStringForDBInsert(txt_Title.Text.Trim().Replace("&nbsp;", ""));
                        string strLink = utils.formatStringForDBInsert(dgrdItem.Cells[2].Text.Trim());
                        string strTag = utils.formatStringForDBInsert(dgrdItem.Cells[3].Text.Trim());

                        if (strTitle != "")
                        {
                            DBCommand cmd = new DBCommand();
                            cmd.CommandText = "[SP_Brand_Insert_RSSFeeds]";
                            cmd.AddParameter("@title", strTitle);
                            cmd.AddParameter("@link", strLink);
                            cmd.AddParameter("@rssTags", strTag);
                            cmd.AddParameter("@c_by", Convert.ToInt32(Session["userid"]));
                            cmd.CommandType = CommandType.StoredProcedure;
                            DataTable dt = dp.FillDataTable(cmd);
                            if (dt.Rows.Count > 0)
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["IsExist"])) && Convert.ToString(dt.Rows[0]["IsExist"]) == "YES")
                                    IsExist = "Some RSS feed(s) already exists.";
                                else
                                    count++;
                            }
                        }
                        else
                        {
                            IsTitleBlank = "Some RSS feed(s) title is bank.";
                        }

                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(IsExist))
                lblMsg.Text += IsExist;
            if (!string.IsNullOrWhiteSpace(IsTitleBlank))
                lblMsg.Text +="<br/>"+ IsExist;
            if (count > 0)
            {
                lblMsg.Text += "<br/>Submitted successfully.";
                ClearCache();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }
    #endregion

    //Cache Code 
    private void ClearCache()
    {
        string KeySortBlogPosts = "rsssummary-" + ReadConfig.getDomainUrl().ToLower().Trim();
        if (Cache[KeySortBlogPosts] != null)
            Cache.Remove(KeySortBlogPosts);
    }
}