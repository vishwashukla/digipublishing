﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminTask_Manage_SEO : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : SEO";
        lblmessage.Text = "";

        if (!IsPostBack)
            GridBind();
    }

    private void GridBind()
    {
        try
        {
            dgrdSEO.DataSource = GetData();
            dgrdSEO.DataBind();
            dgrdTitle.DataSource = GetTiltle();
            dgrdTitle.DataBind();
            if (GetTiltle().Rows.Count > 0)
                chkMetatag.Checked = Convert.ToBoolean(GetTiltle().Rows[0]["IsMetatag"]);//
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    private DataTable GetData()
    {
        string strQuery = "SELECT *,CASE WHEN name='keywords' OR name='description' THEN 'False' ELSE 'True' END AS Visible "
                        + " FROM Brand_AppSettings WHERE IsDelete='False' AND name<>'title' AND Type=1";
        DataTable dtData = dp.FillDataTable(strQuery);
        return dtData;
    }

    private DataTable GetTiltle()
    {
        string strQuery = "SELECT * FROM Brand_AppSettings WHERE IsDelete='False' AND name='title' AND Type=1";
        DataTable dtData = dp.FillDataTable(strQuery);
        return dtData;
    }

    protected void lbAddNew_Click(object sender, EventArgs e)
    {
        SetBlank();
        btnSave.Text = "Save";
        txtName.Enabled = true;
        mView.SetActiveView(vAdd);
    }

    private void SetBlank()
    {
        txtName.Text = "";
        txtContent.Text = "";
        drpType.SelectedValue = "0";
        drpType.Enabled = true;
        hdnID.Value = "-1";
        txtCountdown.Value = "";
    }

    protected void dgrdTitle_OnEditCommand(object sender, DataGridCommandEventArgs e)
    {
        try
        {
            SetBlank();
            hdnID.Value = e.Item.Cells[0].Text;
            Label lblName=(Label) e.Item.FindControl("lblName");
            if(lblName!=null)
                txtName.Text = lblName.Text;
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            if (lblDescription != null)
                txtContent.Text = lblDescription.Text;
            btnSave.Text = "update";
            txtName.Enabled = false;
            drpType.SelectedValue = "0";
            drpType.Enabled = false;
            mView.SetActiveView(vAdd);
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    protected void dgrdSEO_OnItemCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            try
            {
                SetBlank();
                hdnID.Value = e.Item.Cells[0].Text;
                Label lblName = (Label)e.Item.FindControl("lblName");
                if (lblName != null)
                    txtName.Text = lblName.Text;
                Label lblDescription = (Label)e.Item.FindControl("lblDescription");
                if (lblDescription != null)
                    txtContent.Text = lblDescription.Text;

                Label lblType = (Label)e.Item.FindControl("lblType");
                if (lblType != null)
                {
                    if (lblType.Text.Trim().ToLower() == "meta property")
                        drpType.SelectedValue = "1";
                    else
                        drpType.SelectedValue = "0";
                }

                if (txtName.Text == "keywords")
                {
                    txtContent.MaxLength = 2000;
                    txtContent.Attributes.Add("onKeyDown", "limitText(this," + txtCountdown.ClientID + ",2000);");
                    txtContent.Attributes.Add("onKeyUp", "limitText(this," + txtCountdown.ClientID + ",2000);");
                    lblMaxChar.Text = "(Maximum characters: 2000)";
                    txtCountdown.Value = 2000.ToString();

                }
                else
                {
                    txtContent.MaxLength = 249;
                    txtContent.Attributes.Add("onKeyDown", "limitText(this," + txtCountdown.ClientID + ",249);");
                    txtContent.Attributes.Add("onKeyUp", "limitText(this," + txtCountdown.ClientID + ",249);");
                    lblMaxChar.Text = "(Maximum characters: 249)";
                    txtCountdown.Value = 249.ToString();
                }
                btnSave.Text = "update";
                txtName.Enabled = true;
                mView.SetActiveView(vAdd);
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message.ToString();
            }
        }
        if (e.CommandName == "Delete")
        {
            try
            {
                string strQuery = "UPDATE Brand_AppSettings SET IsDelete='True' WHERE id='" + e.Item.Cells[0].Text + "'";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Deleted successfully.";
                GridBind();
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message.ToString();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "[SP_Brand_InsertUpdate_AppSettings]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.AddParameter("@name", utils.formatStringForDBInsert(txtName.Text));
            cmd.AddParameter("@description", utils.formatStringForDBInsert(txtContent.Text));
            cmd.AddParameter("@MetaType", drpType.SelectedValue.ToString().Trim());
            cmd.AddParameter("@c_by", Convert.ToInt32(Session["userid"]));
            if (hdnID.Value != "-1")
                cmd.AddParameter("@id", Convert.ToInt32(hdnID.Value));
            DataTable dt = dp.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                lblmessage.Text = Convert.ToString(dt.Rows[0][0]);
                if (Convert.ToBoolean(dt.Rows[0][1]))
                {
                    GridBind();
                    SetBlank();
                    mView.SetActiveView(Vgrid);
                }
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetBlank();
        btnSave.Text = "Save";
        mView.SetActiveView(Vgrid);
    }

    protected void chkMetatag_Checked(object sender, EventArgs e)
    {

        string strQuery = "SELECT top 1 id FROM Brand_AppSettings WHERE IsDelete='False' AND name='title' AND Type=1";
        DataTable dtID = dp.FillDataTable(strQuery);
        if (dtID.Rows.Count > 0)
        {
            if (chkMetatag.Checked)
            {
                strQuery = "UPDATE Brand_AppSettings SET IsMetatag='True' WHERE id='" + dtID.Rows[0]["id"].ToString() + "'";
            }
            else
                strQuery = "UPDATE Brand_AppSettings SET IsMetatag='false' WHERE id='" + dtID.Rows[0]["id"].ToString() + "'";
            dp.ExecuteNonQuery(strQuery);
        }
        lblmessage.Text = "Updated successfully.";
    }

    /// <summary>
    /// Sort command event of datagrid for users list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dgrdSEO_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetData();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }


    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }
    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }
    protected void dgrdSEO_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dgrdSEO.CurrentPageIndex = e.NewPageIndex;
        DataTable dt = new DataTable();
        dt = this.GetData();
        DataView dv = new DataView(dt);
        dv.Sort = SortField;

        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdSEO.DataSource = dv;
        dgrdSEO.DataBind();
    }
}