﻿using System;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminTask_Manage_Users : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : Members";
        lblmessage.Text = "";

        if (!IsPostBack)
            BindUsers();
    }

    // <summary>
    /// Method to bind data grid with all users list
    /// </summary>
    private void BindUsers()
    {
        DataTable dt = getUserList();
        dgrdUsers.DataSource = dt;
        dgrdUsers.DataBind();
        mView.SetActiveView(Vgrid);
    }

    /// <summary>
    /// Return data table with all users list
    /// </summary>
    /// <returns></returns>
    private DataTable getUserList()
    {
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "BRAND_SEL_USER_SP";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.AddParameter("@status", "True");
        cmd.AddParameter("@roles", "All");
        DataTable dt = new DataTable();
        try
        {
            dt = dp.FillDataTable(cmd);
            dt.Columns.Add("CountryName", typeof(string));
            dt.Columns.Add("IsShowDelete", typeof(bool));
            foreach (DataRow dr in dt.Rows)
            {
                dr["CountryName"] = utils.LookUpDetails(Convert.ToString(dr["user_ip"]));
                if (Convert.ToString(dr["roles"]) == "super admin" || Convert.ToString(dr["uid"]) == Convert.ToString(Session["userid"]))
                    dr["IsShowDelete"] = false;
                else
                    dr["IsShowDelete"] = true;
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
        return dt;
    }

    protected void btnEmailSearch_Click(object sender, EventArgs e)
    {
        DataView dv = new DataView(getUserList());
        dv.RowFilter = "email like '%" + txtEmailSearch.Text.Trim().Replace("'", "''") + "%'";
        dgrdUsers.DataSource = dv;
        dgrdUsers.DataBind();
        mView.SetActiveView(Vgrid);
    }

    /// <summary>
    /// Method event to add new user
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbAddUser_Click(object sender, EventArgs e)
    {
        SetBlank();
        PopulateControls();
        btnCreate.Text = "Create";
        mView.SetActiveView(VaddNew);
    }


    /// <summary>
    /// Method event to edit/delete a user's information
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dgrdUsers_OnItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName.Equals("Edit"))
            {
                EditUser(e.Item.Cells[0].Text.Trim());
            }
            if (e.CommandName == "Delete")
            {
                string strQuery = "delete from brand_user  WHERE uid='" + e.Item.Cells[0].Text + "';";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "user deleted successfully.";
                BindUsers();
            }
        }
    }

    /// <summary>
    /// Method to edit a user. It provides all data stored in data base for a particular user
    /// </summary>
    /// 
    public void EditUser(string UsrID)
    {
        SetBlank();
        PopulateControls();
        btnCreate.Text = "Update";

        hdnUsrID.Value = UsrID;
        btnCreate.Text = "Update";
        DBCommand cmd = new DBCommand();
        cmd.CommandText = "BRAND_SEL_USER_SP";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@uid", Convert.ToInt32(hdnUsrID.Value));

        try
        {
            DataTable dt = dp.FillDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                pwd.Value = Convert.ToString(dt.Rows[0]["password"]);
                txtEmail.Text = Convert.ToString(dt.Rows[0]["email"]);

                ListItem role = ddlRole.Items.FindByValue(Convert.ToString(dt.Rows[0]["roles"]));
                if (role != null)
                    ddlRole.SelectedValue = Convert.ToString(dt.Rows[0]["roles"]);

                //Show Management Access Permissions
                if (dt.Rows[0]["roles"].ToString() == "admin")
                {
                    trPermissions.Visible = true;
                    string strUserPermissions = Convert.ToString(dt.Rows[0]["UserPermissions"]);
                    if (!string.IsNullOrWhiteSpace(strUserPermissions))
                        utils.SetSelectedCheckBoxItems(ChkbxlstAccessPermiossions, strUserPermissions);
                    else
                    {
                        if (Convert.ToString(dt.Rows[0]["roles"]) == "admin")
                        {
                            ChkbxlstAccessPermiossions.Items.Cast<ListItem>().Where(n => n.Value != "-1").Select(n => n).ToList().ForEach(n => n.Selected = true);
                        }
                    }
                }
                txtname.Text = utils.formatStringForDBSelect(dt.Rows[0]["fullname"].ToString());
                txtTitle.Text = utils.formatStringForDBSelect(dt.Rows[0]["title"].ToString());
                welcome_text.Value = utils.formatStringForDBSelect(dt.Rows[0]["header_text"].ToString());
            }
            mView.SetActiveView(VaddNew);
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        int tagcnt = 0;
        if (!ValidateFields(ref tagcnt))
            return;

        if (txtCPwd.Text.Trim() != "")
        {
            /// Password conversion 
            int saltSize = 2;
            Byte[] saltBytes = new byte[saltSize];
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(txtCPwd.Text.Trim() + this.sault.Value.Trim());
            encodedBytes = md5.ComputeHash(originalBytes);
            //Convert encoded bytes back to a 'readable' string
            pwd.Value = BitConverter.ToString(encodedBytes).Replace("-", "");
        }

        try
        {
            //Save User's Details 
            DBCommand cmd = new DBCommand();
            cmd.CommandText = "[SP_BRAND_UPDATE_USER]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@pwd", pwd.Value);
            cmd.Parameters.Add("@email", txtEmail.Text.Trim());
            cmd.Parameters.Add("@header_text", (utils.formatStringForDBInsert(welcome_text.Value).Replace("\'", "\"")));
            cmd.Parameters.Add("@fullname", utils.formatStringForDBInsert(txtname.Text.Trim()));
            cmd.Parameters.Add("@title", utils.formatStringForDBInsert(txtTitle.Text.Trim()));
            cmd.Parameters.Add("@ALTER_by", Convert.ToString(Session["userid"]));
            if (ddlRole.Visible == true)
                cmd.Parameters.Add("@roles", ddlRole.SelectedValue);

            if (trPermissions.Visible == true)
            {
                string strPermissions = utils.GetSelectedCheckBoxItems(ChkbxlstAccessPermiossions);
                if (!string.IsNullOrWhiteSpace(strPermissions))
                    cmd.Parameters.Add("@Permissions", strPermissions + ",");
            }

            if (!string.IsNullOrWhiteSpace(hdnUsrID.Value))
            {
                cmd.Parameters.Add("@uid", hdnUsrID.Value);
                dp.ExecuteNonQuery(cmd);
            }
            else
            {
                hdnUsrID.Value = Convert.ToString(dp.FillDataTable(cmd).Rows[0][0]);
            }

            lblmessage.Text = "Action performed successfully.";
            BindUsers();
            SetBlank();
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    private bool ValidateFields(ref int tagcnt)
    {
        if (string.IsNullOrWhiteSpace(hdnUsrID.Value) && this.txtPwd.Text.Trim() == "")
        {
            this.lblmessage.Text = "Please fill the password.";
            return false;
        }

        string strQuery = "DECLARE @MSG varchar(100)=''"
                    + " SET @MSG=(SELECT CASE WHEN COUNT(*)>0 THEN 'Email id already exists.' ELSE '' END FROM dbo.brand_user where status=1 AND email='" + txtEmail.Text.Trim() + "'";
        if (!string.IsNullOrWhiteSpace(hdnUsrID.Value))
            strQuery += "  and uid<>" + hdnUsrID.Value;
        strQuery += ")";
        strQuery += " IF @MSG =''"
                    + " BEGIN"
                    + " SET @MSG=(SELECT CASE WHEN COUNT(*)>0 THEN 'Name already exists,please enter another name.' ELSE '' END FROM dbo.brand_user where status=1 AND uname='" + utils.formatStringForDBInsert(txtname.Text.Trim()) + "'";

        if (!string.IsNullOrWhiteSpace(hdnUsrID.Value))
            strQuery += "  and uid<>" + hdnUsrID.Value;
        strQuery += ")";
        strQuery += " END"
                    + " SELECT @MSG as MSG";
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0][0])))
        {
            lblmessage.Text = Convert.ToString(dt.Rows[0][0]);
            return false;
        }
        return true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        mView.SetActiveView(Vgrid);
        SetBlank();
    }

    /// <summary>
    /// Method to reset all text fields
    /// </summary>
    private void SetBlank()
    {
        hdnUsrID.Value = string.Empty;
        txtTitle.Text = "";
        txtname.Text = "";
        txtEmail.Text = "";
        pwd.Value = "";
        ddlRole.SelectedValue = "user";
        trPermissions.Visible = false;
        ChkbxlstAccessPermiossions.ClearSelection();
        welcome_text.Value = "";
        ddlRole.Visible = true;
    }

    /// <summary>
    /// Page index event of datagrid for users list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dgrdUsers_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dgrdUsers.CurrentPageIndex = e.NewPageIndex;
        DataView dv = new DataView(this.getUserList());
        dv.Sort = SortField;

        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        dgrdUsers.DataSource = dv;
        dgrdUsers.DataBind();
    }

    /// <summary>
    /// Sort command event of datagrid for users list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dgrdUsers_SortCommand(object sender, DataGridSortCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = this.getUserList();
        DataView dv = new DataView(dt);
        this.SortField = e.SortExpression;
        dv.Sort = e.SortExpression;
        dv.Sort = SortField;
        if (!Sortascending)
        {
            dv.Sort += " DESC";
        }
        DataGrid dgrdOne = (DataGrid)sender;
        dgrdOne.CurrentPageIndex = 0;
        dgrdOne.DataSource = dv;
        dgrdOne.DataBind();
    }
    /// <summary>
    /// Gets and Sets the sort field.
    /// </summary>
    public string SortField
    {
        get
        {
            object o = ViewState["SortField"];
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }
        set
        {
            if (value == SortField)
            {
                //if ascending change to descending or vice versa.
                Sortascending = !Sortascending;
            }
            else
            {
                Sortascending = true;
            }
            ViewState["SortField"] = value;
        }
    }
    /// <summary>
    /// Gets and Sets the sort type(ascending or descending).
    /// </summary>
    public bool Sortascending
    {
        get
        {
            object o = ViewState["Sortascending"];
            if (o == null)
            {
                return true;
            }
            return (bool)o;
        }
        set
        {
            ViewState["Sortascending"] = value;
        }
    }

    public void ddlRole_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRole.SelectedValue == "admin")
            trPermissions.Visible = true;
        else
            trPermissions.Visible = false;
    }

    private void PopulateControls()
    {
        PopulatePermissions();
        SetFCKPath();
    }

    private void PopulatePermissions()
    {
        ChkbxlstAccessPermiossions.Items.Clear();
        //1:Manage Users ; 2:Manage Team; 3:Manage Regions; 4:Manage Blogs; 5:Manage RSS; 6:Manage BookMarks; 7:Manage User Activity; 
        //8:Manage Front End Component; 9:Manage Events; 10:Manage Email Notification; 12:Manage Forum;
        //14:Manage Component; 15:Admin Advance Search; 16:Manage Industries; 17:Manage Login; 18:Moderation Summary;19:Manage SEO;
        //20:Manage Status; 21:Manage Themes;

        string strQuery = "select features from brand_settings";
        DataTable dtAdminFeatures = dp.FillDataTable(strQuery);

        string[] strArrFeatures = { };
        if (dtAdminFeatures != null && dtAdminFeatures.Rows.Count > 0)
            strArrFeatures = Convert.ToString(dtAdminFeatures.Rows[0][0]).Split(',');

        ListItem li = new ListItem();

        //Blog
        string Blog = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Blog)));
        if (strArrFeatures.Contains(Blog))
        {
            li = new ListItem("Manage Blogs", "4");
            ChkbxlstAccessPermiossions.Items.Add(li);
        }
        //RSS
        string RSS = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.RSS)));
        if (strArrFeatures.Contains(RSS))
        {
            li = new ListItem("Manage RSS", "5");
            ChkbxlstAccessPermiossions.Items.Add(li);
        }
        //BookMark
        string Bookmark = Convert.ToString((int)Enum.Parse(typeof(features), Convert.ToString(features.Bookmark)));
        if (strArrFeatures.Contains(Bookmark))
        {
            li = new ListItem("Manage BookMarks", "6");
            ChkbxlstAccessPermiossions.Items.Add(li);
        }

        string[,] strArrTaskComp = { { "8", "Manage Default Landing Page" }, { "14", "Manage Component" }, { "19", "Manage SEO" } };
        for (int i = 0; i < strArrTaskComp.GetLength(0); i++)
        {
            li = new ListItem(strArrTaskComp[i, 1], strArrTaskComp[i, 0]);
            ChkbxlstAccessPermiossions.Items.Add(li);
        }
    }

    private void SetFCKPath()
    {
        string strPath = Request.ApplicationPath;
        if (strPath == "/")
            welcome_text.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
        else
            welcome_text.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
    }

    protected void welcome_text_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedField(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this,
                                                    welcome_text.GetType(),
                                                    "FCKUpdater",
                                                    script,
                                                    true);
            ScriptManager.RegisterOnSubmitStatement(this,
                                                    welcome_text.GetType(),
                                                    "WebContentManagerEditorScript_" + this.welcome_text.ClientID,
                                                    "FCKUpdateLinkedField('" + this.welcome_text.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(welcome_text.GetType(),
                                                        "FCKUpdater",
                                                        script,
                                                        true);
            this.Page.ClientScript.RegisterOnSubmitStatement(welcome_text.GetType(),
                                                             "WebContentManagerEditorScript_" + this.welcome_text.ClientID,
                                                             "FCKUpdateLinkedField('" + this.welcome_text.ClientID + "');");
        }
    }
    protected void welcome_text_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.welcome_text.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, welcome_text.GetType(), "FCKSource", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, welcome_text.GetType(), "WebContentManagerEditorSourceScript_" + this.welcome_text.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(welcome_text.GetType(), "FCKSource", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(welcome_text.GetType(), "WebContentManagerEditorSourceScript_" + this.welcome_text.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
    }
}