﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_Users.aspx.cs" Inherits="AdminTask_Manage_Users" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <script src="../JS/Password.js"></script>
    <h3 class="HeaderClass">Manage Users</h3>
    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label><br />
    <asp:MultiView ID="mView" runat="server">
        <asp:View ID="Vgrid" runat="server">
            <div class="content-full">
                <asp:TextBox ID="txtEmailSearch" runat="server" />
                <asp:Button ID="btnEmailSearch" runat="server" CssClass="btnBgClass" Text="Search by Email" OnClick="btnEmailSearch_Click" />
                <div class="AdminRightMenu">
                    <asp:LinkButton ID="lbAddUser" runat="server" CssClass="toplink" OnClick="lbAddUser_Click" Text="Add New User">
                    </asp:LinkButton>
                </div>
                <asp:DataGrid ID="dgrdUsers" runat="server" PageSize="20" GridLines="None" AllowPaging="True" PagerStyle-Mode="NumericPages" AutoGenerateColumns="False"
                    AllowSorting="True" OnSortCommand="dgrdUsers_SortCommand" OnPageIndexChanged="dgrdUsers_PageIndexChanged" OnItemCommand="dgrdUsers_OnItemCommand" Width="100%">
                    <Columns>
                        <asp:BoundColumn DataField="uid" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="username" HeaderText="Name" SortExpression="username"></asp:BoundColumn>
                        <asp:BoundColumn DataField="roles" HeaderText="Role" SortExpression="roles"></asp:BoundColumn>
                        <asp:BoundColumn DataField="email" HeaderText="Email"></asp:BoundColumn>
                        <asp:BoundColumn DataField="c_date" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Registration Date"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Last Login IP">
                            <ItemTemplate>
                                <asp:Label ID="lbluser_ip" runat="server" Text='<%#Eval("user_ip")%>'></asp:Label><br />
                                <asp:Label ID="lblGeouser" runat="server" Text='<%#Eval("CountryName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:BoundColumn DataField="CreatedBy" HeaderText="Created By"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ModifiedBy" HeaderText="Modified By"></asp:BoundColumn>--%>
                        <asp:ButtonColumn CommandName="Edit" Text="Edit"></asp:ButtonColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDel" runat="server" Text="Delete" CommandName="Delete" CssClass="toplink" OnClientClick="return confirm('Are you sure you wish to delete?')"
                                    CausesValidation="false" Visible='<%#Convert.ToBoolean(Eval("IsShowDelete")) %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a target="_self" href="agencyLogin.aspx?id=<%#Eval("email")%>" class="toplink">Login</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </asp:View>
        <asp:View ID="VaddNew" runat="server">
            <div class="content-full">
                <table class="table-full">
                    <tr>
                        <td>Title</td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td>
                            <asp:DropDownList ID="ddlRole" runat="server" CssClass="select" OnSelectedIndexChanged="ddlRole_OnSelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="admin">admin</asp:ListItem>
                                <asp:ListItem Value="user">user</asp:ListItem>
                                <asp:ListItem Value="subscribe">subscribe</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Name*</td>
                        <td>
                            <asp:TextBox ID="txtname" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true" CssClass="error"
                                ControlToValidate="txtname" ErrorMessage="Please enter name" ValidationGroup="MngUsrVCG"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Email*</td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>&nbsp;                               
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" SetFocusOnError="true" CssClass="error"
                                ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="invalid e-mail address! please re-enter"
                                ValidationGroup="MngUsrVCG" ValidationExpression="^[a-zA-Z]?[\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr id="trPermissions" runat="server" visible="false">
                        <td>Management Access Permissions</td>
                        <td>
                            <asp:CheckBoxList ID="ChkbxlstAccessPermiossions" CssClass="select" runat="server">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="fckeditor">
                                <FCKeditorV2:FCKeditor OnPreRender="welcome_text_PreRender" ID="welcome_text" runat="server" OnLoad="welcome_text_Load">
                                </FCKeditorV2:FCKeditor>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <%--Password Section--%>
            <fieldset style="text-align: left;">
                <legend><span style="color: Black">Create password</span></legend>
                <table class="table-full">
                    <tr>
                        <td>Password*                                                   
                            <input id="sault" type="hidden" name="sault" runat="server" />
                        </td>
                        <td>
                            <script>function rfvCPWD() { return document.getElementById('<%=rfvCPWD.ClientID %>'); }</script>
                            <asp:TextBox ID="txtPwd" onkeypress="javascript:return alphanumericPwd();" onkeyup="javascript:passwordStrengthCheck(this.id,rfvCPWD());"
                                runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>&nbsp;
                                                   
                            <br />
                            <div style="width: 100%; color: red" id="dvPwd">
                            </div>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" SetFocusOnError="true" CssClass="error"
                                ControlToValidate="txtPwd" Display="Dynamic" ErrorMessage="A - Z with at least 1 digit, max 15 & min 8 characters"
                                ValidationGroup="MngUsrVCG" ValidationExpression="^.*(?=.{8,})(?=.*\d)(?=.*[a-z]).*$"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width:500px;">
                            <span id="Words"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Confirm&nbsp;password*</td>
                        <td>
                            <asp:TextBox ID="txtCPwd" runat="server" TextMode="Password"></asp:TextBox>&nbsp;
                                        <br />
                            <asp:RequiredFieldValidator ID="rfvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" Enabled="false"
                                Display="Dynamic" ErrorMessage="please enter confirm password" ValidationGroup="MngUsrVCG" CssClass="error"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvCPWD" runat="server" SetFocusOnError="true" ControlToValidate="txtCPwd" CssClass="error"
                                Display="Dynamic" ErrorMessage="password & confirm password must match" ValidationGroup="MngUsrVCG" ControlToCompare="txtPwd"></asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <%--End : Password Section--%>
            <asp:Button ID="btnCreate" OnClick="btnCreate_Click" runat="server" Text="Create" CssClass="btnBgClass" ValidationGroup="MngUsrVCG"></asp:Button>
            <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CssClass="btnBgClass"></asp:Button>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="hdnUsrID" runat="server" />
    <asp:HiddenField ID="pwd" runat="server"></asp:HiddenField>
</asp:Content>