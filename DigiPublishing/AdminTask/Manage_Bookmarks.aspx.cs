﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

public partial class AdminTask_Manage_Bookmarks : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    public static string strGetCompleteFilePath = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : Bookmarks";
        lblmessage.Text = "";

        if (!IsPostBack)
        {
            BindBookmarks();
            bindChkBxRandomiseStatus();
            bindChkBxRandomiseLinksStatus();
        }
    }

    private void bindChkBxRandomiseStatus()
    {
        string strQuery = "Select  ISNULL(RandomiseLinksInSummary,0) as RandomiseLinksInSummary,ISNULL(NumberOfLinksInSummary,0) as NumberOfLinksInSummary from brand_settings";
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(strQuery);
        chbxRandomiseLinkOrderinSummary.Checked = Convert.ToBoolean(dt.Rows[0]["RandomiseLinksInSummary"]);
        txtNumberOfLinksInSummary.Text = Convert.ToString(dt.Rows[0]["NumberOfLinksInSummary"]);
    }
    private void bindChkBxRandomiseLinksStatus()
    {
        string strQuery = "Select  ISNULL(RandomiseLinksInLink,0) as RandomiseLinksInLink,ISNULL(NumberOfLinksinlink,0) as NumberOfLinksinlink from brand_settings";
        DataTable dt = new DataTable();
        dt = dp.FillDataTable(strQuery);
        chbxRandomiseLinkOrderinLinks.Checked = Convert.ToBoolean(dt.Rows[0]["RandomiseLinksInLink"]);
        txtNumberOfLinksInLinks.Text = Convert.ToString(dt.Rows[0]["NumberOfLinksinlink"]);
    }

    public void BindBookmarks()
    {
        try
        {
            DataTable dtBookmark = new DataTable();
            DBCommand cmdBookmark = new DBCommand();
            cmdBookmark.CommandType = CommandType.StoredProcedure;
            cmdBookmark.CommandText = "brand_SelectBookmark";
            dtBookmark = dp.FillDataTable(cmdBookmark);
            if (dtBookmark.Rows.Count > 0)
            {
                lblNoSavedbookmark.Text = "";
                int totalblogs = dtBookmark.Rows.Count;
                int pagesize = grdBookmark.PageSize;
                int currentpage = grdBookmark.CurrentPageIndex;
                int totalpages = (totalblogs / pagesize) + ((totalblogs % pagesize) == 0 ? 0 : 1);
                if (totalpages > 0 && (totalpages - 1) < currentpage)
                {
                    grdBookmark.CurrentPageIndex = totalpages - 1;
                }
                grdBookmark.DataSource = dtBookmark;
                grdBookmark.DataBind();
            }
            else
            {
                grdBookmark.DataSource = dtBookmark;
                grdBookmark.DataBind();
                lblNoSavedbookmark.Text = "no bookmark saved..";
            }
            mView.SetActiveView(vGrid);
        }
        catch (Exception ex)
        {
            lblmessage.Text = Convert.ToString(ex.Message);
        }
    }
  
    protected void btnAddBookMark_Click(object sender, EventArgs e)
    {
        resetControls();
        btnCreate.Text = "Add";
        lblAddBookMark.Text = "Add bookmark";
        mView.SetActiveView(vAddBookMark);
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string url = utils.formatStringForDBInsert(txtURL.Text.Trim());
            txtTags.Text = txtTags.Text.Trim().Replace(";", ",");
            string[] tags = txtTags.Text.Split(',');
            int tagcnt = 0;
            foreach (string str in tags)
            {
                if (str.Trim().Length > 0)
                    tagcnt++;
                else
                    continue;
                if (str.Length > 25)
                {
                    lblmessage.Text = "Tag length should not exceeded more than 25 characters.";
                    return;
                }
            }
            
            if (txttitle.Text.Trim() != "" && txtURL.Text.Trim() != "")
            {
                int nofollow = chknofollowall.Checked ? 1 : 0;
                int noindex = chknoindexall.Checked ? 1 : 0;
                int noopener = chknoopenerall.Checked ? 1 : 0;
                int noreferrer = chknoreferreradd.Checked ? 1 : 0;//RK
                DBCommand cmd = new DBCommand();
                cmd.CommandText = "[SP_Brand_InsertUpdate_BookMark]";
                cmd.AddParameter("@BK_Title", utils.formatStringForDBInsert(txttitle.Text.Trim()));
                cmd.AddParameter("@BK_URL", url.StartsWith("http://") || url.StartsWith("https://") ? url : "http://" + url);
                cmd.AddParameter("@BK_Tags", utils.formatStringForDBInsert(txtTags.Text));
                cmd.AddParameter("@BK_Desc", utils.formatStringForDBInsert(txtDesc.Text.Trim()));
                cmd.AddParameter("@c_by", Convert.ToInt32(Session["userid"]));
                cmd.AddParameter("@nofollow", nofollow);
                cmd.AddParameter("@noindex", noindex);
                cmd.AddParameter("@noopener", noopener);
                cmd.AddParameter("@noreferrer", noreferrer);//RK

                if (hdBookMarkID.Value!="-1")
                    cmd.AddParameter("@ID", hdBookMarkID.Value);
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = dp.FillDataTable(cmd);
                if(dt!=null && dt.Rows.Count>0)
                {                    
                    lblmessage.Text = Convert.ToString(dt.Rows[0]["MSG"]);

                    BindBookmarks();
                    resetControls();
                    ClearCache();
                }
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = Convert.ToString(ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        BindBookmarks();
        resetControls();
    }

    protected void grdBookmark_ItemCommand(object source, DataGridCommandEventArgs e)
    {
      
        if (e.CommandName.Equals("Up"))
        {
            if (e.Item.Cells[0].Text.Trim() != "")
            {
                int ID = Convert.ToInt32(e.Item.Cells[0].Text.Trim());

                string strQuery = "DECLARE @SortOrder1 int=0"
                                + " DECLARE @SortOrder2 int=0"
                                + " SET @SortOrder1=(SELECT ISNULL(BK_SORTORDER,0) FROM Brand_Bookmark  WHERE Id=" + ID + ")"
                                + " SET @SortOrder2=(SELECT ISNULL(MIN(BK_SORTORDER),0) FROM Brand_Bookmark WHERE BK_SORTORDER>@SortOrder1)"
                                + " UPDATE Brand_Bookmark  SET BK_SORTORDER=@SortOrder1 WHERE BK_SORTORDER=@SortOrder2"
                                + " UPDATE Brand_Bookmark  SET BK_SORTORDER=@SortOrder2 WHERE Id=" + ID;
                dp.ExecuteNonQuery(strQuery);
                BindBookmarks();
                ClearCache();
            }
        }
        if (e.CommandName.Equals("Down"))
        {
            if (e.Item.Cells[0].Text.Trim() != "")
            {
                int ID = Convert.ToInt32(e.Item.Cells[0].Text.Trim());

                string strQuery = "DECLARE @SortOrder1 int=0"
                                + " DECLARE @SortOrder2 int=0"
                                + " SET @SortOrder1=(SELECT ISNULL(BK_SORTORDER,0) FROM Brand_Bookmark WHERE Id=" + ID + ")"
                                + " SET @SortOrder2=(SELECT ISNULL(MAX(BK_SORTORDER),0) FROM Brand_Bookmark  WHERE BK_SORTORDER<@SortOrder1)"
                                + " UPDATE Brand_Bookmark SET BK_SORTORDER=@SortOrder1 WHERE BK_SORTORDER=@SortOrder2"
                                + " UPDATE Brand_Bookmark SET BK_SORTORDER=@SortOrder2 WHERE Id=" + ID;
                dp.ExecuteNonQuery(strQuery);
                BindBookmarks();
                ClearCache();
            }
        }
       
        if (e.CommandName.Equals("deleteBookmark"))
        {
            string strSql = string.Empty;
            strSql = "Update brand_bookmark SET Status='false' where id=" + e.Item.Cells[0].Text.Trim();
            dp.ExecuteNonQuery(strSql);
            BindBookmarks();
            ClearCache();
        }
    }

    private void resetControls()
    {
        txttitle.Text = "";
        txtURL.Text = "";
        txtTags.Text = "";
        txtDesc.Text = "";
        hdBookMarkID.Value = "-1";
    }

    protected void btnBookMarks_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (DataGridItem dgrdItem in grdBookmark.Items)
            {
                if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
                {
                    string ID=string.Empty;
                    string strBkmarkTitle = "";
                    string strBkmarkURL = "";
                    string strDescription = "";
                    string strnofollow = "0";
                    string strnoindex = "0";
                    string strnoopener = "0";
                    string strnoreferrer = "0";//RK

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dgrdItem.Cells[0].Text)))
                        ID = Convert.ToString(dgrdItem.Cells[0].Text);

                    TextBox txtEditBookmark = (TextBox)dgrdItem.FindControl("txtEditBookmark");
                    TextBox txtEditURL = (TextBox)dgrdItem.FindControl("txtEditURL");
                    TextBox txtEditDescription = (TextBox)dgrdItem.FindControl("txtEditDescription");
                    CheckBox chknofollow = (CheckBox)dgrdItem.FindControl("chknofollow");
                    CheckBox chknoindex = (CheckBox)dgrdItem.FindControl("chknoindex");
                    CheckBox chknoopener = (CheckBox)dgrdItem.FindControl("chknoopener");
                    CheckBox chknoreferrer = (CheckBox)dgrdItem.FindControl("chknoreferrer");//RK

                    CheckBox chkStatusClick = (CheckBox)dgrdItem.FindControl("chkStatusClick");
                    TextBox txtValueClick = (TextBox)dgrdItem.FindControl("txtValueClick");
                    CheckBox chkStatusClickIP = (CheckBox)dgrdItem.FindControl("chkStatusClickIP");
                    TextBox txtValueClickIP = (TextBox)dgrdItem.FindControl("txtValueClickIP");

                    if (txtEditBookmark != null && txtEditURL != null && txtEditBookmark.Text.Trim() != "" && txtEditURL.Text.Trim() != "")
                    {
                        if (txtEditBookmark != null)
                        {
                            strBkmarkTitle = utils.formatStringForDBInsert(txtEditBookmark.Text.Trim());
                        }
                        if (txtEditURL != null)
                        {
                            strBkmarkURL = utils.formatStringForDBInsert(txtEditURL.Text.Trim());
                        }
                        if (txtEditDescription != null)
                        {
                            strDescription = utils.formatStringForDBInsert(txtEditDescription.Text.Trim());
                        }
                        if (chknofollow != null)
                        {
                            strnofollow = chknofollow.Checked?"1":"0";
                        }
                        if (chknoindex != null)
                        {
                            strnoindex = chknoindex.Checked ? "1" : "0";
                        }
                        if (chknoopener != null)
                        {
                            strnoopener = chknoopener.Checked ? "1" : "0";
                        }
                        if(chknoreferrer !=null)//RK
                        {
                            strnoreferrer = chknoreferrer.Checked ? "1" : "0";
                        }

                        DBCommand cmd = new DBCommand();
                        cmd.CommandText = "[SP_Brand_InsertUpdate_BookMark]";
                        cmd.AddParameter("@BK_Title", strBkmarkTitle);
                        cmd.AddParameter("@BK_URL", strBkmarkURL.StartsWith("http://") || strBkmarkURL.StartsWith("https://") ? strBkmarkURL : "http://" + strBkmarkURL);
                        cmd.AddParameter("@BK_Desc", strDescription);
                        cmd.AddParameter("@c_by", Convert.ToInt32(Session["userid"]));
                        cmd.AddParameter("@ID", ID);
                        cmd.AddParameter("@nofollow", strnofollow);
                        cmd.AddParameter("@noindex", strnoindex);
                        cmd.AddParameter("@noopener", strnoopener);
                        cmd.AddParameter("@noreferrer", strnoreferrer);//RK
                        cmd.AddParameter("@Blocker", Convert.ToString(Convert.ToInt32(chkStatusClick.Checked)));
                        cmd.AddParameter("@BlockerValue", (txtValueClick.Text.ToString().Trim()==""?"0": txtValueClick.Text.ToString().Trim()));
                        cmd.AddParameter("@SameIPStatus", Convert.ToString(Convert.ToInt32(chkStatusClickIP.Checked)));
                        cmd.AddParameter("@SameIPValue", (txtValueClickIP.Text.ToString().Trim() == "" ? "0" : txtValueClickIP.Text.ToString().Trim()));
                        cmd.AddParameter("@IsGridEdit", 1);

                        cmd.CommandType = CommandType.StoredProcedure;
                        DataTable dt = dp.FillDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            lblmessage.Text = Convert.ToString(dt.Rows[0]["MSG"]);
                        }
                    }
                }
            }
            ClearCache();
        }
        catch (Exception ex)
        {
            lblmessage.Text = Convert.ToString(ex.Message);
        }
    }

    protected void lnkbtnImpExp_Click(object sender, EventArgs e)
    {
       mView.SetActiveView(VImportExport);
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        if (fileImport.Value != null && fileImport.Value != "")
        {
            string strFileExt = fileImport.PostedFile.FileName.Substring(fileImport.PostedFile.FileName.LastIndexOf(".") + 1);
            string strFileName = System.DateTime.UtcNow.ToFileTimeUtc().ToString() + "." + strFileExt;
            string strFilePath = Server.MapPath("~/Config/" + ReadConfig.getDomainUrl() + "/upload/userfiles/" + strFileName);
            fileImport.PostedFile.SaveAs(strFilePath);

            strGetCompleteFilePath = getDatafromtxtFile2(strFilePath);

            DataSet ds = new DataSet();
            ds.ReadXml(strGetCompleteFilePath);

            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                Session["bookmarkimportall"] = dt;
                if(!dt.Columns.Contains("bookmark_desc"))
                {
                    dt.Columns.Add("bookmark_desc");
                }
                this.dgrdImpBookmark.DataSource = dt;
                this.dgrdImpBookmark.DataBind();
                this.dgrdImpBookmark.Visible = true;
                this.btnImpbkmarkSave.Visible = true;
                this.btnImpbkmarkSaveAll.Visible = true;
            }
        }
    }

    private string getDatafromtxtFile2(string strFileSavedPath)
    {
        string[] strArrFieldvalues;
        string[] strArrHREF;
        DataTable myTable = new DataTable();
        string[] alllines = File.ReadAllLines(strFileSavedPath);

        string strFileName = System.DateTime.UtcNow.ToFileTimeUtc().ToString() + ".xml";
        string strFilePath = Server.MapPath("~/Config/" + ReadConfig.getDomainUrl() + "/upload/userfiles/" + strFileName);
        Stream streamfile = File.Create(strFilePath);
        StreamWriter sw = new StreamWriter(streamfile);
        sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");

        sw.WriteLine("<body>");
        for(int j=0;j< alllines.Length;j++)
        {
            string line = alllines[j];
            string strHREF = "";
            string strTitle = "";
            string strDesc = "";
            bool isBookmark = false;
            strArrFieldvalues = line.Split(' ');

            for (int i = 0; i < strArrFieldvalues.Length; i++)
            {
                if (strArrFieldvalues[i].ToUpper().Contains("HREF"))
                {
                    isBookmark = true;

                    int hrefstartindex = line.IndexOf('=');
                    strHREF = line.Substring(hrefstartindex+2);
                    int hrefendindex = strHREF.IndexOf('"');
                    strHREF = strHREF.Substring(0, hrefendindex);


                    //strHREF = strArrFieldvalues[i];
                    //strArrHREF = strHREF.Split('=').ToArray().Skip(1).ToArray();
                    //strHREF = String.Join("=", strArrHREF);
                    //strHREF = strHREF.Substring(1, strHREF.Length - 1);
                    //strHREF = strHREF.Substring(0, strHREF.Length - 1);


                    if (line.Contains("</A>"))
                    {
                        int intstart = line.LastIndexOf('"') + 2;
                        int intend = line.LastIndexOf('<');

                        strTitle = line.Substring(intstart, (intend - intstart));
                    }
                }
            }
            if (strHREF != "")
            {
                sw.WriteLine("<bookmark>");
                sw.WriteLine("\t<bookmark_url>" + strHREF.Replace("&", "&amp;") + "</bookmark_url>");
            }
            if (isBookmark)
            {
                j++;
                string nextline = alllines[j];
                if (nextline.StartsWith("<DD>"))
                {
                    strDesc = nextline.Replace("<DD>", "");
                }
                else
                {
                    j--;
                }
                if (strDesc != "")
                {
                    sw.WriteLine("\t<bookmark_desc>" + strDesc.Replace("&", "&amp;") + "</bookmark_desc>");
                }
            }
            if (strTitle != "")
            {
                sw.WriteLine("\t<bookmark_title>" + strTitle.Replace("&rsquo;", "'").Replace("&", "and") + "</bookmark_title>");
                sw.WriteLine("</bookmark>");
            }
        }
        sw.WriteLine("</body>");
        sw.Close();
        streamfile.Close();
        this.btnImpbkmarkSave.Visible = true;
        this.btnImpbkmarkSaveAll.Visible = true;
        return strFilePath;
    }

    protected void btnImpbkmarkSave_Click(object sender, EventArgs e)
    {
        string strQuery="";
        try
        {
            foreach (DataGridItem dgrdItem in dgrdImpBookmark.Items)
            {
                if (dgrdItem.ItemType == ListItemType.Item || dgrdItem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkBookmark = (CheckBox)dgrdItem.FindControl("chkBookmark");

                    if (chkBookmark != null && chkBookmark.Checked)
                    {
                        strQuery += "insert into dbo.brand_bookmark(BK_Title, BK_URL,BK_Desc, c_by, BK_SortOrder, BK_Status) values"
                                   + " ('" + utils.formatStringForDBInsert(dgrdItem.Cells[1].Text.Trim()) + "','" + utils.formatStringForDBInsert(dgrdItem.Cells[2].Text.Trim()) 
                                   + "','" + utils.formatStringForDBInsert(dgrdItem.Cells[3].Text.Trim()) + "'," + Convert.ToString(Session["userid"])
                                   + ",(SELECT ISNULL(MAX(BK_SortOrder),0)+1 FROM brand_bookmark),1);";

                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(strQuery))
            {
                dp.ExecuteNonQuery(strQuery);
                BindBookmarks();
                ClearCache();
            }
        }
        catch(Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }
    protected void lnkExport_Click(object sender, EventArgs e)
    {
        StringBuilder str = new StringBuilder();
        DataTable dtBookMarks = new DataTable();
        DBCommand cmd = new DBCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "brand_SelectBookmark";
        dtBookMarks = dp.FillDataTable(cmd);

        if (dtBookMarks.Rows.Count > 0)
        {
            str.Append("<!DOCTYPE NETSCAPE-Bookmark-file-1>");
            str.AppendLine();
            str.Append("<TITLE>Bookmarks</TITLE>");
            str.AppendLine();
            str.Append("<H1>Bookmarks</H1>");
            str.AppendLine();
            str.Append("<DL><p>");
            str.AppendLine();
            str.Append("<DT><H3 FOLDED ADD_DATE=\"" + DateTime.Now.ToShortDateString() + "\">Default</H3>");
            str.AppendLine();
            str.Append("<DL><p>");
            str.AppendLine();
            foreach (DataRow dr in dtBookMarks.Rows) 
            {
                if (Convert.ToString(dr["BK_Title"]) != "" && Convert.ToString(dr["BK_URL"]) != "")
                {
                    str.Append("<DT><A HREF=\"" + Convert.ToString(dr["BK_URL"]) + "\" ADD_DATE=\"" + DateTime.Now.ToShortDateString() + "\" BOOKMARK_TITLE=\"" + Convert.ToString(dr["BK_Title"]) + "\">" + Convert.ToString(dr["BK_Title"]) + "</A>");
                    str.AppendLine();
                    str.Append("<DD>"+ Convert.ToString(dr["BK_Desc"]));
                    str.AppendLine();
                }
            }
            str.Append("</DL><p>");
        }
        Response.Clear();
        Response.AddHeader("content-disposition","attachment;filename=BookMark.html");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.html";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite =
        new HtmlTextWriter(stringWrite);

        Response.Write(str.ToString());
        Response.End();
    }

    protected void grdBookmark_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdBookmark.CurrentPageIndex = e.NewPageIndex;
        BindBookmarks();
    }

    protected void dgrdImpBookmark_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dgrdImpBookmark.CurrentPageIndex = e.NewPageIndex;
        DataSet ds = new DataSet();
        ds.ReadXml(strGetCompleteFilePath);
        DataTable dt = ds.Tables[0];
        this.dgrdImpBookmark.DataSource = ds;
        this.dgrdImpBookmark.DataBind();
    }

    //Cache Code 
    private void ClearCache()
    {
        string KeySortBlogPosts = "bookmarksummary-" + ReadConfig.getDomainUrl();
        if (Cache[KeySortBlogPosts] != null)
            Cache.Remove(KeySortBlogPosts);
    }

    protected void chbxRandomiseLinkOrderinSummary_CheckedChanged(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        dp.ExecuteNonQuery("update brand_settings set RandomiseLinksInSummary= " + Convert.ToInt32(chbxRandomiseLinkOrderinSummary.Checked) + ";");
    }
    protected void chbxRandomiseLinkOrderinLinks_CheckedChanged(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        dp.ExecuteNonQuery("update brand_settings set RandomiseLinksInLink= " + Convert.ToInt32(chbxRandomiseLinkOrderinLinks.Checked) + ";");
    }
    protected void SaveSummaryDetail_Click(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        dp.ExecuteNonQuery("update brand_settings set RandomiseLinksInSummary=" + Convert.ToInt32(chbxRandomiseLinkOrderinSummary.Checked) 
            + ",NumberOfLinksInSummary=" + Convert.ToInt32((txtNumberOfLinksInSummary.Text=="" ? "0" : txtNumberOfLinksInSummary.Text)) + "; ");
    }
    protected void btnLinksDetail_Click(object sender, EventArgs e)
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
            dp.ExecuteNonQuery("update brand_settings set RandomiseLinksInLink=" + Convert.ToInt32(chbxRandomiseLinkOrderinLinks.Checked)
               + ",NumberOfLinksinlink=" + Convert.ToInt32((txtNumberOfLinksInLinks.Text == "" ? "0" : txtNumberOfLinksInLinks.Text)) + "; ");
    }

    protected void btnImpbkmarkSaveAll_Click(object sender, EventArgs e)
    {
        string strQuery = "";
        DataTable dt = (DataTable)Session["bookmarkimportall"];
        try
        {
            foreach (DataRow dr in dt.Rows)
            {
                strQuery += "insert into dbo.brand_bookmark(BK_Title, BK_URL,BK_Desc, c_by, BK_SortOrder, BK_Status) values"
                                + " ('" + utils.formatStringForDBInsert(dr["bookmark_title"].ToString().Trim()) + "','" + utils.formatStringForDBInsert(dr["bookmark_url"].ToString().Trim())
                                + "','" + utils.formatStringForDBInsert(dr["bookmark_desc"].ToString().Trim()) + "'," + Convert.ToString(Session["userid"])
                                + ",(SELECT ISNULL(MAX(BK_SortOrder),0)+1 FROM brand_bookmark),1);";
            }
            if (!string.IsNullOrWhiteSpace(strQuery))
            {
                dp.ExecuteNonQuery(strQuery);
                BindBookmarks();
                ClearCache();
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void lnkdeleteAll_Click(object sender, EventArgs e)
    {
        string strQuery = "";
        try
        {
            strQuery = "truncate table dbo.brand_bookmark;";
            if (!string.IsNullOrWhiteSpace(strQuery))
            {
                dp.ExecuteNonQuery(strQuery);
                BindBookmarks();
                ClearCache();
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }

    protected void btnSaveForAll_Click(object sender, EventArgs e)
    {
        string strQuery = "";
        try
        {
            int nofollow = chknofollowall.Checked ? 1 : 0;
            int noindex = chknoindexall.Checked ? 1 : 0;
            int noopener = chknoopenerall.Checked ? 1 : 0;
            int noreferrer = chknoreferrerall.Checked ? 1 : 0;//RK
            int click = chkStatusClickAll.Checked ? 1 : 0;
            int clickValue = Convert.ToInt32(txtValueClickAll.Text.Trim());
            int clickIP = chkStatusClickIPAll.Checked ? 1 : 0;
            int clickIPValue = Convert.ToInt32(txtValueClickIPAll.Text.Trim());
            strQuery = "update dbo.brand_bookmark set nofollow=" + nofollow + ",noindex=" + noindex + ",noopener=" + noopener + ",noreferrer="+ noreferrer + ",Blocker=" + click + ",BlockerValue=" + clickValue + ",SameIP_Status=" + clickIP + ",SameIP_Value=" + clickIPValue + ";";//RK
            if (!string.IsNullOrWhiteSpace(strQuery))
            {
                dp.ExecuteNonQuery(strQuery);
                BindBookmarks();
                ClearCache();
                chknofollowall.Checked = false;
                chknoindexall.Checked = false;
                chknoopenerall.Checked = false;
                chknoreferrerall.Checked = false;//RK
                chkStatusClickAll.Checked = false;
                txtValueClickAll.Text = "0";
                chkStatusClickIPAll.Checked = false;
                txtValueClickIPAll.Text = "0";
                lblmessage.Text = "Settings successfully updated.";
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message;
        }
    }
  
}