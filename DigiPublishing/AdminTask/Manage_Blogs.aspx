﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_Blogs.aspx.cs" Inherits="AdminTask_Manage_Blogs" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="container-full">
    <h3 class="HeaderClass">Manage Blogs</h3>
    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label><br />
    
    <h3><asp:Label ID="lblBlogtitle" runat="server"></asp:Label></h3>
    <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
        <asp:View ID="Vgrid" runat="server">
            <asp:CheckBox ID="chbxRandomiseBlogOrder" Text="Randomise blog order" TextAlign="Left" runat="server" AutoPostBack="true" OnCheckedChanged="chbxRandomiseBlogOrder_CheckedChanged" />
            <div class="AdminRightMenu">
                <p>
                    <asp:LinkButton ID="lnkbtnImpExp" runat="server" Text="Import/Export" OnClick="lnkbtnImpExp_Click" />
                </p>
            </div>
            <div class="AdminRightMenu">
                <asp:LinkButton ID="lnkbtnAddPost" OnClick="lnkbtnAddPost_OnClick" runat="server" Text="[Add New Post]"></asp:LinkButton>
            </div>
            <asp:DataGrid ID="dgrdBlogPosts" HorizontalAlign="Right" runat="server" GridLines="None"
                AutoGenerateColumns="false" PageSize="20" AllowPaging="True" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrdBlogPosts_PageIndexChanged"
                OnItemCommand="dgrdBlogPosts_ItemCommand" AllowSorting="true" OnSortCommand="dgrdBlogPosts_SortCommand">
                <Columns>
                    <asp:BoundColumn DataField="posts_blog_id" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="post_id" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="C_BY" Visible="False"></asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnUP" AlternateText="upimg" runat="server" ImageUrl="~/images_common/upimg.gif"
                                CommandName="Up" />
                            <asp:ImageButton ID="imgbtnDown" AlternateText="DownImg" runat="server" ImageUrl="~/images_common/DownImg.gif"
                                CommandName="Down" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Blog Post" SortExpression="post_title">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnPostTitle" runat="server" CommandName="View" Text='<%# Eval("post_title")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" Visible="false"> 
                        <ItemTemplate>
                            <strong># of Comments</strong><%# Eval("CommentsCount")%><br /><strong># of Users Commented</strong><%# Eval("CommentsUserCount")%><br /><strong>Last Comment</strong><%# Eval("C_DATE")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkPreview" runat="server" CommandName="Preview" Text="Preview"></asp:LinkButton>&nbsp;
                            <asp:LinkButton ID="lnkEdit" runat="server"  CommandName="Edit" Text="Edit"></asp:LinkButton>&nbsp;
                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure to delete this record?');"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Delete">
                        <HeaderTemplate>
                            <label for="chkAll">
                                Delete</label>
                            <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkDelete', this.checked)" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Active">
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkFeatured" runat="server" Checked='<%#Convert.ToBoolean(Eval("Active_Inactive")) %>'/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
            <div class="AdminRightMenu">
                <asp:Button ID="btnDeleteUpdate" CssClass="btnBgClass" OnClick="btnDeleteUpdate_Click" OnClientClick="return confirm ('Are you sure you wish to delete?')" runat="server" Text="Delete" />
                <asp:Button ID="btnFeaturedUpdate" OnClick="btnFeaturedUpdate_Click" CssClass="btnBgClass" runat="server" Text="Save Active"></asp:Button>
            </div>
        </asp:View>
        <asp:View ID="vShowBlog" runat="server">
            <div class="AdminRightMenu">
                <asp:LinkButton ID="lnkBackBlog1" OnClick="lnkBackBlog_Click" runat="server" Text="Back to blog post"></asp:LinkButton>
            </div>
            
            <div id="dvBlogDetails" style="background: #E2EAF4">
                <asp:Label ID="lblBlogPostTitle" runat="server" Font-Bold="true" Font-Size="Medium"></asp:Label>
                <asp:LinkButton ID="itemToEdit" runat="server" OnClick="Blog_Event" CommandName="edit" Text="[Edit]" />&nbsp;
                       
                <asp:LinkButton ID="itemToDelete" runat="server" OnClick="Blog_Event" CommandName="delete" Text="[Delete]" OnClientClick="return confirm ('Are you sure you wish to delete?')" /><br />
                <div id="divPost">
                    <strong>Posted by:</strong>
                    <asp:Label ID="lblPostingDetails" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lbPostDescription" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lbPostTags" runat="server"></asp:Label>
                    <br />
                    <div id="postcomment" runat="server">
                        <strong>
                            <asp:Label ID="lblComment" runat="server" Text="Blog Comments"></asp:Label>
                        </strong>
                            (<asp:Label ID="lblCommentsCount" runat="server"></asp:Label>)&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkAddComment" runat="server" Text="[Add Comment]" OnClientClick="return showCommentDiv('','')"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="dvComments" runat="server" style="background: #C0D2E6">
                <asp:DataGrid ID="dgrdComment" runat="server" OnItemCommand="dgrdBlogComment_ItemCommand" HeaderStyle-Height="0px" AllowPaging="false" 
                    ShowHeader="false" GridLines="None" AllowSorting="true" AutoGenerateColumns="false" PageSize="3">
                    <HeaderStyle HorizontalAlign="Left" />
                    <FooterStyle HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="coment_post_id" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <span>comment posted <b>by</b>
                                    <%# Eval("fullname")%>
                                    <b>on</b>
                                    <%# Eval("c_date") %>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton Text="[Edit]" ID="btnEditComment" runat="server" OnClientClick=<%# "return showCommentDiv('"+Eval("id")+"','"+utils.formatStringForDBSelect(Convert.ToString(Eval("comment_desc")))+"')"%>></asp:LinkButton>
                                    <asp:LinkButton Text="[Delete]" ID="btnDeleteComment" CommandName="delete" OnClientClick="return confirm ('Are you sure you wish to delete?')" runat="server">
                                    </asp:LinkButton><br />
                                    <asp:Label ID="lblCommentDesc" runat="server" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("comment_desc"))) %>'></asp:Label>
                                </span>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" />
                            <HeaderStyle Font-Bold="True" />
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div id="divAddComments" runat="server" style="display: none;">
                    Comment : <asp:TextBox ID="txtEditorComment" TextMode="MultiLine" CssClass="txtEditor-AdminBlog" runat="server"></asp:TextBox>
                    <br />
                    <asp:Button ID="btnAddCmt" Text="comment" CssClass="btnBgClass" runat="server" OnClick="btnAddCmt_OnClick" />
                    <asp:Button ID="btnCancelCmt" Text="cancel" CssClass="btnBgClass" runat="server" OnClientClick="return hideCommentDiv()"/>
                    <asp:HiddenField ID="hdnCommentId" runat="server" />
                </div>
            </div>
        </asp:View>
        <asp:View ID="vEditBlog" runat="server">
            <div class="AdminRightMenu">
                <asp:LinkButton ID="lnkBackBlog2" OnClick="lnkBackBlog_Click" runat="server" Text="Back to blog post" CssClass="toplink" Font-Bold="true"></asp:LinkButton>
            </div>
            <table class="table-full">
                <tr>
                    <td>
                        post title*<br />
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="50" ValidationGroup="blogVCG"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" Display="Dynamic" CssClass="error"
                            runat="server" ControlToValidate="txtTitle" ErrorMessage="please enter blog title" ValidationGroup="blogVCG"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        post description<br />
                        <div id="divAdminBlog">
                            <div id="fckeditor">
                                <FCKeditorV2:FCKeditor ID="FCKDesc" OnPreRender="FCKDesc_PreRender" runat="server" OnLoad="FCKDesc_Load"></FCKeditorV2:FCKeditor>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    tags&nbsp;&nbsp;( Separate by comma )<br />
                                    <asp:TextBox ID="txtTags" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    Latitude<br />
                    <asp:TextBox ID="txtLatitude" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revLatitude" runat="server" ControlToValidate="txtLatitude" SetFocusOnError="true" CssClass="error"
                        Display="dynamic" ErrorMessage="please enter only numeric value" ValidationExpression="\-?[0-9]*\.?[0-9]*" ValidationGroup="blogVCG"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    Longitude<br />
                    <asp:TextBox ID="txtLongitude" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revLongitude" runat="server" ControlToValidate="txtLongitude" SetFocusOnError="true" CssClass="error" 
                        Display="dynamic" ErrorMessage="please enter only numeric value" ValidationExpression="\-?[0-9]*\.?[0-9]*" ValidationGroup="blogVCG"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>  
                    <td>
                        <input type="button" value="update" class="btnBgClass" id="btncreateupdate" runat="server" onclick="ProcessDescription()" />
                        <div style="display:none;">
                            <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" CssClass="btnBgClass" Text="update" ValidationGroup="blogVCG"></asp:Button>
                        </div>
                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" CssClass="btnBgClass" Text="cancel"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:View>
         <asp:View ID="VImportExport" runat="server">
            <div>
            <table>
                    <tr>
                        <td>
                           <h4>Import Blog Posts</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <p>Select an file: <input id="fileImport" name="fileImport" type="file" runat="server" />                       
                        </p>
                        <p>
                          <asp:Button ID="btnImport" runat="server" Text="Upload" CssClass="btnBgClass" OnClick="btnImport_Click" />
                        </p>
                            <asp:Button ID="btnImpblogPostSave2" CssClass="btnBgClass" Text="Import Selected" Visible="false" runat="server" OnClick="btnImpblogPostSave_Click"/>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnImpblogPostSaveAll2" CssClass="btnBgClass" Text="Import All" Visible="false" runat="server" OnClick="btnImpblogPostSaveAll_Click"/>
                            <br />
                          <div style="width: 765px; overflow: auto;">
                              <asp:DataGrid ID="dgrdImpBlogPosts" runat="server" Visible="false" AllowPaging="false" AutoGenerateColumns="false">
                                <Columns>                                
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                         <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkPost', this.checked)"/>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost"  runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Post Title" DataField="post_title"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Post Description" DataField="post_description"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Sort Order" DataField="SortOrder"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Images" DataField="ImageUrls"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Latitude" DataField="post_latitude"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Longitude" DataField="post_longitude"></asp:BoundColumn>
                                </Columns>
                              </asp:DataGrid>
                          </div> 
                          <br />
                          <asp:Button ID="btnImpblogPostSave" CssClass="btnBgClass" Text="Import Selected" Visible="false" runat="server" OnClick="btnImpblogPostSave_Click"/>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnImpblogPostSaveAll" CssClass="btnBgClass" Text="Import All" Visible="false" runat="server" OnClick="btnImpblogPostSaveAll_Click"/>
                            <p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                            <h4>Export Blog Posts</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <p>
                            <asp:LinkButton ID="lnkExportBlogPosts" OnClick="lnkExportBlogPosts_Click" runat="server" Text="Export Blog Posts" OnClientClick="return confirm('do you want to export all blog posts?');"></asp:LinkButton>
                        </p>
                        </td>
                    </tr>
                </table>
        </div>
        </asp:View> 
    </asp:MultiView>
    <asp:HiddenField ID="hdBlogPostId" runat="server" />
    <asp:HiddenField ID="hdEditSource" runat="server" />
</div>

<script type="text/javascript">
    function ProcessDescription() {
        var oEditorPostDesc = FCKeditorAPI.GetInstance('<%=FCKDesc.ClientID%>');
        var postdescinnerHTML;
        if (oEditorPostDesc.EditMode == 1)
        {
            postdescinnerHTML = oEditorPostDesc.EditingArea.Textarea.value;
        }
        else
        {
            postdescinnerHTML = oEditorPostDesc.EditingArea.IFrame.contentWindow.document.body.innerHTML;
        }
        var div = document.createElement("div");
        div.innerHTML = postdescinnerHTML;
        var images = div.getElementsByTagName("img");
        for (var i = 0; i < images.length; i++)
        {
            var imagealt = images[i].alt;
            if (imagealt == "")
            {
                var imagesrc = images[i].src;
                var filename = imagesrc.split('/').pop().split('#')[0].split('?')[0];
                imagealt = filename.substring(0, filename.lastIndexOf(".")).split("-").join(" ").split("_").join(" ");
                images[i].alt = imagealt;
            }
        }
        if (oEditorPostDesc.EditMode == 1) 
        {
            oEditorPostDesc.EditingArea.Textarea.value = div.innerHTML;
        }
        else
        {
            oEditorPostDesc.EditingArea.IFrame.contentWindow.document.body.innerHTML = div.innerHTML;
        }
        document.getElementById("<%=btnUpdate.ClientID%>").click();
    }

    function FCKUpdateLinkedField(id) {
        try {
            if (typeof (FCKeditorAPI) == "object") {
                FCKeditorAPI.GetInstance(id).UpdateLinkedField();
            }
        }
        catch (err) {
        }
    }

    function showCommentDiv(CommentId,Comment) {
        if (CommentId != null && CommentId != '') {
            document.getElementById('<%= hdnCommentId.ClientID%>').value = CommentId;
            document.getElementById('<%= txtEditorComment.ClientID%>').value = Comment;
        }
        else {
            document.getElementById('<%= hdnCommentId.ClientID%>').value = "";
            document.getElementById('<%= txtEditorComment.ClientID%>').value = "";
        }
        document.getElementById('<%= divAddComments.ClientID%>').style.display = "inline";
        return false;
    }
    function hideCommentDiv() {
        document.getElementById('<%= txtEditorComment.ClientID%>').value = "";
        document.getElementById('<%= hdnCommentId.ClientID%>').value = "";
        document.getElementById(divElement).style.display = "none";
        return false;
    }

    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID);  //generated control name starts with a colon
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i];
            if (elm.type == 'checkbox') {
                if (re.test(elm.name))
                    elm.checked = checkVal;
            }
        }
    }
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID);  //generated control name starts with a colon
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i];
            if (elm.type == 'checkbox') {
                if (re.test(elm.name))
                    elm.checked = checkVal;
            }
        }
    }
</script>

</asp:Content>

