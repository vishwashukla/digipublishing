﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_Calendar.aspx.cs" Inherits="AdminTask_Manage_Calendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="content-full">
        <h3 class="HeaderClass">Manage Calendar</h3>
        <asp:Label ID="lblmessage" runat="server" Text="" CssClass="error"></asp:Label>
        <asp:MultiView ID="mView" runat="server" ActiveViewIndex="0">
            <asp:View ID="Vgrid" runat="server">
                <div class="AdminRightMenu">   
                    <asp:LinkButton ID="btnAddiCal" runat="server" OnClick="btnAddiCal_Click" Text="[Add iCal feed URL]"></asp:LinkButton>
                </div>
               <asp:DataGrid ID="dgrdCalendar" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnSortCommand="dgrdCalendar_SortCommand"
                OnItemCommand="dgrdCalendar_OnItemCommand" AllowPaging="True" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrdCalendar_PageIndexChanged" PageSize="20">
                    <Columns>
                        <asp:BoundColumn DataField="Cal_Id" Visible="False"></asp:BoundColumn>
                          <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnUP" runat="server" AlternateText="upimg" ImageUrl="~/images_common/upimg.gif" CommandName="Up" />
                            <asp:ImageButton ID="imgbtnDown" runat="server" AlternateText="DownImg" ImageUrl="~/images_common/DownImg.gif" CommandName="Down" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Name" SortExpression="Title">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("Title"))) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Contents" SortExpression="link">
                            <ItemTemplate>
                                <asp:Label ID="lblFeedURL" runat="server" Text='<%# Convert.ToString(Eval("Link")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnDelete" runat="server" Text="Delete" CommandName="Delete" Visible='<%#Convert.ToBoolean(Eval("Status"))%>' OnClientClick="return confirm('Are you sure,you want to delete this record?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:View>
            <asp:View ID="vAdd" runat="server">
                <table class="table-full">
                    <tr>
                        <td>
                            Title</td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle" ForeColor="Red"
                                Display="Dynamic" ErrorMessage="Please enter title" SetFocusOnError="True" ValidationGroup="vGrpCalendar"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td>
                            Calendar URL
                        </td>
                        <td>
                            <asp:TextBox ID="txtCalURL" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCalURL" runat="server" ControlToValidate="txtCalURL" ForeColor="Red"
                                Display="Dynamic" ErrorMessage="Please enter URL" SetFocusOnError="True" ValidationGroup="vGrpCalendar"></asp:RequiredFieldValidator><br />
                            <asp:RegularExpressionValidator ID="revCalURL" runat="server" ControlToValidate="txtCalURL" ForeColor="Red"
                                Display="Dynamic" ErrorMessage="Please enter valid URL" ValidationExpression="^(http(s)?|webcal)://([\w-]+\.)+[\w-]+(/[\w- ./?%&=,]*)?"
                                ValidationGroup="vGrpCalendar"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">  
                            <asp:Button ID="btnSubmit" CssClass="btnBgClass" runat="server" OnClick="btnSubmit_Click" ValidationGroup="vGrpCalendar" Text="update" />
                            <asp:Button ID="btnCancel" CssClass="btnBgClass" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>    
</asp:Content>

