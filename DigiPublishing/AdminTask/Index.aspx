﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="AdminTask_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    Welcome <b><%= Convert.ToString(Session["username"]) %></b>, 
    <br /><br /> Please click on links in left panel to get into related section.
</asp:Content>

