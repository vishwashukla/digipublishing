﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminTask_Manage_Components : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Administrator : Homepage";
        lblmessage.Text = "";

        if (!IsPostBack)
        {
            BindFeature_Comp();
            ShowFeatures_Comp();

            BindFeatures();
            ShowFeatures();
            SetFCKPath();
        }
    }

    private void BindFeature_Comp()
    {
        string strQuery = "select features from brand_settings";
        DataTable dtAdminFeatures = dp.FillDataTable(strQuery);
        string[] arrAdminFeatures = { };
        if (dtAdminFeatures != null && dtAdminFeatures.Rows.Count > 0)
            arrAdminFeatures = Convert.ToString(dtAdminFeatures.Rows[0][0]).Split(',');
        DataTable dtFeature = new DataTable();
        dtFeature.Columns.Add("featureName", typeof(string));
        dtFeature.Columns.Add("featureVal", typeof(Int32));
        foreach (string feature in Enum.GetNames(typeof(features)))
        {
            int enumVal = (int)Enum.Parse(typeof(features), feature);
            if (arrAdminFeatures.Contains(Convert.ToString(enumVal)))
            {
                DataRow dr = dtFeature.NewRow();
                if (feature == features.MostPopular.ToString())
                    dr["featureName"] = "Most Popular";
                else if (feature == features.BlogPostSummary.ToString())
                    dr["featureName"] = "Blog Post Summary";
                else if (feature == features.registerlogin.ToString())
                    dr["featureName"] = "Register / Login";
                else
                    dr["featureName"] = feature;
                dr["featureVal"] = enumVal;
                dtFeature.Rows.Add(dr);
            }
        }
        chkDefaultFeature.DataSource = dtFeature;
        chkDefaultFeature.DataBind();
    }

    private void ShowFeatures_Comp()
    {
        utils.SetSelectedCheckBoxItems(chkDefaultFeature, ReadConfig.GetFeatures(ReadConfig.getDomainUrl()));
        foreach (ListItem li in chkDefaultFeature.Items)
        {
            switch (li.Value)
            {
                case "8":
                    li.Enabled = false;
                    li.Selected = true;
                    break;
                case "9":
                    li.Enabled = false;
                    li.Selected = true;
                    break;
                case "10":
                    li.Enabled = false;
                    li.Selected = true;
                    break;
                case "20":
                    li.Enabled = false;
                    li.Selected = true;
                    break;
                case "21":
                    li.Enabled = false;
                    li.Selected = true;
                    break;
            }
        }
    }

    private void BindFeatures()
    {
        string strQuery = "select features from brand_settings";
        DataTable dtAdminFeatures = dp.FillDataTable(strQuery);

        string[] arrAdminFeatures = { };
        if (dtAdminFeatures != null && dtAdminFeatures.Rows.Count > 0)
            arrAdminFeatures = Convert.ToString(dtAdminFeatures.Rows[0][0]).Split(',');

        string[] arrconfigFeatures = ReadConfig.GetFeatures(ReadConfig.getDomainUrl()).Split(',');

        DataTable dtFeatureDefault = new DataTable();
        dtFeatureDefault.Columns.Add("featureName", typeof(string));
        dtFeatureDefault.Columns.Add("featureVal", typeof(Int32));
        rdoDefaultFeatureBPS.Visible = false;
        foreach (string s in Enum.GetNames(typeof(features)))
        {
            int enumVal = (int)Enum.Parse(typeof(features), s);
            if (arrconfigFeatures.Contains(Convert.ToString(enumVal)) && arrAdminFeatures.Contains(Convert.ToString(enumVal)))
            {
                if (s != features.BookMarkSummary.ToString() && s != features.RSSSummary.ToString() && s != features.Share.ToString()
                    && s != features.MostPopular.ToString() && s != features.Subscribe.ToString() && s != features.registerlogin.ToString())
                {
                    if (s == features.BlogPostSummary.ToString())
                    {
                        rdoDefaultFeatureBPS.Visible = true;
                    }
                    else if (s == features.Search.ToString())
                    {
                        rdoDefaultFeatureSearch.Visible = true;
                    }
                    else
                    {
                        DataRow dr = dtFeatureDefault.NewRow();
                        dr["featureName"] = s;
                        dr["featureVal"] = enumVal;
                        dtFeatureDefault.Rows.Add(dr);
                    }
                }
            }
        }

        rdoDefaultFeature.DataSource = dtFeatureDefault;
        rdoDefaultFeature.DataBind();
    }

    private void ShowFeatures()
    {
        string strQuery = "select defaultfeature,defaultfeatureMulti,defaultfeatureType,SinglePageCode,BlogsNUmberInSummary,SearchKeyword,SearchResultPageCode,defaultSearch from brand_settings where id=1";
        DataTable dt = dp.FillDataTable(strQuery);
        if (dt != null && dt.Rows.Count > 0)
        {
            if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["defaultfeature"])))
            {
                if (Convert.ToString(dt.Rows[0]["defaultfeature"]) == ((int)Enum.Parse(typeof(features), features.BlogPostSummary.ToString())).ToString())
                {
                    if (rdoDefaultFeature_BPS.Visible == true)
                    {
                        rdoDefaultFeature_BPS.Checked = true;
                    }
                    else
                    {
                        if (rdoDefaultFeature.Items.Count > 0)
                        {
                            rdoDefaultFeature.SelectedValue = Convert.ToString((int)Enum.Parse(typeof(features), rdoDefaultFeature.Items[0].Value.ToString()));
                            string strQuery1 = "update brand_settings set defaultfeature = " + rdoDefaultFeature.SelectedValue + ",defaultfeatureType =0 where id=1";
                            dp.ExecuteNonQuery(strQuery1);
                        }
                    }
                }
                else if (Convert.ToString(dt.Rows[0]["defaultfeature"]) == ((int)Enum.Parse(typeof(features), features.Search.ToString())).ToString())
                {
                    if (rdoDefaultFeature_Search.Visible == true)
                    {
                        rdoDefaultFeature_Search.Checked = true;
                    }
                    else
                    {
                        if (rdoDefaultFeature.Items.Count > 0)
                        {
                            rdoDefaultFeature.SelectedValue = Convert.ToString((int)Enum.Parse(typeof(features), rdoDefaultFeature.Items[0].Value.ToString()));
                            string strQuery1 = "update brand_settings set defaultfeature = " + rdoDefaultFeature.SelectedValue + ",defaultfeatureType=0 where id=1";
                            dp.ExecuteNonQuery(strQuery1);
                        }
                    }
                }
                else
                {
                    rdoDefaultFeature.SelectedValue = Convert.ToString(dt.Rows[0]["defaultfeature"]);
                }
            }
            txtNumberOfPostsinSummary.Text = dt.Rows[0]["BlogsNUmberInSummary"].ToString();
            txtSearchText.Text = dt.Rows[0]["SearchKeyword"].ToString();

            if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["SinglePageCode"])))
                FCKDesc.Value = Convert.ToString(dt.Rows[0]["SinglePageCode"]);

            if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["SearchResultPageCode"])))
                FCKDescSeach.Value = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["SearchResultPageCode"]));

            if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["defaultSearch"])))
            {
                FCKDesc.Value = Convert.ToString(dt.Rows[0]["SinglePageCode"]);
                if(Convert.ToString(dt.Rows[0]["defaultSearch"]) == "2")
                {
                    rdbtnsearch.Checked = true;
                }
                else
                {
                    rdbtnsearch.Checked = false;
                }
            }

            

            string featureType = Convert.ToString(dt.Rows[0]["defaultfeatureType"]);

            if (featureType == "0")
            {
                rdbtnDefault.Checked = true;
                rdbtnsingle.Checked = false;
                this.rdoDefaultFeature.Enabled = true;
                this.btnDefaultFeature.Enabled = true;
                this.btnSinglePage.Enabled = false;
            }
            else if (featureType == "2")
            {
                rdbtnDefault.Checked = false;
                rdbtnsingle.Checked = true;
                this.rdoDefaultFeature.Enabled = false;
                this.btnDefaultFeature.Enabled = false;
                this.btnSinglePage.Enabled = true;
            }
        }
    }
    protected void btnDefaultFeature_Click(object sender, EventArgs e)
    {
        string strCount = "select defaultfeature from brand_settings ";
        DataTable dtCount = dp.FillDataTable(strCount);

        if (dtCount.Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(rdoDefaultFeature.SelectedValue))
            {
                string strQuery = "update brand_settings set defaultfeature = " + rdoDefaultFeature.SelectedValue + ",defaultfeatureType =0 where id=1";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Default feature updated successfully";
            }
            else
            {
                if (rdoDefaultFeature_BPS.Visible == true)
                {
                    if (rdoDefaultFeature_BPS.Checked == true)
                    {
                        int val = (int)Enum.Parse(typeof(features), features.BlogPostSummary.ToString());
                        int numberofpost = 10;
                        if (!String.IsNullOrEmpty(txtNumberOfPostsinSummary.Text))
                        {
                            numberofpost = Convert.ToInt32(txtNumberOfPostsinSummary.Text.Trim());
                        }
                        string strQuery = "update brand_settings set defaultfeature = " + val + ",defaultfeatureType =0,BlogsNUmberInSummary=" + numberofpost + " where id=1";
                        dp.ExecuteNonQuery(strQuery);
                        lblmessage.Text = "Default feature updated successfully";
                        string Key = "TOTALNUMBEROFBLOGPOST_" + ReadConfig.getDomainUrl();
                        if (Cache[Key] != null)
                            Cache.Remove(Key);
                    }
                }
                if (rdoDefaultFeature_Search.Visible == true)
                {
                    if (rdoDefaultFeature_Search.Checked == true)
                    {
                        int val = (int)Enum.Parse(typeof(features), features.Search.ToString());
                        string searckkeyword = Convert.ToString(txtSearchText.Text);
                        string strQuery = "update brand_settings set defaultfeature = " + val + ",defaultfeatureType =0,SearchKeyword='" + searckkeyword + "' where id=1";
                        dp.ExecuteNonQuery(strQuery);
                        lblmessage.Text = "Default feature updated successfully";
                    }
                }
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(rdoDefaultFeature.SelectedValue))
            {
                string strQuery = "insert into brand_settings(defaultfeatureType,defaultfeature) values(0," + rdoDefaultFeature.SelectedValue + ")";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Default feature inserted successfully";
            }
        }
        ShowFeatures();
    }

    protected void btnSinglePage_Click(object sender, EventArgs e)
    {
        string strCount = "select defaultfeature from brand_settings ";
        DataTable dtCount = dp.FillDataTable(strCount);

        if (dtCount.Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(FCKDesc.Value))
            {
                string strQuery = "update brand_settings set SinglePageCode = '" + FCKDesc.Value.Replace("'", "''") + "',defaultfeatureType =2 where id=1";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Default feature updated successfully";
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(rdoDefaultFeature.SelectedValue))
            {
                string strQuery = "insert into brand_settings(defaultfeatureType,SinglePageCode) values(2,'" + FCKDesc.Value.Replace("'", "''") + "')";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Default feature inserted successfully";
            }
        }
        ShowFeatures();
    }

    protected void btnSaveContent_Click(object sender, EventArgs e)
    {
        string strCount = "select defaultfeature from brand_settings ";
        DataTable dtCount = dp.FillDataTable(strCount);

        if (dtCount.Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(FCKDesc.Value))
            {
                string strQuery = "update brand_settings set SinglePageCode = '" + FCKDesc.Value.Replace("'", "''") + "' where id=1";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Single Page Content Saved Successfully";
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(rdoDefaultFeature.SelectedValue))
            {
                string strQuery = "insert into brand_settings(SinglePageCode) values ('" + FCKDesc.Value.Replace("'", "''") + "')";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Single Page Content Saved Successfully";
            }
        }
        ShowFeatures();
    }

    protected void rdbtnDefault_CheckedChanged(object sender, EventArgs e)
    {
        this.rdoDefaultFeature.Enabled = true;
        this.btnDefaultFeature.Enabled = true;
        this.btnSinglePage.Enabled = false;
    }

    protected void rdbtnsingle_CheckedChanged(object sender, EventArgs e)
    {
        this.rdoDefaultFeature.Enabled = false;
        this.btnDefaultFeature.Enabled = false;
        this.btnSinglePage.Enabled = true;
    }

    public void SetFCKPath()
    {
        string strPath = Request.ApplicationPath;
        if (strPath == "/")
        {
            FCKDesc.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
            //FCKDescSeach.BasePath = strPath + "Mj68UNnFfswhXwaFCKeditor/";
        }
        else
        {
            FCKDesc.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
            //FCKDescSeach.BasePath = strPath + "/Mj68UNnFfswhXwaFCKeditor/";
        }
    }

    protected void FCKDesc_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedField(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this,
                                                    FCKDesc.GetType(),
                                                    "FCKUpdater",
                                                    script,
                                                    true);
            ScriptManager.RegisterOnSubmitStatement(this,
                                                    FCKDesc.GetType(),
                                                    "WebContentManagerEditorScript_" + this.FCKDesc.ClientID,
                                                    "FCKUpdateLinkedField('" + this.FCKDesc.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKDesc.GetType(),
                                                        "FCKUpdater",
                                                        script,
                                                        true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKDesc.GetType(),
                                                             "WebContentManagerEditorScript_" + this.FCKDesc.ClientID,
                                                             "FCKUpdateLinkedField('" + this.FCKDesc.ClientID + "');");
        }
    }
    protected void FCKDesc_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.FCKDesc.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, FCKDesc.GetType(), "FCKSource", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, FCKDesc.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKDesc.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKDesc.GetType(), "FCKSource", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKDesc.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKDesc.ClientID, "setTimeout(FCKeditor_OnComplete,500);");
        }
    }

    protected void btnDefaultFeature1_Click(object sender, EventArgs e)
    {
        string featureList = utils.GetSelectedCheckBoxItems(chkDefaultFeature);
        ReadConfig.UpdateSingleNodeInConfig(ReadConfig.getDomainUrl(), "features", featureList);
        ShowFeatures_Comp();
        BindFeatures();
        ShowFeatures();
    }

   /* 
    protected void FCKDescSeach_PreRender(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = @"function FCKUpdateLinkedFieldSearch(id)
                            {
                                try
                                {
                                    if(typeof(FCKeditorAPI) == 'object')
                                    {
                                        FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                                    }
                                }
                                catch(err)
                                {
                                }
                            }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this,
                                                    FCKDescSeach.GetType(),
                                                    "FCKUpdaterSearch",
                                                    script,
                                                    true);
            ScriptManager.RegisterOnSubmitStatement(this,
                                                    FCKDescSeach.GetType(),
                                                    "WebContentManagerEditorScript_" + this.FCKDescSeach.ClientID,
                                                    "FCKUpdateLinkedFieldSearch('" + this.FCKDescSeach.ClientID + "');");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKDescSeach.GetType(),
                                                        "FCKUpdaterSearch",
                                                        script,
                                                        true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKDescSeach.GetType(),
                                                             "WebContentManagerEditorScript_" + this.FCKDescSeach.ClientID,
                                                             "FCKUpdateLinkedFieldSearch('" + this.FCKDescSeach.ClientID + "');");
        }
    }

    protected void FCKDescSeach_Load(object sender, EventArgs e)
    {
        ScriptManager current = ScriptManager.GetCurrent(this.Page);
        String script = "function FCKeditor_OnComplete_Search(ID)"
                       + "{"
                       + "var oFCKeditor = FCKeditorAPI.GetInstance('" + this.FCKDescSeach.ClientID + "');"
                       + " oFCKeditor.Commands.GetCommand('Source').Execute();"
                      + " }";
        if (current != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, FCKDescSeach.GetType(), "FCKSourcesearch", script, true);
            ScriptManager.RegisterOnSubmitStatement(this, FCKDescSeach.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKDescSeach.ClientID, "setTimeout(FCKeditor_OnComplete_Search,500);");
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(FCKDescSeach.GetType(), "FCKSourcesearch", script, true);
            this.Page.ClientScript.RegisterOnSubmitStatement(FCKDescSeach.GetType(), "WebContentManagerEditorSourceScript_" + this.FCKDescSeach.ClientID, "setTimeout(FCKeditor_OnComplete_Search,500);");
        }
    }
    */


    protected void btnSaveContentSearch_Click(object sender, EventArgs e)
    {
        string strCount = "select defaultfeature from brand_settings ";
        DataTable dtCount = dp.FillDataTable(strCount);

        if (dtCount.Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(FCKDesc.Value))
            {
                string strQuery = "update brand_settings set SearchResultPageCode = '" + utils.formatStringForDBInsert(FCKDescSeach.Value.Replace("'", "''")) + "' where id=1";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Single Page Content Saved Successfully";
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(rdoDefaultFeature.SelectedValue))
            {
                string strQuery = "insert into brand_settings(SearchResultPageCode) values ('" + utils.formatStringForDBInsert(FCKDescSeach.Value.Replace("'", "''")) + "')";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Single Page Content Saved Successfully";
            }
        }
        ShowFeatures();
    }

    protected void btnSetSearcResult_Click(object sender, EventArgs e)
    {
        string strCount = "select defaultfeature from brand_settings ";
        DataTable dtCount = dp.FillDataTable(strCount);
        int defaultsearch = 1;
        if (rdbtnsearch.Checked)
            defaultsearch = 2;

        if (dtCount.Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(FCKDesc.Value))
            {
                string strQuery = "update brand_settings set SearchResultPageCode = '" + utils.formatStringForDBInsert(FCKDescSeach.Value.Replace("'", "''")) + "',defaultSearch="+ defaultsearch + " where id=1";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Default feature updated successfully";
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(rdoDefaultFeature.SelectedValue))
            {
                string strQuery = "insert into brand_settings(defaultSearch,SearchResultPageCode) values(" + defaultsearch + ",'" + utils.formatStringForDBInsert(FCKDescSeach.Value.Replace("'", "''")) + "')";
                dp.ExecuteNonQuery(strQuery);
                lblmessage.Text = "Default feature inserted successfully";
            }
        }
        ShowFeatures();
    }
}