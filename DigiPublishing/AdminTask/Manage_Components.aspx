﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_Components.aspx.cs" Inherits="AdminTask_Manage_Components" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="content-full">
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                  && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
        </script>
        <style>
            .AspNet-RadioButtonList ul
            {
                margin-bottom:0px;
            }
        </style>
        <h3 class="HeaderClass">Manage Component</h3>
        <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
       <div>
           <table class="table-full">
                <tr>
                    <td>&nbsp;&nbsp;<strong>Features</strong></td>
                    <td>
                        <strong>Single Homepage</strong>&nbsp;&nbsp;
                        <asp:RadioButton ID="rdbtnDefault" runat="server" Checked="true" AutoPostBack="true" GroupName="grpHomepage" OnCheckedChanged="rdbtnDefault_CheckedChanged" />
                    </td>
                </tr>
                <tr style="vertical-align:top">
                    <td>
                        <asp:CheckBoxList ID="chkDefaultFeature" runat="server" CssClass="select" DataTextField="featureName" DataValueField="featureVal"></asp:CheckBoxList>
                    </td>
                    <td style="background-color:#ffffff!important;">                 
                        <asp:RadioButtonList ID="rdoDefaultFeature" runat="server" DataTextField="featureName" DataValueField="featureVal"></asp:RadioButtonList>
                        <div class="AspNet-RadioButtonList">
		                    <ul class="AspNet-RadioButtonList-RepeatDirection-Vertical" style="margin:0px;">
                                <li class="AspNet-RadioButtonList-Item" id="rdoDefaultFeatureSearch" runat="server" visible="false" style="padding-bottom:1px;">
                                    <input id="rdoDefaultFeature_Search" type="radio" runat="server" name="rdoDefaultFeature" value="7" />
                                    <label for="ctl00_ContentPlaceHolderMain_rdoDefaultFeature_Search">Search</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <b>Keyword for search : </b><asp:TextBox ID="txtSearchText" runat="server" Width="100"></asp:TextBox>  
			                    </li>
			                    <li class="AspNet-RadioButtonList-Item" id="rdoDefaultFeatureBPS" runat="server" visible="false">
                                    <input id="rdoDefaultFeature_BPS" type="radio" runat="server" name="rdoDefaultFeature" value="11" />
                                    <label for="ctl00_ContentPlaceHolderMain_rdoDefaultFeature_BPS">Blog Post Summary</label>
                                    <asp:TextBox ID="txtNumberOfPostsinSummary" runat="server" MaxLength="2" Width="30" onkeypress="return isNumberKey(event);"></asp:TextBox>  
			                    </li>
		                    </ul>
                        </div>             
                    </td>
                </tr>
                <tr>
                <td>
                    &nbsp;&nbsp;<asp:Button ID="btnDefaultFeature1" runat="server" CssClass="btnBgClass" Text="Set Default Feature" OnClick="btnDefaultFeature1_Click" />
                  </td>
                    <td>
                        <asp:Button ID="btnDefaultFeature" runat="server" CssClass="btnBgClass" Text="Set Default Homepage" OnClick="btnDefaultFeature_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="content-lower" >
        <div class="div-single-page" >
            <strong>Single Page</strong>&nbsp;&nbsp;
                <asp:RadioButton ID="rdbtnsingle" runat="server" AutoPostBack="true" GroupName="grpHomepage" OnCheckedChanged="rdbtnsingle_CheckedChanged" />
        </div>

        <div id="fckeditor">
            <FCKeditorV2:FCKeditor ID="FCKDesc" OnPreRender="FCKDesc_PreRender" runat="server" OnLoad="FCKDesc_Load"></FCKeditorV2:FCKeditor>
        </div>

        <div class="save-page">
            &nbsp;&nbsp;<asp:Button ID="btnSinglePage" runat="server" CssClass="btnBgClass" Text="Set Single Page" OnClick="btnSinglePage_Click" />
            &nbsp;<asp:Button ID="btnSaveContent" runat="server" CssClass="btnBgClass" Text="Save Conent" OnClick="btnSaveContent_Click" />
        </div>
    </div>
    <br />
    <div id="content-lower" >
        <div class="div-single-page" >
            <strong>Search Results Page</strong>&nbsp;&nbsp;
                <asp:CheckBox ID="rdbtnsearch" runat="server" AutoPostBack="true" />
        </div>

        <div id="fckeditor-search">
            <%--<FCKeditorV2:FCKeditor ID="FCKDescSeach" OnPreRender="FCKDescSeach_PreRender" runat="server" OnLoad="FCKDescSeach_Load"></FCKeditorV2:FCKeditor>--%>
            <textarea runat="server" id="FCKDescSeach" style="width:99%;height:200px;"></textarea>
        </div>

        <div class="save-page">
            &nbsp;&nbsp;<asp:Button ID="btnSetSearcResult" runat="server" CssClass="btnBgClass" Text="Set Default Search" OnClick="btnSetSearcResult_Click" />
            &nbsp;<asp:Button ID="btnSaveContentSearch" runat="server" CssClass="btnBgClass" Text="Save Conent" OnClick="btnSaveContentSearch_Click" />
        </div>
    </div>

    <script type="text/javascript">
        function FCKUpdateLinkedField(id) {
            try {
                if (typeof (FCKeditorAPI) == "object") {
                    FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                }
            }
            catch (err) {
            }
        }
        function FCKUpdateLinkedFieldSearch(id) {
            try {
                if (typeof (FCKeditorAPI) == "object") {
                    FCKeditorAPI.GetInstance(id).UpdateLinkedField();
                }
            }
            catch (err) {
            }
        }
    </script>
</asp:Content>

