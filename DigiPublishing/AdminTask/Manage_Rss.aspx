﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrandTemplates/DigiPublishingAdmin.master" AutoEventWireup="true" CodeFile="Manage_Rss.aspx.cs" Inherits="AdminTask_Manage_Rss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="content-full">
    <h3 class="HeaderClass">Manage RSS Feeds</h3>
    <asp:Label ID="lblMsg" runat="server" Text="" CssClass="error"></asp:Label><br />
    <a href="../RSSFeed-instant-articles.aspx" target="_blank">Facebook Instant Articles RSS</a>
    <asp:MultiView ID="MVRssReader" runat="server">
        <asp:View ID="ViewRssGrid" runat="server">
            <div class="AdminRightMenu">
                <asp:LinkButton ID="btnAddFeed" runat="server" OnClick="btnAddFeed_Click" Text="[Add to Rss Reader]"></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkImportExport" runat="server" Text="[Import/Export Feeds]" OnClick="lnkImportExport_Click" />
            </div>
            <strong>RSS Reader</strong>
            <asp:DataGrid ID="drgdRssReader" GridLines="None" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" AllowSorting="true" 
                OnItemCommand="drgdRssReader_ItemCommand" PagerStyle-Mode="NumericPages" OnSortCommand="drgdRssReader_SortCommand" OnPageIndexChanged="drgdRssReader_PageIndexChanged">
                <Columns>
                    <asp:BoundColumn DataField="feed_id" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnUP" runat="server" AlternateText="upimg" ImageUrl="~/images_common/upimg.gif" CommandName="Up" />
                            <asp:ImageButton ID="imgbtnDown" runat="server" AlternateText="DownImg" ImageUrl="~/images_common/DownImg.gif" CommandName="Down" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Title" SortExpression="RssTitle">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRssTitle" runat="server" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("Title"))) %>' Width="150px" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Link" SortExpression="link">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRssLink" runat="server" Text='<%# Eval("link") %>' Width="350px" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Tag" SortExpression="Rss_Tags">
                        <ItemTemplate>
                            <asp:TextBox ID="txtTag" runat="server" Text='<%# utils.formatStringForDBSelect(Convert.ToString(Eval("RssTags"))) %>' Width="50px" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Delete">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDelete" Text="[Delete]" runat="server" CommandName="delete" OnClientClick="return confirm('Are you sure to delete?')"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <a href='<%# Eval("link") %>' target="_blank">Preview</a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
            <div class="AdminRightMenu">
                <asp:Button ID="btnUpdateRssReaderFeatures" OnClick="btnUpdateRssReaderFeatures_Click" CssClass="btnBgClass" runat="server" Text="Save"></asp:Button>
            </div>
        </asp:View>
        <asp:View ID="VAddRss" runat="server">
            <table>
                <tr>
                    <td>
                        Title</td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle" ForeColor="Red"
                            Display="Dynamic" ErrorMessage="please enter title" SetFocusOnError="True" ValidationGroup="vGrpRss"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        RSS URL</td> 
                    <td>
                        <asp:TextBox ID="txtLink" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvURL" runat="server" ControlToValidate="txtLink" ForeColor="Red"
                            Display="Dynamic" ErrorMessage="please enter URL" SetFocusOnError="True" ValidationGroup="vGrpRss"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="revURL" runat="server" ControlToValidate="txtLink" ForeColor="Red"
                            Display="Dynamic" ErrorMessage="please enter valid URL" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=,]*)?"
                            ValidationGroup="vGrpRss"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tags</td>
                    <td>
                        <asp:TextBox ID="txtTags" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnCreate" CssClass="btnBgClass" runat="server" OnClick="btnCreate_Click" ValidationGroup="vGrpRss" Text="Add Feed" />
                        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" CssClass="btnBgClass" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="VShowRss" runat="server">
            <div class="AdminRightMenu">
                <asp:LinkButton ID="lnkBackRss" runat="server" Text="Back to RSS Reader" CssClass="toplink" OnClick="lnkBackRss_Click"></asp:LinkButton>
            </div>
            <asp:Label ID="lblNoFeedSaved" runat="server" Visible="false" Text="No feed has been added by you yet..." />
            <asp:DataList ID="dlstFeed" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <a href='<%# Eval("link")%>' target="_blank">
                                    <%# Eval("title")%>
                                </a>
                                <br />
                                <%# Eval("pubdate")%>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%# Eval("description")%>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
        </asp:View>
        <asp:View ID="VImportExport" runat="server">
            <table>
                <tr>
                    <td>
                        <h4>
                            Import your subscriptions</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Select an OPML file:
                            <input id="fileImportOpml" name="fileImportOpml" type="file" runat="server" />
                        </p>
                        <p>
                            <asp:Button ID="btnImportOpml" runat="server" Text="Upload" CssClass="btnBgClass" OnClick="btnImportOpml_Click" />
                            <asp:DataGrid ID="dgrdOPML" runat="server" OnItemDataBound="dgrdOPML_OnItemDataBound">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkRSS', this.checked)" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRSS" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Title" SortExpression="link">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txt_Title" runat="server" Text='<%# Eval("title_text") %>' Width="150px" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="RSS URL" DataField="xmlurl"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="RSS Tag" DataField="tagging"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <br />
                            <asp:Button ID="btnSubmit" CssClass="btnBgClass" Text="Submit" Visible="false" runat="server" OnClick="btnSubmit_Click" />
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>
                            Export your subscriptions</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <asp:LinkButton ID="lnkExportOpml" runat="server" OnClick="lnkExportOpml_Click">
                                Export your subscriptions as an OPML file.</asp:LinkButton>
                        </p>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    </div>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID);  //generated control name starts with a colon
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal;
                }
            }
        }
    </script>
</asp:Content>