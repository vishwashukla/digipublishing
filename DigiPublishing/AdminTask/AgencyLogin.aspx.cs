﻿using System;
using System.Data;

public partial class AdminTask_AgencyLogin : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Request["id"])))
        {
            string strSql = "SELECT TOP 1 uid,fullname,email,roles FROM Brand_User WHERE email='" + Convert.ToString(Request["id"]) + "'";
            DataTable dt = dp.FillDataTable(strSql);
            if (dt.Rows.Count > 0)
            {
                Session["userid"] = Convert.ToString(dt.Rows[0]["uid"]);
                Session["username"] = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["fullname"]));
                Session["Email"] = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["email"]));
                Session["role"] = Convert.ToString(dt.Rows[0]["roles"]);
            }
            Response.Redirect("~/ondemandhome.aspx");
        }
    }
}