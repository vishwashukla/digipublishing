﻿using System;
using System.Data;
using System.Text;
using System.Web;

public partial class Widget_Ajax : System.Web.UI.Page
{
    private StringBuilder SB = new StringBuilder();
    private string strDomainName;
    private string BannerIDs = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        strDomainName = ReadConfig.getDomainUrl();
        GetWidgets();
        Response.ContentType = "application/xml";
        Response.Write(SB.ToString());
    }

    private void GetWidgets()
    {
        string widgetconid = utils.Getcountry();
        string widgetBrowser = utils.GetBrowser();
        string widgetOS = utils.GetOperatingSystem();
        string stradValue = string.Empty;
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dtAdBox = dp.FillDataTable("select * from brand_Applist_Random");
        SB.AppendLine("<AllWidgets>");
        if (dtAdBox.Rows.Count > 0)
        {
            foreach (DataRow dr in dtAdBox.Rows)
            {
                /*  right */
                if (Convert.ToString(dr["Random_Position"]) == "random_right")
                {
                    SB.AppendLine("<Right>");
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        funAdRandom("righttop", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        funAd("righttop", widgetconid, widgetBrowser, widgetOS);
                    }
                    SB.AppendLine("</Right>");

                } /* left */
                else if (Convert.ToString(dr["Random_Position"]) == "random_left")
                {
                    SB.AppendLine("<Left>");
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        funAdRandom("leftbottom", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        funAd("leftbottom", widgetconid, widgetBrowser, widgetOS);
                    }
                    SB.AppendLine("</Left>");
                }
                /*  bottom */
                else if (Convert.ToString(dr["Random_Position"]) == "random_bottom")
                {
                    SB.AppendLine("<Bottom>");
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        funAdRandom("bottomright", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        funAd("bottomright", widgetconid, widgetBrowser, widgetOS);
                    }
                    SB.AppendLine("</Bottom>");
                }
                /*  pop under */
                else if (Convert.ToString(dr["Random_Position"]) == "random_pop_under")
                {
                    SB.AppendLine("<Pop>");
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        funAdRandom("pop_under", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        funAd("pop_under", widgetconid, widgetBrowser, widgetOS);
                    }
                    SB.AppendLine("</Pop>");
                }
                /*  top  */
                else if (Convert.ToString(dr["Random_Position"]) == "random_topheader")
                {
                    SB.AppendLine("<Top>");
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        funAdRandom("topheader", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        funAd("topheader", widgetconid, widgetBrowser, widgetOS);
                    }
                    SB.AppendLine("</Top>");
                }
                /*  subline  */
                else if (Convert.ToString(dr["Random_Position"]) == "random_subline")
                {
                    SB.AppendLine("<Subline>");
                    if (Convert.ToInt32(dr["Value"]) == 1)
                    {
                        funAdRandom("subline", widgetconid, widgetBrowser, widgetOS);
                    }
                    else if (Convert.ToInt32(dr["Value"]) == 0)
                    {
                        funAd("subline", widgetconid, widgetBrowser, widgetOS);
                    }
                    SB.AppendLine("</Subline>");
                }
            }
        }
        SB.AppendLine("<BannerIDs><![CDATA[" + BannerIDs.Trim(',') + "]]></BannerIDs>");
        SB.AppendLine("</AllWidgets>");
    }

    private void funAd(string strPosition, string widgetconid, string widgetBrowser,string widgetOS)
    {
        DataTable dtAdBoxIndivi = new DataTable();
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        string codebox = string.Empty;
        int App_Id = 0;
        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_BrandAppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        strQueryAdBox.AddParameter("@Position", strPosition);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);
        if (dt_AppID.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_AppID.Rows)
            {
                App_Id = Convert.ToInt32(dr["id"]);
                string KeyWidgetAllPosition = "WidgetAllPosition" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + App_Id + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + strPosition.Trim() + widgetOS.Trim();
                if (Cache[KeyWidgetAllPosition] != null)
                {
                    dtAdBoxIndivi = (DataTable)Cache[KeyWidgetAllPosition];
                }
                else
                {
                    string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
                    strSQLRule = SqlQueryMain;
                    DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
                    dtAdBoxIndivi = dp1.FillDataTable(strSQLRule);
                    Cache.Insert(KeyWidgetAllPosition, dtAdBoxIndivi, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
                }

                if (dtAdBox.Rows.Count > 0)
                {
                    foreach (DataRow drIndivi in dtAdBoxIndivi.Rows)
                    {
                        dtAdBox.ImportRow(drIndivi);
                    }
                }
                else
                    dtAdBox = dtAdBoxIndivi.Copy();
            }
        }
        if (dtAdBox.Rows.Count > 0)
        {
            foreach (DataRow dr in dtAdBox.Rows)
            {
                if (utils.ReferalCheck(Convert.ToString(dr["Referal"]), Convert.ToString(dr["Not_Referal"])))
                {
                    GetfunAd(dr, strPosition, dtAdBox.Rows.IndexOf(dr));
                }
            }
        }
    }

    private void funAdRandom(string strPosition, string widgetconid, string widgetBrowser,string widgetOS)
    {
        DataTable dtAdBox = new DataTable();
        string strSQLRule = string.Empty;
        int App_Id = 0;

        DBCommand strQueryAdBox = new DBCommand();
        strQueryAdBox.CommandText = "SP_Brand_Select_Top1_Brand_AppList_OnPosition";
        strQueryAdBox.CommandType = CommandType.StoredProcedure;
        strQueryAdBox.AddParameter("@Position", strPosition);
        strQueryAdBox.AddParameter("@Country", widgetconid);
        strQueryAdBox.AddParameter("@Browser", widgetBrowser);
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        DataTable dt_AppID = dp.FillDataTable(strQueryAdBox);

        if (dt_AppID.Rows.Count > 0)
        {
            App_Id = Convert.ToInt32(dt_AppID.Rows[0]["id"]);
        }
        string KeyWidgetRandomAllPosition = "WidgetRandomAllPosition" + Convert.ToString(ReadConfig.getDomainUrl()).Trim() + App_Id + widgetBrowser.Trim() + Convert.ToInt32(widgetconid.Trim()) + strPosition.Trim() + widgetOS.Trim();
        if (Cache[KeyWidgetRandomAllPosition] != null)
        {
            dtAdBox = (DataTable)Cache[KeyWidgetRandomAllPosition];
        }
        else
        {
            string SqlQueryMain = "EXEC SP_Brand_Select_Brand_AppListRuleSettings @Country='" + widgetconid + "' ,@Browser='" + widgetBrowser + "',@id=" + App_Id + ",@OS='" + widgetOS + "'";
            strSQLRule = SqlQueryMain;
            DBHelper dp1 = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            dtAdBox = dp1.FillDataTable(strSQLRule);
            Cache.Insert(KeyWidgetRandomAllPosition, dtAdBox, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        if (dtAdBox.Rows.Count > 0)
        {
            if (utils.ReferalCheck(Convert.ToString(dtAdBox.Rows[0]["Referal"]), Convert.ToString(dtAdBox.Rows[0]["Not_Referal"])))
            {
                GetfunAd(dtAdBox.Rows[0], strPosition, 0);
            }
        }
    }

    private void GetfunAd(DataRow dr, string strPosition, int rowindex)
    {
        string stradimage = "";
        int IPStatus = 0;
        int IPValue = 0;
        int IPDays = 0;
        int Blocker = 0;
        string ApplicationName = Convert.ToString(dr["ApplicationName"]);
        int BlockerValue = 0;
        int BanID = Convert.ToInt32(dr["id"]);
        int height = 0;
        int width = 0;
        string codebox = string.Empty;

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Status"])))
        {
            IPStatus = Convert.ToInt32(dr["SameIP_Status"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Value"])))
        {
            IPValue = Convert.ToInt32(dr["SameIP_Value"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SameIP_Days"])))
        {
            IPDays = Convert.ToInt32(dr["SameIP_Days"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["Blocker"])))
        {
            Blocker = Convert.ToInt32(dr["Blocker"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["BlockerValue"])))
        {
            BlockerValue = Convert.ToInt32(dr["BlockerValue"]);
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["height"])))
        {
            height = Convert.ToInt32(dr["height"].ToString().Trim());
        }

        if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["width"])))
        {
            width = Convert.ToInt32(dr["width"].ToString().Trim());
        }
        string strclickfunction = "";
        SB.AppendLine("<Data>");
        if (strPosition == "pop_under")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "imagepopunderblock", "WidgetClick", "PopunderBlockerCount", "SameIPCountPopunder", "popunderscript", rowindex, ref strclickfunction);
        }

        if (strPosition == "subline")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "imagesublineblock", "WidgetClick", "SublineBlockerCount", "SameIPCountSubline", "sublinescript", rowindex, ref strclickfunction);
        }
        if (strPosition == "topheader")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "imageblock", "ClickTop", "TopBlockerCount", "SameIPCountTop", "topHeader", rowindex, ref strclickfunction);
        }

        if (strPosition == "leftbottom")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "imageleftblock", "Clickleftbottom", "leftbottomBlockerCount", "SameIPCountleftbottom", "leftBottom", rowindex, ref strclickfunction);
        }

        if (strPosition == "righttop")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "Imglogotextright", "Clickrighttop", "righttopBlockerCount", "SameIPCountrighttop", "rightTop", rowindex, ref strclickfunction);
        }

        if (strPosition == "bottomright")
        {
            stradimage = Get_Image_RegisterScriptBlock_And_Attach_Click_Event(BanID, IPStatus, IPValue, IPDays, Blocker, BlockerValue, ApplicationName
                , "imageBottomblock", "Clickbottomright", "bottomrightBlockerCount", "SameIPCountbottomright", "rightBottom", rowindex, ref strclickfunction);
        }

        Random random = new Random();
        int cacheBuster = random.Next(10000000, 100000000);
        codebox = utils.formatStringForDBSelect(Convert.ToString(dr["CodeBox"])); 
        string pageUrl = Request.Url.Scheme + "://" + Request.Url.Host + Request.RawUrl;
        string newcodebox = codebox.Replace("[Host_URL]", strDomainName).Replace("[CACHEBUSTER]", Convert.ToString(cacheBuster)).Replace("[page_url]", pageUrl).Replace("[ip]", utils.GetUserIP()).Replace("[ua]", utils.GetUserAgent());
        SB.AppendLine("<Codebox>");
        if (height == 0 && width == 0)
        {
            var strcodebox = HttpUtility.JavaScriptStringEncode("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" " + strclickfunction + ">" + stradimage
                + HttpUtility.HtmlDecode(newcodebox) + "</div>");
            SB.Append("<![CDATA[" + strcodebox + "]]>");
        }
        else
        {
            var strcodebox = HttpUtility.JavaScriptStringEncode("<div id=\"" + ApplicationName + "\" class=\"" + ApplicationName + "\" style=\"height:auto;width:" + width + "px;position:relative;\" " + strclickfunction + ">" + stradimage
                + HttpUtility.HtmlDecode(utils.formatStringForDBSelect(newcodebox)) + "</div>");
            SB.Append("<![CDATA[" + strcodebox + "]]>");
        }
        SB.AppendLine("</Codebox>");
        SB.AppendLine("</Data>");
        //utils.updateWidgetCount(BanID);
    }

    private string Get_Image_RegisterScriptBlock_And_Attach_Click_Event(int BanID, int IPStatus, int IPValue, int IPDays, int Blocker, int BlockerValue, string ApplicationName
        , string imageid, string clickfunname, string JSVarBlockerCount, string JSVarSameIPCount, string ScriptblockKeyName, int rowindex, ref string clickfunction)
    {

        int SameIPCount = utils.SameIPClicks(BanID, "banner", IPDays);
        BannerIDs += BanID + ",";
        //utils.Setimperssion(BanID, "banner");
        string strdis = "none";
        string type = "0";
        if (IPStatus == 1 && IPValue <= SameIPCount && BanID != 0)
        {
            strdis = "block";
            type = "2";
        }
        if (strdis == "none" && Blocker != 1)
            type = "1";
        clickfunction = "onclick=\"javascript:" + clickfunname + "('" + BanID + "'," + type + "," + IPStatus + "," + IPValue + "," + BlockerValue + ",'" + imageid + "_" + rowindex
            + "','" + ApplicationName + "','" + rowindex + "')\"";
        string stradimage = "<img  alt='1x2' id ='" + imageid + "_" + rowindex + "' style='display:" + strdis + ";z-index:9999;float:right;position:absolute;left:0;top:0;width:100%; height:100%;' "
            + "src='../images_common/1x2.gif' />";
        StringBuilder strScript = new StringBuilder();
        SB.AppendLine("<SameIPCountVarName><![CDATA[" + JSVarSameIPCount + "_" + rowindex + "]]></SameIPCountVarName>");
        SB.AppendLine("<SameIPCountVarValue><![CDATA[" + SameIPCount + "]]></SameIPCountVarValue>");
        SB.AppendLine("<BlockerCountVarName><![CDATA[" + JSVarBlockerCount + "_" + rowindex + "]]></BlockerCountVarName>");
        SB.AppendLine("<BlockerCountVarValue><![CDATA[0]]></BlockerCountVarValue>");
        return stradimage;
    }
}