﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Text;

public partial class OndemandHome : System.Web.UI.Page
{
    DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
    PagedDataSource _PageDataSourceLinks = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        /* Code to make this page non-accessible by Super Admin domain name*/
        if (Request.Url.Host.ToString() == ConfigLoader.GetSAMainURL())
            Response.Redirect(ConfigLoader.GetErrorPageUrl());
        /*******************************************************************************/

        lblMessage.Text = "";
        if (!Page.IsPostBack)
        {
            SetDefaultHomePage();
        }
    }

    protected void SetDefaultHomePage()
    {
        try
        {
            string defaultPage = "";
            string defaultPageType = "";
            string defaultPageMulti = "";
            string Searchkeyword = "";
            DBCommand strQuery = new DBCommand();
            strQuery.CommandText = "select defaultfeature,defaultfeatureType,defaultfeatureMulti,SearchKeyword,defaultSearch from brand_settings";
            strQuery.CommandType = CommandType.Text;
            DataTable dt = dp.FillDataTable(strQuery);
            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToString(dt.Rows[0]["defaultfeature"]) != "")
                {
                    defaultPage = Convert.ToString(dt.Rows[0]["defaultfeature"]);
                }
                if (Convert.ToString(dt.Rows[0]["defaultfeatureType"]) != "")
                {
                    defaultPageType = Convert.ToString(dt.Rows[0]["defaultfeatureType"]);
                }
                if (Convert.ToString(dt.Rows[0]["defaultfeatureMulti"]) != "")
                {
                    defaultPageMulti = Convert.ToString(dt.Rows[0]["defaultfeatureMulti"]);
                }
                Searchkeyword = Convert.ToString(dt.Rows[0]["SearchKeyword"]);

                if (defaultPageType == "1")
                {
                    string[] arrconfigFeatures = ReadConfig.GetFeatures(ReadConfig.getDomainUrl()).Split(',');
                    string[] arrAdminFeatures = { };

                    if (!string.IsNullOrWhiteSpace(defaultPageMulti))
                        arrAdminFeatures = defaultPageMulti.Split(',');

                    foreach (string feature in Enum.GetNames(typeof(features)))
                    {
                        int enumVal = (int)Enum.Parse(typeof(features), feature);
                        if (arrconfigFeatures.Contains(Convert.ToString(enumVal)) && arrAdminFeatures.Contains(Convert.ToString(enumVal)))
                        {
                            features currFeatures = (features)enumVal;
                            switch (currFeatures)
                            {
                                case features.Blog:
                                    divAdvBlog.Style.Value = "display:inline";
                                    GetLatestSortBlogPost();
                                    break;
                                case features.RSS:
                                    divAdvRss.Style.Value = "display:inline";
                                    break;
                                case features.Bookmark:
                                    divAdvBookmark.Style.Value = "display:inline";
                                    BindGridBookmarks();
                                    break;
                            }
                        }
                    }
                }
            }

            if (defaultPageType == "0" && !string.IsNullOrWhiteSpace(defaultPage))
            {
                features currFeatures = (features)Convert.ToInt32(defaultPage);
                string[] arrconfigFeatures = ReadConfig.GetFeatures(ReadConfig.getDomainUrl()).Split(',');
                int enumVal = (int)Enum.Parse(typeof(features), currFeatures.ToString());
                if (arrconfigFeatures.Contains(Convert.ToString(enumVal)))
                {
                    switch (currFeatures)
                    {
                        case features.Calendar:
                            Server.Transfer("~/Calendar/Index.aspx");
                            break;
                        case features.Blog:
                            Server.Transfer("~/Features/Index.aspx");
                            break;
                        case features.RSS:
                            Server.Transfer("~/Feeds/Index.aspx");
                            break;
                        case features.Bookmark:
                            Server.Transfer("~/Links/Index.aspx");
                            break;
                        case features.BlogPostSummary:
                            Server.Transfer("~/BlogPostSummary.aspx");
                            break;
                        case features.Search:
                            string defaultSearch = "1";
                            defaultSearch = Convert.ToString(dt.Rows[0]["defaultSearch"]);
                            if (defaultSearch == "2")
                            {
                                Response.Redirect("~/search.aspx?q=" + Searchkeyword);
                            }
                            else
                            {
                                Session["q"] = Searchkeyword;
                                Server.Transfer("~/search.aspx");
                            }
                            break;
                        default:
                            Server.Transfer("~/Features/Index.aspx");
                            break;
                    }
                }
            }
            else if (defaultPageType == "2")
            {
                Server.Transfer("~/Home.aspx");
            }
        }
        catch
        {
        }
    }

    #region Blogs
    private void RandomRows(DataTable dt)
    {
        DataTable dtFeatures = new DataTable();
        foreach (DataColumn dc in dt.Columns)
        {
            if (dc.ColumnName.ToLower() != "random")
            {
                dtFeatures.Columns.Add(dc.ColumnName);
            }
        }
        dtFeatures.Columns.Add("random", typeof(int));

        if (dt.Rows.Count > 0)
        {
            Random rnd = new Random();
            foreach (DataRow dr in dt.Rows)
            {
                DataRow drFeatures = dtFeatures.NewRow();
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToLower() != "random")
                    {
                        drFeatures[dc.ColumnName] = dr[dc.ColumnName];
                    }
                }
                drFeatures["random"] = rnd.Next(100);
                dtFeatures.Rows.Add(drFeatures);
            }
        }
        string random = dp.ExecuteScalar("select ISNULL(RandomiseStatus,0 ) from brand_settings").ToString();
        if (random == "True")
        {
            DataView dv = dtFeatures.DefaultView;
            dv.Sort = "random asc";
            dtFeatures = dv.ToTable();
            Session["featuresdtnewHome"] = dtFeatures;
        }
        else
        {
            Session["featuresdtnewHome"] = dtFeatures;
        }
    }

    private void GetLatestSortBlogPost()
    {
        DataTable dt = new DataTable();
        //cache code 
        string KeySortBlogPosts = "SortBlogPostsHome" + ReadConfig.getDomainUrl().ToLower().Trim();

        if (Cache[KeySortBlogPosts] != null)
        {
            dt = (DataTable)Cache[KeySortBlogPosts];
        }
        else
        {
            DBCommand cmdsortblog = new DBCommand();
            cmdsortblog.CommandText = "SP_Brand_Select_Blog_N_Posts";
            cmdsortblog.CommandType = CommandType.StoredProcedure;
            dt = dp.FillDataTable(cmdsortblog);
            Cache[KeySortBlogPosts] = dt;
        }
        RandomRows(dt);
        if (dt.Rows.Count > 0)
        {
            lblBlogtitle.Text = utils.formatStringForDBSelect(Convert.ToString(dt.Rows[0]["blog_title"]));

            BindItemsListBlog();
        }
    }

    protected void BindItemsListBlog()
    {
        if (Session["featuresdtnewHome"] != null)
        {
            PagedDataSource _PageDataSource = new PagedDataSource();
            DataView dv = ((DataTable)Session["featuresdtnewHome"]).DefaultView;
            if (dv != null)
            {
                DataTable dt = dv.ToTable();
                if (dt.Rows.Count > 10)
                    pnlPagingMainBlog.Visible = true;
                else
                    pnlPagingMainBlog.Visible = false;
                _PageDataSource.DataSource = dv;
                _PageDataSource.AllowPaging = true;
                _PageDataSource.PageSize = 10;
                _PageDataSource.CurrentPageIndex = CurrentPageBlog;
                ViewState["TotalPagesBlog"] = _PageDataSource.PageCount;
                this.lblPageInfoBlog.Text = "Page " + (CurrentPageBlog + 1) + " of " + _PageDataSource.PageCount;
                this.dlstBlog.DataSource = _PageDataSource;
                this.dlstBlog.DataBind();
                this.doPagingBlog();
            }
        }
    }

    #region Private Properties
    private int CurrentPageBlog
    {
        get
        {
            object objPage = ViewState["_CurrentPageBlog"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPageBlog"] = value; }
    }
    private int firstIndexBlog
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndexBlog"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndexBlog"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndexBlog"] = value; }
    }
    private int lastIndexBlog
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndexBlog"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndexBlog"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndexBlog"] = value; }
    }
    #endregion

    private void doPagingBlog()
    {
        lbtnPreviousBlog.Visible = true;
        lbtnNextBlog.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        firstIndexBlog = CurrentPageBlog - (CurrentPageBlog % 10);
        lastIndexBlog = CurrentPageBlog + (10 - (CurrentPageBlog % 10));
        if (lastIndexBlog > Convert.ToInt32(ViewState["TotalPagesBlog"]))
        {
            lastIndexBlog = Convert.ToInt32(ViewState["TotalPagesBlog"]);
            firstIndexBlog = lastIndexBlog - 10;
            lbtnNextBlog.Visible = false;
        }

        if (firstIndexBlog <= 0)
        {
            firstIndexBlog = 0;
            lbtnPreviousBlog.Visible = false;
        }

        for (int i = firstIndexBlog; i < lastIndexBlog; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        this.dlPagingBlog.DataSource = dt;
        this.dlPagingBlog.DataBind();
    }

    protected void lbtnPreviousBlog_Click(object sender, EventArgs e)
    {
        lbtnPreviousBlog.Visible = true;
        lbtnNextBlog.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPageBlog = CurrentPageBlog - 10;
        firstIndexBlog = CurrentPageBlog - (CurrentPageBlog % 10);
        lastIndexBlog = CurrentPageBlog + (10 - (CurrentPageBlog % 10));

        if (lastIndexBlog > Convert.ToInt32(ViewState["TotalPagesBlog"]))
        {
            lastIndexBlog = Convert.ToInt32(ViewState["TotalPagesBlog"]);
            firstIndexBlog = lastIndexBlog - 10;
            lbtnNextBlog.Visible = false;
        }

        if (firstIndexBlog <= 0)
        {
            firstIndexBlog = 0;
            lbtnPreviousBlog.Visible = false;
        }
        CurrentPageBlog = firstIndexBlog;
        for (int i = firstIndexBlog; i < lastIndexBlog; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfoBlog.Text = "Page " + (CurrentPageBlog + 1) + " of " + Convert.ToString(ViewState["TotalPagesBlog"]);
        this.dlPagingBlog.DataSource = dt;
        this.dlPagingBlog.DataBind();
        this.BindItemsListBlog();
    }

    protected void lbtnNextBlog_Click(object sender, EventArgs e)
    {
        lbtnPreviousBlog.Visible = true;
        lbtnNextBlog.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPageBlog = CurrentPageBlog + 10;
        firstIndexBlog = CurrentPageBlog - (CurrentPageBlog % 10);
        lastIndexBlog = CurrentPageBlog + (10 - (CurrentPageBlog % 10));

        if (lastIndexBlog > Convert.ToInt32(ViewState["TotalPagesBlog"]))
        {
            lastIndexBlog = Convert.ToInt32(ViewState["TotalPagesBlog"]);
            firstIndexBlog = lastIndexBlog - 10;
            lbtnNextBlog.Visible = false;
        }

        if (firstIndexBlog <= 0)
        {
            firstIndexBlog = 0;
            lbtnPreviousBlog.Visible = false;
        }
        CurrentPageBlog = firstIndexBlog;
        for (int i = firstIndexBlog; i < lastIndexBlog; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfoBlog.Text = "Page " + (CurrentPageBlog + 1) + " of " + Convert.ToString(ViewState["TotalPagesBlog"]);
        this.dlPagingBlog.DataSource = dt;
        this.dlPagingBlog.DataBind();
        this.BindItemsListBlog();
    }

    protected void dlPagingBlog_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            CurrentPageBlog = Convert.ToInt16(Convert.ToString(e.CommandArgument));
            this.BindItemsListBlog();
        }
    }

    protected void dlPagingBlog_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");
        if (Convert.ToString(lnkbtnPage.CommandArgument) == Convert.ToString(CurrentPageBlog))
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("font-size", "Medium");
            lnkbtnPage.Style.Add("color", "red");
            lnkbtnPage.Font.Bold = true;
        }

    }
    #endregion

    #region BookMarks
    public void BindGridBookmarks()
    {
        DBHelper dp = new DBHelper(ReadConfig.GetConnectionString(), DBSType.MSSQL);
        try
        {
            DataTable dtBookmark = new DataTable();
            DBCommand cmdBookmark = new DBCommand();
            cmdBookmark.CommandType = CommandType.Text;
            cmdBookmark.CommandText = "select *,CASE WHEN LEN(ISNULL(BK_Title,''))> 50 THEN SUBSTRING(BK_Title,1,50)+'..' END AS BK_Title,"
                                    + " CASE WHEN LEN(ISNULL(BK_Desc,''))> 1000 THEN SUBSTRING(BK_Desc,1,1000)+'..' END AS BK_Desc"
                                    + ",isnull(nofollow,0) as nofollow"
                                    + ",isnull(noindex,0) as noindex"
                                    + ",isnull(noopener,0) as noopener"
                                    + ",isnull(noreferrer,0) as noreferrer"
                                    + " from brand_bookmark WHERE Status='True' AND BK_Status<>'0' order by BK_SortOrder desc";
            dtBookmark = dp.FillDataTable(cmdBookmark);
            if (dtBookmark.Rows.Count > 0)
            {
                if (!dtBookmark.Columns.Contains("rel"))
                    dtBookmark.Columns.Add("rel");
                foreach (DataRow dr in dtBookmark.Rows)
                {
                    int nofollow = Convert.ToInt32(dr["nofollow"].ToString());
                    int noindex = Convert.ToInt32(dr["noindex"].ToString());
                    int noopener = Convert.ToInt32(dr["noopener"].ToString());
                    int noreferrer = Convert.ToInt32(dr["noreferrer"].ToString());//RK
                    string strrel = " rel=\"";
                    if (nofollow != 0)
                    {
                        strrel += "nofollow";
                    }
                    if (noindex != 0)
                    {
                        strrel += strrel == " rel=\"" ? "noindex" : " noindex";
                    }
                    if (noopener != 0)
                    {
                        strrel += strrel == " rel=\"" ? "noopener" : " noopener";
                    }
                    if (noreferrer!=0)//RK
                    {
                        strrel += strrel == " rel=\"" ? "noreferrer" : " noreferrer";
                    }
                    strrel += "\"";
                    dr["rel"] = strrel;
                }
            }
            StringBuilder bookmarkLink = new StringBuilder();
            if (dtBookmark.Rows.Count > 0)
            {
                dgrdBookmark.Visible = true;
                lblNoSavedbookmark.Visible = false;

                if (dtBookmark.Rows.Count <= 30)
                {
                    pnlPagingMainLinks.Visible = false;
                }

                DataView dv = dtBookmark.DefaultView;
                _PageDataSourceLinks.DataSource = dv;
                _PageDataSourceLinks.AllowPaging = true;
                _PageDataSourceLinks.PageSize = 30;
                _PageDataSourceLinks.CurrentPageIndex = CurrentPageLinks;
                ViewState["TotalPagesLinks"] = _PageDataSourceLinks.PageCount;
                this.lblPageInfoLinks.Text = "Page " + (CurrentPageLinks + 1) + " of " + _PageDataSourceLinks.PageCount;
                this.dgrdBookmark.DataSource = _PageDataSourceLinks;
                this.dgrdBookmark.DataBind();
                this.doPagingLinks();
            }
            else
            {
                lblNoSavedbookmark.Visible = true;
                dgrdBookmark.Visible = false;
                pnlPagingMainLinks.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblNoSavedbookmark.Text = ex.Message.ToString();
        }
    }

    #region Private Properties
    private int CurrentPageLinks
    {
        get
        {
            object objPage = ViewState["_CurrentPageLinks"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPageLinks"] = value; }
    }
    private int firstIndexLinks
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndexLinks"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndexLinks"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndexLinks"] = value; }
    }
    private int lastIndexLinks
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndexLinks"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndexLinks"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndexLinks"] = value; }
    }
    #endregion

    #region Pagination
    private void doPagingLinks()
    {
        lbtnPreviousLinks.Visible = true;
        lbtnNextLinks.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        firstIndexLinks = CurrentPageLinks - (CurrentPageLinks % 10);
        lastIndexLinks = CurrentPageLinks + (10 - (CurrentPageLinks % 10));
        if (lastIndexLinks > Convert.ToInt32(ViewState["TotalPagesLinks"]))
        {
            lastIndexLinks = Convert.ToInt32(ViewState["TotalPagesLinks"]);
            firstIndexLinks = lastIndexLinks - 10;
            lbtnNextLinks.Visible = false;
        }

        if (firstIndexLinks <= 0)
        {
            firstIndexLinks = 0;
            lbtnPreviousLinks.Visible = false;
        }

        for (int i = firstIndexLinks; i < lastIndexLinks; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        this.dlPagingLinks.DataSource = dt;
        this.dlPagingLinks.DataBind();
    }
    protected void lbtnPreviousLinks_Click(object sender, EventArgs e)
    {
        lbtnPreviousLinks.Visible = true;
        lbtnNextLinks.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPageLinks = CurrentPageLinks - 10;
        firstIndexLinks = CurrentPageLinks - (CurrentPageLinks % 10);
        lastIndexLinks = CurrentPageLinks + (10 - (CurrentPageLinks % 10));
        CurrentPageLinks = firstIndexLinks;
        if (lastIndexLinks > Convert.ToInt32(ViewState["TotalPagesLinks"]))
        {
            lastIndexLinks = Convert.ToInt32(ViewState["TotalPagesLinks"]);
            lbtnNextLinks.Visible = false;
        }

        if (firstIndexLinks <= 0)
        {
            firstIndexLinks = 0;
            lbtnPreviousLinks.Visible = false;
            CurrentPageLinks = 0;
        }

        for (int i = firstIndexLinks; i < lastIndexLinks; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfoLinks.Text = "Page " + (CurrentPageLinks + 1) + " of " + Convert.ToString(ViewState["TotalPages"]);
        this.dlPagingLinks.DataSource = dt;
        this.dlPagingLinks.DataBind();
        this.BindGridBookmarks();
    }
    protected void lbtnNextLinks_Click(object sender, EventArgs e)
    {
        lbtnPreviousLinks.Visible = true;
        lbtnNextLinks.Visible = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        CurrentPageLinks = CurrentPageLinks + 10;
        firstIndexLinks = CurrentPageLinks - (CurrentPageLinks % 10);
        lastIndexLinks = CurrentPageLinks + (10 - (CurrentPageLinks % 10));
        CurrentPageLinks = firstIndexLinks;
        if (lastIndexLinks > Convert.ToInt32(ViewState["TotalPagesLinks"]))
        {
            lastIndexLinks = Convert.ToInt32(ViewState["TotalPagesLinks"]);
            lbtnNextLinks.Visible = false;
        }

        if (firstIndexLinks <= 0)
        {
            firstIndexLinks = 0;
            lbtnPreviousLinks.Visible = false;
            CurrentPageLinks = 0;
        }

        for (int i = firstIndexLinks; i < lastIndexLinks; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.lblPageInfoLinks.Text = "Page " + (CurrentPageLinks + 1) + " of " + Convert.ToString(ViewState["TotalPages"]);
        this.dlPagingLinks.DataSource = dt;
        this.dlPagingLinks.DataBind();
        this.BindGridBookmarks();
    }
    protected void dlPagingLinks_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            CurrentPageLinks = Convert.ToInt16(Convert.ToString(e.CommandArgument));
            this.BindGridBookmarks();
        }
    }
    protected void dlPagingLinks_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");

        lnkbtnPage.PostBackUrl = "~/links/index.aspx";
        if (Convert.ToString(lnkbtnPage.CommandArgument) == Convert.ToString(CurrentPageLinks))
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("font-size", "Medium");
            lnkbtnPage.Style.Add("color", "red");
            lnkbtnPage.Font.Bold = true;
        }
    }
    #endregion
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}