﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class terms : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Terms";
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}