﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forgot Password</title>
</head>
<body>
    <div id="content-wrapper-holder">
        <div class="top-bkg-content">
        </div>
        <div id="content-wrapper-logon">
            <div class="imageLogo">
                <img id="imgLogo" runat="server" alt="logo" />
            </div>
            <form id="form1" runat="server">
                <div class="details">
                    <!--- BEGIN MAIN BODY --->
                    <asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">
                        <asp:View ID="viewForgetPassword" runat="server">
                            <b>Forgot your password?</b><br /><br />
                            Please enter your registered email address and the code shown below. <br />
                            We will then send you an e-mail reconfirming your login details.
                            <div class="loginMain">
                                <asp:Label ID="lblmessage" CssClass="messages" runat="server" Text=""></asp:Label>
                                <br />
                                <br />
                                Email address<br />
                                <asp:TextBox ID="txtEmailID" CssClass="textbox" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtEmailID" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="txtEmailID" 
                                    ErrorMessage="please enter email address" ValidationGroup="PwdVCG" ForeColor="Red"></asp:RequiredFieldValidator>
                                <br />
                                <br />
                                <asp:RegularExpressionValidator ID="REVtxtEmailID" runat="server" SetFocusOnError="true" ControlToValidate="txtEmailID" Display="Dynamic" ErrorMessage="invalid e-mail address! please re-enter"
                                    ValidationGroup="PwdVCG" ValidationExpression="^[a-zA-Z]?[\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" ForeColor="Red"></asp:RegularExpressionValidator>
                                <br />
                                <img id="imgcaptcha" runat="server" src="captcha.aspx" alt="captcha" /><br />
                                Enter the code shown<br />
                                <asp:TextBox ID="CodeNumberTextBox" CssClass="textbox" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCode" SetFocusOnError="true" Display="Dynamic" runat="server" ControlToValidate="CodeNumberTextBox" ErrorMessage="please enter code"
                                    ValidationGroup="PwdVCG" ForeColor="Red"></asp:RequiredFieldValidator>
                                <br />
                                <br />
                                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="submit" CssClass="btnBgClass" ValidationGroup="PwdVCG" />
                                <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btnBgClass" Text="back" />
                            </div>
                        </asp:View>
                        <asp:View ID="viewPWDMailmsg" runat="server">
                            <table class="table-full">
                                <tr>
                                    <td>
                                        <br />
                                        Password sent
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <br />
                                        Your password has been sent to you, please check your email.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:Button ID="btnLogin" CssClass="btnBgClass" Text="login" runat="server" OnClick="btnLogin_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
                <!--- END OF MAIN BODY --->
            </form>
        </div>
        <div class="bottom-bkg-content">
        </div>
    </div>
</body>
</html>
