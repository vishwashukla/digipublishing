using System;
using System.IO;
using System.Web.UI;

public partial class PrivacyPolicy : System.Web.UI.Page
{
    public string privacyFileContents;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ReadConfig.getDomainUrl() + " - Privacy Policy";
        string domain = ReadConfig.getDomainUrl().ToLower().Replace("www.","").Trim();
        if (File.Exists(Server.MapPath(@"~/privacyterm/" + domain.ToLower() + "/privacy.html")))
            privacyFileContents = File.ReadAllText(Server.MapPath(@"~/privacyterm/" + domain.ToLower() + "/privacy.html"));
        else
            privacyFileContents = "";
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.Theme = ReadConfig.GetTheme();
        base.OnPreInit(e);
    }
}
