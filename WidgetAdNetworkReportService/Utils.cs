﻿using System;
using System.Configuration;
using System.IO;
using System.Data;

namespace WidgetAdNetworkReportService
{
    public static class Utils
	{
		public static void WriteErrorLog(Exception ex)
		{
			StreamWriter sw = null;
			try
			{
				sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
				sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
				sw.Flush();
				sw.Close();
			}
			catch
			{
			}
		}

		public static void WriteErrorLog(string Message)
		{
			StreamWriter sw = null;
			try
			{
				sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
				sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
				sw.Flush();
				sw.Close();
			}
			catch
			{
			}
		}

		public static void ErrorLog(string Name, string Message)
		{
			string urlLogPath = Convert.ToString(ConfigurationManager.AppSettings["ErrorLogFile"]);
			string file = urlLogPath;
			if (File.Exists(file))
			{
				File.AppendAllText(file, "\n================================================\n");
				File.AppendAllText(file, "Event : " + Name + " \r\n ");
				File.AppendAllText(file, "Error : " + Message + " \r\n ");
				File.AppendAllText(file, "Time : " + System.DateTime.Now + " \r\n ");
			}
			else
			{
				File.Create(urlLogPath);
				File.AppendAllText(file, "\n================================================\n");
				File.AppendAllText(file, "Event : " + Name + " \r\n ");
				File.AppendAllText(file, "Error : " + Message + " \r\n ");
				File.AppendAllText(file, "Time : " + System.DateTime.Now + " \r\n ");
			}
		}

		public static void Log(string message, string MethodName)
		{
            string urlLogPath = Convert.ToString(ConfigurationManager.AppSettings["LogFile"]);
			string file = urlLogPath;
			if (File.Exists(file))
			{
				File.AppendAllText(file, "\n=================\n");
				File.AppendAllText(file, MethodName + message + System.DateTime.Now + " \r\n ");
			}
			else
			{
				File.Create(urlLogPath);
				File.AppendAllText(file, "File created on --" + System.DateTime.Now + " ");
				File.AppendAllText(file, MethodName + message + System.DateTime.Now + " \r\n ");
			}
		}

		public static string formatStringForDBInsert(string strVal)
		{
			DataFormatter df = new DataFormatter();
			df.setDefaultInputSearch();
			string strOut = df.getInputString(strVal);
			return strOut;
		}

		public static string formatStringForDBSelect(string strVal)
		{
			DataFormatter df = new DataFormatter();
			df.setDefaultOutputSearch();
			string strOut = df.getInputString(strVal);
			return strOut;
		}

        public static int GetStartADNetworkAndWidgetReport()
        {
            int intAllowAdnetworkData = 0;
            DBHelper dpSA = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
            DataTable dtcheck = dpSA.FillDataTable("select isnull(Start_Stop,0) as Start_Stop from brand_Record_Data");
            if (dtcheck.Rows.Count > 0)
            {
                intAllowAdnetworkData = Convert.ToInt32(dtcheck.Rows[0]["Start_Stop"].ToString());
            }
            return intAllowAdnetworkData;
        }

	}
}
