﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace WidgetAdNetworkReportService
{
    class DiskCheck
    {
        public DiskCheck()
        {

        }

        public void MainFunction()
        {
            GetTotalFreeSpace("C");
        }

        private void GetTotalFreeSpace(string driveName)
        {
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == (driveName + ":\\"))
                {
                    double size = 0; ;
                    double gb = 0;
                    string suffix = "MB";
                    double mb = (drive.TotalFreeSpace / 1024f) / 1024f;
                    size = mb;
                    if (mb > 1024)
                    {
                        gb = mb / 1024f;
                        size = gb;
                        suffix = "GB";
                    }
                    size = Math.Round(size, 2);

                    if (mb <= 2048)
                    {
                        sendMail(size + suffix);
                    }
                }
            }
        }

        public void sendMail(string available)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient();
                System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                MailAddress dev1 = new MailAddress(Convert.ToString(ConfigurationManager.AppSettings["Dev1EmailAddress"]));
                MailAddress dev2 = new MailAddress(Convert.ToString(ConfigurationManager.AppSettings["Dev2EmailAddress"]));
                MailAddress admin = new MailAddress(Convert.ToString(ConfigurationManager.AppSettings["AdminEmailAddress"]));
                MailAddress toAddress = new MailAddress(ConfigLoader.GetSupportEmailAddress());

                message.To.Add(dev1);
                message.To.Add(dev2);
                //message.To.Add(admin);

                message.From = toAddress;

                message.Subject = "Low Disk Storage!";

                string strDateTime = DateTime.Now.ToString();
                StringBuilder SB = new StringBuilder();
                SB.AppendLine("Warning!");
                SB.AppendLine("");
                SB.AppendLine("This is to notify that the disk storage space of 'C:' drive is running low.");
                SB.AppendLine("");
                SB.AppendLine("Currently available storage space is: " + available + ", as on " + strDateTime);

                message.Body = SB.ToString();
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                Utils.ErrorLog("Main Error", ex.Message + "\n========\n" + ex.StackTrace);
            }
        }
    }
}
