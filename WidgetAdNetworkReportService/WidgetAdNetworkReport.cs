﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace WidgetAdNetworkReportService
{
    class WidgetAdNetworkReport
    {
        string strBannerMailTo = ConfigLoader.GetSupportEmailAddress();
        string strBannerMailCC = "";
        string strBannerMailSub = "Click and Impression Log Report";
        string strAdNetworkMailTo = ConfigLoader.GetSupportEmailAddress();
        string strAdNetworkMailSub = "Ad Network Log Report";
        string strBannerReportPath = "C:/Agniwork/BannerReports/";
        string strBannerClicksFileName = "AdClicksDigipublishing.xls";
        string strBannerMailLogFilePath = "C:/Agniwork/BannerReports/logDigipublishing.txt";
        string strfromAdd = ConfigLoader.GetSupportEmailAddress();
        string strBccAdd = "";
        string strApp_name_LogMail = ConfigLoader.GetSupportEmailAddress();

        DBHelper dp = new DBHelper(ConfigLoader.GetConnectionString(), DBSType.MSSQL);
        DBHelper dpreport = new DBHelper(ConfigLoader.GetReportDBConnectionString(), DBSType.MSSQL);

        public WidgetAdNetworkReport()
        {

        }

        public void MainFunction()
        {
            int intdtEmailReport = 0;
            DataTable dtEmailReport = dp.FillDataTable("select isnull(EmailReport,0) as EmailReport from brand_Record_Data");
            if (dtEmailReport.Rows.Count > 0)
            {
                intdtEmailReport = Convert.ToInt32(dtEmailReport.Rows[0]["EmailReport"].ToString().Trim());
            }
            bool result1 = false;
            bool result2 = false;
            if (Utils.GetStartADNetworkAndWidgetReport() == 1)
            {
                string strToday = DateTime.Now.AddDays(-1).Year + "-" + DateTime.Now.AddDays(-1).Month + "-" + DateTime.Now.AddDays(-1).Day;
                if (intdtEmailReport == 1)
                {
                    if (GetBannerClicks(strToday) == true)
                    {
                        result1 = sendEmailReport(strBannerMailTo, strBannerMailCC, strBannerMailSub, strToday);
                    }
                }
                else
                {
                    result1 = true;
                }

                if (intdtEmailReport == 1)
                {
                    result2 = sendAdNetWorkReport(strAdNetworkMailTo, strAdNetworkMailSub, strToday);
                }
                else
                {
                    result2 = true;
                }
            }
            else
            {
                result1 = true;
                result2 = true;
            }
            if (result1 == true && result2 == true)
            {
                TruncateLogData();
            }
        }

        private bool GetBannerClicks(string strToday)
        {
            bool result = false;
            try
            {
                DataTable dt = new DataTable();
                DBCommand strSQL = new DBCommand();
                strSQL.CommandText = "SP_Brand_Select_View_Select_Widget_Clicks";
                strSQL.CommandType = CommandType.StoredProcedure;
                dt = dpreport.FillDataTable(strSQL);
                string strBannerClicksLogFile = strBannerReportPath + strToday + "-" + strBannerClicksFileName;
                result = CreateExcelFile(dt, strBannerClicksLogFile); ;
            }
            catch
            {
            }
            return result;
        }

        private DataTable GetImpressionsData()
        {
            DataTable dt = new DataTable();
            try
            {
                DBCommand strSQL = new DBCommand();
                strSQL.CommandText = "SP_Brand_Select_View_Select_Widget_Impressions";
                strSQL.CommandType = CommandType.StoredProcedure;
                dt = dpreport.FillDataTable(strSQL);
            }
            catch
            {
            }
            return dt;
        }

        public bool CreateExcelFile(DataTable dt, string sExcelFile)
        {
            bool bFileCreated = false;
            StringBuilder sTableStart = new StringBuilder();
            sTableStart.Append("<HTML><BODY><TABLE Border=1>");

            foreach (DataColumn dl in dt.Columns)
            {
                sTableStart.Append("<TD><B>" + Convert.ToString(dl.ColumnName) + "</B></TD>");
            }

            foreach (DataRow dr in dt.Rows)
            {
                sTableStart.Append("<TR>");
                foreach (DataColumn dl in dt.Columns)
                {
                    sTableStart.Append("<TD>" + Convert.ToString(dr[dl.Caption]) + "</TD>");
                }
                sTableStart.Append("</TR>");
            }
            sTableStart.Append("</TABLE></BODY></HTML>");

            System.IO.StreamWriter oExcelWrite = null;
            oExcelWrite = System.IO.File.CreateText(sExcelFile);
            oExcelWrite.WriteLine(sTableStart.ToString());
            oExcelWrite.Close();
            bFileCreated = true;
            return bFileCreated;
        }

        private bool sendEmailReport(string strEmailTo, string strEmailCc, string strSubject, string strToday)
        {
            bool result = false;
            string strBannerClicksLogFile = strBannerReportPath + strToday + "-" + strBannerClicksFileName;

            string file = strBannerMailLogFilePath;
            MailMessage MailMsg = new MailMessage();
            try
            {
                SmtpClient smtpClient = new SmtpClient();

                System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");

                Attachment attachemailClicksFile = new Attachment(strBannerClicksLogFile);
                attachemailClicksFile.Name = strToday + "-" + strBannerClicksFileName;

                MailMsg.Attachments.Add(attachemailClicksFile);

                StringBuilder strBody = new StringBuilder();

                strBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
                strBody.Append("<html><head>");
                strBody.Append("<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
                strBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
                strBody.Append("<style type='text/css'>");
                strBody.Append("body { font-family:arial;font-size:12px;color:#000000;} ");
                strBody.Append("div.footer{background-color:#eeeeee;border:1px solid #000000;width:90%;font-family:tahoma; font-size:10px;font-weight:bold;color:#000000;padding:10px;}");
                strBody.Append("</style>");
                strBody.Append("Hi,");
                strBody.Append("<br/>");
                strBody.Append("<br/>");
                strBody.Append("<b>Ad Network click log report</b> is attached. And <b>Ad Network impression log report</b> is following :");
                strBody.Append("<br/>");
                strBody.Append("<br/>");
                strBody.Append(CreateHTML(GetImpressionsData()));
                strBody.Append("<br/>");

                MailMsg.BodyEncoding = Encoding.Default;
                MailMsg.To.Add(strEmailTo);
                if (strEmailCc.Trim() != "")
                    MailMsg.CC.Add(strEmailCc);
                MailMsg.Priority = MailPriority.High;
                if (strBccAdd.Trim() != "")
                    MailMsg.Bcc.Add(strBccAdd);
                MailMsg.From = new MailAddress(strfromAdd, strApp_name_LogMail);
                MailMsg.Subject = strSubject;
                MailMsg.Body = strBody.ToString();
                MailMsg.IsBodyHtml = true;
                AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody.ToString(), HTMLType);

                smtpClient.Send(MailMsg);
                if (File.Exists(file))
                {
                    File.AppendAllText(file, "\n=================\n");
                    File.AppendAllText(file, " Email sent to " + strEmailTo + " " + System.DateTime.Now + " \r\n ");
                }
                else
                {
                    File.Create(strBannerMailLogFilePath);
                    File.AppendAllText(file, "File created on --" + System.DateTime.Now + " \r\n ");
                    File.AppendAllText(file, " Email sent to " + strEmailTo + " " + System.DateTime.Now + " \r\n ");
                }
                result = true;
            }
            catch (Exception ex)
            {
                if (File.Exists(file))
                {
                    File.AppendAllText(file, "\n=================\n");
                    File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                }
                else
                {
                    File.Create(strBannerMailLogFilePath);
                    File.AppendAllText(file, "File created on --" + System.DateTime.Now + " \r\n ");
                    File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                }
            }
            finally
            {
                MailMsg.Attachments.Dispose();
                MailMsg.Dispose();
                try
                {
                    File.Delete(strBannerClicksLogFile);
                }
                catch (Exception ex) { File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n "); }
            }
            return result;
        }

        private string CreateHTML(DataTable dt)
        {
            StringBuilder sTableStart = new StringBuilder();
            sTableStart.Append("<HTML><BODY><TABLE Border=1>");
            foreach (DataColumn dl in dt.Columns)
            {
                sTableStart.Append("<TD><B>" + Convert.ToString(dl.ColumnName) + "</B></TD>");
            }
            foreach (DataRow dr in dt.Rows)
            {
                sTableStart.Append("<TR>");
                foreach (DataColumn dl in dt.Columns)
                {
                    sTableStart.Append("<TD>" + Convert.ToString(dr[dl.Caption]) + "</TD>");
                }
                sTableStart.Append("</TR>");
            }
            sTableStart.Append("</TABLE></BODY></HTML>");
            return sTableStart.ToString();
        }

        private DataTable GetAdNetWorkReport()
        {
            DataTable dt = new DataTable();
            try
            {
                DBCommand strSQL = new DBCommand();
                strSQL.CommandText = "SP_Brand_Select_View_Select_AdNetwork_Report";
                strSQL.CommandType = CommandType.StoredProcedure;
                dt = dpreport.FillDataTable(strSQL);
            }
            catch
            {
            }
            return dt;
        }

        private bool sendAdNetWorkReport(string strEmailTo, string strSubject, string strToday)
        {
            bool result = false;
            MailMessage MailMsg = new MailMessage();
            try
            {
                SmtpClient smtpClient = new SmtpClient();
                System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
                StringBuilder strBody = new StringBuilder();

                strBody.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
                strBody.Append("<html><head>");
                strBody.Append("<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">");
                strBody.Append("<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>");
                strBody.Append("<style type='text/css'>");
                strBody.Append("body { font-family:arial;font-size:12px;color:#000000;} ");
                strBody.Append("div.footer{background-color:#eeeeee;border:1px solid #000000;width:90%;font-family:tahoma; font-size:10px;font-weight:bold;color:#000000;padding:10px;}");
                strBody.Append("</style>");
                strBody.Append("Hi,");
                strBody.Append("<br/>");
                strBody.Append("<br/>");
                strBody.Append("<b>Ad Network log report</b> is following :");
                strBody.Append("<br/>");
                strBody.Append("<br/>");
                strBody.Append(CreateHTML(GetAdNetWorkReport()));
                strBody.Append("<br/>");

                MailMsg.BodyEncoding = Encoding.Default;
                MailMsg.To.Add(strEmailTo);
                MailMsg.Priority = MailPriority.High;
                if (strBccAdd.Trim() != "")
                    MailMsg.Bcc.Add(strBccAdd);
                MailMsg.From = new MailAddress(strfromAdd, strApp_name_LogMail);
                MailMsg.Subject = strSubject;
                MailMsg.Body = strBody.ToString();
                MailMsg.IsBodyHtml = true;
                AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody.ToString(), HTMLType);

                smtpClient.Send(MailMsg);
                if (File.Exists(strBannerMailLogFilePath))
                {
                    File.AppendAllText(strBannerMailLogFilePath, "\n=================\n");
                    File.AppendAllText(strBannerMailLogFilePath, " Email sent to " + strEmailTo + " " + System.DateTime.Now + " \r\n ");
                }
                else
                {
                    File.Create(strBannerMailLogFilePath);
                    File.AppendAllText(strBannerMailLogFilePath, "File created on --" + System.DateTime.Now + " \r\n ");
                    File.AppendAllText(strBannerMailLogFilePath, " Email sent to " + strEmailTo + " " + System.DateTime.Now + " \r\n ");
                }
                result = true;
            }
            catch (Exception ex)
            {
                if (File.Exists(strBannerMailLogFilePath))
                {
                    File.AppendAllText(strBannerMailLogFilePath, "\n=================\n");
                    File.AppendAllText(strBannerMailLogFilePath, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                }
                else
                {
                    File.Create(strBannerMailLogFilePath);
                    File.AppendAllText(strBannerMailLogFilePath, "File created on --" + System.DateTime.Now + " \r\n ");
                    File.AppendAllText(strBannerMailLogFilePath, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                }
            }
            finally
            {
                MailMsg.Dispose();
            }
            return result;
        }

        private void TruncateLogData()
        {
            string sqlSel = "Select MaxDays_To_StoreData FROM brand_Record_Data";
            DataTable dt = dp.FillDataTable(sqlSel);
            string MaxDays = "";
            if (dt.Rows.Count > 0)
            {
                MaxDays = "-" + dt.Rows[0]["MaxDays_To_StoreData"].ToString().Trim();
            }
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(ConfigLoader.GetReportDBConnectionString());
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandTimeout = 3000;
            cmd.CommandText = "SP_DELETE_LogData";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@DateDifference", SqlDbType.Int).Value = Convert.ToInt32(MaxDays.ToString().Trim());
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
