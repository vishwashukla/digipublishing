﻿using System.Collections;
using System.Data;

namespace WidgetAdNetworkReportService
{
    public class DBCommand
    {
        /// <summary>
        /// A hash table contains the parameters
        /// </summary>
        private Hashtable _Parameters = new Hashtable();
        private string _CommandText;
        private CommandType _CommandType;

        /// <summary>
        /// A hashtable containg parameters
        /// </summary>
        public Hashtable Parameters
        {
            get { return this._Parameters; }
        }

        /// <summary>
        /// SQL Command text
        /// </summary>
        public string CommandText
        {
            get { return this._CommandText; }
            set { this._CommandText = value; }
        }

        /// <summary>
        /// SQL command Type
        /// </summary>
        public CommandType CommandType
        {
            get { return this._CommandType; }
            set { this._CommandType = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DBCommand(string strCommandText)
        {
            this._CommandText = strCommandText;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public DBCommand()
        {

        }

        /// <summary>
        /// Adds a parameter to the Command
        /// </summary>
        public void AddParameter(string parameter, object value)
        {
            _Parameters[parameter] = value;
        }
    }
}
