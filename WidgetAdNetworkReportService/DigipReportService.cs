﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;


namespace WidgetAdNetworkReportService
{
    public partial class DigipReportService : ServiceBase
	{
        private System.Timers.Timer Servicetimer = null;
        private EventLog evtLog;
		public DigipReportService()
		{
			InitializeComponent();
		}

        protected override void OnStart(string[] args)
        {
            string serviceName = Convert.ToString(ConfigurationManager.AppSettings["ServiceName"]);
            Servicetimer = new System.Timers.Timer();
            Servicetimer.Elapsed += new System.Timers.ElapsedEventHandler(_timer_Elapsed);

            string file = Convert.ToString(ConfigurationManager.AppSettings["LogFile"]);
            if (File.Exists(file))
            {
                File.AppendAllText(file, "\n=================\n");
                File.AppendAllText(file, serviceName + " is up and started running at " + System.DateTime.Now + " \r\n ");
            }
            else
            {
                File.Create(file);
                File.AppendAllText(file, "File created on --" + System.DateTime.Now + " ");
                File.AppendAllText(file, serviceName + " is up and started running at " + System.DateTime.Now + " \r\n ");
            }
            CreateEventLog(serviceName);
            SetTimerOnStart();
        }

        private void CreateEventLog(string serviceName)
        {
            if (!EventLog.SourceExists(serviceName))
            {
                EventLog.CreateEventSource(serviceName, "WidgetAdNetworkReportService");
            }
            this.evtLog = new EventLog();
            evtLog.Source = serviceName;
        }

        private void SetTimerOnStart()
        {
            double timeinterval = Convert.ToDouble(15);
            TimeSpan ts = System.DateTime.Now.AddSeconds(timeinterval) - System.DateTime.Now;
            double inter = (double)ts.TotalMilliseconds;
            Servicetimer.Interval = inter;
            Servicetimer.Start();
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string serviceName = Convert.ToString(ConfigurationManager.AppSettings["ServiceName"]);
            string file = Convert.ToString(ConfigurationManager.AppSettings["LogFile"]);
            if (File.Exists(file))
            {
                File.AppendAllText(file, "\n=================\n");
                File.AppendAllText(file, serviceName + " is in timer event at " + System.DateTime.Now + " \r\n ");
            }
            else
            {
                File.Create(file);
                File.AppendAllText(file, "File created on --" + System.DateTime.Now + " ");
                File.AppendAllText(file, serviceName + " is in timer event at " + System.DateTime.Now + " \r\n ");
            }
            Servicetimer.Stop();
            SetTimer();
            try
            {
                MainServiceFucntion();
            }
            catch (Exception ex)
            {
                Utils.ErrorLog("Main Error", ex.Message + "\n========\n" + ex.StackTrace);
            }
        }

        private void SetTimer()
        {
            double inter = (double)GetNextInterval();
            Servicetimer.Interval = inter;
            Servicetimer.Start();
        }

        private double GetNextInterval()
        {
            TimeSpan ts = new TimeSpan();
            double timeinterval = Convert.ToDouble(ConfigurationManager.AppSettings["timeinterval"].ToString());
            ts = System.DateTime.Now.AddMinutes(timeinterval) - System.DateTime.Now;
            return ts.TotalMilliseconds;
        }

        protected override void OnStop()
        {
            string serviceName = Convert.ToString(ConfigurationManager.AppSettings["ServiceName"]);
            string file = Convert.ToString(ConfigurationManager.AppSettings["LogFile"]);
            if (File.Exists(file))
            {
                File.AppendAllText(file, "\n=================\n");
                File.AppendAllText(file, serviceName + " is stopped running at " + System.DateTime.Now + " \r\n ");
            }
            else
            {
                File.Create(file);
                File.AppendAllText(file, "File created on --" + System.DateTime.Now + " ");
                File.AppendAllText(file, serviceName + " is stopped running at " + System.DateTime.Now + " \r\n ");
            }
        }

        private void MainServiceFucntion()
        {
            TimeSpan start = new TimeSpan(1, 0, 0);
            TimeSpan end = new TimeSpan(2, 0, 0);
            TimeSpan now = DateTime.Now.TimeOfDay;
            if ((now > start) && (now < end))
            {
                WidgetAdNetworkReport WANR = new WidgetAdNetworkReport();
                WANR.MainFunction();
            }

            DiskCheck DC = new DiskCheck();
            DC.MainFunction();
        }
	}
}
