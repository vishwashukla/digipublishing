﻿using System.ServiceProcess;

namespace WidgetAdNetworkReportService
{
    static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[] 
            { 
                new DigipReportService() 
            };
			ServiceBase.Run(ServicesToRun);
		}
	}
}
