using System;
using System.Timers;
using System.IO;
using System.Configuration;
using System.Net;


namespace GeoIPSchedulerDigip
{
    public class ScheduleService : System.ServiceProcess.ServiceBase
    {
        private string timeString;
        public static string strmsgForAdvace = "";
        public static string strmsgForBasic = "";
        private System.Timers.Timer timer;
        public string zipPathAndFile = "";
        public string outputFolder = "";

        public ScheduleService()
        {
            // This call is required by the Windows.Forms Component Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitComponent call

            int strTime = Convert.ToInt32(ConfigurationManager.AppSettings["callDuration"]);
            zipPathAndFile = System.Windows.Forms.Application.StartupPath + "\\Data\\GeoIPDB.tar.gz";
            outputFolder = System.Windows.Forms.Application.StartupPath + "\\OutPutData\\";
           
            timer = new System.Timers.Timer();
            double inter = (double)GetNextInterval();
            timer.Interval = inter;
            timer.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
        }

        # region Logic to get the number of milliseconds before the next fire
        private double GetNextInterval()
        {
            timeString = ConfigurationManager.AppSettings["StartTime"];
            DateTime t = DateTime.Parse(timeString);
            TimeSpan ts = new TimeSpan();

            ts = t - System.DateTime.Now;
            if (ts.TotalMilliseconds < 0)
            {
                ts = t.AddDays(1) - System.DateTime.Now;
            }

            return ts.TotalMilliseconds;
        }

        private void SetTimer()
        {
            try
            {
                double inter = (double)GetNextInterval();
                timer.Interval = inter;
                timer.Start();
            }
            catch (Exception ex)
            {
            }
        }
        # endregion


        // The main entry point for the process
        [STAThread]
        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = New System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
            //
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ScheduleService() };

            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // ScheduleService
            // 
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.ServiceName = "GeoSchedulerDigip";

        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Set things in motion so your service can do its work.
        /// </summary>
        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            /// set the timer interval and start the service

            timer.AutoReset = true;
            timer.Enabled = true;

            //remove
            string urlLogPath = System.Windows.Forms.Application.StartupPath + "\\Schedulerlog\\log.txt";
            string file = urlLogPath;
            if (File.Exists(file))
            {
                File.AppendAllText(file, "\n=================\n");
                File.AppendAllText(file, "On Start" + System.DateTime.Now + " \r\n ");
            }
            else
            {
                File.Create(urlLogPath);
                File.AppendAllText(file, "File created on --" + System.DateTime.Now + " \r\n ");
                File.AppendAllText(file, "On Start" + System.DateTime.Now + " \r\n ");
            }
            //
        }

        /// <summary>
        /// Stop this service.
        /// </summary>
        /// 
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            timer.AutoReset = false;
            timer.Enabled = false;

        }

        private void ServiceTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            string urlLogPath = System.Windows.Forms.Application.StartupPath + "\\Schedulerlog\\log.txt";
            string file = urlLogPath;
            string UpdatedDay = ConfigurationManager.AppSettings["UpdatedDay"];
            string today = DateTime.Now.DayOfWeek.ToString();
            try
            {
                if (today == UpdatedDay)
                {
                    //string url = "http://www.maxmind.com/app/download_new?edition_id=106&suffix=tar.gz&license_key=UqLFaEvRhJmM";
                    string url = ConfigurationManager.AppSettings["SiteURL"];
                    try
                    {
                        using (WebClient Client = new WebClient())
                        {
                            Client.DownloadFile(url, zipPathAndFile);
                        }
                        ZipUtil.Extract(this.zipPathAndFile, this.outputFolder, file);
                    }
                    catch (Exception ex)
                    {
                        if (File.Exists(file))
                        {
                            File.AppendAllText(file, "\n=================\n");
                            File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                        }
                        else
                        {
                            File.Create(urlLogPath);
                            File.AppendAllText(file, "File created on --" + System.DateTime.Now + " \r\n ");
                            File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                if (File.Exists(file))
                {
                    File.AppendAllText(file, "\n=================\n");
                    File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                }
                else
                {
                    File.Create(urlLogPath);
                    File.AppendAllText(file, "File created on --" + System.DateTime.Now + " \r\n ");
                    File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
                }
            }

            timer.Stop();
            System.Threading.Thread.Sleep(1500000);
            SetTimer();

        }
    }
}

