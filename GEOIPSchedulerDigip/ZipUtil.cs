﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Tar;
using System.IO.Compression;
using System.Text;
using System.Net.Mail;
using System.Configuration;

public static class ZipUtil
{
    public static void Extract(string zipPathAndFile, string outputFolder, string file)
    {
        TarArchive ta = TarArchive.CreateInputTarArchive(new GZipStream(new FileStream(zipPathAndFile, FileMode.Open, FileAccess.Read), CompressionMode.Decompress));
        ta.ExtractContents(outputFolder);
        ta.Close();
        CopyDatFile(outputFolder, file);

    }

    public static void CopyDatFile(string inputFolder, string file)
    {
        string urlLogPath = System.Windows.Forms.Application.StartupPath + "\\Schedulerlog\\log.txt";
        string[] subdirectoryEntries = Directory.GetDirectories(inputFolder);
        string dirName = "";
        foreach (string subdirectory in subdirectoryEntries)
            dirName = new DirectoryInfo(subdirectory).Name;
        inputFolder = inputFolder + dirName; 
        string[] filePaths = Directory.GetFiles(inputFolder, "*.dat");
        string outputFolder = ConfigurationManager.AppSettings["OnDemandPath"];
        try
        {
            FileInfo fileInfo = new FileInfo(outputFolder + "GeoIP.dat");
            if (File.Exists(outputFolder + "GeoIP.dat"))
                fileInfo.Delete();
            File.Copy(filePaths[0], outputFolder + "GeoIP.dat", true);
            sendEmail();
            if (Directory.Exists(inputFolder))
            {
                Directory.Delete(inputFolder, true);
            }
        }
        catch (Exception ex)
        {
            if (File.Exists(file))
            {
                File.AppendAllText(file, "\n=================\n");
                File.AppendAllText(file, ex.Message.ToString());
            }
            else
            {
                File.Create(urlLogPath);
                File.AppendAllText(file, "File created on --" + System.DateTime.Now + " ");
                File.AppendAllText(file, ex.Message.ToString());
            }
        }
    }

    private static void sendEmail()
    {
        string urlLogPath = System.Windows.Forms.Application.StartupPath + "\\Schedulerlog\\log.txt";
        string file = urlLogPath;

        try
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage MailMsg = new MailMessage();

            string FromName = ConfigurationManager.AppSettings["fromAdd"].ToString();
            string ToName = ConfigurationManager.AppSettings["ToAdd"].ToString();
            string BccName = ConfigurationManager.AppSettings["BccAdd"].ToString();

            System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
            string Subject = "Notice About GeoUpdate";
            string strBody = "";
            strBody = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
            strBody += "<html><head>";
            strBody += "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">";
            strBody += "<meta content=\"MSHTML 6.00.5700.6\" name=GENERATOR>";
            strBody += "<style type='text/css'>";
            strBody += "table { font-family:arial;font-size:12px;color:#000000;} ";
            strBody += "div.footer{background-color:#eeeeee;border:1px solid #000000;width:90%;font-family:tahoma; font-size:10px;font-weight:bold;color:#000000;padding:10px;}";
            strBody += "</style>";
            strBody += "</head><body><table>";
            strBody += "<tr><td>Hi";
            strBody += "</td></tr>";
            strBody += "<tr><td></br></td></tr>";
            strBody += "<tr><td>Your GeoDataBase Is Updated on " + ConfigurationManager.AppSettings["ServerName"].ToString() + " server "; 
            strBody += "</td></tr>";

            MailMsg.BodyEncoding = Encoding.Default;
            MailMsg.To.Add(ToName);
            MailMsg.Priority = MailPriority.High;
            MailMsg.Bcc.Add(BccName);
            MailMsg.From = new MailAddress(FromName, "BrandStation");
            MailMsg.Subject = Subject;
            MailMsg.Body = strBody;
            MailMsg.IsBodyHtml = true;
            AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(strBody, HTMLType);

            smtpClient.Send(MailMsg);
        }
        catch (Exception ex)
        {
            if (File.Exists(file))
            {
                File.AppendAllText(file, "\n=================\n");
                File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
            }
            else
            {
                File.Create(urlLogPath);
                File.AppendAllText(file, "File created on --" + System.DateTime.Now + " \r\n ");
                File.AppendAllText(file, ex.Message.ToString() + " " + System.DateTime.Now + " \r\n ");
            }
        }
    }
}
